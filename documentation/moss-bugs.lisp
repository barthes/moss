﻿;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;16/06/15
;;;		
;;;		B U G S - I M P R O V E M E N T S - (File moss-bugs.lisp)
;;;		Copyright Barthes@UTC 2010-
;;;	
;;;==========================================================================

BUGS
====

160615
In the find-subset function, when trying to get objects from an entry point, one
should take into account the current language, meaning that the =make-entry method
should be language sensitive. I.e. in case of an MLN value, it should compute 
only entry points for the current language. Then, if we have several synonyms in
the MLN value of the attribute clause, it should collect objects for each synonym

151126
When invoking the MOSS editor, if the IDE is on CG-USER, crashes since *context*
is undefined.
A way around is to set the editor to the package of the object to be edited.

151107 fixed when using add-values
When adding a number of values > max, MOSS does not add the values due to the use
of %validate-tp

150628
MOSS versions: when a class has been deleted, then it is not possible to recreate
it in a subsequent version, since it is marked with a tombstone, but it still 
exists

140508
NEWS: when adding a news item with an index, if the index already exists, then
recreates it instead of reusing it.

131215
In moss::=make-object-description method, MLN values are not correctly decoded 
when the corresponding path of the page description includes the attribute.

130616
The default option does not work well with subsumption, e.g.
(defconcept "c-news" (:att "status" (:default "unpublished")))
(defconcept "news-item" (:is-a "c-news"))
(defindividual "news-item" (:var _a))
(send _a '=get "status")
NIL
Thus, the default for status, saved at the c-news.0 ideal level is not transferred
to the subsumed instances.
This may be done on purpose, knowing that the defaults are attached to classes 
and do not transfer through.
Another point:
If we attach the default to the subsumed class and we try to collect all instances of c-news: 
   (access '("c-news" ("status" :is "unpublished")))
This does not work. The main reason is that the access is done through the 
$T-C-NEWS-STATUS property and not through the $T-NEWS-ITEM-STATUS property.
The problem should be addressed and solved one way or the other.

130419 fixed
query for selecting object in edit window does not work.

130415 fixed
(send _person '=print-methods)
LOCAL INSTANCE METHODS
======================
=PRINT-SELF
Error: MOSS::$FN=I=0=PRINT-DOC got 2 args, wanted 0 args.


130415 fixed
Prototyping is not working for orphans:
(defobject (:var _p4)("NAME" "George")(:is-a _p2))
Warning: while trying to define instance of class "NULL-CLASS" in package #<The STARTER package>
         context 0, property :IS-A does not exist. We ignore it.

130415 fixed try (send _has-person-brother '=print-self)
(defconcept "Person"
    (:rel "brother" (:to "Person")))
produces
----- $S-BROTHER
 MOSS-INVERSE: (EN IS-BROTHER-OF)
 MOSS-SUCCESSOR: MOSS-UNIVERSAL-CLASS
 MOSS-PROPERTY-NAME: brother
-----
The multilingual name should be printed as IS-BROTHER-OF not (EN IS-BROTHER-OF)


130415 fixed use :class-ref rather than :concept
after executing (defattribute "sex")
executing
   (catch :error
    (defattribute "sex" (:min 1)(:max 1)(:concept "Person")))
should define a particular instance of attribute for the concept Person
One gets the message:
Warning: reusing previously defined generic attribute HAS-SEX, in package
#<The STARTER package> and context 0 ignoring eventual options (class: NIL).


111105 Fixed. However, accessors are only defined for the canonical entry in the
              current language.
when modifying a class with the editor on disk adding an attribute, the accessors
_get-has-xxx  and _set-has-xxx are not created nor saved onto disk. This results 
into an error when trying to reconnect agent to the disk data (missing accessors).

100302 Fixed.
=make-entry refers to inexisting methods:
      '($E-FN.1 $FN.127 $E-FN.2 $FN.128 $E-FN.3 $FN.129 MOSS::$FN.0 MOSS::$FN.1 MOSS::$FN.2
       MOSS::$FN.3 MOSS::$FN.4 MOSS::$FN.5 MOSS::$FN.6 MOSS::$FN.7 MOSS::$FN.8
       MOSS::$FN.96 MOSS::$FN.97 MOSS::$FN.108 MOSS::$FN.109 MOSS::$FN.110 MOSS::$FN.111
       MOSS::$FN.112)
       $FN.127, $FN.128, $FN.129 do not exist they seem to have been created when 
       creating $E-FN.1, $E-FN.2, $E-FN.3...
Spurious methods has been created following a bug in the previous version.

100302 Fixed.
Bug in %make-generic-tp trying to get class when does not exist

101028 
When defining a universal method in a package different from :moss, ignores the 
current package and installs the method in the MOSS package. Should install it in
the current package at the application level, otherwise it might be in conflict 
with other packages.

101028 Not fixed. This is a restriction. See Note
We cannot use global variables in ontologies since they are not saved into the 
database.

101028 Fixed
Skills and agents do not have a =summary method in each application package.
   
101030 
When using defrelation :default option does not work
If $E-PERSON.1 is viewed as a non PDM object and '$e-person.1 is not an instance of
class *any* ... Must review the role of default values in relations.
Currently cannot use defrelation to define generic properties.

101031
When loading OMAS-MOSS and setting package to :moss, *language* is :EN. When loading
SERVER application and setting package to :moss, *language* is :FR.
Furthermore functions in opened files do not compile in their package...

110116
When asking a question in the PA one should replace CR and LF by space before 
processing the input string. Otherwise we tag the last word in the line with the 
CR or LF and it is not recognized.

110727 Not fixed but kept as a feature.
When a property has several synonyms, only the first synonym has an HAS-xxx macro, 
similarly when a property has a multilingual name, e.g.
(defconcept (:en "person" :fr "personne")
    (:att (:fr "nom" :en "name")))
HAS-NAME exists, but HAS-NOM does not.
Does not know if this is a voluntary feature...


IMPROVEMENTS
============
1. When setting up a MOSS environment in a separate thread (e.g. for an agent), one
should create a number of global variables belonging to this environment. Among 
them: *context* *language* *version-graph* *ontology* *system*, etc.
One could do as in OMAS:*omas* for agents, namely put such variables in a global
object.
Here, a good candidate would be *system* (i.e. local $SYS.1).

Pb. All library functions use *context* as a global variable. However, when they 
were compiled *context* was identified as moss::*context*. Therefore it would be 
easier to redefine moss::*context* in the function called when starting the thread.
Q. If we create a new window from this thread, which context will be active in the
callbacks??

2. When reinitializing a thread with an ontology saved on disk, we should reset 
moss::*context* with the value saved on disk. Id. for *version-graph* *language* 
and other global variables.

3. Dialog Echo
When using vocal input, it would be interesting to see what the system has 
recognized in a small echo window on the graphics table. This is analogous to a
dribble file.
The output channel is stored in the conversation object. It contains the object
reference to the window in which the output will be printed.
When using vocal interface, in addition to the visual echo, we should have a 
verbal echo (of what is to be printed, not of the text to be redisplayed in the
                output pane.
                
4. NULL attribute values. Should create a default-make-entry functioo for dreating
entry points for NULL values.
(let ((att-name (car (send *self* '=get "MOSS-PROPERTY-NAME"))))
      (if (mln::%mln? att-name)(setq att-name (mln::make-mln att-name)))
      (if (mln::mln? att-name)
          (setq att-name (car (mln::extract att-name :canonical t))))
      ;; cook up an entry point with the property name
      (list (intern (string+ "NO-"  att-name))))                
                
NOTES
=====
Use of variables in object definitions when objects are saved in a database
---------------------------------------------------------------------------
When one uses the :var option while defining an object, then the resulting variable
is not recorded into the database, and one cannot use it in subsequent sessions.
To save the variable, one should modify the %%make-instance-from-class-id function
saving the variable when it is created. Is it useful? One then runs the risk of
overwriting a previously defined variable, since the name of a variable is chosen
freely.
While creating objects in the same session, variables can be used. However, they
are not recorded and disappear at the end of the session.
Overcoming the use of variable in between sessions can be done by using a MOSS 
query.


