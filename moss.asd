﻿;;;=================================================================================
;;;20/01/29
;;;		
;;;		M O S S - A S D F - (File moss.asdf)
;;;	
;;;=================================================================================

;;;
;;; File defining the MOSS system to be loaded in the ACL environment

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (January 2020)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
2020
 0129 creation from MOSS-version-info 
|#

(defsystem "moss"
  :version "9.0"
  :author "Barthès"
  :license "CeCILL-B"
  :depends-on (:quicklisp)
  :components ((:module "src"
                        :serial t
                        :components
                        ((:file "packages")
                         (:file "globals")
                         (:file "mln")
                         (:file "W-labels")
                         (:file "macros")
                         (:file "utils")
                         ;;-- SOL files
                         #-OMAS (:file "sol-init")
                         #-OMAS (:file "sol2owl")
                         #-OMAS (:file "sol2rules")
                         #-OMAS (:file "sol2html")
                         ;;-- end SOL files
                         (:file "service")
                         (:file "boot")
                         (:file "engine")
                         (:file "def")
                         (:file "persistency")
                         (:file "W-macros")
                         (:file "W-browser")
                         (:file "query")
                         (:file "kernel")
                         (:file "W-editor")
                         (:file "dialog-classes")
                         (:file "dialog-engine")
                         (:file "W-window")
                         #-OMAS (:file "W-init")
                         #-OMAS (:file "W-import")
                         (:file "W-overview")
                         (:file "W-value-editor")
                         (:file "paths")
                         (:file "time")
                         (:file "web")
                         #-OMAS (:file "dialog" :depends-on ("query")) 
                         (:file "export")
                         (:file "online-doc")      ; aditional info for help dialog
                         #-OMAS (:file "main")
                         )))
  :description "MOSS is a knowledge representation language"
  :in-order-to ((test-op (test-op "moss/tests"))))


;; :EOF