﻿# Moss
MOSS is a knowledge representation language using an object-oriented framework. It requires ACL, QUICKLISP for loading subsystems
## Usage
MOSS is used for representing versioned multilingual ontologies. Its output may be a Lisp system or an OWL file associated with JENA or SPARQL files.
The resulting ontology can also be viewed with an HTML browser.
## Installation
Running MOSS requuires installing ACL from Franz Lisp, which requires a licence. Tests can be done with AllegroExpress.