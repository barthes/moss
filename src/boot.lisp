;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=============================================================================
;;;20/03/13
;;;		  M O S S - B O O T  (in boot.lisp)
;;;
;;;	The file contains functions to create classes, properties, and
;;;	methods for bootstrapping MOSS
;;;=============================================================================
#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; Design considerations
;;; =====================
;;; The idea is to start from the functions below and to boostrap MOSS. It uses
;;; specific simplified functions taking advantage of the fact that we are in
;;; the bootstrap phase.
;;;
;;; Multilingual names (MLN)
;;; ------------------------
;;; Multilingual names are represented by a list with the following format:
;;;   (:fr "titre principal; titre de chapitre" :en "chapter title")
;;; Each language has a tag and the following string may contain synonyms.
;;; One of the synonyms is chosen to build the internal ID and the other synonyms 
;;; are used to define entry points.
;;; Multilingual names can be used to define attributes, relations, classes,...
;;;
;;; NOTE: from version 8 up the MLN format has been changed to be an a-list:
;;;   ((:fr "titre principal" "titre de chapitre")(:en "chapter title"))
;;; which is less pleasant but more efficient. All MLN handling functions have
;;; been moved to the MLN package.
;;;
;;; All MOSS entities are represented by MLNs, language is English


#|
2010
 0126 creating the new file
 0329 adding DEF-EXPR property to SYS
 0917 prefixing all MOSS class names and property names by MOSS- to avoid conflicts
      with user names. REFERENCE replaced by MOSS-REF
 1121 introducing a fake send-no-trace to keep the MCL compiler quiet
2012
 1029 adding new properties for a class: sequence, or-part, and-part, xor-part
      allowing to express that a class is a structured combination of other classes
 1217 adding save-new-id to %MAKE-PROP-GET-ACCESSOR and %MAKE-PROP-SET-ACCESSOR
2014
 0307 -> UTF8 encoding
 1130 modifying %process-doc-option to allow (:doc (:en "en" :fr "fr"))
2015
 1126 adding a few comments
2016
 0128 adding class MOSS-VERSION
|#

(in-package "MOSS")

;********** used in service functions to mark the booting phase
(setq *boot-mode* t)

;;: we declare those to keep the compiler happy

(defVar $ent)
(defVar $ept)
(defVar $uni.ctr)
(defVar $sys.ctr)

;;; this is to keep the MCL compiler quiet while defining the HAS-XXX accessors
;(defUn send-no-trace (&rest ll) ll)

;;;========================== service functions ================================

;;;----------------------------------------------------- %MAKE-PROP-GET-ACCESSOR

(defUn %make-prop-get-accessor (prop-id &optional prop-name)
  "get the string-value of prop-id and cook up a GET access function. The name ~
   of the property is used to name the setf access function, in the package of ~
   the prop-id, e.g. HAS-PROPERTY-NAME
Arguments:
   prop-id: id of prop, e.g. $PNAME
   prop-name (opt): symbol of property used to name the access function
Return:
   the name of the function if prop-id is a bona fide prop, nil otherwise"
  ;; at boot time, $EPR may not be defined yet...
  (when (and (boundp prop-id) (or (%type? prop-id '$EPS)
                                  (%type? prop-id '$EPT)
                                  (%type? prop-id '$EPR)))
    ;; if prop-name is not given, recover it from moss object 
    (unless prop-name
      (setq prop-name (intern (car (%%get-value prop-id '$PNAM *context*))
                              (symbol-package prop-id))))
    
    ;; build accessor, use =get since =get can be later used with string ref
    ;; for property to avoid package problems
    (eval 
     `(defUn ,prop-name (&optional obj-id)
        (declare (special *self*))
        ;(format t "~&; %make-prop-get-accessor prop-name: ~S" ',prop-name)
        ;; subseq gets rid of the HAS- part of the property name
        (send-no-trace (or obj-id *self*) 
                       '=get ',(subseq (symbol-name prop-name) 4))
        ))
    
    ;; save definition into a variable for eventual saving to database
    (%make-defexpr
     (intern (concatenate 'string "_GET-" (symbol-name prop-name)))
     `(defUn ,prop-name (&optional obj-id)
        (declare (special *self*))
        (send-no-trace (or obj-id *self*) 
                       '=get ',(subseq (symbol-name prop-name) 4))
        ))
    
    ;; save-new-id checks if we are editing. If so and if the obj-id has not 
    ;; already been saved, we save it and mark its p-list
    (save-new-id (intern (concatenate 'string "_GET-" (symbol-name prop-name))))
    
    ;; return name of accessor
    prop-name))

;;;----------------------------------------------------- %MAKE-PROP-SET-ACCESSOR
;********** maybe should test property type like in %make-prop-get-accessor

(defUn %make-prop-set-accessor (prop-id prop-name)
  "get the string-value of prop-id and cook up a SET access function. The name ~
   of the property is used to name the setf access function, in the package of ~
   the prop-id.
Arguments:
   prop-id: id of prop
   prop-name: symbol of property used to name the access function
Return:
   the name of the function if prop-id is a bona fide prop, nil otherwise"
  (when (and (boundp prop-id) (%type? prop-id '$EPR))
    (let ((prop-string))
      ;; if prop-string is a symbol take its pname
      (setq prop-string 
            (if (stringp prop-name) prop-name (symbol-name prop-name)))
      ;; remove the HAS- part of the property name
      (setq prop-string (subseq prop-string 4))
      
      ;; build accessor 
      (eval 
       `(defsetf  ,prop-name (&optional obj-id) (value) 
          `(progn 
             (send-no-trace (or ,obj-id *self*) '=replace ',',prop-name ,value)
             ,value)))
      
      ;; save definition into a variable for database saving
      (%make-defexpr
       (intern (concatenate 'string "_SET-" (symbol-name prop-name)))
       `(defsetf  ,prop-name (&optional obj-id) (value) 
          `(progn 
             (send-no-trace (or ,obj-id *self*) '=replace ',',prop-name ,value)
             ,value)))
      
      ;; save-new-id checks if we are editing. If so and if the obj-id has not 
      ;; already been saved, we save it and mark its p-list
      (save-new-id (intern (concatenate 'string "_SET-" (symbol-name prop-name))))
      
      ;; return name of accessor
      prop-name)))


;;;--------------------------------------------------------- %PROCESS-DOC-OPTION
;;; the format or doc-list may be:
;;;   ("a single string")
;;;   (:en "the English string" :fr "the French string" ...)
;;; in all cases we store an MLN

(defUn %process-doc-option (id doc-list &key (context *context*) &aux temp)
  "processes a doc option attaching it to the specified object.
Arguments:
   id: identifier of the object
   doc-list: doc value: mln or list containing a simple string, or an MLN
Return:
   nothing interesting."
  (cond
   ((not (listp doc-list))
    (terror "Bad format while defining documentation: ~&~S~& for object ~S"
            doc-list id))
   
   ;; original format: (:doc (((:en "en" :fr "fr"))))
   ((and (null (cdr doc-list)) (mln::mln? (car doc-list))) ; jpb 1406
    (%%set-value-list id doc-list '$DOCT context))
   
   ;; original format: (:doc ((:en "en" :fr "fr"))), update format
   ((and (null (cdr doc-list)) (mln::%mln? (car doc-list))) ; jpb 1406
    (%%set-value-list id (list (mln::make-mln (car doc-list))) '$DOCT context))
   
   ;; single value not a string
   ((and (null (cdr doc-list))(not (stringp (car doc-list))))
    (terror "Bad format wile defining documentation: ~&~S~& for object ~S"
            doc-list id))
   
   ;; single value a string
   ((null (cdr doc-list))
    (%%set-value-list
     id (list (mln::make-mln (car doc-list) :language *language*))
              '$DOCT context))
    
   ;; if we can cook up an MLN, do it
   ((setq temp (mln::make-mln doc-list))
    (%%set-value-list id (list temp) '$DOCT context))
   
   ;; otherwise give up
   (t 
    (terror "Bad format while defining documentation: ~&~S~& for object ~S"
            doc-list id))))

#|
(definstance "person" ("name" "albert"))
$E-PERSON.1

(moss::%process-doc-option '$E-PERSON.1 '("small test"))
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1))
 ($T-PERSON-NAME (0 "albert")) (MOSS::$DOCT (0 (:EN "small test"))))

(moss::%process-doc-option '$E-PERSON.1 '(:en "small test"))
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1))
 ($T-PERSON-NAME (0 "albert")) (MOSS::$DOCT (0 (:EN "small test"))))

(moss::%process-doc-option '$E-PERSON.1 '(:en "small test 2"))
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1))
 ($T-PERSON-NAME (0 "albert")) (MOSS::$DOCT (0 (:EN "small test 2"))))

(moss::%process-doc-option '$E-PERSON.1 '((:en "small test 3")))
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1))
 ($T-PERSON-NAME (0 "albert")) (MOSS::$DOCT (0 (:EN "small test 3"))))

(catch :error (moss::%process-doc-option '$E-PERSON.1 "albert"))
"Bad format while defining documentation: 
\"albert\"
 for object $E-PERSON.1"

(catch :error (moss::%process-doc-option '$E-PERSON.1 '(22)))
"Bad format wile defining documentation: 
(22)
for object $E-PERSON.1"

(catch :error (moss::%process-doc-option '$E-PERSON.1 '(:en "..." :fr)))
"Bad format wile defining documentation: 
(:EN \"...\" :FR)
for object $E-PERSON.1"

(catch :error (moss::%process-doc-option '$E-PERSON.1 '(:en "..." :fr "...")))
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1))
 ($T-PERSON-NAME (0 "albert")) (MOSS::$DOCT (0 (:EN "..." :FR "..."))))
|#

;;;=============================================================================
;;;
;;;                       ATTRIBUTE (Terminal Property / tp) 
;;;
;;;=============================================================================

;;; Next things to create in the bootstrap are properties, since classes will
;;; call for property definitions

;;; G: System property: a property of the MOSS kernel, e.g. $PNAM, $CTRS
;;; All system properties are generic properties.

;;;------------------------------------------------------------- %MAKE-SYSTEM-TP
;;; (%make-system-tp tp-ref &rest option-list) - creates a terminal property 
;;;     tp-ref qualifies the attribute
;;; Option list:
;;;   (:id <id>)              prop id  (required)
;;;   (:class-id <class-id>)  class-id of the class to which the property 
;;;                           is attached
;;;   (:entry)
;;;   (:min <number>)	      minimal cardinality
;;;   (:max <number>)	      maximal cardinality
;;;   (:unique)	              minimal and maximal cardinality are each 1


(defUn %make-system-tp (tp-ref &rest option-list)
  "creates a generic terminal property. Options are taken into account only ~
   for system properties.
Arguments:
   tp-ref: symbol, string or mln of the attribute, e.g. (:en \"entity name\")
   Option-list (rest): list of possible options
Return:
   the id of the generic property"
  ;; thereafter tp-name is the name of the property, e.g. HAS-ENTITY-NAME
  ;; id is the id of the property, e.g. $T-ENTITY-NAME
  ;; class-id option used when function is called from %make-system-concept
  
  (let* ((class-id (car (getv :class-id option-list)))
         tp-name tp-mln id)
    
    ;; cook up an attribute name, e.g. HAS-XXX
    (setq tp-name (%make-name-for-attribute tp-ref))
    ;; check then if we have already a generic terminal property with this name
    (setq id (%%get-id tp-name :tp))
    
    ;; if property exists, return it (e.g., to use in a class)
    (when id
      ;; when called from %make-system-concept, link attribute to class
      (if class-id (%link class-id '$PT id))
      (return-from %make-system-tp id))
    
    ;;===== here property does not exist, we must create it
    ;; get internal name
    (setq id (car (getv :id option-list)))
    (unless id 
      (error "missing :id option when defining system attribute ~S" tp-name))
    
    ;; build an MLN from property name (*language* should be :EN)
    (setq tp-mln (mln::make-mln tp-ref :language *language*))
    ;(%make-string-from-ref tp-ref :all t)))
    
    ;; create property object
    (set id `(($TYPE (,*context* $EPT))
              ($ID (,*context* ,id))
              ($PNAM (,*context* ,tp-mln))))
    
    ;; add new property-id to system-list
    (%link *MOSS-SYSTEM* '$ETLS id) ; add to system list of tp
    
    ;; build entry-point if it does not exist already, or else add entry
    ;; we must build an entry point for each synonym, exporting it everywhere
    (%make-ep-from-mln tp-mln '$PNAM id :type '$EPT :export t)
    
    ;; Now we consider inverse property, exporting it to applications
    (%make-inverse-property id tp-mln :export t)
    
    ;;=== process now OPTION LIST 
    
    ;; special case for the default option:
    ;; default is normally attached to the ideal of the corresponding class
    ;; if the property is defined without reference to a class, then the
    ;; default is attached to the property itself. However, if the property is used
    ;; later to support a local property with a different default, the default
    ;; attached to ideal will shadow the default attached to the property. 
    ;; NB: we only do that for system properties
    (cond
     ((and (assoc :default option-list) class-id)
      ;; system property with a specified class, attach default to ideal
      (%%set-value-list (%make-id-for-ideal class-id)
                        (getv :default option-list) id *context*))
     
     ;; sytem property, no class
     ((and (assoc :default option-list)(null class-id))
      (%%set-value-list id (getv :default option-list) '$DEFT *context*)))
    
    ;; build now all options. Note: no fancy options
    (while
     option-list
     (case (caar option-list)
       ;; entry option allows us to associate entry-points with attributes
       ((:entry :index)
        (if (cdar option-list)
            ;; if function is specified, build it
            (apply #'%make-system-ownmethod '=make-entry id (cdar option-list))
          ;; when nothing particular is specified, use a default function
          (%make-system-ownmethod :default-make-entry id nil))
        )
       (:min
        (%%set-value id (cadar option-list) '$MINT *context*)
        )
       (:max
        (%%set-value id (cadar option-list) '$MAXT *context*)
        )
       (:unique	;; has both min and max to 1
        (%%set-value id 1 '$MINT *context*)
        (%%set-value id 1 '$MAXT *context*)
        )
       (:class-id
        (%link class-id '$pt id)
        )
       ;; Record documentation if specified
       (:doc	
        (%process-doc-option id (cdar option-list))
        )
       ) ; end case
     ;; all other options are ignored
     (pop option-list)
     )
    
    ;(format t "~%== id: ~S - flag: ~S - tp-name: ~S" id *moss-engine-loaded* tp-name)
    ;; build external-name for temporary use only (current session)
    (set (%make-name-for-variable tp-name) id)
    
    ;; add now a macro to allow to use property name as a function in methods
    (%make-prop-get-accessor id tp-name)
    (%make-prop-set-accessor id tp-name)
    
    ;; return tp-id
    id))

#|
(%make-system-tp '(:en "chapter title")'(:id $CHT)
                 '(:default :none) '(:doc (:fr "texte" :en "text")) '(:unique))
$CHT

(($TYPE (0 $EPT)) ($ID (0 $CHT)) ($PNAM (0 ((:EN "chapter title")))) 
 ($ETLS.OF (0 $SYS.1)) ($INV (0 $CHT.OF)) ($DEFT (0 :NONE))
 ($DOCT (0 ((:FR "texte") (:EN "text")))) 
 ($MINT (0 1))
 ($MAXT (0 1)))

HAS-CHAPTER-TITLE
(($TYPE (0 $EP)) ($ID (0 HAS-CHAPTER-TITLE)) ($PNAM.OF (0 $CHT)) 
 ($EPLS.OF (0 $SYS.1)))

(%make-system-tp '(:en "chapter title")
                 '(:default :none) '(:doc (:fr "texte" :en "text")) '(:max 2))
$CHT
;; should send a warning message...:
; Warning: reusing previously defined generic attribute CHAPTER-TITLE, ignoring eventual options.
; While executing: %MAKE-GENERIC-TP

(%make-system-tp '(:en "Second-name")'(:id $SNM) '(:entry))
$SNM

(($TYPE (0 $EPT)) ($ID (0 $SNM)) ($PNAM (0 ((:EN "Second-name")))) 
 ($ETLS.OF (0 $SYS.1)) ($INV (0 $SNM.OF)) ($OMS (0 $FN.132)))

$FN.132
(($TYPE (0 $FN)) ($ID (0 $FN.132)) ($MNAM (0 =MAKE-ENTRY)) ($FNLS.OF (0 $SYS.1))
 ($OMS.OF (0 $SNM)) ($FNAM (0 DEFAULT-MAKE-ENTRY)))

DEFAULT-MAKE-ENTRY
(LAMBDA (VALUE-LIST)
  "default make-entry fonction"
  (CATCH :RETURN (%MAKE-ENTRY-SYMBOLS VALUE-LIST)))
;;********** how about *package* ?
|#
;;;=============================================================================
;;;
;;;                RELATIONS (Structural Properties / sp)
;;;
;;;=============================================================================

;;; (%make-system-sp sp-ref key class-name suc-name &rest option-list) - creates 
;;; a relation that is a link between class and suc. If classes do not exist it 
;;; is an error
;;; Syntax of options is
;;;     (:doc <documentation>)  documentation (can be multilingual)

;;;------------------------------------------------------------ %MAKE-GENERIC-SP
;;; from class and to class must have been defined before we can use the function
;********** doc cannot be attached to the property...

(defUn %make-system-sp (sp-ref class-name suc-name &rest option-list)
  "creates a system structural property.
   Options are ignored. No check on arguments.
Arguments:
   sp-ref: name of property: symbol, string or mln
   class-name: from class
   suc-name: to class
   option-list (opt): list of options 
Returns:
   the id of the generic property, e.g. $SUC"
  (let* ((class-id (%%get-id class-name :class))
         (suc-id (%%get-id suc-name :class))
         sp-name sp-mln id)
    
    (unless (and class-id suc-id)
      (error "bad class or successor names (~S, ~S) when defining relation ~S"
             class-name suc-name sp-ref))
    
    (setq sp-name (%make-name-for-relation sp-ref))
    ;; check first if we have a generic structural property with this name
    ;; should use (%%get-id sp-name :sp :class-ref class-id)??
    (setq id (%%get-id sp-name :sp))
    ;; we are happy if property exists
    (when id
      (return-from %make-system-sp id))
    
    ;;===== here, the property does not exist, we must create it 
    ;; get internal name
    (setq id (car (getv :id option-list)))
    (unless id 
      (error "missing :id option when defining system relation ~S" sp-name))
    
    ;; build a multilingual name (even if sp-ref is already one)
    (setq sp-mln (mln::make-mln sp-ref :language *language*)) ; jpb 1406
    
    ;; create new object
    (set id  `(($TYPE (,*context* $EPS))
               ($ID (,*context* ,id))
               ($PNAM (,*context* ,sp-mln))
               ))  ; inverse prop is missing yet
    
    ;; link class and successor
    (%link class-id '$PS id)
    ;; and into suc
    (%link id '$SUC suc-id)
    ;; add new property-id to system-list
    (%link *moss-system* '$ESLS id)
    
    ;; build entry-points one for each synonym
    (%make-ep-from-mln sp-mln '$PNAM id :type '$EPS :export t)
    
    ;; ...
    ;; Now we consider inverse property
    (%make-inverse-property id sp-mln :export t)
    
    ;; build external-name for temporary use only (current session)
    (set (%make-name-for-variable sp-name) id)
    
    ;; create access function unless engine is not loaded yet 
    
    ;; add now a macro to allow to use property name as a function in methods
    (%make-prop-get-accessor id sp-name)
    (%make-prop-set-accessor id sp-name)
    
    ;; return sp-id
    id))

;;;=============================================================================
;;;
;;;                             CONCEPT (Class) 
;;;
;;;=============================================================================

;;; Next thing to create in the bootstrap are classes

;;; (%make-system-concept class-ref &rest option-list) - creates a system concept
;;; option-list (opt): a list of option pairs:
;;;    (:id <id>)                  id of the class object (required)
;;;    (:is-a <class-id>*)	   for defining inheritance
;;;    (:att <att-description>)    specifies attribute (formerly terminal prop)
;;;    (:rel <rel-description>)    specifies relationship (formerly structural prop)
;;;    (:doc <documentation>)      text expressed as a multilingual list
;;;    (:no-counter)               reserved for creating classes $FN and $CTR 
;;; unused   (:export <export>)          t if class symbol is exported
;;;     system classes are always exported
;;; unused   (:do-not-save)        if there, does not save into *moss-system*
;;; unused   (:if-exists :return)  if exists, skip definition (so that we can reload)"


;;-------------------------------------------------------- %MAKE-SYSTEM-CONCEPT

(defUn %make-system-concept (class-ref &rest option-list)
  "Classes are created with associated counter using a predefined counter ~
   $ENT.CTR - I use the same mechanism as for ~
   methods, e.g. the counter is addressed explicitly and modified directly ~
   However at first classes $CTR (counter) and $FN (method) cannot be created ~
   with a counter since they already exist. We use the special option ~
   (:no-counter) in the option list.
Arguments:
   class-ref: symbol, string or multilingual name
   option-list (opt): a list of option pairs"
  
  (let (name ctr-id id class-mln)
    ;; create class name (package is :MOSS) 
    ;(trformat "=== *package* : ~S" *package*)
    (setq name (%make-name-for-class class-ref))
    ;(vformat "=== creating class with name: ~S in package ~S" name *package*)
    
    ;; then cook up the class id. We can leave the :id option on the option-list
    ;; it will be ignored during the processing of options
    (setq id (car (getv :id option-list)))
    
    (when (boundp id)
      ;; otherwise, throw to :error
      (error  "while trying to define class:~%; ~S~%; in package ~S, ~
               ~%; internal id (~A) exists with value:~%  ~S"
              name *package* id (symbol-value id)))
    ;; otherwise proceed
    
    ;; build mln from class-ref input (must specify language because of package)
    (setq class-mln (mln::make-mln class-ref :language *language*))
    ;(setq class-mln (list *language* (%make-string-from-ref class-ref :all t)))
    
    ;; OK we create class
    (set id `(($TYPE (,*context* $ENT))
              ($ID (,*context* ,id))
              ($ENAM (,*context* ,class-mln))
              ($RDX (,*context* ,id))))
    
    ;; add it to MOSS
    (%link *moss-system* '$ENLS id)
    ;; don't forget entry point
    (%make-ep-from-mln class-mln '$ENAM id :type '$ENT :export t)
    
    ;; create now associated counter unless option (:no-counter) is there
    ;; design decision: counters where created distinct from classes to avoid
    ;; storing the whole class to disc when counter is increased.
    (unless (assoc :no-counter option-list)
      ;; make new symbol
      (setq ctr-id (%make-id-for-counter id))
      ;; build counter
      (set ctr-id `(($TYPE (,*context* $CTR))
                    ($VALT (,*context* 1))))
      ;; link to class
      (%link id '$CTRS ctr-id)
      ;; we record instance at the system level
      ;(%link *moss-system* '$SVL ctr-id *context*)
      )
    
    ;; we create the ideal associated with the class
    (set (%make-id-for-ideal id) 
         `(($TYPE (,*context* ,id))))
    
    ;; process options
    (while option-list
      (case (caar option-list)
        ;; skip some options
        ((:id :no-counter)) 
        
        (:is-a	;; defines super classes (a list of ids)
         (%link-all id '$IS-A 
                    (remove nil (mapcar #'(lambda(xx)(%%get-id xx :class))
                                        (cdar option-list)))))
        
        ((:tp :att)  ; call the slave adding context and package info
         ; will eventually be shadowed
         (apply #'%make-system-tp 
                (cadar option-list)
                (list :class-id id)
                (cddar option-list))
         )
        
        ((:sp :rel)  ; call the slave (sp-ref class-name suc-name &rest option-list)
         (apply #'%make-system-sp 
                (cadar option-list) 
                name 
                (cddar option-list)))
        
        ;; Record documentation if specified, :doc takes a single value JPB060228
        (:doc	
         (%process-doc-option id (cdar option-list))
         )
        
        (otherwise
         (error "unknown option ~S while defining class ~S." 
                (car option-list) name))	
        )  ; end case
      (pop option-list))
    
    ;; build external-name for temporary use only (current session)
    ;********** do we really need that for system concepts ?
    (mapc #'(lambda (xx) (set (%make-name-for-variable xx) id))
      (mln::extract-all-synonyms class-mln))
    
    ;; return entity-id
    id))

;;;=============================================================================
;;;
;;;                            INSTANCE METHOD 
;;;
;;;=============================================================================

;;;--------------------------------------------------------- %MAKE-SYSTEM-METHOD

(defUn %make-system-method (name selector arg-list body &rest option-list)
  "Used to define methods attached to an existing class. it is an error if ~
   the class is not already defined. When the method does not already exist, ~
   then a method object is first created; then the method is compiled and ~
   attached to an internal name built from the class-id, the current version, ~
   and the method-name. If the method already exists, then the code and arguments ~
   are updated, and the method is recompiled.
Arguments:
   name: name of the method
   selector: class ref (symbol, string or mln)
   arglist: argument of the method
   body: code for the method
   option-list (rest): ignored
Return:
   id of the method."
  (declare (special *context* *package*) (ignore option-list))
  (let (fn-id fn-id-list class-id method-function-name)
    
    ;; get class-id from selector (class ref)
    (setq class-id (%%get-id selector :class))
    ;; if we have no value, complain
    (unless class-id
      (terror "while defining method ~A, selector is not a valid class reference~%:~
               ~A ~%; in context ~S and package ~S."
              name selector *context* *package*))
    
    ;; otherwise check if method already exists for the class
    (if (and (%pdm? name)       ; method-name is enfry point?
             (setq fn-id-list   ; usurp value for a short while
                   (%get-value name (%make-id-for-inverse-property '$MNAM)))
             ;; look for presence of class-id in the list
             (dolist (item fn-id-list)
               (if (member class-id 
                           (%get-value item (%make-id-for-inverse-property '$IMS)))
                 (return (setq fn-id item))))
             )
      (progn
        ;; here method already exists we simply patch it
        ;;********** we should remove it from the p-list of all classes and orphans
        (verbose-warn "redefining method ~A for class ~A" name selector)
        ;; upgrade documentation
        (if (stringp (car body))
          (%%set-value fn-id (car body) '$DOCT *context*))
        )
      (progn
        ;; otherwise method does not exist we must create it
        (setq fn-id (%make-id-for-instance-method (%get-and-increment-counter '$FN)))
        ;; build new entity (code and args are available in function object)
        ;(print `(+++ ,*package* ,fn-id))
        (set fn-id 
             `(($TYPE ,(list *context* '$FN))
               ($ID (,*context* ,fn-id))
               ($MNAM (,*context* ,name))
               ,@(if (stringp (car body))
                   `(($DOCT (,*context* ,(car body)))))
               ))
        ;; build entry-point if it does not exist already and export it
        (%make-ep name '$MNAM fn-id :export t)
        ;; add new method-name to system-list
        (%link *moss-system* '$FNLS fn-id)
        ;; link to class
        (%link class-id '$IMS fn-id)))
    
    ;; next is common code to cases where method is created or simply patched
    ;; define or redefine a global symbol for holding method code
    (setq method-function-name 
          (%make-name-for-instance-method-function name class-id))
    ;; record internal method name
    (%%set-value fn-id method-function-name '$FNAM *context*)
    ;; ...build and attach code to method-name and compile it
    (%build-method method-function-name arg-list body)
    
    ;; put reference onto p-list of class (***** only if methods cached???)
    (%putm class-id method-function-name name *context* :instance)
    
    ;; return method-name
    fn-id
    ))

;;;=============================================================================
;;;
;;;                            OWN METHOD 
;;;
;;;=============================================================================

;;;------------------------------------------------------ %MAKE-SYSTEM-OWNMETHOD

;;; Defines an own-method, i.e. a method specific to an object and directly 
;;; attached to the object. It is an error if the object is not already defined. 
;;; When selector is a symbol then it is considered an object-id.
;;;   Otherwise syntax is:
;;;	(entry tp-name class {:select function}{:class-ref <class-ref>} )
;;; where function is used when more than one object is reached via the 
;;; access path entry-tp-name-class, and class-ref to select properties.
;;;   When the method does not already exist, ~
;;; then a method object is first created; then the method is compiled and 
;;; attached to an internal name build from the class-id, the current version, 
;;; and the method-name. If the method already exists, then the code and arguments 
;;; are updated, and the method is recompiled.

(defUn %make-system-ownmethod (ref obj-id arg-list &rest body)
  "Defines a system own-method.
Arguments:
   ref: method-name or :default-make-entry
   obj-id: id of the object to attach method to
   arg-list: typical lisp arg-list
   body (rest): list: body of the function
Return:
   id of the method."
  (declare (special *context*))
  
  (let ((name (if (eql ref :default-make-entry) '=make-entry ref))
        fn-id method-function-name)
    ;; when selector is a symbol, it must be an object identifier or the
    ;; name of a variable whose value is an object identifier
    (unless (%pdm? obj-id)
      (error "non existing object ~A, while attempting ~
              to create method ~A" obj-id name))
    ;; name is exported when creating the entry point
   
    ;; make new symbol
    (setq fn-id (%make-id '$FN :value (%get-and-increment-counter '$FN)))
    
    ;; build new entity (code and args are available in function object)
    ;(print `(+++ ,*package* ,fn-id))
    ;; build new entity
    (set fn-id 
         `(($TYPE (,*context* $FN))
           ($ID (,*context* ,fn-id))
           ($MNAM (,*context* ,name))
           ,@(if (stringp (car body))
               `(($DOCT (,*context* ,(car body)))))
           ))		  
    ;; build entry-point if it does not exist already
    (%make-ep name '$MNAM fn-id :export t)
    ;; add new method-name to system-list
    (%link *moss-system* '$FNLS fn-id)
    ;; link to class
    (%link obj-id '$OMS fn-id)
    
    ;; define a global symbol for holding method code
    (if (eql ref :default-make-entry)    
      (progn
        (setq method-function-name 'default-make-entry)
        (%build-method method-function-name '(value-list) 
                       '("default make-entry fonction"
                         (%make-entry-symbols value-list)))
        )
      (progn
        (setq method-function-name 
              (%make-name-for-own-method-function name obj-id))
        ;; ...build and attach code to method-name and compile it
        (%build-method method-function-name arg-list body)
        ))
    
    ;; record internal method name
    (%%set-value fn-id method-function-name '$FNAM *context*)
    
    ;; put reference onto p-list of object
    (%putm obj-id method-function-name name *context* :own)
    
    ;; return method-name
    fn-id))

#|
(defsystp "test1" (:id $test1)(:entry))
$TEST1
(($TYPE (0 $EPT)) ($ID (0 $TEST1)) ($PNAM (0 ((:EN "test1")))) 
 ($ETLS.OF (0 $SYS.1))
 ($INV (0 $TEST1.OF)) ($OMS (0 $FN.133)))
$FN.133
(($TYPE (0 $FN)) ($ID (0 $FN.133)) ($MNAM (0 =MAKE-ENTRY)) ($FNLS.OF (0 $SYS.1))
 ($OMS.OF (0 $TEST1)) ($FNAM (0 DEFAULT-MAKE-ENTRY)))
DEFAULT-MAKE-ENTRY
(LAMBDA (VALUE-LIST)
  "default make-entry fonction"
  (CATCH :RETURN (%MAKE-ENTRY-SYMBOLS VALUE-LIST)))

(defsystp "test2" (:id $test2)
  (:entry (a b) "test method" (print "youpee") (list a b)))
$TEST2
(($TYPE (0 $EPT)) ($ID (0 $TEST2)) ($PNAM (0 ((:EN "test2")))) ($ETLS.OF (0 $SYS.1))
 ($INV (0 $TEST2.OF)) ($OMS (0 $FN.134)))
$FN.134
(($TYPE (0 $FN)) ($ID (0 $FN.134)) ($MNAM (0 =MAKE-ENTRY)) ($DOCT (0 "test method"))
 ($FNLS.OF (0 $SYS.1)) ($OMS.OF (0 $TEST2)) ($FNAM (0 $TEST2=S=0=MAKE-ENTRY)))
$TEST2=S=0=MAKE-ENTRY
(LAMBDA (A B) "test method" (CATCH :RETURN (PRINT "youpee") (LIST A B)))
|#
;;;=============================================================================
;;;
;;;                            UNIVERSAL METHOD 
;;;
;;;=============================================================================

;;;------------------------------------------------------ %MAKE-SYSTEM-UNIVERSAL

(defUn %make-system-universal (name arg-list body) 
  "restricted to MOSS boot. Defines a system universal method.
Arguments:
   name: method-name or :default-make-entry
   arg-list: typical lisp arg-list
   body (rest): list: body of the function
Return:
   id of the method."
  (declare (special *context*))
  (let (fn-id method-id doc)
    (if (setq fn-id (car (%extract-from-id  name '$UNAM '$UNI)))
      (progn
        ;; when universal method already exists...
        (verbose-warn "~&redefining universal method ~A: " name)
        
        ;;; must update doc, otherwise the rest remains unchanged
        (if (stringp (car body))
          (%%set-value fn-id (car body) '$DOCT *context*))
        ;; get the method-id
        (setq method-id (car (%%get-value fn-id '$FNAM *context*)))
        )
      ;; otherwise create it
      (progn
        ;; make new symbol
        (setq fn-id (%make-id '$UNI :value (%get-and-increment-counter '$UNI)))
        
        ;; look for documentation string
        (setq doc (if (stringp (car body)) (car body)))
        ;; build new entity
        (set fn-id (copy-list
                    `(($TYPE (,*context* $UNI))
                      ($ID (,*context* ,fn-id))
                      ($UNAM (,*context* ,name))
                      ,@(if doc `(($DOCT (,*context* ,doc))))
                      ;($CODT (,context ,(if (stringp (car body)) (cdr body) body)))
                      )))
        
        ;; add new method-name to system-list
        (%link *MOSS-SYSTEM* '$FNLS fn-id)
        (%make-ep name '$UNAM fn-id :export t)
        
        ;; cook up global name
        (setq method-id (%make-name-for-universal-method-function name))
        ;; record internal method name
        (%%set-value fn-id method-id '$FNAM *context*)
        )
      )
    ;; build or rebuild the function
    (set method-id `(lambda ,arg-list ,@body))
    ;; and compile method
    (if (and (member :microsoft *features*)
             (not (member :compiler *features*)))
      ;; when delivering ACL execs, compiler is not part of the environment
      (setq method-id `(lambda ,arg-list ,@body))
      (compile method-id `(lambda ,arg-list ,@body)))
    
    ;; put reference onto p-list of name!
    (%putm name method-id name *context* :universal)
    
    ;; return method-name
    fn-id))

;;;=============================================================================
;;;
;;;                      EXPRESSIONS AND DEFINITIONS 
;;;
;;;=============================================================================

;;; We save special definitions, e.g. defsetf definitions, so that when we use
;;; the database, we reload and execute the definition expression to recreate
;;; the corresponding functions

;********** do we need that for system data, since we recreate them each time
; we reload the system?

;;;--------------------------------------------------------------- %MAKE-DEFEXPR

(defUn %make-defexpr (name def-expr)
  (setf (symbol-value name) def-expr)
  (%%add-value (symbol-value (intern "*MOSS-SYSTEM*")) '$DFXL name *context*))

;;;=============================================================================
;;;
;;;                              FUNCTIONS 
;;;
;;;=============================================================================

;;; deffunction defined in macros.lisp

;;;=============================================================================
;;;
;;;                            MOSS System 
;;;
;;;=============================================================================

;;;=============================================================================
;;;                              macros
;;;=============================================================================

(defMacro defsysclass (class-mln &rest option-list)
  `(apply #'%make-system-concept ',class-mln ',option-list))

(defMacro defsyssp (sp-mln class-id suc-id &rest option-list)
  `(apply #'%make-system-sp ',sp-mln ',class-id ',suc-id ',option-list))

(defMacro defsystp (tp-mln &rest option-list)
  `(apply #'%make-system-tp ',tp-mln ',option-list))

(defMacro defsysmethod (method-name class-ref arg-list &rest body)
  `(funcall #'%make-system-method ',method-name ',class-ref ',arg-list ',body))

(defMacro defsysownmethod (name selector arg-list &rest body)
  `(apply #'%make-system-ownmethod ',name ',selector ',arg-list ',body))

(defMacro defsysuniversal (method-name arg-list comment &rest body)
  `(funcall #'%make-system-universal ',method-name ',arg-list ',(cons comment body)))

;;;=============================================================================
;;;                             MOSS CORE
;;;=============================================================================

;;; defsysXXX checks first if a system exists, hence we must have a predefined
;;; system MOSS
;;; $TYPE must be there otherwise MOSS does not work
;;; all handcrafted objects receive a 0 transaction number
;;; sp successors must be unique

(eval-when (:compile-toplevel :load-toplevel :execute) (export 'MOSS)); JPB040913

(defVar moss '(($TYPE (0 $EP))($ID (0 MOSS))($SNAM.OF (0 $SYS.1))($XNB (0 0))
               ($EPLS.OF (0 $SYS.1))) ; store into the list of entry points
  "Entry point for MOSS (system name)")

(defVar $sys.1
  ;; $MOSSSYS must exist otherwise we could not create it
  '(($TYPE (0 $SYS))
    ($ID (0 $SYS.1))
    ($SNAM (0 ((:en "MOSS") (:fr "MOSS"))))
    ($PRFX (0 "moss"))
    ($EPLS (0 MOSS)) ; list of all entry-points
    ($CRET (0 "J-P.A.BARTHES"))
    ($DTCT (0 "January 10"))
    ($VERT (0 8.0))
    ($XNB  (0 0))
    ($FNAM (0 "MOSS8-KERNEL"))
    )
  "Object representing the MOSS system")

;;; Now $MOSSSYS is defined as a PDM object, we declare it as the default system

(setq *moss-system* '$SYS.1)

;;; we need to define a counter for methods ($FN) and for counters ($CTR),
;;; Both counters are ready to be linked to $FN and $CTR as soon as they are
;;; created 
;;; <the counters are not really necessary for creating service functions>

(defVar $fn.ctr '(($TYPE (0 $CTR))($ID (0 $FN.CTR))($VALT (0 0))
                  ($CTRS.OF (0 $FN))($XNB (0 0)))
  "Internal counter for numbering methods")

(defVar $ctr.ctr '(($TYPE (0 $CTR))($ID (0 $CTR.CTR))($VALT (0 0))
                   ($CTRS.OF (0 $CTR))($XNB (0 0)))
  "Internal counter for creating stand-alone counters")

;;; we need also a counter to create classless objects

(defVar $cls.ctr '(($TYPE (0 $CTR))($ID (0 $CLS.CTR))($VALT (0 0))
                   ($CTRS.OF (0 $SYS))($XNB (0 0)))
  "internal counter for creating classless objects")

;; make a temporary $ENT object to keep the compiler quiet
(setq $ENT `(($TYPE (0 $ENT))))

;; same with $EPT
(setq $EPT `(($TYPE (0 $ENT))))

;;;============================= MOSS System (continued) =======================
;;; Define now static MOSS structures
;;; All documentation concerning MOSS objects is contained in the online document-
;;; ation file.

;;; define $REF and $ID that are special attributes
;;; %REF (multiclass belonging) is made obsolete by the existence of irtual classes


(defsystp "MOSS-REF" (:id $REF))

(defsystp "MOSS-IDENTIFIER" (:id $ID))

;;; We add terminal property $TYPE only to create entry-point HAS-TYPE

(defsystp "MOSS-TYPE" (:id $TYPE))

(defsystp "MOSS-DEFAULT" (:id $DEFT))

;;; First let's create all meta-models - Note that we cannot use :sp option
;;; before successor classes exist

;;; we must create first classes for which a counter exists
;;; Interestingly the class counter does not need a counter for meta-models
;;; But could be necessary for creating special instances of counters

(defsysclass ((:en "MOSS-COUNTER"))
  (:id $CTR)
  (:tp MOSS-VALUE (:id $VALT) (:unique))
  (:tp MOSS-COUNTER-NAME (:id $CNAM) (:unique))
  (:tp MOSS-TRANSACTION-NUMBER (:id $XNB) (:unique))
  (:tp MOSS-TOMBSTONE (:id $TMBT) (:max 1))
  (:no-counter)
  )

;;; link counter
(%link '$CTR '$CTRS '$CTR.CTR)

(defsysclass ((:en "MOSS-METHOD"))
  (:id $FN)
  ;(:tp METHOD-NAME (:id $MNAM) (:unique)(:entry))
  (:tp MOSS-DOCUMENTATION (:id $DOCT))
  (:tp MOSS-ARGUMENTS (:id $ARG))
  (:tp MOSS-REST (:id $REST))
  (:tp MOSS-CODE (:id $CODT))
  (:tp MOSS-FUNCTION-NAME (:id $FNAM))
  (:tp MOSS-TRANSACTION-NUMBER (:id $XNB))
  (:tp MOSS-TOMBSTONE (:id $TMBT))
  (:no-counter)
  )

;;; link counter
(%link '$FN '$CTRS '$FN.CTR)
;;; we cannot use the :entry option until $FN is given a counter.
(defsystp "MOSS-METHOD-NAME" (:id $MNAM)(:entry)(:class-id $FN))

(defsysclass ((:en "MOSS-UNIVERSAL-METHOD")) (:id $UNI)
  ;	(:is-a METHOD)
  (:tp MOSS-UNIVERSAL-METHOD-NAME (:id $UNAM) (:unique)(:entry))
  (:tp MOSS-DOCUMENTATION (:id $DOCT))
  (:tp MOSS-ARGUMENTS (:id $ARG))
  (:tp MOSS-REST (:id $REST))
  (:tp MOSS-CODE (:id $CODT))
  (:tp MOSS-FUNCTION-NAME (:id $FNAM))
  (:tp MOSS-TRANSACTION-NUMBER (:id $XNB))
  (:tp MOSS-TOMBSTONE (:id $TMBT))
  (:no-counter)
  )

;;; Counter for specifying universal methods
;;; Remember that we do not record counter in the system frame...

(setq $UNI.CTR ; Counter for universal methods
      '(($TYPE (0 $CTR))($ID (0 $UNI.CTR))($VALT (0 0)))
      )
;;; link counter
(%link '$UNI '$CTRS '$UNI.CTR)

(defsysclass ((:en "MOSS-SYSTEM")) (:id $SYS)
  (:tp MOSS-SYSTEM-NAME (:id $SNAM) (:unique)(:entry))
  (:tp MOSS-PREFIX (:id $PRFX) (:unique))
  (:tp MOSS-PACKAGE (:id $PKG) (:unique)) ; ontology package
  ;(:tp ENTRY-POINT-LIST (:id $EPLS))
  (:tp MOSS-VARIABLE-LIST (:id $SVL))
  (:tp MOSS-FUNCTION-LIST (:id $SFL))
  (:tp MOSS-MACRO-LIST (:id $SML))
  (:tp MOSS-DEF-EXPR (:id $DFXL))  ; to save defsetf definitions
  (:tp MOSS-CREATOR  (:id $CRET))
  (:tp MOSS-CREATION-DATE (:id $DTCT) (:unique))
  (:tp MOSS-VERSION (:id $VERT) (:unique))
  (:tp MOSS-DOCUMENTATION (:id $DOCT))
  (:tp MOSS-TRANSACTION-NUMBER (:id $XNB))
  (:tp MOSS-TOMBSTONE (:id $TMBT))
  (:no-counter) ; counter starts at 1
  )

(setq $SYS.CTR ; Counter for systems MOSS system is $SYS.1
      '(($TYPE (0 $CTR))($ID (0 $SYS.CTR))($VALT (0 1)))
      )
;;; sometimes during the execution of the above code $EPS gets set to nil
;;; after getting an angry message from MGF saying that no base is opened...
;;; this happens while processing the option (:tp entry-point-list $EPLS)
;;; ... We must reset $EPS to unbound done just before defining it

;;; Now redefine $ENT properly
(makunbound '$ent)
;-
(defsysclass ((:en "MOSS-CONCEPT" "MOSS-ENTITY" "MOSS-CLASS")) (:id $ENT)
  (:tp ((:en "MOSS-CONCEPT-NAME" "MOSS-ENTITY-NAME")) (:id $ENAM)(:entry)) ; using multiple class names...
  ;(:tp LABEL (:id $LBLT))  ; used as follows: (0 (:en "Entity")(:fr "Entit�"))
  (:tp MOSS-RADIX (:id $RDX) (:unique))
  (:tp MOSS-DOCUMENTATION (:id $DOCT))
  (:tp MOSS-ONE-OF (:id $ONEOF))
  (:tp MOSS-TRANSACTION-NUMBER (:id $XNB))
  (:tp MOSS-TOMBSTONE (:id $TMBT))
  )

(defsysclass ((:en "MOSS-VIRTUAL-CONCEPT" "MOSS-VIRTUAL-CLASS")) (:id $VENT)
  (:tp MOSS-CONCEPT-NAME (:id $ENAM)) ; using multiple class names...
  (:tp MOSS-REFERENCE-CLASS (:id $RFC)(:unique))
  (:tp MOSS-DEFINITION (:id $DEF))
  (:tp MOSS-DOCUMENTATION (:id $DOCT))
  (:tp MOSS-TRANSACTION-NUMBER (:id $XNB))
  (:tp MOSS-TOMBSTONE (:id $TMBT))
  )

(defsysclass ((:en "MOSS-PROPERTY")) (:id $EPR)           
  (:tp MOSS-PROPERTY-NAME (:id $PNAM) (:unique)(:entry)) ;********** wrong entry
  (:tp MOSS-MINIMAL-CARDINALITY (:id $MINT) (:unique))
  (:tp MOSS-MAXIMAL-CARDINALITY (:id $MAXT) (:unique))
  (:tp MOSS-VALUE-RESTRICTION (:id $VRT)(:unique))
  (:tp MOSS-DOCUMENTATION (:id $DOCT))
  (:tp MOSS-TRANSACTION-NUMBER (:id $XNB))
  (:tp MOSS-TOMBSTONE (:id $TMBT))
  (:tp MOSS-SELECTION (:id $SEL))
  (:tp MOSS-RESTRICTION-OPERATOR (:id $OPR))
  (:tp MOSS-RESTRICTION-OPERATOR-ALL (:id $OPRALL))
  )

;+ kludge to get rid of loading error
(makunbound '$eps)
(defsysclass ((:en "MOSS-RELATION" "MOSS-STRUCTURAL-PROPERTY")) (:id $EPS)
  (:is-a MOSS-PROPERTY)
  )

(makunbound '$ept)
(defsysclass ((:en "MOSS-ATTRIBUTE" "MOSS-TERMINAL-PROPERTY")) (:id $EPT)
  (:is-a MOSS-PROPERTY)
  (:tp MOSS-ONE-OF (:id $ONEOF))
  (:tp MOSS-VALUE-TYPE (:id $TPRT))
  (:tp MOSS-VALUE-NOT-TYPE (:id $NTPR))
  (:tp MOSS-VALUE-REF (:id $VALR))
  ) ;; class option is used by system properties

;;; removing INDEX as a synonym of ENTRY-POINT because of a conflict with CG-USER!
(defsysclass ((:en "MOSS-ENTRY-POINT")) (:id $EP)
  (:tp MOSS-TRANSACTION-NUMBER (:id $XNB))
  (:tp MOSS-DOCUMENTATION (:id $DOCT))
  (:tp MOSS-TOMBSTONE (:id $TMBT))
  )

(makunbound '$eil)
(defsysclass "MOSS-INVERSE-LINK" (:id $EIL)
  (:tp MOSS-TRANSACTION-NUMBER (:id $XNB))
  (:tp MOSS-TOMBSTONE (:id $TMBT))
  (:tp MOSS-INVERSE-NAME (:id $INAM))
  )

(defsysclass ((:en "MOSS-NULL-CLASS")) (:id *none*))

(defsysclass ((:en "MOSS-UNIVERSAL-CLASS")) (:id *any*))

;; used as a default name property for objects

(defsysclass ((:en "MOSS-DOCUMENTATION")) (:id $DOCE)
  (:tp MOSS-TITLE (:id $TIT) (:entry (mln &key (package *package*)) 
                                "make a specific entry for doc indexes"
                                (declare (ignore package))
                                 (%make-entry-symbols mln :type '$DOCE)))
  (:tp MOSS-DOCUMENTATION (:id $DOCT))
  )

;;; versions. Class created so that we can attach descriptions to versions
;;; see MOSS-CONFIGURATION (depends on particular application and should be defined
;;; in the application)
;;;(defsysclass "MOSS-VERSION" (:id $VER)
;;;  (:tp MOSS-VERSION-NUMBER (:id $VNB)(:entry)) ; e.g. V-4 -> context=4
;;;  (:tp MOSS-VERSION-NAME (:id $VNM)(:entry (val &key (package *package*)) 
;;;                                           "make a specific entry for version names"
;;;                                           (declare (ignore package))
;;;                                           `(,(intern (string+ "V_" VAL))))
;;;       ) ; e.g. "2015"
;;;  (:tp MOSS-VERSION-DOCUMENTATION (:id $VDOC))
;;;  )

;;; Documentation (never used so far)
(defsysclass "English documentation" (:id $ENDCE) (:is-a DOCUMENTATION))
(defsysclass "French documentation" (:id $FRDCE) (:is-a DOCUMENTATION))

;;; defind now relations to allow attaching doc to any object
(defsyssp "MOSS-DOCUMENT" MOSS-UNIVERSAL-CLASS MOSS-DOCUMENTATION (:id $DOCS))

;;;=============  Now we are ready to create some links...

;;; for $ENT
;;; note that both values of TERMINAL-PROPERTY do not represent same entity
;;; HAS-MOSS-ATTRIBUTE is an accessor, but not HAS-MOSS-TERMINAL-PROPERTY
(defsyssp ((:en "MOSS-ATTRIBUTE"  "MOSS-TERMINAL-PROPERTY"))
  MOSS-ENTITY MOSS-TERMINAL-PROPERTY (:id $PT))

(defsyssp ((:en "MOSS-RELATION" "MOSS-STRUCTURAL-PROPERTY"))
  MOSS-ENTITY MOSS-STRUCTURAL-PROPERTY (:id $PS))

(defsyssp "MOSS-IS-A" MOSS-ENTITY MOSS-ENTITY (:id $IS-A))

(defsyssp "MOSS-OWN-METHOD" MOSS-ENTITY MOSS-METHOD (:id $OMS))

(defsyssp "MOSS-INSTANCE-METHOD" MOSS-ENTITY MOSS-METHOD (:id $IMS))

(defsyssp "MOSS-COUNTER" MOSS-ENTITY MOSS-COUNTER (:unique) (:id $CTRS))

(defsyssp "MOSS-SYSTEM" MOSS-ENTITY MOSS-SYSTEM (:id $SYSS) (:unique))

(defsyssp "MOSS-DEFAULT-SUCCESSORS" MOSS-ENTITY MOSS-UNIVERSAL-CLASS (:id $DEFS)) ;JPB0802

(defsyssp "MOSS-SEQUENCE" MOSS-ENTITY MOSS-ENTITY (:id $SEQS)) ; JPB121029

(defsyssp "MOSS-AND-PART" MOSS-ENTITY MOSS-ENTITY (:id $ANDPS)) ; JPB121029

(defsyssp "MOSS-OR-PART" MOSS-ENTITY MOSS-ENTITY (:id $ORPS)) ; JPB121029

(defsyssp "MOSS-XOR-PART" MOSS-ENTITY MOSS-ENTITY (:id $XORPS)) ; JPB121029


;;; of $SYS

(defsyssp "MOSS-REQUIREMENT" MOSS-SYSTEM MOSS-SYSTEM (:id $REQS))

(defsyssp "MOSS-ENTRY-POINT-LIST" MOSS-SYSTEM MOSS-ENTRY-POINT (:id $EPLS))

(defsyssp "MOSS-METHOD-LIST" MOSS-SYSTEM MOSS-METHOD (:id $FNLS))

(defsyssp "MOSS-STRUCTURAL-PROPERTY-LIST" MOSS-SYSTEM MOSS-STRUCTURAL-PROPERTY (:id $ESLS))

(defsyssp "MOSS-INVERSE-LINK-LIST" MOSS-SYSTEM MOSS-INVERSE-LINK (:id $EILS))

(defsyssp "MOSS-TERMINAL-PROPERTY-LIST" MOSS-SYSTEM MOSS-TERMINAL-PROPERTY (:id $ETLS))

(defsyssp "MOSS-ENTITY-LIST" MOSS-SYSTEM MOSS-ENTITY (:id $ENLS))

(defsyssp "MOSS-VIRTUAL-ENTITY-LIST" MOSS-SYSTEM MOSS-VIRTUAL-CONCEPT (:id $VELS))

(defsyssp "MOSS-CLASSLESS-COUNTER" MOSS-SYSTEM MOSS-COUNTER (:id $CLCS))

;;; adding the classless object counter to MOSSSYS
(%link  *moss-system* '$CLCS '$cls.ctr)


;;; of $EPS
(defsyssp "MOSS-INVERSE" MOSS-STRUCTURAL-PROPERTY MOSS-INVERSE-LINK (:id $INV))

(defsyssp "MOSS-SUCCESSOR" MOSS-STRUCTURAL-PROPERTY MOSS-ENTITY (:id $SUC))

(defsyssp "MOSS-SUC-ONE-OF" MOSS-STRUCTURAL-PROPERTY MOSS-UNIVERSAL-CLASS (:id $ONESUCOF))

(defsyssp "MOSS-SUC-VALUE" MOSS-STRUCTURAL-PROPERTY MOSS-UNIVERSAL-CLASS (:id $SUCV))

(defsyssp "MOSS-NOT-SUCCESSOR" MOSS-STRUCTURAL-PROPERTY MOSS-ENTITY (:id $NSUC))

;;; of $EPT
(defsyssp "MOSS-INVERSE" MOSS-TERMINAL-PROPERTY MOSS-INVERSE-LINK (:id $INV))

;;; for orphans
(defsystp "MOSS-OBJECT-NAME" (:id $OBJNAM) (:entry))

;;;=========================== Global variables ================================

(defsysvarname *context*)
(defsysvarname *language*)
(defsysvarname *version-graph*)

;;;========================= Not so fundamental methods ========================

;;; We add there information for describing a configuration and a user

(defsysclass "MOSS-CONFIGURATION" (:id $CNFG)
  (:tp MOSS-VERSION-NUMBER (:id $VNBT))
  (:tp MOSS-CONFIGURATION-NAME (:id $CFNM) (:entry))
  )

(defsysclass "MOSS-USER" (:id $USR)
  (:tp MOSS-USER-NAME (:id $USRNAM) (:entry))
  (:tp MOSS-RIGHTS (:id $ACRT))
  (:tp MOSS-CURRENT-VERSION (:id $CVRS))
  (:sp MOSS-CONFIGURATION MOSS-CONFIGURATION (:id $CFGS))
  )

(defsyssp "MOSS-USER"  MOSS-SYSTEM MOSS-USER (:id $USRS))

;;;------------------------------------------------- =UNKNOWN-METHOD (UNIVERSAL)
;;; used by moss-engine

(defsysuniversal =unknown-method (method-name)
  "Default method for taking care of unknown methods - must return nil" 
  (warn "~&Method ~A not available for object ~A in package ~S" 
    method-name *self* *package*)
  nil )

(setq *boot-mode* nil)

"*** end MOSS kernel bootstrap ***"

;;; make function undefined (no longer necessary)
;(fmakunbound 'send-no-trace)

(format t "~%;*** MOSS v~A - Boot loaded ***" *moss-version-number*)

;;; :EOF