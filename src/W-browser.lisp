;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;===============================================================================
;;;20/01/30
;;;		
;;;		          M O S S - B R O W S E R (File W-browser.Lisp)
;;;	
;;;===============================================================================
;;; This file contains all the functions related to browsing the MOSS space
;;; in the ACL environment.

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
-------
2020
 0130 restructuring the MOSS as an asdf system
|#

(in-package :moss)

;;;============================ MOSS function ===================================

(defun browse (obj-id &key owner database moss-window)
  (make-browser-window obj-id 
                       :owner owner ; unused if MOSS
                       :database database
                       :moss-window moss-window))
;(browse '$ENT)

;(defun edit (&rest ll) ll)

;;;=============================== Globals =======================================

;;; Parameters pertaining to this file
(defParameter *browser-height* 500)
(defparameter *cell-height* 14)
(defparameter *left-offset* 30 "left offset of the temporary window")
(defparameter *page-offset* 15 "top and left offset for identing browsers")

(defParameter *bw-trace* nil "debugging trace, when T prints everything.")

;;;============================= Macros ==========================================

(defMacro bwformat (&rest ll)
  `(if *bw-trace* (format *debug-io* ,@ll)))

;;;============================= Classes =========================================

(defClass BROWSER-BUTTON (cg:button) ())

;;; create a class for displaying lists of values
(defClass DISPLAY-AREA (cg:single-item-list) 
  ((current-property-type :accessor current-property-type)
   (value-list :accessor value-list :initform ())
   )
  (:documentation
   "Defines an item for displaying the list of values attached to a given property. ~
       When those values are clicked upon, then a new browser window appear with ~
       the new object. Nothing happens for terminal properties."))

;;; We define a default window for displaying entities using the window system
;;; The window shows the name of the class, the various properties

(defClass BROWSER (cg:dialog)
  ((current-entity :accessor current-entity)
   (current-editor :accessor current-editor :initform nil)
   (curent-property-info :accessor current-property-info :initform nil)
   (database :accessor database :initform nil)
   (moss-window :accessor moss-window :initform nil) ; nil for OMAS
   ;; owner and database are used by agent editor windows
   (owner :accessor owner :initform nil) ; an OMAS agent
   )
  (:documentation 
   "Defines the format and behavior of pages used by the MOSS browser. Does not ~
    allow any edition of the objects. Any object can be viewed. Its terminal ~
    structural and inverse properties are shown. Entry points associated with ~
    terminal properties can be visualized by double-clicking on any terminal ~
    property. Objects connected to the displayed object can be shown by ~
    double clicking on its summary. When a property is multi-valued, then ~
    clicking on it displays its values as a list. One then can double-click ~
    on the elements of the detailed list."))

;;;(defmethod user-close ((win browser))
;;;  "Closes the current browser, enabling the previous one"
;;;  (setq *browser* (previous-browser *BROWSER*))
;;;  (call-next-method)) 

;;;=========================== Service-functions =================================

(defmethod set-view-position ((win DISPLAY-AREA) position)
  "move a window to a specific position.
Arguments:
   position: top left corner."
  (setf (cg:left win) (cg:position-x position))
  (setf (cg:top win) (cg:position-y position))
  t)
  
;;;================================= Windows =====================================

(defun bw-compute-exterior (position old-page)
  "builds a new window for showing object.
Arguments:
   old-page: the page from which the new page is built
Return:
   the box giving the position and size of the new page."
  (declare (ignore position))
  (if old-page
      (cg:make-box (+ (cg:left old-page) *page-offset*) 
                   (+ (cg:top old-page) *page-offset*)
                   (+ (cg:right old-page) *page-offset*) 
                   (+ (cg:bottom old-page) *page-offset*))
    (cg:make-box 555 250 
                 (+ 555 520) (+ 250 *browser-height*))))

;;;------------------------------------------------------------ MAKE-BROWSER-INDOW
;;; builds a browser window 
;(make-browser-window '$ENT)

(defun make-browser-window
    (current-entity &key parent (window-owner (or parent (cg:screen cg:*system*)))
                    (border :frame) position form-p old-page owner database
                    current-editor moss-window)
  "Creates a window for displaying a MOSS object
Arguments (specific):
   old-page (key): unused
   owner (key): e.g. an oMAS agent
   database (key): database pathname if there
   current-editor (key): last created editor
   moss-window (key): moss-window only for MOSS"
  (let ((win
         (cg:make-window 
             'BROWSER
           :owner window-owner
           :class 'browser
           :exterior (BW-compute-exterior position old-page)
           :border border
           :childp t
           :close-button t
           :cursor-name :arrow-cursor
           :font (cg:make-font-ex :swiss "MS Sans Serif / ANSI" 11 nil)
           :form-package-name :common-graphics-user
           :BACKGROUND-COLOR (cg:MAKE-RGB :RED 241 :GREEN 243 :BLUE 169)
           :form-state :normal
           :maximize-button nil
           :minimize-button nil
           :name current-entity
           :pop-up nil
           :resizable nil
           :scrollbars nil
           :state :normal
           :status-bar nil
           :system-menu t
           ;:title (name agent)
           :title-bar t
           :toolbar nil
           :dialog-items (make-browser-widgets current-entity)
           :help-string nil
           :form-p form-p
           :form-package-name :common-graphics-user)))
    ;; save entity id
    (setf (current-entity win) current-entity)
    ;; save reference to previous browser
    ;(setf (previous-browser win) *browser*)
    ;; save agent-owner and browser in case editor page needs them
    (setf (owner win) owner)
    (setf (database win) database)
    (setf (current-editor win) current-editor)
    (setf (moss-window win) moss-window)
    ;; return window
    win))

(defun make-browser-widgets (current-entity)
  "build the various areas to display object info.
Arguments:
   current-entity: id ob object to be displayed
Return:
   a list of widgets."
    (list
     ;; === area to display details of a multivalued property
     (make-instance 'DISPLAY-AREA
       :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) :name :show-area
       :background-color (cg:make-RGB :RED 245 :GREEN 240 :BLUE 205)
       :behind nil  ; created to appear in front of all other widgets
       :border :black
       :tabs nil :top 10000 :left 10000 :width 400 :height 16
       :on-double-click 'BW-show-next-page-on-double-click
       :range nil)
     ;; === first zone: close, name, task, action, args
     ;; edit button (to open an editor for the displayed object)
     (make-instance 'browser-button
       :font (cg:make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)
       :left 8 :height 16 :width 50 :top 8 :title "Edit"
       :on-click 'BW-edit)
     ;; context
     (make-instance 'cg:STATIC-TEXT 
       :FONT (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
       :HEIGHT 14 :LEFT 430 :NAME :LABEL-ANSWERS :TOP 12 :width 80
       :VALUE (format nil "Context    ~A" moss::*context*))
     ;; description of object
     (make-instance 'cg:STATIC-TEXT 
       :FONT (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 24 NIL)
       :HEIGHT 27 :LEFT 120 :NAME :LABEL-ANSWERS :TOP 30 :width 335
       :name :class-name-area
       :VALUE (if (moss::%is-orphan? current-entity)
                "ORPHAN"
                (rformat 60 nil "~{~A ~}" 
                         (moss::send-no-trace current-entity 'moss::=summary))))
     ;; === second zone: area to display terminal properties
     (make-instance 'cg:STATIC-TEXT 
       :FONT (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
       :HEIGHT 16 :LEFT 20 :TOP 66 :width 135
       :VALUE "Attributes")
     (make-instance 'cg:multi-item-list 
       :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) :name :tp-area
       :tabs nil :left 60 :top 90 :width 400 
       :height (bwf-display-area-compute-height current-entity :tp)
       :on-click 'BW-show-tp-details-on-click
       :range (BW-page-get-terminal-property-and-value current-entity))
     ;; === third zone: area to display strctural properties
     (make-instance 'cg:STATIC-TEXT 
       :FONT (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
       :HEIGHT 16 :LEFT 20 :TOP 195 :width 145
       :VALUE "Relations")
     (make-instance 'cg:multi-item-list 
       :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) :name :sp-area
       :tabs nil :left 60 :top 220 :width 400 
       :on-click 'BW-show-sp-details-on-click
       :height (bwf-display-area-compute-height current-entity :sp)
       :range (BW-page-get-structural-property-and-value current-entity))
     ;; === fourth zone: area to display inverse links
     (make-instance 'cg:STATIC-TEXT 
       :FONT (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
       :HEIGHT 16 :LEFT 20 :TOP 350 :width 135
       :VALUE "Inverse Links")
     (make-instance 'cg:multi-item-list 
       :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) :name :il-area
       ;:background-color (AW-status-color agent)
       :tabs nil :left 60 :top 375 :width 400 
       :height (bwf-display-area-compute-height current-entity :il)
       :on-click 'BW-show-il-details-on-click
       :range (BW-page-get-inverse-property-and-value current-entity))
     ))

;;;============================= Actions ====================================

;;;------------------------------------------------------------------ BW-EDIT

(defun bw-edit (dialog widget)
  "call the editor on request"
  (declare (ignore widget))
  (let ((editor (edit (current-entity dialog)
                      :owner (owner dialog)
                      :database (database dialog)))
        (moss-window (moss-window dialog)))
      ;; otherwise set up moss-window
      (if moss-window
          (setf (next-editor moss-window) editor)))
  t)
        
;;;---------------------------------------------- BW-SHOW-IL-DETAILS-ON-CLICK

(defun bw-show-il-details-on-click (current-page widget)
  "Called when a line of the inverse property display area has been clicked. 
We display details of the predecessors, one per line."
  (let* ((current-entity (current-entity current-page))
         display-list inv-id value-list)
    ;; get first selected property
    (setq inv-id 
          (BWF-get-selected-prop-id current-page widget current-entity :il))
    (bwformat "~&;bw-show-il-details-on-click; inv-id: ~S" inv-id)
    ;; then get the list of predecessors linked by the inverse property
    (setq value-list (moss::%%get-value current-entity inv-id moss::*context*))
    ;; record property type and value list
    (setf (current-property-info current-page) (cons :il value-list))
    ;; otherwise prepare a display list of the successors
    (setq display-list 
          (mapcar #'(lambda (xx) 
                      (cons xx (rformat 60 nil "~{~A~^ ~}" 
                                        (moss::send xx '=summary))))
            value-list))
    ;; deselect tp and il items and remove any detail menus
    ;; not sure this works for ACL
    (bwf-deselect current-page :tp-area :sp-area)
    ;; display predecessors
    (bwf-enable-view-area widget :show-area display-list :il)
    ;; return widget identity (why not?)
    widget))

;;;---------------------------------------- BW-SHOW-NEXT-PAGE-ON-DOUBLE-CLICK
  
(defun bw-show-next-page-on-double-click (current-page item)
    "Displays the selected entity on another page, when clicking on item."
  (let ((property-type (car (current-property-info current-page)))
        (object-list (cdr (current-property-info current-page)))
        index obj-id)
    (bwformat "~&;=== BW-show-next-page; current-page: ~S ~
     ~&property-type: ~S; object-list: ~S" 
      current-page property-type object-list)
      ;; we open windows only for sp or il
      (case property-type
        ((:sp :il)
         ;; get the index of the selected cell
         (setq index (cg:list-widget-get-index item))
         ;; get the object from the list of objects
         (setq obj-id (nth index object-list))
         (bwformat "~&;bw-show-next-page; obj-id: ~S" obj-id)
         ;; open a new browser
         (make-browser-window obj-id 
                              :owner (owner current-page)
                              :database (database current-page)
                              :current-editor (current-editor current-page)
                              :moss-window (moss-window current-page))
         )
        ;; don't do anything for terminal values
        )))

;;;---------------------------------------------- BW-SHOW-SP-DETAILS-ON-CLICK

(defun bw-show-sp-details-on-click (current-page item)
  "Called when a line of the structural property display area has been clicked upon. 
   We display the details of the successors one per line."
  (let* ((current-entity (current-entity current-page))
         display-list suc-id value-list)
    ;; get first selected property
    (setq suc-id 
          (bwf-get-selected-prop-id current-page item current-entity :sp))
    (bwformat "~&;bw-show-sp-details-on-click; suc-id: ~S" suc-id)
    ;; then get the list of successors
    (setq value-list (moss::%%get-value current-entity suc-id moss::*context*))
    ;; record property and value list
    (setf (current-property-info current-page) (cons :sp value-list))
    ;; prepare a display list of the successors
    (setq display-list 
          (mapcar #'(lambda (xx) 
                      (cons xx (rformat 60 nil "~{~A~^ ~}" 
                                        (moss::send xx '=summary))))
                  value-list))
    ;; first deselect tp and il items and remove detail menus, clean edit area
    (bwf-deselect current-page :tp-area :il-area)
    ;; display successors
    (bwf-enable-view-area item :show-area display-list :sp)
    ;; return item identity
    item))

;;;---------------------------------------------- BW-SHOW-TP-DETAILS-ON-CLICK

(defun bw-show-tp-details-on-click (current-page item)
  "Called when a line of the terminal property display area has been clicked upon. 
   We display the details in the detail window one per line."
  (let* ((current-entity (current-entity current-page))
         tp-id display-list value-list)
    ;; get first selected property
    (setq tp-id 
          (BWF-get-selected-prop-id current-page item current-entity :tp))
    (bwformat "~&;bw-show-tp-details-on-click; tp-id: ~S" tp-id)
    ;; then get the list of values
    (setq value-list (moss::%%get-value current-entity tp-id moss::*context*))
    ;; record property and value-list
    (setf (current-property-info current-page) (cons :tp value-list))
    ;; prepare a display of the list of values
    (setq display-list 
          (mapcar #'(lambda (xx)
                      (rformat 60 nil "~A" (moss::send tp-id '=format-value xx)))
            value-list))
    ;; first deselect sp and il items and remove detail menus, clean edit area
    (bwf-deselect current-page :sp-area :il-area)
    ;; display successors
    (bwf-enable-view-area item :show-area display-list :tp)  
    
    ;; return item identity
    item))

;;;============================== Service Functions ===============================

;;;------------------------------------------------------------------- BWF-DESELECT

(defun BWF-deselect (current-page &rest area-list)
  "remove selection on items of the area name-list."
  (mapc #'(lambda (xx)
            (setf (cg:value (cg:find-component xx current-page)) nil) )
        area-list))
;(bwf-deselect *browser* :tp-area)
;;; ========== Functions to adjust initial size of the display areas

(defun BWF-display-area-compute-height (current-entity prop-type)
  "Adjust the original height of the display window so that it shows a maximum of ~
      6 values. If the entity has more than 6 properties, then we add scroll bars."
  (let ((number-of-values 
         (length 
          (case prop-type
            (:tp (moss::%%has-terminal-properties current-entity moss::*context*))
            (:sp (moss::%%has-structural-properties current-entity moss::*context*))
            (:il (moss::%%has-inverse-properties current-entity moss::*context*))))))
    ;; return the computed height taking into account the borders
    (+ 4 (if (< number-of-values 7)
             (* *cell-height* number-of-values)
           (* 6 *cell-height*)))))

;;;----------------------------------------------------------- BWF-ENABLE-VIEW-AREA

(defun BWF-enable-view-area (item area-name value-list property-type)
  "Enables the viewing area right below the selected cell and to the right. ~
   item is the area displaying the list (needed to position the table).
Arguments:
   item: widget containing the list of properties
   area-name: temporary window name, e.g. :show-area, :value-area
   value-list: list of values to display
   property-type: type of property, e.g. :il, :tp, :sp (unused)"
  (declare (ignore property-type))
  ;; first get cell position
  (let* ((current-page (cg:parent item))
         (position (car (cg:list-widget-get-index item)))
         (top-index (cg:first-visible-line item))
         (area (cg:find-component area-name current-page))
         (nb-of-items (length value-list))
         (max-nb-of-lines (if (> nb-of-items 10) 10 nb-of-items))
         left top view-height)
    ;; adjust position in case of scrolling
    (setq position (- position top-index))
    (bwformat "~&;bwf-enable-view-area; area: ~S;~&   position: ~S; ~
               nb-of-items: ~S; max-nb-of-lines: ~S~&   value-list: ~S; top-index: ~S"
               AREA position nb-of-items max-nb-of-lines value-list top-index)
    ;; compute the left top position of the temporary-window
    (setq left (+ (cg:left item) *left-offset*)
        top (+ (cg:top item) (* (1+ position) *cell-height*)))
    ;; compute the view height, limiting it to display 10 values
    (setq view-height (min (- *browser-height* (+ top
                                                  (- *cell-height* 2)))
                           (* max-nb-of-lines *cell-height*)))
    ;; dimension and position the temporary window
    (setf (cg:height area) view-height)
    (setf (cg:left area) left)
    (setf (cg:top area) top)
    ;; initialize content
    (setf (cg:range area) value-list)
    ;; put it in front (don't know how to do that)
    (cg:set-focus (cg:window area))
    ;; return area name
    area))

;;;------------------------------------------------------- BWF-GET-SELECTED-PROP-ID

(defun BWF-get-selected-prop-id (dialog widget current-entity prop-type)
  "obtain tp or sp or il from the selected value in the display list"
  (declare (ignore dialog))
  (let ((position (car (cg:list-widget-get-index widget)))
        prop-list)
    (bwformat "~&;BWF-get-selected-prop-id; position: ~S" position)
    ;; if not there error
    (unless position (error "cannot locate property from the display list"))
    ;; get the corresponding list of property ids
    (setq prop-list
          (case prop-type
            (:tp (moss::%%has-terminal-properties current-entity moss::*context*))
            (:sp (moss::%%has-structural-properties current-entity moss::*context*))
            (:il (moss::%%has-inverse-properties current-entity moss::*context*))))
    ;; extract and return property
    (nth position prop-list)))
    
;;; ========== Browsing page and functions

;;; functions to get values to display into the browsing window

;;;------------------------------------ =FORMAT-VALUE-LIST (INVERSE PROPERTY)

(catch :error
         (%defmethod 
          =format-value-list INVERSE-LINK (suc-list)
          "Prints a summary of all linked entities - 
Arguments: 
   suc-list: list of successor ids."   
          (rformat 60 nil "~A:     ~{~<~%     ~1:;~{~A~^ ~}~>~^, ~}"
                   (car (moss::send *self* '=get-name))
                   (mapcar #'(lambda (xx) (moss::send xx '=summary))
                     suc-list))))

;;;----------------------------------- BW-PAGE-GET-INVERSE-PROPERTY-AND-VALUE

(defun bw-page-get-inverse-property-and-value (obj-id)
  "get all local inverse properties and associated values as a list of strings"
  (mapcar 
      #'(lambda (xx)(moss::send xx '=format-value-list 
                                (moss::%get-value obj-id xx)))
    (moss::%%has-inverse-properties obj-id moss::*context*)))

;;;--------------------------------- =FORMAT-VALUE-LIST (STRUCTURAL PROPERTY)

(catch :error
       (moss::%defmethod 
        =format-value-list STRUCTURAL-PROPERTY (suc-list)
        "Prints a summary of all linked entities.
Arguments:
   successors: list of successor ids."   
        (rformat 60 nil "~A:     ~{~<~%     ~1:;~{~A~^ ~}~>~^, ~}"
                 (car (moss::send *self* '=get-name))
                 (mapcar #'(lambda (xx) (moss::send xx '=summary))
                   suc-list))))

;;;-------------------------------- BW-PAGE-GET-STRUCTURAL-PROPERTY-AND-VALUE

(defun bw-page-get-structural-property-and-value (obj-id)
  "get all local structural properties and associated values as a list of strings"
  (mapcar 
      #'(lambda (xx) (moss::send xx '=format-value-list 
                           (moss::%get-value obj-id xx)))
   (moss::%%has-structural-properties obj-id moss::*context*)))

;;;----------------------------------- =FORMAT-VALUE-LIST (TERMINAL PROPERTY)

(catch :error
       (moss::%defmethod 
        =format-value-list TERMINAL-PROPERTY (value-list)
        "Format a list of values, presumably attached to a property. We limit the length ~
             of the string to 80 characters, to avoid problems with Pascal strings.
Arguments:
   value-list: list of values to format (successors)."
        (rformat 80 nil "~A:     ~{~A~^, ~}"
                 (car (moss::send *self* '=get-name))
                 (mapcar #'(lambda (xx) (moss::send *self* '=format-value xx))
                   value-list))))

;;;---------------------------------- BW-PAGE-GET-TERMINAL-PROPERTY-AND-VALUE

(defun bw-page-get-terminal-property-and-value (obj-id)
  "get all local terminal properties and associated values as a list of strings"
  (mapcar 
      #'(lambda (xx)(moss::send xx '=format-value-list 
                          (moss::%get-value obj-id xx)))
   (moss::%%has-terminal-properties obj-id moss::*context*)))


(moss::format t "~%;*** MOSS v~A - browser loaded ***" moss::*moss-version-number*)

;;; :EOF