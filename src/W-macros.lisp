;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;===============================================================================
;;;20/01/30
;;;                W I N D O W - M A C R O S  (file W-macros.lisp)
;;;
;;;
;;;===============================================================================
;;; The idea is to define a single set of functions that could create and handle MCL 
;;; or ACL windows. However, MCL has disappeared but the macros are still used.

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
-------
2010
 0816 Created by importing file from OMAS
      only use actions on widgets currently
 0928 bug in cr-multi-item-list
 1121 correcting %unselect-cell-at-index
 1130 correcting bug in cr-multi-item-list
2011
 0623 adding double-clicks to buttons
2013
 0420 adding raw option to displaying values (for the editor)
2014
 0307 -> UTF8 encoding
2020
 restrucuturing the MOSS system
|#

#|
The following code is used to create ACL or MCL windows

(make-window 'OMAS-agent :owner owner
            :class 'omas-agent
            :exterior (AW-compute-exterior position )
            :border border
            :childp t
            :close-button nil
            :cursor-name :arrow-cursor
            :font (make-font-ex :swiss "MS Sans Serif / ANSI" 11 nil)
            :form-package-name :common-graphics-user
            :BACKGROUND-COLOR (MAKE-RGB :RED 255 :GREEN 128 :BLUE 128)
            :form-state :normal
            :maximize-button nil
            :minimize-button nil
            :name 'form-message
            :pop-up nil
            :resizable nil
            :scrollbars nil
            :state :normal
            :status-bar nil
            :system-menu t
            ;:title (name agent)
            :title-bar nil
            :toolbar nil
            :dialog-items (make-agent-widgets agent)
            :path #p"D:\\OMAS-v5.0.12a-PC\\5-omas-agentwin-P.lisp"
            :help-string nil
            :form-p form-p
            :form-package-name :common-graphics-user)

(make-instance 
          'OMAS-agent
          :WINDOW-TYPE :single-edge-box 
          :WINDOW-TITLE (format nil "~S" (name agent))
          :VIEW-POSITION `,(make-point (- *screen-left-offset* 4)
                                       (+ 158 (* (1- position) 114)))
          :VIEW-SIZE #@(540 110)
          :COLOR-P t
          ;:VIEW-FONT '("Chicago" 12 :SRCOR :PLAIN)
          :VIEW-SUBVIEWS (make-agent-window-widgets agent)
          )

(cr-window 'OMAS-agent
           :type :document

ACL/MCL Window Options are:
---------------------------

window class (OMAS-agent)
:class :window-type
:owner :view-container
:exterior :view-size/:view-position/:view-origin?
:border -
:childp -
:close-button / :close-box-p
:cursor-name :arrow-cursor
:font :view-font
:form-package-name :common-graphics-user
:BACKGROUND-COLOR :back-color
:form-state -
:maximize-button :grow-icon-p
:minimize-button -
:name :view-nick-name
:pop-up -
:resizable -
:scrollbars - :h-scrollp / :v-scrollp (scrolling-fred-window)
:state -
:status-bar -
:system-menu -
:title :window-title
:title-bar -
:toolbar -
:dialog-items :view-subviews
:path -
:help-string -
:form-p -
:form-package-name -
- :color-p
- :color-list
- :wptr
- :window-do-first-click
- :window-active-p
- :view-scroll-position
- :direction
- :window-active-p
- :auto-position


SCROLLING-FRED-VIEW (SFV)
SIMPLE-VIEW (SV)
DIALOG-ITEM (DI

:ALLOW-RETURNS         SFV  
:ALLOW-TABS            SFV
:AUTO-POSITION               W
:BACK-COLOR                  W
:BAR-DRAGGER           SFV
:BUFFER                SFV
:BUFFER-CHUNK-SIZE     SFV
:CLOSE-BOX-P                 W
:COLOR-P                     W
:COMTAB                SFV
:CONTENT-COLOR               W
:COPY-STYLES-P         SFV
:DIALOG-ITEM-ACTION                       DI
:DIALOG-ITEM-ENABLED-P                    DI
:DIALOG-ITEM-HANDLE                       DI
:DIALOG-ITEM-TEXT      SFV                DI
:DIRECTION             SFV   W   V   SV   DI
:ERASE-ANONYMOUS-INVALIDATIONS W
:FILENAME              SFV
:GROW-ICON-P                 W
:H-SCROLLP             SFV
:H-SCROLL-CLASS        SFV
:H-PANE-SPLITTER       SFV       
:HELP-SPEC             SFV   W   V   SV   DI
:HISTORY-LENGTH        SFV
:IGNORE                SFV   W   V   SV   DI
:JUSTIFICATION         SFV
:LINE-RIGHT-P          SFV
:MARGIN                SFV
:PART-COLOR-LIST       SFV                DI
:PROCESS                     W
:PROCID                      W
:SAVE-BUFFER-P         SFV
:TEXT-EDIT-SEL-P       SFV
:THEME-BACKGROUND            W
:TRACK-THUMB-P         SFV
:V-SCROLL-CLASS        SFV
:V-SCROLLP             SFV
:V-PANE-SPLITTER       SFV
:VIEW-FONT             SFV
:VIEW-CONTAINER        SFV
:FRED-ITEM-CLASS       SFV
:Dg-SCROLLER-OUTLINE SFV
:H-SCROLL-FRACTION     SFV
:GROW-BOX-P            SFV
:VIEW-CONTAINER              W   V   SV   DI
:VIEW-FONT                   W   V   SV   DI
:VIEW-NICK-NAME        SFV   W   V   SV   DI
:VIEW-POSITION         SFV   W   V   SV   DI
:VIEW-SCROLL-POSITION  SFV   W   V
:VIEW-SIZE             SFV   W   V   SV   DI
:VIEW-SUBVIEWS         SFV   W   V
:WINDOW-DO-FIRST-CLICK       W
:WINDOW-LAYER                W
:WINDOW-SHOW                 W
:WINDOW-TITLE                W
:WINDOW-TYPE                 W
:WORD-WRAP-P           SFV
:WPTR                  SFV   W   V   SV   DI
:WRAP-P                SFV

Common Windows Options
----------------------
Type
   :type 
      :standard
      :document
      :menu
      :pop-up
Title
   :title
Position and size
   :top :left :bottom :right (screen coordinates)
Name
   :name
Scrollbbars
   :scrollbars
State
   :state
Background-color
   :background-color
Font
   :font
Owner
   :owner
Package
   :package
Subviews, dialo-items
   :subviews vs :dialog-items

|#

(in-package :moss)

#+ALLEGRO
(eval-when (compile load eval)
  (import '(cg:beep cg:find-component cg:select-window cg:screen cg:make-box
            cg:make-rgb cg:make-font-ex cg:make-window cg:find-sibling
            cg:windowp cg:set-focus-component cg:selected-object 
            cg:find-window cg:find-pixmap cg:interior-width
            cg:interior-height cg:add-component
            cg:static-text cg:editable-text cg:multi-line-editable-text
            cg:button cg:check-box cg:progress-indicator cg:radio-button
            )))

;;;===============================================================================
;;;                               COLORS
;;;===============================================================================

;;; colors. When in doubt use (ask-user-for-color) ACL or (user-pick-color) MCL
#+ALLEGRO (defParameter *black-color* cg:black)
#+ALLEGRO (defParameter *blue-color* cg:blue)
#+ALLEGRO (defParameter *green-color* cg:green)
#+ALLEGRO (defParameter *red-color* cg:red)
#+ALLEGRO (defParameter *light-blue-color* (cg:make-rgb :RED 66 :GREEN 160 :BLUE 255))
#+ALLEGRO (defParameter *orange-color* (cg:make-rgb :RED 255 :GREEN 128 :BLUE 0))
#+ALLEGRO (defParameter *tan-color* (cg:make-rgb :RED 128 :GREEN 64 :BLUE 0))
#+ALLEGRO (defParameter *white-color* cg:white)
;; not really yellow that one cannot read
(defParameter *yellowish-color*  #+MCL 15781437 #+ALLEGRO cg:yellow)
(defParameter *grey-color* #+MCL 9866133 
  #+ALLEGRO (cg:make-rgb :RED 128 :GREEN 128 :BLUE 128))
(defParameter *light-green-color* #+MCL 6748510
  #+ALLEGRO (cg:make-rgb :RED 0 :GREEN 255 :BLUE 64))
(defParameter *light-grey-color* #+MCL *grey-color*
  #+ALLEGRO (cg:make-rgb :RED 228 :GREEN 228 :BLUE 228))

;;;===============================================================================
;;;
;;;                          COMPATIBILITY MACROS
;;;
;;;===============================================================================

;;;--------------------------------------------------------------------- CR-WINDOW

(defMacro cr-window (&key class type title top left bottom right height width
                          (close-button t) maximize-button (minimize-button t)
                          name scrollbars state background-color font
                          scroller-class track-thumb-p
                          (title-bar t)
                          package owner subviews dialog-items)
  "produced MCL or ACL make-window function according to the feature list.
Argument:
   background-color (key): depends on system
   bottom (key): bottom
   class: window-class, a user defined window class
   close-button (key): t, the default close button (default t)
   font (key): depends on system
   left (key): left
   maximize-button (key): t, the button for expanding the window (default nil)
   minimize-button (key): t, the button makes window into an icon (default t)
   name (key): name, ususlly a keyword, e.g. :omas-window
   owner (key): the owner of the window (another window or the screen)
   package (key): package
   right (key): right
   scrollbars (key): :vertical, :horizontal, :both
   scroller-class (key): MCL, class of the scrolling area
   state (key): :normal, shrunk
   subviews (key): list of widgets
   dialog-items (key): same as subviews
   title (key): window title
   title-bar (key): t, specifies a title bar area (defaault t)
   top (key): top
   track-thumb-p (key): MCL, needed for scrolling
   type (key): :standard, :document, :popup, :menu (default :document, MCL)
Return:
   a window object."
  #+MICROSOFT-32
  (declare (ignore type scroller-class track-thumb-p))
  #+MICROSOFT-32
  `(cg:make-window 
    ,(or name ''cg:frame-window)
    :owner ,(or owner '(cg:screen cg:*system*))
    :class ,class
    :exterior (cg:make-box ,left ,top (or ,right (+ ,left ,width)) 
                           (or ,bottom (+ ,top ,height)))
    :border :frame
    :childp nil
    :close-button ,close-button
    :cursor-name :arrow-cursor
    :font ,(or font (cg:make-font-ex :swiss "MS Sans Serif / ANSI" 11 nil))
    :form-package-name :common-graphics-user
    :background-color ,(or background-color
                           (cg:make-rgb :red 255 :green 128 :blue 128))
    :form-state ,state
    :maximize-button ,maximize-button
    :minimize-button ,minimize-button
    ,@(if name `(:name ,name))
    :pop-up nil
    :resizable nil
    :scrollbars ,scrollbars
    :state ,(or state :normal)
    :status-bar nil
    :system-menu t
    :title ,(or title "")
    :title-bar ,title-bar
    :toolbar nil
    :dialog-items ,(or dialog-items subviews)
    :path nil
    :help-string nil
    :form-p nil
    :form-package-name (or ,package *package*))
  #+MCL
  (declare (ignore owner package scrollbars state close-button
                   maximize-button title-bar))
  #+MCL
  `(let* ((width (or ,width (if (and ,left ,right) (- ,right ,left) 200)))
          (height (or ,height (if (and ,top ,bottom) (- ,bottom ,top) 100)))
          (win
           (make-instance ,(or class ''dialog)
             ,@(if type `(:window-type ,type))
             :window-title ,(or title "")
             :view-position (make-point (or ,left 100) 
                                        (or ,top 100))
             :view-size (make-point width height)
             :color-p t
             ,@(if name `(:view-nick-name ,name))
             ,@(if font `(:view-font ,font))
             :view-subviews ,(or subviews dialog-items)
             ,@(if scroller-class `(:scroller-class ,scroller-class))
             ,@(if track-thumb-p `(:track-thumb-p ,track-thumb-p))
             ,@(if minimize-button `(:grow-icon-p ,minimize-button)
                   `(:grow-icon-p t))
             )))
     ,@(if background-color `((set-back-color win ,background-color)))
     win)
  )

#|
(cr-window :class 'OMAS-agent
           :type :single-edge-box
           :title (format nil "~S" (name agent))
           :left 4
           :top (+ 158 (* (1- position) 114))
           :right (+ 4 540)
           :bottom (+ (+ 158 (* (1- position) 114)) 110)
           :background-color (omas::\%rgb 64764 48059 51143) ; pink
           :dialog-items (make-agent-window-widgets agent))
;;; expands to (MCL)
(LET* ((WIDTH (OR NIL (IF (AND 4 (+ 4 540)) (- (+ 4 540) 4) 200)))
       (HEIGHT
        (OR NIL
            (IF (AND (+ 158 (* (1- POSITION) 114))
                     (+ (+ 158 (* (1- POSITION) 114)) 110))
                (- (+ (+ 158 (* (1- POSITION) 114)) 110)
                   (+ 158 (* (1- POSITION) 114)))
                100)))
       (WIN
        (MAKE-INSTANCE
          'OMAS-AGENT
          :WINDOW-TYPE
          :SINGLE-EDGE-BOX
          :WINDOW-TITLE
          (FORMAT NIL "~S" (NAME AGENT))
          :VIEW-POSITION
          (MAKE-POINT (OR 4 100) (OR (+ 158 (* (1- POSITION) 114)) 100))
          :VIEW-SIZE
          (MAKE-POINT WIDTH HEIGHT)
          :COLOR-P
          T
          :VIEW-SUBVIEWS
          (MAKE-AGENT-WINDOW-WIDGETS AGENT)
          :GROW-ICON-P
          T)))
  (SET-BACK-COLOR WIN (%RGB 64764 48059 51143))
  WIN)
;;; axpands to (ACL)
(CG.BASE:MAKE-WINDOW 'CG.BASE:FRAME-WINDOW :OWNER
                     (CG.BASE:SCREEN CG.BASE:*SYSTEM*)
  :CLASS 'OMAS-AGENT
  :EXTERIOR (CG.BASE:MAKE-BOX 4 (+ 158 (* (1- POSITION) 114))
                              (COND ((+ 4 540)) (T (+ 4 NIL)))
                              (COND
                               ((+
                                 (+ 158 (* (1- POSITION) 114))
                                 110))
                               (T
                                (+
                                 (+ 158 (* (1- POSITION) 114))
                                 NIL))))
  :BORDER :FRAME
  :CHILDP NIL
  :CLOSE-BUTTON T
  :CURSOR-NAME :ARROW-CURSOR
  :FONT #.(CG.BASE:MAKE-FONT-EX :SWISS "MS Sans Serif / ANSI" 11)
  :FORM-PACKAGE-NAME :COMMON-GRAPHICS-USER
  :BACKGROUND-COLOR (%RGB 64764 48059 51143)
  :FORM-STATE NIL
  :MAXIMIZE-BUTTON NIL
  :MINIMIZE-BUTTON T
  :POP-UP NIL
  :RESIZABLE NIL
  :SCROLLBARS NIL
  :STATE :NORMAL
  :STATUS-BAR NIL
  :SYSTEM-MENU T
  :TITLE (FORMAT NIL "~S" (NAME AGENT))
  :TITLE-BAR T
  :TOOLBAR NIL
  :DIALOG-ITEMS (MAKE-AGENT-WINDOW-WIDGETS AGENT)
  :PATH NIL
  :HELP-STRING NIL
  :FORM-P NIL
  :FORM-PACKAGE-NAME (COND (NIL) (T *PACKAGE*)))
|#
;;;===============================================================================
;;;
;;;                                  WIDGETS
;;;
;;;===============================================================================

;;; Supported WIDGETS are:
;;;   - check-box
;;;   - button
;;;   - static-text
;;;   - one-line-editable-text
;;;   - editable-text
;;;   - multi-item-list
;;;
#|
;; close button, agent name is associated with this button
(make-instance 'check-box :font (make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)
           :left 5 :height 16 :name 'agent-close :top 3 :title (omas::name agent) 
           :on-click 'AW-close-on-click)

;;;=== CLOSE
(make-instance 'check-box-dialog-item
     :view-position #@(5 6)
     :view-size #@(15 15)
     :dialog-item-action 'aw-close-on-click)  
(make-instance 'static-text-dialog-item
     :view-position #@(25 6)
     :view-size #@(510 15)
     :view-font '("Chicago" 12 :SRCOR :PLAIN)
     :dialog-item-text (format nil "~S" (name agent)))

(cr-check-box :top 6 :left 5 :height 15 :on-click 'aw-close-on-click :title "exit")
|#
;;;------------------------------------------------------------------------ BUTTON
#|
;; inspect button (example)
(make-instance 'button :font (make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil)
               :height 16 :left 5 :name 'inspect-button :width 50 :on-click
               'AW-inspect-on-click :title "Inspect" :top 20)
;;=== INSPECT
(make-instance 'button-dialog-item
  :view-position #@(465 2)
  :view-size #@(70 15)
  :dialog-item-text "inspect"
  :dialog-item-action 'aw-inspect-on-click
  :view-nick-name :inspect-button)
(cr-check-button 'button-dialog-item :top 20 :left 5 :width 50 :height 16 
                 :name :inspect-button :on-click 'aw-inspect-on-click)

|#

(defMacro cr-button (&key class left top height width name title font on-click
                          on-double-click)
  #+ALLEGRO
  `(make-instance ,(or class ''cg:button)
     ,@(if font `(:font ,font))
     :left ,left
     :top ,top
     :height ,height
     :width ,(or width 70)
     :name ,name 
     :title ,(or title "")
     :on-click ',on-click
     ,@(if on-double-click `(:on-double-click ',on-double-click))
     )
  #+MCL
  `(make-dialog-item 
    ,(or class ''button-dialog-item)
    (make-point (max (- (or ,left 3) 3) 0) (or ,top 0))
    (make-point (or ,width 100) (or ,height 20))
    ,(or title "")
    ,(if on-click `#'(lambda (item)(,on-click (view-container item) item)))
        ,@(cond 
       ((and on-click on-double-click)
        `(#'(lambda(item)
              (if (double-click-p)
                (,on-double-click (view-container item) item)
                (,on-click (view-container item) item)))))
       (on-double-click
        `(#'(lambda(item)
              (if (double-click-p)
                (,on-double-click (view-container item) item)
                ))))
       (on-click
        `(#'(lambda(item)
              (,on-click (view-container item) item))))
       (T (LIST NIL)))
    ,@(if name `(:view-nick-name ,name))
    ,@(if font `(:view-font ,font))
    )
  )

#|
? (cr-button 
    :class 'OMAS-button
    :left 388 :top 2 :width 70 :height 15
    :title "trace"
    :on-click aw-trace-on-click
    :name :trace-button)
;; For MCL
(MAKE-DIALOG-ITEM
  'OMAS-BUTTON
  (MAKE-POINT (MAX (- (OR 388 3) 3) 0) (OR 2 0))
  (MAKE-POINT (OR 70 100) (OR 15 20))
  "trace"
  #'(LAMBDA (ITEM) ('AW-TRACE-ON-CLICK (VIEW-CONTAINER ITEM) ITEM))
  :VIEW-NICK-NAME
  :TRACE-BUTTON)
;; for ACL
(MAKE-INSTANCE 'OMAS-BUTTON :LEFT 388 :TOP 2 :HEIGHT 15 :WIDTH 70
               :NAME :TRACE-BUTTON :TITLE "trace" :ON-CLICK
               ''AW-TRACE-ON-CLICK)
|#
;;;--------------------------------------------------------------------- CHECK-BOX

;;; check boxes are created using the default classes

(defMacro cr-check-box (&key left height width name top title title-width font 
                             color check on-change)
  #+ALLEGRO
  (declare (ignore width))
  #+ALLEGRO
  `(make-instance 'cg:check-box
     ,@(if name `(:name ,name))
     ,@(if font `(:font ,font))
     :left ,left
     :top ,top
     ,@(if title-width `(:width ,title-width))
     :height ,(or height 20)
     ,@(if color `(:foreground-color ,color))
     :title ,(or title "")
     :on-change ',on-change
     :value ,check
     )
  #+MCL
  (declare (ignore width))
  #+MCL
  `(make-dialog-item 
    'check-box-dialog-item
    (make-point ,(max (- (or left 3) 3) 0) ,(or top 0))
    (make-point ,(or title-width 100) ,(or height 20))
    ,(or title "")
    #'(lambda(item) 
        (,on-change item
                    (check-box-checked-p item) ; new value
                    (not (check-box-checked-p item)) ; old value
                    )) 
    ;; color for MCL is a list e.g. (:text *yellow-color* :frame *green-color*)
    ,@(if color `(:part-color-list ,color))
    ,@(if font `(:view-font ,font))
    :check-box-checked-p ,check
    ,@(if name `(:view-nick-name ,name)))
  )

#|
(cr-check-box :title "trace messages"
              :title-width 120
              :name :trace-messages
              :on-change OP-trace-messages-on-change
              :top 30 :left 8 :height 20
              :font *check-label-font*
              :color *check-label-color*
              :check (trace-messages *omas*)
              )

;;; for MCL:
(MAKE-DIALOG-ITEM
  'CHECK-BOX-DIALOG-ITEM
  (MAKE-POINT 5 30)
  (MAKE-POINT 120 20)
  "trace messages"
  #'(LAMBDA (ITEM)
      (OP-TRACE-MESSAGES-ON-CHANGE
        ITEM
        (CHECK-BOX-CHECKED-P ITEM)
        (NOT (CHECK-BOX-CHECKED-P ITEM))))
  :PART-COLOR-LIST
  (LIST :TEXT *CHECK-LABEL-COLOR*)
  :VIEW-FONT
  *CHECK-LABEL-FONT*
  :CHECK-BOX-CHECKED-P
  (TRACE-MESSAGES *OMAS*)
  :VIEW-NICK-NAME
  :TRACE-MESSAGES)
;;; For ACL:
(MAKE-INSTANCE 'CG.CHECK-BOX:CHECK-BOX :NAME :TRACE-MESSAGES :FONT
               *CHECK-LABEL-FONT* :LEFT 8 :TOP 30 :WIDTH 120 :HEIGHT
               20 :FOREGROUND-COLOR *CHECK-LABEL-COLOR* :TITLE
               "trace messages" :ON-CHANGE
               'OP-TRACE-MESSAGES-ON-CHANGE :VALUE
               (TRACE-MESSAGES *OMAS*))
|#

;;;----------------------------------------------------------------- EDITABLE-TEXT
#|
   (make-instance 'editable-text-dialog-item
     :view-position #@(7 27)
     :view-size #@(526 12)
     :view-font '("Chicago" 9 :SRCOR :PLAIN)
     :dialog-item-text (aw-make-task-id-list agent)
     :text-edit-sel-p t
     :view-nick-name 'task-id)

   (make-instance 'MOSS-OUTPUT-PANE
     :view-position #@(10 245)
     :view-size #@(622 519)
     :h-scrollp nil
     :word-wrap-p t
     :view-font '("Tahoma" 11 :SRCOR :bold)
     :dialog-item-text "<answer pane>"
     :view-nick-name :answer-pane)

(make-instance 'cg:EDITABLE-TEXT 
  :FONT (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
  :LEFT 8 :TOP 10
  :HEIGHT 20 :WIDTH 368
  :NAME :display-pane
  :TEMPLATE-STRING NIL 
  :UP-DOWN-CONTROL NIL 
  :VALUE "" 
  ) 
|#

(defMacro cr-text-item (&key class left height width name top text font word-wrap
                             allow-returns delayed
                             (color #xFFFFFF color?)
                             (text-selected nil there?)
                             on-change)
  #+ALLEGRO
  (declare (ignore word-wrap allow-returns color color? text-selected there?))
  #+ALLEGRO
  `(make-instance 
     ',(or (eval class) 'cg:editable-text)
     :left ,left
     :top ,top
     :height ,height
     :width ,width
     ,@(if name `(:name ,name))
     ,@(if delayed `(:delayed ,delayed))
     ,@(if font `(:font ,font))
     ,@(if on-change `(:notify-when-selection-changed t :on-change ',on-change
                                                      :up-down-control nil
                                                      :delayed nil))
     :value ,(if (null text) "" text)
     )
  #+MCL
  (declare (ignore on-change delayed))
  #+MCL
  `(make-instance
     ',(or (eval class) 'editable-text-dialog-item)
     :view-position (make-point (or ,left 0) (or ,top 0))
     :view-size (make-point (or ,width 100) (or ,height 16))
     ,@(if name `(:view-nick-name ,name))
     ,@(if font `(:view-font ,font))
     ,@(if there? `(:text-edit-sel-p ,text-selected))
     ,@(if color? `(:part-color-list 
                    '(:body ,color :frame *master-window-color*)))
     :word-wrap-p ,word-wrap
     :allow-returns ,allow-returns
     :dialog-item-text ,text
     )
  )

#|
(cr-text-item :class  'omas-dialog-item
              :left (car pos)
              :top  (cadr pos)
              :width  (car size)
              :height (cadr size)
              :text "so what?"
              :word-wrap  t
              :allow-returns  t 
              :name :list
              :font *assistant-label-font*)
expands to (MCL)
(MAKE-INSTANCE
  'OMAS-DIALOG-ITEM
  :VIEW-POSITION
  (MAKE-POINT (OR (CAR POS) 0) (OR (CADR POS) 0))
  :VIEW-SIZE
  (MAKE-POINT (OR (CAR SIZE) 100) (OR (CADR SIZE) 16))
  :VIEW-NICK-NAME
  :LIST
  :VIEW-FONT
  *ASSISTANT-LABEL-FONT*
  :WORD-WRAP-P
  T
  :ALLOW-RETURNS
  T
  :DIALOG-ITEM-TEXT
  "so what?")
;; for ACL
(MAKE-INSTANCE 'OMAS-DIALOG-ITEM :LEFT (CAR POS) :TOP (CADR POS)
               :HEIGHT (CADR SIZE) :WIDTH (CAR SIZE) :NAME :LIST
               :FONT *ASSISTANT-LABEL-FONT* :VALUE "so what?")
|#
;;;--------------------------------------------------------------- MULTI ITEM LIST
#|
     ;; input messages
     (make-instance 'multi-item-list 
       :font (make-font-ex nil "Tahoma / ANSI" 11 nil) :name :input-messages
       :background-color (AW-status-color agent)
       :tabs nil :left 5 :top 38 :width 282 :height 70
       :range (mapcar #'omas::message-format (omas::input-messages agent)))
   ;;=== INPUT MESSAGES
   (make-instance 'area-for-values
     :view-position #@(5 45) 
     :view-size #@(260 60) 
     :dialog-item-text "Input messages"
     :cell-size #@(260 15)
     :view-font '("Chicago" 9 :SRCOR :PLAIN)
     :table-hscrollp nil
     :table-vscrollp t
     :view-nick-name 'input-messages
     :table-sequence 
     (mapcar #'message-format (input-messages agent)))

(cr-multi-item-list 'area-for-values  
                    :left 5 :top 45 :height 60 :width 260 :title "Input Messages"
                    :name :input-messages 
                    :sequence (mapcar #'message-format (input-messages agent))
                    :hscrollp nil :vscrollp t :cell-height 15)
|#
(defMacro cr-multi-item-list (&key class left height width name top title 
                                   (title-bar t title-bar?)
                                   font sequence cell-height cell-width hscrollp 
                                   vscrollp selection-type tabs cell-color state
                                   (selected-values nil selected-values?)
                                   on-click on-double-click)
  #+ALLEGRO
  (declare (ignore cell-height hscrollp vscrollp))
  #+ALLEGRO
  `(make-instance ,(or class ''cg:multi-item-list)
     ,@(if font `(:font ,font))
     :left ,left
     :top ,top
     :width ,(or width cell-width)
     :height ,height
     ,@(if title-bar? `(:title-bar ,title-bar))
     ,@(if name `(:name ,name))
     ,@(if state `(:state ,state))
     ,@(if tabs `(:tabs ,tabs))
     ,@(if selection-type `(:selection-type ,selection-type))
     ,@(if selected-values? `(:value ,selected-values))
     ,@(if on-click `(:on-click ',on-click))
     ,@(if on-double-click `(:on-double-click ',on-double-click))
     :range ,sequence
     :title ,(or title "")
     ,@(if cell-color `(:background-color ,cell-color))
     )
  #+MCL
  (declare (ignore background-color tabs state selected-values title-bar?
                   title-bar selected-values?))
  #+MCL
  `(make-dialog-item 
    ,(or class ''sequence-dialog-item)
    (make-point (max (- (or ,left 3) 3) 0) (or ,top 0))
    (make-point (or ,(or width cell-width) 100) (or ,height 32))
    ,(or title "")
    ,@(cond 
       ((and on-click on-double-click)
        `(#'(lambda(item)
              (if (double-click-p)
                (,on-double-click (view-container item) item)
                (,on-click (view-container item) item)))))
       (on-double-click
        `(#'(lambda(item)
              (if (double-click-p)
                (,on-double-click (view-container item) item)
                ))))
       (on-click
        `(#'(lambda(item)
              (,on-click (view-container item) item))))
       (T (LIST NIL)))
    ,@(if name `(:view-nick-name ,name))
    ,@(if font `(:view-font ,font))
    :cell-size (make-point (or ,cell-width ,width 100) (or ,cell-height 16))
    ,@(if cell-color `(:cell-colors ,cell-color))
    ,@(if selection-type `(:selection-type ,selection-type)) ;:single, :contiguous
    ; :disjoint
    :table-hscrollp ,hscrollp
    :table-vscrollp ,vscrollp
    :table-sequence ,sequence
    )
  ) 
#|
? (cr-multi-item-list :class 'area-for-values
                      :title "List of found objects"
                      :name :agents/messages
                      :on-double-click OP-agents/messages-on-double-click
                      :cell-width 100
                      :cell-height 15
                      :hscrollp nil
                      :vscrollp t
                      :selection-type :disjoint
                      :sequence (mapcar #'car (omas::local-agents omas::*omas*))
                      :top 5 :left 383 :height 95
                      )
;; for MCL
(MAKE-DIALOG-ITEM
  'AREA-FOR-VALUES
  (MAKE-POINT 380 5)
  (MAKE-POINT 100 95)
  "List of found objects"
  #'(LAMBDA (ITEM)
      (IF (DOUBLE-CLICK-P)
          (OP-AGENTS/MESSAGES-ON-DOUBLE-CLICK (VIEW-CONTAINER ITEM) ITEM)))
  :VIEW-NICK-NAME
  :AGENTS/MESSAGES
  :CELL-SIZE
  (MAKE-POINT 100 15)
  :SELECTION-TYPE
  :DISJOINT
  :TABLE-HSCROLLP
  NIL
  :TABLE-VSCROLLP
  T
  :TABLE-SEQUENCE
 (MAPCAR #'CAR (LOCAL-AGENTS *OMAS*)))
;; for ACL
(MAKE-INSTANCE 'AREA-FOR-VALUES :LEFT 383 :TOP 5 :WIDTH
               (COND (NIL) (T 100)) :HEIGHT 95 :NAME
               :AGENTS/MESSAGES :ON-DOUBLE-CLICK
               'OP-AGENTS/MESSAGES-ON-DOUBLE-CLICK :RANGE
               (MAPCAR #'CAR (LOCAL-AGENTS *OMAS*)) :TITLE
               "List of found objects")
|#
;;;------------------------------------------------------------------- STATIC TEXT
#|
   (make-instance 'static-text-dialog-item
     :view-position #@(25 6)
     :view-size #@(510 15)
     :view-font '("Chicago" 12 :SRCOR :PLAIN)
     :dialog-item-text (format nil "~S" (name agent)))


(make-instance 'static-text-dialog-item
     :view-position #@(7 25)
     :view-size #@(526 14)
     :view-font '("Chicago" 9 :SRCOR :PLAIN)
     :dialog-item-text (aw-make-task-id-list agent)
     ;:text-edit-sel-p t
     :part-color-list '(:body #xFFFFFF)
     :view-nick-name 'task-id)

(make-dialog-item 'omas-DIALOG-ITEM
                       #@(5 8) #@(368 13) 
                       ""
                       NIL
                       :VIEW-NICK-NAME 'output
                       :ALLOW-RETURNS t
                       )

|#

(defMacro cr-static-text (&key class top left height width name text title font 
                               color justification)
  #+MCL
  `(make-dialog-item 
    ,(or class ''static-text-dialog-item)
    (make-point (max (- (or ,left 3) 3) 0) (or ,top 0))
    (make-point (or ,width 100) (or ,height 32))
    ,(or title text "")
    nil
    ,@(if name `(:view-nick-name ,name))
    ,@(if font `(:view-font ,font))
    ,@(if justification `(:text-justification ,justification))
    ,@(if color `(:part-color-list (list :body ,color)))
    )
  #+ALLEGRO
  (declare (ignore justification))
  #+ALLEGRO
  `(make-instance ,(or class ''cg:static-text)
     ,@(if top `(:top ,top))
     ,@(if left `(:left ,left))
     ,@(if height `(:height ,height))
     ,@(if width `(:width ,width))
     ,@(if name `(:name ,name))
     ,@(if font `(:font ,font))
     ,@(if color `(:background-color ,color))
     :value ,(or title text "")
     )
  )

#|
(cr-static-text :class 'omas-DIALOG-ITEM
                :top 8 :left 8 :width 368 :height 13
                :name :display-pane
                )
;; for MCL
(MAKE-DIALOG-ITEM
  'OMAS-DIALOG-ITEM
  (MAKE-POINT (MAX (- (OR 8 3) 3) 0) (OR 8 0))
  (MAKE-POINT (OR 368 100) (OR 13 32))
  ""
  NIL
  :VIEW-NICK-NAME
 :DISPLAY-PANE)
;; for ACL
(MAKE-INSTANCE 'OMAS-DIALOG-ITEM :TOP 8 :LEFT 8 :HEIGHT 13 :WIDTH
  368 :NAME :DISPLAY-PANE :VALUE "")
|#
;;;===============================================================================
;;;
;;;                              Service functions
;;;
;;;===============================================================================
#|
;--- windows
close (win)                            -> %window-close (win)
cg:add-component
cg:dialog-items (win)                  -> %get-window-panes (win)
cg:find-window 
cg:make-box
cg:make-window (...)                   -> cr-window (...)
cg:screen 
cg:*system* 
cg:windowp (win)                       -> %window? (win)
;--- widgets
cg:button                              -> cr-button
cg:check-box                           -> cr-checkbox
cg:dialog-items 
cg:editable-text                       -> cr-text-item
cg:left (item)                         -> %left (item)
cg:multi-item-list                     -> cr-multi-item-list
cg:multi-line-editable-text
cg:progress-indicator                  -> cr-thermometer (to do)
cg:radio-button 
cg:static-text                         -> cr-static-text
;--- operations on widgets
cg:available                           -> %disable-item (item)
                                          %enable-item (item)
                                          %is-item-enabled? (item)
cg:background-color (item)             -> %set-item-color (item)
cg:beep ()                             -> %beep ()
                                       -> %disable-item (item)
                                       -> %enable-item (item)
cg:find-component (name win)           -> %find-named-item (name win)
cg:find-pixmap 
cg:find-sibling (name win)             -> %find-sibling (name win)
cg:first-visible-line (item)           -> %first-visible-cell-index (item)
cg:font 
cg:font-line-height 
cg:foreground-color                    -> %set-text-color
                                       -> %get-cell-size (item)
                                       -> %get-cell-index-from-value (item value)
                                       -> %get-cell-value-from-index (item index)
cg:height (item)                       -> %height (item)
cg:interior-width
cg:interior-height 
cg:list-widget-get-index               -> %get-selected-cells-indexes (pane)
cg:list-widget-set-index (pane)        -> %set-selected-cell-at-index (to do)
cg:make-font-ex 
cg:make-position (hh vv)               -> %make-point (hh vv)
cg:make-rgb (rr gg bb)                 -> %rgb (rr gg bb)
cg:parent (item)                       -> %container (item)
                                       -> %point-x
                                       -> %point-y
cg:range (pane)                        -> %get-item-sequence (pane)
                                          %set-item-sequence (pane value-list)
cg:right (item)                        -> %right (item)
cg:set-focus-component (pane)          -> %select-pane (win pane)
cg:selected-object (win)               -> %get-selected-objet
                                          %select-cell (item index)
cg:select-window (dialog)              -> %select-window (dialog)
cg:set-focus-component (item)          -> %set-focus-item (item)
cg:set-selection (dialog)              -> %select-window (dialog)
                                          %select-all (pane)
cg:title (item text)                   -> %set-item-text (item text)
                                       -> %set-title (pane text)
cg:value (item)                        -> %get-item-text (item)
                                          %set-item-value (item value)
                                          %get-selected-cells (item)
                                          %set-item-text (item text)
                                          %unselect (item)
                                          %unselect-all (item)
                                          %unselect-cell (item index)
cg:width (item)                        -> %width (item)
;--- miscellaneous
cg:read-from-string-safely
|#
;;;------------------------------------------------------------------------- %BEEP

(defUn %beep ()
  #+MCL (beep)
  #+ALLEGRO (cg:beep)
  )

;;;-------------------------------------------------------------------- %CONTAINER

(defUn %container (item)
  #+MCL (view-window item) ;; check for view-container??
  #+ALLEGRO (cg:parent item)
  )

;;;----------------------------------------------------------------- %DISABLE-ITEM

(defUn %disable-item (item)
  #+MCL (dialog-item-disable item)
  #+ALLEGRO (setf (cg:available item) nil)
  )

;;;------------------------------------------------------------------ %ENABLE-ITEM

(defUn %enable-item (item)
  #+MCL (dialog-item-enable item)
  #+ALLEGRO (setf (cg:available item) t)
  )

;;;-------------------------------------------------------------- %FIND-NAMED-ITEM

(defUn %find-named-item (name win)
  "get the named view inside the window."
  #+MCL (view-named name win)
  #+ALLEGRO (cg:find-component name win)
  )

;;;----------------------------------------------------------------- %FIND-SIBLING

(defUn %find-sibling (name win)
  "get the (nick)named view inside the same window as item."
  #+MCL
  (view-named name win)
  #+ALLEGRO
  (cg:find-component name (cg:parent win))
  )

;;;----------------------------------------------------- %FIRST-VISIBLE-CELL-INDEX

(defun %first-visible-cell-index (item)
  "get the index of the first visible cell (0 if no scroll)"
  #+MCL
  (scroll-position item)
  #+ALLEGRO
  (cg:first-visible-line item)
  )

;;;---------------------------------------------------- %GET-CELL-INDEX-FROM-VALUE

(defUn %get-cell-index-from-value (item index)
  "get the index of a cell corresponding to its content value."
  #+MCL
  (cell-to-index item index)
  #+ALLEGRO
  (position index (cg:range item) :test #'equal)
  )

;;;---------------------------------------------------------------- %GET-CELL-SIZE

(defUn %get-cell-size (item)
  "get the size of a cell for a given item."
  #+MCL
  (cell-size item)
  #+ALLEGRO
  (%make-point (cg:width item) (cg:font-line-height (cg:font item)))
  )

;;;---------------------------------------------------- %GET-CELL-VALUE-FROM-INDEX

(defUn %get-cell-value-from-index (item index)
  "get the content value of a cell from its index (first is 0)."
  #+MCL
  (error "%get-cell-value-from-index not implemented")
  #+ALLEGRO
  (nth index (cg:range item))
  )

;;;------------------------------------------------------------ %GET-ITEM-SEQUENCE

(defUn %get-item-sequence (pane)
  #+MCL (table-sequence pane)
  #+ALLEGRO (cg:range pane)
  )

;;;--------------------------------------------------------------- %GET-ITEM-TEXT

(defUn %get-item-text (pane)
  #+MCL (dialog-item-text pane)
  #+ALLEGRO (cg:value pane)
  )

;;;-------------------------------------------------------------- %GET-ITEM-VALUE

(defUn %get-item-value (pane)
  "same as %get-item-text"
  #+MCL (dialog-item-text pane)
  #+ALLEGRO (cg:value pane)
  )


;;;----------------------------------------------------------- %GET-SELECTED-CELLS

(defUn %get-selected-cells (item)
  "returns the selected cells of a multi-item-list."
  #+MCL
  (mapcar #'(lambda (xx) (nth (cell-to-index item xx) (table-sequence item)))
          (selected-cells item))
  #+ALLEGRO
  (cg:value item)
  )

;;;--------------------------------------------------- %GET-SELECTED-CELLS-INDEXES
;;;********** test for MCL ??

(defUn %get-selected-cells-indexes (item)
  "get a list of indexes corresponding to the selected cell, e.g. (2 3)."
  #+MCL (mapcar #'(lambda(xx) (cell-to-index item xx))(selected-cells item))
  #+ALLEGRO (cg:list-widget-get-index item)
  )

;;;------------------------------------------------------------- %GET-WINDOW-PANES

(defUn %get-window-panes (win)
  #+MCL (view-subviews win)
  #+ALLEGRO (cg:dialog-items win)
  )

;;;----------------------------------------------------------------------- %HEIGHT

(defUn %height (item)
  #+MCL (point-v (view-size item))
  #+ALLEGRO (cg:height item)
  )

;;;------------------------------------------------------------- %IS-ITEM-ENABLED?

(defUn %is-item-enabled? (item)
  #+MCL (dialog-item-enabled-p item)
  #+ALLEGRO (cg:available item)
  )

;;;------------------------------------------------------------------------- %LEFT

(defUn %left (item)
  #+MCL (point-h (view-position item))
  #+ALLEGRO (cg:left item)
  )

;;;------------------------------------------------------------------- %MAKE-POINT

(defUn %make-point (hh vv)
  #+MCL (make-point hh vv)
  #+ALLEGRO (cg:make-position hh vv)
  )

;;;---------------------------------------------------------------------- %POINT-X

(defUn %point-x (point)
  #+MCL (point-h point)
  #+ALLEGRO (cg:position-x point)
  )

;;;---------------------------------------------------------------------- %POINT-Y

(defUn %point-y (point)
  #+MCL (point-v point)
  #+ALLEGRO (cg:position-y point)
  )

;;;-------------------------------------------------------------------------- %RGB

(defUn %rgb (red green blue)
  "make a color from the components 0-65535."
  #+ALLEGRO
  ;; ACL components are limited to 0-255
  (cg:make-rgb :red (floor (/ red 256)) 
               :green (floor (/ green 256)) 
               :blue (floor (/ blue 256)))
  #+MCL
  (make-color red green blue))

; (omas::%rgb 65535 0 0)
;;;----------------------------------------------------------------------- %RIGHT

(defUn %right (item)
  #+MCL (+ (point-h (view-position item))(point-h (view-size item)))
  #+ALLEGRO (cg:right item)
  )

;;;----------------------------------------------------------------------- %RIGHT

(defUn %scroll-to-cell (item index)
  "make sure that the indexed cell is visible in the pane.
Arguments:
   item: pane
   index: index of the cell (0, 1, 2,...)"
  #+MCL (scroll-to-cell item (index-to-cell item index))
  #+ALLEGRO (cg:set-first-visible-line item index :relative)
  )

;;;------------------------------------------------------------------- %SELECT-ALL

(defUn %select-all (pane)
  "select all the text of a text window"
  #+MCL (select-all pane)
  #+ALLEGRO (cg:set-selection pane 0 (1+ (length (cg:value pane)))) 
  t)

;;;--------------------------------------------------------- %SELECT-CELL-AT-INDEX

(defUn %select-cell-at-index (item index)
  "select cell at a given position (index)."
  (declare (ignore dummy))
  #+MCL (cell-select item (index-to-cell item index))
  #+ALLEGRO (cg:list-widget-set-index item index) 
  ;#+ALLEGRO(setf (cg:value item)
  t)

;;;------------------------------------------------------------------ %SELECT-PANE

(defUn %select-pane (dialog pane)
  "select pane setting the mouse into it"
  #+MCL (set-current-key-handler dialog pane)
  #+ALLEGRO (declare (ignore dialog))
  #+ALLEGRO (cg:set-focus-component pane)
  )

;;;---------------------------------------------------------------- %SELECT-WINDOW

(defUn %select-window (dialog)
  "selects a window"
  #+MCL (window-show dialog)
  #+ALLEGRO (cg:select-window dialog)
  )

;;;--------------------------------------------------------------- %SET-FOCUS-ITEM

(defUn %set-focus-item (item)
  "set the focus on the particular item."
  #+MCL (set-current-key-handler (view-window item) item t)
  #+ALLEGRO (cg:set-focus-component item)
  )

;;;-------------------------------------------------------------- %SET-ITEM-BOTTOM

(defun %set-item-bottom (item bottom)
  "change the item bottom position"
  #+MCL
  (set-view-size item (point-h (view-size item))
                 (- bottom (point-v (view-position item))))
  #+ALLEGRO
  (setf (cg:bottom item) bottom)
  )

;;;--------------------------------------------------------------- %SET-ITEM-COLOR

(defUn %set-item-color (item color)
  "set the color (different structure in ACL or MCL) of a part."
  #+MCL (set-part-color item :body color)
  #+ALLEGRO (setf (cg:background-color item) color)
  )

;;;-------------------------------------------------------------- %SET-ITEM-HEIGHT

(defun %set-item-height (item height)
  "change the item height"
  #+MCL
  (set-view-size item (point-h (view-size item)) height)
  #+ALLEGRO
  (setf (cg:height item) height)
  )

;;;---------------------------------------------------------------- %SET-ITEM-LEFT

(defun %set-item-left (item left)
  "change the item left position"
  #+MCL
  (set-view-position item left (point-v (view-position item)))
  #+ALLEGRO
  (setf (cg:left item) left)
  )

;;;--------------------------------------------------------------- %SET-ITEM-RIGHT

(defun %set-item-right (item right)
  "change the item right position, careful view size should be set for MCL"
  #+MCL
  (set-view-size item (- right (point-h (view-position item)))
                 (point-v (view-size item)))
  #+ALLEGRO
  (setf (cg:right item) right)
  )

;;;------------------------------------------------------------ %SET-ITEM-SEQUENCE

(defUn %set-item-sequence (item sequence &optional raw)
  "set the values of a sequence. 
Arguments:
   item: a the multi item list component 
   sequence: a list of strings.
   raw (opt): any non nil value will display the ra value, e.g. strings with quotes"
  #+MCL (set-table-sequence item sequence)
  #+ALLEGRO (if raw
            (setf (cg:range item) 
              (mapcar #'(lambda(xx)(format nil "~S" xx)) sequence))
          (setf (cg:range item) sequence)
          )
  )

;;;---------------------------------------------------------------- %SET-ITEM-TEXT

(defUn %set-item-text (item text &optional raw)
  (declare (ignore raw))
  #+MCL (set-dialog-item-text item text)
  #+ALLEGRO (setf (cg:value item) text)
  )

;;;-------------------------------------------------------------- %SET-ITEM-TITLE

(defUn %set-item-title (item text)
  "set the text of a part."
  #+MCL (error "to do")
  #+ALLEGRO (setf (cg:title item) text)
  )

;;;----------------------------------------------------------------- %SET-ITEM-TOP

(defun %set-item-top (item top)
  "change the item top position"
  #+MCL
  (set-view-position item (point-h (view-position item)) top)
  #+ALLEGRO
  (setf (cg:top item) top)
  )

;;;--------------------------------------------------------------- %SET-ITEM-VALUE

(defUn %set-item-value (item text &optional raw)
  #+MCL (set-dialog-item-text item text)
  #+ALLEGRO 
  (if raw
      (setf (cg:value item) (format nil "~S" text))
    (setf (cg:value item) text)
    )
  )

;;;--------------------------------------------------------------- %SET-ITEM-WIDTH

(defun %set-item-width (item width)
  "change the item width"
  #+MCL
  (set-view-size item width (point-v (view-size item)))
  #+ALLEGRO
  (setf (cg:width item) width)
  )

;;;--------------------------------------------------------------- %SET-TEXT-COLOR

(defUn %set-text-color (item color)
  #+ALLEGRO (setf (cg:foreground-color item) color)
  #+MCL (set-part-color item :text color)
  )
;;;-------------------------------------------------------------------- %SET-TITLE

(defUn %set-title (pane text)
  #+MCL (set-dialog-item-text pane text)
  #+ALLEGRO (setf (cg:title pane) text)
  )

;;;------------------------------------------------------------------------- %TOP

(defUn %top (item)
  #+MCL (point-v (view-position item))
  #+ALLEGRO (cg:top item)
  )

;;;--------------------------------------------------------------------- %UNSELECT

(defUn %unselect (item)
  #+MCL (declare (ignore item))
  #+MCL nil
  #+ALLEGRO (setf (cg:value item) nil)
  )

;;;----------------------------------------------------------------- %UNSELECT-ALL

(defUn %unselect-all (item)
  #+MCL (let ((cell-list (selected-cells item)))
          (dolist (cell cell-list)
            (cell-deselect item cell)))
  #+ALLEGRO (cg:list-widget-set-index item -1 t)
  )

;;;-------------------------------------------------------- %UNSELECT-CELL-AT-INDEX

(defUn %unselect-cell-at-index (item index)
  #+MCL (cell-deselect item (index-to-cell item index)) ; JPB1011
  #+ALLEGRO (cg:list-widget-set-index item index t)
  )

;;;------------------------------------------------------------------------ %WIDTH

(defUn %width (item)
  #+MCL
  (point-h (view-size item))
  #+ALLEGRO
  (cg:width item)
  )

;;;---------------------------------------------------------------------- %WINDOW?

(defUn %window? (win)
  "checks if win is an existing window.
Arguments:
   win: a lisp object
Return:
   t if an existing window."
  #+MCL
  (and win (wptr win))
  #+ALLEGRO
  (cg:windowp win)
  )

;;;----------------------------------------------------------------- %WINDOW-CLOSE

(defUn %window-close (win)
  (when (%window? win)
    (close win)
    ))

(format t "~%;*** MOSS v~A - window macros loaded ***" *moss-version-number*)

;;; :EOF
