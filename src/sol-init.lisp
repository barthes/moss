;;;-*- Mode: Lisp; Package: "SOL" -*-
;;;============================================================================
;;;20/01/30		
;;;		S O L - I N I T - (File sol-init.lisp)
;;;	
;;;============================================================================
;;; SOL-INIT was written for CLISP. Here it is adapted to Franz ACL v9.0

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; This file is used to define the SOL packages and initialize globals to be used 
;;; for compiling MOSS files into something else.
;;; Initializes *sol-directory* to point to *load-pathname* of the fasl file.
;;; Default language is English.

#|
History
-------
2006
 0918 adding relative reference for the ontology folder. 
      Uses *ontology-initial-directory*
 0929 introducing sol-make to prepare a folder for a new SOL version
2007
 0210 new SOL version without ltk (batch)
 1022 adding sparql
2008
 0111 version 5.2
 0308 adding compilation directives
 0318 adding compile-sol to compile sol-init for the sol-make
2018 
 1218 === version 6.0 - inserting it into the MOSS environment
|#

(in-package "SOL")

;;;----------------------------- Globals --------------------------------------

(defParameter *copyright* "�Barth�s@UTC,2008")

;;;------------------------------- MOSS mode ----------------------------------
;;; We call SOL from the MOSS panel window
;;; Set up a new process in a specific package :SOL-<application>
;;; We set a default rule format to :jena because a rule format is needed for  
;;; virtual concepts, attributes, relations or rules.

(defun sol (sol-file-pathname &key owl html rules (rule-format :jena)
                              (language :EN) (verbose t) errors-only)
  "get the parameters from the import window and call the compiler.
   First arg is SOL file, the rest are keywords enclosed in strings.
Arguments:
   sol-file-pathname: name of the file to compile
   errors-only (key): if t, compile check no output file
   owl (key): pathname of the output file for the OWL translation
   html (key): pathname of the output HTML file
   rules (key): pathname of the output file for JENA or SPARQL rules
   language (key): language of the ontology (default is :EN for English)
   rule-format (key): Jena or SPARQL (default :jena)
   verbose (key): tracing flag (default is t, we trace compilation)
Return:
   nil in case of error"
  (let ((control-file-pathname
         (make-pathname 
          :name (format nil "control-~A" (moss::get-current-date :compact t))
          :type "txt"
          :defaults sol-file-pathname)))
    (format t "~%; sol::sol /*package*: ~S" *package*)
    
    ;;=== debug: print parameters into a text file
    (with-open-file (ss control-file-pathname :if-exists :supersede
                        :if-does-not-exist :create :direction :output)
      (format ss "~%Ontology: ~S ~%language: ~S ~
                  ~%verbose: ~S ~%OWL output: ~S~%TEXT output: ~S  ~
                   ~%RULE output: ~S" 
        sol-file-pathname language verbose owl html rules) 
      )
    ;;=== end debug

    ;; compile-sol returns nil on error
    ;; it will create an output file near the input file
    (unless (sol-owl::compile-sol sol-file-pathname  
                                  :rule-output rules
                                  :verbose verbose 
                                  :language language
                                  :errors-only errors-only
                                  :rule-format rule-format)
      ;(format t "*** Error while compiling to OWL ***")
      (return-from sol nil))
    
    ;; when OK, check if html or text output is required; if so, do it
    (when html
      ;; produce HTML and TEXT files, no trace
      ;(sol-html::compile-sol sol-file-pathname :language language :verbose nil)
      )
    ;(format t "~%*** End of processing ***")
    
    t))

(format t "~%;*** MOSS v~A - SOL init loaded ***" moss::*moss-version-number*)
  
;;; :EOF