;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=============================================================================
;;;20/01/30
;;;		D I A L O G - E N G I N E (File dialog-engine.lisp)		
;;;             Copyright Jean-Paul Barthes @ UTC 2000-2013
;;;
;;;=============================================================================
;;; This file contains a model of dialog engine to be used between a user and MOSS.

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
-------
2005
 0408 creation
      We create a conversation class whose instances will contain instances of 
      a particular conversation graph. The current conversation id is kept in
      the *conversation* global variable.
 0413 changing lists to strings in dialogs.
 0906 change start-conversation, show-conversation, display text, activate-
      answer-panel
 0907 removing the resume feature from the =execute macro
      corrected resume-conversation
 0926 adapting the m-defstate to the agent world by restoring the :send-message
      feature
2006
 0308 Version 6
 1009 correcting :ask-TCW option in =execute macro
      =========
      m-defstate -> defstate
      changing ask-TCW for MCL/OMAS
      changing make-state mechanism
 1011 changig *pattern-marker* to the string "?*" to avoid package problems
 1014 adding restart-conversation,modifying transitions and immediate transition
      :reset option
 1102 adding performative to conversations in transitions
      improving the treatment of the :no-execute option
 1203 adding functions to produce and fill patterns
2007
 0107 introducing defsubdialog as a synonym to defdialogmixin
 0116 adding find-objects, generate-access-patterns and clean-patterns
 0130 adding process-why-question and incidence mechanism to process why and
      interruption requests
 0214 changes in activate-answer-panel
 0217 modified find-objects to make it more efficient
 0222 updating process-wait for ACL
 0225 correcting conversation-process-transitions
 0305 minor changes (adding ELIZA to immediate transitions)
 0320 adding :return to set-state-for-subdialog so that we can return from incident
      conversations
 0328 replacing ACTION by Q-ACTION, remove TSW
 0728 replacing display-text call by send conversation '=display-text
 0730 *conversation* is declared as a gobal variable of the converse process
 0728 replacing display-text call by send conversation '=display-text
      in the start-conversation function
 0809 introducing French viewpoint switching in ELIZA
 0823 merge activate-answer-panel for MCL and MICROSOFT
      corrected a mistake in fill-answer-pattern-for-one-level
 1003 correcting make-state-send-message
 1103 removing OMAS context when using only MOSS
 1205 changing =execute to "=EXECUTE" to intern into execution package in defstate
2008
 0610 replacing #+OMAS #-OMAS by (member :OMAS *features*) so as to use the same
      code when running MOSS separately from OMAS. Compiler is unhappy when
      compiling without OMAS
 0621 === v7.0.0
 0725 change make-state-send-message send-request -> send-message
 0817 restructuring the defstate functions
2009
 0208 change in make-state-send-message
 0209 change in show-conversation
 0519 simplifying the dialog mechanism
 0804 removing noop bindings in make-state-resume-transitions (cosmetic change)
 0806 adding from option to sent messages in make-state-send-message 
 0910 (this seems to be a mistake since send-subask does not take a from argument)
 1017 integrating make-state-send-message patch and FILL-ANSWER-PATTERN-FOR-ONE-LEVEL
2010
 0406 include :error as a failure in is-answer-failure?
 0523 making second arg optional in is-answer-failure?
 0606 correcting a bug in is-answer-failure?
 0622 adding defsimple-subdialog to simplify dialog programming
 0725 adding a states option to the defsubdialog macro
 0814 adding "?" as a segmenting char to Eliza
 1015 cleaning state crawler
 1010 modified make-state-resume-transitions, 
 1015 bug in fill-answer-pattern-for-one-level
 1110 adding example to defsimple-subdialog
 1122 improving display-answer
2011
 0227 removing (noop bindings) from make-state-resume-transitions
      adding :wait test into make-state-question-there? so that we can force the
      =resume method to wait for an external input
      correcting the :reset option
 0303 modifying segment-master-input to single out abort commands
 0314 we reestablish the distinction between a master's input into the FACTS/RAW-INPUT
      and an answer from an external source (when a request message has been sent)
      into the FACTS/ANSWER area of the conversation object in the activate-
      answer-panel function
 0315 fixing up the activate-answer-panel function
 0319 fixing activate-answer-panel
2011
 0416 adding repeat-count to MAKE-STATE-SEND-MESSAGE
 0422 introducing the no-wait option in the defstate macro
 0423 introducing the :rules and :display-result options in the defstate macro
 0503 adding a case to is-answer-faillure?
 0526 introducing simplified patterns (* "j" "ai" "dit" ?x) and defelizarules
 0707 updating moss-eliza and use-eliza-rules to use simplified rules
 0804 adding input into the answer slot (maybe  not such a good idea)
      adding :clear-all-facts, :clear-fact and :clear-facts options to =execute
 0806 modifying order in make-state-resume-transitions
 1230 introducing salient features
2012
 0124 microcontext: adding header instead of state in SF marks
 0403 correcting bug in  make-state-resume-method
2013
 0110 adding the :format-answer option to the list of defstate options
 0117 introducing a new mechanism for web interaction: saving text, question 
      and answer, and let them be sent back by activate-answer-panel
 0129 adding make-complex-pattern and defcomplexelizarules
 0130 adding make-complex-action
 1207 changing gate reference into crawler
2014
 0307 -> UTF8 encoding
 0902 adding :text-exec to make-state-question-there?
 0903 adding (:pa-dialog . t) to message args to indicate the message comes from
     the dialog and the answer should be a list of a single string
2015
 0402 tracing is done by inserting :dialog into *debug-tags*
2016
 0209 adding :format-answer-full to :transitions options
 0704 ajout d'un "ignore-errors dans la fonction dialog-engine
 0710 make :explanation optional in defstate
|#

(in-package :moss)

;; we need the omas package to compile the macros
(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :omas)(make-package :omas))
  (import '(mp:process-wait))
  (moss-trace-add :dialog "Tracing the dialog crawler and related functions")
  (moss-trace :dialog 1)
  ;(setq *verbose* t)
  )

;(defVar _q-abort () "abort state in main dialog")

(defUn noop (&rest ll) ll) ; to keep the compiler quiet when bindings is used

(defUn in-let-list? (xx let-list)
  (cond
   ((null let-list) nil)
   ((and (listp (car let-list))(eql xx (caar let-list))))
   ((listp (car let-list)) (in-let-list? xx (cdr let-list)))
   ((eql xx (car let-list)))
   (t (in-let-list? xx (cdr let-list)))))


;;;=================================== macros ==================================

;;;-------------------------------------------------------------------- DEFSTATE
;;; The defstate macro is fairly complex and defined the dialog language.
;;; The use of the various options is non trivial.
;;; When a state is entered, the =execute method is applied to the new state.
;;; Then the =resume method is called on the state.
;;; If a question to the user was asked (:question, :ask, options),
;;; then processing the answer waits until something appears in the
;;; to-do slot of the conversation object.
;;;
;;; the =execute method
;;; ===================
;;; The method normally processes the content of the :INPUT area of the FACTS slot
;;; of the conversation. It may have a single option :question. Without options 
;;; the method does nothing and the =resume method is called immediately.
;;; The FACTS/INPUT area should contain a list of words or the symbol :FAILURE.
;;; The return value is :WAIT or :RESUME.
;;;
;;;   :answer-type  
;;;   ------------
;;; Can be used to specify the type of the expected answer message. If the answer
;;; is not of that type, it shows a rupture in the dialog.
;;;
;;;   :clear-all-facts
;;;   ----------------
;;; Clears the content of the FACT base (removes everything)
;;;
;;;   :clear-fact (item)
;;;   ------------------
;;; Sets the value associated with item to NIL. Usually item is a keyword.
;;;
;;;   :clear-facts (item-list)
;;;   ------------------------
;;; Same as clear-fact but takes a list of items
;;;
;;;   :delay-answer (&optional direction)
;;;   -------------------------
;;; Applies to web interfaces. Sets a flag in the FACT area of the conversation 
;;; objects so that the answer of requests to external agents are not posted
;;; immediately but saved until OMAS asks the next question. Direction is :inc,
;;; :dec, or :clear (default is :inc).
;;; When setting the ref count must be done before calling :send-message.
;;;
;;;   :eliza  
;;;   ------
;;; Forced call to ELIZA. We expect an answer from the master. 
;;;
;;;   :execute-preconditions  
;;;   ----------------------
;;; Escape mechanism to execute Lisp code directly in the =execute method, e.g. 
;;; to set flags. 
;;;
;;;   :question | :question-no-erase
;;;   ------------------------------
;;; The associated value is either a string or a list of strings. When 
;;; we have a list of strings, one is chosen randomly for printing. uses =display-text
;;; Once the question is asked, the user is supposed to answer. The answer is
;;; inserted into the INPUT area of the FACTS slot in the conversation object 
;;; that is the link between the external system and MOSS.
;;; The question is processed by the =resume method.
;;;
;;;   :reset-conversation
;;;   -------------------
;;; Restarts the main conversation, cleaning all values of the conversaiton oject.
;;;
;;;   :send-message args
;;;   ------------------
;;; Send a message to other agents. Args are those for an OMAS message. An answer
;;; is expected. A timeout may be specified.
;;;
;;;   :send-message-no-wait args
;;;   --------------------------
;;; Send a message to other PAs. Since the message is similar to an e-mail, we do
;;; not wait for the answer in the dialog (not in the process).
;;;
;;;   :text {:no-erase}
;;;   -----------------
;;; Print text e.g. prior to asking a question. Uses =display-text
;;;
;;;   :text-exec expr
;;;   ---------------
;;; If expr is a list starting with format, execute it before printing it. Uses
;;; =display-text
;;;
;;;   :wait
;;;   -----
;;; Force the dialog crawler to wait for an input even if no question was put to
;;; the master by letting the =execute method return a :wait tag
;;;
;;; The =resume method
;;; ==================
;;; Called after the =resume method. The user data are in the :INPUT area of the 
;;; current conversation, or in different field of the FACTS slot of the 
;;; conversation object.
;;; Options are the following.
;;;
;;; Whenever the input contains ":quit :abort :exit :reset :cancel" a transition
;;; to the global _Q-abort state occurs.
;;;
;;;   :answer-analysis
;;;   ----------------
;;; Indicates that a special method named =answer-analysis will be applied to
;;; the data (INPUT). The method must return a transition state or
;;; the :abort keyword, in which case the conversation is locally restarted.
;;; All other options are ignored.
;;;
;;;   :execute function args
;;;   --------
;;; Executes the corresponding FUNCTION and args
;;;
;;;   :transitions
;;;   ------------
;;; transitions are clauses (transition rules) with an IF part, an INTERMEDIATE part
;;; and a TRANSITION part, each with options.
;;; IF part options of a transition clause:
;;;
;;; :always
;;;    always execute this clause, any clause following is ignored
;;; :contains <list of words>
;;;    test if the input contains one of the words
;;; :empty 
;;;    test if the input is nil (not very useful?)
;;; :no 
;;;    test if the input is a negation (does not work for Asian languages)
;;; :on-failure
;;;    tests if the returned value from the master or from the agents was a failure
;;;    by checking if FACTS/INPUT is (:failure) 
;;; :otherwise
;;;    always fire (should be the last option)
;;; :patterns ({<pattern> {<action>}+}+)
;;;    test each pattern against the content of INPUT, if it matches, executes 
;;;    actions
;;; :rules ({{<pattern>}{<answer>}*}* {<action>}+}+)
;;;    tries to apply a set of rules. If one applies, then computes an answer ELIZA
;;;    style, should use :display-result to display it
;;; :starts-with 
;;;    test if the answer starts with a word (use rather patterns)
;;; :test expr
;;;    fires if the test applies (expr evaluates to non nil)
;;;    when expr returns nil the clause is skipped
;;; :yes 
;;;    test if the answer is yes (does not work with Asian languages)
;;;
;;; THEN options for intermediate actions (before a transition):
;;; :display-answer
;;;    send the content of the FACTS/ANSWER area to the output channel for
;;;    printing; calls display-answer that uses =display-text that uses the 
;;;    display-text method of the output channel (window)
;;; :display-result
;;;    displays the sentence resulting from applying the set of rules to the input;
;;;    uses =display-text directly
;;; :exec {sexpr}
;;;    executes the sexpr in the context of =resume
;;; :format-answer format-function
;;;    formats the content of the FACTS/ANSWER area applying the format-function to
;;;    produce a string, uses then the =display-text method. It is the responsibility
;;;    of the format-function to produce HTML strings when the answer must be shippec
;;;    to the web server. format-answer takes 2 arguments: the first one is the answer
;;;    the second one if t indicates that the result needs to be an HTML string
;;; :format-answer-full format-function
;;;    like :format-answer but accepts conversation as a third argument, allowing to
;;;    let format-function to have access to the FACTS area of conversation
;;; :keep
;;;    used with patterns, saves the value of the pattern variable into FACTS/INPUT 
;;; :print-answer print-function
;;;    prints the content of the FACTS/ANSWER area applying the print funtion
;;; :replace {value}
;;;    replaces the content of INPUT with the specified value (word list)
;;; :set-performative {:request|:command|:assert}
;;;    set the FACTS/PERFORMATIVE area (canbe used to check if we are waiting for an
;;;    answer rather than a request
;;;
;;; THEN options for transitions:
;;; :failure
;;;    exit from state with :failure marker, anything following that is ignored 
;;; :reset
;;;    restarts the local (sub-)conversation, incompatible with anything else
;;; :sub-dialog dialog-header
;;;    specifies a sub-dialog
;;;    :success state : transfer state in case of success
;;;    :failure state : transfer state in case of failure
;;; :success
;;;    exit from state with success marker, anything following that is ignored
;;; :target state
;;;    specifies the transition state
;;; Default is to restart the conversation at the beginning.

;;;-------------------------------------------------------------------- DEFSTATE

;;; the result of defstate can be checked by setting moss::*debug* to t. In that
;;; case executing defstate only prints the resulting =ECECUTE and =RESUME 
;;; methods and does not build any object

(defMacro defstate (state-name &rest arg-list)
  "macro to produce the code for buiding a conversation state.
Arguments:
   state-name: name of a global variable pointing to the state object
   arg-list: rest of the arguments for defining the state (the syntax is complex)."
  `(apply #'make-state ',state-name ',arg-list))

#|
? (setq moss::*debug* t)
T
Should be executed from the :ALBERT package

? (defstate 
  _pt-brush
  (:label "bookkeeping")
  (:explanation "do some light bookkeeping.")
  (:exec (setf (has-details conversation) nil))
  (:immediate-transitions
   (:always :success))
  (:no-resume)
  )

;***** Debug: STATE: _PT-BRUSH

;*** Debug: state: _PT-BRUSH =EXECUTE 
   (MOSS::%MAKE-OWNMETHOD
     =EXECUTE
     _PT-BRUSH
     (MOSS::CONVERSATION &REST MOSS::MORE-ARGS)
     "*see explanation attribute of the _PT-BRUSH state*"
     (DECLARE (IGNORE MOSS::MORE-ARGS))
     (let* ((moss::data (MOSS::GET-DATA MOSS::CONVERSATION))
            (moss::context (car (HAS-Q-CONTEXT MOSS::CONVERSATION)))
            (moss::results (SEND MOSS::CONTEXT '=GET 'HAS-RESULTS))
            moss::return-value
            moss::bindings)
       (MOSS::NOOP MOSS::BINDINGS MOSS::DATA MOSS::RESULTS)
       (setf (HAS-PERFORMATIVE MOSS::CONTEXT) NIL)
       (if (intersection MOSS::*ABORT-COMMANDS* MOSS::DATA :TEST #'STRING-EQUAL)
           (throw :DIALOG-ERROR NIL))
       (catch :RETURN
         (setf (HAS-DETAILS CONVERSATION) NIL)
         (cond (T (throw :RETURN (setq MOSS::RETURN-VALUE (list :SUCCESS)))))
         (setq MOSS::RETURN-VALUE '(:RESUME)))
       MOSS::RETURN-VALUE))
ALBERT:_PT-BRUSH
|#

;;;--------------------------------------------------------- DEFSIMPLE-SUBDIALOG

;;; This macro is provided for simplifying the writing of very simple dialogs 
;;; when the conversation graph contains a single node and we want to send a 
;;; message to another agent to get some data or information tthat we are going
;;; to print. 
;;; The macro produces the declarations of the global variables, the subdialog 
;;; headers, the state, and the =execute and =resume methods.
;;; E.g.
;;;  (defsimple-subdialog "get-financing" "_gfi"
;;;     :explanation "Master is trying to obtain info about a financing program."
;;;     :from PA_HDSRI :to :FINANCING :action :get-financing
;;;     :language :fr
;;;     :pattern ("financement" ("pays")("titre")("date limite") ("URL"))		 
;;;     :sorry "- D�sol�, je ne trouve pas le programme de financement demand�."
;;;     :print-fcn #'print-financing
;;;     )
;;; This macro call builds a graph for sending a message to the FINANCING agent
;;; with action :get-financing, pattern for the answer, a message to print in
;;; case of failure and a function to print the results in case of success. The
;;; data arguments are read from the conversation :input slot.
;;; Default printing function is display-answer

(defMacro defsimple-subdialog (label prefix &key explanation from to action
                                     language pattern format-fcn print-fcn sorry)
  (declare (ignore print-fcn))
  (let ((conversation-tag 
         (intern  ; could use make-symbol ?
          (string-upcase (concatenate 'string "_" label "-conversation"))))
        (entry-state-tag
         (intern 
          (string-upcase (concatenate 'string prefix "-entry-state"))))
        (sorry-tag
         (intern 
          (string-upcase (concatenate 'string prefix "-sorry"))))
        )
    `(progn
       (eval-when (:compile-toplevel :load-toplevel :execute)
         (proclaim '(special ,conversation-tag ,entry-state-tag ,sorry-tag)))
       
       (defsubdialog 
           ,conversation-tag
           (:label ,(format nil "~A dialog" label))
         (:explanation ,explanation))
       
       (defstate
           ,entry-state-tag
           (:label ,(format nil "~A dialog - entry state" label))
         (:entry-state ,conversation-tag)
         (:explanation 
          ,(format nil "Assistant is sending a free-style message to the ~A agent."
             to))
         (:send-message :to ,to :self ,from :action ,action
                        :args `(((:data . ,(get-fact :input))
                                 (:language . ,,language)
                                 ,@,(if pattern `'((:pattern . ,pattern)))
                                 )))
         (:transitions
          (:on-failure :target ,sorry-tag)
          (:otherwise
           ,@(if format-fcn `(:format-answer ,format-fcn) '(:display-answer))
           :success)))
       
       (defstate
           ,sorry-tag
           (:label ,(format nil "~A failure." label))
         (:explanation "We can't get the info.")
         (:reset)
         (:text ,sorry)
         (:transitions (:failure)))
       )))

#|
CG-USER(1): (moss::defsimple-subdialog "get-financing" "_gfi"
     :explanation "Master is trying to obtain info about a financing program."
     :from PA_HDSRI :to :FINANCING :action :get-financing
     :language :fr
     :pattern ("financement" ("pays")("titre")("date limite") ("URL"))		 
     :sorry "- D�sol�, je ne trouve pas le programme de financement demand�."
     :format-fcn #'format-financing
     )

(PROGN (EVAL-WHEN (:COMPILE-TOPLEVEL :LOAD-TOPLEVEL :EXECUTE)
         (PROCLAIM '(SPECIAL _GET-FINANCING-CONVERSATION _GFI-ENTRY-STATE _GFI-SORRY)))
  (DEFSUBDIALOG _GET-FINANCING-CONVERSATION (:LABEL "get-financing dialog")
    (:EXPLANATION "Master is trying to obtain info about a financing program."))
  (DEFSTATE _GFI-ENTRY-STATE (:LABEL "get-financing dialog - entry state")
    (:ENTRY-STATE _GET-FINANCING-CONVERSATION)
    (:EXPLANATION
     "Assistant is sending a free-style message to the FINANCING agent.")
    (:SEND-MESSAGE :TO :FINANCING :SELF PA_HDSRI :ACTION :GET-FINANCING :ARGS
                   (EXCL::BQ-LIST (EXCL::BQ-LIST* (EXCL::BQ-CONS `:DATA
                                                                 (READ-FACT
                                                                  MOSS::CONVERSATION
                                                                  :INPUT))
                                                  (EXCL::BQ-CONS `:LANGUAGE :FR)
                                                  '((:PATTERN "financement" ("pays") ("titre")
                                                              ("date limite") ("URL"))))))
    (:TRANSITIONS (:ON-FAILURE :TARGET _GFI-SORRY)
                  (:OTHERWISE :PRINT-ANSWER #'PRINT-FINANCING :SUCCESS)))
  (DEFSTATE _GFI-SORRY (:LABEL "get-financing failure.")
    (:EXPLANATION "We can't get the info.") (:RESET)
    (:TEXT "- D�sol�, je ne trouve pas le programme de financement demand�.")
    (:TRANSITIONS (:FAILURE))))
|#
;;;---------------------------------------------------------------- DEFSUBDIALOG

(defMacro defsubdialog (dialog-name &rest option-list)
  "defines a piece of dialog that will be used in larger dialogs. A sub-dialog  ~
   can be called as a subroutine in a programming environment. It is in charge ~
   of finding a specific information. 
   The sub-dialog  gets data in the HAS-DATA slot if the context object and ~
   returns answer in the :RESULTS area of the FACTS slot Its has a ~
   header, an entry state, a success exit state and failure exit state.
Arguments:
   dialog-name: a symbol that will be used to make various names
   option-list: a list of options
    action: action to be executed
    label: title of the subdialog
    sates: list of state variables of the dialog
    explanation: text
Return:
   the identifier of the dialog header."
  (let* ((action (cadr (assoc :action option-list)))
         (label (cadr (assoc :label option-list)))
         (explanation (cadr (assoc :explanation option-list)))
         (states (cdr (assoc :states option-list)))
         )
    `(progn
       (declaim (special ,dialog-name ,@states))
       (definstance 
           MOSS-DIALOG-HEADER
           (HAS-MOSS-LABEL ,label)
         (HAS-MOSS-EXPLANATION ,explanation)
         ,@(if action `((HAS-MOSS-ACTION ,action))) ; points towards the subclass
         (:var ,dialog-name)
         )
       ,dialog-name)))

#|
(defsubdialog _explain-conversation 
    (:label "Explain conversation")
  (:states  _exp-entry-state)
  (:explanation "The data is searched for a concept to be explained.")
  )
Expansion

(PROGN (DECLAIM (SPECIAL _EXPLAIN-CONVERSATION _EXP-ENTRY-STATE))
  (DEFINSTANCE MOSS-DIALOG-HEADER (HAS-MOSS-LABEL "Explain conversation")
    (HAS-MOSS-EXPLANATION "The data is searched for a concept to be explained.")
    (:VAR _EXPLAIN-CONVERSATION))
  _EXPLAIN-CONVERSATION)
|#
;;;=============================================================================
;;;                             =EXECUTE METHOD
;;;=============================================================================

;;;---------------------------------------------- MAKE-STATE-EXECUTE-METHOD-BODY

(defun make-state-execute-method-body (arg-list &aux body)
  (macrolet ((+append (arg) `(setq body (append body ,arg))))   
    (dolist (item arg-list body)
      (+append 
       (case (car item)
         (:execute-preconditions (make-state-execute-preconditions (cdr item)))
         (:clear-all-facts (make-state-clear-all-facts))
         (:clear-fact (apply #'make-state-clear-fact (cdr item)))
         (:clear-facts (apply #'make-state-clear-facts (cdr item)))
		 (:delay-answer (apply #'make-state-delay-answer (cdr item)))
         (:eliza (make-state-eliza t))
         (:reset-conversation (make-state-reset-conversation t))
         (:text (apply #'make-state-text (cdr item)))
         (:text-exec (apply #'make-state-text-exec (cdr item)))
         (:question (make-state-question (cdr item)))
         (:question-no-erase (make-state-question-no-erase (cdr item)))
         #+OMAS (:send-message (make-state-send-message (cdr item)))
         #+OMAS (:send-message-no-wait (make-state-send-message (cdr item)))
         (:answer-type (make-state-set-answer-type (cdr item)))
         )))))
#|
(make-state-execute-method-body
 '((:question "Is ELIZA here?")))
((SEND CONVERSATION '=DISPLAY-TEXT "Is ELIZA here?" :ERASE T))

(make-state-execute-method-body
 '((:execute-preconditions
    (setq flag t)
    (replace moss::conversation :item t))
   (:question "Is ELIZA here?")
   ))
((SETQ MOSS::FLAG T) (REPLACE MOSS::CONVERSATION :ITEM T)
 (SEND MOSS::CONVERSATION '=DISPLAY-TEXT '"Is ELIZA here?" :ERASE T))

(make-state-execute-method-body
  '((:send-message :to :ANATOLE-NEWS :self PA_ANATOLE :action :add-category
                 :args 
                 `(((:data . ,(mln::make-mln `((:fr ,(get-fact :label))
                                               (:en ,(get-fact :english-label)))))
                    (:language . :FR))))))
|#
;;;--------------------------------------------------- MAKE-STATE-EXECUTE-METHOD

(defUn make-state-execute-method (state-name arg-list)
  "builds the =execute own method for a STATE. only prints the result ~
   if *debug* is true.
Arguments:
   state-name: e.g. _pt-clean
   arg-list: the arguments to the defstate macro
Return:
   the name of the method"
  (declare (special *debug*))
  ;; when *debug* is true simply prints the method
  ((lambda (body)
     (if *debug*
         (format *debug-io* "~2&;*** Debug: state: ~S =EXECUTE ~&   ~S"
           state-name
           (list* '%make-ownmethod (intern "=EXECUTE")
                  state-name
                  '(conversation &rest more-args)
                  (if (%occurs-in? 'conversation body)
                      (cons (car body)
                            (cons
                             '(declare (ignore more-args))
                             (cdr body)))
                    (cons (car body)
                          (cons 
                           '(declare (ignorable conversation more-args)) 
                           (cdr body))))))
       (%make-ownmethod 
        (intern "=EXECUTE") state-name '(conversation &rest more-args)
        (if (%occurs-in? 'conversation body)
            (cons (car body)
                  (cons
                   '(declare (ignore more-args))
                   (cdr body)))
          (cons (car body)
                (cons 
                 '(declare (ignorable conversation more-args)) 
                 (cdr body)))))))
   ;; body
   (let ((options (make-state-execute-method-body arg-list)))
     `(,(format nil "*see explanation attribute of the ~s state*" state-name)
         ;(declare (ignore more-args))
         ;; the only options are :question and :question-no-erase
         ,@options
         ;; the return value is (:wait) or (:resume)
         ,(cond
           ;; if no-wait is forced, then obey
           ((assoc :no-wait arg-list) ''(:resume))
           ;; if options contain a question or send-message, then wait
           ((make-state-question-there? arg-list) ''(:wait))
           ;; in all other cases don't wait
           (t ''(:resume)))
         ;; the return value is a (:wait) or (:resume)
         ;,(if (make-state-question-there? arg-list) ''(:wait) ''(:resume))
         ))))



#|
(setq moss::*debug* t)
(make-state-execute-method '_test-state '((:question "What is it?")))
;*** Debug: state: _TEST-STATE =EXECUTE 
(%MAKE-OWNMETHOD =EXECUTE _TEST-STATE (CONVERSATION &REST MORE-ARGS) 
                 "*see explanation attribute of the _TEST-STATE state*"
                 (DECLARE (IGNORE MORE-ARGS)) 
                 (SEND CONVERSATION '=DISPLAY-TEXT '("What is it?") :ERASE T) 
                 '(:WAIT))
(make-state-execute-method '_test-state '((:reset-conversation)
                                          (:question "What is it?")))
|#
;;;=============================================================================
;;;                             =RESUME METHOD
;;;=============================================================================

;;;---------------------------------------------------- MAKE-STATE-RESUME-METHOD

(defUn make-state-resume-method 
    (state-name arg-list &aux (*local-resume-vars* '(return-value)))
  "builds the =RESUME own method for a given state. If *debug* is true, then ~
      simply prints it.
Arguments:
   state-name: e.g. _pt-clean
   arg-list: the list of arguments of the defstate macro
Returns:
   the name of the method, NIL when printing."
  (declare (special *debug* *local-resume-vars*))
  ;; when *debug* is true simply prints the method
  ((lambda (body)
     (if *debug*
         (format *debug-io* "~2&;*** Debug: state: ~S =RESUME ~&   ~S"
           state-name
           (list* '%make-ownmethod (intern "=RESUME") state-name
                  '(conversation)
                  (if (%occurs-in? 'conversation body)
                      body
                    (cons (car body)
                          (cons 
                           '(declare (ignorable conversation)) 
                           (cdr body))))))
       (%make-ownmethod 
        (intern "=RESUME") state-name '(conversation)
        (if (or (%occurs-in? 'conversation body)
                (%occurs-in? 'get-fact body)
                (%occurs-in? 'copy-fact body)
                (%occurs-in? 'set-fact body))
            body
          `(,(car body) (declare (ignore conversation)) . ,(cdr body))
          ))))
   ;; body of the method 
   (list
    (format nil "*see explanation attribute of the ~s state*" state-name)
    ;; data to analyze is in the :INPUT area of FACTS
    (let* ((resume-body (make-state-resume-method-body state-name arg-list)))
      `(let* ,*local-resume-vars* 
         (declare (ignorable bindings))
         (catch :return
                ,@resume-body)
         return-value)))))

#|
(make-state-resume-method '_test '((:transitions(:patterns '(("OK")) :target _OK))))
;*** Debug: state: _TEST =RESUME 
(%MAKE-OWNMETHOD
 =RESUME
 _TEST
 (CONVERSATION)
 "*see explanation attribute of the _TEST state*"
 (LET* (BINDINGS RETURN-VALUE)
   (CATCH :RETURN
          (COND ((SETQ BINDINGS (PROCESS-PATTERNS INPUT ''(("OK"))))
                 (SETQ RETURN-VALUE (LIST :TRANSITION _OK)))
                (T
                 (THROW :DIALOG-ERROR
                   (FORMAT NIL "bad transition syntax in state ~S" _TEST)))))
   RETURN-VALUE))
NIL
|#
;;;----------------------------------------------- MAKE-STATE-RESUME-METHOD-BODY

(defUn make-state-resume-method-body (state-name arg-list)
  "build the body of the =resume method."  
  (cond 
   ;; if we need a complex detailed analysis we call =answer-analysis
   ((assoc :answer-analysis arg-list )
    (make-state-answer-analysis 
     (or (cdr (assoc :answer-analysis arg-list)) :input) state-name))
   ;; otherwise we do a simple analysis
   (t
    `(;; eventually execute a simple expression (escape mechanism)
      ,@(getv :execute arg-list)
         ;; now deal with the transitions
         ,(let ((transitions (getv :transitions arg-list)))
            `(cond
              ;; when input contains a reset indicator, we quit
              ;; this is only necessary when we have an input from master
              ,@(if (make-state-question-there? arg-list)
                    '(((intersection *abort-commands* (read-fact conversation :input)
                                     :test #'string-equal)
                       ;; ... and restart dialog
                       (throw :dialog-error nil))))
              ;; add a line for each option of the  transitions
              ,@(mapcar #'make-state-resume-transitions transitions)
              ;; close all transitions with a safeguard clause
              ;; i.e., if we did not find a transition, we restart the dialog
              ,@(unless (or (assoc :otherwise transitions)
                            (assoc :always transitions)
                            (assoc :eliza arg-list))
                  `((t (throw :dialog-error 
                         (format nil "bad transition syntax in state ~S"
                           ,state-name)))))
              ))))))

#|
(make-state-resume-method-body '_test 
                               '((:transitions
                                  (:patterns 
                                   ((?* ?x) "Albert")
                                   :set-performative (list :request)
                                   :target _q-find-task))))
((COND ((SETQ BINDINGS (PROCESS-PATTERNS INPUT '((?* ?X) "Albert")))
        (REPLACE-FACT CONVERSATION :PERFORMATIVE (LIST :REQUEST))
        (SETQ RETURN-VALUE (LIST :TRANSITION _Q-FIND-TASK)))
       (T
        (THROW :DIALOG-ERROR
          (FORMAT NIL "bad transition syntax in state ~S" _TEST)))))

(make-state-resume-method-body '_test '((:eliza)))
((LET ((ELIZA-RULES (INTERN "*ELIZA-RULES*" *PACKAGE*)))
   (UNLESS (BOUNDP ELIZA-RULES) (SETQ ELIZA-RULES '*ELIZA-RULES*))
   (SEND CONVERSATION
         '=DISPLAY-TEXT
         (MOSS-ELIZA DATA (SYMBOL-VALUE ELIZA-RULES))
         :ERASE
         T)))
|#
;;;----------------------------------------------- MAKE-STATE-RESUME-TRANSITIONS

(defUn make-state-resume-transitions (arg-list)
  "input format: (:contains (...) :action {:clean, :add} :target new-state
                  :reset t)
Argument:
   arg-list: an alternate list containing keyword options"
  (declare (special *local-resume-vars*))
  (let ((target (cadr (member :target arg-list))))
    (remove 
     nil 
     (append 
      ;;--- start of the clause (conditional part)
      (if (member :contains arg-list)
          `((intersection ',(cadr (member :contains arg-list)) 
                          (read-fact conversation :input)
                          :test #'string-equal)))
      (if (member :empty arg-list)
          `((null (read-fact conversation :input))))
      (if (member :no arg-list)
          `((intersection *no-answers* (read-fact conversation :input)
                          :test #'string-equal)))
      (if (member :on-failure arg-list)
          `((is-answer-failure? (car (read-fact conversation :input)))))
      (if (member :starts-with arg-list)
          `((member (car (read-fact conversation :input)) 
                    ',(cadr (member :starts-with arg-list))
                    :test #'string-equal)))
      (when (member :patterns arg-list)
        ;; declare local variable
        (pushnew 'bindings *local-resume-vars*)
        ;; we got a set of patterns ELIZA style. Apply process-pattern to each one
        `((setq bindings 
                (process-patterns (read-fact conversation :input)
                                  ',(cadr (member :patterns arg-list))))
          ;(noop bindings) ; to keep the compiler quiet when bindings is not used
          ))
      (when (member :rules arg-list)
        ;; declare a local variable
        (pushnew 'result *local-resume-vars*)        
        ;; fires if applying the rules yields a non nil answer
        `((setq result (moss-eliza (read-fact conversation :input) 
                                   ',(getf arg-list :rules))))
        ;; display the answer
        )
      (if (member :test arg-list)
          `(,(cadr (member :test arg-list))))
      (if (member :yes arg-list)
          `((intersection *yes-answers* (read-fact conversation :input)
                          :test #'string-equal)))
      (if (or (member :otherwise arg-list)
              (member :always arg-list))
          '(t))
      
      ;;--- then deal with the rest of condition clause
      
      ;;--- intermediate actions
      ;; only to be used in a patterns clause
      (if (member :keep arg-list)
          ;`((replace-fact conversation :input
          `((replace-fact conversation ',(getf arg-list :keep)
                          (cdr (assoc ',(getf arg-list :keep) bindings)))))
      ;; replace INPUT with whatever is associated to :replace
      (if (member :replace arg-list)
          `((replace-fact conversation :input
                          ',(cadr (member :replace arg-list)))))
      (if (member :set-performative arg-list)
          `((replace-fact conversation :PERFORMATIVE 
                          ,(cadr (member :set-performative arg-list)))))
      ;; usually for bookkeeping (must occur after :keep before printing)
      (if (member :exec arg-list)
          `(,(cadr (member :exec arg-list))))
      ;; default function to print using the conversation output channel
	  ;; should appear after :exec in order to let :exec set up :answer for example
      (if (member :display-answer arg-list)
          `((if (web-active? conversation)
                (web-add-text conversation (get-fact :answer))
              (send conversation '=display-text 
                    (format nil "~A" (get-fact :answer)))))) ; JPB1702
      ; we could do without the display-answer function, e.g.
      ;`((send conversation '=display-text 
      ;        (format nil "~S" (get-fact :answer))))
      ;`((display-answer conversation))) ; JPB1011		  
		
      ;; should replace all printing options. Applies the formatting function that
      ;; produces a string, then uses =display-text to output data, taking care of
      ;; web servers if the WEB tag (marker) is set
      (if (member :format-answer arg-list)
          `((let ((text (funcall ,(cadr (member :format-answer arg-list))
                                 (read-fact conversation :answer)
                                 (web-active? conversation))))
              (vformat "~%; ++++format-answer option/ :web ~S~% :answer ~S~% text: ~S"
                (read-fact conversation :web)
                (read-fact conversation :answer)
                       text)
              ;; may not be useful, since =display-text checks the same situation
              (if (web-active? conversation)
                  (web-add-text conversation text)
                (send conversation '=display-text text)))))
      ;; like :format-answer but give access to the converation object so that we
      ;; can pass more arguments to the printing function
      (if (member :format-answer-full arg-list)
          `((let ((text (funcall ,(cadr (member :format-answer-full arg-list))
                                 (read-fact conversation :answer)
                                 (web-active? conversation)
                                 conversation))) ; added arg JPB 1602
              (vformat "~%; ++++format-answer-full args: :web ~S~% :answer ~S~
                        ~%conversation: ~S ~% text: ~S"
                       (read-fact conversation :web)
                       (read-fact conversation :answer)
                       conversation
                       text)
              ;; may not be useful, since =display-text checks the same situation
              (if (web-active? conversation)
                  (web-add-text conversation text)
                (send conversation '=display-text text)))))
      ;`((send conversation '=display-text
      ;     (funcall ,(cadr (member :format-answer arg-list))
      ;	            (read-fact conversation :answer)
      ;			   (read-fact conversation :web)))))  ; JPB1301
      ;; to be used after sending a message to other agents
      ;; kept for backward compatibility, does not apply to web exchanges
      (if (member :print-answer arg-list)
          `((funcall ,(cadr (member :print-answer arg-list))
                     (read-fact conversation :answer)))) ; JPB1010
      ;; to be used in conjunction with a :rules clause
      ;; don't see the purpose of this function
      (if (member :display-result arg-list)
          `((if (web-active? conversation)
                (web-add-text conversation result)
              (send conversation '=display-text result
                    ,@(if (member :erase arg-list) '(:erase t))))))
      ;`((send conversation '=display-text result
      ;       ,@(if (member :erase arg-list) '(:erase t)))))
      
      ;;--- transitions
      (if (and (member :failure arg-list) (not (member :sub-dialog arg-list)))
          `((throw :return (setq return-value (list :failure)))))
      (if (member :reset arg-list)
          ;; reset conversation
          `((restart-conversation conversation)
            ;; we must return the entry state of the dialog
            (setq return-value
                  (list :transition 
                        (car (HAS-MOSS-ENTRY-STATE 
                              (car (HAS-MOSS-DIALOG-HEADER conversation)))))))
        )
      (if (member :sub-dialog arg-list)
          `((throw :return 
              (setq return-value
                    '(:sub-dialog ,@(cdr (member :sub-dialog arg-list)))))))
      (if (and (member :success arg-list) (not (member :sub-dialog arg-list)))
          `((throw :return (setq return-value (list :success)))))
      (if (member :target arg-list)
          `((setq return-value (list :transition ,target))))
      ))))

#|
(make-state-resume-transitions '(:contains ("get" "obtain" "give" ":get") 
                                           :clean-data (input data)
                                           :target _Q-get-info))
((INTERSECTION '("get" "obtain" "give" ":get") (READ-FACT CONVERSATION :INPUT) 
               :TEST #'STRING-EQUAL)
 (SETQ RETURN-VALUE (LIST :TRANSITION _Q-GET-INFO)))

(make-state-resume-transitions '(:test (has-to-do conversation)
                                       :self =resume))
((HAS-TO-DO CONVERSATION) (SEND *SELF* '=RESUME CONVERSATION))
(make-state-resume-transitions '(:PATTERNS  (("get" "obtain" "give" ":get"))
                                           :set-performative (list :request)
                                           :target _Q-get-info))
|#
;;;=============================================================================
;;;                          BUILDING THE STATE
;;;=============================================================================

;;;------------------------------------------------------------------ MAKE-STATE

(defUn make-state (state-name &rest arg-list)
  "builds three things:
   - an instance of MOSS-STATE
   - an =execute method for this state
   - a =resume method for this state
   When *debug* is t then only prints info, does not build the corresponding objects.
Arguments:
   state-name: e.g. _pt-clean
   arg-list (rest): options to the defstate macro
Returns:
   the name of the state."
  (eval-when (:compile-toplevel :load-toplevel :execute)
    (proclaim `(special ,state-name))
    (export (list state-name)))
  
  ;; when debugging print header
  (if *debug* (format *debug-io* "~&;***** Debug: STATE: ~S" state-name))
  
  ;; made a MOSS instance of a MOSS-STATE, initialize it
  (unless *debug*
    (set state-name
         (%make-instance  
          'MOSS-state 
          `(has-moss-label ,(cadr (assoc :label arg-list)))
          ;; if no explanation, then repeat the label to have something to print
          `(has-moss-explanation ,(or (cadr (assoc :explanation arg-list))
                                      (cadr (assoc :label arg-list)))) ; JPB1607
          ))
    
    ;(break "MOSS::make-state: before checking entry-state")  
    
    ;; when an entry state, link it to the conversation header
    (when (assoc :entry-state arg-list)
      ;(trformat "make-state /conversation header: ~S ; entry state of: ~S"
      ;          (eval (cadr (assoc :entry-state arg-list))) (list (eval state-name)))
      (send (eval (cadr (assoc :entry-state arg-list))) 
            '=replace 'HAS-MOSS-ENTRY-STATE
            (list (eval state-name))))
    
    ;(break "MOSS::make-state: after checking entry-state")     
    
    ;; ???
    (when (assoc :process-state arg-list)
      ;(trformat "make-state /conversation header: ~S ; process state of: ~S"
      ;          (eval (cadr (assoc :process-state arg-list))) (list (eval state-name)))
      (send (eval (cadr (assoc :process-state arg-list))) '=replace 'HAS-MOSS-PROCESS-STATE
            (list (eval state-name))))
    )
  
  ;; synthetizing the =execute method (should always exist)
  (unless (assoc :manual-execute arg-list)
    (make-state-execute-method state-name arg-list))
  
  (make-state-resume-method state-name arg-list)
  ;(trformat "make-state /state: ~S value: ~S" state-name (eval state-name))
  state-name)
  
  #|
(in-package :moss)
(in-package :stevens)
(in-package :albert)
*package*
|#
;;;=============================================================================
;;;            FUNCTIONS FOR CONSTRUCTING PIECES OF STATE METHODS
;;;=============================================================================

;;;----------------------------------------------------------- MAKE-STATE-ACTION

#|
(defun make-state-action (action)
  "action contains the name of a method and arguments for *self*"
  (when action
    `((send *self* ,@action))))
|#

;;;----------------------------------------------------------- MAKE-STATE-ANSWER

(defUn make-state-answer (answer &optional no-erase)
  "if answer print it"
  (when answer
    `((send conversation '=display-text  ',answer 
            ,@(unless no-erase '(:erase t))))))
#|
(make-state-answer "youppee..." :no-erase)
((SEND CONVERSATION '=DISPLAY-TEXT "youppee..."))
(make-state-answer '("encore" "youppee..."))
((SEND CONVERSATION '=DISPLAY-TEXT '("encore" "youppee...") :ERASE T))
(moss::make-state '_test-state 
                  '(:label "Test state")
                  '(:explanation "no explanation")
                  '(:reset-conversation)
                  '(:answer "youppee..."))
|#
;;;-------------------------------------------------- MAKE-STATE-ANSWER-ANALYSIS

(defUn make-state-answer-analysis (tag state-name)
  "if we need a complex detailed analysis we call =answer-analysis"
  `((send *self* (intern "=ANSWER-ANALYSIS" *package*) ; defined in agent's package
          conversation (read-fact conversation ,tag))
    ;; the returned answer should be :abort or a transition state
    (if (eql *answer* :abort)
        (throw :dialog-error 
          (format nil "abort from special analysis in state ~S" ,state-name))
      (throw :return (setq return-value *answer*)))))

;;;-------------------------------------------------------------- MAKE-STATE-ASK

(defUn make-state-ask (ask prompt)
  "ask user"
  (declare (ignore prompt))
  (when ask
    ;; do not forget that welcome can contain arguments
    `((send conversation '=display-text (format nil ,@ask))
      ;; make answer panel active
      '(:resume)
      )))

;;;--------------------------------------------------------- MAKE-STATE-ASK-TCW

#|
(defun make-state-ask-tcw (ask-tcw)
  "ask user for choice"
  (when ask-tcw
    ;; asking text-choice-why question, put answer into com bin
    `((ask-tcw conversation ,@ask-tcw)
      (setq return-value '(:resume)))))
|#

;;;--------------------------------------------------------- MAKE-STATE-ASK-TSW

#|
(defun make-state-ask-tsw (ask-tsw)
  "ask user for multiple choice"
  (when ask-tsw
    ;; asking text-select-why question, put answer into com bin
    `((setf (has-to-do conversation) 
        (list (ask-tsw conversation ,@ask-tsw)))
      (setq return-value '(:resume)))))
|#
;;;-------------------------------------------------- MAKE-STATE-CLEAR-ALL-FACTS

(defun make-state-clear-all-facts ()
  "clears the FACT base in the conversation object"
  `((%%set-value conversation '$FCT nil (symbol-value (intern "*CONTEXT*")))))

;;;------------------------------------------------------- MAKE-STATE-CLEAR-FACT

(defun make-state-clear-fact (item)
  "resets a specific item in the conversation FACT base"
  (when item
    `((replace-fact conversation ',item nil))))

;;;------------------------------------------------------ MAKE-STATE-CLEAR-FACTS

(defun make-state-clear-facts (&rest item-list)
  "resets several items in the conversation fact base"
  (when item-list
    `((mapc #'(lambda (xx) (replace-fact conversation xx nil)) ',item-list))))
	
;;;----------------------------------------------------- MAKE-STATE-DELAY-ANSWER

(defun make-state-delay-answer (&optional (item :inc))
   "handles the delay-ref-count parameter in conversation/FACTS"
 (case item
   (:inc '((web-set-delay conversation)))
   (:dec '((web-dec-delay conversation)))
   (:clear '((web-clear-delay conversation)))))

;;;------------------------------------------------------------ MAKE-STATE-ELIZA

(defUn make-state-eliza (ok)
  "switches to an ELIZA dialog, using the rules found in the execution package."
  (when OK
    `(      
      ;(format *debug-io* "~&+++ make-state-eliza; package: ~S" *package*)
      ;; check if the application has a set of ELIZA rules
      (let ((eliza-rules (intern "*ELIZA-RULES*" *package*)))
        ;; if there are no local rules, use the MOSS set
        (unless (boundp eliza-rules)
          (setq eliza-rules 'moss::*eliza-rules*))
        ;(trformat "ELIZA /data: ~S" data)
        (send conversation '=display-text 
              (moss-eliza (read-fact conversation :input)
                          (symbol-value eliza-rules)) :erase t)
        ;(format *debug-io* "~&+++ make-state-eliza; package: ~S" *package*)
        ))))

#|
(make-state-eliza t)
((LET ((MOSS::ELIZA-RULES (INTERN "*ELIZA-RULES*" *PACKAGE*)))
   (UNLESS (BOUNDP MOSS::ELIZA-RULES) (SETQ MOSS::ELIZA-RULES 'MOSS::*ELIZA-RULES*))
   (SEND MOSS::CONVERSATION
         '=DISPLAY-TEXT
         (MOSS::MOSS-ELIZA (READ-FACT MOSS::CONVERSATION :INPUT)
                           (SYMBOL-VALUE MOSS::ELIZA-RULES))
         :ERASE
         T)))
|#
;;;---------------------------------------------------- MAKE-STATE-EXECUTE-ARGS?

(defUn make-state-execute-args? (arg-list)
  "execute a single expression in the =execute method and proceed."
  ;; return the list of arguments 
  (or (assoc :text arg-list)
      (assoc :question arg-list)
      (assoc :question-no-erase arg-list)
      (assoc :reset-conversation arg-list)))

;;;-------------------------------------------- MAKE-STATE-EXECUTE-PRECONDITIONS

(defUn make-state-execute-preconditions (arg-list)
  "execute Lisp expression in the =execute method (escape mechanism)."
  arg-list)

;;;--------------------------------------------------------- MAKE-STATE-QUESTION
;;; adapted to web JPB1301
;;;********** seems to be wrong when arg is a single value...

(defUn make-state-question (question)
  "if question and web save it otherwise ask it, erasing-screen
Arguments:
   question : a string or list of strings"
  (when question
    `((if (web-active? conversation)
	      (web-add-text conversation ',@question)
		(send conversation '=display-text ',@question :erase t)))))
		
#|
(defUn make-state-question (question)
  "if question ask it, erasing-screen
Arguments:
   question : a string or list of strings
   no-erase (opt): if t does not erase assistant pane"
  (when question
    `((send conversation '=display-text ',@question :erase t))
    ))
|#
#|
(make-state-question '("Hello?" "How are you?"))
((SEND CONVERSATION '=DISPLAY-TEXT '("Hello?" "how are you?")))
(make-state-question "Hello?")
((SEND CONVERSATION '=DISPLAY-TEXT '"Hello?" :ERASE T))
|#
;;;------------------------------------------------ MAKE-STATE-QUESTION-NO-ERASE

(defUn make-state-question-no-erase (question)
  "if question and web save it, otherwise ask it but do not erase screen.
Arguments:
   question : a string or list of strings"
  (when question
    `((if (web-active? conversation)
	     (web-add-text conversation ',@question)
	   (send conversation '=display-text ',@question)))))

#|
(make-state-question-no-erase "Autre chose?")
((IF (GET-FACT :WEB)
     (SET-FACT :QUESTION (QUOTE . "Autre chose?"))
   (SEND CONVERSATION '=DISPLAY-TEXT (QUOTE . "Autre chose?"))))
|#
;;;-------------------------------------------------- MAKE-STATE-QUESTION-THERE?

(defUn make-state-question-there? (arg-list)
  "test if the arg-list contains a question.
Arguments:
   arg-list: options of the defstate macro."
  (or
   (assoc :question arg-list)
   (assoc :question-no-erase arg-list)
   (assoc :text-exec arg-list)  ; JPB 1409
   (assoc :eliza arg-list)
   (assoc :send-message arg-list)
   ;; if we ask the state to wait for an external input
   (assoc :wait arg-list)
   ))

;;;----------------------------------------------- MAKE-STATE-RESET-CONVERSATION

(defUn make-state-reset-conversation (ok)
  "reset does several things:
     - clean the FACTS
     - puts a new goal (action pattern) into the GOAL slot of conversation"
  (when OK
    `((let ((action (car (HAS-MOSS-ACTION
                          (car (HAS-MOSS-DIALOG-HEADER conversation))))))
        ;; reset facts
        (setf (HAS-MOSS-FACTS conversation) nil)
        ;; remove task and task list
        (send conversation '=delete-all-successors 'HAS-MOSS-TASK)
        (send conversation '=delete-all-successors 'HAS-MOSS-TASK-LIST)
        ;; set a new goal specified by the type of action in the dialog header
        (if action
            (send conversation '=replace 'HAS-MOSS-GOAL
                  (list (send action '=create-pattern)))
          ;; otherwise should delete everything
          (progn
            (send conversation '=get 'HAS-MOSS-GOAL)
            ;; seems to indicate that we can have several goals at the same time...
            (if *answer*
                (send conversation '=delete-sp 'HAS-MOSS-GOAL *answer*)))
          )))))

;;;----------------------------------------------------- MAKE-STATE-SEND-MESSAGE
;;; When sending a message, the dialog can be run from the master window or from
;;; a browser page. In the first case the answer of the message should be a string
;;; or a list of strings, in the second case the whole process was started by a
;;; get-answer function and the result should be a list with the ref to the object
;;; that was modified (to be able to refresh the web page) and a list of messages.
;;; The precise situation is indicated by the $WEBT property of the conversation
;;; object, and is tested by the web-active? service function. We thus add to the
;;; message arguments a new parameter :pa-dialog that is true if we have a dialog
;;; from the master window and false if the dialog is run from the web.

(defUn make-state-send-message (send-message)
  "creates an internal message for sending a request to other agents (staff?) ~
   the content of the internal message is part of a message free-style with ~
   the content of INPUT area of the fact base."
  (when send-message
    `((let* ((agent-id (car (HAS-MOSS-AGENT conversation)))
             (from (or ,(cadr (member :from send-message))
                       (omas::key agent-id)))
             (to ,(or (cadr (member :to send-message)) :ALL))
             (action ,(or (cadr (member :action send-message)) :explain))
             (type ,(or (cadr (member :type send-message)) :request))
             (data ,(cadr (member :args send-message)))
             (pa-dialog (not (web-active? conversation)))
             (args   ; adding a tag to track sending context
                   (list (cons (cons :pa-dialog pa-dialog) (car data)))) ; JPB0902, JPB1409
             (agent-key (omas::key agent-id))
             (timeout ,(cadr (member :timeout send-message)))
             (strategy ,(cadr (member :strategy send-message)))
             (protocol ,(cadr (member :protocol send-message)))
             (repeat-count ,(or (cadr (member :repeat-count send-message)) 0))
             message content)
        
        ;; make a list to be given as a task description list in the internal 
        ;; :send skill
        (setq content
              (append (list :from from :to to :action action :args args
                            :type type)
                      (if protocol (list :protocol protocol))
                      (if strategy (list :strategy strategy))
                      ;(if timeout (list :timeout timeout))
                      (if repeat-count (list :repeat-count repeat-count))
                      ))
        ;; we must create a task-id in case we have a request, so that the system
        ;; knows where to send the answer
        (setq message
              (omas::message-make 
               :date (get-universal-time) 
               :type :internal 
               :from agent-key :to agent-key
               :action :send-message
               :task-id (omas::create-task-id) 
               :timeout (or timeout 1000000) ; JPB2002 or most-positive-fixnum?
               :args content))
        (moss::vformat "message: ~S" message)
        ;; save it into the pending-task-id slot of the master bins
        ;; this should not be necessary (only for trace)
        ;(omas::assistant-display-pending-requests agent)
        ;; put message directly into agenda; if it is put into the mailbox, since the
        ;; action is :send-message it is transferred to the agenda readily; thus,
        ;; this saves a delay
        (omas::agent-add agent-id message :agenda)
        ;; the answer to an internal message is posted into the
        ;; to-do slot of the agent as a string. This wakes up the
        ;; =resume method that finds the answer in the corresponding
        ;; slot of the conversation object
        ))))

#|
(MAKE-STATE-SEND-MESSAGE
         '(:TO :ANATOLE-NEWS :SELF PA_ANATOLE :ACTION :ADD-CATEGORY :ARGS
               `(((:DATA . "test") (:LANGUAGE . :FR)))))
((LET* ((AGENT-ID (CAR (HAS-MOSS-AGENT CONVERSATION)))
        (FROM (OR NIL (OMAS::KEY AGENT-ID)))
        (TO :ANATOLE-NEWS)
        (ACTION :ADD-CATEGORY)
        (TYPE :REQUEST)
        (DATA `(((:DATA . "test") (:LANGUAGE . :FR))))
        (PA-DIALOG (NOT (WEB-ACTIVE? CONVERSATION)))
        (ARGS (LIST (CONS (CONS :PA-DIALOG PA-DIALOG)) (CAR DATA))))
   (AGENT-KEY (OMAS::KEY AGENT-ID))
   (TIMEOUT NIL)
   (STRATEGY NIL)
   (PROTOCOL NIL)
   (REPEAT-COUNT 0)
   MESSAGE
   CONTENT)
 (SETQ CONTENT
       (APPEND (LIST :FROM FROM :TO TO :ACTION ACTION :ARGS ARGS :TYPE TYPE)
               (IF PROTOCOL (LIST :PROTOCOL PROTOCOL)) (IF STRATEGY (LIST :STRATEGY STRATEGY))
               (IF TIMEOUT (LIST :TIMEOUT TIMEOUT)) (IF REPEAT-COUNT (LIST :REPEAT-COUNT REPEAT-COUNT))))
 (SETQ MESSAGE
       (OMAS::MESSAGE-MAKE :DATE (GET-UNIVERSAL-TIME) :TYPE :INTERNAL :FROM AGENT-KEY :TO AGENT-KEY
                           :ACTION :SEND-MESSAGE :TASK-ID (OMAS::CREATE-TASK-ID) :ARGS CONTENT))
 (VFORMAT "message: ~S" MESSAGE) (OMAS::AGENT-ADD AGENT-ID MESSAGE :AGENDA))
|#
;;;-------------------------------------------------- MAKE-STATE-SET-ANSWER-TYPE

(defun make-state-set-answer-type (answer-type)
  "puts the specified performatives into the fact-base"
  (when answer-type 
    ;(trformat "make-state-set-performative /answer-type: ~S" answer-type)
    `((replace-fact conversation :answer-type ',answer-type)))
  )

;;;------------------------------------------------- MAKE-STATE-SET-PERFORMATIVE

(defUn make-state-set-performative (answer-type)
  "puts the specified performatives into the state context object"
  (when answer-type 
    ;(trformat "make-state-set-performative /answer-type: ~S" answer-type)
    `((replace-fact conversation :performative) ',answer-type))
  )

;;;------------------------------------------- MAKE-STATE-PROCESS-SUBDIALOG-ARGS

(defUn make-state-process-subdialog-args (arg-list)
  (cond
   ((null arg-list) nil)
   ((not (member (car arg-list) '(:failure :success))) nil)
   ((eql (car arg-list) :failure)
    (append (list :failure (eval (cadr arg-list)))
            (make-state-process-subdialog-args (cddr arg-list))))
   ((eql (car arg-list) :success)
    (append (list :success (eval (cadr arg-list)))
            (make-state-process-subdialog-args (cddr arg-list))))))
#|
? (make-state-process-subdialog-args '(:failure _q-more?))
(:FAILURE $QSTE.3)
? (make-state-process-subdialog-args '(:failure _q-more? :success _q-process))
(:FAILURE $QSTE.3 :SUCCESS $QSTE.4)
|#
;;;------------------------------------------------------------- MAKE-STATE-TEXT
;;; adapting to web exchange JPB1301

(defUn make-state-text (text &optional no-erase)
  "if text and web save it otherwise print it"
  (when text
    `((if (web-active? conversation)
	     (web-add-text conversation ',text) 
	   (send conversation '=display-text  ',text  ; JPB1104 removing quote
            ,@(if no-erase '(:erase nil) '(:erase t)))))))

#|
(make-state-text "tout va bien")
((IF (GET-FACT :WEB)
     (SET-FACT :TEXT '"tout va bien")
   (SEND CONVERSATION '=DISPLAY-TEXT '"tout va bien" :ERASE T)))
|#
#|
(defUn make-state-text (text &optional no-erase)
  "if text print it"
  (when text
    `((send conversation '=display-text  ',text  ; JPB1104 removing quote
            ,@(if no-erase '(:erase nil) '(:erase t))))))
|#
;;;-------------------------------------------------------- MAKE-STATE-TEXT-EXEC
;;; adapting to web exchange JPB1301

(defUn make-state-text-exec (text &optional no-erase)
  "if text print it"
  (when text
    `((if (web-active? conversation)
	     (web-add-text conversation ,text)
	   (send conversation '=display-text  ,text 
            ,@(if no-erase '(:erase nil) '(:erase t)))))))

#|
(make-state-text-exec '(format nil "~S" "Bonjour"))
((IF (GET-FACT :WEB)
     (SET-FACT :TEXT (FORMAT NIL "~S" "Bonjour"))
   (SEND CONVERSATION '=DISPLAY-TEXT (FORMAT NIL "~S" "Bonjoue") :ERASE T)))
|#
#|
(defUn make-state-text-exec (text &optional no-erase)
  "if text print it"
  (when text
    `((send conversation '=display-text  ,text 
            ,@(if no-erase '(:erase nil) '(:erase t))))))
|#
;;;------------------------------------------------------------- MAKE-STATE-WAIT

(defUn make-state-wait ()
  "ask OMAS to wait for mater input"
  `((:wait)))

;;;=============================================================================
;;; we make 2 activate-answer-panel function according the the presence of OMAS
;;; to keep things legible

;;;------------------------------------------------------- ACTIVATE-ANSWER-PANEL

#-OMAS
(defUn activate-answer-panel (conversation &key read-parents (prompt "-"))
  "activates the answer panel specified in the conversation object ~
   (when the panel is t, listener, reads the input as a list). Reads in the ~
    data and puts the result into the FACT/RAW-INPUT slot of the conversation ~
    object.
Argument:
   conversation: conversation object
   read-parents (key): if t, then read parentheses, otherwise replace by spaces
   prompt (key): prompt default is - (for lisener only)
Return:
   text"
  (unless conversation
    (warn "Can't activate answer panel if no conversation is active.")
    (return-from activate-answer-panel nil))
  
  (vformat "~%; entering activate-answer-panel - FACTS:~%~{~&   ~S~}" 
           (has-MOSS-facts conversation))
  
  (let ((win (car (HAS-MOSS-INPUT-WINDOW conversation)))
        text)
    
    (unless win
      (warn "Can't activate answer panel if no input channel is specified.")
      (return-from activate-answer-panel nil)
      )
    ;(moss::trformat "activate-answer-panel /and wait for something in the TO-DO ~
    ;                 conversation slot.")
    
    (when (eq win t)
      ;; we input from the Lisp listener
      (dformat :dialog 0 "~&~A " prompt)
      (setq text (read-from-listener read-parents))
      (replace-fact conversation :RAW-INPUT text)
      (return-from activate-answer-panel text))
    
    ;;=== Here we have a reference to a window
    ;; it should have an activate-input method
    ;(trformat "activate-answer-panel")
    ;; activate the user panel
    (activate-input win)
    
    ;; when we are executing MOSS alone, then the answer goes into the user-input
    ;; of the MOSS window
    
    ;; we wait on the content of the USER-INPUT slot of the MOSS window
    (vformat "activate-answer-panel /conversation: ~S~
                   ~&; window: ~S" conversation win)
    
    (process-wait "waiting for master input" #'user-input win)
    ;(break "activate-answer-panel")
    ;; transfer value into conversation slot for further processing
    (replace-fact conversation :RAW-INPUT (user-input win))
    ;; reset input buffer
    (setf (user-input win) nil)
    ;; after input, data are in the FACTS/RAW-INPUT slot of MOSS::conversation
    
    ;(dformat :dialog 1 "~&...echo from read: ~S saved into: ~S" 
    ;        text (has-to-do conversation))
    text))


;;;(defUn activate-answer-panel (conversation)
;;;  "activates the answer panel specified in the conversation object ~
;;;   (when the panel is t, listener, reads the input as a list). Reads in the ~
;;;    data and puts the result into the FACT/RAW-INPUT slot of the conversation ~
;;;    object.
;;;Argument:
;;;   conversation: conversation object
;;;   prompt (key): prompt default is - (for lisener only)
;;;Return:
;;;   text"
;;;  (unless conversation
;;;    (warn "Can't activate answer panel if no conversation is active.")
;;;    (return-from activate-answer-panel nil))
;;;  
;;;  (format t "~2%;----------------------------------------------------------------")
;;;  (format t "~%;                     activate-answer-panel")
;;;  (format t "~%;----------------------------------------------------------------")
;;;  (format t "~%;--- web-active?: ~S" (web-active? conversation))
;;;  (format t "~%;--- gate: ~S" (web-get-gate conversation))
;;;  (format t "~%;--- text ($WOUT): ~S" (web-get-text conversation))
;;;  ;(vformat "~3%;---------- entering activate-answer-panel")
;;;  ;(vformat " FACTS:~%~{~&   ~S~}" (has-MOSS-facts conversation))
;;;  ;(vformat " activate-answer-panel/ web-active?: ~S ~%  gate: ~S~
;;;  ;         ~%  text (from conversation slot $WOUT): ~S" 
;;;  ;         (web-active? conversation)(web-get-gate conversation)
;;;  ;         ;; reads the text to output to the web from $WOUT
;;;  ;         (web-get-text conversation))
;;;  
;;;  (let ((agent (car (%get-value conversation '$AGT)))
;;;        (win (car (HAS-MOSS-INPUT-WINDOW conversation)))
;;;        text gate)
;;;    
;;;    (unless agent
;;;      (error "activate-answer-panel: Agent is missing in the conversation object.")
;;;      (return-from activate-answer-panel nil))
;;;    
;;;    ;;=== First test the case where the agent has no window
;;;    ;; we cannot activate the window. However, if we are in a turn initiated from
;;;    ;; the web server, we must send back the answer and prompt, and wait for the
;;;    ;; next request.
;;;    ;; The problem is that we could have sent a request to another agent and are
;;;    ;; going to wait for an answer, in which case we do not want to send anything
;;;    ;; back to the web but we want to keep accumulating data into the text area.
;;;    ;; One question is to determine when we are ready to send info back. This should
;;;    ;; be indicated by the dialog (anytime we want to ask a question ?) I.e, if 
;;;    ;; we return a result, then we go back to the top level in the more? state, 
;;;    ;; which will print something and issue a wait.
;;;    ;; We must be careful since the first time around when we start the dialog
;;;    ;; no web request has been done and we do not have a process waiting for it,
;;;    ;; which can be tested by using web-active?
;;;    ;; also in order to answer we must have a gate
;;;    (when (omas::no-window agent)
;;;      ;; IF 1) web is active, 2) there is a gate 3) we have some text,
;;;      ;;  THEN open the gate, meaning that the waiting Web process will get the
;;;      ;;       text and post it
;;;      (when (and (web-active? conversation)
;;;                 (web-get-gate conversation)
;;;                 ;; let's try that to distinguish with waiting for the return of 
;;;                 ;; an external message. reads from $WOUT in conversation object
;;;                 (web-get-text conversation))
;;;        ;; added jpb1407. 
;;;		(cond
;;;		  ;; If :delay-ref-count is >0 (i.e. non nil), then we do not want to send
;;;		  ;; the answer back right away. So we simply decrease the ref count
;;;		  ((web-get-delay conversation)
;;;		   (format t "~%;--- decreasing delay reference count. Answer delayed.")
;;;		   (web-dec-delay conversation))
;;;		  ;; otherwise we open the gate to send answer back to web page
;;;		  (t
;;;        (setq gate (web-get-gate conversation))
;;;        ;; forget gate info
;;;        (web-clear-gate conversation)
;;;        ;(vformat "activate-answer-panel/ opening gate: ~S to resume web process."
;;;        ;         gate)
;;;        ;; careful, here gate is an index, not the vector reference to the gate
;;;        ;(omas::web-open-gate gate)))
;;;        (format t "~%;--- opening gate to allow posting the text into web page.")
;;;        ;; kludge: gates sets by MOSS are vectors, gates sets by OMAS are numbers!
;;;        (if (numberp gate)
;;;            (omas::web-open-gate gate)
;;;          (mp:open-gate gate))))
;;;		))
;;;    
;;;    ;;=== Now the case where we should have a window
;;;    (unless (omas::no-window agent) ; no-window is NIL, thus agent has a window
;;;      ;; we should have a window
;;;      (unless win
;;;        (error "activate-answer-panel/ agent should have an output window."))
;;;      ;; it should have an activate-input method
;;;      (activate-input win))
;;;    
;;;    ;;=== now set up the waiting loop
;;;    ;; when we have an input from an asssistant agent, we wait on the TO-DO slot
;;;    ;; of the agent structure
;;;    ;; when the input comes from the interface window, it is inserted into the
;;;    ;; TO-DO slot directly. In this case it may be a string, or a :failure mark
;;;    ;; when a timeout has been reached and no answer is available. The default
;;;    ;; timeout is for all practical purposes infinite (most-positive-fixnum)
;;;    ;; and is contained in the assistant ANSWER-TIMEOUT slot (not available for an
;;;    ;; SA or XA).
;;;    ;; when the input comes from an other agent, then it is inserted into the 
;;;    ;; ANSWER slot of the agent and a mark :answer-there is inserted into the
;;;    ;; TO-DO slot of the agent.
;;;    
;;;    ;; wait for a new input into the TO-DO agent slot (default is indefinitely)
;;;    (unless
;;;        (mp:process-wait-with-timeout "waiting for master or agent input"
;;;                                      (omas::answer-timeout agent)
;;;                                      #'omas::to-do agent)
;;;      ;; if nil means that we had a timeout, initialize to-do slot
;;;      (setf (omas::to-do agent) :failure)
;;;      (setf (omas::error-contents agent) "Timeout: No answer")
;;;      )
;;;    
;;;    (vformat "activate-answer-panel /~%  (to-do agent): ~S~%  (answer agent): ~S"
;;;             (omas::to-do agent) (omas::answer agent))
;;;    
;;;    ;; check for failure should be done when processing the answer, not here
;;;    
;;;    ;;=== reorganize FACTS according to type of input
;;;    (cond
;;;     ;; if the answer is a failure (is-answer-failure?) put "*failure*" into the
;;;     ;; FACTS/raw-input area (?)
;;;     ((is-answer-failure? (omas::to-do agent) (omas::answer agent))
;;;      (replace-fact conversation :raw-input :failure)
;;;      (replace-fact conversation :answer (list "*failure*"))
;;;      ;; maybe the reason for failure is provided...if so, record it
;;;      (replace-fact conversation :reason (omas::error-contents agent)))
;;;     ;; if the slot contains :answer-there then the answer is to be found
;;;     ;; in the answer slot of the agent (answer from other agents)
;;;     ((equal (omas::to-do agent) :answer-there)
;;;      ;; copy into raw-input
;;;      (replace-fact conversation :raw-input (omas::answer agent))
;;;      ;; next line is not necessary since the crawler will put "*see-answer*"
;;;      ;; into the FACT/INPUT area and copy the FACTS/RAW-INPUT into 
;;;      ;; FACTS/ANSWER
;;;      ;; we keep it just in case
;;;      (replace-fact conversation :answer (omas::answer agent))
;;;      )
;;;     ;; otherwise transfer to the FACTS/RAW-INPUT of the conversation object
;;;     (t (replace-fact conversation :raw-input (omas::to-do agent))
;;;        ;; copy answer or else should clean FACTS/ANSWER
;;;        (replace-fact conversation :answer nil)))
;;;    
;;;    ;; reset the waiting trigger
;;;    (setf (omas::to-do agent) nil)
;;;    ;; clean input JPB 1006
;;;    (setf (omas::answer agent) nil) ; as a safety measure
;;;    ))
#+OMAS

(defUn activate-answer-panel (conversation &aux ag)
  "activates the answer panel specified in the conversation object ~
   (when the panel is t, listener, reads the input as a list). Reads in the ~
    data and puts the result into the FACT/RAW-INPUT slot of the conversation ~
    object.
Argument:
   conversation: conversation object
   prompt (key): prompt default is - (for lisener only)
Return:
   text"
  (unless conversation
    (warn "Can't activate answer panel if no conversation is active.")
    (return-from activate-answer-panel nil))
  
  (dformat :dialog 0
            "~2%;----------------------------------------------------------------")
  (dformat :dialog 0
           "~%; ~A                    activate-answer-panel"
           (if (setq ag (car (send conversation '=get "moss-agent")))
               (omas::key ag)))
  (dformat :dialog 0
            "~%;----------------------------------------------------------------")
  (dformat :dialog 0 "~%;--- web-active?: ~S" (web-active? conversation))
  (dformat :dialog 0 "~%;--- delay ref count: ~S" (web-get-delay conversation))
  (dformat :dialog 0 "~%;--- input: ~S" (read-fact conversation :input))
  (dformat :dialog 0 "~%;--- gate: ~S" (web-get-gate conversation))
  (dformat :dialog 0 "~%;--- text ($WOUT): ~S" (web-get-text conversation))
  (dformat :dialog 0 "~%;--- answer: ~S" (read-fact conversation :answer))
  
  (let ((agent (car (%get-value conversation '$AGT)))
        (win (car (HAS-MOSS-INPUT-WINDOW conversation)))
         gate)
    
    (unless agent
      (error "activate-answer-panel: Agent is missing in the conversation object.")
      (return-from activate-answer-panel nil))
    
    ;(break "activate-answer-panel, web-active?: ~S" (web-active? conversation))
    
    ;;=== First test the case where the agent has no window
    ;; we cannot activate the window. However, if we are in a turn initiated from
    ;; the web server, we must send back the answer and prompt, and wait for the
    ;; next request.
    ;; The problem is that we could have sent a request to another agent and are
    ;; going to wait for an answer, in which case we do not want to send anything
    ;; back to the web but we want to keep accumulating data into the text area.
    ;; If the delay ref count is non nil, then we do not open the gate to send
    ;; the answer back but simply decrease the ref count.
    
    ;; we distinguish answers from master from returned data by testing the
    ;; to-do slot for ""*see-answer*" mark
    
    ;; We must be careful since the first time around when we start the dialog
    ;; no web request has been done and we do not have a process waiting for it,
    ;; which can be tested by using web-active?
    ;; also in order to answer we must have a gate
    (when (omas::no-window agent) ; e.g. the case when using a web interface
      
      (cond
       ;; if 1) web is active, 2) we have an answer from another agent, 3) the
       ;;   delay ref count is non nil, then we decrease ref count and add the 
       ;;   answer to the waiting text if it is a string.
       ((and (web-active? conversation)
             (web-get-delay conversation)
             (equal+ (car (read-fact conversation :input)) "*see-answer*"))
        (dformat :dialog 0
                  "~%;--- decreasing delay reference count. Answer delayed.")
        ;; add answer to whatever text we had in $WOUT (conversion to string OK)
        (web-add-text conversation 
                      (format nil "~{~A~^ <B> ~}" (read-fact conversation :answer)))
        (web-dec-delay conversation)
        )
       
       ;; IF 1) web is active, 2) there is a gate 3) we have some text,
       ;;  THEN open the gate, meaning that the waiting Web process will get the
       ;;    text and post it
       ((and (web-active? conversation)
             (web-get-gate conversation)
             ;; let's try that to distinguish with waiting for the return of 
             ;; an external message. reads from $WOUT in conversation object
             (web-get-text conversation))
        (setq gate (web-get-gate conversation))
        ;; forget gate info
        (web-clear-gate conversation)
        ;; clear delay ref count 
        (web-clear-delay conversation)
        ;(vformat "activate-answer-panel/ opening gate: ~S to resume web process."
        ;         gate)
        ;; careful, here gate is an index, not the vector reference to the gate
        ;(omas::web-open-gate gate)))
        (dformat :dialog 0
                  "~%;--- opening gate to allow posting the text into web page.")
        ;; kludge: gates sets by MOSS are vectors, gates sets by OMAS are numbers!
        (if (numberp gate)
            (omas::web-open-gate gate)
          (mp:open-gate gate))
        )
       ) ; en cond
      ) ; end :no-window clause
    
    ;;=== Now the case where we should have a window
    (unless (omas::no-window agent) ; no-window is NIL, thus agent has a window
      ;; we should have a window
      (unless win
        (error "activate-answer-panel/ agent should have an output window."))
      ;; it should have an activate-input method
      (activate-input win))
    
    ;;=== now set up the waiting loop
    ;; when we have an input from an asssistant agent, we wait on the TO-DO slot
    ;; of the agent structure
    ;; when the input comes from the interface window, it is inserted into the
    ;; TO-DO slot directly. In this case it may be a string, or a :failure mark
    ;; when a timeout has been reached and no answer is available. The default
    ;; timeout is for all practical purposes infinite (most-positive-fixnum)
    ;; and is contained in the assistant ANSWER-TIMEOUT slot (not available for an
    ;; SA or XA).
    ;; when the input comes from an other agent, then it is inserted into the 
    ;; ANSWER slot of the agent and a mark :answer-there is inserted into the
    ;; TO-DO slot of the agent.
    
    ;; wait for a new input into the TO-DO agent slot (default is indefinitely)
    (unless
        (mp:process-wait-with-timeout "waiting for master or agent input"
                                      (omas::answer-timeout agent)
                                      #'omas::to-do agent)
      ;; if nil means that we had a timeout, initialize to-do slot
      (setf (omas::to-do agent) :failure)
      (setf (omas::error-contents agent) "Timeout: No answer")
      )
    
    (break "activate-answer-panel (to-do agent): ~S" (omas::to-do agent))
    
    (vformat "activate-answer-panel /~%  (to-do agent): ~S~%  (answer agent): ~S"
             (omas::to-do agent) (omas::answer agent))
    
    ;; check for failure should be done when processing the answer, not here
    
    ;;=== reorganize FACTS according to type of input
    (cond
     ;; if the answer is a failure (is-answer-failure?) put "*failure*" into the
     ;; FACTS/raw-input area (?)
     ((is-answer-failure? (omas::to-do agent) (omas::answer agent))
      (replace-fact conversation :raw-input :failure)
      (replace-fact conversation :answer (list "*failure*"))
      ;; maybe the reason for failure is provided...if so, record it
      (replace-fact conversation :reason (omas::error-contents agent)))
     
     ;; if the slot contains :answer-there then the answer is to be found
     ;; in the answer slot of the agent (answer from other agents)
     ((equal (omas::to-do agent) :answer-there)
      ;; copy into raw-input
      (replace-fact conversation :raw-input (omas::answer agent))
      ;; next line is not necessary since the crawler will put "*see-answer*"
      ;; into the FACT/INPUT area and copy the FACTS/RAW-INPUT into 
      ;; FACTS/ANSWER
      ;; we keep it just in case
      (replace-fact conversation :answer (omas::answer agent))
      )
     
     ;; otherwise transfer to the FACTS/RAW-INPUT of the conversation object
     (t 
      (replace-fact conversation :raw-input (omas::to-do agent))
      ;; copy answer or else should clean FACTS/ANSWER
      (replace-fact conversation :answer nil)))
    
    ;; reset the waiting trigger
    (setf (omas::to-do agent) nil)
    ;; clean input JPB 1006
    (setf (omas::answer agent) nil) ; as a safety measure
    ))

;;;--------------------------------------------------------------------- ASK-TCW

#|
(defun ask-tcw (conversation &rest question)
  "we process a TCW (text, choice, why) question by simply printing the choices ~
      in the agent-output panel, and activate user input.
Arguments:
  conversation: conversation
  question: (rest) list containing question to be asked to the user.
Return:
   nil if the choice was canceled, a list containing a single selection otherwise."
  ;(print (list "question:" question))
  (let ((choice-list (cadr (member :choice-list question)))
        (title (cadr (member :title question)))
        (nn 0))
    ;; display the choices. Explanations are printed only if why? is asked
    ;;... does not clean the window
    ;(trformat "ask-tcw/ conversation: ~S" conversation)
    (send 
     conversation '=display-text
     (format nil "~%~A (give number; 0 selects everything): ~{~{~& ~2D. ~A~}~}"  
       title  
       (mapcar #'(lambda(xx) (list (incf nn) (car xx))) choice-list)))
    ;; activate master's input
    ;(activate-answer-panel conversation :prompt (cadr (member :prompt question)))
    ;; exiting means that the answer is returned to the caller
    ;; returns a list as it were a user's answer
    :done))
|#

#|
(moss::ask-tcw '$CVSE.1 :title "Please choose one"
               :choice-list '(("premier" $DOC.1)
                              ("deuxi�me" $DOC.2))
               :why "You must select one.")
|#
;;;--------------------------------------------------------------------- ASK-TSW

(defUn ask-tsw (conversation &rest question)
  "obsolete. Dialogs should take care of that feature. noop."
  (declare (ignore conversation question))
  nil)

;;;  "we received a TSW (text, select, why) question. We  show it to the user ~
;;;      immediately for selection of the items in the list. TSW questions occur in ~
;;;      dialogs, when an conversation is trying to get a list of items approved by 
;;;      the sender.
;;;Arguments:
;;;  conversation: conversation
;;;  environment: ignored
;;;  question: list containing question to be asked to the user.
;;;Return:
;;;   nil or a list of choices."
;;;  (declare (ignore environment))
;;;  ;(print (list "question:" question))
;;;  (let (why choice-list choice)
;;;    ;; get list describing the question
;;;    (setq choice-list (cadr (member :choice-list question))
;;;          why (cadr (member :why question)))
;;;    (print (list "choice-list:" choice-list))
;;;    ;; show the explanation in the text window of the master, if any
;;;    ;; it corresponds to the text following the :why keyword
;;;    ;;... but first clean the window
;;;    ;(omas::assistant-reset-window conversation)
;;;    (when why (display-text conversation why))
;;;    ;; display a floating window for the choices
;;;    (setq choice (catch-cancel
;;;                   (select-item-from-list 
;;;                    (mapcar #'car choice-list)
;;;                    :window-title (cadr (member :title question))
;;;                    :selection-type :disjoint)))
;;;    (print choice)
;;;    ;; print the choices in the standard window so that we record the dialog
;;;    (format t "~&~A : ~{~&   - ~A~}"  
;;;            (cadr (member :title question))  
;;;            (mapcar #'car choice-list))
;;;    (cond 
;;;     ;; return nil if canceled
;;;     ((eq choice :cancel) 
;;;      ;; print a trace for dialog records
;;;      (format t "~&<*cancelled*>")
;;;      nil)
;;;     ;; get the necessary data from the :choice option of the question
;;;     (t 
;;;      ;; print a trace for recording dialogs
;;;      (format t "~&? <choice: ~{~A~^ ; ~}>" choice)
;;;      (setq choice 
;;;            (mapcar #'(lambda (xx) (cadr (assoc xx choice-list :test #'equal)))
;;;                    choice))))
;;;    ;; put the selected choice into the assistant window text area (user input)
;;;    (display-text conversation "~%OK.")
;;;    ;; exiting means that the answer is put into the to-do slot to resume conversation
;;;    choice))

;;;------------------------------------------------------ CHECK-AGAINST-ONTOLOGY

(defUn check-against-ontology (conversation data)
  "look into the sentence for words belonging to the ontology. Return them.
Arguments:
   conversation: conversation
   data: list o be tested."
  (declare (ignore conversation))
  (remove nil (mapcar #'(lambda (xx)(if (%pdm? xx) xx)) data)))

;;;-------------------------------------------------------------- CLEAN-PATTERNS

(defUn clean-patterns (patterns used-patterns)
  "removes patterns in patterns that belong to used-patterns.
Arguments:
   patterns: list of patterns to clean
   used-patterns: patterns that have been tested already
Return:
   a list of patterns to be tested."
  (let (result)
    (dolist (item patterns)
      (unless (assoc (car item) used-patterns :test #'string=)
        (push item result)))
    (reverse result)))

;;;------------------------------------------------------ CLEAR-ASSISTANT-OUTPUT

(defUn clear-assistant-output (conversation)
  "Clears the assistant output pane of the com-window. If conversation refers to ~
   the standard listener (t), then does nothing.
Argument:
   conversation: conversation object`
Return:
   t"
  (send conversation '=display-text "" :header " " :erase t)
  t)

;;;------------------------------------------------------ DETERMINE-PERFORMATIVE

(defUn determine-performative (conversation &key data)
  "tries to determine the performative corresponding to the data input of the ~
   current conversation.
Arguments:
   conversation: current conversation
   data (key): a list of string words, if present conversation is ignored
Return:
   one of the keywords :request :assert :command"
  (setq data (or data (read-fact conversation :input)))
  (cond
   ((process-patterns data *assert-patterns*) :assert)
   ((process-patterns data *request-patterns*) :request)
   ((process-patterns data *command-patterns*) :command)
   (t :answer)))

#|
? (moss::determine-performative 
   '$CVSE.1
   :data '("how" "do" "you" "do" "today" "?"))
:REQUEST
? (moss::determine-performative 
   '$CVSE
   :data '("please" "note" "this" "fact"))
:ASSERT
? (moss::determine-performative 
   '$CVSE
   :data '("Dominique" "s" "address"))
:ANSWER
? (moss::determine-performative 
   '$CVSE
   :data '("send" "a " "mail" "to" "John" "now"))
:COMMAND
|#
;;;--------------------------------------------------------------- DIALOG-ENGINE

(defUn dialog-engine (conversation state)
  "global control of the conversation loop. Catches the fatal conversation ~
   errors by restarting the process.
Arguments:
   conversation: a conversation object
   state: a specified initial state
Return:
   does not return."
  (unless (%type? conversation '$CVSE)
    (error "unknown conversation object ~S" conversation))
  ;; launch dialog, restarting completely in case of error (the assistant gets lost)
  
  ;(ignore-errors
   (catch :dialog-error
         (dialog-state-crawler conversation :state state))
   ;)
  (send conversation '=display-text
         "***** Dialog Programming error, I restart dialog *****" :color cg:red) 
  ;; reset conversation object
  (restart-conversation conversation)
  )

#|
(step (moss::restart-conversation 'stevens::$CVSE.1))
|#
;;;-------------------------------------------------------- DIALOG-STATE-CRAWLER
;;; the idea is to print the results together with the next prompt. This can be
;;; done when calling activate-answer-panel. However, the function is also called
;;; when we send a message and the =resume method returns a :wait tag, which
;;; leads to awaking the sending process before the actual answer has been 
;;; obtained.
;;; Therefore, returning something to the user should be distinguished from 
;;; waiting from an answer from a message.
;;; This is difficult since the master can get impatient and do some input prior
;;; to the return of the message (or when there is no answer).
;;; One way to distinguish the cases is when there is nothing to return in the
;;; FACTS/answer-to-print or FACTS/question-to-print

;;;(defUn dialog-state-crawler (conversation &key state)
;;;  "works as follows:
;;;   - start from the initial state of the conversation header or a specific state
;;;   - send an =execute method to the state
;;;   - makes a transition according to what is returned by the =execute method
;;;      - if :resume send a =resume method to the current state
;;;      - if :success or :failure (subdialogs) make the correponding transition
;;;   - after a =resume method
;;;      - if :success or :failure (subdialogs) return the corresponding state
;;;      - if a state object makes the transition
;;;Arguments:
;;;   conversation: a conversation object
;;;   state (key): a specified state of the conversation
;;;Return:
;;;   when exiting from a sub-dialog, returns a state, e.g. :failure or _xx-state."
;;;  ;; check type of first argument
;;;  (unless (%type? conversation '$CVSE)
;;;    (error "unknown conversation object ~S" conversation))
;;;  ;; fetch dialog header from the conversation object and initial state from the
;;;  ;; dialog header
;;;  (let* ((dialog-header (car (%get-value conversation '$DHDRS)))
;;;         (initial-state (car (%get-value dialog-header '$ESTS)))
;;;         (count 0)  ; will count the number of transitions
;;;         old-state result action arg-list raw-input old-header)
;;;    (declare (ignore success failure))
;;;    ;; check state if not given, start with initial state
;;;    (unless state
;;;      (setq state initial-state))
;;;    ;; trace what we are doing if verbose set
;;;    (vformat "=================================================================~2%~
;;;              Entering dialog-state-crawler ~S/state: ~S; package: ~S"
;;;             count state *package*)
;;;    
;;;    (loop
;;;      ;; at the beginning of the loop the state variable may have several distinct
;;;      ;; values: 
;;;      ;;  - a state id (normal case)
;;;      ;;  - :failure or :success when returning from a sub-dialog
;;;      ;;  - :return to return from an incident sub-conversation
;;;      
;;;      ;; first increase value of turn/step counter (for debugging purposes)
;;;      (incf count)
;;;   
;;;      ;; then process special cases
;;;      (case state
;;;        
;;;        ((:failure :success)
;;;         ;; pop task-frame-list to return to calling (super-)dialog
;;;         (setq old-state state
;;;             old-header (car (has-MOSS-sub-dialog-header conversation))
;;;			;; get transition state from the task-frame-list
;;;             state (exec-return-from-sub-dialog conversation state))
;;;         (vformat "dsc ~S/returning from ~S sub-dialog.~%state: ~S"
;;;                  count (if old-header (car (has-MOSS-label old-header))) state)
;;;         ;; if OMAS is active put a stackframe mark onto the salient features queue
;;;         (dialog-state-crawler-put-sf-end conversation `(quote ,old-header))
;;;         )
;;;        
;;;        ;; here we return from an incident dialog to the main one
;;;        (:return
;;;         ;;********** should pop frame-list and set context???
;;;         ;; restore sub-dialog header and state-context. state stays the same
;;;         (vformat "dsc ~S/...returning from side-conversation." count)
;;;         (return-from dialog-state-crawler))
;;;        
;;;        (otherwise
;;;         ;; here, we have a transition (state or header)
;;;         ;; when transition is sub-conversation header, we make a transition
;;;         ;; onto the input state of the sub-conversation (in make-transition)
;;;         (setq state (make-transition conversation old-state state))
;;;         ;; check for errors or illegal states
;;;         (unless (%type? state '$QSTE)
;;;           (error "illegal state object ~S" state))
;;;         
;;;         ;; here we have a bona fide case and launch the =execute method
;;;         (vformat "~2%================== STATE: ~S" (car (HAS-MOSS-LABEL state)))
;;;         
;;;         (setq result (send state "=execute" conversation))
;;;         ;; possible values for result are:
;;;         ;;   - (:wait) to wait for the user's input
;;;         ;;   - (:resume) we do not wait (no question asked)
;;;         ;; "=execute" rather than '=execute so that name is interned into current
;;;         ;; execution package
;;;         ;; if there is a :text or :question option and :web is t, then they are
;;;         ;; stored onto the :text-to-print and question-to-print FACTS fields
;;;         ;(vformat "~%; dialog-state-crawler /*language*: ~S" *language*)
;;;         
;;;         (setq action (car result)
;;;             ;arg-list (cdr result)
;;;             old-state state)
;;;         ;(vformat "dsc ~S/=execute returned:~%   ~S" count result)
;;;         
;;;         ;; do something according to the value of result
;;;         (case action
;;;           
;;;           ;; if we asked something to the user, then we must wait for the answer, 
;;;           ;; i.e.
;;;           ;; until something appears in the TO-DO slot of the conversation object
;;;           (:wait
;;;            
;;;            ;; activate the interface and wait for an input. The input may come: 
;;;            ;;   - from the master (keyboard or vocal)
;;;            ;;   - from a message: an answer to a PA request (may be :failure)
;;;            ;;   - from the click of the PROCESS button associated with the tasks 
;;;            ;;     to do
;;;            ;; when :eb is true, prints the saved texts
;;;            
;;;            (activate-answer-panel conversation)
;;;            ;; now, we got some answer in FACTS/RAW-INPUT, whether from the master 
;;;            ;; or from other agents. Several cases:
;;;            ;;   - :failure (e.g. from an agent's answer)
;;;            ;;   - verbatim input (for long texts)
;;;            ;;   - standard short text
;;;            ;;   - structured list from another agent's answer (pattern)
;;;            (vformat "dsc ~S/answer received - FACTS:~%~{~&   ~S~}" 
;;;                     count (has-MOSS-facts conversation))
;;;            
;;;            ;; get input data
;;;            (setq raw-input (read-fact conversation :raw-input))
;;;            
;;;            (cond
;;;             ;; if the flag :verbatim is set, it means that we want the text to be
;;;             ;; passed without analysis
;;;             ((read-fact conversation :verbatim)
;;;              (replace-fact conversation :input (list raw-input)))
;;;             ;; the :verbatim flag should be reset in the calling dialog
;;;             
;;;             ;; if a failure transfer it to :input
;;;             ((eql raw-input :failure)
;;;              (replace-fact conversation :input (list :failure)))
;;;             
;;;             ;; if we have a string, we go process it (segmenting it) and putting
;;;			;; the result into FACTS/INPUT
;;;             ((stringp raw-input)
;;;              (replace-fact conversation :input
;;;                            (segment-master-input 
;;;                               (car (has-moss-agent conversation))
;;;                             raw-input)))
;;;             
;;;             ;; otherwise, put the result into FACTS/ANSWER
;;;             (t
;;;              (replace-fact conversation :answer raw-input)
;;;              ;; put a mark into FACTS/INPUT to avoid positive failure test
;;;              ;; it seems that nobody uses that mark. Indeed, when an answer is
;;;              ;; expected, the concerned functions fetch the data from FACTS/ANSWER
;;;              ;; directly
;;;              (replace-fact conversation :input (list "*see-answer*"))))
;;;            
;;;            (vformat "dsc ~S/input processed - FACTS:~%~{~&   ~S~}" 
;;;                     count (has-MOSS-facts conversation))
;;;            
;;;            ;(%pep (car (has-q-context conversation)))
;;;            ;; go analyze master input
;;;            (cond
;;;             
;;;             ;; here we should have provision for interrupting conversation
;;;             ;; see commented lines after function
;;;             
;;;             )
;;;            ) ; end of first part or :wait case, go execute =resume method
;;;           
;;;           (:resume
;;;            ;; case when no question was asked to master. exit from cond, will call 
;;;            ;; =resume method
;;;            (vformat "dsc ~S/FACTS:~%~{~&   ~S~}" 
;;;                     count (has-MOSS-facts conversation))
;;;            )
;;;           
;;;           (otherwise
;;;            ;; anything else than :wait or :resume is an error
;;;            ;(trformat "dialog-state-crawler ~S/bad =execute return: ~S" count  action)
;;;            (throw :dialog-error nil))
;;;           )
;;;         
;;;       ;;============================================================================
;;;       ;; call now the =resume method possible return values are:
;;;       ;;     (:failure) (:success) (:transition <state>) (:sub-dialog ...)
;;;       ;;============================================================================
;;;         (vformat "dsc ~S/calling =resume" count)
;;;         (vformat "dsc ~S/FACTS:~%~{~&   ~S~}" 
;;;                  count (has-MOSS-facts conversation))
;;;         
;;;         (setq result (send state "=resume" conversation)
;;;             action (car result)
;;;             arg-list (cdr result))
;;;         ;(trformat "dialog-state-crawler ~S/=resume returned: ~S" count result)
;;;         (vformat "dsc ~S return-from-resume/FACTS:~%~{~&   ~S~}" 
;;;                  count (has-MOSS-facts conversation))
;;;         ;; analyze results from :resume
;;;         (case action
;;;		 
;;;           (:transition
;;;            ;; normal transition
;;;            (setq state (car arg-list))
;;;            )
;;;			
;;;           ((:failure :success) 
;;;            (setq state action)
;;;            ;; do nothing, return to the start of the loop
;;;            )
;;;           (:sub-dialog
;;;            ;; we step into the sub-dialog, getting the entry state
;;;            (vformat "dsc ===== calling subdialog: ~S" (car arg-list))
;;;            (setq state 
;;;                  (apply #'make-transition-to-sub-dialog
;;;                         conversation
;;;                         state
;;;                         (eval (car arg-list))
;;;                         (cdr arg-list)))
;;;            ;; the subdialog returns a transition state
;;;            ;(trformat "dialog-state-crawler ~S/subdialog returned: ~S" 
;;;            ;          count state)
;;;            ;; if OMAS is active put a stackframe mark on salient features queue
;;;            ;; record sub-dialog-header onto the saleint list in case of a PA
;;;            (dialog-state-crawler-put-sf conversation (car arg-list))
;;;            )
;;;			
;;;           (otherwise
;;;            ;; anything else is illegal and aborts the dialog turn
;;;            (throw 
;;;                :dialog-error
;;;              (format nil 
;;;                  "illegal value returned by the =resume method of state ~S"
;;;                state)))
;;;           )
;;;         ;; end processing =resume returns
;;;         ) ; end of the otherwise case
;;;        ) ; end of the top level case
;;;      
;;;      ;;=== debug trace
;;;      ;; prints leading stars (;*****), then skips a line and prints the rest
;;;      ;(trformat "~2%;===== dialog-state-crawler ~S/transition to: ~S~%;   ~S" 
;;;      ;          count state (if (keywordp state) state (HAS-MOSS-LABEL state)))
;;;      ;(print-q-state  conversation)
;;;      ) ; end loop
;;;    ))

(defUn dialog-state-crawler (conversation &key state)
  "works as follows:
   - start from the initial state of the conversation header or a specific state
   - send an =execute method to the state
   - makes a transition according to what is returned by the =execute method
      - if :resume send a =resume method to the current state
      - if :success or :failure (subdialogs) make the correponding transition
   - after a =resume method
      - if :success or :failure (subdialogs) return the corresponding state
      - if a state object makes the transition
Arguments:
   conversation: a conversation object
   state (key): a specified state of the conversation
Return:
   when exiting from a sub-dialog, returns a state, e.g. :failure or _xx-state."
  ;; check type of first argument
  (unless (%type? conversation '$CVSE)
    (error "unknown conversation object ~S" conversation))
  ;; fetch dialog header from the conversation object and initial state from the
  ;; dialog header
  (let* ((dialog-header (car (%get-value conversation '$DHDRS)))
         (initial-state (car (%get-value dialog-header '$ESTS)))
         (count 0)  ; will count the number of transitions
         old-state result action arg-list raw-input old-header
         saved-state saved-conversation)
    (declare (ignore success failure))
    ;; check state if not given, start with initial state
    (unless state
      (setq state initial-state))
    ;; trace what we are doing if verbose set
    (vformat "=================================================================~2%~
              Entering dialog-state-crawler ~S/state: ~S; package: ~S"
             count state *package*)
    
    (loop
      ;; at the beginning of the loop the state variable may have several distinct
      ;; values: 
      ;;  - a state id (normal case)
      ;;  - :failure or :success when returning from a sub-dialog
      ;;  - :return to return from an interrupting sub-conversation
      
      ;; first increase value of turn/step counter (for debugging purposes)
      (incf count)
      
      ;; the catch directive is used to reset the control at the beginning of the
      ;; loop when an interrupting dialog terminates
      (catch 
       :start
       
       ;; save state and conversation for resetting them after an interrupting
       ;; subdialog
       (setq saved-state state 
           saved-conversation (copy-list (symbol-value conversation)))
       
       ;; then process special cases
       (case state
         
         ((:failure :success)
          ;; pop task-frame-list to return to calling (super-)dialog
          (setq old-state state
              old-header (car (has-MOSS-sub-dialog-header conversation))
              ;; get transition state from the task-frame-list
              state (exec-return-from-sub-dialog conversation state))
          (vformat "dsc ~S/returning from ~S sub-dialog.~%state: ~S"
                   count (if old-header (car (has-MOSS-label old-header))) state)
          ;; if OMAS is active put a stackframe mark onto the salient features queue
          (dialog-state-crawler-put-sf-end conversation `(quote ,old-header))
          )
         
         ;; here we return from an incident dialog to the main one
         (:return
          ;;********** should pop frame-list and set context???
          ;; restore sub-dialog header and state-context. state stays the same
          (vformat "dsc ~S/...returning from side-conversation." count)
          (return-from dialog-state-crawler))
         
         (otherwise
          ;; here, we have a transition (state or header)
          ;; when transition is sub-conversation header, we make a transition
          ;; onto the input state of the sub-conversation (in make-transition)
          (setq state (make-transition conversation old-state state))
          ;; check for errors or illegal states
          (unless (%type? state '$QSTE)
            (error "illegal state object ~S" state))
          
          ;;=====================================================================
          ;; call now the =execute method possible return values are:
          ;;     (:wait) (:resume)
          ;;=====================================================================
          (vformat "~2%================== STATE: ~S" (car (HAS-MOSS-LABEL state)))
          
          (setq result (send state "=execute" conversation))
          ;; "=execute" rather than '=execute so that name is interned into current
          ;; execution package
          ;; possible values for result are:
          ;;   - (:wait) to wait for the user's input
          ;;   - (:resume) we do not wait (no question asked)
          ;; if there is a :text or :question option and :web is t, then they are
          ;; stored onto the :text-to-print and :question-to-print FACTS fields
          ;(vformat "~%; dialog-state-crawler /*language*: ~S" *language*)
          
          (setq action (car result)
              old-state state)
          ;(vformat "dsc ~S/=execute returned:~%   ~S" count result)
          
          ;; do something according to the value of result
          (case action
            
            ;; if we asked something to the user, then we must wait for the answer, 
            ;; i.e.
            ;; until something appears in the TO-DO slot of the conversation object
            (:wait
             
             ;; activate the interface and wait for an input. The input may come: 
             ;;   - from the master (keyboard or vocal)
             ;;   - from a message: an answer to a PA request (may be :failure)
             ;;   - from the click of the PROCESS button associated with the tasks 
             ;;     to do
             ;; when :eb is true, prints the saved texts
             
             (activate-answer-panel conversation)
             ;; now, we got some answer in FACTS/RAW-INPUT, whether from the master 
             ;; or from other agents. Several cases:
             ;;   - :failure (e.g. from an agent's answer)
             ;;   - verbatim input (for long texts)
             ;;   - string short text
             ;;   - structured list from another agent's answer (pattern)
             (vformat "dsc ~S/answer received - FACTS:~%~{~&   ~S~}" 
                      count (has-MOSS-facts conversation))
             
             ;; get input data
             (setq raw-input (read-fact conversation :raw-input))
             
             (cond
              ;; if the flag :verbatim is set, it means that we want the text to be
              ;; passed without analysis
              ((read-fact conversation :verbatim)
               (replace-fact conversation :input (list raw-input)))
              ;; the :verbatim flag should be reset in the calling dialog
              
              ;; if a failure transfer it to :input
              ((eql raw-input :failure)
               (replace-fact conversation :input (list :failure)))
              
              ;; if we have a string, we go process it (segmenting it) and putting
              ;; the result into FACTS/INPUT
              ((stringp raw-input)
               (replace-fact conversation :input
                             (segment-master-input 
                              (car (has-moss-agent conversation))
                              raw-input)))
              
              ;; if raw-input is a list starting with :interrupt, we call the 
              ;; dialog-state-crawler-interrupt function that will prepare a recursive
              ;; call to dialog-state-crawler
              ((and (listp raw-input)(eql (car raw-input) :interrupt))
               (vformat "dsc ~S/saved conversation:~%   ~S" 
                      count saved-conversation)
               (vformat "dsc ~S/saved-state: ~S~%" count saved-state)
               ;; when returning from this function (actually from a recursive call
               ;; to dialog-state-crawler, the state variable will be the current
               ;; one, but the FACTS area should be reinstalled and the FRAME-LIST
               ;; poped (done by dialog-state-crawler-interrupt)
               (dialog-state-crawler-interrupt conversation state count)
               ;; the state variable is that of the state prior to the interruption
               (setq state saved-state)
               ;; the conversation is reset to the one prior to the interruption
               (set conversation saved-conversation)
               ;; we should tell master that we go back to the interrupted conversation
               ;; here we restart the turn at the =execute level
               
               (vformat "dsc ~S/input processed - FACTS:~%~{~&   ~S~}" 
                      count (has-MOSS-facts conversation))
               (vformat "dsc ~S/state: ~S~%" count state)
               (throw :start nil)
               )
              
              ;; otherwise, put the result into FACTS/ANSWER
              (t
               (replace-fact conversation :answer raw-input)
               ;; put a mark into FACTS/INPUT to avoid positive failure test
               ;; it seems that nobody uses that mark. Indeed, when an answer is
               ;; expected, the concerned functions fetch the data from FACTS/ANSWER
               ;; directly
               (replace-fact conversation :input (list "*see-answer*"))))
             
             (vformat "dsc ~S/input processed - FACTS:~%~{~&   ~S~}" 
                      count (has-MOSS-facts conversation))
             
             ) ; end of first part or :wait case, go execute =resume method
            
            (:resume
             ;; case when no question was asked to master. exit from cond, will call 
             ;; =resume method
             (vformat "dsc ~S/FACTS:~%~{~&   ~S~}" 
                      count (has-MOSS-facts conversation))
             )
            
            (otherwise
             ;; anything else than :wait or :resume is an error
             ;(trformat "dialog-state-crawler ~S/bad =execute return: ~S" count  action)
             (throw :dialog-error nil))
            )
          
          ;;============================================================================
          ;; call now the =resume method possible return values are:
          ;;     (:failure) (:success) (:transition <state>) (:sub-dialog ...)
          ;;============================================================================
          (vformat "dsc ~S/calling =resume" count)
          (vformat "dsc ~S/FACTS:~%~{~&   ~S~}" 
                   count (has-MOSS-facts conversation))
          
          (setq result (send state "=resume" conversation)
              action (car result)
              arg-list (cdr result))
          ;(trformat "dialog-state-crawler ~S/=resume returned: ~S" count result)
          (vformat "dsc ~S return-from-resume/FACTS:~%~{~&   ~S~}" 
                   count (has-MOSS-facts conversation))
          ;; analyze results from :resume
          (case action
            
            (:transition
             ;; normal transition
             (setq state (car arg-list))
             )
            
            ((:failure :success) 
             (setq state action)
             ;; do nothing, return to the start of the loop
             )
            
            (:sub-dialog
             ;; we step into the sub-dialog, getting the entry state
             (vformat "dsc ===== calling subdialog: ~S" (car arg-list))
             (setq state 
                   (apply #'make-transition-to-sub-dialog
                          conversation
                          state
                          (eval (car arg-list))
                          (cdr arg-list)))
             ;; the subdialog returns a transition state
             ;(trformat "dialog-state-crawler ~S/subdialog returned: ~S" 
             ;          count state)
             ;; if OMAS is active put a stackframe mark on salient features queue
             ;; record sub-dialog-header onto the saleint list in case of a PA
             (dialog-state-crawler-put-sf conversation (car arg-list))
             )
            
            (otherwise
             ;; anything else is illegal and aborts the dialog turn
             (throw 
                 :dialog-error
               (format nil 
                   "illegal value returned by the =resume method of state ~S"
                 state)))
            )
          ;; end processing =resume returns
          ) ; end of the otherwise case
         ) ; end of the top level case
       
       ;;=== debug trace
       ;; prints leading stars (;*****), then skips a line and prints the rest
       ;(trformat "~2%;===== dialog-state-crawler ~S/transition to: ~S~%;   ~S" 
       ;          count state (if (keywordp state) state (HAS-MOSS-LABEL state)))
       ;(print-q-state  conversation)
       ) ; end catch
      ) ; end loop
    ))

#|
(step (moss::dialog-state-crawler 'albert::$CVSE.1))
|#
;;;((has-interrupt conversation)
;;; ;; this case should be fairly rare, since it is most probable that the
;;; ;; master will not start processing a message in the middle of a dialog,
;;; ;; but first finish it
;;; ;; if the input is a request to start a :process-ask dialog, then
;;; ;; the INTERRUPT slot of the conversation object is set
;;; ;; reset interrupt slot
;;; (setf (has-interrupt conversation) nil)
;;; (vformat "dsc ~S/interrupting conversation...~2%" count )
;;; ;; Start a subconversation
;;; ;;********** <not implemented yet>
;;; )

             
;;; If the input is a why? question, then we call a why sub-dialog 
;;; DATA are left untouched, so that we can continue from this point
;;;
;;;((process-patterns (car (has-to-do conversation)) *why-patterns*)
;;; (vformat "dsc ~S/answering a why question." count)
;;; (process-why-question conversation) ; return state is ignored
;;; ;; on return, the conversation should be restarted where it was left
;;; ;; i.e. with the current state. DATA slot must be restored...
;;; (set-data conversation saved-data)
;;; )

            
;;;((and (setq answer (has-performative (car (has-q-context conversation))))
;;;      (not (member (determine-performative conversation) answer)))
;;; (vformat "dsc ~S/interrupting conversation...~2%" count )
;;; ;(break "...frame-list:~%  ~S" (has-frame-list conversation))
;;; ;; in the =exec part of the state we set the expected performative
;;; ;; that the master's input should use. At the beginning of a turn
;;; ;; any performative is legal, in other stages of the conversation
;;; ;; performatives can be determined (like expecting an answer).
;;; ;; if the input is not what is expected, then we start a new conversation 
;;; ;; segment. In that case, we call the crawler recursively and set
;;; ;; failure and success return values to be :return
;;; ;; we must save the state-context object, process the new segment and
;;; ;; return to the state we are currently in, and reexecute the =execute
;;; ;; method
;;; ;; fake a subdialog entry with a :return tag
;;; ;; <success> <failure> <cur-state-context> <goal> <sub-dialog-header>
;;; (push (list :return :return 
;;;             (car (HAS-Q-CONTEXT conversation))
;;;             (HAS-GOAL conversation)
;;;             (HAS-MOSS-SUB-DIALOG-HEADER conversation))
;;;       (HAS-FRAME-LIST conversation))
;;; ;; reset sub-dialog header
;;; (setf (HAS-SUB-DIALOG-HEADER conversation) nil)
;;; ;; call the crawler recursively
;;; ;; however, we are going to enter the process state of the main
;;; ;; conversation after the initial message, directly at the processing
;;; ;; state
;;; (setq next-state (car (HAS-PROCESS-STATE
;;;                        (car (HAS-DIALOG-HEADER conversation)))))
;;; ;; the input will be taken from the data slot of the state-context, 
;;; ;; thus we should set it up accordingly, i.e. create a new state context
;;; ;; empty as if we had reset the dialog, with data properly set up
;;; (setq state-context 
;;;       (m-make-instance 'Q-CONTEXT
;;;                        `(HAS-DATA ,(car (HAS-TO-DO conversation)))))
;;; (send conversation '=replace 'HAS-Q-CONTEXT (list state-context))
;;; ;; the only thing to keep is the salient feature stack!
;;; ;(break "...recursive call; state: ~S" next-state)
;;; ;(trformat "dialog-state-crawler ~S/calling function recursively ~
;;; ;           frame-list: ~% ~S~2%" count (HAS-FRAME-LIST conversation)) 
;;; 
;;; (dialog-state-crawler conversation :state next-state)
;;; ;; on return, the conversation is restarted where it was left
;;; ;(trformat "dialog-state-crawler ~S/returning from incident ~
;;; ;           conversation; frame-list:~% ~S~2%" count 
;;; ;          (HAS-FRAME-LIST conversation))
;;; ;(break "...return; frame-list:~%  ~S" (has-frame-list conversation))
;;; )
;;;---------------------------------------------- DIALOG-STATE-CRAWLER-INTERRUPT

(defun dialog-state-crawler-interrupt (conversation state count)
  "handles dialog interruptions by creating a new sub-dialog to process the ~
   interruption. facts:raw-input contains a list (:interrupt <info>)
Arguments:
   conversation: the current conversation object
   state: the current state
Return:
   nothing interesting since we use frame-list stack"
  (declare (ignore state))
  (let (next-state raw-input todo-message frame)
    (vformat "~2%========== dsci ~S/interrupting conversation...~%" count )
    (vformat "dsci ~S/input processed - FACTS:~%~{~&   ~S~}" 
                      count (has-MOSS-facts conversation))
    ;(break "...frame-list:~%  ~S" (has-frame-list conversation))
    ;; Here we start a new conversation segment
    ;; In that case, we call the crawler recursively and set
    ;; failure and success return values to be :return
    ;; we must save the context (FACTS area), process the new segment and
    ;; return to the state we are currently in, and reexecute the =execute
    ;; method
    ;; fake a subdialog entry with a :return tag
    ;; <success> <failure> <cur-state-context> <goal> <sub-dialog-header>
    (push (list :return :return 
                (symbol-value conversation) ; save the whome conversation object
                (HAS-MOSS-GOAL conversation)
                (HAS-MOSS-SUB-DIALOG-HEADER conversation))
          (HAS-MOSS-FRAME-LIST conversation))
    ;; reset sub-dialog header
    (setf (HAS-MOSS-SUB-DIALOG-HEADER conversation) nil)
    ;; call the crawler recursively
    ;; however, we are going to enter the process state of the main current
    ;; conversation after the initial message, directly at the processing
    ;; state. We assume that the top-level is valid for the interrupting
    ;; conversation since it is a feature of the application.
    (setq next-state (car (HAS-MOSS-PROCESS-STATE
                           (car (HAS-MOSS-DIALOG-HEADER conversation)))))
    ;(vformat "dsci ~S/next state: ~S~%" count next-state)
    
    ;;=== here we should set FACTS properly to allow the dialog to proceed
    ;; get raw-input from the current FACTS
    (setq raw-input (read-fact conversation :raw-input))
    ;; we must also save the reference to the message to be processed
    (setq todo-message (read-fact conversation :todo-message))
    ;; clean current FACTS
    (clear-all-facts conversation)
    ;; initialize :input with second part of raw-input
    (replace-fact conversation :input (cdr raw-input))
    ;; add message to process
    (replace-fact conversation :todo-message todo-message)
    
    ;; the only thing to keep is the salient feature stack!
    ;(break "...recursive call; state: ~S" next-state)
    ;(trformat "dialog-state-crawler ~S/calling function recursively ~
    ;           frame-list: ~% ~S~2%" count (HAS-FRAME-LIST conversation)) 
    
    (dialog-state-crawler conversation :state next-state)
    ;; on return, the conversation is restarted where it was left
    ;; when returning from a conversation we return to top-level 
    ;(trformat "dialog-state-crawler ~S/returning from incident ~
    ;           conversation; frame-list:~% ~S~2%" count 
    ;          (HAS-MOSS-FRAME-LIST conversation))
    
    ;; pop the frame-list stack
    (setq frame (pop (HAS-MOSS-FRAME-LIST conversation)))
    ;; reset conversation object as it was before interruption
    (set conversation (nth 2 frame))
    
    (vformat "~%========== dsci ~S/returning from interrupting conversation...~2%"
             count)
    
    ;(break "dialog-state-crawler-interrupt")
    
    ;; FRAME-LIST will be poped when the answer of a =resume method will be :success
    ;; or :failure(defUn mw-ae-save-on-click (dialog widget)
  
  t)
    )
  
;;;------------------------------------------------- DIALOG-STATE-CRAWLER-PUT-SF
;;; this function is used with omas to add a salient features stack frame mark

(defun dialog-state-crawler-put-sf (conversation state)
  "adds a stack-frame micro-context mark on OMAS agent salient features queue."
  #-omas (declare (ignore state))
  #-OMAS conversation ; does nothing without OMAS
  #+OMAS
  (let ((agent (car (%get-value conversation '$AGT))))
    ;; recover agent from conversation object
    ;; then insert mark
    (if agent 
        (omas::sf-put agent `(:micro-context ,(get-universal-time) ,state)))
    ))

#|
(dialog-state-crawler-put-sf '$CVSE.1)
((:MICRO-CONTEXT 3534182443 $QHDE.12) 
 (:END-MICRO-CONTEXT "success")
 ("postit" ("id" "P-21"))
 (:MICRO-CONTEXT 3534171765 'JEAN-PAUL::_DELETE-POSTIT-CONVERSATION))
|#
;;;--------------------------------------------- DIALOG-STATE-CRAWLER-PUT-SF-END
;;; this function is used with OMAS for inserting an end-of-micro-context mark
;;; on the salient feature queue

(defun dialog-state-crawler-put-sf-end (conversation state)
  "adds a stack frame micro-context-end mark on OMAS agent salient features ~
    queue."
  #-OMAS (list conversation state) ; does nothing without OMAS
  #+OMAS
  (let ((agent (car (%get-value conversation '$AGT))))
    ;; insert mark (state is :success or :failure)
    (if agent
        (omas::sf-put agent
                      `(:end-micro-context ,(get-universal-time) ,state))))
  )

#|
(dialog-state-crawler-put-sf-end '$CVSE.1)
((:END-MICRO-CONTEXT 3534183086)
 (:MICRO-CONTEXT 3534182443 $QHDE.12)
 (:END-MICRO-CONTEXT "success")
 ("postit" ("id" "P-21"))
 (:MICRO-CONTEXT 3534171765 'JEAN-PAUL::_DELETE-POSTIT-CONVERSATION))
|#
;;;-------------------------------------------------------------- DISPLAY-ANSWER
;;; this function is not very useful: duplicates the =display-text method

(defUn display-answer (conversation)
  "displays the content of FACTS/ANSWER using the output channel of conversation.
Argument:
   conversation: a conversation object
Return:
   nil."
  ;; check that conversation is a conversation object
  (unless (%type? conversation '$CVSE)
    (error "argument should be a conversation object instead of: ~S" conversation))
  (let ((output (car (send conversation '=get  "moss-output-window")))
        (data (read-fact conversation :answer))
        text)
    ;;********** check that output is non nil
    (unless output
      (error "conversation object ~S has no output channel"))
    
    ;; enforce a string to print
    (setq text
          (if (listp data) 
              (format nil "~{~S~^~%~}" data)
            ;; if not a list make a simple string
            (format nil "~S" data)))
    ;; print it on the right channel
    (send conversation '=display-text text)
    nil))

;;;----------------------------------------------------------------- DISPLAY-DOC

(defUn display-doc (conversation &rest text)
  "display text into the output window between bars. Calls display-text.
Arguments:
   conversation: current conversation object. If nil, print into listener
   text: text to display (a string or a list of strings)
Return:
   nil"
  (format t "~%-------------------------------------------------------------------")
  (apply #'send conversation #'display-text text)
  (format t "~%-------------------------------------------------------------------")
  )

;;;--------------------------------------------------------- FILL-ANSWER-PATTERN

(defUn fill-answer-pattern (pattern object-id-list)
  "considers each object of the object list and returns a list of filled paterns.
Arguments:
   pattern: an answer pattern
   object-id-list: a list of object identifiers
Return:
   a list of filled patterns."
  (let (result)
    ;; build a list for each object
    (dolist (obj-id object-id-list)
      (push (fill-answer-pattern-for-one-level pattern obj-id) result))
    (reverse result)))

;;;------------------------------------------- FILL-ANSWER-PATTERN-FOR-ONE-LEVEL

(defUn fill-answer-pattern-for-one-level (pattern obj-id)
  "fills a pattern with the info for one object. Calls fill-pattern recursively ~
   if needed.
Arguments:
   pattern: answer pattern
   obj-id: id of a single object
Return:
   a list representing the pattern filled with the info from the object."
  (when (and (%pdm? obj-id) 
             (%is-a?
              (car (%type-of obj-id))
              (%%get-id (car pattern) :class)))
    (let ((saved-pattern pattern)
          prop-id successors value lres)
      ;; first element must be a class name
      ;; instead of (car pattern) should record class-name of obj-id
      (push (car (send (car (%type-of obj-id)) '=get-name)) lres)
      (pop pattern)
      ;; if null pattern, call =summary
      (unless pattern
        (push (send obj-id '=summary) lres)
        (return-from fill-answer-pattern-for-one-level (reverse lres)))
      ;; then take care of properties, e.g. (("name")("first name") 
      ;;                                     ("telephone" 
      ;;                                         ("cell phone")))
      (loop
        (dformat :dialog 0
                  "~%; fill-answer-pattern-for-one-level/ obj-id: ~S pattern:~% ~S"
          obj-id pattern)
        (unless pattern (return nil))
        ;; check if attribute
        (setq prop-id (%get-property-id-from-name obj-id (caar pattern)))
        (cond
         ((%is-attribute? prop-id)
          (setq value (send obj-id '=get-id prop-id))
          ;; fill in value only if non nil
          (when value
            ;(push (append (pop pattern) value) lres)
            (push (append (car pattern) value) lres) ;jpb0910
            )
          (pop pattern)) ; jpb0910
         ;; check now relation cadr should be a list, e.g. ("telephone" 
         ;;                                                 ("cell phone"))
         ((and (%is-relation? prop-id) (cadar pattern)(listp (cadar pattern)))
          (setq successors (send obj-id '=get-id prop-id))
          (when successors
            (setq value (remove nil (fill-answer-pattern (cadar pattern) successors)))
            (when value
              (vformat "fill-answer-pattern-for-one-level/value: ~S; pattern: ~S" 
                       value pattern)
              (push (cons (caar pattern) value) lres)
              ))
          
          (pop pattern)) ; JPB1010
         
         ;; when pattern has an isolated relation, e.g. ("telephone")
         ;; then if there are values associated to this property, we summarize them
         ((and (%is-relation? prop-id) (null (cdar pattern)))
          (setq successors (send obj-id '=get-id prop-id))
          (setq value (remove nil (broadcast successors '=summary)))
          (when value
            (push (cons (caar pattern) (mapcar #'car value)) lres))
          (pop pattern))
         
         ;; otherwise bad format
         (t (warn "bad pattern format ~S" saved-pattern)
            (return-from fill-answer-pattern-for-one-level nil))))
      (reverse lres))))

;;;(defun fill-answer-pattern-for-one-level (pattern obj-id)
;;;  "fills a pattern with the info for one object. Calls fill-pattern recursively ~
;;;      if needed.
;;;Arguments:
;;;   pattern: answer pattern
;;;   obj-id: id of a single object
;;;Return:
;;;   a list repreenting the pattern filled with the info from the object."
;;;  (when (and (%pdm? obj-id) 
;;;             (%is-a?
;;;              (car (%type-of obj-id))
;;;              (%get-class-id-from-ref (car pattern))))
;;;    (let ((saved-pattern pattern)
;;;          prop-id successors value lres)
;;;      ;; first element must be a class name
;;;      ;; instead of (car pattern) should record class-name of obj-id
;;;      (push (car (send (car (%type-of obj-id)) '=get-name)) lres)
;;;      (pop pattern)
;;;      ;; if null pattern, call =summary
;;;      (unless pattern
;;;        (push (send obj-id '=summary) lres)
;;;        (return-from fill-answer-pattern-for-one-level (reverse lres)))
;;;      ;; then take care of properties, e.g. (("name")("first name") 
;;;      ;;                                     ("telephone" 
;;;      ;;                                         ("telephone" ("cell phone"))))
;;;      (loop
;;;        (unless pattern (return nil))
;;;        ;; check if attribute
;;;        (setq prop-id (%get-property-id-from-name obj-id (caar pattern)))
;;;        (cond
;;;         ((%is-attribute? prop-id)
;;;          (setq value (send obj-id '=get-id prop-id))
;;;          ;; fill in value only if non nil
;;;          (when value
;;;            (push (append (pop pattern) value) lres)
;;;            ))
;;;         ;; check now relation cadr should be a list, e.g. ("telephone" 
;;;         ;;                                         ("telephone" ("cell phone"))))
;;;         ((and (%is-relation? prop-id) (cadar pattern)(listp (cadar pattern)))
;;;          (setq successors (send obj-id '=get-id prop-id))
;;;          (when successors
;;;            (setq value (remove nil (fill-answer-pattern (cadar pattern) successors)))
;;;            (when value
;;;              (vformat "fill-answer-pattern-for-one-level/value: ~S; pattern: ~S" 
;;;                       value pattern)
;;;              (push (cons (caar pattern) value) lres)
;;;              )
;;;            
;;;            (pop pattern)))
;;;         (t (warn "bad pattern format ~S" saved-pattern)
;;;            (return-from fill-answer-pattern-for-one-level nil))))
;;;      (reverse lres))))

#|
? (in-package :sa-address )
#<Package "SA-ADDRESS">
? (moss::fill-answer-pattern '("person") '($E-PERSON.1))
(("person" "Barthes: Jean-Paul"))

? (moss::fill-answer-pattern '("person" ("name")) '(sa-address::$E-PERSON.1))
(("person" ("name" "Barthes")))

? (moss::fill-answer-pattern '("person" ("name") ("first-name")) '(sa-address::$E-PERSON.1))
(("person" ("name" "Barthes") ("first-name" "Jean-Paul")))

? (moss::fill-answer-pattern-for-one-level '("person" ("phone" ("telephone"))) '$E-PERSON.1)
("person"
 ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
  ("cell phone" "(0)6 80 45 32 67")))

? (moss::fill-answer-pattern '("person" ("phone" ("telephone"))) '($E-PERSON.1))
(("person"
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67"))))

? (moss::fill-answer-pattern '("person" ("name") ("first name")("phone" ("telephone"))) '($E-PERSON.1))
(("person" ("name" "Barthes") ("first name" "Jean-Paul")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67"))))

? (moss::fill-answer-pattern '("person" ("phone" ("telephone"))("name") ("first name")) '($E-PERSON.1))
(("person"
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67"))
  ("name" "Barthes") ("first name" "Jean-Paul")))
|#
;;;---------------------------------------------------------------- FIND-OBJECTS

(defUn find-objects (query word-list &key all-objects (max-pattern-size 3)
                           used-patterns)
  "function to access objects from a list of words and a MOSS query.
Arguments:
   query: moss query pattern in which the symbol ? will be replaced by a string
   word-list: the list of words (strings) making the sentence
   all-objects (key): if true tries to extract all possible objects otherwise only
                      the first one
   max-pattern size (key): maximum number of words in a pattern
   used-patterns (key): patterns that have already been tried
Return:
   a list of objects identifiers."
  (let ((first-find (not all-objects))
        patterns tested-patterns result entry input pat)
    ;; first compute the pattern list, returns (("xxx" start end)*)
    (setq patterns (generate-access-patterns word-list :max max-pattern-size))
    ;(trformat "find-objects /patterns 1: ~S" patterns)
    (if used-patterns (setq patterns (clean-patterns patterns used-patterns)))
    ;(trformat "find-objects /patterns 2: ~S" patterns)
    ;; if no pattern left, then quit
    (unless patterns (return-from find-objects nil))
    ;(moss::trformat "find-objects /patterns: ~S" patterns)
    (is-answer-failure? nil :failure)
    ;; access each entry
    (setq pat 
          (catch :next
                 (dolist (pattern patterns)
                   ;(trformat "find-objects /pattern: ~S~%; query: ~S" pattern 
                   ;          (subst (car pattern) :? query))
                   ;; save pattern
                   (push pattern tested-patterns)
                   (setq entry (moss::access (subst (car pattern) :? query)))
                   ;; if result and we need only first entry then quit
                   (if (and first-find entry) (return-from find-objects entry))
                   ;; if we have an entry, then we must remove the entry from the initial 
                   ;; compute a new list of patterns, remove the ones that have been 
                   ;; checked and try again
                   (when entry 
                     ;(trformat "find-objects /entry: ~S" entry) 
                     (setq result entry) 
                     (throw :next pattern)))))
    ;(trformat "find-objects /pat: ~S; result: ~S; tested-patterns: ~&~S" 
    ;          pat result tested-patterns)
    ;; if pat is nil then we are done
    (unless pat (return-from find-objects result))
    ;; otherwise remove pattern from the word-list
    (setq input (list (subseq word-list 0 (cadr pat)) ; before pattern
                      (subseq word-list (caddr pat)))) ; after pattern
    ;; input is a set of fragments of the word-list (sentence)
    (setq input (remove nil input))
    ;(trformat "find-objects /input fragments left: ~S" input)
    ;; if no imput left then quit
    (unless input (return-from find-objects result))
    (setq result  ; JPB 140820 removed mapcan
          (append result
                  (reduce
                   #'append
                   (mapcar #'(lambda (xx) 
                               (find-objects query xx 
                                             :all-objects all-objects
                                             :max-pattern-size max-pattern-size
                                             :used-patterns tested-patterns))
                     input))))
    ;(trformat "find-objects /result: ~S" result)
    (delete-duplicates result)))
  
#|
(in-package :albert)
#<The ALBERT package>
(moss::find-objects '(task (has-index-pattern
                            (task-index (has-index :is :?))))
                    '("est" "qui") :all-objects t)
($E-TASK.11)

$E-TASK.11
((MOSS::$TYPE (0 $E-TASK)) (MOSS::$ID (0 $E-TASK.11))
 (MOSS::$DOCT
  (0 (:EN "Task to obtain information about a person." 
          :FR "Tache permettant d'obtenir des infos sur une personne.")))
 ($T-TASK-TASK-NAME (0 "who is")) ($T-TASK-PERFORMATIVE (0 :REQUEST)) 
 ($T-TASK-DIALOG (0 _GET-PERSON-INFO-CONVERSATION))
 ($T-TASK-INDEX (0 "qui" "est" "c'est qui")) 
 ($T-TASK-INDEX-WEIGHTS (0 ("qui" 0.5) ("est" 0.2) ("c'est qui" 0.7))))
|#
;;;-------------------------------------------------------- FIND-TASKS-FROM-TEXT

(defun find-tasks-from-text (agent args &key (performative :all))
  "this function is used for OMAS agents. It takes a data structure and computes ~
   a list of possile tasks in the current package
Arguments:
   agent: current agent
   args: a-list like ((:raw-data . <text>)(:language . :FR)
                      (:empty-words . <empty-word-lis>))
   performative (key): :request, :command, :assert, :all (default :all)
Return:
   a list of tasks possibly empty"
  (let ((input (cdr (assoc :raw-input args)))
        ;(language (or (cdr (assoc :language args)) *language*))
        ;(empty-words (cdr (assoc :empty-words args)))
        task-list patterns level weights word-weight-list first-task result 
        pair-list)
    ;; preprocess email string -> list of words
    ;; must remove final question mark or period
    (setq input (moss::segment-master-input agent input))
    ;; we assume that resulting text contains a request or command
    ;; try to find a task from the input text
    (setq task-list (moss::find-objects 
                     '("task" ("index-pattern" ("task-index" ("index" :is :?))))
                     input :all-objects t))
    ;; filter tasks that do not have the right performative
    (unless (eql performative :all) 
      (setq task-list   ; JPB 140820 removed mapcan
            (reduce 
             #'append
             (mapcar #'(lambda (xx) 
                         (if (member performative 
                                     (send xx '=get "performative"))
                             (list xx)))
               task-list))))
    (moss::vformat "_mc-find-task /task list after performative check: ~S" task-list)
    
    ;; if no task left, quit
    (unless task-list (return-from find-tasks-from-text nil))
    
    ;;=== select task to activate
    ;; first compute a list of patterns (combinations of words) from the input
    (setq patterns (mapcar #'car (generate-access-patterns input)))
    (vformat "find-tasks-from-text /input: ~S~&  generated patterns:~& ~S" 
             input patterns)
    
    ;;=== then, for each task compute index
    (dolist (task task-list)
      (setq level 0)
      ;; get the weight list
      (setq weights (%get-INDEX-WEIGHTS task))
      (vformat "find-tasks-from-text /task: ~S~&  weights: ~S" task weights)
      ;; check the patterns according to the weight list
      (setq word-weight-list (moss::%get-relevant-weights weights patterns))
      (vformat "find-tasks-from-text /word-weight-list:~&  ~S" word-weight-list)
      ;; combine the weights
      (dolist (item word-weight-list)
        (setq level (+ level (cadr item) (- (* level (cadr item))))))
      (vformat "find-tasks-from-text /level: ~S" level)
      ;; push the task and weight onto the result list
      (push (list task level) result)
      )
    (vformat "find-tasks-from-text /result:~&~S" result)
    
    ;; if no task applies return nil
    (unless result
      (return-from find-tasks-from-text))
    
    ;; otherwise, order the list
    (setq pair-list (sort result #'> :key #'cadr))
    (vformat "process-single-email /pair-list:~&  ~S" pair-list)
    
    ;; keep the first task even if its score is low
    (setq first-task (caar pair-list))
    ;; remove the tasks that have a weight less than task-threshold (default 0.4)
    #+OMAS
    (setq pair-list
          (remove nil 
                  (mapcar 
                      #'(lambda (xx) 
                          (if (>= (cadr xx) (omas::task-threshold omas::*omas*)) xx))
                    pair-list)))
    ;; compute list of tasks
    (setq task-list (or (mapcar #'car pair-list) (list first-task)))
    ;; return the list of tasks
    task-list))

#|
HDSRI(44): (MOSS::FIND-TASKS-FROM-TEXT PA_HDSRI '((:raw-data . "contacts de barth�s.")))
($E-TASK.7 $E-TASK.8 $E-TASK.5 $E-TASK.3)

HDSRI(45): (MOSS::FIND-TASKS-FROM-TEXT PA_HDSRI '((:raw-data . "contacts de barth�s."))
                                       :performative :request)
($E-TASK.7 $E-TASK.8)

HDSRI(46): (MOSS::FIND-TASKS-FROM-TEXT PA_HDSRI '((:raw-data . "contacts de barth�s."))
                                       :performative :command)
($E-TASK.5 $E-TASK.3)

HDSRI(47): (MOSS::FIND-TASKS-FROM-TEXT PA_HDSRI '((:raw-data . "contacts de barth�s."))
                                       :performative :assert)
NIL
|#
;;;---------------------------------------------------- GENERATE-ACCESS-PATTERNS

(defUn generate-access-patterns (word-list &key (max 3))
  "function to generate a list of patterns from a sentence expressed as a list of ~
      words.
Arguments:
   word-list: list of words (strings)
   max (key): max length if word combination (default 3)
Return:
   a list of entries like (\"soupe du jour\" 2 5) where 2 is the position of the ~
      substring in the sentence and 5 its end position."
  (let ((word-list-length (length word-list))
        nmax kk expr result)
    ;; otherwise try several words at a time starting with triples
    (setq nmax (min max (1- (1+ word-list-length))))
    (dotimes (nn nmax)
      ;; nn takes values 0 1 2 if list has more than 3 (max) elements
      ;; 0 1 if list has 3 elements, 0 if list has 2 elements
      ;; Now try to access using combination of words starting with triples
      ;; and down
      (setq kk (- nmax nn))
      ;; kk ranges from 3 down to 1
      (dotimes (jj (1+ (- word-list-length kk)))
        ;; jj sweeps the word-list
        (setq expr (apply #'moss::%make-phrase (subseq word-list jj (+ jj kk))))
        ;; extract combination of words
        (push (list expr jj (+ jj kk)) result)
        ;; whenever we find something, then quit
        ))
    (reverse result)))

#|
(moss::generate-access-patterns '("le" "jour" "le" "plus" "long") :max 3)
(("le jour le" 0 3) ("jour le plus" 1 4) ("le plus long" 2 5) ("le jour" 0 2) 
 ("jour le" 1 3) ("le plus" 2 4) ("plus long" 3 5) ("le" 0 1) ("jour" 1 2) 
 ("le" 2 3) ...)

|#
#|
;;;-------------------------------------------------------------------- GET-DATA

(defun get-data (conversation)
  "get data part of the context object in a conversation state.
Arguments:
   conversation: conversation."
  (let ((answer (send-no-trace conversation '=get 'has-q-context)))
    ;(trformat "get-data /state-context ~S ~%" answer)
    ;(%pep (car answer))
    (car (send (car answer) '=get 'has-data))))
|#

#|
;;;----------------------------------------------------------- GET-STATE-CONTEXT

(defun get-state-context (conversation)
  "get the state context from the environment area of the current task-frame. It ~
   is an error to call this function when the conversation is not executing any task, nor ~
   engaged in a dialog. The current state is a Moss object carrying context ~
   information, used to achieve a particular goal in a particular dialog.
Arguments:
   conversation: conversation."
  (let ((task-frame (omas::task-in-progress conversation)))
    (unless task-frame
      (error "no task being currently executed by conversation ~S" 
        (omas::name conversation)))
    (getf (omas::environment task-frame) :current-state-context)))

;;;----------------------------------------------------------- GET-MASTER-ANSWER


(defun get-master-answer (conversation)
  "returns when the master has provided an answer. When I/O occurs via the terminal ~
   device, then the functio is a simple read. When through a window pane, one must ~
   wait for the end of the interaction, i.e. when the data arrives into the ~
   TO-DO slot of the conversation object.
Arguments:
   conversation: the conversation object, contains the channel info
Return:
   not significant, returns when the interaction is completed."
  
  :done)
|#
;;;------------------------------------------------------------ INPUT-AVAILABLE?

;;; might not be necessary if process-waits returns the data...

(defUn input-available? (input-channel conversation)
  "Gets the input object. Sends an input-received? method to this object, which ~
      should reset the trigger, and transfers the data into the TO-DO slot of the ~
      conversation object.
Arguments:
   input-channel: the window object
Return:
   t if something was written, nil otherwise."
  (let ((data (input-received? input-channel)))
    (if data (%%set-value conversation data '$TDO
                          (symbol-value (intern "*CONTEXT*"))))
    data)
  )

;;;---------------------------------------------------------- IS-ANSWER-FAILURE?

(defUn is-answer-failure? (input &optional answer)
  "argument means failure if input is :failure, nil, \"failure\", :error, ~
   \"*failure*\".
Arguments:
   input: argument to test (usually input slot of FACTS)
   answer (opt): secondary argument (usually answer slot of FACTS). If input ~
                 contains answer-there, then answer is checked for error mark
Return:
   t or nil"
  (cond
   ((eql input :failure) t)
   ((eql input :error) t)
   ((null input) t)
   ((and (eql input :answer-there)
         (or (eql answer :failure)
             (and (stringp answer)
                  (or (string-equal answer "failure")
                      (string-equal answer "*failure*"))))))
   ;; now string case
   ((and (stringp input)(string-equal input "failure")))
   ((and (stringp input)(string-equal input "*failure*")))
   ((and (stringp input)
         (string-equal input "answer-there")
         (eql answer :error)))
   ((and (stringp input)
         (string-equal input "answer-there")
         (stringp answer)
         (or (string-equal answer "failure")
             (string-equal answer "*failure*"))))
   ;; next line puts the system into a loop when the answer slot has not been
   ;; cleaned from a previous failure. JPB 1006
   ;((equal answer :failure)) 
   ))

#|
(is-answer-failure? "failure" nil)
7
(is-answer-failure? nil nil)
T
(is-answer-failure? :failure nil)
T
(is-answer-failure? "answer-there" "failure")
7
(is-answer-failure? "answer-there" "*failure*")
9
(is-answer-failure? nil :failure)
T
(is-answer-failure? "youppee..." nil)
NIL
(is-answer-failure? "answer-there" :error)
T
(is-answer-failure? "Albert" :failure)
NIL
|#
;;;-------------------------------------------------------- PRINT-ANSWER-PATTERN

;(defParameter *result* nil)

(defUn print-answer-pattern-1 (expr)
  (declare (special *result*))
  (cond
   ((null expr) *result*)
   ((listp expr) 
    (print-answer-pattern-1 (car expr))
    (print-answer-pattern-1 (cdr expr)))
   (t (push (format nil "~%~A" expr) *result*))))

(defUn print-answer-pattern (expr)
  (let (*result*)
    (declare (special *result*))
    (print-answer-pattern-1 expr)
    (apply #'concatenate 'string (reverse *result*))))

#|
? (print-answer-pattern '("person:" ("   name: Barthes") ("   first-name: Jean-Paul")
 ("   phone:" ("      home phone: $E-HOME-PHONE.1")
  ("      office phone: $E-OFFICE-PHONE.1") ("      cell phone: $E-CELL-PHONE.1"))))
"
person:
   name: Barthes
   first-name: Jean-Paul
   phone:
      home phone: $E-HOME-PHONE.1
      office phone: $E-OFFICE-PHONE.1
      cell phone: $E-CELL-PHONE.1"
|#
;;;------------------------------------------------------------ MAKE-ANSWER-LIST

(defUn make-answer-list (expr &optional (offset 0) no-head)
  "default print function for returned answer patterns.
Argument:
   expr: pattern filled with data
   offset (opt): initial offset for printing the data
   no-head (opt): if t do not print the name of the property
Return;
   nil."
  (let ((indent (make-string  offset :initial-element #\space))
        lres)
    ;; check case with only class reference and summary value
    ;; or property and list of values (recursive call)
    (cond
     ;; class name and summary or attribute and values
     ((stringp (cadr expr))
      (push 
       (if no-head
           (format nil "~A~*~{~A~^, ~}" indent (pop expr) expr)
         (format nil "~A~A: ~{~A~^, ~}" indent (pop expr) expr))
       lres))
     (t
      (setq lres (list (format nil "~A~A:" indent (pop expr))))
      (incf offset 3)
      ;; loop on properties (("first-name" "Jean-Paul")
      ;;                     ("phone" ("home phone" "(0)3 44 23 31 37")))
      (loop
        (unless expr (return nil))
        ;; an attribute is followed by a simple list of strings
        (push (make-answer-list (car expr) offset no-head) lres)
        (setq expr (cdr expr))
        ;;
        )))
    (reverse lres)))

#|
? (make-answer-list '("person" "Barthes: Jean-Paul"))
("person: Barthes: Jean-Paul")

? (make-answer-list 
   '("person" ("name" "Barthes") ("first-name" "Jean-Paul") 
     ("phone" ("home phone" "(0)3 44 23 31 37") 
      ("office phone" "(0)3 44 23 44 66")
      ("cell phone" "(0)6 80 45 32 67"))))
("person:" ("   name: Barthes") ("   first-name: Jean-Paul")
 ("   phone:" ("      home phone: $E-HOME-PHONE.1")
  ("      office phone: $E-OFFICE-PHONE.1") ("      cell phone: $E-CELL-PHONE.1")))

? (make-answer-list 
   '("person" ("name" "Barthes" "Biesel") ("first-name" "Jean-Paul") 
     ("phone" ("home phone" "(0)3 44 23 31 37") 
      ("office phone" "(0)3 44 23 44 66")
      ("cell phone" "(0)6 80 45 32 67"))))
("person:" ("   name: Barthes, Biesel") ("   first-name: Jean-Paul")
 ("   phone:" ("      home phone: (0)3 44 23 31 37")
  ("      office phone: (0)3 44 23 44 66") ("      cell phone: (0)6 80 45 32 67")))

? (make-answer-list 
   '("person" ("name" "Barthes" "Biesel") ("first-name" "Jean-Paul") 
     ("phone" ("home phone" "(0)3 44 23 31 37") 
      ("office phone" "(0)3 44 23 44 66")
      ("cell phone" "(0)6 80 45 32 67")))
   0 t)
|#
;;;------------------------------------------------------------- MAKE-PRINT-LIST

(defUn make-print-list (expr &optional (offset 0) no-head)
  "takes a list of objects to print in MOSS format and produces a list to be ~
      printed.
Argument:
   expr: list of expr to print
   offset (opt): initial offset for printing the data
   no-head (opt): if t do not print the name of the property
Return;
   nil."
  (let ((result (mapcar #'(lambda (xx) (make-answer-list xx offset no-head)) expr)))
    (format nil "~{~A~%~}" (mapcar #'print-answer-pattern result))))


#|
? (make-print-list
    '(("person" ("name" "Barthes") ("first-name" "Jean-Paul")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67")))
 ("person" ("name" "Barthes Biesel" "Barthes" "Biesel") ("first-name" "Dominique")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 43 93")
   ("cell phone" "(0)6 81 24 46 66")))
 ("person" ("name" "Barthes") ("first-name" "Camille")
  ("phone" ("home phone" "+86 571 8701 0782") ("cell phone" "+86 134 5678 3173")))))
"
person:
   name: Barthes
   first-name: Jean-Paul
   phone:
      home phone: (0)3 44 23 31 37
      office phone: (0)3 44 23 44 66
      cell phone: (0)6 80 45 32 67

person:
   name: Barthes Biesel, Barthes, Biesel
   first-name: Dominique
   phone:
      home phone: (0)3 44 23 31 37
      office phone: (0)3 44 23 43 93
      cell phone: (0)6 81 24 46 66

person:
   name: Barthes
   first-name: Camille
   phone:
      home phone: +86 571 8701 0782
      cell phone: +86 134 5678 3173
"

? (make-print-list
    '(("person" ("name" "Barthes") ("first-name" "Jean-Paul")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67")))
 ("person" ("name" "Barthes Biesel" "Barthes" "Biesel") ("first-name" "Dominique")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 43 93")
   ("cell phone" "(0)6 81 24 46 66")))
 ("person" ("name" "Barthes") ("first-name" "Camille")
  ("phone" ("home phone" "+86 571 8701 0782") ("cell phone" "+86 134 5678 3173"))))
 0 t)
"
person:
   Barthes
   Jean-Paul
   phone:
      (0)3 44 23 31 37
      (0)3 44 23 44 66
      (0)6 80 45 32 67

person:
   Barthes Biesel, Barthes, Biesel
   Dominique
   phone:
      (0)3 44 23 31 37
      (0)3 44 23 43 93
      (0)6 81 24 46 66

person:
   Barthes
   Camille
   phone:
      +86 571 8701 0782
      +86 134 5678 3173
"
|#
;;;------------------------------------------------------------- MAKE-TRANSITION

(defUn make-transition (conversation old-state state)
  "makes a transition to the new state. 
Arguments:
   conversation: current conversation
   old-state: current-state
   state: new state to which me make the transition
Return:
   the id of the new state"
  ;; check state for the transition
  (unless (%type? state '$QSTE)
    (error "bad state parameter; ~S in =set-transition from ~S" state old-state))
  (send conversation '=replace 'HAS-MOSS-STATE (list state))
  ;;return the id of the state
  state)

;;;----------------------------------------------- MAKE-TRANSITION-TO-SUB-DIALOG

(defUn make-transition-to-sub-dialog (conversation current-state dialog-header
                                                   &key (success :success)
                                                   (failure :failure))
  "makes a transition to the initial input state of the sub-conversation. ~
   Calls make-transition to do the transition.
Arguments:
   conversation: current conversation
   current-state: current-state
   dialog-header: header of the sub-conversation
   success: a state to go to if return is successful (or :success)
   failure: a state to go to if sub-dialog was a failure (or :failure)
Return:
   the id of the new state"
  (let (state)
    ;; check the validity of the dialog header
    (unless (%type? dialog-header '$QHDE)
      (error "bad sub-conversation dialog header: ~S in state: ~S" 
        dialog-header current-state))
    ;; OK. push an entry frame onto the frame-list
    ;;   (<suc> <fail> <q-context> <goals> <sub-dialog-header>)
    ;; we assume that the success and failure state arguments are there
    (push (list success failure 
                :no-context ; used to record context object
                (HAS-MOSS-GOAL conversation)
                (HAS-MOSS-SUB-DIALOG-HEADER conversation))
          (HAS-MOSS-FRAME-LIST conversation))
    ;; record current sub-conversation
    (send conversation '=replace 'HAS-MOSS-SUB-DIALOG-HEADER (list dialog-header))
    ;; if we have an action, then set up the corresponding goal
    (if (HAS-MOSS-ACTION dialog-header)
        (send conversation '=replace 'HAS-MOSS-GOAL 
              (list (send (car (HAS-MOSS-ACTION dialog-header)) '=create-pattern))))
    ;; replace conversation header by entry state
    (setq state (car (HAS-MOSS-ENTRY-STATE dialog-header)))
    ;(break)
    ;; do the actual transition
    (make-transition conversation current-state state)
    ))

;;;------------------------------------------------------------ PRINT-MOSS-STATE

#|
(defun print-MOSS-state (conversation)
  "prints a summary of the content of the context-state pointed to by the ~
      conversation object.
Arguments:
   conversation: conversation object
Return:
   :done"
  (let* ((context (car (has-q-context conversation)))
         (state (car (has-state-object context)))
         )
    (vformat "context (~S)~%  STATE:~20T~{~S~^, ~}~%  ~{~S~^, ~}~
              ~%  DATA:~20T~{~S~^, ~}~%  PERFORMATIVE:~20T~{~S~^, ~}~
              ~%  RESULTS:~20T~{~S~^, ~}~%  TASK:~20T~{~S~^, ~}~
              ~%  TASK-LIST:~20T~{~S~^, ~}~%"
             context
             (has-label state)
             (has-explanation state)
             (has-data context)
             (has-performative context)
             (has-results context)
             (has-task context)
             (has-task-list context)
             )
    :done))
|#
;;;------------------------------------------------------------ PROCESS-PATTERNS
;;; dealing with simplified patterns 1105

(defUn process-patterns (input pattern-list)
  "takes a list representing test and a set of ELIZA-type rules and returns the ~
      bindings associated with the first rule that applies.
Arguments:
   input: a list of word strings representing text
   pattern-list: a list of patterns (ELIZA style)
Return:
   the list of bindings corresponding to the first pattern that applies ~
      or nil if it fails."
  ;(trformat "process-patterns input: ~S ~&  patterns: ~S" input pattern-list)
  (some #'(lambda (pattern) (pat-match pattern input)) 
        (mapcar #'make-pattern pattern-list)))

;;;-------------------------------------------------------- PROCESS-WHY-QUESTION

(defUn process-why-question (conversation)
  "prints info about why assistant asked the previous question.
Arguments:
   conversation: current conversation
Return:
   not significant."
  (let* ((task (car (send conversation '=get 'HAS-MOSS-TASK)))
         (sub-dialog-header (car (HAS-MOSS-SUB-DIALOG-HEADER conversation)))
         explanation)
    ;; we should print the current goal and the content of the why slot attached
    ;; to the current state
    (when task
      (moss::mformat "~%We are executing: ~A" (car (HAS-MOSS-DOCUMENTATION task))))
    ;; check if there is an explanation in the current state
    (setq explanation (HAS-MOSS-EXPLANATION (car (HAS-MOSS-STATE conversation))))
    ;; if so, print it
    (when explanation
      (moss::mformat "~%~A~%" (car explanation)))
    ;; if sub-dialog print more info
    (when sub-dialog-header
      (moss::mformat "~%Current objective: ~A"
                     (car (HAS-MOSS-EXPLANATION sub-dialog-header))))
    :done))

;;;---------------------------------------------------------- READ-FROM-LISTENER

(defUn read-from-listener (&optional read-parents)
  "read lines of text until a period or a question mark is found. Uses readline, ~
   concatenating the lines and then reading from the resulting string.
Arguments:
   read-parents (opt): if t checks if parents are balanced, if nil replaces parents 
                                  with space
Return:
   a list or nil if read-from-string fails."
  (let (line line-list text)
    (loop
      (setq line (read-line))
      (nsubstitute #\space #\, line)
      (nsubstitute #\space #\; line)
      ;(nsubstitute #\space #\: line)
      (unless read-parents
        (nsubstitute #\space #\( line)
        (nsubstitute #\space #\) line))
      (nsubstitute #\space #\, line)
      (nsubstitute #\space #\# line)
      (nsubstitute #\space #\' line)
      (nsubstitute #\space #\" line)
      ;(print line)
      ;; if dot or question mark or exclamation mark, end of text
      (when (position  #\. line)
        (nsubstitute #\space #\. line)
        (push line line-list)
        (return nil))
      (when (position  #\? line)
        (nsubstitute #\space #\? line)
        (push line line-list)
        (return nil))
      (when (position  #\! line)
        (nsubstitute #\space #\! line)
        (push line line-list)
        (return nil))
      (push line line-list)
      ;; add space between 2 lines
      (push " " line-list))
    ;; make a single list
    (setq text (apply #'concatenate 
                      (if read-parents
                          `(string ,@(reverse line-list))
                        `(string ,@(reverse line-list)))))
    (if (and read-parents (not (eql (count #\( text) (count #\) text))))
        (progn
          (verbose-warn "unbalanced parenthesis in expression~& ~A" text)
          nil)
      text)))

;;;-------------------------------------------------------- RESTART-CONVERSATION
#|
Example of input conversation object
----- $CVSE.1
 OUTPUT-WINDOW: #<SCROLLING-FRED-VIEW #x2E676F6>
 INPUT-WINDOW: #<OMAS-DIALOG-ITEM #x2E68AAE>
 AGENT: #<AGENT STEVENS>
 FRAME-LIST: ($QSTE.34 $QSTE.34 $QHDE.14 NIL)
 STATE: Aborting
 INITIAL-STATE: Dialog start
 Q-CONTEXT: $SCXTE.21
 DIALOG-HEADER: $QHDE.9
 GOAL: $PAPE.1
|#

(defUn restart-conversation (conversation-id)
  "restarts a conversation. Uses the conversation object to restart at the ~
   beginning. Reinitializes with initial state. 
Arguments:
   conversation: conversation to be restarted
Return:
   the id of the conversation object."
  (let* ((initial-state (car (HAS-MOSS-ENTRY-STATE 
                              (car (HAS-MOSS-DIALOG-HEADER conversation-id))))))
    
    ;; keep input and output channels
    ;; keep DIALOG-HEADER 
    ;; reset frame-list, goal, facts
    (send 'moss::$FLT '=delete-all conversation-id)
    (send 'moss::$GLS '=delete-all conversation-id)
    (send 'moss::$FCT '=delete-all conversation-id)
    ;; copy initial state into STATE (current state)
    (send conversation-id '=replace 'HAS-MOSS-STATE (list initial-state))
    
    (if *transition-verbose*
        (format *trace-output* "~&===== Restarting conversation with state: ~S" 
          (car (has-MOSS-label initial-state))))
    ;;*** debug
    ;(print-conversation conversation-id)
    ;;*** end debug
    ;; go execute the new state with the associated context
    #+OMAS
    (dialog-engine conversation-id initial-state)
    #-OMAS
    (send initial-state '=execute conversation-id)
    ;; return the id of the conversation object
    conversation-id))

#|
(step (moss::restart-conversation 'stevens::$CVSE.1))
|#
;;;--------------------------------------------------------- RESUME-CONVERSATION
;;;********** must be checked....

(defUn resume-conversation (conversation)
  "transfers the control to the current conversation state. If no context is present ~
   then creates a new one using data and sends an =execute message to the conversation ~
   state. Otherwise, sends a =resume ~message to conversation state, with data as ~
   arguments. 
Arguments:
   conversation: conversation
   data: data found in the to-do bin."
  (let* ((state (car (has-MOSS-state conversation)))
         )
    (if *transition-verbose*
        (format *trace-output* "~&===== Resuming conversation with state: ~A" 
          (car (send state '=get 'HAS-MOSS-LABEL))))
    ;; when we have no state, we start at the beginning of the conversation
    (unless state 
      (return-from resume-conversation
        (restart-conversation conversation)))
    ;;*** debug part
    ;(print-conversation conversation)
    ;;*** end debug part
    ;; go execute the new state with the associated context
    (send state '=resume conversation)
    :done))

;;;------------------------------------------------- EXEC-RETURN-FROM-SUB-DIALOG

(defUn exec-return-from-sub-dialog (conversation fail/success)
  "returns from a sub-dialog. Get the frame list from the conversation object ~
   pops it, reinstall the old goal if any, the corresponding state context, ~
   and return the fail/success state that can be keywords.
   When the frame list is empty resets the conversation by throwing to dialog ~
   error.
Arguments:
   conversation: current conversation
   fail/success: a keyword indicating failure or success
Return:
   a state to go to or :failure or :success"
  (let ((frame-list (HAS-MOSS-FRAME-LIST conversation))
        ;(state-context (has-q-context conversation))
        frame)
    ;(trformat "exec-return-from-sub-dialog /frame-list IN: ~%  ~S" 
    ;          (has-moss-frame-list conversation))
    ;; if frame-list is empty, reset dialog
    (unless frame-list (restart-conversation conversation))
    ;; otherwise get frame (<fail> <success> <context> <goal> <sub-dial-hdr>)
    (setq frame (pop frame-list))
    ;; update frame-list
    (setf (HAS-MOSS-FRAME-LIST conversation) frame-list)
    ;; frame looks like (success failure state goal sub-dialog-header)
    (send conversation '=replace 'HAS-MOSS-GOAL (nth 3 frame))
    ;; reinstall previous state-context (in case we start an alternative task)
    ;; must not fo that on normal returns (loss of results)
    ;(send conversation '=replace 'HAS-Q-CONTEXT (list (nth 2 frame)))
    ;; reset previous sub-dialog header
    (send conversation '=replace 'HAS-MOSS-SUB-DIALOG-HEADER (nth 4 frame))
    ;(trformat "exec-return-from-sub-dialog /frame-list OUT: ~%  ~S" 
    ;          (has-frame-list conversation))
    ;; return ad hoc state or keyword to set up new transition
    (case fail/success
      (:success (set-state-for-subdialog (car frame)))
      (:failure (set-state-for-subdialog (cadr frame)))
      (otherwise 
       (error "bad fail/success argument when returning from sub-dialog")))
    ))

;;;-------------------------------------------------------- SEGMENT-MASTER-INPUT
;;; Here we should do segmentation and stemming and other things...

(defUn segment-master-input (agent text)
  "Get the raw input text as a string and segment it according to the language. ~
   Return a list of words.
Arguments:
   agent: CLOS object, e.g. PA_ALBERT
   text: a string containing raw text.
Return:
   a list of words or nil if empty."
  #+OMAS
  (let ((language (or (omas::language agent) *language*))
        (trimmed-text  (string-trim '(#\space #\. #\?) text)))
    ;; if the input stirng is one of the abort commands pass it as such
    ;(dformat :dialog 1 "~%; segment-master-input /trimmed text: ~S" trimmed-text)
    (if (member trimmed-text *abort-commands* :test #'string-equal)
        (return-from segment-master-input (list trimmed-text)))
    
    ;; currently the same process for every language
    (case language
      (:jp
       ;; for Japanese, suffixes should be removed from the words (stemming)
       (%get-words-from-japanese-text text omas::*japanese-delimiters*))
      (otherwise
       ;; keep "?" 
       (%get-words-from-text text *delimiters* '(#\?))))
    )
  #-OMAS
  (declare (ignore agent))
  #-OMAS
  (%get-words-from-text text *delimiters*)
  )

#|
? (%get-words-from-text "I thintk, that i'd like to have dominique's phone number.  " 
      '(#\space #\, #\' #\.))
("I" "think" "that" "i" "d" "like" "to" "have" "dominique" "s" "phone" "number")
? (moss::%get-words-from-text "hello" moss::*delimiters*)
("hello")
? (moss::%get-words-from-text "define address ?" moss::*delimiters*)
("define" "address" "?")
? (segment-master-input hdsri::PA_hdsri ":abort")
(":abort")
|#
;;;-------------------------------------------------------------------- SET-DATA
;;; context ojects have been suppressed...

(defUn set-data (conversation data)
  "set data part of the context object in a conversation state.
Arguments:
   conversation: conversation.
   data: data to store into the context slot"
  (let ((moss-context (send-no-trace conversation '=get 'has-MOSS-context)))
    ;(trformat "set-data /state-context ~S ~%" q-context )
    (send (car moss-context ) '=replace 'has-data (list data))
    ;(%pep (car q-context ))
    ))

;;;----------------------------------------------------- SET-STATE-FOR-SUBDIALOG

(defUn set-state-for-subdialog (state-ref)
  "get the state to return to when returning from a subdialog. State can be ~
   a keyword (:success :failure :return) a state objects ($QSTE.nn), or ~
   the name of a state object (_q-more?). In the later case we must get ~
   its value.
Arguments:
   state-ref: reference to a virtual (keyword) ar real state object
Return:
   keyword of state object, error if not a valid state ref."
  (cond
   ((member state-ref '(:success :failure :return)) state-ref)
   ((and (%pdm? state-ref)(%type? state-ref '$QSTE)) state-ref)
   ((and (symbolp state-ref)
         (%pdm? (symbol-value state-ref))
         (%type? (eval state-ref) '$QSTE))
    (symbol-value state-ref))
   (t (error "bad state reference: ~S" state-ref))))

#|
? (set-state-for-subdialog :failure)
:FAILURE

? (set-state-for-subdialog '$QSTE.2)
$QSTE.2

? (set-state-for-subdialog _q-process)
$QSTE.4

? (set-state-for-subdialog :fail)
> Error: bad state reference: :FAIL
> While executing: SET-STATE-FOR-SUBDIALOG
|#
#|
;;;----------------------------------------------------------------- SHOW-ACTION

(defun show-action (action &key (doc t))
  "prints the current content of an action. Debugging function.
Arguments:
   action: identifier of the action
   doc (key): if t print the documentation associated with the action (default)
Return:
   :done"
  (let* ((arg-list (HAS-OPERATOR-ARGUMENTS action)))
    (format t "~%==========")
    (format t "~%action name: ~S" 
      (or (car (HAS-Q-ACTION-NAME action))
          (car (HAS-CONCEPT-NAME (car (%type-of action))))))
    (if doc (format t "~&  ~A" 
              (or (car (HAS-Q-DOCUMENTATION action))
                  (car (HAS-Q-DOCUMENTATION (car (%type-of action)))))))
    (format t "~%  ~S" (car (HAS-Q-OPERATOR action)))
    (dolist (item arg-list)
      (format t "~&    ~S : ~S" (car (HAS-Q-ARG-KEY item))
        (car (HAS-Q-ARG-VALUE item))))
    (format t "~%==========")
    :done))
|#
;;;----------------------------------------------------------- SHOW-CONVERSATION

#|
(defun show-conversation (source &key q-context (explain t))
  "prints a trace of the states that were traversed during a conversation.  ~
   Debugging function.
Arguments:
   source: the source of the conversation
   q-context (key): current q-context of the conversation
   explain (key): if t explanations are printed for each state
Return:
   :done"
  (let* (q-context path conversation state)
    ;; first determine the source of the conversation, in standalone MOSS
    ;; it is the system, in OMAS it is an agent if specified
    ;#+OMAS
    (when (member :OMAS *features*)
      (setq conversation
            (if (typep source 'agent) 
                (omas::conversation source) 
              source)))
    ;; determine context and path
    (setq q-context (or q-context (car (HAS-Q-CONTEXT conversation))))
    (setq path (%sp-gamma q-context (%inverse-property-id '$NSTS)))
    
    ;#-OMAS
    (unless (member :OMAS *features*)
      (setq conversation source))
    (format t "~%==========")
    (dolist (item (reverse path))
      (setq state (car (HAS-STATE-OBJECT item)))
      (unless (%type? state '$QSTE)
        (error "bad state reference ~S in q-context: ~S" state item))
      ;; print state label
      (format t "~%state: ~S" (car (HAS-MOSS-LABEL state)))
      ;; print explanations
      (if explain (format t "~&  ~A" (car (HAS-EXPLANATION state))))
      ;; then print context info
      (format t "~&  data: ~S~&  results: ~S" 
        (car (HAS-DATA item))
        (car (HAS-RESULTS item)))
      (format t "~%==========")
      )
    :done))
|#

;;;---------------------------------------------------------- START-CONVERSATION
;;; start-conversation is executed asynchronously in a specific process
;;; so that we can wait a few seconds to let the dialog file be loaded
;;; In the case of a web interaction with an OMAS assistant, then the agent has
;;; no window and the connection may not be yet achieved between the PA and the
;;; web browser. Thus it is not a good idea to post a welcome message until we
;;; are sure where to post it. The function should simply start the dialog and 
;;; wait for some input (posted in the TO-DO slot of the PA).

(defUn start-conversation 
    (dialog-header &key first-time data (input t) (output t) agent)
  "start a conversation. Creates a new conversation object. Initialize ~
   with initial state. Initialize I/O windows. Create context.
Arguments:
   dialog-header: header specifying the dialog
   first-time (key): when true it is our first time around
   data (key): data to be inserted into the context data slot
   input (key): input channel (a pane - default is t for listener)
   output (key): output channel (a pane - default is t for listener)
   agent (key): the id of a personal assistant agent
Return:
   the id of the conversation object."
  (declare (ignore first-time #-OMAS agent))
  
  ;;===== code for OMAS
  #+OMAS
  (progn
    (unless (%type? dialog-header '$QHDE)
      (error "unknown conversation reference ~S" dialog-header))
    (let* ((conversation-id (m-make-instance 'MOSS-CONVERSATION))
           (initial-state (car (HAS-MOSS-ENTRY-STATE dialog-header))))
      ;; if DATA is specified, we initialize INPUT
      (when data
        (replace-fact conversation-id :input data))
      ;; record initial state
      (send conversation-id '=add-sp 'HAS-MOSS-INITIAL-STATE (list initial-state))
      ;; record it as current state
      (send conversation-id '=add-sp 'HAS-MOSS-STATE (list initial-state))
      ;; initialize I/O channels
      (setf (has-moss-input-window conversation-id) (list input))
      (setf (has-moss-output-window conversation-id) (list output))
      (setf (has-moss-agent conversation-id) (list agent))
      (setf (has-moss-dialog-header conversation-id) (list dialog-header))
      (if *transition-verbose*
          (format *trace-output* "~&===== Starting conversation with state: ~S" 
            (car (has-moss-label initial-state))))
      ;; set global variable to initial conversation state
      ;; in the case of a PA, the reference is in the PA structure, since we could
      ;; run several PAs in the same environment (not recommended though!)
      (if agent
          (setf (omas::conversation agent) conversation-id)
        (setq *conversation* conversation-id))
      ;;*** debug part
      ;(when *dialog-verbose*
      ;  (pc)
      ;(format *trace-output* "~&New context:")
      ;(when *trace-output*
      ;  (send c-context-id '=print-object :stream *trace-output*))
      ;  )
      ;;*** end debug part
      
      ;(break "moss:start-conversation /conversation-id: ~S" conversation-id)      
      
      ;; go execute the new state with the associated context
      (dialog-engine conversation-id initial-state)
      ;; record current conversation and return the id of the conversation object
      (return-from start-conversation conversation-id)))
  
  ;;===== here the code for MOSS only
  #-OMAS
  (let ((*package* *package*) ; for MOSS dialog on user application
        *conversation*) ; global specific to each conversation process
    (declare (special *conversation*))
    (unless (%type? dialog-header '$QHDE)
      (error "unknown conversation reference ~S" dialog-header))
    (let* ((conversation-id (m-make-instance 'MOSS-CONVERSATION))
           (initial-state (car (HAS-MOSS-ENTRY-STATE dialog-header))))
      ;; if DATA is specified, we fill the DATA area with the value
      (when data
        (replace-fact conversation-id :input data))
      ;; record initial state
      (send conversation-id '=add-sp 'HAS-MOSS-INITIAL-STATE (list initial-state))
      ;; record it as current state
      (send conversation-id '=add-sp 'HAS-MOSS-STATE (list initial-state))
      ;; initialize I/O channels
      (trformat "start-conversation /input channel: ~S~&   output-channel: ~S"
                input output)
      (setf (has-moss-input-window conversation-id) (list input))
      (setf (has-moss-output-window conversation-id) (list output))
      (trformat "start-conversation /conversation-id ~S~&   value: ~S"
                conversation-id (eval conversation-id)) ; JPB0802
      
      (setf (has-MOSS-dialog-header conversation-id) (list dialog-header))
      (if *transition-verbose*
          (format *trace-output* "~&===== Starting conversation with state: ~S" 
            (car (has-moss-label initial-state))))
      ;; set global variable to initial conversation state
      ;; in the case of a PA the reference is in the PA structure, since we could
      ;; run several PAs in the same environment (not recommended though!)
      (setq *conversation* conversation-id)
      ;;*** debug part
      (when *dialog-verbose*
        (pc)
        ;(format *trace-output* "~&New context:")
        ;(when *trace-output*
        ;  (send c-context-id '=print-object :stream *trace-output*))
        )
      ;;*** end debug part
      ;; go execute the new state with the associated context
      (dialog-engine conversation-id initial-state)
      ;; record current conversation and return the id of the conversation object
      conversation-id)))


;;;------------------------------------------------------------------- TRIM-DATA

(defUn trim-data (word-list data)
  "remove all words of the word-list from the data list. Keep the order.
Arguments:
   word-list: list of words to remove from data
   data: list of words to be cleaned.
Return:
   list without the trimmed words."
  (cond
   ((null word-list) data)
   ((null data) nil)
   ((and (stringp (car data))(member (car data) word-list :test #'string-equal))
    (trim-data word-list (cdr data)))
   ((member (car data) word-list)
    (trim-data word-list (cdr data)))
   (t (cons (car data) (trim-data word-list (cdr data))))))


;;;================================================================================
;;;                       
;;;               Peter Norvig ELIZA Pattern matcher
;;;
;;;================================================================================

;;;================================================================== DEFELIZARULES
;;; the following macro allows using a simplified format for the rules and produces
;;; the standard Norvig format. An ELIZA rule has a single pattern and several
;;; possible conclusions.

(defmacro defelizarules (&rest rules)
  "takes a set of simplified rules and expands it to standard format"
  `(mapcar #'(lambda (xx) (cons (make-pattern (car xx)) (cdr xx))) ',rules))

#|
(defelizarules
    (((* "were" "you" ?y)
      "Perhaps I was ?y"
      "What do you think ?"
      "What if I had been ?y ?")
     ((* "I" "can't" ?y)
      "Maybe you could ?y now"
      "What if you could ?y ?")
     ((* "I" "feel" ?y)
      "Do you often feel ?y ?")))
((((?* :*) "were" "you" (?* ?Y)) "Perhaps I was ?y" "What do you think ?" "What if I had been ?y ?")
 (((?* :*) "I" "can't" (?* ?Y)) "Maybe you could ?y now" "What if you could ?y ?")
 (((?* :*) "I" "feel" (?* ?Y)) "Do you often feel ?y ?"))
|#
;;;=========================================================== DEFCOMPLEXELIZARULES
;;; same but allow expressions to be evaluated, e.g.
;;; (make-complex-pattern 
;;;     '(<> (* "were" < ?y) (symbol-name (omas::name albert::PA_ALBERT))))

(defmacro defcomplexelizarules (&rest rules)
  "takes a set of simplified rules and expands it to standard format"
  `(mapcar #'(lambda (xx) (cons (make-complex-pattern (car xx))
                                (mapcar #'make-complex-action (cdr xx))))
     ',rules))

;;;================================================================================

;;; Set up special space
; (unless (find-package :matcher)(create-package :matcher))
; (in-package :matcher)

;;;
(defConstant fail nil "Indicates pat-match failure")
(defConstant no-bindings '((t . t)) "Indicates pat-match success, with no variables.")

;;;-------------------------------------------------------------------- GET-BINDING

(defUn get-binding (var bindings)
  "Find a (variable . value) pair in a binding list."
  (assoc var bindings))

;;;-------------------------------------------------------------------- BINDING-VAL

(defUn binding-val (binding)
  "Get the value part of a single binding."
  (cdr binding))

;;;------------------------------------------------------------------------- LOOKUP
;;; adding inline directive

(defUn lookup (var bindings)
  "Get the value part (for var) from a binding list."
  (declare (inline get-binding binding-val))
  (binding-val (get-binding var bindings)))

;;;---------------------------------------------------------------- EXTEND-BINDINGS

(defUn extend-bindings (var val bindings)
  "Add a (var . value) pair to a binding list."
  (cons (cons var val)
        ;; Once we add a "real" binding,
        ;; we can get rid of the dummy no-bindings
        (if (and (eq bindings no-bindings))
            nil
          bindings)))

;;;------------------------------------------------------------ MAKE-COMPLEX-ACTION
;;; this function allows composing strings statically when defining ELIZA rules
;;; the answer is either a string or a list like
;;; (<> "Bonjour, je suis ~A!" (omas::key (omas::agent-being-loaded *omas*))

(defun make-complex-action (answer)
  "allows format expressions as possible ELIZA answers, which is quite limited."
  (cond
   ;; noop if a string
   ((stringp answer) answer)
   ((and (listp answer)(eql (car answer) '<>))
    (apply #'format nil (cadr answer)(mapcar #'eval (cddr answer))))
   ;; anything else is an error
   (t "?")))
  
;;;----------------------------------------------------------- MAKE-COMPLEX-PATTERN
;;; this function allows inserting expressions to evaluate into ELIZA rules. 
;;; Evaluating each argument must return a list of strings.

(defun make-complex-pattern (pattern)
  (if (eql (car pattern) '<>)
      (let ((pat (cadr pattern))(arg-list (cddr pattern)))
        (reduce 
         #'append
         (mapcar #'(lambda (xx)
                     (cond ((skip-symbol-p xx) '((?* :*)))
                           ((variable-p xx) `((?* ,xx)))
                           ((stringp xx) (list xx))
                           ((listp xx) (list xx))
                           ((eql xx '<) (eval (pop arg-list)))
                           (t (error "bad pattern format: ~S" pattern))))
           pat)))
    (make-pattern pattern)))

#|
(make-complex-pattern '(<> (* "were" < ?y) '( "me" "albert")))
((?* :*) "were" "albert" (?* ?Y))
(make-complex-pattern '(* "were" "you" ?y))
((?* :*) "were" "you" (?* ?Y))
(make-complex-pattern '(<> (* "were" < ?y) 
                           (list (symbol-name (omas::name albert::PA_ALBERT)))))
((?* :*) "were" "PA_ALBERT" (?* ?Y))
|#
;;;------------------------------------------------------------------- MAKE-PATTERN
;;; function to allow simpler pattersn
;;;  (* "je" "vous" "ai" "compris" ?x)
;;; is transformed into
;;;  ((?* :*) "je" "vous" "ai" "compris" (?* ?x))
;;; the reason for not simplifying further the input to
;;;  (* "je vous ai compris" ?x)
;;; is related to Japanese and Chinese segmentation. Thus, we already segment the 
;;; pattern
;;; Former patterns are left unchanged by make-pattern

(defUn make-pattern (pattern)
    (mapcar #'(lambda (xx) 
                (cond ((skip-symbol-p xx) '(?* :*))
                      ((variable-p xx) `(?* ,xx))
                      ((stringp xx) xx)
                      ((listp xx) xx)
                      (t (error "bad pattern format: ~S" pattern))))
      pattern))

#|
? (make-pattern '(* "j" "ai" "compris" "que" "vous" ?x))
((?* :*) "j" "ai" "compris" "que" "vous" (?* ?X))

? (make-pattern '((?* ?Y) "j" "ai" "compris" "que" "vous" (?* ?X)))
((?* ?Y) "j" "ai" "compris" "que" "vous" (?* ?X))
|#

;;;----------------------------------------------------------------- MATCH-VARIABLE
;;; adding :*

(defUn match-variable (var input bindings)
  "Does VAR match input? Uses or updates and returns bindings."
  (let ((binding (get-binding var bindings)))    
    (cond ((eql var :*) bindings)
          ((null binding) (extend-bindings var input bindings))
          ((same-p input (binding-val binding)) bindings)
          (t fail))))

;;;---------------------------------------------------------------------- PAT-MATCH

(defUn pat-match (pattern input &optional (bindings no-bindings))
  "Match pattern against input in the context of bindings."
  (cond
   ((eq bindings fail) fail)
   ((variable-p pattern)
    (match-variable pattern input bindings))
   ;((prog1 nil (print `(+++ pattern ,pattern input ,input))))
   ((or (and (stringp pattern)(stringp input)(string-equal pattern input))
        (eql pattern input)) bindings)
   ((segment-pattern-p pattern)
    (segment-match pattern input bindings))
   ((and (consp pattern)(consp input))
    (pat-match (rest pattern) (rest input)
               (pat-match (first pattern) (first input) bindings)))
   (t fail)))

#|
;;; tests
(pat-match '((?* ?x) "I" "want" (?* ?y)) '("Now" "i" "want" "sugar"))
(pat-match '((?* ?x) "I" "want" (?* ?y)) '("I" "want" "sugar"))
(pat-match '((?* ?x) "I" "want" (?* ?y)) '("I" "want"))
(pat-match '((?* ?x) "I" "want" (?* ?y)) '("want"))
(pat-match '((?* ?x) "I" (?* ?y)) '("I"))
(pat-match '((?* ?x) "I" (?* ?y)) '("i"))
(pat-match '((?* ?x) "I" (?* ?y)) '("today" "i" "TODAY"))
(pat-match '((?* ?x) "I" (?* ?x)) '("today" "i" "TODAY"))
(pat-match '((?* ?x) "I" (?* ?x)) '("today" "i" "not" "TODAY"))
|#
;;;------------------------------------------------------------------------- SAME-P

(defUn same-p (x y)
  "allows to compare strings when needed."
  (or (and (stringp x)(stringp y)(string-equal x y))
      (and (listp x)(listp y) (same-p (car x)(car y)) (same-p (cdr x)(cdr y)))
      (equal x y)))

#|
Bug:
(pat-match '((?* ?x) "I" (?* ?x)) '("today" "morning" "i" "TODAY" "afternoon"))
((?X "today" "morning"))
((?X "today" "morning"))
|#
;;;-------------------------------------------------------------- SEGMENT-PATTERN-P

(defUn segment-pattern-p (pattern)
  "Is this a segment matching pattern: ((?* var) . pat)"
  (declare (special *pattern-marker*))
  ;; it is necessary to compare marker independently from the packages where 
  ;; they have been defined 
  ;; since the marker is a symbol we should compare symbol names
  (and (consp pattern)
       (starts-with (first pattern) *pattern-marker*)))

;;;------------------------------------------------------------------ SEGMENT-MATCH
;;; This function does not work for a pattern like ((moss::?* ?x)(moss::?* :*))
;;; because it assumes that the value following the variable is a string
;;; This construct is however illegal since the first part is a segment variable


(defUn segment-match (pattern input bindings &optional (start 0))
  "Match the segment pattern ((?* var) . pat) against input."
  (let ((var (second (first pattern)))
        (pat (rest pattern)))
    (if (null pat)
        ;; if pat is exhausted, match variable against input
        (match-variable var input bindings)
      
      ;; we assume that pat starts with a constant
      ;; in other words, a pattern cannot have 2 consecutive vars
      
      ;; if pat is not nil try to locate the first element of pat in the input
      (let ((pos (position (first pat) input :start start :test #'string-equal)))
        (if (null pos)
            ;; if it does not appear, then we fail to match pat to input
            fail
          ;; if it appears, then we try to match pat with the input, starting at
          ;; the anchor position
          ;; set the bindings so that the first variable matches the beginning
          ;; of the input
          (let ((b2 (pat-match
                     pat (subseq input pos)
                     (match-variable var (subseq input 0 pos) bindings))))
            ;; if this match failed, try another moving 1 position down the input
            (if (eq b2 fail)
                (segment-match pattern input bindings (1+ pos))
              ;; otherwise return the bindings
              b2)))))))

;;;------------------------------------------------------------------ SKIP-SYMBOL-P
;;; when we do not need to store the sentence fragment

(defUn skip-symbol-p (x)
  "is x the skip symbol * ?"
  (and (symbolp x)(equal (char (symbol-name x) 0) #\*)))

;;;-------------------------------------------------------------------- STARTS-WITH

(defUn starts-with (subpattern mark)
  "Do we have a cons starting with a ?* atom?"
  (and (consp subpattern)
       (symbolp (car subpattern))
       (equal (symbol-name (car subpattern)) mark)))
#|
?(starts-with '(?* ?x) "?*")
T
?(starts-with '((?* ?x)) "?*")
nil
|#
;;;--------------------------------------------------------------------- VARIABLE-P

(defUn variable-p (x)
  "Is x a variable (a symbol beginning with '?')?"
  (and (symbolp x)(equal (char (symbol-name x) 0) #\?)))

;;;================================================================================
;;;                    ELIZA according to Peter NORVIG
;;;================================================================================

(defUn rule-pattern (rule) (car rule))
(defUn rule-responses (rule)(cdr rule))

(defUn eliza ()
  (loop
    ;(print 'eliza>)
    (format t "~A"
      (use-eliza-rules 
       (%get-words-from-text (read-from-listener) '(#\space #\, #\' #\. #\?))
       ))))

(defUn moss-eliza (input eliza-rules)
  "a version of ELIZA taylored for moss.
Arguments: 
  input: a list of atoms as a sentence
  eliza-rules: the set of rules defined for ELIZA smalltalk
Return:
   a string to be printed."
  (some #'(lambda (rule)
            (let ((result (pat-match (make-pattern (rule-pattern rule)) input)))
              (if result
                  (upgrade-answer (switch-viewpoint result)
                                  (random-elt (rule-responses rule))))))
        eliza-rules))

(defUn use-eliza-rules (input)
  (declare (special *eliza-rules*))
  (some #'(lambda (rule)
            (let ((result (pat-match (make-pattern (rule-pattern rule)) input)))
              (if result
                  (upgrade-answer (switch-viewpoint result)
                                  (random-elt (rule-responses rule))))))
        *eliza-rules*))

#|
(defun switch-viewpoint (words)
  (sublis '(("I" . "you")("you" . "I")("me" . "you")("am" . "are")) words
          :test #'(lambda (xx yy) 
                    (or (and (stringp xx)(stringp yy)(string-equal xx yy))
                        (equal xx yy)))))
|#

(defUn switch-viewpoint (words)
  (case *language* 
    (:fr
     (sublis '(("Je" . "vous")("vous" . "je")("me" . "vous")("suis" . "�tes")
               ("mon" . "votre")("ma" . "votre") ("votre" . "mon")
               ("moi" . "vous")) 
             words
             :test #'(lambda (xx yy) 
                       (or (and (stringp xx)(stringp yy)(string-equal xx yy))
                           (equal xx yy)))))
    (:en
     (sublis '(("I" . "you")("you" . "I")("me" . "you")("am" . "are")("my" . "your")
               ("your" . "my")("our" . "your")) 
             words
             :test #'(lambda (xx yy) 
                       (or (and (stringp xx)(stringp yy)(string-equal xx yy))
                           (equal xx yy)))))
    (otherwise
     words)))

(defUn replace-substrings (str old-substr new-substr)
  (let ((index (string-find str old-substr)))
    (if (> index 0)
        (format nil "~a~a~a" 
          (subseq str 0 (- index 1) )
          new-substr 
          (replace-substrings (subseq str (+ (- index 1) (length old-substr)))
                              old-substr new-substr)
          )
      str)))

(defUn string-find (string phrase)
  (if (search phrase string)
      (+ (search phrase string) 1)
    0))

(defUn upgrade-answer (alist answer)
  "takes something like ((?Y #\"sugar#\") (?X #\"Now#\" #\"you#\") (?Z)) and ~
   replaces occurences of the variables in the answer string.
Arguments:
   alist: variables and values
   answer: answer pattern
Return:
   upgraded answer string."
  (let (new-value (result answer))
    (dolist (pair alist)
      (unless (equal pair '(T . T))
        ;; for each pair replace value
        (setq new-value (if (cdr pair)(format nil "~{~A~^ ~}" (cdr pair)) ""))
        (setq result 
              (replace-substrings 
               result  (string-downcase (format nil "~A" (car pair))) new-value))))
    result))

(defUn flatten (the-list)
  (mappend #'mklist the-list))

(defUn mklist (x)
  (if (listp x) x (list x)))

(defUn mappend (fn the-list)
  (apply #'append (mapcar fn the-list)))

(defUn random-elt (choices)
  (elt choices (random (length choices))))

(format t "~%;*** MOSS v~A - dialog engine loaded (control) ***" *moss-version-number*)

;;; :EOF
