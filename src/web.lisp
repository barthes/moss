﻿;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;20/01/30		
;;;		W E B - (File moss-web.lisp)
;;;	
;;;==========================================================================

#|
Copyright: Fuckner@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

marcio.fuckner@hds.utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
-------
2013
 1121 Creation
 1128 Adapting to the MOSS environment + alphabetic order + cosmetic changes
      + export of main functions
2014
 0307 -> UTF8 encoding
2020
 0130 restructuring MOSS as an asdf system
|#

(in-package :moss)

;;;--------------------------------------------------------------------------
;;; Special functions related to the graph visualization. They
;;;--------------------------------------------------------------------------

(eval-when (:compile-toplevel :load-toplevel :execute)
  (export '(display-message
            create-graph-message
            create-envelope
            ;marshall-moss-object
            moss-list-to-json
            get-selected-attributes
            create-chart-message
            group-moss-list
            get-selected-attributes
            ))
  )

;;;---------------------------------------------------- BUILD-MOSS-OBJECT-KEY

(defun build-moss-object-key (object keys)
  "Extract properties corresponding to a group of keys. If keys are strings, ~
   they must have the same case as the properties (otherwise should use equal+).
Arguments:
  object: the source object, e.g. (class (a 1)(b 2)(c 3)(d 4))
  keys: the candidate keys, e.g. (a c)
Return
  e.g. (class (a 1)(c 3))"
  (let (obj-key)
    (dolist (attrib (cdr object))
      ;; if the property is one of the keys, extract the sublist
      (if (some #'(lambda (x) (if (equal x (car attrib)) x)) keys)
          (push attrib obj-key)))
    ;; add class
    (push (car object) obj-key)
    ;; return the result
    obj-key))

#|
(build-moss-object-key '("project" ("start" "2011") ("montant" "2000")
                         ("ann? "2011")) ("start" "ann?))
                       '("start" "montant"))
("project" ("montant" "2000") ("start" "2011"))
|#
;;;-------------------------------------------------------------- COMBINATION

"Cartesian product of lists 
  Arguments:
  lists: a set of lists used for the product operation"
(defun combinations (&rest lists)
  (if (car lists)
      (mapcan (lambda (inner-val)
                (mapcar (lambda (outer-val)
                          (cons outer-val
                                inner-val))
                        (car lists)))
              (apply #'combinations (cdr lists)))
    (list nil)))

;;;(defun expand-moss-list (symb attrs &optional label)
;;;  "Creates a de-normalized version of a tree-like representation
;;;  Arguments:
;;;  symb: the moss object
;;;  attrs: list that will be used as a filter
;;;  label: to add in front of the selected attributes"
;;;  ;;(format t "~%@expand-moss-list called symb: ~S attrs: ~S label: ~S " symb attrs label)
;;;  (let (res exp-attr)
;;;    (cond
;;;      ;; if the symbol is a list of moss objects
;;;      ((moss::is-list-obj-rep symb)
;;;        ;; iterates over the list of symbols, calling the expand function recursively
;;;        (dolist (s symb)
;;;          (setf exp-attr (expand-moss-list s attrs label))         
;;;          (if exp-attr 
;;;            (dolist (elem exp-attr)
;;;              (push elem res)
;;;            )
;;;          )
;;;        )
;;;      )
;;;      ;; if the symbol is a moss object
;;;      ((moss::is-obj-rep symb)
;;;        (let (tuple attr (has-rel nil))
;;;          (dolist (y (cdr symb))
;;;            (cond
;;;              ;; is a simple property
;;;              ((moss::is-simple-attr-rep y)
;;;                (if (some #'(lambda (x) (if (equal x (car y)) x)) attrs)
;;;                  (progn
;;;                    (setf attr nil)
;;;                    (dolist (elem (cdr y))
;;;                      ;;(format t " ~%@expand-moss-list y = ~S elem = ~S" (car y) elem)
;;;                      (push (list (car y) elem) attr)
;;;                    )
;;;                    (push attr tuple)
;;;                  )
;;;                )
;;;              )
;;;              ;; is a complex property
;;;              (t 
;;;                (let (rel)
;;;                  (setq rel (expand-moss-list (cdr y) attrs nil))
;;;                  ;;(format t " ~%@expand-moss-list rel = ~S" rel)
;;;                  (if rel
;;;                    (progn 
;;;                      (push rel tuple)
;;;                      (setf has-rel t)
;;;                    )
;;;                  )
;;;                )
;;;              )
;;;            )
;;;          )
;;;          ;;(format t " ~%@expand-moss-list tuple at the end = ~S, symb = ~S" tuple symb)
;;;          (if tuple
;;;            (progn
;;;              (setf res (reverse (apply #'combinations tuple)))
;;;              ;;(format t " ~%@expand-moss-list res = ~S tuple = ~S" res tuple)
;;;              (if label
;;;                (let (newtuple)
;;;                  (dolist (elem res)
;;;                    (push label elem)
;;;                    (push elem newtuple)
;;;                  )
;;;                  (setf res newtuple)
;;;                )
;;;              )
;;;              (if has-rel
;;;                (progn
;;;                  ;;(format t "~%@expand-moss-list has-rel is true: ~S" res)
;;;                  (let (newtuple newres)
;;;                    (dolist (line res)
;;;                      (setf newtuple nil)
;;;                      (dolist (elem line)
;;;                        (format t "~%@expand-moss-list elem: ~S" elem)
;;;                        (if (and (listp elem) (listp (car elem)))
;;;                          (mapcar #'(lambda(xx) (push xx newtuple)) elem)
;;;                          (push elem newtuple)
;;;                        )
;;;                      )
;;;                      (push (reverse newtuple) newres)
;;;                    )
;;;                    (setf res (reverse newres))
;;;                  )
;;;                )
;;;              )
;;;            )
;;;            (setf res nil)
;;;          )
;;;        )
;;;      )
;;;      (t 
;;;        (error "invalid parameter: ~S" symb)
;;;      )
;;;    )
;;;    (return-from expand-moss-list res)
;;;  )
;;;)


#|
;; unitary test
(progn
;; multiple attr values
(if (not (equal 
  (expand-moss-list 
  '(("a" ("b" "1" "2") ("c" "3" "4" "5") 
         ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1")) 
              ("d" ("e" "5.1" "5.2") ("f" "6.1"))) 
         ("g" ("g" ("h" " 10" "11"))))
    ("a" ("b" "a1" "a2") ("c" "a3" "a4" "a5") 
         ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1")) 
              ("d" ("e" "a5.1" "a5.2") ("f" "a6.1"))) 
         ("g" ("g" ("h" " a10" "a11"))))) '("b" "c") "line") 
  '(("line" ("c" "a3") ("b" "a1")) 
    ("line" ("c" "a4") ("b" "a1")) 
    ("line" ("c" "a5") ("b" "a1")) 
    ("line" ("c" "a3") ("b" "a2")) 
    ("line" ("c" "a4") ("b" "a2")) 
    ("line" ("c" "a5") ("b" "a2")) 
    ("line" ("c" "3") ("b" "1")) 
    ("line" ("c" "4") ("b" "1")) 
    ("line" ("c" "5") ("b" "1")) 
    ("line" ("c" "3") ("b" "2")) 
    ("line" ("c" "4") ("b" "2")) 
    ("line" ("c" "5") ("b" "2"))))) 
    (error "unitary test error") 
    (print "ok"))
;; empty value    
(if (not (equal 
  (expand-moss-list 
  '(("a" ("b") ("c" "3" "4" "5") 
         ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1")) 
              ("d" ("e" "5.1" "5.2") ("f" "6.1"))) 
         ("g" ("g" ("h" " 10" "11"))))
    ("a" ("b" "a1" "a2") ("c" "a3" "a4" "a5") 
         ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1")) 
              ("d" ("e" "a5.1" "a5.2") ("f" "a6.1"))) 
         ("g" ("g" ("h" " a10" "a11"))))) '("b" "c") "line")
  '(("line" ("c" "a3") ("b" "a1")) 
    ("line" ("c" "a4") ("b" "a1")) 
    ("line" ("c" "a5") ("b" "a1"))
    ("line" ("c" "a3") ("b" "a2")) 
    ("line" ("c" "a4") ("b" "a2")) 
    ("line" ("c" "a5") ("b" "a2"))
    ("line" ("c" "3")) 
    ("line" ("c" "4")) 
    ("line" ("c" "5")))))       
    (error "unitary test error") 
    (print "ok"))
;; single values
(if (not (equal
(expand-moss-list 
  '(("a" ("b" "1") ("c" "3") 
         ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1")) 
              ("d" ("e" "5.1" "5.2") ("f" "6.1"))) 
         ("g" ("g" ("h" " 10" "11"))))
    ("a" ("b" "a1") ("c" "a3") 
         ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1")) 
              ("d" ("e" "a5.1" "a5.2") ("f" "a6.1"))) 
         ("g" ("g" ("h" " a10" "a11"))))) '("b" "c") "line")
  '(("line" ("c" "a3") ("b" "a1")) ("line" ("c" "3") ("b" "1")))))
    (error "unitary test error") 
    (print "ok"))
;; using relation    
(if (not (equal
  (expand-moss-list 
    '(("a" ("b" "1" "2") ("c" "3" "4" "5") 
          ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1")) 
               ("d" ("e" "5.1" "5.2") ("f" "6.1"))) 
          ("g" ("g" ("h" " 10" "11"))))
      ("a" ("b" "a1" "a2") ("c" "a3" "a4" "a5") 
           ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1")) 
                ("d" ("e" "a5.1" "a5.2") ("f" "a6.1"))) 
           ("g" ("g" ("h" " a10" "a11"))))) '("b" "f") "line")
    '(("line" ("f" "a2.1") ("b" "a1")) 
      ("line" ("f" "a6.1") ("b" "a1")) 
      ("line" ("f" "a2.1") ("b" "a2"))
      ("line" ("f" "a6.1") ("b" "a2")) 
      ("line" ("f" "2.1") ("b" "1")) 
      ("line" ("f" "6.1") ("b" "1"))
      ("line" ("f" "2.1") ("b" "2")) 
      ("line" ("f" "6.1") ("b" "2")))))
    (error "unitary test error") 
    (print "ok"))  
;; multiple relation    
(if (not (equal
  (expand-moss-list 
    '(("a" ("b" "1" "2") ("c" "3" "4" "5")
           ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1"))
                ("d" ("e" "5.1" "5.2") ("f" "6.1")))
           ("g" ("g" ("h" " 10" "11"))))
      ("a" ("b" "a1" "a2") ("c" "a3" "a4" "a5")
           ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1"))
                ("d" ("e" "a5.1" "a5.2") ("f" "a6.1")))
           ("g" ("g" ("h" " a10" "a11"))))) '("b" "e") "line")
  '(("line" ("e" "a1.1") ("b" "a1")) 
   ("line" ("e" "a1.2") ("b" "a1")) 
   ("line" ("e" "a5.1") ("b" "a1"))
   ("line" ("e" "a5.2") ("b" "a1")) 
   ("line" ("e" "a1.1") ("b" "a2")) 
   ("line" ("e" "a1.2") ("b" "a2"))
   ("line" ("e" "a5.1") ("b" "a2")) 
   ("line" ("e" "a5.2") ("b" "a2")) 
   ("line" ("e" "1.1") ("b" "1"))
   ("line" ("e" "1.2") ("b" "1")) 
   ("line" ("e" "5.1") ("b" "1")) 
   ("line" ("e" "5.2") ("b" "1"))
   ("line" ("e" "1.1") ("b" "2")) 
   ("line" ("e" "1.2") ("b" "2")) 
   ("line" ("e" "5.1") ("b" "2"))
   ("line" ("e" "5.2") ("b" "2")))))
    (error "unitary test error") 
    (print "ok"))
)    
|#

;;;----------------------------------------------------  CREATE-CHART-MESSAGE
#|
(defun create-chart-message 
    (chart-type categories group-function attribute content)
  "Creates a chart message that will be usually sent to a web browser.
Arguments:
  chart-type: pie, line, column for example
  categories: list of candidate attributes
  group-function: sum, avg for example
  attribute: target attribute
  content: the underlying data
"
  (list "pa-chart-message" 
        (list "chart-type" chart-type)
        (cons "categories" categories)
        (list "group-function" group-function)	
        (list "attribute" attribute)	
        (cons "content" content)))
|#

(defun create-chart-message 
  (chart-type categories group-function attribute content &optional series-selector chart-title)
  "Creates a chart message that will be usually sent to a web browser
  Arguments:
  chart-type: pie, line, column for example
  categories: list of candidate attributes
  group-function: sum, avg for example
  attribute: target attribute
  content: the underlying data
  series selector"
  (let (message) 
    (push (cons "content" content) message)
    (if (not (eq chart-title nil))
        (push (list "chart-title" chart-title) message))
    (if (not (eq series-selector nil))
        (push (list "series-selector" series-selector) message))
    (push (list "attribute" attribute) message)
    (push (list "group-function" group-function) message)
    (push (list "chart-type" chart-type) message)
    (push (cons "categories" categories) message)
    (push "pa-chart-message" message)
    (return-from create-chart-message message)
  )
)

#|
(create-chart-message "pie" '("year" "month") "sum" "net-value"
 '(("sales" ("year" "2012") ("month" "December") ("net-value" "100520.32"))
   ("sales" ("year" "2013") ("month" "January") ("net-value" "252000.00"))))
("pa-chart-message" ("chart-type" "pie") ("categories" "year" "month") 
 ("group-function" "sum") ("attribute" "net-value")
 ("content" ("sales" ("year" "2012") ("month" "December") ("net-value" "100520.32"))
  ("sales" ("year" "2013") ("month" "January") ("net-value" "252000.00"))))
|#	
;;;---------------------------------------------------------- CREATE-ENVELOPE
	
(defun create-envelope (sender messages)
  "Creates an envelope to send to the end-user.
Arguments:
  sender: the sender agent
  messages: the list of message objects
"
  (list "pa-message-envelope"
        (list "sender" sender)
        (list "timestamp" (write-to-string (get-universal-time)))	
        (list "payload" messages)))

#|
(create-envelope
 "ALBERT"
 (create-chart-message 
  "pie" '("year" "month") "sum" "net-value"
  '(("sales" ("year" "2012") ("month" "December") ("net-value" "100520.32"))
    ("sales" ("year" "2013") ("month" "January") ("net-value" "252000.00")))))
("pa-message-envelope" ("sender" "ALBERT") ("timestamp" "3594713083")
 ("payload"
  ("pa-chart-message" ("chart-type" "pie") ("categories" "year" "month") ("group-function" "sum")
   ("attribute" "net-value")
   ("content" ("sales" ("year" "2012") ("month" "December") ("net-value" "100520.32"))
    ("sales" ("year" "2013") ("month" "January") ("net-value" "252000.00"))))))

(create-envelope 
 "ALBERT"
 (create-graph-message 
  '(("projet" ("id" "PROJECT/$E-PROJECT.202") ("pays" "Brésil")
     ("titre" "MOBYDICK") ("sigle" "MDK") ("débutt" "2012") ("fin" "2015")
     ("participants" "Thouvenin, I.") ("domaine" "GI"))
    ("projet" ("id" "PROJECT/$E-PROJECT.218") ("pays" "Belgique")
     ("titre" "NIKITA") ("sigle" "NKT") ("début" "2010") ("fin" "2013")
     ("participants" "Lourdeaux, D.") ("domaine" "BS"))
    ("projet" ("id" "PROJECT/$E-PROJECT.22") ("pays" "France")
     ("titre" "ARAKIS") ("sigle" "ARA") ("début" "2010") ("fin" "2013")
     ("participants" "Lourdeaux, D." "Lenne, D.") ("domaine" "BIO")))))
("pa-message-envelope" ("sender" "ALBERT") ("timestamp" "3663309693")
 ("payload"
  ("pa-graph-message"
   ("content"
    ("projet" ("id" "PROJECT/$E-PROJECT.202") ("pays" "Brésil") ("titre" "MOBYDICK") ("sigle" "MDK")
     ("débutt" "2012") ("fin" "2015") ("participants" "Thouvenin, I.") ("domaine" "GI"))
    ("projet" ("id" "PROJECT/$E-PROJECT.218") ("pays" "Belgique") ("titre" "NIKITA") ("sigle" "NKT")
     ("début" "2010") ("fin" "2013") ("participants" "Lourdeaux, D.") ("domaine" "BS"))
    ("projet" ("id" "PROJECT/$E-PROJECT.22") ("pays" "France") ("titre" "ARAKIS") ("sigle" "ARA")
     ("début" "2010") ("fin" "2013") ("participants" "Lourdeaux, D." "Lenne, D.") ("domaine" "BIO"))))))
|#
;;;----------------------------------------------------  CREATE-GRAPH-MESSAGE
	
;;;(defun create-graph-message (content)
;;;  "Creates a graph message, usually for browser rendering.
;;;Arguments:
;;;  content: The graph using the moss object representation"
;;;  (list "pa-graph-message" 
;;;        (cons "content" content)))

;;; redefining function for the use of orientation option in HDSRI-R-DIALOG

(defun create-graph-message (content &optional orientation)
  "Creates a graph message, usually for browser rendering
Arguments:
  content: The graph using the moss object representation"
  (cond
   (orientation
    (list "pa-graph-message" 
          (list "orientation" orientation)
          (cons "content" content)))
   (t 
    (list "pa-graph-message" 
          (cons "content" content)))))

#|
(create-graph-message 
   '(("projet" ("id" "PROJECT/$E-PROJECT.202") ("pays" "Br?l")
     ("titre" "MOBYDICK") ("sigle" "MDK") ("d?t" "2012") ("fin" "2015")
     ("participants" "Thouvenin, I.") ("domaine" "GI"))
    ("projet" ("id" "PROJECT/$E-PROJECT.218") ("pays" "Belgique")
     ("titre" "NIKITA") ("sigle" "NKT") ("d?t" "2010") ("fin" "2013")
     ("participants" "Lourdeaux, D.") ("domaine" "BS"))
    ("projet" ("id" "PROJECT/$E-PROJECT.22") ("pays" "France")
     ("titre" "ARAKIS") ("sigle" "ARA") ("d?t" "2010") ("fin" "2013")
     ("participants" "Lourdeaux, D." "Lenne, D.") ("domaine" "BIO"))))
("pa-graph-message"
 ("content"
  ("projet" ("id" "PROJECT/$E-PROJECT.202") ("pays" "Br?l") ("titre" "MOBYDICK")
   ("sigle" "MDK")
   ("d?t" "2012") ("fin" "2015") ("participants" "Thouvenin, I.") ("domaine" "GI"))
  ("projet" ("id" "PROJECT/$E-PROJECT.218") ("pays" "Belgique") ("titre" "NIKITA") ("sigle" "NKT")
   ("d?t" "2010") ("fin" "2013") ("participants" "Lourdeaux, D.") ("domaine" "BS"))
  ("projet" ("id" "PROJECT/$E-PROJECT.22") ("pays" "France") ("titre" "ARAKIS") ("sigle" "ARA")
   ("d?t" "2010") ("fin" "2013") ("participants" "Lourdeaux, D." "Lenne, D.") ("domaine" "BIO"))))
|#	
;;;---------------------------------------------------------- DISPLAY-MESSAGE

(defun display-message (message conversation)
  "takes a message and displays its content into the assistant pane.
Arguments:
  message: A string containing the message
  conversation: the moss conversation object"
  (send conversation '=display-text
        (format nil "~%~A" message)))

;;;---------------------------------------------------------- EVAL-ATTR-VALUE
#|
(defun eval-attr-value (attr-value)
  "this function checks if the attribute is multilingual. 
if so, it extracts the corresponding value according to the default language.
It the argument is a list, makes a JSON attr-values string.
Arguments:
   attr-value: the attribute, or list (attr values)
Return:
   changed attribute"
  (cond
   ;; when MLN strip language
   ((mln::mln? attr-value) ; jpb 1406
    (string+ #\" (mln::print-string attr-value *language*) #\"))
   ;; normal add "" layer
   ((stringp attr-value)
    (string+ #\" attr-value #\"))
   ;; list, consider (attr value*)
   ((listp attr-value)
    (moss-list-to-json attr-value))))
|#
#|
(eval-attr-value "Albert")
"\"Albert\""
(eval-attr-value '(:en "Albert"))
"\"Albert\""
(eval-attr-value '("Joseph" "Kessel"))
"\"Joseph\" :  [ \"Kessel\" ] "
|#

;; --------------------------------------------------------------------------
;; Evaluate attribute during transformation
;; --------------------------------------------------------------------------

(defun eval-attr-value (class-ref attr-name attr-value language)
  "this function deals with a variety of attribute types (mln,ids,references)
Arguments:
   class-ref: the class reference
   attr-name: the attribute name
   attr-value: the attribute
Return:
   changed attribute"
  (cond
    ;; if the content is multilingual
    ((mln::mln? attr-value)
      ;; extracts the corresponding value according to the language 
      (return-from eval-attr-value 
        (concatenate 'string "\"" (mln::print-string attr-value language) "\""))
    )
    ;; if the attribute is a unique id
    ((equal attr-name "marshaller::uid")
      ;; if the content is a string then concatenate the symbol & at the first position
      (when (stringp attr-value)
        (return-from eval-attr-value
          (concatenate 'string "\"&" attr-value "\"")))
      ;; is a symbol then create the string representation
      (when (symbolp attr-value)
        (return-from eval-attr-value
          (concatenate 'string "\"" (symbol-name attr-value) "\""))
      )
    )
    ;; a symbol value as a content is presumably a relationship (we don't need to check)
    ((symbolp attr-value)
      (return-from eval-attr-value
        (concatenate 'string "\"" (symbol-name attr-value) "\""))
    )
    ;; a string value with related class (must investigate if it is a relation
    ((and class-ref (stringp attr-value))
      (if (moss::%%get-id attr-name :relation :class-ref class-ref)
         ;; add an address symbol in front of the string if is a reference    
        (return-from eval-attr-value (concatenate 'string "\"&" attr-value "\""))
        (return-from eval-attr-value (concatenate 'string "\"" attr-value "\""))
      )
    )
    ;; a normal string value
    ((stringp attr-value)
      (return-from eval-attr-value (concatenate 'string "\"" attr-value "\""))
    )
    ;; a complex attribute (list)
    ((listp attr-value)
      (return-from eval-attr-value (moss-list-to-json attr-value language))
    )    
  )
)

;;;--------------------------------------------------------- EXPAND-MOSS-LISP

(defun expand-moss-list (symb attrs &optional label)
  "Creates a de-normalized version of a tree-like representation.
Arguments:
  symb: the moss object
  attrs: list that will be used as a filter
  label: to add in front of the selected attributes"
  (let (res exp-attr)
    (cond
     ;; if the symbol is a list of moss objects
     ((is-list-obj-rep symb)
      ;; iterates over the list of symbols in order to expand each moss object
      (dolist (s symb)
        (setf exp-attr (expand-moss-list s attrs label))         
        (if exp-attr 
            (dolist (elem exp-attr)
              (push elem res)))))
     ;; if the symbol is a moss object
     ((is-obj-rep symb)
      (let (line relations proc-relations)
        ;; push the object type into the line (push (car symb) line)
        
        ;; iterate over the object attributes
        (dolist (y (cdr symb))
          (cond
           ((is-simple-attr-rep y)
            (if (some #'(lambda (x) (if (equal x (car y)) x)) attrs)
                (push y line)))
           ;; complex
           (t 
            (let (rel)
              (setq rel (expand-moss-list (cdr y) attrs))
              (if rel
                  (push rel relations))))))
        (dolist (r relations)
          (dolist (ri r)
            (dolist (attr line)
              (push attr ri))
            (push (cons label ri) proc-relations)))
        (if label
            (push label line))
        (if (> (length proc-relations) 0)
            (setf res proc-relations)
          (if line (setf res (list line))))))
     
     (t (error "invalid parameter: ~S" symb)))		 
    res))

#|
(expand-moss-list 
 '(("project" ("title" "@QUA") ("start" "2011") ("financement" "20000")
    ("financement annuel"
     ("financement annuel" ("année" "2011") ("montant" "2000")) 
     ("financement annuel" ("année" "2012") ("montant" "6000"))
     ("financement annuel" ("année" "2013") ("montant" "1000"))
     ("financement annuel" ("année" "2014") ("montant" "12000")))
    ("?ipe" ("team" ("nom" "ICI") ("perc" "50")) 
     ("team" ("nom" "DI") ("perc" "40"))
     ("team" ("nom" "RO") ("perc" "10"))))
   ("project" ("title" "SCOOP") ("start" "2007") ("financement" "3000")
    ("financement annuel" 
     ("financement annuel" ("année" "2008") ("montant" "1000")) 
     ("financement annuel" ("année" "2009") ("montant" "2000")))
    ("?ipe" ("team" ("nom" "ICI") ("perc" "50")) 
     ("team" ("nom" "DI") ("perc" "40"))
     ("team" ("nom" "RO") ("perc" "5"))
     ("team" ("nom" "ASER") ("perc" "5")))))
 '("start" "année" "montant") "project"))

(("project" ("montant" "1000") ("année" "2008") ("start" "2007"))
 ("project" ("montant" "2000") ("année" "2009") ("start" "2007"))
 ("project" ("montant" "2000") ("année" "2011") ("start" "2011"))
 ("project" ("montant" "6000") ("année" "2012") ("start" "2011"))
 ("project" ("montant" "1000") ("année" "2013") ("start" "2011"))
 ("project" ("montant" "12000") ("année" "2014") ("start" "2011")))
|#
;;;---------------------------------------------------- GENERATE-SUMMARY-LINE
;;; ???

(defun generate-summary-line 
    (group-function group-value count-value id attribute current-key)
  "generates a summary line with categories and grouped data.
Arguments:
   group-function: sum, avg for example
   group-value: 
   count-value: 
   id: object id
   attribute: target attribute
   current-key: category
"						
  (if (equal group-function "avg")
      (setf group-value (float (/ group-value count-value))))            
  (if (equal group-function "count")
      (setf group-value count-value))	
  (append current-key 
          (list (list "id" (write-to-string id))) 
          (list (list (concatenate 'string group-function "(" attribute ")") 
                      (write-to-string group-value)))))

;;;-------------------------------------------------- GET-SELECTED-ATTRIBUTES

(defun get-selected-attributes (word-list categories)
  "Selects attributes based on the raw input.
Arguments:
  word-list: A list of words typed by user
  categories: The list of attributes
Returns:
  The selected list of attributes"
  (let (patterns selected)
    (setq patterns (moss::generate-access-patterns word-list :max 4))
    (dolist (pattern patterns)
      (setq pattern (car pattern))
      (dolist (category categories)
        (if (equalp category pattern)
            (push category selected)
          )
        )
      )
    (reverse selected)
    )
  )
 
#| ???
(get-selected-attributes '("la" "date" "de" "début" "et" "la" "durée")
                         '("date de début" "durée" "id"))
("date de début" "durée")
|#
;;;---------------------------------------------------------- GROUP-MOSS-LIST

(defun group-moss-list (moss-list keys attribute group-function) 
  "executes grouping functions
Arguments:
  moss-list: source-list
  keys: candidate attributes
  attribute: target attribute
  group-function: sum, avg for example
"
  (if (eq 0 (length moss-list))
      (return-from group-moss-list nil))
  
  (let (expanded-list sorted-list current-key grouped-list (group-value 0) 
                      (count-value 0) (id 0))
    ;; first expand the list
    (setf expanded-list (expand-moss-list moss-list 
                                          (cons attribute keys)
                                          (caar moss-list)))
    
    ;; sort the list
    (setq sorted-list (reverse (sort-moss-list expanded-list keys)))
    
    ;; generate the current key
    (setf current-key (build-moss-object-key (car sorted-list) keys))
    
    ;(format t "~%; group-moss-list / current-key = ~S keys = ~S" current-key keys)
    (let (attr-value)
      (dolist (object sorted-list)
        (if (not (equal (build-moss-object-key object keys) current-key))
            (progn   
              ;(format t "~%; - break: current-key = ~S object-key = ~S keys = ~S ~
              ;          group-value = ~S count-value = ~S" 
              ;  
              ;  current-key (build-moss-object-key object keys) 
              ;  keys group-value count-value)
              ;; generate summary line from the previous key            
              (setf id (+ id 1))
              (push (generate-summary-line
                     group-function group-value 
                     count-value id attribute current-key) 
                    grouped-list)
              ;; store new key
              (setf current-key (build-moss-object-key object keys))
              ;(format t "~%; group-moss-list / new current-key ~S" current-key)
              (setf group-value 0)
              (setf count-value 0)
              )
          )
        ;; find attribute
        (dolist (attr (cdr object))
          (if (equal attribute (car attr))
              (setq attr-value (cadr attr))))
        ;; if attribute was found			  
        (if attr-value
            (progn
              (setq attr-value (car (parse-string-to-float attr-value)))
              ;; execute grouping function
              (cond
               ((equal group-function "sum")
                (setq group-value (+ group-value attr-value)) 
                )
               ((equal group-function "count")
                (setq count-value (+ count-value 1))
                )
               ((equal group-function "avg")
                (setq group-value (+ group-value attr-value)) 
                (setq count-value (+ count-value 1))
                )
               )
              )
          )
        ))
    (push (generate-summary-line group-function group-value 
                                 count-value (+ id 1) attribute 
                                 current-key)
          grouped-list)))

#| ???
(group-moss-list 
 '(("project" ("start" "2007") ("montant" "2000") ("année" "2009"))
   ("project" ("start" "2007") ("montant" "1000") ("année" "2008"))
   ("project" ("start" "2011") ("montant" "12000") ("année" "2014"))
   ("project" ("start" "2011") ("montant" "1000") ("année" "2013"))
   ("project" ("start" "2011") ("montant" "6000") ("année" "2012"))
   ("project" ("start" "2011") ("montant" "2000") ("année" "2012")))
 '("start" "année") "montant" "sum")

(("project" ("start" "2007") ("année" "2008") ("id" "5") ("sum(montant)" "1000"))
 ("project" ("start" "2007") ("année" "2009") ("id" "4") ("sum(montant)" "2000"))
 ("project" ("start" "2011") ("année" "2012") ("id" "3") ("sum(montant)" "8000"))
 ("project" ("start" "2011") ("année" "2013") ("id" "2") ("sum(montant)" "1000"))
 ("project" ("start" "2011") ("année" "2014") ("id" "1") ("sum(montant)" "12000")))
|#
;;;-------------------------------------------------------------- IS-ATTR-REP
#|
(defun is-attr-rep (ref-list)
  "check if the symbol has the moss object format.
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
  (if (not (listp ref-list))
    (return-from is-attr-rep nil))
  ;; to be an attribute the first element must be a string
  (if (not (stringp (car ref-list)))
      (return-from is-attr-rep nil))
  ;; to be an attribute the remainder must be a:
  ;; string, mln string or an object
  (mapc #'(lambda(x) 
            (if (and (not (stringp x) ) 
                     (not (mln::mln? x)) ; jpb 1406
                     (not (is-obj-rep x)))
              (return-from is-attr-rep nil)))
    (cdr ref-list))
  t)
|#
;; --------------------------------------------------------------------------
;; Check of symbol is a moss attribute (alist format)
;; --------------------------------------------------------------------------

(defun is-attr-rep (ref-symbol)
  "check if the symbol is according to the moss object format
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
  (if (not (listp ref-symbol))
      (return-from is-attr-rep nil))
  ;; to be an attribute the first element must be a string
  (if (not (stringp (car ref-symbol)))
      (return-from is-attr-rep nil))
  ;; to be an attribute the remainder must be a:
  ;; string, symbol, mln string, an object
  (mapc #'(lambda(x) 
            (if (and (not (stringp x) )
                     (not (symbolp x) )
                     (not (mln::mln? x))
                     (not (is-obj-rep x)))
                (return-from is-attr-rep nil))) 
    (cdr ref-symbol))
  t)	  

;;;---------------------------------------------------------- IS-LIST-OBJ-REP
;;; ???
#|
(defun is-list-obj-rep (ref-symbol)
  "check if the symbol is a list of moss objects.
Arguments:
   ref-symbol: the moss list
Return:
   a boolean result"
  ;; must be a list
  (if (not (listp ref-symbol))
      (return-from is-list-obj-rep nil))
  ;; evaluate each list member to check its contents	
  (mapc #'(lambda(x) 
            (if (not (is-obj-rep x))
                (return-from is-list-obj-rep nil))) 
    (cdr ref-symbol))
  t)	
|#  
;; --------------------------------------------------------------------------
;; Check of symbol is a list of moss objects (alist format)
;; --------------------------------------------------------------------------
(defun is-list-obj-rep (ref-symbol)
  "check if the symbol is a list of moss objects
Arguments:
   ref-symbol: the moss list
Return:
   a boolean result"
  ;; must be a list
  (if (not (listp ref-symbol))
      (return-from is-list-obj-rep nil))
  ;; evaluate each list member to check its contents	
  (mapc #'(lambda(x) 
            (if (not (is-obj-rep x))
                (return-from is-list-obj-rep nil))) 
    (cdr ref-symbol))
  t)		 


;;;--------------------------------------------------------------- IS-OBJ-REP
;;; ???
#|
(defun is-obj-rep (ref-list)
  "check if the symbol is according to the moss object format
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
  ;; must be a list
  (if (not (listp ref-list))
      (return-from is-obj-rep nil))
  ;; to be an object the first element must be a string
  (if (not (stringp (car ref-list)))
      (return-from is-obj-rep nil))
  ;; to be an object the remainder must be attributes
  (mapc #'(lambda(x) 
            (if (not (is-attr-rep x))
                (return-from is-obj-rep nil))) 
    (cdr ref-list))
  t)
|#
;; --------------------------------------------------------------------------
;; Check of symbol is a moss object (alist format)
;; --------------------------------------------------------------------------

(defun is-obj-rep (ref-symbol)
  "check if the symbol is according to the moss object format
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
  ;; must be a list
  (if (not (listp ref-symbol))
      (return-from is-obj-rep nil))
  ;; to be an object the first element must be a string
  (if (not (stringp (car ref-symbol)))
      (return-from is-obj-rep nil))
  ;; to be an object the remainder must be attributes
  (mapc #'(lambda(x) 
            (if (not (is-attr-rep x))
                (return-from is-obj-rep nil))) 
    (cdr ref-symbol))
  t)


;;;------------------------------------------------------- IS-SIMPLE-ATTR-REP
;;; ???

(defun is-simple-attr-rep (ref-symbol)
  "check if the argument obeys the moss object format
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
  (if (not (listp ref-symbol))
      (return-from is-simple-attr-rep nil))
  ;; to be an attribute the first element must be a string
  (if (not (stringp (car ref-symbol)))
      (return-from is-simple-attr-rep nil))
  ;; to be an attribute the remainder must be a:
  ;; string, mln string
  (mapc #'(lambda(x) 
            (if (and (not (stringp x) ) 
                     (not (mln::mln? x))) ; jpb 1406
                (return-from is-simple-attr-rep nil))) 
    (cdr ref-symbol))
  t)

;; ============================================================================
;; Function: marshall-moss-object
;; Creates an alist representation of an object or a list of objects
;; ============================================================================

(defun marshall-moss-object (elem &optional inv-rel-to-expand expanded-list)
  (when (not expanded-list)
    (push nil expanded-list))
  (let (result-rep obj-rep to-expand uid summ)
    (cond 
      ;; if elem is a list (presumably moss objects!)
      ((listp elem)
        (dolist (e elem)
          (let ((response (marshall-moss-object e inv-rel-to-expand expanded-list)))
            (when response
              (setq result-rep (append result-rep response))))
        )
        ;; identify direct and indirect objects
        (setq result-rep (mapcar #'(lambda(x)  
            (let (moss-id)
              (setq moss-id (cadr (assoc "marshaller::moss-id" (cdr x) :test #'equal)))
              (if (find moss-id elem)
                (append x (list (list "marshaller::condition" :direct)))
                (append x (list (list "marshaller::condition" :indirect))))               
            )
          ) result-rep))
      )
      ;; if elem is a moss object
      ((moss::%pdm? elem)
        ;; if elem is already in the expanded list, ignore the expansion
        (when (find elem expanded-list)
          (return-from marshall-moss-object))
        ;; add type to object
        (push (car (moss::%get-object-class-name elem)) obj-rep)
        ;; look for a unique entry point
        (setq uid (moss::%get-entry-point-if-unique elem))
        ;; if there is no uid, use moss id (not it will not be used by the unmarshaller, 
        ;; only used as a guide!)
        (if (not uid)
          (setq uid (car (moss::%get-value elem 'moss::$id)))
          (setq uid (symbol-name uid))
        )
        (push (list "marshaller::uid" uid) obj-rep)
        (push (list "marshaller::moss-id" (car (moss::%get-value elem 'moss::$id))) obj-rep)
        ;; get summary
        (setq summ (car (send elem '=summary)))
        (if (stringp summ)
          (push (list "marshaller::summary" summ) obj-rep)
          (push (list "marshaller::summary" (symbol-name summ)) obj-rep)
        )        
        ;; iterate over properties
        (let (prop-name)
          (dolist (property (moss::%get-properties elem))
            ;; get property name
            (setq prop-name (car (moss::%%get-value property 'moss::$pnam *context*)))
            ;; extract canonical name
            (let (partial-rep)
              (push (mln::get-canonical-name prop-name) partial-rep)    
              (cond
                ((moss::%is-attribute? property)
                    ;; iterate over values
                    ;;(dolist (item (moss::%get-value elem property))
                    (dolist (item (send elem '=get (mln::get-canonical-name prop-name)))
                      (push item partial-rep)))
                ((moss::%is-relation? property)
                    ;; iterate over references
                    ;;(dolist (item (moss::%get-value elem property))
                    (dolist (item (send elem '=get (mln::get-canonical-name prop-name)))
                      ;; look for a unique entry point of object
                      (setq uid (moss::%get-entry-point-if-unique item))
                      ;; if there is no uid, use moss id (not it will not be used by the unmarshaller, only used as a guide!)
                      (if (not uid)
                        (setq uid (car (moss::%get-value item 'moss::$id)))
                        (setq uid (symbol-name uid))
                      )
                      (push uid partial-rep)
                      ;; if reference is not in the expanded list, so it has to be expanded!
                      (when (not (find item expanded-list))
                        (push item to-expand))))
                (t
                  (error "not an attribute nor a relation")))
              ;; add attribute only if it has values
              (when (> (length partial-rep) 1)
                (push (reverse partial-rep) obj-rep))))) 
        ;; iterate over inverse properties
        (when inv-rel-to-expand
          (format t "~%@marshall-moss-object - entered inverse for [~S] - inv-rel-to-expand: [~S]" elem inv-rel-to-expand)                  
          (dolist (inv-rel 
                    (moss::%%get-all-class-inverse-relations 
                      (moss::%%get-id 
                        (car (moss::%get-object-class-name elem)) :class)))
            (let (src-prop src-prop-name src-canonical-name partial-rep)                          
              (format t "~%@marshall-moss-object - each inv-rel for [~S] - inv-rel: [~S]" elem inv-rel)            
              ;; get source property
              (setq src-prop (car (moss::%%get-value inv-rel 'moss::$inv.of *context*)))
              ;; if property is marked to expand
              (when (find src-prop inv-rel-to-expand)
                (format t "~%@marshall-moss-object - each inv-rel for [~S] - src-prop [~S] is desired" elem src-prop)            
                ;; get src property name
                (setq src-prop-name (car (moss::%%get-value src-prop 'moss::$pnam *context*)))              
                ;; get canonical name
                (setq src-canonical-name (mln::get-canonical-name src-prop-name))
                ;; add prefix
                (setq src-canonical-name (string+ "inverse::" src-canonical-name))
                ;; iterate over references
                (push src-canonical-name partial-rep)
                (dolist (item (moss::%get-value elem inv-rel))
                  ;; look for a unique entry point of object
                  (setq uid (moss::%get-entry-point-if-unique item))
                  ;; if there is no uid, use moss id (not it will not be used by the unmarshaller, only used as a guide!)
                  (if (not uid)
                    (setq uid (car (moss::%get-value item 'moss::$id)))
                    (setq uid (string+ "&" (symbol-name uid)))
                  )                  
                  (push uid partial-rep)
                  ;; if reference is not in the expanded list, so it has to be expanded!
                  (when (not (find item expanded-list))
                    (push item to-expand)
                  )
                )
                ;; add partial representation to object representation
                (when (> (length partial-rep) 1)
                  (push (reverse partial-rep) obj-rep)
                )                
              )
            )
          )
        )
        ;; add the object to the result representation
        (push (reverse obj-rep) result-rep)        
        ;; add object to the expanded-list
        (setf (cdr (last expanded-list)) (cons elem nil))        
        ;; process to-expand list
        (dolist (te to-expand)
          (let ((result (marshall-moss-object te inv-rel-to-expand expanded-list)))
            (when result
              (setq result-rep (append result-rep result))))))
      ;; not an object nor a list
      (t
        (error "element is not an object nor a list")))    
    (return-from marshall-moss-object (reverse result-rep))))
    
#|
(defconcept "credit-card6"
	(:att "number")
	(:att "name"))
	
(defconcept "type6"
	(:att "acronym")
	(:att "name")
	(:rel "cards" (:to "credit-card6")))

(defrelation "type" (:from "credit-card6") (:to "type6"))

(defindividual "credit-card6" ("number" "123") ("name" "Marcio Fuckner"))
(defindividual "credit-card6" ("number" "456") ("name" "Ana Maria"))
(defindividual "credit-card6" ("number" "789") ("name" "Giovani Bernardes"))

(defindividual "type6" ("acronym" "VISA") ("name" "VISA CORP") ("cards" $e-credit-card6.1 $e-credit-card6.3))
(defindividual "type6" ("acronym" "AMEX") ("name" "American Express") ("cards" $e-credit-card6.2))

(send '$e-credit-card6.1 '=add-attribute-values 'has-type '$e-type6.1)
(send '$e-credit-card6.2 '=add-attribute-values 'has-type '$e-type6.2)
(send '$e-credit-card6.3 '=add-attribute-values 'has-type '$e-type6.1)

(marshall-moss-object '($e-credit-card6.1 $e-credit-card6.2 $e-credit-card6.3))

|#

#|
(defconcept "credit-card8"
	(:att "number" (:entry) (:unique))
	(:att "name"))
	
(defconcept "type8"
	(:att "acronym" (:entry) (:unique))
	(:att "name")
	(:rel "cards" (:to "credit-card8")))

(defrelation "type" (:from "credit-card8") (:to "type8"))

(defindividual "credit-card8" ("number" "123") ("name" "Marcio Fuckner"))
(defindividual "credit-card8" ("number" "456") ("name" "Ana Maria"))
(defindividual "credit-card8" ("number" "789") ("name" "Giovani Bernardes"))

(defindividual "type8" ("acronym" "VISA") ("name" "VISA CORP") ("cards" $e-credit-card8.1 $e-credit-card8.3))
(defindividual "type8" ("acronym" "AMEX") ("name" "American Express") ("cards" $e-credit-card8.2))

(send '$e-credit-card8.1 '=add-attribute-values 'has-type '$e-type8.1)
(send '$e-credit-card8.2 '=add-attribute-values 'has-type '$e-type8.2)
(send '$e-credit-card8.3 '=add-attribute-values 'has-type '$e-type8.1)

(marshall-moss-object '($e-credit-card8.1 $e-credit-card8.2 $e-credit-card8.3))

|#

;;;-------------------------------------------------------- MOSS-LISP-TO-JSON
#|
(defun moss-list-to-json (result)
  "Creates a JSON representation of a MOSS list of individuals
Arguments:
   result: the resulting moss list
Return:
   the list in a json format"
  (let (json)
    (cond
     ;; is an attribute representation
     ((is-attr-rep result)
      (setq json (concatenate 'string "\"" (car result) "\"" " : "))
      (setq json (concatenate 'string json " [ "))
      (let ((index 0))
        (mapc #'(lambda(x) 
                  (setq json (concatenate 'string json (eval-attr-value x) ))
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (concatenate 'string json " , "))))
          (cdr result)))
      (setq json (concatenate 'string json " ] ")))
     ;; is an object representation
     ((is-obj-rep result)
      (setq json (concatenate 'string json " { "))
      (setq json (concatenate 'string json "\"type\" : \"" 
                   (car result) "\","))
      (let ((index 0))
        (mapc #'(lambda(x)
                  (setq json (concatenate 'string 
                               json (moss-list-to-json x))) 
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (concatenate 'string json " , ")))) 
          (cdr result)))
      (setq json (concatenate 'string json " } ")))
     ;; is a list of objects
     ((is-list-obj-rep result)
      (setq json (concatenate 'string json " [ "))
      (let ((index 0))
        (mapc #'(lambda(x)
                  (setq json (concatenate 'string 
                               json (moss-list-to-json x))) 
                  (incf index)
                  (if (< index (list-length result))
                      (setq json (concatenate 'string json " , ")))) result))
      (setq json (concatenate 'string json " ] "))))	
    json))
|#
;; --------------------------------------------------------------------------
;; Transform a moss alist representation into a json format
;; --------------------------------------------------------------------------

(defun moss-list-to-json (result &optional  language pred-ref )
  "Creates a JSON representation of a MOSS list of individuals
Arguments:
   result: the resulting moss list
   pred-ref (optional): the reference of the predecessor. 
Return:
   the list in a json format"
  (setf language (or language *language*))
  (let (json card prop-ref list-flag)
    (cond
     ;; is an attribute representation
     ((is-attr-rep result)            
      ;; check attribute cardinality
      (setq list-flag t)
      (setq prop-ref nil)
      (setq card nil)
      (when (not (equal nil pred-ref))
        (setq prop-ref (moss::%%get-id-for-property (car result) :property :class-ref pred-ref))
        (setq card (car (moss::%get-value prop-ref 'moss::$maxt)))
      )
      ;; avoid list creation if we have <= one element with cardinality equal to 1
      (when (and (<= (length (cdr result)) 1) (equal card 1))
          (setq list-flag nil))
      
      (setq json (concatenate 'string "\"" (car result) "\"" " : "))
      (when list-flag
        (setq json (concatenate 'string json " [ "))
      )
      (let ((index 0))
        (mapc #'(lambda(x) 
                  (setq json (concatenate 'string json (eval-attr-value pred-ref (car result) x language) ))
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (concatenate 'string json " , "))))
          (cdr result)))
      (when list-flag
        (setq json (concatenate 'string json " ] ")))
     
     ;; if empty and without list-flag
     (when (and (equal (length result) 1) (not list-flag))
       (setq json (concatenate 'string json "null"))))
      
     ;; is an object representation
     ((is-obj-rep result)
      (setq json (concatenate 'string json " { "))
      (setq json (concatenate 'string json "\"marshaller::type\" : \"" 
                   (car result) "\","))
      (let ((index 0))
        (mapc #'(lambda(x)
                  (setq json (concatenate 'string 
                               json (moss-list-to-json x language (car result) ))) 
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (concatenate 'string json " , ")))) 
          (cdr result)))
      (setq json (concatenate 'string json " } ")))
     ;; is a list of objects
     ((is-list-obj-rep result)
      (setq json (concatenate 'string json " [ "))
      (let ((index 0))
        (mapc #'(lambda(x)
                  (setq json (concatenate 'string 
                               json (moss-list-to-json x language))) 
                  (incf index)
                  (if (< index (list-length result))
                      (setq json (concatenate 'string json " , ")))) result))
      (setq json (concatenate 'string json " ] "))))	
    json))


;;;---------------------------------------------------- PARSE-STRING-TO-FLOAT

(defun parse-string-to-float (line)
  "Parse a string to float
Arguments:
  line: a line with potential numbers"
  (with-input-from-string (s line)
    (loop
      :for num := (read s nil nil)
      :while num
      :collect num))) 

#|
(moss::parse-string-to-float "30 123")
(30 123)

(moss::parse-string-to-float "30.123")
(30.123)
|#
;;;----------------------------------------------------------- SORT-MOSS-LIST

(defun sort-moss-list (moss-list keys)
  "Executes a non-destructive sort in a list
Arguments:
  moss-list: the source list, e.g. ((\"project\" (\"start\" \"2007\") (...))...)
  keys: the candidate attributes, e.g. (\"start\" \"année\")
Return:
   the list ordered according to the set of keys."
  (let (new-moss-list temp-moss-list)
    (dolist (elem moss-list)
      ;(format t "~% elem: ~S" elem)
      
      ;; navigate through the attributes in order to find key values
      (let (obj-key) ; reset obj-key
        ;; for each sublist of each element of the source list
        ;; e.g. ("project" ("start" "2007") ("montant" "2000") ("année" "2009"))
        (dolist (attrib (cdr elem))
          ;; e.g. attrib = ("start" "2007")
          ;(format t "~% attrib ~S (car attrib) ~S" attrib (car attrib))
          ;; check if some property of the list is one of the keys
          (when (some #'(lambda (x) (if (equal x (car attrib)) x)) keys)                    
            ;(print (cadr attrib))
            ;; if so, build a composite key with values, e.g. "20072009"
            (setf obj-key (concatenate 'string obj-key (cadr attrib)))
            ;(print obj-key)
            ))
        ;; build a list starting with the composite key
        (if obj-key
            (push (list obj-key elem) temp-moss-list))))
    
    ;; now the sorting process
    (setq temp-moss-list (sort temp-moss-list #'string-greaterp :key #'car))
    
    ;; now remove the temporary key
    ;; could use and return: (mapcar #'cadr (reverse temp-moss-list))
    (dolist (elem temp-moss-list)
      (push (cadr elem) new-moss-list))
    
    ;; return sorted list
    new-moss-list))

#|
(sort-moss-list 
 '(("project" ("start" "2007") ("montant" "2000") ("année" "2009"))
   ("project" ("start" "2007") ("montant" "1000") ("année" "2008"))
   ("project" ("start" "2011") ("montant" "12000") ("année" "2014"))
   ("project" ("start" "2011") ("montant" "1000") ("année" "2013"))
   ("project" ("start" "2011") ("montant" "6000") ("année" "2012"))
   ("project" ("start" "2011") ("montant" "2000") ("année" "2012")))
 '("start" "année"))
(("project" ("start" "2007") ("montant" "1000") ("année" "2008"))
 ("project" ("start" "2007") ("montant" "2000") ("année" "2009"))
 ("project" ("start" "2011") ("montant" "6000") ("année" "2012"))
 ("project" ("start" "2011") ("montant" "2000") ("année" "2012"))
 ("project" ("start" "2011") ("montant" "1000") ("année" "2013"))
 ("project" ("start" "2011") ("montant" "12000") ("année" "2014")))
|#
(defun unmarshall-moss-object (elem)
  (let (concept-name original-individual-uid new-individual-uid prop-name 
                     conversion-table condition-list corresp-ref)
    ;; iterate over the elements ignoring the relationships for the moment
    (dolist (e elem)
      (setq concept-name (car e))
      (setq original-individual-uid (cadadr e))
      (setq new-individual-uid nil)
      ;; if id is a string then look for an object related to this "possible" entry point
      (when (stringp original-individual-uid)
        (setq new-individual-uid (car (moss::find-objects-from-text original-individual-uid :target concept-name))))
      ;; create a new individual if needed
      (when (not new-individual-uid)
        (setq new-individual-uid (eval `(defindividual ,concept-name))))
      ;; update converstion table adding old id and new id
      (push (list original-individual-uid new-individual-uid) conversion-table)      
      ;; work with attributes using add addtribute values and remove-attribute-values
      (let (work-list property)
        (dolist (prop (cddr e))
          (setq prop-name (car prop))
          (setq property (moss::%%get-id prop-name :attribute :class-ref concept-name))
          ;; only attribute from now!
          (when property
            (push (cons prop-name (cdr prop)) work-list)))
        (setq work-list (reverse work-list))
        ;; updating attributes
        (dolist (elem work-list)
          (send new-individual-uid '=replace (car elem) (cdr elem)))))
    ;; iterate over the elements - rebuilding relationships
    (dolist (e elem)
      ;; recover concept name, original and new uid
      (setq concept-name (car e))
      (setq original-individual-uid (cadadr e))
      (setq new-individual-uid (cadr (assoc original-individual-uid conversion-table :test #'equal)))
      ;; now it's time to rebuild relationships
      (let (work-list property work-prop)
        ;; iterate over properties to find relationships
        (dolist (prop (cddr e))
          (setq prop-name (car prop))
          (setq property (moss::%%get-id prop-name :relation :class-ref concept-name))
          (when property
            (push prop-name work-prop)
            ;; iterate over each property value
            (dolist (v (cdr prop))
              (setq corresp-ref (assoc v conversion-table :test #'equal))
              (if corresp-ref
                (push (cadr corresp-ref) work-prop)
                (error "unmarshalling error - relationship error")))
            (push (reverse work-prop) work-list)))
        (setq work-list (reverse work-list))
        ;; updating relations
        (dolist (elem work-list)
          (send new-individual-uid '=replace (car elem) (cdr elem)))))    
    ;; retrieve object conditions
    (setq condition-list 
      (mapcar #'(lambda(x) 
                  (list (cadr (assoc "marshaller::uid" (cdr x) :test #'equal)) 
                        (cadr (assoc "marshaller::condition" (cdr x) :test #'equal)))) elem
      )
    )
    (mapcar #'(lambda(x)
                (let (condition)
                  (setq condition (cadr (assoc (car x) condition-list)))
                  (list (cadr x) condition)
                )
              ) conversion-table
    )
  )
)

#|
(unmarshall-moss-object 
'(
 ("credit-card7" ("moss::$id" $E-CREDIT-CARD7.100) ("number" "456") ("name" "Ana Maria") ("type" $E-TYPE7.100))
 ("type7" ("moss::$id" $E-TYPE7.100) ("acronym" "VISA") ("name" "VISA CORP") ("cards" $E-CREDIT-CARD7.100))
 ))
 
 (unmarshall-moss-object 
'(
  ("credit-card8" ("uid" "999") ("number" "999") ("name" "Ana Maria") ("type" "AMEIXA"))
  ("type8" ("uid" "AMEIXA") ("acronym" "AMEIXA") ("name" "American Express") ("cards" "999"))
 ))
 |#
 

(format t "~%;*** MOSS v~A - Web support loaded ***" *moss-version-number*)

;;; :EOF