;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=================================================================================
;;;20/01/30
;;;		
;;;		E D I T O R - (File editor.lisp)
;;;	
;;;=================================================================================
;;; The MOSS interactive editor for modifying objects dynamically

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; The file is organized following the layout of the editor window
;;;   HEADER
;;;   LEFT TP AREA
;;;   LEFT SP AREA
;;;   LEFT IL AREA
;;;   MESSAGE AREA
;;;   RIGHT TP AREA
;;;   RIGHT SP AREA
;;;   QUERY AREA
;;;   PROPERTY AREA
;;;   AUXILIARY FUNCTIONS
;;;

#|
List of callbacks
-----------------
Top lines
  Object identity   ewf-get-object-name
  Class             ewf-get-class-name
  Context           ewf-get-context
  COMMIT            ew-commit-on-click
  ABORT             ew-abort-on-click

Left Attribute area
  OK/END            ew-tp-left-ok-button-on-click
  ADD               ew-tp-left-add-button-on-click
  EDIT              ew-tp-left-edit-button-on-click
  DELETE            ew-tp-left-delete-button-on-click
  posting attributes            ewf-tp-get-property-and-value
  selection on click            ew-tp-left-show-details-on-click
  selection on double click     ew-tp-left-show-details-on-double-click

Left Relation area
  OK/END            EW-sp-left-OK-button-on-click
  ADD               EW-sp-left-ADD-button-on-click
  EDIT              EW-sp-left-edit-button-on-click
  DELETE            EW-sp-left-delete-button-on-click
  posting relations             ew-sp-left-show-details-on-click
  selection on click            ew-sp-left-show-details-on-double-click
  selection on double click     ewf-sp-get-property-and-value

Left Inverse link area
  OK/EN             EW-il-left-OK-button-on-click
  posting IL                    EW-il-show-details-on-click
  selection on click            EW-il-show-details-on-double-click
  selection on double click     ewf-il-get-property-and-value

Rigth top buttons
  ADD               EW-tp-right-add-button-on-click
  INSERT            EW-tp-right-insert-button-on-click
  DELETE            EW-tp-right-delete-button-on-click
  MODIFY            EW-tp-right-modify-button-on-click
  QUIT              EW-tp-right-cancel-button-on-click
  OK                EW-tp-right-ok-button-on-click
  selection on click (first pane)     EW-edit-prop-value-on-click
  selection on click (second pane)    nothing

Right middle buttons
  ADD               EW-sp-right-ADD-button-on-click
  INSERT            EW-sp-right-INSERT-button-on-click
  CREATE            EW-sp-right-CREATE-button-on-click
  DELETE            EW-sp-right-DELETE-button-on-click
  QUIT              EW-sp-right-QUIT-button-on-click
  OK                EW-sp-right-OK-button-on-click
  CANCEL            EW-sp-right-CANCEL-button-on-click
  CLONE             EW-sp-right-CLONE-buton-on-click
Middle panes
  Concept           editor-class-item-on-change
  Property          editor-property-item-on-change
  Entry             editor-entry-point-item-on-change
  EXAMINE           ew-sp-right-examine-on-click
  Found objects on click         EW-object-list-on-click
  Found objects on double click  EW-object-list-show-object-on-double-click
  Query             ew-query-on-click

Right bottom panes
  properties from application    EW-select-local-property-on-click
  properties from model          EW-select-global-property-on-click

Message area
  printing   nothing special
|#

#|
2002 created from te DIALOG file (1994) for MOSS v4
2004
 0610 changed set-part-color to set-back-color in editor window
 0616 double-click does not seem t work in ew-tp-left-show-details-on-click when
      first-line contains more than one value. Something is in conflict with the
      double-click mechanism (racing condition?)
 0619 adding +dim-color+ for inactive views, editor-view-dim, ew-undim-view, 
      ew-dim-only
 0630 cleaning up the mess of service functions

      SEMANTICS:
        - enable/disable usual meaning
        - expose/dim obvious meaning
        - activate/deactivate combines enable/disable with dim/expose
        - clear means that we reset the local views to their default content
        - close means disable + dim + reset internal variables, + optionally
          resetting the local views to their default content
        - open means that we reset internal variables, enable and expose the area
2005
 0310 removing mo from ewf-sp-delete-property, replaced with symbol-value
2006
 0219 Version 6
      =========
      modifying the display function for local and global properties to take into
      account generic and local properties
      modifying the display of the title of the property
 0220 ewf-extract-objects modified to use access rather than simple entry points
      key handlers modified to deal with multiple properties
 1107 modifying ew-sp-right-examine-on-click
2011
 0623 adding %beep to ewf-post-message
      adding a KILL button to kill the current entity
      updating initialize-editor-window to check for dead objects
 0806 make sure to enable kill button
2012
 1122 fixing some problems with the query area, removing all references to MCL
2013
 0121 fixing MLN problems on attribute modification
 0419 fixing a bug when using queries
2014
 0302 adding a gate slot to the EDItor class for implementing synchronization with
      the overview window
      modifying edit, make-editor-window, aw-abort-proceed, ew-commit-proceed
 0307 -> UTF8 encoding
 0701 modifying ewf-tp-insert-nth-value to account for MLNs
|#

;;; Can be used as follows:
;;; (moss::edit '$ent) or (edit '$ent) when package uses :MOSS package

(in-package "MOSS")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (import '(cg:beep cg:find-component cg:select-window cg:screen cg:make-box
                    cg:make-rgb cg:make-font-ex cg:make-window cg:find-sibling
                    cg:windowp cg:set-focus-component cg:selected-object 
                    cg:find-window cg:find-pixmap cg:interior-width
                    cg:interior-height cg:add-component
                    cg:static-text cg:editable-text cg:multi-line-editable-text
                    cg:button cg:check-box cg:progress-indicator cg:radio-button
                    ))
  (export '(make-editor-window edit))
  )

;(format *standard-output* "~%;*** loading MOSS Editor ~A ..." *moss-version*)

#|
(defun tt() (make-editor-window '$ept))
(tt)
|#

;;;=================================================================================
;;;
;;;                            MACROS and GLOBALS
;;;
;;;=================================================================================
;;; the following constructs are restricted to this file

;;;================================== Macros =======================================

(defParameter *ew-trace* nil "debugging trace, when T prints everything.")

(defMacro ewformat (&rest ll)
  `(if *ew-trace* (format ,@ll)))

;;;--------------------------------------------------------- WITH-EDITOR-ENVIRONMENT
;;; obtain environment from the editor structure, restore on output

(defMacro with-editor-environment (editor &rest ll)
  `(let ((*context* (context ,editor))
         (*package* (current-package ,editor))
         (*version-graph* (version-graph ,editor))
         (*language* (language ,editor))) 
     (%%allowed-context? (context ,editor))
     ,@ll))

;;;================================== Globals ======================================

(defVar *editor-window* () "The MOSS editor in the MOSS package")

(defparameter *editor-top* 50 "top of the first editor")
(defparameter *editor-left* 100)

(defParameter *max-number-of-objects-to-display* 20 "in the sp right window")

;;; === global parameters for specifying the various positions  and sizes of the 
;;; different areas in the edit window

(defParameter *editor-header-height* 40
  "The height of the header in the edit window")
(defParameter *editor-inter-area-height* 30
  "The height of the area separating each work area in the edit window")
(defParameter *editor-inter-area-width* 20
  "The width of the area separating each work area in the edit window")
(defParameter *tp-button-width* 80 
  "The width of a TP button")
(defParameter *editor-window-width* 1150 "Total width of the edit window.")
(defParameter *editor-window-height* 1000 "Total height of the edit window.")
(defParameter *editor-left-width* 400 "Width of the left display area.")
(defParameter *editor-right-width*
  (- *editor-window-width* *editor-inter-area-width* *tp-button-width*
     *editor-inter-area-width*
     *editor-left-width* *editor-inter-area-width* *tp-button-width*
     *editor-inter-area-width*)
  "Size of the right part of the edit window, computed from the total width ~
   and the various stuff on the left, and less a rightmost margin.")
;---
(defParameter *tp-area-v-position* (+ *editor-header-height*
                                      *editor-inter-area-height*)
  "The vertical position of the start of the TP area")
(defParameter *tp-left-button-h-position* 10 
  "The left margin for the TP left button array")
(defParameter *tp-button-vertical-offset* 10 
  "The vertical offset of TP left button array")
(defParameter *tp-button-vertival-spacing* 14 
  "The vertical spacing between TP left buttons")
(defParameter *tp-button-vertival-thickness* 16 
  "The vertical thickness of TP buttons")
;---
(defParameter *tp-edit-area-width* *editor-left-width* 
  "The left margin for the TP left button array")
(defParameter *tp-edit-area-height* 160 
  "The left margin for the TP left button array")
;---
(defParameter *tp-work-area-width* 500 
  "The left margin for the TP left button array")
(defParameter *tp-work-area-height* 160 
  "The left margin for the TP left button array")
;---
(defParameter *tp-value-area-height* 80 
  "The height of the array displaying the TP values")
(defParameter *tp-value-area-cell-height* 16 
  "The heigth of table cell displaying values for a single TP")
(defParameter *tp-value-nb-of-cells* 
  (/ *tp-value-area-height* *tp-value-area-cell-height*)
  "Number of cells of the TP value-area for displaying TP values")
;---
(defVar *detail-window-cell-height* 16
  "the height of a cell in the window showing multiple values one per line")
(defVar *detail-window-width* (- *tp-work-area-width* 50)
  "the width of the window displaying values")
;---
(defParameter *sp-area-v-position* (+ *tp-area-v-position* *tp-edit-area-height* 
                                      *editor-inter-area-height*) 
  "The vertical position of the start of the SP area")
(defParameter *sp-left-button-h-position* 10 
  "The left margin for the SP left button array")
(defParameter *sp-button-vertical-offset* 10 
  "The vertical offset of SP left button array")
(defParameter *sp-button-vertival-spacing* 14 
  "The vertical spacing between SP left buttons")
(defParameter *sp-button-vertival-thickness* 16 
  "The vertical thickness of SP buttons")
(defParameter *sp-button-width* 80 
  "The width of a TP button")
;---
(defParameter *sp-edit-area-width* *editor-left-width* 
  "The left margin for the SP left button array")
(defParameter *sp-edit-area-height* 160 
  "The left margin for the SP left button array")

(defParameter *sp-work-area-h-position* 
  (+ *tp-left-button-h-position* *tp-button-width* *editor-inter-area-width*
     *sp-edit-area-width* *editor-inter-area-width* *tp-button-width*
     *editor-inter-area-width*)
  "The left margin for the SP work area")
(defParameter *sp-work-area-v-position* *sp-area-v-position* 
  "The vertical start position of the SP work area")
;---
(defParameter *il-area-v-position* 
  (+ *sp-area-v-position* *sp-edit-area-height* *editor-inter-area-height*) 
  "The vertical position of the start of the IL area")
(defParameter *il-left-button-h-position* 10 
  "The left margin for the IL left button array")
(defParameter *il-button-vertical-offset* 10 
  "The vertical offset of IL left button array")
(defParameter *il-button-vertival-spacing* 14 
  "The vertical spacing between IL left buttons")
(defParameter *il-button-vertival-thickness* 16 
  "The vertical thickness of IL buttons")
(defParameter *il-button-width* 80 
  "The width of an IL button")
;---
(defParameter *il-edit-area-width* *editor-left-width* 
  "The left margin for the SP left button array")
(defParameter *il-edit-area-height* 160 
  "The left margin for the SP left button array")
;---
(defParameter *message-area-v-position* (+ *il-area-v-position*
                                           *il-edit-area-height* 
                                           *editor-inter-area-height*) 
  "The vertical position of the start of the message area")
(defParameter *message-area-width* 400 
  "The width of the message area.")
(defParameter *message-area-height* 40 
  "The height of the message area.")
;---
(defParameter *sel-area-h-position* 
  (+ *tp-left-button-h-position* *tp-button-width* *editor-inter-area-width*
     *sp-edit-area-width* *editor-inter-area-width* *tp-button-width*
     *editor-inter-area-width*) 
  "The left margin of the selection area")
;---
(defParameter *sel-area-v-position* *il-area-v-position* 
  "The vertical position of the start of the message area.")
(defParameter *sel-area-width* *editor-right-width* 
  "The width of the message area.")
(defParameter *sel-area-height* 
  (+ *il-edit-area-height* *editor-inter-area-height* *message-area-height*)
  "The height of the message area.")
(defParameter *sel-button-h-position* 10 
  "The left margin for the Selection button array")
(defParameter *sel-button-vertical-offset* 10 
  "The vertical offset of Selection button array")
(defParameter *sel-button-vertival-spacing* 14 
  "The vertical spacing between Selction buttons")
(defParameter *sel-button-vertival-thickness* 16 
  "The vertical thickness of Selection buttons")
(defParameter *sel-button-width* 80 
  "The width of a Selection button")

;;;========== colors
;;; Colors do not have the same hew in Windows or Mac environment...
;(defConstant *cantaloup* 16760662)  ; yellowish
(defConstant +imperial-yellow-color+ 
  (%RGB (* 252 256) (* 244 256) (* 194 256))
  )  ; yellowish
;(defVar +dim-color+ 16766339 "color indicating a non active view")
(defConstant +dim-color+  
  (%rgb (* 249 256) (* 252 256)(* 216 256))
  "color indicating a non active view")

;;;========== fonts
(defConstant +static-text-font-18+ 
  (cg:make-font-ex :SWISS "Chicago / ANSI" 18 '(:BOLD))
  "font for displaying static text")
(defConstant +static-text-font-11+ 
  (cg:make-font-ex :SWISS "Tahoma / ANSI" 11 '(:BOLD))
  "font for displaying smaller static text")
(defConstant +static-text-font-11-it+ 
  (cg:make-font-ex :SWISS "Helvetica / ANSI" 11 '(:italic))
  "font for displaying static text small italics")
(defConstant +static-text-font-10-b+ 
  (cg:make-font-ex :SWISS "Helvetica / ANSI" 10 '(:bold))
  )
(defConstant +static-text-font-10-it+ 
  (cg:make-font-ex :SWISS "Helvetica / ANSI" 10 '(:italic))
  )
(defConstant +detail-window-text+ 
  (cg:make-font-ex :SWISS "MS Sans Serif / ANSI" 11 NIL)
  )

;;;=============================================================================
;;;
;;;                            EDIT FUNCTION
;;;
;;;=============================================================================
;;; *editor-window* is a global variable, different in each editing process, that 
;;; points to the last created editor.
;;; If we create a new editor recursively and temporarily go back to a previous
;;; one, then *editor-window* does not point onto the active editor but on the 
;;; last one that was created..

;;; An editing session starts when the first editor is created, and ends when it 
;;; is exited with an abort or commit. On abort all changes done on any of the
;;; recursivey called editors are abandonned, meaning that a change committed on
;;; a recursively called editor is not final until the end of the editing session.

;;; if database is NIL and if the edited object exists on disk, then the object is
;;; modified in core and not on disk, which could lead to inconsistencies if  some
;;; of the linked objects are called from disk later on.

(defUn edit (obj-id &key owner database previous-editor action 
                    (current-package *package*) (language *language*) 
                    (context (symbol-value (intern "*CONTEXT*")))
                    (version-graph (intern "*VERSION-GRAPH*"))
                    gate)
  "function that creates an editor. If object is not a MOSS object, quits. If no ~
   editor is already opened, creates a new editor (window) records that owner is ~
   nil, record the database, creates an editing session, record the editing box. 
   When the caller is another editor, enters recursive editing, which changes the ~
   abort/commit exit behavior.
   We assume that the new editor window is in the same package as the previous ~
   ones if any.
   If database is nil, then the modified object is not saved on disk.
Arguments:
   obj-id: points to the object to edit
   owner (key): ? (agent?, application?)
   previous-editor (key): the last opened editor window
   database (key): the database reference, for saving changes when leaving
   previous-editor (key): previous editor window when spawning from it
   action (key): :create, i.e. obj-id is id of class to create an instance, or
                 :clone, i.e. obj-id is id of instance to clone
   gate (key): a gate structure passed to the first editor of a chain
   current-package (key): editing package
   context (key): editing context
   language (key: editing langage
   version-graph (key): editing configuration tree
Return:
   :done"
  (proclaim (list 'special (intern "*EDITOR-WINDOW*" current-package)))
  ;; if object not PDM, we quit
  (with-package current-package
    (unless (moss::%pdm? obj-id)
      (warn "While trying to create a new editor, ~S not a PDM object" obj-id)
      (return-from edit)))
  
  (print `("edit/ *package*" ,*package*))
  
  ;; edwin will point to the previous editor window (needed when edit is called
  ;; outside a previous editor, e.g. from a browser); we assume same package
  ;; box-pointer will host the current editing box (the previous one can be 
  ;; obtained from the previous editor if needed)
  (let ((old-edwin (intern "*EDITOR-WINDOW*" current-package))
        new-editor)
    
    ;(format t "~%; moss::edit /*package*: ~S" *package*)
    
    ;; are we editing or sub-editing (*editor-window* points to the last editor)
    (cond
     ((or previous-editor
          (and (boundp old-edwin) (symbol-value old-edwin)))
      ;; here, some editor window already exists
      ;; however, when we call from outside the editor, previous-editor may be nil
      ;; if so recover pointer from old *editor-window*
      (setq previous-editor (or previous-editor (symbol-value old-edwin)))
      
      ;; create new page, which changes the value of *editor-window* and links
      ;; to previous editor. A new editing box is created and *editing-box* is set
      (setq new-editor (make-editor-window obj-id 
                                           :old-page previous-editor
                                           :package current-package
                                           :language language
                                           :context context
                                           :version-graph version-graph))
      ;; link old editor to new editor
      (setf (next-editor previous-editor) new-editor)
      ;; copy owner (e.g. OMAS agent)
      (setf (owner new-editor) (owner previous-editor))
      ;; also record database
      (setf (database new-editor) (database previous-editor))
      ;; add owner to editing box
      (setf (owner (editing-box new-editor)) owner)
      )

     (t
      ;; here no previous editor, meaning that we start editing

      ;; here we create an editor window, changing the value of *editor-window*
      (setq new-editor (make-editor-window obj-id
                                           :gate gate
                                           :package current-package
                                           :language language 
                                           :context context
                                           :version-graph version-graph))
      
      ;; save owner even if nil (could be application or agent in OMAS)
      (setf (owner new-editor) owner)
      ;; record potential database
      (setf (database new-editor) database)
      ;; add owner to editing box
      (setf (owner (editing-box new-editor)) owner)
      )
     )

    (with-editor-environment new-editor
      ;; create object to edit
      (case action
        (:create
         (setq obj-id (%make-instance obj-id))
         )
        (:clone
         (setq obj-id (send obj-id '=clone))
         )
        )
      ;; record new object in previous editor
      (if previous-editor
        (setf (new-object previous-editor) obj-id))
     
      ;; now ready to initialize page
      (initialize-editor-window new-editor obj-id)
      )
    ))

#|
(with-package :news
  (edit 'news::$E-SECTION.5 :owner news::SA_NEWS))
|#
;;;======================== Positioning Functions ==================================
;;; All positions  have been parametrized so that one can change the global
;;; size of the edit window.

;;; ===== IL area

(defUn il-dap ()
  "IL display area position; Position of the area in which we display the list ~
   of inverse properties associated to a specific object."
  (%make-point
   (+ *sp-left-button-h-position* *sp-button-width* *editor-inter-area-width*)
   *il-area-v-position*))

(defUn il-das ()
  "SP display area size; Size of the area in which we display the list ~
   of structural properties associated to a specific object."
  (%make-point
   *sp-edit-area-width*
   *sp-edit-area-height*))

;;; ===== Message area

(defUn msg-dap ()
  "Message display area position; Position of the area in which we display messages ~
   to the user."
  (%make-point
   (+ *sp-left-button-h-position* *sp-button-width* *editor-inter-area-width*)
   *message-area-v-position*))

(defUn msg-das ()
  "Message display area size; Size of the area in which we display messages ~
   to the user."
  (%make-point
   *message-area-width*
   *message-area-height*))

;;; ===== Selection area

(defUn sel-aep ()
  "Selection area position; Position of the area in which we display the list ~
   of properties available for creating a specific object."
  (%make-point
   *sp-work-area-h-position*
   (+ *sel-area-v-position* 5)))

(defUn sel-agp ()
  "Selection area position; Position of the area in which we display the list ~
   of properties available in the system."
  (%make-point
   (+ *sp-work-area-h-position* (/ *editor-right-width* 2))
   (+ *sel-area-v-position* 5)))

(defUn sel-das ()
  "Selection area size; Size of the area in which we display the list ~
   of structural properties associated to a specific object."
  (%make-point
   (/ *sel-area-width* 2)
   *sel-area-height*))

(defUn sel-msb (nb)
  "SP make Selection button. Computes the position of a selection button for the ~
   selection area from *sp-left-button-h-position*, *sp-area-v-position*, ~
   *sp-button-vertival-spacing*, *sp-button-vertical-offset*, *sp-area-width* ~
   from the arg which is the button numero (1, 2, 3,...)."
  (%make-point 
   (+ *sp-left-button-h-position* *sp-button-width* *editor-inter-area-width*
      *sp-edit-area-width* *editor-inter-area-width*)
   (+ *sel-button-vertical-offset*
      *sel-area-v-position*
      (* (1- nb) (+ *sel-button-vertival-spacing* *sel-button-vertival-thickness*)))))

(defUn sel-bs ()
  "Size of a selection button."
  (%make-point *sel-button-width* *sel-button-vertival-thickness*))

;;; ===== IL area

(defUn il-mlb (nb)
  "IL make left button. Computes the position of a left button for the IL ~
   edit area from *il-left-button-h-position*, *il-area-v-position*, ~
   *il-button-vertival-spacing*, *il-button-vertical-offset*, from the arg ~
   which is the button numero (1, 2, 3,...)."
  (%make-point 
   *il-left-button-h-position*
   (+ *il-button-vertical-offset*
      *il-area-v-position*
      (* (1- nb) (+ *il-button-vertival-spacing* *il-button-vertival-thickness*)))))

(defUn il-lbs ()
  "IL left button size. Computes the size of a left button for the IL ~
   edit area from *il-left-button-width*, *il-button-vertical-thickness."
  (%make-point *il-button-width* *il-button-vertival-thickness*))

;;; ===== SP area

(defUn sp-mlb (nb)
  "SP make left button. Computes the position of a left button for the SP ~
   edit area from *sp-left-button-h-position*, *sp-area-v-position*, ~
   *sp-button-vertival-spacing*, *sp-button-vertical-offset*, from the arg ~
   which is the button numero (1, 2, 3,...)."
  (%make-point 
   *sp-left-button-h-position*
   (+ *sp-button-vertical-offset*
      *sp-area-v-position*
      (* (1- nb) (+ *sp-button-vertival-spacing* *sp-button-vertival-thickness*)))))

(defUn sp-lbs ()
  "SP left button size. Computes the size of a left button for the SP ~
   edit area from *sp-left-button-h-position*, *sp-area-v-position*, ~
   *sp-button-vertival-spacing*, *sp-button-vertical-offset*, from the arg ~
   which is the button numero (1, 2, 3,...)."
  (%make-point *sp-button-width* *sp-button-vertival-thickness*))

(defUn sp-dap ()
  "SP display area position; Position of the area in which we display the list ~
   of structural properties associated to a specific object."
  (%make-point
   (+ *sp-left-button-h-position* *sp-button-width* *editor-inter-area-width*)
   *sp-area-v-position*))

(defUn sp-das ()
  "SP display area size; Size of the area in which we display the list ~
   of structural properties associated to a specific object."
  (%make-point
   *sp-edit-area-width*
   *sp-edit-area-height*))

(defUn sp-mrb (nb)
  "SP make right button. Computes the position of a right button for the SP ~
   edit area from *sp-left-button-h-position*, *sp-area-v-position*, ~
   *sp-button-vertival-spacing*, *sp-button-vertical-offset*, *sp-area-width* ~
   from the arg which is the button numero (1, 2, 3,...)."
  (%make-point 
   (+ *sp-left-button-h-position* *sp-button-width* *editor-inter-area-width*
      *sp-edit-area-width* *editor-inter-area-width*)
   (+ *sp-button-vertical-offset*
      *sp-area-v-position*
      (* (1- nb) (+ *sp-button-vertival-spacing* *sp-button-vertival-thickness*)))))
;---
(defUn sp-w-text-pos (nb &optional (deltav 0))
  "Position of the various labels of the SP work area. They are computed with ~
   respect to the left upper corner of the area (parameters). They include: ~
   class, property, and entry, like in the LOB command window."
  (%make-point 
   *sp-work-area-h-position* 
   (+ *sp-work-area-v-position* (* (1- nb) 30) deltav )))

(defUn sp-w-input-pos (nb)
  "Position of the various labels of the SP work area. They are computed with ~
   respect to the left upper corner of the area (parameters). They include: ~
   class, property, and entry, like in the LOB command window."
  (%make-point 
   (+ *sp-work-area-h-position* 70)
   (+ *sp-work-area-v-position* (* (1- nb) 30))))

(defUn sp-wd ()
  "Position of the display area showing the result of the query in the SP work area."
  (%make-point
   (+ *sp-work-area-h-position* 210)
   *sp-work-area-v-position*))

;;; ===== TP area

(defUn tp-mlb (nb)
  "TP make left button. Computes the position of a left button for the TP ~
   edit area from *tp-left-button-h-position*, *tp-area-v-position*, ~
   *tp-button-vertival-spacing*, *tp-button-vertical-offset*, from the arg ~
   which is the button numero (1, 2, 3,...)."
  (%make-point 
   *tp-left-button-h-position*
   (+ *tp-button-vertical-offset*
      *tp-area-v-position*
      (* (1- nb) (+ *tp-button-vertival-spacing* *tp-button-vertival-thickness*)))))

(defUn tp-lbs ()
  "TP left button size. Computes the size of a left button for the TP ~
   edit area from *tp-left-button-h-position*, *tp-area-v-position*, ~
   *tp-button-vertival-spacing*, *tp-button-vertical-offset*, from the arg ~
   whic is the button numero (1, 2, 3,...)."
  (%make-point *tp-button-width* *tp-button-vertival-thickness*))

(defUn tp-dap ()
  "TP display area position; Position of the area in which we display the list ~
   of terminal propoerties associated to a specific object."
  (%make-point
   (+ *tp-left-button-h-position* *tp-button-width* *editor-inter-area-width*)
   *tp-area-v-position*))

(defUn tp-das ()
  "TP display area size; Size of the area in which we display the list ~
   of terminal propoerties associated to a specific object."
  (%make-point
   *tp-edit-area-width*
   *tp-edit-area-height*))

(defUn tp-mrb (nb)
  "TP make right button. Computes the position of a right button for the TP ~
   edit area from *tp-left-button-h-position*, *tp-area-v-position*, ~
   *tp-button-vertival-spacing*, *tp-button-vertical-offset*, *tp-area-width* ~
   from the arg which is the button numero (1, 2, 3,...)."
  (%make-point 
   (+ *tp-left-button-h-position* *tp-button-width* 
      *editor-inter-area-width* *tp-edit-area-width* *editor-inter-area-width*)
   (+ *tp-button-vertical-offset*
      *tp-area-v-position*
      (* (1- nb) (+ *tp-button-vertival-spacing* *tp-button-vertival-thickness*)))))
;---
(defUn tp-wap ()
  "TP work area position; Position of the area in which we actually do the editing ~
   of a particular value associated with a TP (one at a time)"
  (%make-point
   (+ *tp-left-button-h-position* *tp-button-width* *editor-inter-area-width*
      *tp-edit-area-width* *editor-inter-area-width* *tp-button-width*
      *editor-inter-area-width*)
   (+ *tp-area-v-position* 100)))

(defUn tp-was ()
  "TP work area size; Size of the area in which we actually do the editing."
  (%make-point
   *editor-right-width*  
   (- *tp-work-area-height* 100)))

(defUn tp-vap ()
  "TP detail area position; Position of the area in which we list all the ~
   values associated with a TP (one per line)"
  (%make-point
   (+ *tp-left-button-h-position* *tp-button-width* *editor-inter-area-width*
      *tp-edit-area-width* *editor-inter-area-width* *tp-button-width*
      *editor-inter-area-width*)
   *tp-area-v-position*))

(defUn tp-vas ()
  "TP work area size; Size of the area in which we actually do the editing."
  (%make-point
   *editor-right-width*  
   *tp-value-area-height*))

;;;=============================================================================
;;;
;;;                               CLASSES
;;;
;;;=============================================================================

;;;=============================================================================
;;;                               BUTTONS
;;;=============================================================================


;;; buttons can have 2 states: enabled or disabled

;;;--------------------------------------------------------------- EDITOR-BUTTON

;;; First create a special class of buttons

(defClass editor-button (cg:button) ())

;;;--- methods

(defMethod activate ((xx editor-button))
  "simply enable the button"
  (%enable-item xx))

(defMethod deactivate ((xx editor-button))
  "simply disable the button"
  (%disable-item xx))

;;;-------------------------------------------------------- EDITOR-ACTIVE-BUTTON

;;; special class for abort and commit buttons, so that they are never deactivated

(defClass editor-active-button (cg:button) ())

;;;=============================================================================
;;;                     VARIOUS COMBINATIONS OF BUTTONS
;;;=============================================================================

;;; organized by areas. add kill button to all: always activated

(defParameter *ew-tp-left-button-list*
  '(:tp-ADD-left-button :tp-EDIT-right-button
    :tp-MODIFY-right-button :tp-DELETE-right-button))

(defParameter *ew-sp-left-button-list*
  '(:sp-ADD-left-button :sp-EDIT-right-button
    :sp-MODIFY-right-button :sp-DELETE-right-button))

(defParameter *ew-tp-right-button-list*
  '(:tp-OK-right-button :tp-ADD-right-button :tp-INSERT-right-button
    :tp-MODIFY-right-button :tp-DELETE-right-button 
    :tp-quit-right-button))

(defParameter *ew-sp-right-button-list*
  '(:sp-OK-right-button :sp-ADD-right-button :sp-INSERT-right-button
    :sp-DELETE-right-button :sp-CANCEL-right-button
    :sp-QUIT-right-button :sp-CLONE-right-button
    :sp-CREATE-right-button :EXAMINE-button))

;;;----------------------------------------------------- EWF-BUTTONS-ENABLE-ONLY

(defUn ewf-buttons-enable-only (current-page button-list)
  "disable all buttons and enable those given in the list."
  (let* ((item-list (mapcar #'(lambda(xx) (%find-named-item xx current-page))
                            button-list)))
    (dolist (sw (%get-window-panes current-page)) 
      (cond
       ;; should remove abort and commit button from the list 
       ;;... but also examine...
       ((not (typep sw 'EDITOR-BUTTON)))
       ((member sw item-list) (activate sw))
       (t (deactivate sw)))))
  )

;(ewf-buttons-enable-only *editor-window* '(:tp-add-right-button))

;;;--------------------------------------------- EWF-EDIT-SP-BUTTONS-DISABLE-ALL

(defUn ewf-edit-sp-buttons-disable-all (win)
  "disable all the buttons of the search area"
  (mapc #'(lambda (xx) (deactivate (%find-named-item xx win)))
        *ew-sp-right-button-list*))



;;;=============================================================================
;;;                             EDITING AREAS
;;;=============================================================================

;;; Various areas in which one writes or displays objects

;;;------------------------------------------------------ EDITOR-AREA-FOR-VALUES

;;; a class for displaying lists of values

(defClass editor-area-for-values (cg:multi-item-list) 
  ((original-list)
   (list-type :accessor list-type)
   (sp-list :accessor sp-list)
   (tp-list :accessor tp-list)
   (il-list :accessor il-list)
   (object-list :accessor object-list :initform ())
   ))

(defMethod activate ((xx editor-area-for-values))
  "reset and activate area"
  (%set-item-color xx  *white-color*)
  (%enable-item xx)
  (%set-item-sequence xx nil)
  (%set-item-value xx ())
  t)

(defMethod deactivate ((xx editor-area-for-values))
  "simply clean and deactivate area"
  (%set-item-color xx  +dim-color+)
  (%disable-item xx)
  (%set-item-sequence xx nil)
  (%set-item-value xx ())
  t)

;;;-------------------------------------------------------- EDITOR-AREA-FOR-TEXT

;;; a class for entering text (concept, property, entry-point)

(defClass editor-area-for-text (cg:editable-text) ())

(defMethod activate ((xx editor-area-for-text))
  "reset and activate area"
  (%set-item-color xx  *white-color*)
  (%enable-item xx)
  (%set-item-value xx "")
  t)

(defMethod deactivate ((xx editor-area-for-text))
  "simply clean and deactivate area"
  (%set-item-color xx  +dim-color+)
  (%disable-item xx)
  (%set-item-value xx "")
  t)

;;;---------------------------------------------------------- EDITOR-STATIC-TEXT

(defClass editor-static-text (cg:static-text)())


;;;================================== LEFT AREA ================================

;;; Left areas for values are special in the sense that when we close them, we 
;;; do not erase the inside text... unless specifically required

(defClass editor-left-area-for-values (editor-area-for-values) ())

(defMethod activate ((xx editor-left-area-for-values))
  "reset and activate area"
  (%set-item-color xx  *white-color*)
  (%enable-item xx)
  t)

(defMethod deactivate ((xx editor-left-area-for-values))
  "simply clean and deactivate area"
  (%set-item-color xx  +dim-color+)
  (%disable-item xx)
  t)

;;;================================= RIGHT AREA ================================

;;;========== TP/SP DISPLAYING INFO

;;;------------------------------------------------ EDITOR-VALUE-AREA-TITLE-ITEM

(defClass editor-value-area-title-item (editor-static-text)())

(defMethod activate ((xx editor-value-area-title-item))
  "same as close..."
  (%set-item-value xx "")
  t)

(defMethod deactivate ((xx editor-value-area-title-item))
  "simply clean and deactivate area"
  (%set-item-value xx "")
  t)

;;;------------------------------------------------------ EDITOR-VALUE-AREA-ITEM
;;; VALUE AREA is used to display values of a given property. Used for selecting 
;;; them

(defClass editor-value-area-item (editor-area-for-values) ())


;;;--------------------------------------------------------- EDITOR-TP-WORK-ITEM
;;;TP-WORK-ITEM is the scratchpad area for text values of attributes

(defClass editor-tp-work-item (editor-area-for-text) ())

;;;========== SP EDITING AREA

;;;----------------------------------------------------------- EDITOR-CLASS-ITEM
;;; used for locating a concept

(defClass editor-class-item (editor-area-for-text)())

(defMethod activate ((xx editor-class-item))
  "undim, reset internal variables and make available"
  (%set-item-color xx  *white-color*)
  (%enable-item xx)
  (%set-item-value xx "")
  (setf (current-ep-class (%container xx)) nil)
  (setf (current-ep-object-list (%container xx)) nil)
  t)

(defMethod deactivate ((xx editor-class-item))
  "simply clean and deactivate area"
  (%set-item-color xx  +dim-color+)
  (%disable-item xx)
  (%set-item-value xx "")
  (setf (current-ep-class (%container xx)) nil)
  t)

;;;----------------------------------------------------- EDITOR-ENTRY-POINT-ITEM
;;; used for locating an entry point

(defClass editor-entry-point-item (editor-area-for-text) ())

(defMethod activate ((xx editor-entry-point-item))
  "clean area, reset internal variables and activate area"
  (%set-item-color xx  *white-color*)
  (%enable-item xx)
  (%set-item-value xx "")
  (setf (current-ep-string (%container xx)) "")
  (setf (current-ep-object-list (%container xx)) nil)
  t)

(defMethod deactivate ((xx editor-entry-point-item))
  "simply clean and deactivate area"
  (%set-item-color xx  +dim-color+)
  (%disable-item xx)
  (%set-item-value xx "")
  (setf (current-ep-string (%container xx)) "")
  (setf (current-ep-object-list (%container xx)) nil)
  t)

;;;-------------------------------------------------------- EDITOR-PROPERTY-ITEM
;;; used for specifying a property when looking for an individual

(defClass editor-property-item (editor-area-for-text) ())

(defMethod activate ((xx editor-property-item))
  "reset and activate area"
  (%set-item-color xx  *white-color*)
  (%enable-item xx)
  (%set-item-value xx "")
  (setf (current-ep-property (%container xx)) nil)
  (setf (current-ep-object-list (%container xx)) nil)
  t)

(defMethod deactivate ((xx editor-property-item))
  "simply clean and deactivate area"
  (%set-item-color xx  +dim-color+)
  (%disable-item xx)
  (%set-item-value xx "")
  (setf (current-ep-property (%container xx)) nil)
  t)

;;;----------------------------------------------------------- EDITOR-QUERY-ITEM
;;; used for inputting queries when looking for ojects

(defClass editor-query-item (cg:multi-line-editable-text)())

(defMethod activate ((xx editor-query-item))
  "reset and activate area"
  (%enable-item xx)
  (%set-item-color xx  *white-color*)
  (%set-item-value xx "")
  (setf (current-query (%container xx)) nil)
  (setf (current-parsed-expr (%container xx)) nil)
  t)

(defMethod deactivate ((xx editor-query-item))
  "simply clean and deactivate area"
  (%disable-item xx)
  (%set-item-color xx  +dim-color+)
  (%set-item-value xx "")
  (setf (current-query (%container xx)) nil)
  (setf (current-parsed-expr (%container xx)) nil)
  t)

;;;--------------------------------------------------------- EDITOR-COUNTER-ITEM
;;; area in which we post the number of individuals corresponding to the selected
;;; concept

(defClass editor-counter-item (editor-area-for-text) ())

(defmethod activate ((xx editor-counter-item))
  "activate counter by changing background color"
  (%set-item-color xx  *white-color*)
  (%disable-item xx)
  (%set-item-value xx "0")
  t)

(defMethod deactivate ((xx editor-counter-item))
  "simply clean and deactivate area"
  (%set-item-color xx  +dim-color+)
  (%disable-item xx)
  (%set-item-value xx "")
  t)

;;;---------------------------------------------------------- EDITOR-OBJECT-LIST
;;; Area where we post the list of instances of a concept so that we can select
;;; some to link to current object

(defclass EDITOR-OBJECT-LIST (EDITOR-AREA-FOR-VALUES)())

(defMethod activate ((xx editor-object-list))
  "also reset the area"
  (%set-item-color xx  *white-color*)
  (%enable-item xx)
  (%set-item-sequence xx nil)
  ;; reset saved list
  (setf (object-list xx) nil)
  ;; also reset parent values
  (setf (current-ep-object-list (%container xx)) nil)
  (%set-item-value xx nil)
  t)

(defMethod deactivate ((xx editor-object-list))
  "simply clean and deactivate area"
  (%set-item-color xx  +dim-color+)
  (%disable-item xx)
  (%set-item-sequence xx nil)
  ;; reset saved list
  (setf (object-list xx) nil)
  ;; also reset parent values
  (setf (current-ep-object-list (%container xx)) nil)
  (%set-item-value xx nil))

;;;=============================== Thermometer widget ============================

;;;----------------------------------------------------------- EDITOR-GAUGE-ITEM

(defClass editor-gauge-item (cg:progress-indicator) ())

(defMethod activate ((xx editor-gauge-item))
  (%enable-item xx)
  (%set-item-value xx 0)
  t)

(defMethod deactivate ((xx editor-gauge-item))
  "simply clean and deactivate area"
  ;(%set-item-color xx  +dim-color+)
  (%disable-item xx)
  (%set-item-value xx 0)
  )

;;;========== LOCAL and GLOBAL PROPERTIES

;;; No special class, use area-for-values

;;;=============================================================================
;;;                     VARIOUS COMBINATIONS OF AREAS
;;;=============================================================================

;;;-------------------------------------------------------- *EW-AREA-NICK-NAMES*
;;; all areas nick names

(defParameter *ew-area-nick-names* 
  '(:tp-item :sp-item :il-item :message-item
    :value-area-title :value-area :tp-work-item
    :class-item :property-item :entry-point-item :counter-item 
    :object-list-item :query-item
    :local-property-item :global-property-item)
  "all Editor views that can be activated or deactivated excepting buttons")

;;;-------------------------------------------------------------- *EW-LEFT-SIDE*

(defParameter *ew-left-side*
  '(:tp-item :sp-item :il-item :message-item)
  "left areas including messages")

;;;------------------------------------------------------------- *EW-RIGHT-SIDE*

(defParameter *ew-right-side*
  '(:value-area :tp-work-item
    :class-item :property-item :entry-point-item :counter-item 
    :object-list-item :query-item
    :local-property-item :global-property-item)
  "right areas")

;;;------------------------------------------------ *EW-SP-RIGHT-AREA-NICKNAMES*
;;; areas in the SP editing part

(defParameter *ew-sp-right-area-nicknames*
  '(:value-area-title :value-area
    :class-item :property-item :entry-point-item :counter-item
    :object-list-item :query-item :gauge-item))

;;;---------------------------------------------------------- *EW-SP-EDIT-STUFF*
;;; areas + buttons
(defParameter *ew-sp-edit-stuff*
  (append *ew-sp-right-button-list* *ew-sp-right-area-nicknames*))

;;;------------------------------------------------ *EW-TP-RIGHT-AREA-NICKNAMES*
;;; areas in the TP editing part

(defParameter *ew-tp-right-area-nicknames*
  '(:value-area :tp-work-item :value-area-title))

;;;========== Actions

;;;----------------------------------------------------------- EWF-ADJUST-SCROLL

(defUn ewf-adjust-scroll (item index)
  "adjust scroll so that index corresponds to a visible position"
  (%scroll-to-cell item index)
  item)

;;;------------------------------------------------------ EWF-ACTIVATE-AREA-LIST

(defUn ewf-activate-area-list (nick-name-list win)
  "enable an area specified by its nick-name"
  (mapc #'(lambda (nick-name) (activate (%find-named-item nick-name win)))
        nick-name-list)
  t)
#|
? (ewf-area-list-activate *ew-sp-right-area-nicknames*)
|#
;;;--------------------------------------------------- EWF-ACTIVATE-EDIT-SP-AREA

(defUn ewf-activate-edit-sp-area (win)
  "activates the sp edit area, enabling it and reset internal variables"
  (unless win 
    (beep)
    (return-from ewf-activate-edit-sp-area))
 
  ;; internal variables
  (setf (current-ep-object-list win) nil)
  (setf (current-ep-class win) nil)
  (setf (current-ep-property win) nil)
  (setf (current-ep-string win) "")

  (ewf-activate-area-list *ew-sp-right-area-nicknames* win)

  ;; disable message area  
  (activate (%find-named-item :message-item win))
  ;; install cursor into the class view
  (%set-focus-item (%find-named-item :class-item win))
  t)

;(ewf-edit-sp-area-open *editor-window*)

;;;----------------------------------------------------- EWF-ACTIVATE-EDIT-TP-AREA

(defUn ewf-activate-edit-tp-area (win)
  "opens the edit tp area by activating the corresponding views, but no buttons."
  (unless win 
    (beep)
    (return-from ewf-activate-edit-tp-area nil))
  (ewf-activate-area-list *ew-tp-right-area-nicknames* win)
  t)

;(ewf-activate-edit-tp-area *editor-window*)

;;;--------------------------------------------------- EWF-ACTIVATE-LEFT-IL-AREA

(defUn ewf-activate-left-il-area (win)
  "activates the left il part of the window"
  (activate (%find-named-item :il-item win))
  t)

;;;------------------------------------------------------ EWF-ACTIVATE-LEFT-SIDE

(defUn ewf-activate-left-side (win)
  "activate tp, sp, and il areas"
  (ewf-activate-area-list *ew-left-side* win)
  t)

;;;--------------------------------------------------- EWF-ACTIVATE-LEFT-SP-AREA

(defUn ewf-activate-left-sp-area (win)
  "activates the left SP part of the window"
  (activate (%find-named-item :sp-item win))
  t)

;;;--------------------------------------------------- EWF-ACTIVATE-LEFT-TP-AREA

(defUn ewf-activate-left-tp-area (win)
  "activates the left TP part of the window"
  (activate (%find-named-item :tp-item win))
  t)

;;;---------------------------------------- EWF-ACTIVATE-PROPERTY-SELECTION-AREA

(defUn ewf-activate-property-selection-area (win)
  "Activates the selection area by enabling all items."
  (ewf-activate-area-list '(:local-property-item :global-property-item) win)
  t)

;;;=============================================================================

;;;---------------------------------------------------- EWF-DEACTIVATE-ALL-AREAS

(defUn ewf-deactivate-all-areas (win)
  "disable all areas except butttons, all right-side areas are cleaned"
  (ewf-deactivate-area-list *ew-area-nick-names* win)
  t)

#|
? (ewf-area-disable-all (symbol-value (intern "*EDITOR-WINDOW*")))
|#
;;;---------------------------------------------------- EWF-DEACTIVATE-AREA-LIST

(defUn ewf-deactivate-area-list (nick-name-list win)
  "disable an area specified by its nick-name"
  (mapc #'(lambda (nick-name) (deactivate (%find-named-item nick-name win)))
        nick-name-list)
  t)

;;;------------------------------------------------- EWF-DEACTIVATE-EDIT-SP-AREA

(defUn ewf-deactivate-edit-sp-area (win)
  "activate the sp edit area, enabling it and reset internal variables"
  (unless win 
    (%beep)
    (return-from ewf-deactivate-edit-sp-area nil))
  (ewf-deactivate-area-list *ew-sp-right-area-nicknames* win)
  ;; clean message area
  (ewf-post-message "" win)
  t)

;;;------------------------------------------------- EWF-DEACTIVATE-EDIT-TP-AREA

(defUn ewf-deactivate-edit-tp-area (win)
  "close the upper right part of the edit window: disable and dim areas, ~
   disable all buttons, reset all concerned internal variables"
  (unless win 
    (%beep)
    (return-from ewf-deactivate-edit-tp-area nil))
  ;; clean the top right title, display window and work window
  (ewf-deactivate-area-list *ew-tp-right-area-nicknames* win)
  ;; reset the values cached in the current-page object
  (setf (current-property-action win) nil)
  (setf (current-property win) nil)
  (setf (current-value-list win) nil)
  (setf (current-value win) nil)
  ;; we do not remove the property type however, this is done when cleaning the
  ;; left property area

  ;; disable all buttons
  (mapc #'(lambda (xx) (deactivate (%find-named-item xx win)))
        *ew-tp-right-button-list*)
  t)

;;;------------------------------------------------ EWF-DEACTIVATE-LEFT-IL-AREA

(defUn ewf-deactivate-left-il-area (win)
  "activates the left il part of the window"
  (deactivate (%find-named-item :il-item win))
  t)

;;;---------------------------------------------------- EWF-DEACTIVATE-LEFT-SIDE

(defUn ewf-deactivate-left-side (win)
  "deactivate tp, sp, and il areas, but leave text visible"
  (ewf-deactivate-area-list *ew-left-side* win)
  t)

;;;------------------------------------------------ EWF-DEACTIVATE-LEFT-SP-AREA

(defUn ewf-deactivate-left-sp-area (win)
  "activates the left SP part of the window"
  (deactivate (%find-named-item :sp-item win))
  t)

;;;------------------------------------------------ EWF-DEACTIVATE-LEFT-TP-AREA

(defUn ewf-deactivate-left-tp-area (win)
  "activates the left TP part of the window"
  (deactivate (%find-named-item :tp-item win))
  t)

;;;---------------------------------------------------------- EWF-MESSAGE-CLOSE

(defUn ewf-deactivate-message (win)
  "close the message area: reset and  dim it"
  (let ((item (%find-named-item :message-item win)))
    (%set-item-text item "")
    (deactivate item)
    t))

;;;-------------------------------------- EWF-DEACTIVATE-PROPERTY-SELECTION-AREA

(defUn ewf-deactivate-property-selection-area (win)
  "Dims the selection area by disabling all items."
  (ewf-deactivate-area-list '(:local-property-item :global-property-item) win)
  t)

;;;--------------------------------------------------- EWF-DEACTIVATE-RIGHT-SIDE

(defUn ewf-deactivate-right-side (win)
  "deactivate tp, sp, and il areas, but leave text visible"
  (ewf-deactivate-area-list *ew-right-side* win)
  t)

;;;----------------------------------------------------------- EWF-DESELECT-AREA

(defUn ewf-deselect-area (item)
  "remove all selections in the specified area"
  (mapc #'(lambda (xx) (%unselect-cell-at-index item xx))
        (%get-selected-cells-indexes item)))

;;;------------------------------------------------------ EWF-DESELECT-AREA-LIST

(defUn ewf-deselect-area-list (win &rest area-list)
  "remove selection on items of the area name-list."
  (mapc #'(lambda (xx)
            (let ((cell-list 
                   (%get-selected-cells-indexes (%find-named-item xx win))))
              (when cell-list 
                (mapc #'(lambda (yy)(%unselect-cell-at-index 
                                     (%find-named-item xx win) yy)) 
                      cell-list))))
        area-list))

;;;=============================================================================
;;;                             DETAIL WINDOW
;;;=============================================================================

;;; special window to display the list of values attached to a property in the 
;;; left part of the editor

;;;-------------------------------------------------------- EDITOR-DETAIL-WINDOW

(defClass editor-detail-window (cg:dialog)
  ((object-list :accessor object-list :initform nil)
   (editor-page :accessor editor-page :initform nil) ; creator of the window
   ))

;;;=============================================================================
;;;                             EDITOR WINDOW CLASS
;;;=============================================================================

(defClass editor (cg:dialog)
  ((button-list :accessor button-list)
   (context :accessor context :initform nil)
   (current-action :accessor current-action)
   (current-entity :accessor current-entity)
   (current-ep-class :accessor current-ep-class :initform nil)
   (current-ep-object-list :accessor current-ep-object-list :initform nil)
   (current-ep-property :accessor current-ep-property :initform nil)
   (current-ep-string :accessor current-ep-string :initform "")
   (current-package :accessor current-package :initform nil)
   (current-parsed-expr :accessor current-parsed-expr :initform nil)
   (current-pos :accessor current-pos :initform nil)
   (current-property-type :accessor current-property-type :initform ())
   (current-property-list :accessor current-property-list)
   (current-property-position :accessor current-property-position :initform ())
   (current-property-action :accessor current-property-action :initform ())
   (current-property :accessor current-property)
   (current-query :accessor current-query :initform nil)
   ;; current selection of objects from the list of found objects (LFO)
   ;; should not be necessary since they are retrieved from the selection
   (current-selection :accessor current-selection :initform nil)
   (current-value :accessor current-value :initform ())
   ;; current list of values associated with a given property
   (current-value-list :accessor current-value-list)
   (current-position :accessor current-position :initform ())
   (database :accessor database :initform nil)
   (detail-window :accessor detail-window :initform nil)
   (editing-box :accessor editing-box :initform nil)
   (gate :accessor gate :initform nil)
   (language :accessor language :initform nil)
   (next-editor :accessor next-editor :initform nil)
   (new-object :accessor new-object :initform nil)
   (owner :accessor owner :initform nil)
   (previous-editor :accessor previous-editor :initform nil)
   (query-abort-condition :accessor query-abort-condition :initform nil)
   (version-graph :accessor version-graph :initform nil)
   )
  (:documentation 
   "Defines the format and behavior of pages used by the MOSS editor. ~
    Any object can be viewed and edited. Its terminal structural and inverse ~
    properties are shown. Objects connected to the displayed object can be shown by ~
    double clicking on them. When a property is multi-valued, then ~
    clicking on it, displays its values as a list. One then can click ~
    on an element of the detailed list to edit it"))

;;;--- methods

;;;--------------------------------------------------- INITIALIZE-EDITOR-WINDOW

(defUn initialize-editor-window (editor-window current-entity)
  "Takes a specific object, fills in the tp, sp, and il areas. Initializes the ~
   various window attributes: current-entity, current-property,... ~
   Activates and deactivates the various areas and buttons."
  (unless (%alive? current-entity *context*)
    (%beep)
    (ewf-deactivate-all-areas editor-window)
    ;; must clear the left part of the window
    (mapc #'(lambda (xx)
              (%set-item-sequence xx nil)
              (%set-item-value xx nil))
      (list (%find-named-item :tp-item editor-window)
            (%find-named-item :sp-item editor-window)
            (%find-named-item :il-item editor-window)
            ))
    (ewf-post-message "non existent or dead object, click COMMIT or ABORT"
                      editor-window)
    (return-from initialize-editor-window nil)
    )
  ;; first record current entity in the page structure
  (setf (current-entity editor-window) current-entity)
  ;; record any opened detailed window
  ;(setf (detail-window editor-window) *detail-window*) 
  ;; initialize header showing entity summary and class name
  (%set-item-text (%find-named-item :object-header-item editor-window)
                  (ewf-get-object-name current-entity))
  (%set-item-text (%find-named-item :class-header-item editor-window)
                  (ewf-get-class-name current-entity))
  (%set-item-text (%find-named-item :context-header-item editor-window)
                  (ewf-get-context))
  ;; clear other display areas
  ;(%set-item-text (%find-named-item :tp-work-item editor-window) "")
  ;(%set-item-sequence (%find-named-item :object-list-item editor-window) ())
  ;(%set-item-sequence (%find-named-item :local-property-item editor-window) ())
  ;(%set-item-sequence (%find-named-item :global-property-item editor-window) ())
  ;; deactivate all buttons
  (ewf-buttons-enable-only editor-window ())
  ;; deactivate right part of the editor
  (%set-item-text (%find-named-item :value-area-title editor-window) "")
  (ewf-deactivate-edit-tp-area editor-window)
  (ewf-deactivate-edit-sp-area editor-window)
  (ewf-deactivate-property-selection-area editor-window)
  ;; activate left part
  (ewf-activate-left-side editor-window)
  ;; then initialize tp, sp and il areas
  (%set-item-sequence (%find-named-item :tp-item editor-window)
                      (ewf-tp-get-property-and-value current-entity))
  (%set-item-sequence (%find-named-item :sp-item editor-window)
                      (ewf-sp-get-property-and-value current-entity))
  (%set-item-sequence (%find-named-item :il-item editor-window)
                      (ewf-il-get-property-and-value current-entity))
  (ewf-post-message "Welcome to the MOSS Editor..." editor-window)
  ;; return editor window
  editor-window)

;;; untested ???
(defMethod redisplay-window ((win editor))
  (set (intern "*EDITOR-WINDOW*") win)
  (set (intern "*EDITING-BOX*") (editing-box win))
  ;(progn (print (incf *c*)) nil)
  (call-next-method)
  t)

;;;--------------------------------------------------------------- EDITOR: CLOSE

(defmethod close ((ww editor) &key abort)
  "reset globals on closing editor window"
  (declare (ignore abort))
  (when (detail-window ww) (close (detail-window ww)))
  (ewformat t "~&;=== window-close; closing EDITOR: ~S" ww)
  (call-next-method ww))

;;;==================================================================================
;;;
;;;                               EDIT window
;;;
;;;==================================================================================

;;; One problem is that *editor-window* is a special variable of the MOSS package.
;;; If we define editors in different packages (e.g. agents) then the last created
;;; editor will crush the previous ones

;(make-editor-window '$PNAM)

(defUn make-editor-window (current-entity &key old-page gate (context *context*)
                                             (language *language*)
                                             (package *package*)
                                             (version-graph *version-graph*))
  "Creates a window to allow editing or creating PDM objects.
Arguments:
   current-entity: id of object to edit
   gate (key): a gate structure for the first editor of a chain
   old-page (key): previous editor if there is one
   context (key): editing context
   package (key): editing package
   language (key): editing language
   version-graph (key): editing configuration tree
Return:
   the window id"
  
  ;; create an application special variable app::*editor-window*
  (proclaim (list 'special (intern "*EDITOR-WINDOW*")))
  
  (let ((edwin 
         (cr-window :class 'EDITOR
                    :name (gentemp "EDITOR-" :keyword)
                    :title (format nil "MOSS Editor v~A" *moss-version-number*)
                    :top *editor-top*
                    :left *editor-left*
                    :width 1180
                    :height 715 ; includes header
                    :close-button nil
                    :font (cg:make-font-ex :SWISS "MS Sans Serif / ANSI" 11 NIL)
                    :subviews (make-editor-widgets current-entity old-page)
                    :background-color +imperial-yellow-color+)
                )
        edbox)

    ;; save editing parameters
    (setf (current-package edwin) package)
    (setf (language edwin) language)
    (setf (context edwin) context)
    (setf (version-graph edwin) version-graph)
    ;; if the first page of a chain put up the gate
    (unless old-page (setf (gate edwin) gate))

    ;; we must create an editing box, otherwise we cannot close the editor window...
    ;; this sets the *editing-box* variable
    (setq edbox (start-editing :package package))
    ;; link to editor
    (setf (editing-box edwin) edbox)
    
    ;; associate entity with the created page
    (setf (current-entity edwin) current-entity)
    ;; record previous page if any
    (when old-page (setf (previous-editor edwin) old-page))
    
    (ewformat t "~&;make-editor-window; current window: ~S" *editor-window*)
    
    (set (intern "*EDITOR-WINDOW*") edwin)))

;;;(defUn make-editor-window (current-entity &key old-page (context *context*)
;;;                                             (language *language*)
;;;                                             (package *package*)
;;;                                             (version-graph *version-graph*))
;;;  "Creates a window to allow editing or creating PDM objects.
;;;Arguments:
;;;   current-entity: id of object to edit
;;;   old-page (key): previous editor itf there is one
;;;   context (key): editing context
;;;   package (key): editing package
;;;   language (key): editing language
;;;   version-graph (key): editing configuration tree
;;;Return:
;;;   the window id"
;;;  
;;;  ;; create an application special variable app::*editor-window*
;;;  (proclaim (list 'special (intern "*EDITOR-WINDOW*")))
;;;  
;;;  (let ((edwin 
;;;         (cr-window :class 'EDITOR
;;;                    :name (gentemp "EDITOR-" :keyword)
;;;                    :title (format nil "MOSS Editor v~A" *moss-version-number*)
;;;                    :top *editor-top*
;;;                    :left *editor-left*
;;;                    :width 1180
;;;                    :height 715 ; includes header
;;;                    :close-button nil
;;;                    :font (cg:make-font-ex :SWISS "MS Sans Serif / ANSI" 11 NIL)
;;;                    :subviews (make-editor-widgets current-entity old-page)
;;;                    :background-color +imperial-yellow-color+)
;;;       
;;;;;;         (make-instance 
;;;;;;           'EDITOR
;;;;;;           :WINDOW-TYPE :document 
;;;;;;           :WINDOW-TITLE (format nil "MOSS Editor v~A" *moss-version-number*)
;;;;;;           :VIEW-POSITION (if old-page 
;;;;;;                            (+ (view-position old-page) (make-point 10 10))
;;;;;;                            '(:TOP 50))
;;;;;;           :VIEW-SIZE (make-point 1180 700)
;;;;;;           :COLOR-P t
;;;;;;           :VIEW-FONT '("Chicago" 12 :SRCOR :PLAIN)
;;;;;;           :VIEW-SUBVIEWS (make-editor-widgets current-entity old-page)
;;;;;;           :CLOSE-BOX-P nil)
;;;         )
;;;        edbox)
;;;
;;;    ;; save editing parameters
;;;    (setf (current-package edwin) package)
;;;    (setf (language edwin) language)
;;;    (setf (context edwin) context)
;;;    (setf (version-graph edwin) version-graph)
;;;
;;;    ;; we must create an editing box, otherwise we cannot close the editor window...
;;;    ;; this sets the *editing-box* variable
;;;    (setq edbox (start-editing :package package))
;;;    ;; link to editor
;;;    (setf (editing-box edwin) edbox)
;;;    
;;;    ;; associate entity with the created page
;;;    (setf (current-entity edwin) current-entity)
;;;    ;; record previous page if any
;;;    (when old-page (setf (previous-editor edwin) old-page))
;;;    
;;;    (ewformat t "~&;make-editor-window; current window: ~S" *editor-window*)
;;;    
;;;    (set (intern "*EDITOR-WINDOW*") edwin)))

#|
List of callbacks
-----------------
Top lines
  Object identity   ewf-get-object-name
  Class             ewf-get-class-name
  Context           ewf-get-context
  COMMIT            ew-commit-on-click
  ABORT             ew-abort-on-click

  KILL              ew-kill-on-click

Left Attribute area
  OK/END            ew-tp-left-ok-button-on-click
  ADD               ew-tp-left-add-button-on-click
  EDIT              ew-tp-left-edit-button-on-click
  DELETE            ew-tp-left-delete-button-on-click
  posting attributes            ewf-tp-get-property-and-value
  selection on click            ew-tp-left-show-details-on-click
  selection on double click     ew-tp-left-show-details-on-double-click

Left Relation area
  OK/END            EW-sp-left-OK-button-on-click
  ADD               EW-sp-left-ADD-button-on-click
  EDIT              EW-sp-left-edit-button-on-click
  DELETE            EW-sp-left-delete-button-on-click
  posting relations             ew-sp-left-show-details-on-click
  selection on click            ew-sp-left-show-details-on-double-click
  selection on double click     ewf-sp-get-property-and-value

Left Inverse link area
  OK/EN             EW-il-left-OK-button-on-click
  posting IL                    EW-il-show-details-on-click
  selection on click            EW-il-show-details-on-double-click
  selection on double click     ewf-il-get-property-and-value

Rigth top buttons
  ADD               EW-tp-right-add-button-on-click
  INSERT            EW-tp-right-insert-button-on-click
  DELETE            EW-tp-right-delete-button-on-click
  MODIFY            EW-tp-right-modify-button-on-click
  QUIT              EW-tp-right-cancel-button-on-click
  OK                EW-tp-right-ok-button-on-click
  selection on click (first pane)     EW-edit-prop-value-on-click
  selection on click (second pane)    nothing

Right middle buttons
  ADD               EW-sp-right-ADD-button-on-click
  INSERT            EW-sp-right-INSERT-button-on-click
  CREATE            EW-sp-right-CREATE-button-on-click
  DELETE            EW-sp-right-DELETE-button-on-click
  QUIT              EW-sp-right-QUIT-button-on-click
  OK                EW-sp-right-OK-button-on-click
  CANCEL            EW-sp-right-CANCEL-button-on-click
  CLONE             EW-sp-right-CLONE-buton-on-click
Middle panes
  Concept           editor-class-item-on-change
  Property          editor-property-item-on-change
  Entry             editor-entry-point-item-on-change
  EXAMINE           ew-sp-right-examine-on-click
  Found objects on click         EW-object-list-on-click
  Found objects on double click  EW-object-list-show-object-on-double-click
  Query             ew-query-on-click

Right bottom panes
  properties from application    EW-select-local-property-on-click
  properties from model          EW-select-global-property-on-click

Message area
  printing   nothing special
|#
;;;--------------------------------------------------------- MAKE-EDITOR-WIDGETS

(defUn make-editor-widgets (current-entity old-page)
  "populates the various areas with buttons and panes.
Arguments:
   current-entity: object being edited
   old-page: if nil we have an initial editor
Return:
   nothing meaningful."
  (append
   (make-editor-widgets-for-information-area current-entity old-page)
   (make-editor-widgets-for-tp-left current-entity)
   (make-editor-widgets-for-sp-left current-entity)
   (make-editor-widgets-for-il-left current-entity)
   (make-editor-widgets-for-message-area)
   (make-editor-widgets-for-tp-right)
   (make-editor-widgets-for-sp-right)
   (make-editor-widgets-for-prop-right)
   ))

;;;------------------------------------ MAKE-EDITOR-WIDGETS-FOR-INFORMATION-AREA

;;; information area is the top part of the window and it displays the summary
;;; of the object, its class, the current context
;;; it also has two buttons: abort and commit that can be used to exit from the
;;; edit window
;;; it should display the name and area of the database if one is opened

(defUn make-editor-widgets-for-information-area (current-entity old-page)
  "builds the editor header.
Arguments:
   current-entity: object being edited
   old-page: if nil we have an initial editor
Return:
   list of widgets."
  (list
   ;;
   ;; ================== Information area: Application, Database, Context
   (cr-static-text :width 50 :height 16 :left 19 :top 10 
                   :font +static-text-font-11+ :title "Object")
   (cr-static-text :width 480 :height 24 :left 80 :top 15
                   :font +static-text-font-18+       :name :object-header-item
                   :text (ewf-get-object-name current-entity))
   (cr-static-text :width 50 :height 16 :left 580 :top 10
                   :font +static-text-font-11+ :title "Class")
   (cr-static-text :width 235 :height 24 :left 654 :top 15
                   :font +static-text-font-18+        :name :class-header-item
                   :text (ewf-get-class-name current-entity))
   (cr-static-text :width 56 :height 16 :left 934 :top 10
                   :font +static-text-font-11+ :title "Context")
   (cr-static-text :width 30 :height 24 :left 1009 :top 15
                   :font +static-text-font-18+        :name :context-header-item
                   :text (ewf-get-context))
   ;;=== add abort and commit buttons
   (cr-button :class 'EDITOR-ACTIVE-BUTTON :left 1050 :top 10 :height 16 :width 80 
              :title (if old-page "save" "Commit") :on-click ew-commit-on-click)
   (cr-button :class 'EDITOR-ACTIVE-BUTTON :left 1050 :top 40 :height 16 :width 80 
              :title "Abort" :on-click ew-abort-on-click)
   ))

;;;--------------------------------------------- MAKE-EDITOR-WIDGETS-FOR-TP-LEFT

(defUn make-editor-widgets-for-tp-left (current-entity)
  (list
   ;;
   ;; ================== TP left buttons area
   ;; special KILL button stay activated
   (cr-button :class 'EDITOR-ACTIVE-BUTTON :name :kill-button :title "KILL" 
              :left (%point-x (tp-mlb 1)) :top (%point-y (tp-mlb 0))
              :height (%point-y (tp-lbs)) :width (%point-x (tp-lbs)) 
              :on-click ew-kill-on-click
              :on-double-click ew-kill-on-double-click)
   (cr-button :class 'EDITOR-BUTTON :name :tp-OK-left-button :title "OK / End" 
              :left (%point-x (tp-mlb 1)) :top (%point-y (tp-mlb 1))
              :height (%point-y (tp-lbs)) :width (%point-x (tp-lbs)) 
              :on-click ew-tp-left-ok-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :tp-add-left-button :title "Add"
              :left (%point-x (tp-mlb 2)) :top (%point-y (tp-mlb 2))
              :width (%point-x (tp-lbs)) :height (%point-y (tp-lbs))
              :on-click ew-tp-left-ADD-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :tp-edit-left-button :title "Edit"
              :left (%point-x (tp-mlb 3)) :top (%point-y (tp-mlb 3))
              :width (%point-x (tp-lbs)) :height (%point-y (tp-lbs))
              :on-click ew-tp-left-EDIT-button-on-click)   
   (cr-button :class 'EDITOR-BUTTON :name :tp-delete-left-button :title "Delete"
              :left (%point-x (tp-mlb 4)) :top (%point-y (tp-mlb 4))
              :width (%point-x (tp-lbs)) :height (%point-y (tp-lbs))
              :on-click ew-tp-left-DELETE-button-on-click)   
   ;; --- tp display area
   (cr-static-text :left (%point-x (tp-dap)) :top (- (%point-y (tp-dap)) 16)
                   :width *tp-edit-area-width* :height 16
                   :font +static-text-font-11-it+
                   :text "Attributes")
   (cr-multi-item-list :class 'EDITOR-LEFT-AREA-FOR-VALUES :name :tp-item
                       :title "List of terminal properties and values"
                       :left (%point-x (tp-dap)) :top (%point-y (tp-dap))
                       :width (%point-x (tp-das)) :height (%point-y (tp-das))
                       :vscrollp t
                       :on-click ew-tp-left-show-details-on-click
                       :on-double-click EW-TP-LEFT-SHOW-DETAILS-ON-DOUBLE-CLICK
                       :sequence (ewf-tp-get-property-and-value current-entity)
                       )
   ))

;;;--------------------------------------------- MAKE-EDITOR-WIDGETS-FOR-SP-LEFT

(defUn make-editor-widgets-for-sp-left (current-entity)
  ;;
  ;; ================== SP left buttons area
  (list 
   (cr-button :class 'EDITOR-BUTTON :name :SP-OK-LEFT-BUTTON :title "OK / End"
              :left (%point-x (sp-mlb 1)) :top (%point-y (sp-mlb 1))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-left-OK-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-add-left-button :title "Add"
              :left (%point-x (sp-mlb 2)) :top (%point-y (sp-mlb 2))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-left-ADD-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-edit-left-button :title "Edit"
              :left (%point-x (sp-mlb 3)) :top (%point-y (sp-mlb 3))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-left-EDIT-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-delete-left-button :title "Delete"
              :left (%point-x (sp-mlb 4)) :top (%point-y (sp-mlb 4))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-left-DELETE-button-on-click)     
   ;; ================== SP display area
   (cr-static-text :left (%point-x (sp-dap)) :top (- (%point-y (sp-dap)) 16)
                   :width *SP-EDIT-AREA-WIDTH* :height 16
                   :text "Relations" 
                   :font +static-text-font-11-it+)
   (cr-multi-item-list :class 'EDITOR-LEFT-AREA-FOR-VALUES :name :sp-item
                       :title "List of structural properties and values"
                       :left (%point-x (sp-dap)) :top (%point-y (sp-dap))
                       :width (%point-x (sp-das)) :height (%point-y (sp-das))
                       :on-click ew-sp-left-show-details-on-click
                       :on-double-click ew-sp-left-show-details-on-double-click
                       :vscrollp t
                       :sequence (ewf-sp-get-property-and-value current-entity))
   ))

;;;--------------------------------------------- MAKE-EDITOR-WIDGETS-FOR-IL-LEFT

(defUn make-editor-widgets-for-il-left (current-entity)
  ;;
  ;; ================== IL left button area
  (list 
   (cr-button :class 'EDITOR-BUTTON :name :il-left-ok-button :title "OK / End"
              :left (%point-x (il-mlb 1)) :top (%point-y (il-mlb 1))
              :width (%point-x (il-lbs)) :height (%point-y (il-lbs))
              :on-click EW-il-left-OK-button-on-click)
   ;; ================== IL display area
   (cr-static-text :left (%point-x (il-dap)) :top (- (%point-y (il-dap)) 16)
                   :width *IL-EDIT-AREA-WIDTH* :height 16
                   :font +static-text-font-11-it+
                   :text "Inverse-links" )
   (cr-multi-item-list :class 'EDITOR-LEFT-AREA-FOR-VALUES :name :il-item
                       :title "List of inverse links and related objects"
                       :left (%point-x (il-dap)) :top (%point-y (il-dap))
                       :width (%point-x (il-das)) :height (%point-y (il-das))
                       :on-click EW-il-show-details-on-click
                       :on-double-click EW-il-show-details-on-double-click
                       :vscrollp t
                       :sequence (ewf-il-get-property-and-value current-entity))
   ))

;;;---------------------------------------- MAKE-EDITOR-WIDGETS-FOR-MESSAGE-AREA

(defUn make-editor-widgets-for-message-area ()
  ;;
  ;; ========== Message area
  (list 
   (cr-static-text :left (%point-x (msg-dap)) :top (- (%point-y (msg-dap)) 16) 
                   :width *message-area-width* :height 16
                   :font +static-text-font-11-it+
                   :text "Message Area")
   (cr-text-item :class 'EDITOR-AREA-FOR-TEXT
                 :left (%point-x (msg-dap)) :top (%point-y (msg-dap))
                 :width (%point-x (msg-das)) :height (%point-y (msg-das))
                 :name :message-item
                 :text "Welcome to the MOSS Editor...")
   ))

;;;-------------------------------------------- MAKE-EDITOR-WIDGETS-FOR-TP-RIGHT

(defUn make-editor-widgets-for-tp-right ()
  ;;
  ;; ================== TP right buttons area
  (list 
   (cr-button :class 'EDITOR-BUTTON :name :tp-add-right-button :title "Add"
              :left (%point-x (tp-mrb 0)) :top (%point-y (tp-mrb 0))
              :width (%point-x (tp-lbs)) :height (%point-y (tp-lbs))
              :on-click EW-tp-right-add-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :tp-insert-right-button :title "Insert"
              :left (%point-x (tp-mrb 1)) :top (%point-y (tp-mrb 1))
              :width (%point-x (tp-lbs)) :height (%point-y (tp-lbs))    
              :on-click EW-tp-right-insert-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :tp-modify-right-button :title "Modify"
              :left (%point-x (tp-mrb 2)) :top (%point-y (tp-mrb 2))
              :width (%point-x (tp-lbs)) :height (%point-y (tp-lbs))    
              :on-click EW-tp-right-modify-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :tp-delete-right-button :title "Delete"
              :left (%point-x (tp-mrb 3)) :top (%point-y (tp-mrb 3))
              :width (%point-x (tp-lbs)) :height (%point-y (tp-lbs))
              :on-click EW-tp-right-delete-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :tp-quit-right-button :title "Quit"
              :left (%point-x (tp-mrb 4)) :top (%point-y (tp-mrb 4))
              :width (%point-x (tp-lbs)) :height (%point-y (tp-lbs))
              :on-click EW-tp-right-quit-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :tp-ok-right-button :title "OK"
              :left (%point-x (tp-mrb 5)) :top (%point-y (tp-mrb 5))
              :width (%point-x (tp-lbs)) :height (%point-y (tp-lbs))
              :on-click EW-tp-right-ok-button-on-click)
   
   ;; === Area to display detail values
   (cr-static-text :class 'EDITOR-VALUE-AREA-TITLE-ITEM
                   :left (%point-x (tp-vap)) :top (- (%point-y (tp-vap)) 16)
                   :width *tp-edit-area-width* :height 16
                   :text "" 
                   :name :value-area-title
                   :font +static-text-font-10-b+)
   (cr-multi-item-list :class 'EDITOR-VALUE-AREA-ITEM :name :value-area
                       :left (%point-x (tp-vap)) :top (%point-y (tp-vap))
                       :width (%point-x (tp-vas)) :height (%point-y (tp-vas))
                       :title "List of structural properties and values"
                       :on-click EW-edit-prop-value-on-click
                       :vscrollp t
                       :sequence ())
   ;; ===
   (cr-static-text :left (%point-x (tp-wap)) :top (- (%point-y (tp-wap)) 16)
                   :width (%point-x (tp-was)) :height 16
                   :text "Area to Edit Values" 
                   :font +static-text-font-11-it+)
   (cr-text-item :class 'EDITOR-TP-WORK-ITEM :name :tp-work-item
                 :left (%point-x (tp-wap)) :top (%point-y (tp-wap))
                 :width (%point-x (tp-was)) :height (%point-y (tp-was)) 
                 :text "<pane to edit terminal property values>")
   ))

;;;-------------------------------------------- MAKE-EDITOR-WIDGETS-FOR-SP-RIGHT

(defUn make-editor-widgets-for-sp-right ()
  ;;
  ;; ================== SP right buttons area
  (list 
   (cr-button :class 'EDITOR-BUTTON :name :sp-add-right-button :title "Add"
              :left (%point-x (sp-mrb 1)) :top (%point-y (sp-mrb 1))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-right-ADD-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-insert-right-button :title "Insert"
              :left (%point-x (sp-mrb 2)) :top (%point-y (sp-mrb 2))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-right-INSERT-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-delete-right-button :title "Delete"
              :left (%point-x (sp-mrb 3)) :top (%point-y (sp-mrb 3))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-right-DELETE-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-create-right-button :title "Create"
              :left (%point-x (sp-mrb 4)) :top (%point-y (sp-mrb 4))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-right-CREATE-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-quit-right-button :title "Quit"
              :left (%point-x (sp-mrb 5)) :top (%point-y (sp-mrb 5))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-right-QUIT-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-OK-right-button :title  "OK"
              :left (%point-x (sp-mrb 6)) :top (%point-y (sp-mrb 6))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-right-OK-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-cancel-right-button :title "Cancel"
              :left (%point-x (sp-mrb 7)) :top (%point-y (sp-mrb 7))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-right-CANCEL-button-on-click)
   (cr-button :class 'EDITOR-BUTTON :name :sp-clone-right-button :title "Clone"
              :left (%point-x (sp-mrb 8)) :top (%point-y (sp-mrb 8))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-right-CLONE-button-on-click)
   ;; special button to kill one of the objects of the found-object-list
   (cr-button :class 'EDITOR-BUTTON :name :sp-kill-right-button :title "Kill"
              :left (%point-x (sp-mrb 10)) :top (%point-y (sp-mrb 10))
              :width (%point-x (sp-lbs)) :height (%point-y (sp-lbs))
              :on-click EW-sp-right-KILL-button-on-click)
   
   ;;
   ;; === SP search area (used to locate neighbors)
   (cr-static-text :left (%point-x (sp-w-text-pos 1)) 
                   :top (%point-y (sp-w-text-pos 1))
                   :width 55 :height 16
                   :text "Concept")
   (cr-text-item :class 'EDITOR-CLASS-ITEM :name :class-item
                 :left (+ (%point-x (sp-w-text-pos 1)) 60) 
                 :top (%point-y (sp-w-text-pos 1))
                 :width 130 :height 20
                 :text "" :delayed nil
                 :on-change editor-class-item-on-change)
   (cr-static-text :left (%point-x (sp-w-text-pos 2)) 
                   :top (%point-y (sp-w-text-pos 2))
                   :width 55 :height 16
                   :text "Property")
   (cr-text-item :class 'EDITOR-PROPERTY-ITEM :name :property-item
                 :left (+ (%point-x (sp-w-text-pos 2)) 60) 
                 :top (%point-y (sp-w-text-pos 2))
                 :width 130 :height 20
                 :text "" :delayed nil
                 :on-change editor-property-item-on-change)
   (cr-static-text :left (%point-x (sp-w-text-pos 3)) 
                   :top (%point-y (sp-w-text-pos 3))
                   :width 55 :height 16
                   :text "Entry")
   (cr-text-item :class 'EDITOR-ENTRY-POINT-ITEM :name :entry-point-item
                 :left (+ (%point-x (sp-w-text-pos 3)) 60) 
                 :top (%point-y (sp-w-text-pos 3))
                 :width 130 :height 20
                 :text "" :delayed nil
                 :on-change editor-entry-point-item-on-change)
   ;---
   (cr-button :class 'EDITOR-BUTTON :name :examine-button :title "Examine"
              :left (+ (%point-x (sp-w-text-pos 4)) 120) 
              :top (%point-y (sp-w-text-pos 4))
              :width 70 :height 16
              :on-click ew-sp-right-examine-on-click)
   
   ;; number showing how many objects have been found on entry point
   (cr-text-item :class 'EDITOR-COUNTER-ITEM :name :counter-item :text "0"
                 :left (+ (%point-x (sp-w-text-pos 4)) 50) 
                 :top (%point-y (sp-w-text-pos 4))
                 :width 53 :height 20
                 :font +static-text-font-18+)
   
   ;; === display detailed information (found objects)
   (cr-static-text :left (%point-x (sp-wd)) :top (- (%point-y (sp-wd)) 16)
                   :width 300 :height 16
                   :text "List of Found Objects" 
                   :font +static-text-font-10-it+ )
   
   (cr-multi-item-list :class 'EDITOR-OBJECT-LIST :name :object-list-item
                       :left (%point-x (sp-wd)) :top (%point-y (sp-wd))
                       :width 300 :height (- *sp-edit-area-height* 50)
                       :on-click EW-object-list-on-click
                       :on-double-click EW-object-list-show-object-on-double-click
                       :selection-type :disjoint  ; allow multiple selections
                       :selected-values nil
                       :vscrollp t
                       :sequence ())
   ;; special area to input a query
   (cr-static-text :left (%point-x (sp-w-text-pos 5)) 
                   :top (- (%point-y (sp-w-text-pos 5)) 16)
                   :width 45 :height 16
                   :text "Query"
                   :font +static-text-font-10-it+)
   (make-instance 'EDITOR-QUERY-ITEM
     :left (%point-x (sp-w-text-pos 5)) :top (%point-y (sp-w-text-pos 5))
     :width *EDITOR-RIGHT-WIDTH* 
     :height (- (+ *SP-EDIT-AREA-HEIGHT* (%point-y (sp-wd)))
                (%point-y (sp-w-text-pos 5)))
     :value ""
     :on-click 'ewf-query-on-click
     :name :query-item)
   (make-instance 'EDITOR-GAUGE-ITEM 
     :FONT NIL 
     :HEIGHT 8 
     :LEFT (%point-x (sp-w-text-pos 5)) 
     :TOP (+ *SP-EDIT-AREA-HEIGHT* (%point-y (sp-wd)))
     :WIDTH *EDITOR-RIGHT-WIDTH*
     :NAME :GAUGE-item
     :RANGE '(0 100) 
     :VALUE 0)
   ))

;;;------------------------------------------ MAKE-EDITOR-WIDGETS-FOR-PROP-RIGHT

(defUn make-editor-widgets-for-prop-right ()
  ;;
  ;; ========== Area for displaying available properties to select      
  (list 
   (cr-static-text :left (%point-x (sel-aep)) :top (- (%point-y (sel-aep)) 16)
                   :width (%point-x (sel-das)) :height 16
                   :text "Properties Obtained from Model" 
                   :font +static-text-font-10-it+)
   (cr-multi-item-list :class 'EDITOR-AREA-FOR-VALUES
                       :left (%point-x (sel-aep)) :top (%point-y (sel-aep))
                       :width (%point-x (sel-das)) :height (%point-y (sel-das))
                       :selected-values nil
                       :on-click EW-select-local-property-on-click
                       :name :local-property-item
                       :vscrollp t
                       :sequence ())
   (cr-static-text :left (%point-x (sel-agp)) :top (- (%point-y (sel-agp)) 16)
                   :width (%point-x (sel-das)) :height 16
                   :text "Properties Available in Current Application"
                   :font +static-text-font-10-it+)
   (cr-multi-item-list :class 'EDITOR-AREA-FOR-VALUES
                       :left (%point-x (sel-agp)) :top (%point-y (sel-agp))
                       :width (%point-x (sel-das)) :height (%point-y (sel-das))
                       :selected-values nil
                       :on-click EW-select-global-property-on-click
                       :name :global-property-item
                       :vscrollp t
                       :sequence ())
   ))


;**********
; (setq ww (make-editor-window '$ENT))
;(make-editor-window '$PNAM :old-page ww)
;**********
;;;==================================================================================
;;;
;;;                                  HANDLERS
;;;
;;;==================================================================================

;;;------------------------------------------------- EDITOR-CLASS-ITEM-ON-CHANGE

(defUn editor-class-item-on-change (item text old-value)
  "Called whenever the content of class item (a string) changes.
   When the string is a class name, we change color and beep."
  (declare (ignore old-value))
  (let ((win (%container item)) class-id)
    
    (ewformat t "~&;=== editor-class-item-on-change /New-class: ~A" text)
    
    ;; must look for objects in the right package (e.g. agent)
    (with-package (current-package win)
      (cond
       ;; special case when the text area becomes empty after erasure
       ((equal text ""))
       
       ;; if text corresponds to a class, show it by using red color
       ((setq class-id (%%get-id text :class))
        (%beep)
        (%set-text-color item *red-color*)
        ;; record class
        (setf (current-ep-class win) class-id)
        ;; call extract only when we have a bona fide class
        (ewf-extract-objects win)
        )
       (t (%set-text-color item *black-color*)
          (%set-item-sequence (%find-named-item :object-list-item win) nil)
          ;; reset class
          (setf (current-ep-class win) nil)
          ;; reset list
          (setf (object-list (%find-named-item :object-list-item win)) nil)
          ;; and counter
          (%set-item-text (%find-named-item :counter-item win) "")
          )
       ))
    
    ;; return t to accept the change
    t))

;;;------------------------------------------- EDITOR-ENTRY-POINT-ITEM-ON-CHANGE

(defUn editor-entry-point-item-on-change (item text old-value)
  "Called whenever the content of entry-point item (a string) changes.
   We check whether it is an entry-point."
  (declare (ignore old-value))
  (let ((win (%container item)))
    ;(format t "~%; editor-entry-point-item-on-change /*package*: ~S" *package*)
    ;(format t "~%; editor-entry-point-item-on-change /current-package: ~S" 
    ;  (current-package win))
    (with-package (current-package win)
      ;; get current text-string removing trailing spaces
      (setq text  (string-trim '(#\space) text))
      (ewformat t "~&;=== editor-entry-point-item-on-change /Entry value: ~S" text)
      (setf (current-ep-string win) text)
      ;; try to find object corresponding to entry point
      (ewf-extract-objects win)
      ;; if some objects are found we are happy
      (cond 
       ((current-ep-object-list win)
        (%beep)
        (%set-text-color item *red-color*))
       (t (%set-text-color item *black-color*)))
      )
    ;; return t to accept the change
    t))

;;;---------------------------------------------- EDITOR-PROPERTY-ITEM-ON-CHANGE

(defUn editor-property-item-on-change (item text old-value)
  "Try to interpret input string as a property name."
  (declare (ignore old-value))
  (let ((win (%container item)))
    
    (ewformat t "~&;=== editor-property-item-on-change /Property name: ~A" text)
    
    ;; must look for objects in the right package (e.g. agent)
    (with-package (current-package win)
      (cond
       ;; special case if text is empty call
       ((equal text ""))
       
       ;; check if we can get a property id from text
       ((%%get-id text :attribute)
        ;; yes set red color
        (%set-text-color item *red-color*)
        ;; call extract when we have a bona fide attribute
        (ewf-extract-objects win)
        )
       
       ;; no, set black color
       (t (%set-text-color item *black-color*))
       )
      
      ;; always call extract
      ;(ewf-extract-objects win)
      )
    ;; return t to take the change into account
    t))

;;;======================== Actions associated with buttons ====================

;;;=============================================================================
;;;
;;;                         ABORT and COMMIT BUTTONS
;;;
;;;=============================================================================

;;;----------------------------------------------------------- EW-ABORT-ON-CLICK
;;; subsequent editors have modified objects. Thus, we must undo all the changes 
;;; before actually aborting the current one

(defUn ew-abort-on-click (current-page item)
  "called when clicking the ABORT button. When sub-editing, forget changes and ~
   restore previous page. When toplevel editor, clean up editing box."
  (declare (ignore item))
  (let ((next-editor (next-editor current-page))
        editor-chain)
    
    (print `("W-editor/ew-abort-on-click/ *package*" ,*package*))
    (print `("W-editor/ew-abort-on-click/ next-editor" ,next-editor))
    
    (loop 
      ;; if no more editor get out of loop
      (unless next-editor (return))
      (push next-editor editor-chain)
      (setq next-editor (next-editor next-editor)))
    
    ;(format t "~%; ew-abort-on-click /editor-chain:~%  ~S" editor-chain)
    
    ;; if there are editors following the current one, abort them
    (if editor-chain
      (dolist (editor (reverse editor-chain))
        (ew-abort-proceed editor)))
    
    ;; then abort, returns :done
    (ew-abort-proceed current-page)
    ))

;;;------------------------------------------------------------ EW-ABORT-PROCEED

(defUn ew-abort-proceed (current-page)
  "Doing the actual abort. When sub-editing, forget changes and ~
   restore previous page. When toplevel editor, clean up editing box."
  (let ((previous-page (previous-editor current-page))
        (box (editing-box current-page))
        gate)
    
    (unless box
      ;; close editor anyway
      (%window-close current-page)
      (error "no editing-box attached to editor: ~S" current-page))
    
    ;(ewformat t "~%; ew-abort-proceed /aborting, box:~%  ~S" box)
    
    ;; make all newly created object-ids unbound
    ;(ewformat t "~%; ew-abort-proceed /aborting, new-object-ids:~%  ~S" 
    ;          (new-object-ids box))
    (mapc #'(lambda (xx) (makunbound xx))
          (new-object-ids box))
    ;; restore all old values to previous state
    (mapc  #'(lambda (xx) (set (car xx)(cdr xx)))
           (old-object-values box))
    
    ;; close detail-window if still open
    (if (detail-window current-page)
      (%window-close (detail-window current-page)))
    ;; close editor
    (%window-close current-page)
    ;; make application pointer point to previous page: NIL if top level editor
    (set (intern "*EDITOR-WINDOW*" (current-package current-page)) previous-page) 
    ;; kill pointer to editing box
    (set (intern "*EDITING-BOX*") nil)
    
    (print `("W-editor/ew-abort-proceed/ *package*" ,*package*))
    
    ;; if not nil, then update forward link
    (cond
     (previous-page 
      (setf (next-editor previous-page) nil)
      ;; revive pointer to previous page box
      (set (intern "*EDITING-BOX*") (editing-box previous-page))

      ;; reactivate some of te sp right buttons
      (ewf-buttons-enable-only previous-page '(:sp-DELETE-right-button
                                               :sp-ADD-right-button
                                               :sp-INSERT-right-button
                                               :sp-CREATE-right-button                                                     
                                               :sp-CLONE-right-button                                                     
                                               :sp-QUIT-right-button
                                               :sp-KILL-right-button
                                               ))
      )
     ;; if no previous page, check if a closed gate has been declared
     (t
      (setq gate (gate current-page))
      (if gate (mp:open-gate gate))
      )
     )

    :done))

;;;---------------------------------------------------------- EW-COMMIT-ON-CLICK
;;; 3 cases
;;;   - dialog has no predecessor nor successor
;;;   - dialog has no successor
;;;   - dialog has successors that must be aborted

(defUn ew-commit-on-click (current-page item)
  "called when clicking the COMMIT button. When sub-editing, move the new ids ~
   and saved values to the previous page. When toplevel editor, if no database ~
   opened, remove tags and kill lists, if database present, must save objects.
   If agent is owner, must save into the agent database."
  (declare (ignore item))
  (let ((next-editor (next-editor current-page))
        editor-chain)
    (loop 
      ;; if no more editor get out of loop
      (unless next-editor 
        (return))
      (push next-editor editor-chain)
      (setq next-editor (next-editor next-editor)))
    
    (ewformat t "~%; ew-commit-on-click /editor-chain:~%  ~S" editor-chain)
    
    ;; if there are editors following the current one, abort them
    (if editor-chain
      (dolist (editor (reverse editor-chain))
        (ew-abort-proceed editor)))
    
    ;; then commit, returns :done
    (ew-commit-proceed current-page)
    ))

;;;----------------------------------------------------------- EW-COMMIT-PROCEED

(defUn ew-commit-proceed (current-page)
  "does the actual commit. When sub-editing, move the new ids ~
   and saved values to the previous page. When toplevel editor, if no database ~
   opened, remove tags and kill lists, if database present, must save objects.
   If agent is owner, must save into the agent database."
  (let ((previous-page (previous-editor current-page))
        gate area-key previous-editing-box transaction)
    
    (with-editor-environment current-page
      
      ;; area name is keyword corresponding to active package
      ;; it is not necessary to track the owner...
      (setq area-key (intern (package-name *package*) :keyword))
      
      ;;=== handle toplevel editing
      (unless previous-page   ; no previous page, we terminate editing
        
        ;; editing in the OMAS environment (owner is agent)
        (when (database current-page)
          ;;== use database pointer recorded in the editor page
          (ewformat t "~%; ew-commit-proceed /area-key: ~S~% database: ~S"
                    area-key (database current-page))
          
          ;;=== start transaction
          (setq transaction (db-start-transaction))
          ;; save new objects
          (mapc 
           #'(lambda (xx) (db-store xx (symbol-value xx) area-key :no-commit t))
           (new-object-ids (editing-box current-page)))
          ;;
          (mapc #'(lambda (xx) 
                    (db-store (car xx) (symbol-value (car xx)) area-key
                              :no-commit t))
                (old-object-values (editing-box current-page)))
          ;;=== end transaction
          (db-commit-transaction transaction)
          )
        
        ;;== when no database, nothing special to do other than closing windows
        
        ;; close: editing box will be garbage collected
        ;(setf (editing-box current-page) nil)
        ;; reset application variable that points onto the current editor
        (set (intern "*EDITOR-WINDOW*") nil)
        (set (intern "*EDITING-BOX*") nil)
        ;; must recover a closed gate and if there, open it
        (setq gate (gate current-page))
        ;; can open it only if it exists
        (if gate (mp:open-gate gate))
        )
      
      ;;=== handle sub-editing
      (when previous-page
        (setq previous-editing-box (editing-box previous-page))
        ;; pass the current object to the previous editor (in case we were creating a
        ;; a new object (could be used to refresh the list of found objects...)
        (setf (new-object previous-page) (current-entity current-page))
        ;; add list of newly created objects to the previous list
        (setf (new-object-ids previous-editing-box)
              (append (new-object-ids previous-editing-box)
                      (new-object-ids (editing-box current-page))))
        ;; insert only non shared values, keeping the shared ids to their original 
        ;; value
        (dolist (pair (old-object-values (editing-box current-page)))
          (pushnew pair (old-object-values previous-editing-box) :key #'car))
        
        ;(setf (editing-box current-page) nil) ; not very useful ?
        (%window-close current-page)
        (sleep .1) ; maybe that could help against random crashes ???
        ;; should maybe activate previous page by selecting it (?)
        (set (intern "*EDITOR-WINDOW*") previous-page)
        (setf (next-editor previous-page) nil)
        (set (intern "*EDITING-BOX*") (editing-box previous-page))
        
        ;; when we return to previous page, if we were adding a new object 
        ;; created instance or cloned object, we must update the sp edit area
        ;; This is the case if the new-object slot was filled
        (when (new-object previous-page)
          (let* ((sp-locate-area (%find-named-item :object-list-item previous-page))
                 (object-list (object-list sp-locate-area)))
            ;; add new object to old-list
            (setq object-list (append object-list (list (new-object previous-page))))
            ;; refresh LFO and counter
            (ew-object-list-refresh previous-page object-list)
            ;; select new object
            (%select-cell-at-index sp-locate-area (1- (length object-list)))
            (ewf-adjust-scroll sp-locate-area (1- (length object-list)))
            ;; erase new-object
            (setf (new-object previous-page) nil)
            ))
        ;; reactivate some of the sp right buttons
        (ewf-buttons-enable-only previous-page '(:sp-DELETE-right-button
                                                 :sp-ADD-right-button
                                                 :sp-INSERT-right-button
                                                 :sp-CREATE-right-button                                                     
                                                 :sp-CLONE-right-button                                                     
                                                 :sp-QUIT-right-button
                                                 :sp-KILL-right-button
                                                 ))
        )
      
      ;; close detail-window if still open
      (if (detail-window current-page)
        (%window-close (detail-window current-page)))
      (%window-close current-page)
      
      :done)))

;;;------------------------------------------------------------ EW-KILL-ON-CLICK

(defun ew-kill-on-click (current-page item)
  "to kill an object the user must double-click the button."
  (declare (ignore item))
  (%beep)
  (ewf-post-message "If you want to delete the object you are editing,
then double-click the KILL button." current-page)
  t)

;;;----------------------------------------------------- EW-KILL-ON-DOUBLE-CLICK

(defun ew-kill-on-double-click (current-page item)
  "to kill an object the user must click the button a second time."
  (declare (ignore item))
  (let ((obj-id (current-entity current-page)))
    (if obj-id
        (progn
          (send obj-id '=delete)
          ;; repaint the window
          (initialize-editor-window current-page obj-id)
          )
      ;; otherwise complain
      (progn
        (ewf-post-message "can't find the entity to kill..." current-page)
        (%beep)
        )
      )
    )
  t)

;;;=============================================================================
;;;
;;;                               TP LEFT BUTTONS
;;;
;;;=============================================================================

;;;---------------------------------------------- EW-TP-LEFT-ADD-BUTTON-ON-CLICK

(defUn ew-tp-left-add-button-on-click (current-page item)
  "Here we clicked on the ADD button, wanting to add a new property ~
   to the list of terminal properties. ~
   MOSS is going to propose properties obtained from the class if the object ~
   has a class, as well as complementary properties that exist in the application."
  (let*
    ((current-entity (current-entity current-page))
     (tp-item (%find-named-item :tp-item current-page))
     tp-list tp-class-list global-tp-list object-tp-list
     local-tp-list)
    (ewformat t "~&;=== EW-tp-left-ADD-button-on-click." )
    
    ;; close any opened detail window
    (ew-detail-kill-window current-page)
    
    (with-editor-environment current-page
      
      ;; indicate first that we are editing a TP
      (setf (current-property-type current-page) :tp)
      ;; and the type of action
      (setf (current-property-action current-page) :add)
      ;; unselect all items in the attribute list
      (ewf-deselect-area tp-item)
      
      ;; obtain the list of all attributes that exist in the system
      (setq tp-list (%get-value (symbol-value (intern "*MOSS-SYSTEM*")) '$ETLS))
      ;; considering current-entity as an instance, get attributes from the class
      (setq tp-class-list (delete-if-not #'%is-terminal-property?
                                         (=> current-entity '=get-properties)))
      ;; get local properties, i.e., those of the object
      (setq object-tp-list 
            (%%has-terminal-properties current-entity 
                                       (symbol-value (intern "*CONTEXT*"))))
      
      ;; activate selection area
      (ewf-activate-property-selection-area current-page) 
      
      ;; now display in the right part system properties that are not class attributes
      ;; or attributes the object already has
      (setq global-tp-list (set-difference tp-list (union tp-class-list object-tp-list)))
      ;; order the list by increasing alphabetical order
      (setq global-tp-list 
            (sort global-tp-list #'string-lessp 
                  ;:key #'(lambda (xx) (car (send xx '=get-name)))));JPB060219
                  :key #'(lambda (xx) (car (send xx '=instance-name :class t)))))
      (%set-item-sequence (%find-named-item :global-property-item  current-page) 
                          (mapcar #'(lambda (xx) (car (send xx '=summary)))
                                  global-tp-list))
      ;; record the list for future use (?)
      (setf (tp-list (%find-named-item :global-property-item current-page)) 
            global-tp-list)
      
      ;; ... on the left part we show only properties that are specified by the model
      ;; and are not local to the object, ordered alphabetically
      (setq local-tp-list (set-difference tp-class-list object-tp-list))
      (setq local-tp-list
            (sort local-tp-list #'string-lessp 
                  :key #'(lambda (xx) (car (send xx '=instance-name :class t)))))
      (%set-item-sequence (%find-named-item :local-property-item  current-page)
                          (mapcar #'(lambda (xx) (car (send xx '=summary)))
                                  local-tp-list))
      ;; record the list for future use (?)
      (setf (tp-list (%find-named-item :local-property-item current-page)) local-tp-list)
      item)))

;;;------------------------------------------- EW-TP-LEFT-DELETE-BUTTON-ON-CLICK

(defUn ew-tp-left-delete-button-on-click (current-page item)
  "Deletes a TP and all associated values of an entity"
  (declare (ignore item))
  
  ;; close any opened detail window
  (ew-detail-kill-window current-page)
  
  (unless (car (%get-selected-cells-indexes (%find-named-item :tp-item current-page)))
    (%beep)
    (ewf-post-message "Please first select a property to delete." current-page)
    (return-from ew-tp-left-delete-button-on-click nil))
  
  (with-editor-environment current-page
    
    ;; call a function to tell MOSS to do the job
    (ewf-tp-delete-property (current-entity current-page)
                            (current-property current-page))
    
    ;; reset the various page slots associated witht the attribute
    (setf (current-property current-page) nil)
    (setf (current-value-list current-page) nil)
    
    ;; update the tp-item area
    (%set-item-sequence (%find-named-item :tp-item current-page)
                        (ewf-tp-get-property-and-value 
                         (current-entity current-page)))))

;;;+------------------------------------------------- EW-TP-LEFT-EDIT-BUTTON-ON-CLICK

(defUn ew-tp-left-edit-button-on-click (current-page item)
  "Edit the content of a property of the TP property list."
  (let*
    ((value-area (%find-named-item :value-area current-page))
     (pos (car (%get-selected-cells-indexes (%find-named-item :tp-item current-page))))
     (value-list (current-value-list current-page)))
    
    ;; close any opened detail window
    (ew-detail-kill-window current-page)
    
    (unless pos
      (%beep)
      (ewf-post-message "Please first select a property to edit." current-page)
      (return-from ew-tp-left-edit-button-on-click nil))
    
    (with-editor-environment current-page
      
      ;; indicate first that we are editing a TP (can't happen)
      (setf (current-property-type current-page) :tp)
      (ewformat t "~&;ew-tp-left-edit-button-on-click; prop-id: ~S value-list ~S" 
                (current-property current-page) value-list)
      
      ;; activate edit area (careful: wipes out title)
      (ewf-activate-edit-tp-area current-page)
      ;; post the property name
      (ewf-edit-tp-title-post current-page 
                              (car (send (current-property current-page) '=get-name)))
      ;; display values in the detail value area when there is one (always)
      (when value-list
        (%set-item-sequence value-area value-list :raw)) ; JPB1304
      ;; default is add
      (setf (current-action current-page) :add)
      
      ;; enable right edit buttons, except OK
      (ewf-buttons-enable-only 
       current-page (remove :tp-ok-right-button *ew-tp-right-button-list*))
      
      ;; warn user
      (ewf-post-message "You can ADD a new-value or SELECT an existing one."
                        current-page)
      item)))

;;;----------------------------------------------- EW-TP-LEFT-OK-BUTTON-ON-CLICK

(defUn ew-tp-left-ok-button-on-click (current-page item)
  "End of TP editing action. We enable sp and il areas"
  (let* ((tp-item (%find-named-item :tp-item current-page)))
    (ewformat t "~&;=== ew-tp-left-ok-button-on-click." )
    ;; close any opened detail window
    (ew-detail-kill-window current-page)

    ;; we reset current-property-type slot to nil
    (setf (current-property-type current-page) nil)
    ;; reset property action and position
    (setf (current-property-action current-page) nil)
    ;; ... and reset current-property and current-property-position
    ;; we reset current-property-list
    (setf (current-property current-page) nil)
    (setf (current-property-position current-page) nil)
    (setf (current-property-list current-page) nil)

    ;; we editor-area-list-deselect any selected line
    (mapc #'(lambda (xx) (%unselect-cell-at-index tp-item xx))
          (%get-selected-cells-indexes tp-item))
    (ewf-deactivate-property-selection-area current-page)

    ;; we reenable sp and il areas
    (ewf-activate-left-sp-area current-page)
    (ewf-activate-left-il-area current-page)

    ;; disable all buttons
    (ewf-buttons-enable-only current-page ())
    item))

;;;-------------------------------------------- EW-TP-LEFT-SHOW-DETAILS-ON-CLICK

(defUn ew-tp-left-show-details-on-click (current-page item)
  "Called when a line of the terminal property display area has been clicked upon. 
   On a simple click cleans the message area; on a double click~
   if  more then 1 value, then a local window appears displaying the values one ~
   per line.
   when the command key is pressed, displays the entry-points
Arguments:
   item: table displaying all tp names and values.
   current-page: the editor window (calue of local *editor-window*"
  (let* ((current-entity (current-entity current-page))
         prop-id value-list)
    
    ;; close any opened detail window
    (ew-detail-kill-window current-page)
    (sleep 0.05) ; ACL
    (ewformat t "~&;=== ew-tp-left-show-details-on-click; ")
    
    ;; deselect everything but TP area
    (ewf-deactivate-all-areas current-page)
    (ewf-activate-left-tp-area current-page)
    ;; editor-area-list-deselect all buttons except for the add, edit, and delete 
    ;; tp left buttons
    (ewf-buttons-enable-only current-page 
                             '(:tp-OK-left-button :tp-Add-left-button
                               :tp-EDIT-left-button :tp-Delete-left-button))
    
    (with-editor-environment current-page

      ;; update editor object description in header if needed
      (%set-item-text (%find-named-item :object-header-item current-page)
                      (ewf-get-object-name (current-entity current-page)))
      
      ;; if nothing has been selected, then quit
      (unless (%get-selected-cells-indexes item) 
        (ewf-buttons-enable-only current-page 
                                 '(:tp-OK-left-button :tp-Add-left-button))
        (return-from ew-tp-left-show-details-on-click nil))
      
      ;; if something is selected, then record property
      (setq prop-id (nth (car (%get-selected-cells-indexes item))
                         (%%has-terminal-properties
                          current-entity 
                          (symbol-value (intern "*CONTEXT*"))))
            value-list (%get-value current-entity prop-id))
      ;; first deselect any selected item in the sp and il tables
      (ewf-deselect-area-list current-page :sp-item :il-item)
      ;; record the selected property and value-list into the corresponding page slots
      (setf (current-property current-page) prop-id)
      (setf (current-value-list current-page) value-list)
      (setf (current-value current-page) nil)
      (setf (current-property-type current-page) :tp)
      item)))


;;;------------------------------------- EW-TP-LEFT-SHOW-DETAILS-ON-DOUBLE-CLICK

(defUn ew-tp-left-show-details-on-double-click (current-page item)
  "Called when a line of the terminal property display area has been clicked upon. 
   If  more then 1 value, then a local window appears displaying the values one per line.
   when the command key is pressed, displays the entry-points.
Arguments:
   item: table displaying all tp names and values."
  (let* ((cell-size (%get-cell-size item))
         (context (symbol-value (intern "*CONTEXT*")))
         prop-id value-list left top
         (selected-cell-index (car (%get-selected-cells-indexes item)))
         (current-entity (current-entity current-page)))
    (sleep 0.05)
    (ewformat t "~&;=== ew-tp-left-show-details-on-double-click; ")   
 
    ;; if nothing is selected, forget it
    (unless selected-cell-index
      (ewf-post-message "No selected line." current-page)
      (%beep)
      (return-from ew-tp-left-show-details-on-double-click nil))

    (with-editor-environment current-page
      
      ;; if something is selected, then record property and values
      (setq prop-id 
            (nth (car (%get-selected-cells-indexes item))
                 (moss::%%has-terminal-properties 
                  current-entity context))
            value-list (moss::%get-value current-entity prop-id))
      
      ;; compute position of the new upper left corner of floating window
      (setq left (+ (%left current-page)(%left item)
                    (* 2 (%point-y cell-size))) ; move a little bit right
            top (+ (%top current-page) (%top item)          
                   (+ #-MCL 24 ; editor header ?
                      (* (%point-y cell-size)  ; tp-item cell height
                         (1+ selected-cell-index)))))
      (ewformat t "~&;... /prop-id: ~S ~&; value-list: ~S" prop-id value-list)
      (cond
       ((not value-list)) ; do something only if some value is present

       ;; when shift-key is pressed, then displays entry points
       ((cg:key-is-down-p cg:vk-shift) ; if there we want entry-points    
        (let ((method (if *lexicographic-inheritance*
                        (lex-get-method prop-id '=make-entry context)
                        (get-method prop-id '=make-entry)))
              ep-list)
          (ewformat t "~&;... /method: ~S" method)

          (when method
            ;; if there, produce an entry point list
            ;(setq prop-id (nth (%get-cell-index-from-value item (car (%get-selected-cells-indexes item)))
            (setq ep-list  
                  (mapcar #'(lambda (xx)(apply method xx nil)) value-list))
            (ewformat t "~&;... /ep-list: ~S" ep-list)
            ;; and display it
            ;(enable-view-area item :show-area ep-list :ep))))
            (ewf-tp-display-details current-page ep-list left top (%width item)))

          (unless method
            ;; send a message to tell that we don't have entry-points
            (ewf-post-message "No entry point method for this property" current-page)
            (%beep))
          ))

       ;; otherwise, show details on double-click only if more than 1 value
       ((cdr value-list) 
        ;; display each value separately
        (ewf-tp-display-details current-page value-list left top (%width item)))
       )

      item)))

;;;=============================================================================
;;;
;;;                               SP LEFT BUTTONS
;;;
;;;=============================================================================

;;;---------------------------------------------- EW-SP-LEFT-ADD-BUTTON-ON-CLICK

(defUn ew-sp-left-add-button-on-click (current-page item)
  "Here we clicked on the ADD button, wanting to add a new property ~
   to the list of structural properties. ~
   MOSS is going to propose properties obtained from the class, if the object ~
   has a class, or SP from the system otherwise."
  (let*
    ((current-entity (current-entity current-page))
     (value-area (%find-named-item :value-area current-page))
     (sp-item (%find-named-item :sp-item current-page))
     sp-list sp-class-list object-sp-list global-sp-list
     local-sp-list)
    
    (with-editor-environment current-page
      
      (ewformat t "~&;=== EW-sp-left-ADD-button-on-click." )
      ;; close any opened detail window
      (ew-detail-kill-window current-page)
      
      ;; indicate first that we are editing an SP
      (setf (current-property-type current-page) :sp)
      ;; and the type of action
      (setf (current-property-action current-page) :add)
      ;; unselect items
      (ewf-deselect-area sp-item)
      ;; clean value area
      (%set-item-sequence value-area nil)
      
      ;; obtain the list of values from the application
      (setq sp-list (%get-value (symbol-value (intern "*MOSS-SYSTEM*")) '$ESLS))
      ;; when object is an instance get relations from the class
      (setq sp-class-list (delete-if-not #'%is-relation?
                                         (send current-entity '=get-properties)))
      ;; get local properties
      (setq object-sp-list 
            (%%has-structural-properties current-entity 
                                         (symbol-value (intern "*CONTEXT*"))))
      
      ;; activate selecton area
      (ewf-activate-property-selection-area current-page) 
      ;; now display in the right part system properties that are not class relations
      ;; or relations the object already has
      (setq global-sp-list 
            (set-difference sp-list (union sp-class-list object-sp-list)))
      ;; order the list by increasing alphabetical order
      (setq global-sp-list
            (sort global-sp-list #'string-lessp 
                  :key #'(lambda (xx) (car (send xx '=summary)))))
      (%set-item-sequence (%find-named-item :global-property-item  current-page) 
                          (mapcar #'(lambda (xx) (car (send xx '=summary)))
                                  global-sp-list))
      ;; record the list for future use
      (setf (sp-list (%find-named-item :global-property-item current-page)) 
            global-sp-list)
      
      ;; on the left part we show only properties that are specified by the model
      ;; and are not local to the object
      (setq local-sp-list (set-difference sp-class-list object-sp-list))
      ;; should order them alphabetically
      (setq local-sp-list
            (sort local-sp-list #'string-lessp 
                  :key #'(lambda (xx) (car (send xx '=summary)))))
      (%set-item-sequence (%find-named-item :local-property-item  current-page)
                          (mapcar #'(lambda (xx) (car (send xx '=summary)))
                                  local-sp-list))
      ;; record the list for future use
      (setf (sp-list (%find-named-item :local-property-item current-page))
            local-sp-list)
      item)))

;;;------------------------------------------- EW-SP-LEFT-DELETE-BUTTON-ON-CLICK

(defUn ew-sp-left-delete-button-on-click (current-page item)
  "Deletes an SP and all associated links to neighbors."
  (declare (ignore item))
  (ewformat t "~&;=== ew-sp-left-delete-button-on-click")
  
  ;; close any opened detail window
  (ew-detail-kill-window current-page)
  
  (with-editor-environment current-page
    
    ;; call a function to tell MOSS to do the job
    (ewf-sp-delete-property (current-entity current-page)
                            (current-property current-page))
    ;; reset the various page slots
    (setf (current-property current-page) nil)
    (setf (current-value-list current-page) nil)
    ;; update the tp-item area
    (%set-item-sequence (%find-named-item :sp-item current-page)
                        (ewf-sp-get-property-and-value 
                         (current-entity current-page)))))

;;;--------------------------------------------- EW-SP-LEFT-EDIT-BUTTON-ON-CLICK

(defUn ew-sp-left-edit-button-on-click (current-page item)
  "Edit the content of a property of the SP property list."
  (declare (ignore item))
  (let
    ((value-area (%find-named-item :value-area current-page))
     (value-list (current-value-list current-page)))
    (ewformat t "~&;=== ew-sp-left-edit-button-on-click") 
    ;; close any opened detail window
    (ew-detail-kill-window current-page)
    
    (unless value-list
      (%beep)
      (ewf-post-message "Please first select a property to edit." current-page)
      (return-from ew-sp-left-edit-button-on-click nil))
    
    ;; indicate first that we are editing a SP
    (setf (current-property-type current-page) :sp)
    ;; enable adequate buttons
    (ewf-buttons-enable-only current-page 
                             '(:sp-quit-right-button :examine-button))
    (ewformat t "~&;... prop-id: ~S~%;... value-list: ~S" 
              (current-property current-page) value-list)
    
    (with-editor-environment current-page
      
      (ewf-activate-edit-sp-area current-page)
      
      ;; display values in the detail value area
      (when value-list
        ;; null value, then do nothing, should never occur
        ;; otherwise, then display them in the value area table
        (setf (object-list value-area) value-list)
        (ewf-edit-tp-title-post current-page 
                                (car (send (current-property current-page) 
                                           '=get-name)))
        (%set-item-sequence value-area (ewf-get-summaries value-list))
        )
      )))

;;;----------------------------------------------- EW-SP-LEFT-OK-BUTTON-ON-CLICK

(defUn ew-sp-left-ok-button-on-click (current-page item)
  "End of SP editing action. We enable tp buttons."
  (let* ((sp-item (%find-named-item :sp-item current-page)))
    (ewformat t "~&;=== EW-sp-left-OK-button-on-click. " )
    ;; close any opened detail window
    (ew-detail-kill-window current-page)
    
    ;; we reset current-property-type slot to nil
    (setf (current-property-type current-page) nil)
    ;; reset property action and position
    (setf (current-property-action current-page) nil)
    ;; we editor-area-list-deselect any selected line
    (mapc #'(lambda (xx) (%unselect-cell-at-index sp-item xx))
          (%get-selected-cells-indexes sp-item))
    
    ;; ... and reset current-property and current-property-position
    ;; we reset current-property-list
    (setf (current-property current-page) nil)
    (setf (current-property-position current-page) nil)
    (setf (current-property-list current-page) nil)
    
    (ewf-deactivate-all-areas current-page)    
    ;; we reenable tp and il area
    (ewf-activate-left-side current-page)
    (ewf-deactivate-message current-page)
    
    ;; we disable all buttons
    (ewf-buttons-enable-only current-page ())
    item))

;;;-------------------------------------------- EW-SP-LEFT-SHOW-DETAILS-ON-CLICK

(defUn ew-sp-left-show-details-on-click (current-page item)
  "Called when a line of the structural property display area has been clicked upon. 
   Sets up information for further processing."
  (let ((current-entity (current-entity current-page))
        prop-id value-list)
    (sleep 0.05) ; useful for ACL
    (ewformat t "~&;=== ew-sp-left-show-details-on-click; ")
    
    ;; deselect anything but SP area
    (ewf-deactivate-all-areas current-page)
    (ewf-activate-left-sp-area current-page)
    ;; indicate that we are working with sp
    (setf (current-property-type current-page) :sp)
    
    ;; if nothing has been selected, then beep and quit
    (unless (%get-selected-cells-indexes item) 
      ;; reset internal variables
      (setf (current-property current-page) nil)
      (setf (current-value-list current-page) nil)
      (setf (current-value current-page) nil)
      (ewf-buttons-enable-only current-page 
                               '(:sp-OK-left-button :sp-Add-left-button))
      (return-from ew-sp-left-show-details-on-click nil))
    
    (with-editor-environment current-page      
      
      ;; when a cell is selected, get property and list of values from selected cell
      (setq prop-id (nth (car (%get-selected-cells-indexes item))
                         (%%has-structural-properties 
                          current-entity 
                          (symbol-value (intern "*CONTEXT*"))))
            value-list (%get-value current-entity prop-id))
      ;; first deselect any selected item in the tp and il tables
      (ewf-deselect-area-list current-page :tp-item :il-item)
      ;; record the selected sp and value-list into the corresponding page slots
      (setf (current-property current-page) prop-id)
      (setf (current-value-list current-page) value-list)
      (setf (current-value current-page) nil)
      
      ;; deselect all buttons except for the OK, add, edit, and delete
      (ewf-buttons-enable-only current-page 
                               '(:sp-OK-left-button 
                                 :sp-Add-left-button
                                 :sp-EDIT-left-button
                                 :sp-DELETE-left-button))
      item)))

;;;------------------------------------- EW-SP-LEFT-SHOW-DETAILS-ON-DOUBLE-CLICK

(defUn ew-sp-left-show-details-on-double-click (current-page item)
  "Called when a line of the structural property display area has been ~
   double-clicked.
   Opens a detail window displaying the list of values."
  (let* ((cell-size (%get-cell-size item))
         prop-id value-list left top
         (selected-cell-index (car (%get-selected-cells-indexes item)))
         (current-entity (current-entity current-page)))
    (sleep 0.05)
    ;; if nothing is selected, forget it
    (unless selected-cell-index
      (%beep)
      (return-from ew-sp-left-show-details-on-double-click nil))
    
    (with-editor-environment current-page
      
      ;; if something is selected, then record property and values
      (setq prop-id 
            (nth (car (%get-selected-cells-indexes item))
                 (moss::%%has-structural-properties 
                  current-entity 
                  (symbol-value (intern "*CONTEXT*"))))
            value-list (moss::%get-value current-entity prop-id))
      ;; compute position of the new upper left corner of floating window
      (setq left (+ (%left current-page)(%left item)(* 2 (%point-y cell-size)))
            top (+ (%top current-page) (%top item) 
                   (+ #-MCL 24 (* (%point-y cell-size)
                                  (1+ selected-cell-index)))))
      (ewformat t "~&;=== ew-sp-left-show-details-on-double-click /prop-id: ~S ~&~
                   ; value-list: ~S" prop-id value-list)
      (cond
       ((not value-list)) ; do something only if some value is present
       ;; otherwise, show details on double-click only if more than 1 value
       (value-list 
        ;; display each value separately
        (let (desc-list)
          ;; if there produce a summary of entity
          (setq desc-list  
                (reduce #'append (mapcar #'(lambda (xx)(send xx '=summary))
                                         value-list)))
          (ewformat t "~&;... /desc-list:~% ~S" 
                    desc-list)
          ;; and display it
          (ewf-sp-display-details 
           current-page desc-list left top (%width item) value-list))
        ))
      item))
  )

;;;=============================================================================
;;;
;;;                         LEFT INVERSE LINKS
;;;
;;;=============================================================================

;;;----------------------------------------------- EW-IL-LEFT-OK-BUTTON-ON-CLICK

(defUn ew-il-left-ok-button-on-click (current-page item)
  "End of IL editing action. We enable all areas buttons."
  (let* ((il-item (%find-named-item :il-item current-page)))
    (ewformat t "~&;=== EW-il-left-OK-button-on-click" )
    ;; we editor-area-list-deselect any selected line
    (mapc #'(lambda (xx)(%unselect-cell-at-index il-item xx))
          (%get-selected-cells-indexes il-item))
    ;; we reenable tp and sp area
    (ewf-activate-left-side current-page)
    ;; we disable all buttons
    (ewf-buttons-enable-only current-page ())
    item))

;;;------------------------------------------------- EW-IL-SHOW-DETAILS-ON-CLICK

(defUn ew-il-show-details-on-click (current-page item)
  "Called when a line of the inverse property display area has been clicked upon. 
   Sets up information for further processing."
  (let* ((current-entity (current-entity current-page))
         (selected-cell-indexes (%get-selected-cells-indexes item))
         prop-id value-list)
    (sleep 0.05) ; wait for selection to take effect
    (ewformat t "~&;=== ew-il-show-details-on-click-0; ")
    
    ;; deselect anything but IL area
    (ewf-deactivate-all-areas current-page)
    (ewf-activate-left-il-area current-page)
    ;; deselect all buttons except for OK
    (ewf-buttons-enable-only current-page '(:il-left-OK-button))
    ;; if nothing has been selected, just quit
    (unless selected-cell-indexes 
      (return-from ew-il-show-details-on-click nil))
    
    (with-editor-environment current-page
      
      (setq prop-id (nth (car selected-cell-indexes)
                         (%%has-inverse-properties 
                          current-entity 
                          (symbol-value (intern "*CONTEXT*"))))
            value-list (%get-value current-entity prop-id))
      ;; first editor-area-list-deselect any selected item in the tp and il tables
      ;(ewf-area-list-deselect current-page :tp-item :sp-item)
      ;; clean message area
      (ewf-clear-message current-page)
      ;; initialize value-list (used for navigation)
      (setf (current-value-list current-page) value-list)   
      item)))

;;;------------------------------------------ EW-IL-SHOW-DETAILS-ON-DOUBLE-CLICK

(defUn ew-il-show-details-on-double-click (current-page item)
  "Called when a line of the inverse property display area has been double-clicked. 
   Displays details on multiple predecessors."
  (let* ((current-entity (current-entity current-page))
         (selected-cell-index (car (%get-selected-cells-indexes item)))
         (cell-size (%get-cell-size item))
         prop-id value-list left top)
    (sleep 0.05)
    
    ;; if nothing has been selected, then beep and quit
    (unless selected-cell-index 
      (%beep)
      (return-from ew-il-show-details-on-double-click nil))
    
    (with-editor-environment current-page
      
      ;; if something is selected, then record property and values
      (setq prop-id (nth selected-cell-index 
                         (moss::%%has-inverse-properties 
                          current-entity 
                          (symbol-value (intern "*CONTEXT*"))))
            value-list (moss::%get-value current-entity prop-id))
      
      ;; compute position of the new upper left corner of floating window    
      (setq left (+ (%left current-page)(%left item)(* 2 (%point-y cell-size)))
            top (+ (%top current-page) (%top item) 
                   (+ 24 (* (%point-y cell-size)
                            (- selected-cell-index -1
                               (or (car (%get-selected-cells-indexes item)) 0))))))
      (ewformat t "~&;=== ew-il-left-show-details-on-double-click /prop-id: ~S ~&~
                   ; value-list ~S" prop-id value-list)
      
      (when value-list  ; if no value, do nothing
        (let (desc-list)
          ;; if there produce a summary of entity
          (setq desc-list  
                (reduce #'append (mapcar #'(lambda (xx)(send xx '=summary))
                                         value-list)))
          (ewformat t "~&;... desc-list: ~S" desc-list)
          ;; and display it
          (ewf-sp-display-details 
           current-page desc-list left top (%width item) value-list)))
      )
    item))

;;;============================================================================
;;;
;;;                              MESSAGE AREA
;;;
;;;============================================================================

;;;---------------------------------------------------------- ewf-clear-message

(defUn ewf-clear-message (win)
  "clean the message area"
  (%set-item-text (%find-named-item :message-item win) "")
  t)

;;;------------------------------------------------------------ ewf-post-message

(defUn ewf-post-message (message win)
  "post a message into the message window, eviving it if needed."
  (let ((item (%find-named-item :message-item win)))
    (activate item)
    (%set-item-text item message)
    ;; tell user that a message was posted
    (%beep)
    t))

;(ewf-post-message "some test" *editor-window*)

;;;=============================================================================
;;;=============================================================================

;;;=============================================================================
;;;
;;;                               TP RIGHT BUTTONS
;;;
;;;=============================================================================

;;;--------------------------------------------- EW-TP-RIGHT-ADD-BUTTON-ON-CLICK

(defUn ew-tp-right-add-button-on-click (current-page item)
  "Add a new value to the (possible empty) list of values."
  ;; value-area is the top right pane in which we list values
  ;; tp-work-item is the second pane in which we type new data
  (let* ((tp-work-item (%find-named-item :tp-work-item current-page))
         (value-area (%find-named-item :value-area current-page))
         (value (%get-item-value tp-work-item))
         value-list)
    ;; we always add at the end of the list
    
    (ewf-clear-message current-page)
    ;; however, if empty (only white spaces) send warning
    (when (string-equal (string-trim '(#\space) value) "")
      (ewf-post-message "Warning: you just added an empty value." current-page)
      (%beep))    ;; first clean the edit area
    
    ;; read taking care of MLN and numbers
    (setq value (ew-tp-right-format-value value current-page))
    (print `(value ,value))
  
    (with-editor-environment current-page
      (ewformat t "~&;=== ew-tp-right-ok-button-on-click/ adding ~
                   value: ~S~%; (if \"\" could be a mistake)" value)    
      ;; update object and left display, and return augmented value-list
      (setq value-list
            (ewf-tp-insert-nth-value (current-entity current-page)
                                     value
                                     (current-property current-page)
                                     nil))
      ;; record the new value list
      (setf (current-value-list current-page) value-list)
      ;; redisplay list of values
      (%set-item-sequence value-area value-list :raw) ; JPB1304
      
      ;; update the list of terminal properties displayed on the left
      (%set-item-sequence 
       (%find-named-item :tp-item current-page)
       (ewf-tp-get-property-and-value (current-entity current-page)))
      
      ;; record data for later processing
      (setf (current-pos current-page) (1- (length value-list)))
      (setf (current-value current-page) value) 
      
      ;; unselect all values of the list
      (%unselect-all value-area)
      ;; select cell in the list
      (%select-cell-at-index value-area (current-pos current-page))
      
      ;; scroll cell up to view last addition (does not work)
      (ewf-adjust-scroll value-area (1- (length (%get-item-sequence value-area))))
      ;; put mouse into work-area
      (%set-focus-item tp-work-item)
      ;; keep old value selected
      (%select-all tp-work-item)
      
      ;; activate the buttons
      (ewf-buttons-enable-only current-page 
                               '(:tp-quit-right-button
                                 :tp-add-right-button
                                 :tp-insert-right-button
                                 :tp-modify-right-button
                                 :tp-delete-right-button))
      item)))


;;;------------------------------------------- EW-TP-RIGHT-DELETE-BUTTON-ON-CLICK

(defUn ew-tp-right-delete-button-on-click (current-page item)
  "Delete the selected value. Removes it from the value-list. Reset current-value ~
   current-value-list, updates the detailed display area. Do not change the ~
   setting of the left buttons. The following value if any becomes the selected ~
   value."
  (let* ((value-area (%find-named-item :value-area current-page))
         (value-list (current-value-list current-page))
         (value-position (current-pos current-page))
         )
    (setf (current-action current-page) :delete)
    (ewformat t "~&;ew-tp-right-delete-button-on-click.")
    
    (ewf-clear-message current-page)
    
    ;; when nil, nothing selected
    (unless value-position
      (ewf-post-message "No value selected." current-page)
      (beep)
      (return-from ew-tp-right-delete-button-on-click nil))
    
    ;; make sure the internal variables are set
    (unless (ewf-edit-tp-area-set-position-and-selection current-page)
      (ewf-post-message "No value selected." current-page)
      (beep)
      (return-from ew-tp-right-delete-button-on-click nil))
    
    (with-editor-environment current-page
      
      ;; delete value from the list, return the updated value-list
      (setq value-list
            (ewf-tp-delete-value (current-entity current-page)
                                 (current-value current-page)
                                 (current-property current-page)
                                 value-position))
      ;; update page slots
      (setf (current-value-list current-page) value-list)
      ;; update the list of terminal properties displayed on the left
      (%set-item-sequence (%find-named-item :tp-item current-page)
                          (ewf-tp-get-property-and-value 
                           (current-entity current-page)))
      
      ;; setting directly to new value does not seem to work. Reset first to nil
      (%set-item-sequence value-area nil)
      (sleep 0.1)
      (%set-item-sequence value-area value-list :raw) ; JPB1304
      
      ;; when nothing left, we switch to adding values
      (unless value-list
        (%beep)
        (setf (current-action current-page) :add)
        ;; post mouse into the work area
        (%set-focus-item (%find-named-item :tp-work-item current-page))
        ;; post message
        (ewf-post-message "You can only add values now, or quit." current-page)
        ;; activate the buttons
        (ewf-buttons-enable-only current-page 
                               '(:tp-quit-right-button
                                 :tp-add-right-button))

        ;; and quit
        (return-from ew-tp-right-delete-button-on-click t))
      
      ;; select next candidate to a "chain" delete
      (unless (nth value-position value-list)
        ;; when value list shrank push the pointer back up
        (setq value-position (1- (length value-list))))
      ;; select the value
      (%select-cell-at-index value-area value-position)
      ;; record it
      (setf (current-value current-page) (nth value-position value-list))
      (setf (current-pos current-page) value-position)
      ;; it is visible (i.e., bring it into the area pane)
      (ewf-adjust-scroll value-area value-position)
      item)))

;;;---------------------------------------------------- EW-TP-RIGHT-FORMAT-VALUE

(defun ew-tp-right-format-value (val current-page &aux new-val)
  "takes a string coming from one of the panes and checks what to do. If the ~
   string starts with a caret ^ tries to do a read-from-string. In case of error ~
   bips and returns the original string.
Arguments:
   val: string to process
   current-page: edit window
Return:
   processed value."
  ;; if val is not a string, do nothing
  (unless (stringp val)
    (return-from ew-tp-right-format-value val))
  
  ;; trim val
  (setq val (string-trim '(#\space) val))
  ;; if string empty return it
  (if (string-equal val "") (return-from ew-tp-right-format-value val))
  
  ;; if string start with caret, try to read the value from the string
  (when (char-equal (char val 0) #\^)
    ;; first read no error
    (setq new-val 
          (handler-case 
              (read-from-string (subseq val 1)) 
            (error () nil)))
    ;; if nil, means bad format
    (unless new-val
      ;; send message and return old value
      (ewf-post-message "Warning: Bad value format." current-page)
      (%beep)
      (return-from ew-tp-right-format-value val))
  
    ;; otherwise return processed value
    (return-from ew-tp-right-format-value new-val)
    )
  
  ;; otherwise try to read content checking for number or mln...
  (setq new-val (handler-case (read-from-string val) (error () nil)))
  ;; if number or mln return it
  (if (or (numberp new-val)
          (mln::mln? new-val)) ; jpb 1406
      ;(list new-val)
      new-val
    ;; otherwise return string
    val))
  

#|
(ew-tp-right-format-value "   salut les copains..." nil)
"salut les copains..."
(ew-tp-right-format-value
 " ^ ((:fr \"Hi\" \"guys\") (:fr \"salut les copains...\"))" nil)
((:FR "Hi" "guys") (:FR "salut les copains..."))
(ew-tp-right-format-value 
 "((:fr \"Hi\" \"guys\") (:fr \"salut les copains...\"))" nil)
((:FR "Hi" "guys") (:FR "salut les copains..."))
(ew-tp-right-format-value "^123" nil)
123
(ew-tp-right-format-value "-123" nil)
-123
(ew-tp-right-format-value "^ " nil)
<Warning: Bad value format.>
NIL
(ew-tp-right-format-value "^   salut les copains..." *editor-window*)
SALUT
(ew-tp-right-format-value "^  ( salut les copains..." *editor-window*)
<beep>+<message>
"^  ( salut les copains..."
|#
;;;------------------------------------------ EW-TP-RIGHT-INSERT-BUTTON-ON-CLICK

(defUn ew-tp-right-insert-button-on-click (current-page item)
  "Insert a new value in front of the current-value in the list of values. ~
   Thus enables the tp-work-area, positioning the key-handler over it. OK ~
   and Cancel rigth buttons are activated. Action is recorded as :insert ~
   at page level."
  (let* ((tp-work-item (%find-named-item :tp-work-item current-page))
         (value (%get-item-value tp-work-item))
         (current-pos (current-pos current-page))
         (value-area (%find-named-item :value-area current-page))
         value-list)
    (ewformat t "~&;=== ew-tp-right-insert-button-on-click /prop-id: ~S"
              (current-property current-page))

    (ewf-clear-message current-page)
    
    ;; however, when value is empty give a warning
    (when (string-equal (string-trim '(#\space) value) "")
      (ewf-post-message "Warning: you just added an empty value." current-page)
      (%beep))
    
    ;; process input value to check for numbers or mlns
    (setq value (ew-tp-right-format-value value current-page))
    
    (with-editor-environment current-page
      
      ;; no need to scroll cell since new value replaces a visible one
      ;; when we are inserting a new value, the old value should stay selected
      ;; update object and left display
      (setq value-list
            (ewf-tp-insert-nth-value (current-entity current-page)
                                     value
                                     (current-property current-page)
                                     current-pos))
      (ewformat t "~&;...inserting value: ~S at pos ~S" value current-pos)
      
      ;; redisplay TP table
      (%set-item-sequence value-area value-list :raw) ; JPB1304
      
      ;; record everything
      (setf (current-value-list current-page) value-list)
      (setf (current-pos current-page) (1+ current-pos))
      ;; update the list of terminal properties displayed on the left
      (%set-item-sequence 
       (%find-named-item :tp-item current-page)
       (ewf-tp-get-property-and-value (current-entity current-page)))
      
      ;; value does not change (selected value before which we insert)
      ;(%set-item-value value-area (current-value current-page))
      ;; move selection so that it stays on the previous selected value
      (%unselect-all value-area)
      (%select-cell-at-index value-area (1+ current-pos))
      ;; scroll so that we see added value and selected value
      (ewf-adjust-scroll value-area (1+ current-pos))
      ;; keep old value selected in scratch pad area
      (%select-all tp-work-item)
      ;; set cursor into work area
      (%set-focus-item (%find-named-item :tp-work-item current-page))
      item)))

;;;------------------------------------------ EW-TP-RIGHT-MODIFY-BUTTON-ON-CLICK

(defUn ew-tp-right-modify-button-on-click (current-page item)
  "Modify the selected current-value in the list of values. ~
   Thus enables the tp-work-area, positioning the key-handler over it. OK ~
   and CANCEL right buttons are activated. Action is recorded as :insert ~
   at page level."
  (let* ((value-area (%find-named-item :value-area current-page))
         (tp-work-item (%find-named-item :tp-work-item current-page))
         (current-position (current-pos current-page))
         )
    ;; make sure the internal variables are set
    (unless current-position
      (%beep)
      (ewf-post-message "No value selected." current-page)
      (return-from ew-tp-right-modify-button-on-click nil))
    
    (with-editor-environment current-page
      
      (setf (current-action current-page) :modify)
      ;; record value to modify
      (setf (current-value current-page)
        (read-from-string
            (nth current-position (%get-item-sequence value-area))))
      (ewformat t "~&;=== ew-tp-right-modify-button-on-click /prop-id: ~S"
                (current-value current-page))
      
      ;; write selected value into the scratch area, do not use raw since the 
      ;; value was obtained from the list of posted values
      (%set-item-text tp-work-item  (current-value current-page))
      ;; dim some buttons
      (ewf-buttons-enable-only current-page 
                               '(:tp-ok-right-button :tp-quit-right-button))
      ;; set focus
      (%set-focus-item tp-work-item)
      item)))

;;;---------------------------------------------- EW-TP-RIGHT-OK-BUTTON-ON-CLICK

(defUn ew-tp-right-ok-button-on-click (current-page item)
  "End of modify interaction (the OK button is only activated for modifications. ~
   We restore the value and reenable editing buttons."
  (let* ((current-action (current-action current-page))
         (value-area (%find-named-item :value-area current-page))
         (tp-item (%find-named-item :tp-item current-page))
         (tp-work-item (%find-named-item :tp-work-item current-page))
         value value-list current-pos)
    
    (with-editor-environment current-page
      
      (case current-action
        (:modify  ; update a specific value deleting the old one
         (ewformat t "~&;=== ew-tp-right-ok-button-on-click; :modify")
         ;; get the value index
         (setq current-pos (current-pos current-page))

         ;; if no value is selected, complain
         (unless current-pos
           (ewf-post-message "Warning: No value was selected." current-page)
           (%beep)
           (return-from ew-tp-right-ok-button-on-click nil))

         ;; get the value from the edit area
         ; (setq value ew-tp-right-format-value (%get-item-value tp-work-item))
         (setq value (%get-item-value tp-work-item))
         
         (setq value (list (ew-tp-right-format-value value current-page)))
         (format t "~%; ew-tp-right-ok-button-on-click /value: ~S" value)
         
         ;; special case when we want to input MLN. First we check value and if it
         ;; is an MLN we remove the surrounding quotes.
         ;; first read no error
         ;(setq val (handler-case (read-from-string value) (error () nil)))
         ;; if non nil, means regular list expr
         ;(when (and val (mln::mln? val)) ; jpb 1406
           ;; in which case we set val to MLN value directly
           ;(setq value (list val)))
         
         ;; replace the old value with the new one in object
         (setq value-list
               (ewf-tp-replace-nth-value (current-entity current-page)
                                         (current-value current-page)
                                         value
                                         (current-property current-page)
                                         current-pos))
         ;; redisplay the TP table with the modified value
         (%set-item-sequence tp-item
                             (ewf-tp-get-property-and-value 
                              (current-entity current-page)))
         ;; find out which value it was in the value list (current-pos)
         (ewformat t "~&;... value-list: ~S" 
                   value-list)
         ;(list-widget-replace-item value-area value current-pos)
         (%set-item-sequence value-area nil)
         (sleep 0.05)
         (%set-item-sequence value-area value-list :raw)
         ;; keep it selected
         (%set-item-value value-area value)
         ))
      ;; reactivate all buttons
      (ewf-buttons-enable-only current-page '(:tp-add-right-button
                                              :tp-delete-right-button
                                              :tp-insert-right-button 
                                              :tp-modify-right-button
                                              :tp-quit-right-button))
      ;; clean the edit area in all cases
      (%set-item-text tp-work-item "")
      item)))

;;;-------------------------------------------- EW-TP-RIGHT-QUIT-BUTTON-ON-CLICK

(defUn ew-tp-right-quit-button-on-click (current-page item)
  "Cancel edit action. Resets current-value and action to nil. Closes the ~
   edit area. Reopens all left panes."
  ;; reset value and action slots
  (setf (current-value current-page) nil)
  (setf (current-action current-page) nil)
  ;; reset editing area
  (ewf-deactivate-edit-tp-area current-page)
  ;; activate left part
  (ewf-activate-left-side current-page)
  ;; close message area
  (ewf-deactivate-message current-page)
  item)


;;;------------------------------------------------- EW-EDIT-PROP-VALUE-ON-CLICK
;;; this function is common to TP and SP

(defUn ew-edit-prop-value-on-click (current-page item)
  "Function called when a value in the detailed list has been clicked. Item ~
   is the table item that was just clicked. ~
   The options of the edit area must be highlighted. 
   If we are processing a TP, then the value is copied into the edit area.
   If we are processing a SP, then the locate area is highlited, and the buttons ~
   are enabled."
  (let* ((property-type (current-property-type current-page))
         (tp-work-item (%find-named-item :tp-work-item current-page))
         ;(selected-cell (car (%get-selected-cells-indexes item)))
         value value-position 
         )
    (sleep 0.1)
    
    ;; unless a cell is selected, complain
    (unless (%get-selected-cells-indexes item)
      (beep)
      (ewf-post-message "No selected value. You only can add values." current-page)
      
      (setf (current-action current-page) :add)
      (setf (current-position current-page) (length (current-value-list current-page)))
      (case property-type
        (:tp
         ;(ewf-edit-tp-area-open current-page)
         )
        (:sp
         ;; reset search area
         (ewf-buttons-enable-only current-page 
                                  '(:sp-add-right-button 
                                    :sp-create-right-button
                                    :sp-clone-right-button
                                    :sp-quit-right-button))
         (ewf-post-message 
          "You can now only add objects." current-page))
        )
      ;; quit
      (return-from ew-edit-prop-value-on-click nil))
    
    ;;=== when something is selected get its index
    (setq value-position (car (%get-selected-cells-indexes item)))
    (ewf-clear-message current-page)
    (setq value (nth value-position (current-value-list current-page)))
    (ewformat t "~&;EW-edit-prop-value-on-click; Value: ~S; position: ~S" 
              value value-position)
    (case property-type
      (:tp
       ;; record the particular value that has been selected in the value slot
       (setf (current-value current-page) value)
       ;; as well as the position in the list
       (setf (current-pos current-page) value-position)
       
       ;; clear scrachpad area, ready to do something
       (%set-item-value tp-work-item "")
       )
      (:sp
       ;; first, enable the useful buttons
       (ewf-buttons-enable-only current-page 
                                '(:sp-add-right-button 
                                  :sp-insert-right-button :sp-modify-right-button
                                  :sp-clone-right-button
                                  :sp-delete-right-button :sp-cancel-right-button
                                  :sp-create-right-button :sp-quit-right-button))
       ;; record the particular value that has been selected in the value slot
       (setf (current-value current-page) value)
       ;; as well as the position in the list
       (setf (current-pos current-page) value-position)
       ))
    ;; should disable other areas (SP, IL)?
    item))

;;;=============================================================================
;;;
;;;                             SP RIGHT BUTTONS
;;;
;;;=============================================================================

;;;--------------------------------------------- EW-SP-RIGHT-ADD-BUTTON-ON-CLICK

(defUn ew-sp-right-add-button-on-click (current-page item)
  "Add a new value to the (possible empty) list of values."
  (let* ((value-area (%find-named-item :value-area current-page))
         (obj-id (current-entity current-page)) ; object being edited
         values value-list)
    (ewformat t "~&;=== EW-sp-right-ADD-button-on-click /Add selected objects.")
    ;; get a list of selected values from the locate area
    ;; some value is always selected otherwise ADD button is inactive (sure?)
    (setq values (ewf-sp-get-selected-values current-page))
    (ewformat t "~&;= editor-sp-right-ADD-button /Values: ~S to add to: ~S"
              values (current-value current-page))
    (unless values
      (ewf-post-message "No selected value to add." current-page)
      (%beep)
      (return-from ew-sp-right-add-button-on-click nil))
    
    (with-editor-environment current-page
      
      ;; update object values for current property, return the updated value-list
      (setq value-list
            (ewf-sp-insert-nth-value obj-id
                                     values
                                     (current-property current-page)
                                     nil)) ; insert at the end
      ;; record new value list
      (setf (current-value-list current-page) value-list)
      ;; redisplay left SP table
      (%set-item-sequence (%find-named-item :sp-item current-page)
                          (ewf-sp-get-property-and-value 
                           obj-id))
      
      ;; display new value-list in the detail area
      (%set-item-sequence value-area (ewf-get-summaries value-list))
      ;; adjust scroll so that we can see the cell selected
      (ewf-adjust-scroll value-area (1- (length value-list)))
      ;; unselect all to avoid multiple selection
      (%unselect-all value-area)
      ;; select corresponding value
      (%select-cell-at-index value-area (1- (length value-list)))

      ;; record data for later processing
      (setf (current-pos current-page) (1- (length value-list)))
      (setf (current-value current-page) (car (last value-list))) 

      ;; should activate delete, insert, OK buttons...
      (ewf-buttons-enable-only current-page '(:sp-ADD-right-button
                                              :sp-DELETE-right-button
                                              :sp-INSERT-right-button
                                              :sp-cancel-right-button
                                              :sp-create-right-button
                                              :sp-quit-right-button
                                              :sp-CLONE-right-button))
      item)))

;;;------------------------------------------ EW-SP-RIGHT-CANCEL-BUTTON-ON-CLICK

(defUn ew-sp-right-cancel-button-on-click (current-page item)
  "cancels the :create or :clone action. Deselects the list of found objects. ~
   On query set the abort flag."
  (let ((object-list-item (%find-named-item :object-list-item current-page)))
    ;; look first if we wanted to clone something
    ;; clear message if any
    (ewf-clear-message current-page)
    
    (case (current-action current-page) 

      ((:clone :create-instance)
       ;; OK, cancel everything
       (setf (current-action current-page) nil)
       (setf (current-value current-page) nil)
       ;; enable some buttons
       (ewf-buttons-enable-only current-page '(:sp-ADD-right-button
                                               :sp-INSERT-right-button
                                               :sp-cancel-right-button
                                               :sp-delete-right-button
                                               :sp-clone-right-button
                                               :sp-quit-right-button
                                               :sp-create-right-button))
       ;; clean message
       (ewf-clear-message current-page)
       ;; quit
       (return-from ew-sp-right-cancel-button-on-click nil)
       )
      (:query
       ;; on a query must indicate that we want to abort 
       (setf (query-abort-condition current-page) t)
       )
      (:kill
       ;; reset selected object
       (setf (new-object current-page) nil)
       ;; reset action
       (setf (current-action current-page) nil)
       ;; reenable some buttons
       (ewf-buttons-enable-only current-page '(:sp-ADD-right-button
                                               :sp-INSERT-right-button
                                               :sp-CREATE-right-button
                                               :sp-CANCEL-right-button                                      
                                               :sp-CLONE-right-button                                      
                                               :sp-QUIT-right-button))

       )
      )    
    ;; otherwise simply deselect found objects
    (ewf-deselect-area object-list-item)
    item))


;;;------------------------------------------- EW-SP-RIGHT-CLONE-BUTTON-ON-CLICK

(defUn ew-sp-right-clone-button-on-click (current-page item)
  "clones a selected object from the list of found objects. Since it is a heavy ~
   operation, request a confirm from th user (OK)"
  (let (values)
    (ewformat t "~&;=== EW-sp-right-CLONE-button-on-click. Creating a new object.")
    ;; get a list of selected values from the locate area
    (setq values (ewf-sp-get-selected-values current-page))
    
    ;; if more than 1 value complain
    (when (cdr values)
      (ewf-post-message "Too many values selected." current-page)
      (beep)
      (return-from ew-sp-right-clone-button-on-click nil))
    
    ;; if no value, look for the class entry
    (unless values
      (ewf-post-message "No value selected and no class specified." current-page)
      (beep)
      (return-from ew-sp-right-clone-button-on-click nil))
    
    ;; record action as CLONE and class
    (setf (current-action current-page) :clone)
    (setf (current-value current-page) (car values))
    
    (with-editor-environment current-page
      
      ;; highlight the OK and cancel buttons
      (%beep)
      (ewf-post-message 
       (format nil "You want to clone ~S?~%Yes, click OK; no, click CANCEL."
               (car (send (car values) '=summary)))
       current-page)
      (ewf-buttons-enable-only current-page '(:sp-ok-right-button
                                              :sp-cancel-right-button))
      ;;exit (the rest is processed at the OK or CANCEL callbacks)
      (return-from ew-sp-right-clone-button-on-click T))
    item))

;;;------------------------------------------ EW-SP-RIGHT-CREATE-BUTTON-ON-CLICK

(defUn ew-sp-right-create-button-on-click (current-page item)
  "Create action."
  (declare (ignore item))
  (let* (class-id)
    (ewformat t "~&;=== EW-sp-right-CREATE-button-on-click. Creating a new object ~
                 of class ~S." (current-ep-class current-page))
    (ewf-clear-message current-page)
    ;; a class should be posted in the class slot
    (setq class-id (current-ep-class current-page))
    ;; if none, complain
    (unless class-id
      (ewf-post-message "No value selected and no class specified." current-page)
      (beep)
      (return-from ew-sp-right-create-button-on-click nil))
    
    ;; OK, create an instance of the class and call the editor recursively
    ;; record action as CREATE-INSTANCE and class
    (setf (current-action current-page) :create-instance)
    
    (with-editor-environment current-page
      
      ;; highlight the OK and cancel buttons
      (%beep)
      (ewf-post-message 
       (format nil "You want to create an instance of ~S?~%Yes, push OK; no, push ~
                    CANCEL."
               (car (send class-id '=get-name)))
       current-page)
      (ewf-buttons-enable-only current-page '(:sp-ok-right-button
                                              :sp-cancel-right-button))
      
      ;;exit (the rest is processed at the OK or CANCEL callbacks)
      (return-from ew-sp-right-create-button-on-click T))))

;;;------------------------------------------ EW-SP-RIGHT-DELETE-BUTTON-ON-CLICK

(defUn ew-sp-right-delete-button-on-click (current-page item)
  "Delete the selected value. Removes it from the successor-list. Reset current-value ~
   current-value-list, updates the detailed display area. Do not change the ~
   setting of the left buttons. The following value if any becomes the selected ~
   value."
  (let* ((value-area (%find-named-item :value-area current-page))
         (sp-item (%find-named-item :sp-item current-page))
         (value-position (current-pos current-page))
         (value-list (current-value-list current-page))
         )
    (setf (current-action current-page) :delete)
    (ewformat t "~&;=== EW-sp-right-DELETE-button-on-click /position: ~S"
              value-position)
    ;; when value-position is nil nothing is selected
    (unless value-position
      (%beep)
      (return-from ew-sp-right-delete-button-on-click nil))
    
    (with-editor-environment current-page
      
      ;; remove object from the list, return the updated value-list
      (setq value-list
            (ewf-sp-delete-successor (current-entity current-page)
                                     (current-value current-page)
                                     (current-property current-page)
                                     value-position))
      ;; update page slots
      (setf (current-value-list current-page) value-list)
      ;; deselect value ??
      (setf (current-value current-page) nil)
      ;; keep current position
      ;; update the list of structural properties displayed on the left
      (%set-item-sequence sp-item
                          (ewf-sp-get-property-and-value 
                           (current-entity current-page)))
      ;; setting directly to new value does not seem to work. Reset first to nil
      (%set-item-sequence value-area nil)
      (sleep 0.1)
      (%set-item-sequence value-area (ewf-get-summaries value-list))
      
      (ewformat t "~&;... value-list:~% ~S" value-list)
      
      ;; when nothing is left we switch to adding values
      (unless value-list
        (%beep)
        ;; set action to :add, postion and value to nil
        (setf (current-action current-page) :add)
        ;; post mouse in the object-list area (not really usefull)
        ;(%set-focus-item (%find-named-item :object-list-item current-page))
        (setf (current-pos current-page) nil)
        ;; update buttons
        (ewf-buttons-enable-only current-page '(:sp-ADD-right-button
                                                :sp-quit-right-button
                                                :sp-create-right-button))
        ;; post message
        (ewf-post-message "You can only add or create objects now." current-page)
        ;; and quit
        (return-from ew-sp-right-delete-button-on-click t))
      
      ;; otherwise, we can prepare for a possible "chain-delete" with the next value
      ;; when no more, consider previous value
      (unless (nth value-position value-list)
        ;; if we deleted last value, we push the pointer back up 1 position
        (setq value-position (1- (length value-list))))
      ;(ewformat t "~&;=== EW-sp-right-DELETE-button-on-click /value-position: ~S"
      ;          value-position)
      ;; select the value
      (%select-cell-at-index value-area value-position)
      (%set-item-value value-area 
                       ;(list (nth value-position (%get-item-sequence value-area))))
                       (nth value-position (%get-item-sequence value-area)))
      ;(ewformat t "~&;=== EW-sp-right-DELETE-button-on-click /selected object: ~S"
      ;          (nth value-position value-list))
      ;; record it
      (setf (current-value current-page) (nth value-position value-list))
      (setf (current-pos current-page) value-position)
      ;; adjust scroll so that we can see the cell selected
      (ewf-adjust-scroll value-area value-position)
      ;; select corresponding value
      ;(%select-cell-at-index value-area nil value-position)
      item)))


;**********
; problem: table areas take lists as value in ACL not in MCL...
; When adding a relation to an instance, there is the problem of selecting a generic
; relation or not...
;**********



;;;----------------------------------------------- EW-SP-RIGHT-EXAMINE-ON-CLICK

(defUn ew-sp-right-examine-on-click (current-page item)
  "called in two cases: (i) to display neighbors in the List-of-found-objects ~
   pane when they are in large number; (ii) to execute a query typed in the ~
   query pane."
  (declare (ignore item))
  (let ((sp-show-area (%find-named-item :object-list-item current-page))
        (query-area (%find-named-item :query-item current-page))
        text query)
    
    (ewformat t "~&;=== ew-sp-right-examine-on-click")

    ;;=== when the number of objects in the LFO is great and there none are visible
    ;; asks the user if she wants to see them
    (when (and (current-ep-object-list current-page)
               (null (%get-item-sequence sp-show-area)))
      (ewf-post-message "Do you want to see all the objects?" current-page)
      (setf (current-action current-page) :show-neighbors)
      (ewf-buttons-enable-only current-page '(:sp-OK-right-button
                                              :sp-quit-right-button
                                              :sp-CANCEL-right-button))
      (return-from ew-sp-right-examine-on-click t))

    ;;=== otherwise, looks for a query to execute
    (setq text (string-trim '(#\space) (%get-item-text query-area)))
    
    (when (equal text "")
      (%beep)
      (ewf-post-message "No query to execute." current-page)
      (return-from ew-sp-right-examine-on-click nil))
      
    ;; when query, try to execute it
    (with-editor-environment current-page
      
      ;; ckeck lisp syntax first, if nil, failure
      (setq query (handler-case (read-from-string text) (error () nil)))

      (unless query 
        (ewf-post-message "Bad query format. Check parents and quotes."
                          current-page)
        (return-from ew-sp-right-examine-on-click nil))

      ;; good query Lisp syntax, try to access objects
      (ew-process-query query query-area)
      t)))

;;;------------------------------------------ EW-SP-RIGHT-INSERT-BUTTON-ON-CLICK

(defUn ew-sp-right-insert-button-on-click (current-page item)
  "Insert a new successor in front of the current-successor in the list. ~
   Thus enables the sp-locate-area, positioning the key-handler over it. OK ~
   and Cancel rigth buttons are activated. Action is recorded as :insert ~
   at page level."
  (let* ((value-area (%find-named-item :value-area current-page))
         values value-list pos)
    (ewformat t "~&;=== EW-sp-right-INSERT-button-on-click.")
    ;; get a list of selected values from the locate area
    ;; some value is always selected otherwise ADD button is inactive (sure?)
    (setq values (ewf-sp-get-selected-values current-page))
    (unless values
      (ewf-post-message "No selected value to insert." current-page)
      (%beep)
      (return-from ew-sp-right-insert-button-on-click nil))
    
    ;; get the position of the selected value in the value area
    (setq pos (car (%get-selected-cells-indexes value-area)))
    (unless pos
      (ewf-post-message "No selected value to determine where to insert."
                        current-page)
      (return-from ew-sp-right-insert-button-on-click nil))
    
    (with-editor-environment current-page
      
      (setf (current-pos current-page) pos)
      ;; update value, return the updated value-list
      (setq value-list
            (ewf-sp-insert-nth-value (current-entity current-page)
                                     values
                                     (current-property current-page)
                                     (1- pos)))
      
      ;; record new value list for object
      (setf (current-value-list current-page) value-list)
      ;; redisplay SP table
      (%set-item-sequence (%find-named-item :sp-item current-page)
                          (ewf-sp-get-property-and-value 
                           (current-entity current-page)))
      ;; display new value-list in the detail area
      (%set-item-sequence value-area (ewf-get-summaries value-list))
      ;; scroll value area
      (ewf-adjust-scroll value-area pos)
      item)))

;;;-------------------------------------------------- EW-SP-RIGHT-KILL-ON-CLICK

(defUn ew-sp-right-kill-button-on-click (current-page item)
  "called ro delete one of the selectid objects of the found objects list."
  (declare (ignore item))
  (let (selected-object-list)
    ;; get selected object
    (setq selected-object-list (ewf-sp-get-selected-values current-page))
    ;; if more than one complain
    (when (cdr selected-object-list)
      (%beep)
      (ewf-post-message "Too many objects selected." current-page)
      (return-from ew-sp-right-kill-button-on-click))
    
    ;; otherwise set action to :kill
    (setf (current-action current-page) :kill)
    ;; save object somewhere
    (setf (new-object current-page) (car selected-object-list))
    ;; activate OK, QUIT and ABORT buttons
    (ewf-buttons-enable-only current-page '(:sp-CANCEL-right-button
                                            :sp-OK-right-button
                                            :sp-QUIT-right-button))
    (%beep)
    (ewf-post-message "Do you really want to kill the selected object?" current-page)
    ;; and quit (OK button will enforce the delete action
    t))

;;;---------------------------------------------- EW-SP-RIGHT-OK-BUTTON-ON-CLICK

(defUn ew-sp-right-ok-button-on-click (current-page item)
  "calling a new editor for creating a new object or cloning an old one"
  (declare (ignore item))
  (let ((current-action (current-action current-page))
        object-list)
    
    (ewformat t "~&;=== EW-sp-right-OK-button-on-click /~%;  action: ~S" 
              current-action)
    
    (with-editor-environment current-page
      
      (case current-action
        
        (:create-instance
         ;; we want to create a new object
         (ewformat t "~%;  class-id: ~S" (current-ep-class current-page))
         
         (edit (current-ep-class current-page)
               :action :create
               :previous-editor current-page
               :current-package (current-package current-page)
               :language (language current-page)
               :context (context current-page)
               :version-graph (version-graph current-page)
               :owner (owner current-page)
               :database (database current-page))
         ;; clean message area
         (ewf-clear-message current-page)
         ;; reset internals
         (setf (current-value current-page) nil)
         (setf (current-action current-page) nil)
         (return-from EW-sp-right-OK-button-on-click t)
         )
        
        (:clone
         ;; we assume that we want to clone object but not to create an orphan
         (edit (current-value current-page)
               :action :clone
               :previous-editor current-page
               :current-package (current-package current-page) ; JPB1304
               :language (language current-page)
               :context (context current-page)
               :version-graph (version-graph current-page)
               :owner (owner current-page)
               :database (database current-page))
         ;; clean message area
         (ewf-clear-message current-page)
         ;; reset internals
         (setf (current-value current-page) nil)
         (setf (current-action current-page) nil)
         (return-from EW-sp-right-OK-button-on-click t)         
         )
        (:kill
         (unless (new-object current-page)
           (%beep)
           (ewf-post-message "No object selected." current-page)
           (return-from ew-sp-right-ok-button-on-click))

         ;; delete selected object
         (send (new-object current-page) '=delete)

         ;; remove object from LFO list
         (setq object-list
               (remove (new-object current-page)
                       (object-list
                        (%find-named-item :object-list-item current-page))))
         ;; update list of posted objects in "list of found objects"
         ;; removing the object from the list of posted objects
         ;; this updates counter
         (ew-object-list-refresh current-page object-list)

         ;; reset recorded list
         (setf (current-ep-object-list current-page) object-list)
         ;; reset action
         (setf (current-action current-page) nil)
         ;; reenable some buttons
         (ewf-buttons-enable-only current-page '(:sp-ADD-right-button
                                            :sp-INSERT-right-button
                                            :sp-CREATE-right-button
                                            :sp-CANCEL-right-button                                      
                                            :sp-CLONE-right-button                                      
                                            :sp-KILL-right-button
                                            :sp-QUIT-right-button))
         )
        (:show-neighbors
         ;; we have many neighbors, did not show them. User wants to see them
         (let ((sp-locate-area (%find-named-item :object-list-item current-page)))
           (%set-item-sequence sp-locate-area 
                               (ewf-get-summaries (object-list sp-locate-area)))
           ;; reenable some buttons
           (ewf-buttons-enable-only current-page '(:sp-ADD-right-button
                                                   :sp-INSERT-right-button
                                                   :sp-CREATE-right-button
                                                   :sp-CANCEL-right-button                                      
                                                   :sp-CLONE-right-button                                      
                                                   :sp-KILL-right-button
                                                   :sp-QUIT-right-button))
           ))
                             
        ))))

;;;-------------------------------------------------- EW-SP-RIGHT-QUIT-ON-CLICK

(defUn ew-sp-right-quit-button-on-click (current-page item)
  "called when wanting to get out."
  ;; reset value and action slots
  (setf (current-value current-page) nil)
  (setf (current-action current-page) nil)
  ;; reset editing area
  (ewf-deactivate-edit-sp-area current-page)
  ;; activate left part
  (ewf-activate-left-side current-page)
  ;; close message area
  (ewf-deactivate-message current-page)
  item)

;;;=============================================================================
;;;
;;;                               OBJECT LIST AREA
;;;
;;;=============================================================================
#|
;;;-------------------------------------------------- EW-DISPLAY-OBJECT-ON-CLICK
;;;********** can't use *current-object-list*


(defUn ew-display-object-on-click (item)
  "Called when the user clicks on one of the object in the display list. ~
   The function must first recover the correponding obj-id, then create a ~
   new page for displaying it"
  ;; first recover the obj-id
  (when (double-click-p)
    (make-browser-window
     (nth (cell-to-index item (car (selected-cells item))) *current-object-list*))))
|#


;;;----------------------------------------------------- EW-OBJECT-LIST-ON-CLICK

(defUn ew-object-list-on-click (current-page item)
  "when user clicks onto a value to select it, enable add button and clone"
  (declare (ignore item))
  (ewf-buttons-enable-only current-page '(:sp-create-right-button
                                          :sp-insert-right-button
                                          :sp-cancel-right-button
                                          :sp-add-right-button 
                                          :sp-quit-right-button 
                                          :sp-clone-right-button 
                                          :sp-kill-right-button
                                          ))
  )

;;;---------------------------------- EW-OBJECT-LIST-SHOW-OBJECT-ON-DOUBLE-CLICK

(defUn ew-object-list-show-object-on-double-click (current-page item)
  "Called when the user double clicks one of the object in the display list. ~
   The function must first recover the correponding obj-id, then create a ~
   new page for displaying it"
  (sleep 0.01)
  ;; first recover the obj-id
  (let ((pos (car (%get-selected-cells-indexes item)))
        (object-list (object-list item)))
    (ewformat t "~&;=== ew-object-list-show-object-on-double-click /index: ~S,~
                 objects:~%  ~S" 
              pos object-list)
    (when (and pos object-list)
      (make-browser-window (nth pos object-list)
                                          :owner (owner current-page)
                                          :current-editor current-page
                                          :database (database current-page))
      )))

#|
;;;--------------------------------------------- EW-PROCESS-OBJECT-LIST-ON-CLICK

(defUn ew-process-object-list-on-click (item obj-list)
  "All found objects are displayed in the sp-locate table using the =summary ~
   method. The list of values is recorded in the table object-list slot."
  (let ((sp-locate-area (find-named-sibling item :object-list-item)))
    (ewformat t "~&Processing object-list: ~S" obj-list)
    ;; record the object list
    (setf (object-list sp-locate-area) obj-list)
    ;; then display it using a more legible format
    (%set-item-sequence sp-locate-area (ewf-get-summaries obj-list))))
|#

;;;------------------------------------------------------ EW-OBJECT-LIST-REFRESH

(defUn ew-object-list-refresh (current-page object-list)
  "refreshes the sp edit region, displays the edit list in the LFO and updates ~
   the counter value."
  (let ((sp-locate-area (%find-named-item :object-list-item current-page)))
    (ewformat t "~&;ew-object-list-refresh / Refreshing LFO and counter")
    ;; record the object list
    (setf (object-list sp-locate-area) object-list)
    ;; also in editor slot
    (setf (current-ep-object-list current-page) object-list)

    ;; redisplay list of objects
    (%set-item-sequence sp-locate-area (ewf-get-summaries object-list))
    ;; update counter
    (%set-item-text (%find-named-item :counter-item current-page) 
                                (format nil "~S" (length object-list)))
    t))
 
#|
? (moss::ew-object-list-refresh *editor-window* '($E-SECTION.3 $E-SECTION.4))
|#
;;;=============================================================================
;;;
;;;                               QUERY AREA
;;;
;;;=============================================================================

;;;---------------------------------------------------------- EWF-QUERY-ON-CLICK

(defUn ewf-query-on-click (current-page item)
  "when clicking into the query area, cleans the class, property, entry, counter ~
   slots" 
  (let ((gauge (%find-named-item :gauge-item current-page))
        (object-list-item (%find-sibling :object-list-item item))
        (query-area (%find-named-item :query-item current-page)) ; JPB1304
        (counter (%find-named-item :counter-item current-page)) ; jpb1304
        )
    
    (ewformat t "~&;=== ewf-query-on-click")
    
    ;; reset gauge
    (%set-item-value gauge 0)
    ;; reset object count
    (%set-item-value counter 0)
    ;; clear display list
    (%set-item-sequence object-list-item nil)
    ;; and list result to oblige examine to process query JPB1304
    (setf (current-ep-object-list current-page) nil)
    
    ;; clean everything, stay where we are
    ;(ewf-activate-area-list '(:query-item) current-page) ; jpb1304
    (%select-all query-area) ; jpb1304
    ;; activate QUIT and OK buttons
    (ewf-buttons-enable-only current-page 
                             '(:sp-OK-right-button :sp-QUIT-right-button
                                                   :examine-button)))
  )

#|
;;;------------------------------------------------ EW-SP-RIGHT-EXAMINE-ON-CLICK

(defUn ew-sp-right-examine-on-click (current-page item)
  "called when trying to access instances of a class. Builds a list of active 
   instances."
  (declare (ignore item)(special *query-abort-condition*))
  (let ((parsed-expr (current-parsed-expr current-page))
        (object-list-item (%find-named-item :object-list-item current-page))
        checking-info)
    
    ;; when we have a query and values to filter we do it
    (when (and (current-parsed-expr current-page)
               (current-value-list current-page))
      (let ((gauge (%find-named-item :gauge-item current-page))
            (candidates (current-value-list current-page))
            (ctr 0) result result-list gauge-max)
        ;; reset gauge
        (%set-item-value gauge 0)
        ;; compute gauge length   
        (setq gauge-max (length candidates))
        ;; when tracing tell what happens
        (ewformat t "Examining ~S candidate(s)" gauge-max)
        
        ;;we must prepare filtering data
        (unless (or (symbolp parsed-expr)(stringp parsed-expr))
          ;; we do that unless we have an entry point query
          (setq checking-info 
                (moss::split-query-and-compute-filters parsed-expr '(:all)))
          ;; reduce the list by applying local filter to each candidate
          (setq candidates (remove nil (mapcar (car checking-info) candidates)))
          ;(moss::mw-format "candidates filtered locally:~% ~S" candidates)
          ;; if the resulting candidate-list is *none*, then we have no solutions
          (when (or (eq candidates 'moss::*none*)  ; early failure detected by find-subset
                    (null candidates)) ; no instances in this class ??
            (ewf-post-message   "...Could not find anything." current-page)
            (return-from ew-sp-right-examine-on-click nil)))
        
        
        ;; loop for filtering objects, and printing them
        (dolist (candidate candidates)
          ;; check for abort condition
          (when *query-abort-condition*
            (setq *query-abort-condition* nil) ;reset flag
            (ewformat t "... Query aborted at user request...")
            (return-from ew-sp-right-examine-on-click nil))
          
          (incf ctr)
          ;; trace
          (ewformat t "~&... candidate#~S: ~S (~{~S ~})" 
                    ctr CANDIDATE (moss::send candidate 'moss::=summary))
          (setq result (moss::validate2 candidate parsed-expr () checking-info))
          ;; update gauge
          (incf (cg:value gauge)
                (ceiling (/ 100 gauge-max)))
          (when result
            ;; add result to the list
            (push result result-list)
            ;; print info for each object
            (%set-item-sequence (%find-named-item :object-list-item current-page)
                                (mapcar #'(lambda (xx) (car (send xx '=summary)))
                                        (reverse result-list)))))
        ;; record list in the viewing window
        (setf (object-list object-list-item) (reverse result-list))
        ;; at the end push gauge to 100%
        (%set-item-value gauge 100)
        )
      (return-from ew-sp-right-examine-on-click nil))
    ))
|#
;;;-----------------------------------------------------------  EW-PROCESS-QUERY

(defUn ew-process-query (expr widget)
  "expr is passed a query from the key handler. Checks its syntax. If OK, prints ~
   the result of the query into the answer pane"
  (let* ((parsed-query (moss::parse-user-query expr))
         (current-page (%container widget))
         (object-list-item (%find-named-item :object-list-item current-page))
         (counter (%find-named-item :counter-item current-page)) ; jpb1304
         checking-info
         candidates gauge gauge-max ctr result result-list)
    ;; if could not parse query, return, maybe tell user?
    (unless parsed-query
      (return-from ew-process-query nil))
    
    ;; now we do not do a block access to be able to use the thermometer
    ;; in practice we rewrite the access function
    (setq candidates (moss::collect-candidates parsed-query))
    
    ;;we must prepare filtering data
    (unless (or (symbolp parsed-query)(stringp parsed-query))
      ;; we do that unless we have an entry point query
      (setq checking-info 
            (moss::split-query-and-compute-filters parsed-query '(:all)))
      ;; reduce the list by applying local filter to each candidate
      (setq candidates (remove nil (mapcar (car checking-info) candidates)))
      ;(moss::mw-format "candidates filtered locally:~% ~S" candidates)
      ;; if the resulting candidate-list is *none*, then we have no solutions
      (when (or (eq candidates 'moss::*none*)  ; early failure detected by find-subset
                (null candidates)) ; no instances in this class ??
        (ewf-post-message  "...Could not find anything." current-page)
        (return-from ew-process-query nil)))
    
    ;; set up thermometer
    (setq gauge (%find-named-item :gauge-item (%container widget))
        ctr 0)
    ;; reset gauge
    (%set-item-value gauge 0)
    ;; compute gauge length   
    (setq gauge-max (length candidates))
    ;; when tracing tell what happens
    (ewformat t "Examining ~S candidate(s)" gauge-max)
    
    ;; loop for filtering objects, and printing them
    (dolist (candidate candidates)
      ;; check for abort condition
      (when (query-abort-condition current-page)
        (setf (query-abort-condition current-page) nil) ;reset flag
        (ewformat t "... Query aborted at user request...")
        (return-from ew-process-query nil))
      
      (incf ctr)
      ;; trace
      (ewformat t "~&... candidate#~S: ~S (~{~S ~})" 
                ctr CANDIDATE (moss::send candidate 'moss::=summary))
      (setq result (moss::validate2 candidate parsed-query () checking-info))
      ;; update gauge
      (incf (cg:value gauge)
            (ceiling (/ 100 gauge-max)))
      (when result
        ;; add result to the list
        (push result result-list)
        ;; print info for each object
        (%set-item-sequence (%find-named-item :object-list-item current-page)
                            (mapcar #'(lambda (xx) (car (send xx '=summary)))
                              (reverse result-list))))
      ) ; end loop
    ;; record list in the viewing window
    (setf (object-list object-list-item) (reverse result-list))
    ;; count objects and post counter JPB1304
    (%set-item-value counter (length result-list))
    
    ;; at the end push gauge to 100%
    (%set-item-value gauge 100)
    )
  (return-from ew-process-query nil))


;;;=============================================================================
;;;
;;;                     LOCAL AND GLOBAL PROPERTY AREA
;;;
;;;=============================================================================

;;;------------------------------------------ EW-SELECT-GLOBAL-PROPERTY-ON-CLICK

(defUn ew-select-global-property-on-click (current-page item)
  "Function called when a value in one of the selected lists has been clicked. ~
   Item points to the table in which we just clicked. ~
   The options of the select area must be highlighted. The value is recorded ~
   into the current-property page slot."
  (let* ((property-type (current-property-type current-page))
         current-property)
    
    (with-editor-environment current-page
      
      ;; wait until the selection is actually done and recorded
      (sleep 0.1)
      (ewformat t "~&;=== EW-select-global-property-on-click. type: ~S" property-type)
      
      (when (%get-selected-cells-indexes item)
        
        ;; recover property from the property list
        (setq current-property (nth (car (%get-selected-cells-indexes item)) 
                                    (case property-type
                                      (:sp (sp-list item))
                                      (:tp (tp-list item)))))
        ;; record the particular property that has been selected in the value slot
        (setf (current-property current-page) current-property)
        ;; list of values or objects associated with new property is nil
        (setf (current-value-list current-page) nil)
        ;; as well as current value (actual value for tp or object for sp)
        (setf (current-value current-page) nil)
        (ewformat t "~&;EW-select-global-property-on-click. Value area: ~S; ~
                     position: ~S; property: ~S" 
                  item (car (%get-selected-cells-indexes item))
                  (current-property current-page))
        
        ;; note that we are adding a value
        (setf (current-action current-page) :add)
        ;;  now, according to the property type we activate either the tp-work-area
        ;; or the locate area
        (case property-type
          (:sp
           ;; activate the locate area
           (ewf-activate-edit-sp-area current-page)
           ;; activate the detail area
           ;(dialog-item-open value-area)
           ;; activate the corresponding buttons
           (ewf-buttons-enable-only current-page '(:sp-create-right-button
                                                   :sp-add-right-button
                                                   :sp-cancel-right-button))
           ;; tell user to add value
           (ewf-post-message 
            "You can now search for an object to add, or create a new one."
            current-page)
           )
          (:tp
           ;; activate TP edit area
           (ewf-activate-edit-tp-area current-page)
           ;; position cursor into the :tp-work-area
           (%set-focus-item (%find-named-item :tp-work-item current-page))
           ;; must also activate the right edit buttons
           (ewf-buttons-enable-only current-page *ew-tp-right-button-list*)
           ;; tell user to add value
           (ewf-post-message "You can now add a value for the attribute..."
                             current-page)))
        
        ;; display property name in the title of the property pane
        (ewf-edit-tp-title-post current-page 
                                (car (-> current-property '=instance-name :class t)))
        ;; and disable selection area
        (ewf-deactivate-property-selection-area current-page)
        item))))

;;;------------------------------------------- EW-SELECT-LOCAL-PROPERTY-ON-CLICK

(defUn ew-select-local-property-on-click (current-page item)
  "Function called when a value in one of the selected lists has been clicked. ~
   Item points to the table in which we just clicked. ~
   The options of the select area must be highlighted. The value is recorded ~
   into the current-property page slot."
  (let* ((property-type (current-property-type current-page))
         current-property property-name)
    
    (with-editor-environment current-page
      
      ;; wait until the selection is actually done and recorded
      (sleep 0.1)
      
      (when (%get-selected-cells-indexes item)
        ;; recover current property from the property list
        (setq current-property (nth (car (%get-selected-cells-indexes item)) 
                                    (case property-type
                                      (:sp (sp-list item))
                                      (:tp (tp-list item)))))
        
        ;; record the particular property that has been selected in the value slot
        (setf (current-property current-page) current-property)
        ;; list of values or objects associated with new property is nil
        (setf (current-value-list current-page) nil)
        ;; as well as current value (actual value for tp or object for sp)
        (setf (current-value current-page) nil)
        
        (ewformat t "~&;=== EW-select-local-property-on-click. Value area:~%  ~S; ~
                     ~%  position: ~S; property: ~S" 
                  item (car (%get-selected-cells-indexes item)) current-property)
        
        ;; note that we are adding a value to a TP
        (setf (current-action current-page) :add)
        ;;  now, according to the property type we activate either the tp-work-area
        ;; or the sp-locate-area
        (case property-type
          (:sp
           ;; activate the locate area
           (ewf-activate-edit-sp-area current-page)
           ;; activate the detail area
           ;(dialog-item-open value-area)
           ;; activate the corresponding buttons
           (ewf-buttons-enable-only current-page '(:sp-create-right-button
                                                   :sp-add-right-button
                                                   :sp-clone-right-button
                                                   :sp-quit-right-button))
           ;; tell user to add value
           (ewf-post-message 
            "You can now search for an object to add, or specify a concept..."
            current-page)
           )
          (:tp
           ;; activate TP edit area
           (ewf-activate-edit-tp-area current-page)
           ;; position cursor into the :tp-work-area
           (%set-focus-item (%find-named-item :tp-work-item current-page))
           ;; must also activate the right edit buttons
           ;; To modify **********
           (ewf-buttons-enable-only current-page 
                                    (list :tp-add-right-button :tp-quit-right-button))
           ;; tell user to add value
           (ewf-post-message "You can now add a value for the attribute..."
                             current-page)))
        ;; set up edit process in the upper right corner
        ;; display property name
        (setq property-name (car (%get-value current-property '$PNAM)))
        (ewf-edit-tp-title-post current-page
                                (if (mln::mln? property-name) ; jpb 1406
                                  (mln::get-canonical-name property-name) ; jpb 1406
                                  property-name))
        ;; and disable selection area
        (ewf-deactivate-property-selection-area current-page)
        item))))

;;;=============================================================================
;;;
;;;                           SERVICE FUNCTIONS
;;;
;;;=============================================================================

;;;--------------------------------- EWF-EDIT-TP-AREA-SET-POSITION-AND-SELECTION

(defUn ewf-edit-tp-area-set-position-and-selection (current-page)
  "set the internal variables current-value and current-pos from the state ~
   of the value-area. When nothing is selected, returns nil"
  (let ((value-area (%find-named-item :value-area current-page))
        selected-cell)
    (setq selected-cell (car (%get-selected-cells-indexes value-area)))
    (unless selected-cell 
      ;; reset internal variables
      (setf (current-pos current-page) nil)
      (setf (current-value current-page) nil)
      (return-from ewf-edit-tp-area-set-position-and-selection nil))
    ;; otherwise set up internal variables
    (setf (current-pos current-page) selected-cell)
    (setf (current-value current-page) 
          (nth selected-cell (current-value-list current-page)))
    t))

;(ewf-edit-tp-area-set-position-and-selection *editor-window*)

;;;------------------------------------------------------ EWF-EDIT-TP-TITLE-POST

(defUn ewf-edit-tp-title-post (window text)
  "display text into the property name title of the tp-work-area"
  (%set-item-text (%find-named-item :value-area-title window) text)
  t)


;;;=============================================================================
;;;
;;;                             AUXILIARY FUNCTIONS
;;;
;;;=============================================================================
;;; The following functions are defined to facilitate programming of the editor.
;;; They are intended to express actions semantically, independently of the
;;; particular window system. The idea is to facilitate porting the editor onto
;;; an other window system
;;;=============================================================================


;;;=============================================================================
;;;
;;;                EXTRACTING OBJECTS (GIVEN ENTRY POINTS OR CLASS)
;;;
;;;=============================================================================

;;;--------------------------------------------------------- EWF-EXTRACT-OBJECTS

;;; this function should behave differently when we only have en entry point
;;; all TP with entry point functions should be collected and functions should
;;; be applied to the text string. Then we could collect objects.

;;; rewritten using access with text data from slots.

(defUn ewf-extract-objects (win)
  "uses the data stored into the ep structure to try to access objects. ~
   If none can be found, then do nothing. If some can be found, then post number ~
   and summaries in the display window.
Arguments:
   win (opt): a locator-window structure
Return:
   nil or object list."
  (let* ((class-ref (string-trim 
                     '(#\space) 
                     (%get-item-value (%find-named-item :class-item win))))
         (prop-ref (string-trim 
                    '(#\space) 
                    (%get-item-value (%find-named-item :property-item win))))
         (text (string-trim 
                '(#\space)                          
                (%get-item-value (%find-named-item :entry-point-item win))))
         (object-list-item (%find-named-item :object-list-item win))
         class-id prop-id object-list)
    
    ;; if we have a null class set ref to nil
    (if (equal class-ref "") (setq class-ref nil))
    ;; if we have a null prop-ref or is not an attribute, set ref to nil              
    (if (equal prop-ref "") (setq prop-ref nil))
    ;; null text are set to nil
    (if (equal text "") (setq text nil))
    
    (ewformat t "~&;=== ewf-extract-objects /class-ref: ~S, prop-ref: ~S, text: ~S"
              class-ref prop-ref text)
    
    ;; several cases according to the content of various areas
    (setq object-list
          (cond
           ;; if no values quit
           ((not (or class-ref prop-ref text))
            (return-from ewf-extract-objects nil))
           
           ;; only entry is there
           ((not (or class-ref prop-ref))
            (access text))
           
           ;; only class is there
           ((not (or prop-ref text))
            (access (list class-ref)))
           
           ;; only property is there (does not make sense)
           ((not (or class-ref text)) nil)
           
           ;; class and entry (?)
           ((not prop-ref)
            ;; check if class is OK
            (when
              (setq class-id (%%get-id class-ref :class))
              ;; access from value and filter out elements out of the class
              (remove 
               nil 
               (mapcar
                #'(lambda (xx) (if (moss::%type? xx class-id) xx))
                (access text)))))
           
           ;; class and property (does not make sense)
           ((not text) nil)
           
           ;; property and value
           ;; if property does not have a =make-entry, give up
           ((not class-ref)
            (when (and
                   (setq prop-id (%%get-id prop-ref :attribute))
                   (get-method prop-id '=make-entry))
              ;; get any object corresponding to entry point
              (setq object-list
                    (access (car (send prop-id '=make-entry text))))
              ;; filter those that do not have the specified property and value
              (remove nil
                      (mapcar #'(lambda (xx)(if (member text (send xx '=get prop-ref) 
                                                        :test #'string-equal)
                                              xx))
                              object-list))))
           
           ;; everything
           (t 
            (access `(,class-ref (,prop-ref :is ,text))))
           ))
    
    (ewformat t "~&;=== ewf-extract-objects /object-list:~%  ~S" object-list)
    
    ;; now we reset object-list
    (setf (current-ep-object-list win) object-list)
    
    ;; and post number and list
    (%set-item-text (%find-named-item :counter-item win) 
                    (format nil "~S" (length object-list)))
    
    ;; and summmaries of objects if less than max number
    (if (<= (length object-list) *max-number-of-objects-to-display*)
      (%set-item-sequence 
       (%find-named-item :object-list-item win)
       (ewf-get-summaries object-list))
      ;; otherwise activate the examine button
      (activate (%find-named-item :examine-button win))
      )
    
    ;; associate the list of objects with the display area
    (setf (object-list object-list-item) object-list)
    
    ;; return list of objects
    object-list))

;;;-------------------------------------------------------- EWF-GET-CLASS-NAME

(defUn ewf-get-class-name (object-id)
  "Must return a string as the class name to be displayed somewhere. If the ~
   is an orphan, then the string -Classless Object- is returned."
  (cond
   ((%pdm? object-id)
    (if (%is-orphan? object-id)
      "Classless Object"
      (format nil "~{~<~{~A~^ ~}~>~^, ~}" 
              (broadcast (-> object-id '=get-id '$TYPE) '=summary))))
   (t "")))
   

;;;----------------------------------------------------------- EWF-GET-CONTEXT

(defUn ewf-get-context ()
  "Returns a string with the current context"
  (format nil"~S" (symbol-value (intern "*CONTEXT*"))))

;;;-------------------------------------------------------- EWF-GET-OBJECT-NAME

(defUn ewf-get-object-name (object-id)
  "Must return a string as the entity name to be displayed somewhere."
  (cond
   ((%pdm? object-id)
    (if (-> object-id '=summary)
      (format nil "~{~A ~}" *answer*)
      (format nil "~S" object-id)))
   ;; when creating an editor, not yet initialized
   ((null object-id) "")
   (t "* Not a PDM object*"))
  )

;;;---------------------------------------------------------- EWF-GET-SUMMARIES

(defUn ewf-get-summaries (object-list)
  "Displays a list of object in the list area, before browsing or editing"
  (when (and object-list (listp object-list))
    (mapcar #'(lambda (xx)
                (rformat 60 nil "~{~A ~}" (-> xx '=summary)))
            object-list)))

;;; functions to get values to display into the browsing window

;;;----------------------------------------------- EWF-SP-GET-PROPERTY-AND-VALUE

(defUn ewf-sp-get-property-and-value (obj-id)
  "get all local structural properties and associated values as a list of strings"
  (cond
   ((%pdm? obj-id)
    (mapcar 
     #'(lambda (xx) (send xx '=format-value-list (%get-value obj-id xx)))
     (%%has-structural-properties obj-id (symbol-value (intern "*CONTEXT*")))))
   ;; otherwise return empty list
   (t nil)))

;;;----------------------------------------------- EWF-IL-GET-PROPERTY-AND-VALUE

(defUn ewf-il-get-property-and-value (obj-id)
  "get all local inverse properties and associated values as a list of strings"
  (mapcar 
   #'(lambda (xx)(send xx '=format-value-list (%get-value obj-id xx)))
   (%%has-inverse-properties obj-id (symbol-value (intern "*CONTEXT*")))))


;;;============================================================================
;;;
;;;                             STRUCTURAL PROPERTY
;;;
;;;============================================================================

;;;----------------------------------------------------- EWF-SP-DELETE-PROPERTY

(defUn ewf-sp-delete-property (entity-id property-id)
  "Function that deletes a property and all associated links."
  (send property-id '=delete-all entity-id)
  (ewformat t "~&;=== ewf-sp-delete-property. new entity value: ~S" 
            entity-id))

;;;----------------------------------------------------- EWF-SP-DELETE-SUCCESSOR

(defUn ewf-sp-delete-successor (entity-id successor-id property-id position)
  "Function that deletes a successor from the list of value of an entity. ~
   This is a positional delete in case there are several links to the ~
   same successor. Checks that the successor-id at the indicated ~
   position matches the specified successor-id.
Arguments:
   entity-id: original object
   successor-id: successor
   property-id: relationship
   position: position of the successor in the successor list
Return:
   modified original object, unmodified in case of error."
  (let* ((successor-list (%get-value entity-id property-id))
         (suc-id (nth position successor-list)))
    ;; specify position rather than value for deletion
    (ewformat t "~&;EW-tp-delete-successor; successors: ~S; to delete: ~S; position: ~S" 
              successor-list successor-id position)
    (unless (eql successor-id suc-id)
      (warn "successor to delete (~S) not found in the specified position (~S)~
             in the list of successors ~S" successor-id position successor-list)
      (return-from ewf-sp-delete-successor successor-list))
    ;; remove successor from the successor list at the right position
    ;; set the value of the value in the object and unlink it
    (setq successor-list (remove suc-id successor-list :start position :count 1))
    ;; remove the links using the first suc available
    (%unlink entity-id property-id successor-id)
    ;; reestablish the properr order (although this should not be important)
    (%%set-value-list entity-id successor-list property-id 
                      (symbol-value (intern "*CONTEXT*")))
    ;; return the list of values
    (%get-value entity-id property-id)))

;;;------------------------------------------------------ EWF-SP-DISPLAY-DETAILS

(defUn ewf-sp-display-details 
       (current-page desc-list left top width &optional value-list)
  "create a temporary window for displaying the current list of values. This is ~
   discarded when the user clicks anywhere in the window.
   The width of the floating window is determined by the number of values to display.
Arguments:
   current-page: the editor window
   desc-list: list of values in a display format
   left: left corner
   top: top corner
   width: width of the window
   value-list (opt): value list, e.g. as list of object ids"
  (ewformat t "~&;=== ewf-sp-display-details /*detail-window*: ~S" 
            (detail-window current-page))
  (let (detwin)
    ;; if window exists then close it
    (ew-detail-kill-window current-page)
    ;; compute the size of a new window, adding room for the frame 
    ;; cell hight is 13 when font size is 11
    (setq detwin
          (make-detail-window desc-list :top top :left left :width width
                              :height (+ 2 (* 15 (length desc-list)))
                              :editor-page current-page))
    (ewformat t "~&;=== ewf-sp-display-details /new detail-window:~%;  ~S~
                 ~%;... value-list: ~S" detwin value-list)
    ;; record object list (redundant with range property)
    (setf (object-list detwin) value-list)
    ;; show window
    (%select-window detwin)))

;;;-------------------------------------------------- EWF-SP-GET-SELECTED-VALUES

;;; the local search is probably broken
(defUn ewf-sp-get-selected-values (current-page)
  "Extract the obj-ids from all the selected cells of the Locate/Search area."
  (let* ((locate-area (%find-named-item :object-list-item current-page))
         values)
    ;; get the list of corresponding objects from the list stored in page
    (setq values (mapcar #'(lambda (xx) (nth xx (object-list locate-area)))
                         (%get-selected-cells-indexes locate-area)))
    (ewformat t "~&;=== ewf-sp-get-selected-values /objects: ~S from: ~%  ~S"
              values (object-list locate-area))
    ;; exit
    values))

;;;----------------------------------------------------- EWF-SP-INSERT-NTH-VALUE

(defUn ewf-sp-insert-nth-value (entity-id values property-id position)
  "Function that adds a list of values to the list of value at a given position. ~
   If position is nil inserts at the end of the list."
  (apply #'send property-id '=add entity-id values  
         (if position `((:before-nth ,position))))
  ;;***** should be able to specify position rather than value for insertion
  (ewformat t "~&;=== ewf-sp-insert-nth-value; returns: ~S" 
            (moss::%get-value entity-id property-id))
  (moss::%get-value entity-id property-id))

;;;=============================================================================
;;;
;;;                               TERMINAL PROPERTY
;;;
;;;=============================================================================

;;;------------------------------------------------------ EWF-TP-DELETE-PROPERTY

(defUn ewf-tp-delete-property (entity-id property-id)
  "Function that adds a value to the list of value of an entity"
  (=> property-id '=delete-all entity-id)
  ;(format t "~&===ewf-tp-delete-property. =delete-all returns: ~S" 
  ;        (%get-value entity-id property-id))
  (%get-value entity-id property-id))

;;;--------------------------------------------------------- EWF-TP-DELETE-VALUE

(defUn ewf-tp-delete-value (entity-id value property-id position)
  "Function that adds a value to the list of value of an entity"
  ;; specify position rather than value for deletion
  (=> property-id '=delete entity-id value `(:nth ,position))
  (ewformat t "~&;ewf-tp-delete-value. =delete returns: ~S" 
            (%get-value entity-id property-id))
  (%get-value entity-id property-id))

;;;------------------------------------------------------ EWF-TP-DISPLAY-DETAILS

(defUn ewf-tp-display-details 
       (current-page desc-list left top width &optional value-list)
  "create a temporary window for displaying the current list of values. This is ~
   discarded when the user clicks anywhere in the window.
   The width of the floating window is determined by the number of values to display.
Arguments:
   current-page: editor window
   desc-list: list of values in a display format
   left: left corner
   top: top corner
   width: width of the window
   value-list (opt): value list, e.g. as list of object ids"
  (let (detwin)
    ;; if window exists then close it
    (ew-detail-kill-window *editor-window*)
    ;; compute the size of a new window, adding room for the frame 
    ;; cell hight is 13 when font size is 11
    (setq detwin
          (make-detail-window desc-list :top top :left left :width width
                              :height (+ 2 (* 15 (length desc-list)))
                              :editor-page current-page))
    (ewformat t "~&;=== ewf-tp-display-details /new detail-window:~%  ~S~
                 ~%; value-list: ~S" detwin value-list)
    ;; record object list (redundant with range property)
    (setf (object-list detwin) value-list)
    ;; show window
    (%select-window detwin)))

;;;----------------------------------------------- EWF-TP-GET-PROPERTY-AND-VALUE

(defUn ewf-tp-get-property-and-value (obj-id)
  "get all local terminal properties and associated values as a list of strings"
  (cond
   ((%pdm? obj-id)
    (mapcar 
     #'(lambda (xx)(send xx '=format-value-list (%get-value obj-id xx)))
     (%%has-terminal-properties obj-id (symbol-value (intern "*CONTEXT*")))))
   ;; otherwise return the empty list
   (t nil)))

;;;----------------------------------------------------- EWF-TP-INSERT-NTH-VALUE

(defUn ewf-tp-insert-nth-value (entity-id value property-id position)
  "Function that adds a value to the list of value at a given position. ~
   If position is nil inserts at the end of the list."
  (ewformat t "~&;=== ewf-tp-add-nth-value /adding: ~S to: ~S" 
            value  (moss::%get-value entity-id property-id))
  ;; if value is an mln, we must add a level of parentheses otherwise =add will
  ;; think it is a list of values
  (if (mln::mln? value) (setq value (list value)))
  (apply #'send property-id '=add entity-id value  
         (if position `((:before-nth ,position))))
  (ewformat t "~&;ewf-tp-add-nth-value. =add returns: ~S" 
            (%get-value entity-id property-id))
  (%get-value entity-id property-id))

#|
(ewf-tp-insert-nth-value $E-INDEX.5 -222 $T-ATTACHED-FILE NIL)
(23 -222 ((:EN "sheep" "ewe") (:FR "mouton")) -222)
|#
;;;---------------------------------------------------- EWF-TP-REPLACE-NTH-VALUE

(defUn ewf-tp-replace-nth-value 
       (entity-id old-value new-value property-id position)
  "Function that replaces a value in the list of value at a given position."
  (apply #'send property-id '=delete entity-id old-value 
         (if position `((:nth ,position))))
  (ewformat t "~&;ewf-tp-replace-value; =delete returns: ~S" 
            (%get-value entity-id property-id))
  (apply #'send property-id '=add entity-id new-value  
         (if position `((:before-nth ,position))))
  ;(format t "~&;ewf-tp-replace-nth-value; =add returns: ~S" 
  ;        (%get-value entity-id property-id))
  (%get-value entity-id property-id))

;;;============================================================================
;;;
;;;                               DETAIL WINDOW
;;;
;;;============================================================================

;;;--------------------------------------------------------- MAKE-DETAIL-WINDOW
;;; not sure a global variable is useful since the window can be found using
;;; find-window with the keyword :detail-window

(defUn make-detail-window 
       (value-list &KEY top left width height (name :DETAIL-WINDOW) editor-page)
  "creates a window to list attribute values or relation neighbors"
  
  (unless (or (boundp (intern "*EDITOR-WINDOW*"))
              (symbol-value (intern "*EDITOR-WINDOW*")))
    (error "cannot make detail window if there is no editor window"))
  
  (let ((win
         (cr-window :CLASS 'EDITOR-DETAIL-WINDOW
                    :top top :left left :width width :height height
                    :title-bar nil
                    :name name
                    :background-color (%RGB (* 252 256) (* 244 256) (* 194 256))
                    ;:owner owner
                    :dialog-items (MAKE-detail-WIDGETS value-list width height)
                    :font +detail-window-text+
                    :state :normal)))
    ;; record detail window in the editor
    (if editor-page
        (setf (detail-window editor-page) win))
    ;; reord the current editor page in the detailed window
    (setf (editor-page win) editor-page)
    win))

#|
(setq $$ (moss::make-detail-window '(1 2 3 4 5 6 7 8) :top 100 :left 200
                             :width 400 :height 120))
(close $$)
|#
;;;--------------------------------------------------------- MAKE-DETAIL-WIDGETS

(defUn make-detail-widgets (value-list width height)
  "single widget occupying the whole surface"
  (ewformat t "~&;=== make-detail-widgets /witdh height: ~S ~S" 
            width height)
  (list
   (cr-multi-item-list
     :left 0 :top 0
     :width width :height height
     :on-click EW-detail-object-on-click
     :name :detail-item
     :sequence value-list)))

;;;------------------------------------------------------- EW-DETAIL-KILL-WINDOW

(defUn ew-detail-kill-window (current-page)
  "closes the detail window if open"
  (when current-page 
    (let ((dtwin (detail-window current-page)))
      (when dtwin
        (ewformat t "~&;=== ew-detail-kill-window /killing:~%  ~S " dtwin)
        (%window-close dtwin))
      t)))

;;;--------------------------------------------------- EW-DETAIL-OBJECT-ON-CLICK
;;; untested

(defUn ew-detail-object-on-click (detail-page item)
  "Called when the user clicks on one of the object in the display list. ~
   The function must first recover the correponding obj-id, then create a ~
   new page for displaying it"
  (sleep 0.01)
  (let ((selected-cell-indexes (%get-selected-cells-indexes item))
        (editor-page (editor-page detail-page))
        object-id)
    (if selected-cell-indexes
        ;; first recover the obj-id
        (setq object-id (nth (car selected-cell-indexes) (object-list detail-page)))
      (return-from ew-detail-object-on-click nil))
    
    (ewformat t "~&;=== ew-detail-object-on-click /object: ~S " object-id)
    (when (moss::%pdm? object-id)
      (make-browser-window object-id 
                           :current-editor editor-page
                           :owner (owner editor-page)
                           :database (database editor-page))
      )))

;;;=============================================================================

(format t "~%;*** MOSS v~A - Editor loaded ***" *moss-version-number*)

;;; :EOF