;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==============================================================================
;;;20/04/02
;;;            I M P O R T (file W-import.lisp)
;;;
;;;==============================================================================
;;; This file contains all the functions to build an intitial menu for loading 
;;; (importing) an ontology or opening an ontology database.

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
-------
2010
 Version 8
 =========
0918 creating the file
 2014
 0307 -> UTF8 encoding
2018
 0129 changing iw-load-on-click to allow more depth in application folders
2020
 0130 restructuring MOSS as an asdf system
|#

(in-package :moss)

;;;================================= globals ====================================

(defParameter *moss-panel* nil) 

;;; we need the following global when the label is "TEST/test0.xxx" 
;;; container is then "TEST" anf application name is the name of the ontology
;;; file, namely "test0"
;;; unused?
(defparameter *ontology-container* "" "folder containing source ontologies")


;;;===============================================================================
;;;                              MOSS-IMPORT-WINDOW
;;;===============================================================================

;;;=================================== Classes ==================================

;;;----------------------------------------------------------- MOSS-IMPORT-WINDOW

(defClass moss-import-window (cg:dialog)
  ((current-moss-window :accessor current-moss-window :initform nil)
   )
  (:documentation 
   "Window for loading an ontology"))

;;;==================================== Window ===================================

;;;------------------------------------------------------------- MAKE-MOSS-WINDOW
;; The maker-function, which always creates a new window.
;; Call this function if you need more than one copy,
;; or the single copy should have a parent or owner window.
;; (Pass :owner to this function; :parent is for compatibility.)

#|
(make-moss-import-window)
|#

(defUn make-moss-import-window ()
  (declare (special *moss-panel*))
  (setq *moss-panel*
         (cg:make-window :moss-import-window :owner (cg:screen cg:*system*)
           :class 'MOSS-WINDOW
           :exterior (cg:make-box 200 300 910 490)
           :background-color (cg:make-rgb :red 224 :green 255 :blue 206)
           :border :frame
           :child-p nil
           :close-button t
           :cursor-name :arrow-cursor
           :font (cg:make-font-ex :swiss "MS Sans Serif / ANSI" 11 nil)
           :form-package-name :moss
           :form-state :normal
           :maximize-button nil
           :minimize-button t
           :name :moss-interface
           :pop-up nil
           :resizable nil
           :scrollbars nil
           :state :normal
           :status-bar nil
           :system-menu t
           :title (concatenate 'string "MOSS " *moss-version-number*)
           :title-bar t
           :toolbar nil
           :dialog-items (make-moss-import-interface-widgets)
           :help-string nil
           )))

;;;------------------------------------------- MAKE-MOSS-IMPORT-INTERFACE-WIDGETS
#|
(make-moss-import-window)
|#

(defUn make-moss-import-interface-widgets ()
  (list 
   ;;=== top row of controls
   ;;--- application
   (make-instance 'cg:static-text
     :name :application-text :value "Ontology" 
     :left 12 :top 14 :width 60 :height 16 
     :font (cg:make-font-ex :swiss "Tahoma / ANSI" 11 '(:bold))
     )
   (make-instance 'cg:editable-text 
     :name :application 
     :left 84 :top 8 :width 120
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :template-string nil 
     :up-down-control nil 
     :value *default-application*
     )
   ;;--- language
   (make-instance 'cg:static-text 
     :name :object-id-text :value "Language"
     :left 280  :top 14 :width 70
     :font (cg:make-font-ex :swiss "Tahoma / ANSI" 11 '(:bold))
     )
   (make-instance 'cg:editable-text 
     :name :language
     :left 352 :top 8 :width 90
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :template-string nil
     :up-down-control nil 
     ;:on-change 'iw-language-on-change ; set after ontology is loaded
     :value "EN"
     )
   ;;--- package
   (make-instance 'cg:static-text 
     :name :object-id-text :value "Package"
     :left 530  :top 14 :width 70
     :font (cg:make-font-ex :swiss "Tahoma / ANSI" 11 '(:bold))
     )
   (make-instance 'cg:editable-text 
     :name :package 
     :left 600 :top 8 :width 90
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :template-string nil
     :up-down-control nil 
     ;:on-change 'iw-package-on-change ; set after ontology is loaded
     :value "?"
     )
   
   ;;=== left goup of buttons
   ;;--- OPEN DATABASE
   (make-instance 'cg:button 
     :name :open-database-button :title "OPEN/DB"
     :left 12 :top 60
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-click 'iw-open-database-on-click
     )
   ;;--- SAVE ALL to DATABASE
   (make-instance 'cg:button 
     :name :saved-all-button  :title "SAVE ALL/DB"
     :left 12 :top 92
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-click 'iw-save-all-on-click
     )
   
   ;;=== Import buttons
   (make-instance 'cg:static-text
     :name :application-text :value "Import format" 
     :left 190 :top 40 :width 90 :height 16 
     :font (cg:make-font-ex :swiss "Tahoma / ANSI" 11 '(:italic))
     )
   (make-instance 'cg::combo-box
     :name :import-format
     :left 190 :top 60 :width 120 :height 500
     :on-change 'iw-set-import-format-on-change
     ;; function returning a string to show in the menu
     ;:on-print #'(lambda (value) (cdr (assoc value *language-tag-names*)))
     :range '(:MOSS)
     :value :MOSS
     )
   ;;--- LOAD (from file)
   (make-instance 'cg:button 
     :name :saved-all-button  :title "IMPORT/LOAD"
     :left 190 :top 92
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-click 'iw-load-on-click
     )
   
   ;;=== Export buttons
   (make-instance 'cg:static-text
     :name :application-text :value "Export format" 
     :left 400 :top 40 :width 80 :height 16 
     :font (cg:make-font-ex :swiss "Tahoma / ANSI" 11 '(:italic))
     )
   (make-instance 'cg::combo-box
     :name :export-format
     :left 400 :top 60 :width 120 :height 500
     ;:on-change 'iw-set-export-format-on-change
     ;; function returning a string to show in the menu
     ;:on-print #'(lambda (value) (cdr (assoc value *language-tag-names*)))
     :range '(:RAW-DATA\(LISP\) :MOSS :OWL\(Compile\) :OWL :OWL+JENA :OWL+SPARQL)
     :value :RAW-DATA\(LISP\)
     )
   ;;--- EXPORT
   (make-instance 'cg:button 
     :name :saved-all-button  :title "EXPORT"
     :left 400 :top 92
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-click 'iw-export-on-click
     )
   
   ;;=== right group of buttons
   ;;--- NEW WINDOW
   (make-instance 'cg:button
     :name :new-window-button :title "NEW WINDOW"
     :left 595 :top 60 :width 94
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil)
     :on-click 'iw-new-window-on-click
     )
   ;;--- (unused)
   (make-instance 'cg:button 
     :name :db-content-button :title "EXIT"
     :left 595 :top 92 :width 94
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil)
     :on-click 'iw-exit-on-click 
     )
   
   ;;=== bottom line for messages
   ;;--- MESSAGE PANE
   (make-instance 'cg:multi-line-editable-text 
     :name :message-pane
     :left 12 :top 127 :width 676 :height 30  
     :font (cg:make-font-ex nil "Tahoma / ANSI" 11 nil)
     :template-string nil 
     :up-down-control nil 
     :value "<message area>")
   ))

#|
(make-moss-import-window)
|#

;;;===============================================================================
;;;                      CALLBACKS & SERVICE FUNCTIONS
;;;===============================================================================

;;;------------------------------------------------------------ IW-CHECK-LANGUAGE

(defun iw-check-language (lan)
  "checks that the input arg refers to one of the legal languages defined in ~~
   mln::*language-tags*.
Argument:
   lan: a string, e.g. \"EN\"
Return:
   a keyword or nil is the language is not available."
  (let ((language (intern (string-upcase lan) :keyword)))
    (if (member language mln::*language-tags*) language)))
       
#|
(iw-check-language "FR")
:FR
(iw-check-language ":FR")
NIL
|#
;;;------------------------------------------------------------- IW-CLEAN-MESSAGE

(defun iw-clean-message (dialog)
  (setf (cg:value (cg:find-component :message-pane dialog)) ""))

;;;------------------------------------------------------------- IW-EXIT-ON-CLICK

(defUn iw-exit-on-click (dialog widget)
  "leaves the application"
  (declare (ignore widget))
  (when (y-or-n-p "Are you sure you want to quit?")
    ;; should close all the opened windows
    (mapc #'close (cg.base:windows (screen cg.base:*system*)))
    (close dialog)
    (excl:exit))
  t)

;;;------------------------------------------------------ IW-EXPORT-FROM-DATABASE

(defun iw-export-from-database (dialog label export-mode language)
  "here data is on the disk partition.
Arguments:
   dialog: moss-panel
   label: application name
   export-mode-key: type of export we want :|RAW-DATA(LISP)| 
Return:
   t if OK, nil otherwise"
  (declare (special *database-pathname*))
  (let*  ((application-name (string-upcase label))
          file-pathname ontology-key application-package
          source-pathname owl-file-pathname jena-file-pathname)
    (format t "~%;--- application-name: ~S" application-name)
    
    ;;===== if we want a raw output from the database, do it now
    (when (eql export-mode :|RAW-DATA(LISP)|)
      ;;=== raw format is a list of pairs <key . value> as a checkpoint of the 
      ;; database
      ;; set file pathname
      (setq file-pathname 
            (make-pathname 
             :name (format nil "~A-DUMP-~A" 
                     application-name (get-current-date :compact t))
             :type "lisp"
             :defaults *database-pathname*))
      (format t "~%;--- file-pathname for dump: ~S" file-pathname)
      ;; make key for partition (if we want raw output, label is application name)
      (setq ontology-key (intern application-name :keyword))
      ;; if file exists, complain
      (if (probe-file file-pathname)
          (progn
            (cg:beep)
            (iw-print-message 
             dialog
             (format nil "dump file ~A already exists" file-pathname)))
        ;; otherwise, call the make function in the right package
        (with-package (find-package ontology-key)
          (moss::db-export *database-pathname* ontology-key 
                           :file-path file-pathname)))
      (return-from iw-export-from-database nil)
      )
    
    ;;===== for the following treatments we need a text file source
    ;; label is the name of the database partition, i.e. the application
    (setq source-pathname 
          (make-pathname 
           :name (format nil "~A-ONTOLOGY-~A" application-name
                   (moss::get-current-date :compact t))
           :type "lisp"
           :defaults *database-pathname*))
    ;; if file exists, complain
    (if (probe-file source-pathname)
        (progn
          (cg:beep)
          (iw-print-message 
           dialog
           (format nil "export file (~A) already exists" source-pathname))
          )
      ;; otherwise, call the make function in the right package
      (with-package (find-package application-name)
        (moss::make-ontology-file :file-pathname source-pathname)))
    
    ;;===== now process other cases
    ;; build a separate package to avoid conflicts with MOSS operations
    (setq application-package
          (or (find-package (string+ "SOL-" application-name))
              (make-package (string+ "SOL-" application-name) :use '(:cl :sol-owl))))
    (format t "~%;--- application-package: ~S" application-package)
    
    (case export-mode
      
      ;;=== here we want a text file
      (:moss)  ; already done, do nothing
      
      ;;=== OWL(COMPILE) compile only (no output file)
      ;; should decide as to where to put the results of compilation
      (:|OWL(COMPILE)|
       ;; :owl argument is nil
       (with-package application-package
         (sol::sol source-pathname :language language :errors-only t)))
      
      ;;=== OWL (no rule files)
      (:OWL
       (with-package application-package
         (sol::sol source-pathname :owl owl-file-pathname 
                   :language language)))
      
      ;;=== OWL+JENA
      (:OWL+JENA
       (setq jena-file-pathname 
             (make-pathname 
              :name (format nil "~A-JENA-~A" application-name
                      (moss::get-current-date :compact t))
              :type "lisp"
              :defaults file-pathname))
       ;; if file exists, complain
       (when (probe-file jena-file-pathname)
         (cg:beep)
         (iw-print-message 
          dialog
          (format nil "JENA export file (~A) already exists" jena-file-pathname))
         (return-from iw-export-from-database nil))
       
       (with-package application-package
         (sol::sol source-pathname 
                   :owl owl-file-pathname 
                   :rules jena-file-pathname
                   :language language
                   :rule-format "Jena")))
      
      ;;=== OWL+SPARQL
      (:OWL+SPARQL
       (cg:beep)
       (iw-print-message dialog "*** OWL+SPARQL Not implemented yet ***")
       (return-from iw-export-from-database nil)
       ;; set file pathnames
       ;; call SOL compiler
       )
      
      (otherwise
       (cg:beep)
       (iw-print-message dialog "*** Illegal export format ***")
       (return-from iw-export-from-database nil)
       )
      ) ; end case
    ))
    
;;;----------------------------------------------------- IW-EXPORT-FROM-TEXT-FILE

(defun iw-export-from-file (dialog source export-mode-key language)
  "here data is in a text file that must not have been loaded.
Arguments:
   dialog: moss-panel
   source: ontology file pathname
   export-mode-key: type of export we want :|RAW-DATA(LISP)|
   language: language, e.g. :FR 
Return:
   t if OK, nil otherwise"
  (let* ((application-name (iw-get-application-name source))
         work-package-name work-package jena-file-pathname sparql-file-pathname)
    
    ;; cook up a work package name
    (setq work-package-name (string+ "SOL-" (string-upcase application-name)))
    ;; if already exists, issue a warning
    (cond
     ((setq work-package (find-package work-package-name))
      (cg:beep)
      (iw-print-message 
       dialog (format nil "*** Reusing Workpackage ~S ***" work-package-name))
      )
     (t
      (setq work-package
            (make-package work-package-name :use '(:sol :sol-owl :cl)))))
    
    ;; post package-name of the original application name (not the package that
    ;; will be used for translating the ontology)
    (setf (cg:value (cg:find-component :package dialog)) application-name)
    
    (case export-mode-key
      
      ;;=== MOSS
      ((:MOSS  :|RAW-DATA(LISP)| ) 
         (cg:beep)
         (iw-print-message 
          dialog
          (format nil "cannot do that from a text file!"))
         (return-from iw-export-from-file nil))
      
      ;;=== OWL (without rules)
      (:OWL
       (with-package work-package
         (sol::sol source :language language)))
      
      ;;=== OWL(COMPILE)
      (:|OWL(COMPILE)|
       (with-package work-package
         (sol::sol source :errors-only t :language language)))

      ;;=== OWL+JENA
      (:OWL+JENA
       (setq jena-file-pathname 
             (make-pathname 
              :name (format nil "~A-JENA-~A" application-name
                      (moss::get-current-date :compact t))
              :type "lisp"
              :defaults source))
       ;; if file exists, complain
       (when (probe-file jena-file-pathname)
         (cg:beep)
         (iw-print-message 
          dialog
          (format nil "JENA export file (~A) already exists" jena-file-pathname))
         (return-from iw-export-from-file nil))
       
       (with-package work-package
         (sol::sol source 
                   :rules jena-file-pathname
                   :language language
                   :rule-format :Jena)))
      
      ;;=== OWL+SPARQL
      (:OWL+SPARQL
       (setq sparql-file-pathname 
             (make-pathname 
              :name (format nil "~A-SPARQL-~A" application-name
                      (moss::get-current-date :compact t))
              :type "lisp"
              :defaults source))
       ;; if file exists, complain
       (when (probe-file sparql-file-pathname)
         (cg:beep)
         (iw-print-message 
          dialog
          (format nil "SPARQL export file (~A) already exists" sparql-file-pathname))
         (return-from iw-export-from-file nil))
       
       (with-package work-package
         (sol::sol source 
                   :rules sparql-file-pathname
                   :language language
                   :rule-format :SPARQL)))

      ;;=== coming here if we do not consider all formats from menu!
      (otherwise
       (cg:beep)
       (iw-print-message 
        dialog (format nil "*** Bad export format ~S ***" export-mode-key))
       )
      )
    ))

;;;----------------------------------------------------------- IW-EXPORT-ON-CLICK
;;; export builds the target files from a text file of the ontology.
;;; Thus, if the database is not opened, it will consider that the source is that
;;; specified in the "ontology" slot of the MOSS panel. 
;;; If the database is opened, then it will first reconstruct a text file from the
;;; content of the database, to use it as a source file.

;;; the compilation will be done in a different process. If the application name
;;; is XXX then the package for compiling will be SOL-XXX to avoid conflicts 
;;; with the package of the ontology in the MOSS environment (indeed we use the
;;; same macros for translating the ontology structures)

;;; iw-export-on-click produces several outputs
;;;   - a raw data lisp, corresponding to a vomit of the database
;;;   - a reconstructed ontology text file (for a given version)
;;;   - an OWL text file
;;;   - a Jena or SPARQL text file
;;;   - an HTML text file
;;;   - a text file ?

(defun iw-export-on-click (dialog widget)
  "exports the content of the database or text file according to the specified ~
   format. Produces a number of files.
Return:
   nil"
  (declare (ignore widget)(special *database-pathname*))
  ;; read export-mode, application-name and language from window
  (let* ((export-mode (cg:value (cg:find-component :export-format dialog)))
         (label (cg:value (cg:find-component :application dialog)))
         (lan (cg:value (cg:find-component :language dialog)))
         source language)
    ;; check language and normalize it, e.g. "French" -> :FR
    (setq language (iw-check-language lan))
    (unless language
      (cg:beep)
      (iw-print-message 
       dialog (format nil "*** Unavailable language ~S ***" lan))
      (return-from iw-export-on-click nil))
    
    ;; make export-mode a key (already upper case)
    (setq export-mode (intern (string-trim '(#\space) export-mode) :keyword))
    ;; clear message area 
    (iw-print-message dialog "")
    
    (format t "~2%;========== Entering iw-export-on-click ==========")
    (format t "~%;--- label: ~S" label)
    (format t "~%;--- language: ~S" language)
    (format t "~%;--- export-mode: ~S" export-mode)
    (format t "~%;--- *database-pathname*: ~S" *database-pathname*)
    
    ;; if we have a database  opened, then process it
    (cond 
     (*database-pathname*
      (iw-export-from-database dialog label export-mode language))
     
     ;; otherwise export from file
     (t
      ;; locate source file
      (setq source (iw-find-file dialog label))
      (unless source
        (cg:beep)
        (iw-print-message 
         dialog (format nil "*** Can't find ontology file ~S ***" label))
        (return-from iw-export-on-click nil))
      ;; otherwile go process the file
      (iw-export-from-file dialog source export-mode language)))
    nil))

;;;----------------------------------------------------------------- IW-FIND-FILE

(defun iw-find-file (dialog label)
  "gets data from the control panel to locate the file specified by the content ~
   of the ontology slot. Looks into the application folder  first, then in the ~
   sample applications folder, then ask the user if not found.
   Checks if the file exists.
Argument:
   dialog: control-panel
   label: a string, e.g. \"Family\" or \"TEST/test0.sol\"
Return:
   the file pathname if found, nil otherwise."
  (declare (ignore dialog)(special *moss-directory-pathname*))
  (let* ((start 0) 
         (ext "lisp")
         pos dir-list file-pathname)
    
    ;; clear trailing spaces
    (setq label (string-trim '(#\space) label))

    ;; if nothing left, get file interactively
    (when (equal label "")
      (return-from iw-find-file
        (cg:ask-user-for-directory :browse-include-files t)))
    
    ;; get extension if any
    (setq pos (position #\. label :from-end t))
    ;; if there record it
    (when pos 
      (setq ext (subseq label (1+ pos)))
      (setq label (subseq label 0 pos)))
    
    ;; get eventual sequence of directory/.../file-name
    (loop
      ;; get next dir or file name
      (unless
          (setq pos (position #\/ label :start start))
        (push (subseq label start) dir-list)
        (return))
      (push (subseq label start pos) dir-list)
      (setq start (1+ pos)))
    
    ;; check if file is in the applications folder
    (setq file-pathname
          (make-pathname
           :defaults *moss-directory-pathname*
           :directory (append (butlast 
                               (pathname-directory *moss-directory-pathname*))
                              (list "applications")
                              (reverse (cdr dir-list)))
           :name (car dir-list)
           :type ext))
    
    (unless (probe-file file-pathname)
      ;; if not check the sample applications folder
      (setq file-pathname
          (make-pathname
           :defaults *moss-directory-pathname*
           :directory (append (butlast
                               (pathname-directory *moss-directory-pathname*))
                              (list "sample-applications")
                              (reverse (cdr dir-list)))
           :name (car dir-list)
           :type ext)))
    
    (unless (probe-file  file-pathname)
      ;; if not ask user to locate a file
      (setq file-pathname
            (cg:ask-user-for-directory :browse-include-files t)))
    
    ;; return file pathname if file exists, nil otherwise
    (if (and file-pathname (probe-file file-pathname))
        file-pathname
      nil)
    ))

#|
(iw-find-file :dummy "Family")
#P"C:\\Users\\Administrateur\\common-lisp\\moss\\sample-applications\\Family.lisp"

(iw-find-file :dummy "test0.sol")
NIL

(iw-find-file :dummy "TEST/test0")
#P"C:\\Users\\Administrateur\\common-lisp\\moss\\applications\\TEST\\test0.lisp"
|#
;;;------------------------------------------------------ IW-GET-APPLICATION-NAME

(defun iw-get-application-name (source-pathname)
  "extract the name of the application from the file pathname. It is the name ~
   of the file, i.e. the name of the ontology file."
  (pathname-name source-pathname))

#|
(iw-get-application-name (iw-get-file-pathname "SOL-tests/test0.sol"))
"test0"

(iw-get-application-name (iw-get-file-pathname "FAMILY"))
"FAMILY"
|#
;;;-------------------------------------------------------- IW-LANGUAGE-ON-CHANGE

(defun iw-language-on-change (widget new-value old-value)
  "when the value changes, check the new one and sets the *language* variable"
  (declare (special *language* mln::*language-tags*)(ignore old-value))
  (let* ((language (iw-check-language new-value)))
    (if language
        (setq *language* language)
      ;; otherwise do not change, but send a message
      (iw-print-message
       (cg:find-component (cg:parent widget) :message)
       (format nil "Illegal language. Legal ones are: ~{~S~^, ~}" 
         mln::*language-tags*))
      )
    )
  t)

;;;------------------------------------------------------------  IW-LOAD-ON-CLICK
;;; can use:
;;; (cg:ask-user-for-directory :browse-include-files t)

(defUn iw-load-on-click (dialog widget)
  "launches the application by loading the corresponding text file. If the file ~
   is not found in the MOSS/applications or MOSS/sample applications folder, ~
   asks the user for it.
The initializing menu is closed but the application does not quit."
  (declare (ignore widget))
  (iw-clean-message dialog)
  
  ;(break "iw-load-on-click")
  
  ;; here we must load the application situated in the APPLICATIONS folder
  (let ((package-name 
         (string-upcase (cg:value (cg:find-component :package dialog))))
        application-name application-package source fasl gate)
    
    ;; get the file pathname using the content of the "ontology" slot
    (setq source 
          (iw-find-file dialog (cg:value (cg:find-component :application dialog))))
    
    ;; whenever the file and compiled file are absent, complain
    (unless source
      (cg:beep)
      (iw-print-message dialog "*** Application file not found")
      (return-from iw-load-on-click nil))
    
    ;; application name is the file name
    (setq application-name (string-upcase (iw-get-application-name source)))
    
    ;; determine package name (either specified or from application name)
    (when (equal package-name "?")     
      (setq package-name application-name) ; name of the package
      ; update package slot
      (setf (cg:value (cg:find-component :package dialog)) application-name)
      )
    
    ;; check if it already exists
    (when (find-package package-name)
      (iw-print-message 
       dialog 
       (format nil 
           "*** Package ~S already exists, can't load text file. Click NEW WINDOW"
         package-name))
      (return-from iw-load-on-click))
    
    ;; otherwise create package, we need a package to create MOSS ontology objects
    (setq application-package (make-package package-name :use '(:moss :cl)))
    
    (format t "~%; iw-load-on-click /package: ~S" application-package)
    
    ;; create a synchronizing object (closed)
    (setq gate (mp:make-gate nil))
    ;; create new process, loading the file catching possible errors
    (mp:process-run-function 
     `(:name ,application-name :initial-bindings ,cg.base:*default-cg-bindings*)
     #'iw-load-on-click-run dialog source fasl application-name 
     application-package gate ; a closed gate to let the process wait
     )
    ;; then, everything OK
    t))

;;;-------------------------------------------------------- IW-LOAD-ON-CLICK-LOAD

(defun iw-load-on-click-load (dialog source fasl)
  "loads the application file guarding against errors.
Arguments:
   source: lisp file
   fasl: compiled file
Return:
   t if OK, nil if error."
  (let (msg errno test)
    (multiple-value-setq (test errno)
      (ignore-errors
       (progn
         ;; first try to catch moss errors
         (setq msg
               (catch 
                :error 
                ;; if we are in the distribution version, then we cannot call the 
                ;; compiler, we can only load source from an exec
                (cond 
                 ((not (member :compiler *features*))
                  (user-load source)
                  (return-from iw-load-on-click-load t))
                 ;; otherwise, if source exists
                 (source
                  (if (null fasl)
                      (setq fasl (compile-file source :external-format :utf-8))
                    ;; compile file if it source was modified
                    (excl:compile-file-if-needed source :output-file fasl 
                                               :external-format :utf-8))
                  ;; load compiled version
                  (m-load fasl :external-format :utf-8))
                 (t (m-load fasl :external-format :utf-8)))
                ))
         (when (stringp msg)
           (iw-print-message 
            dialog
            (format nil "*** Error while processing file:~% ~A" msg))
           (return-from iw-load-on-click-load nil))
         t)
       ))
    ;; look for system error
    (unless test
      (iw-print-message 
       dialog        
       (format nil "*** System error while processing file:~%   ~S" errno))
      (return-from iw-load-on-click-load nil))
    
    ;(break "iw-load-on-click-load")
    
    ;; otherwise return success
    (iw-print-message 
     dialog
     (format nil "~%Current package is: ~S" (package-name *package*)))
    t))

;;;--------------------------------------------------------- IW-LOAD-ON-CLICK-RUN

(defun iw-load-on-click-run (dialog source fasl name package gate)
  "runs the ontology editing process, creating a new window and loading the ontology ~
   text file. It sets default values for *context* *language* until the loaded ~
   values replace them.
Arguments:
   dialog: INIT window
   source: pathname for text file
   fasl: pathname for compiled ontology file
   name: ontology name (string)
   package: package specified in the init window (string) or application name
   gate: a special ACL structure to optimize process waiting (if nil, until t)
Return:
   nothing important"
  (declare (special *package* *editor-window* *editing-box*))
  ;; define globals to be used while processing this ontology (package is :moss)
  (let* ((*context* 0)
         (*language* 
          (intern (string-upcase (cg:value (cg:find-component :language dialog)))
                  :keyword))
         (*version-graph* '((0)))
         *editor-window* ; set a specific global
         *editing-box* ; and one for editing
         (*package* package)
         )
    ;; create a new environment, making sure that system name is a string
    (%create-new-package-environment package (format nil "~A" name))
    ;; load text file (will update globals)
    (iw-load-on-click-load dialog source fasl)
    
    ;; create MOSS window in the MOSS package ?
    ;; after loading the ontology, *package* is normally ontology package...
      
    ;(break "iw-load-on-click-run ... 2")

    (moss::make-moss-window 
     :name (intern name :keyword)
     :application-package *package* ; pass ontology package (?)
     :title (concatenate 'string "MOSS " *moss-version-number* " - " name)
     :gate gate)
    ;; record moss window locally. Note: it won't be reset when win is closed
    ;; for some reason can't do that (because panel is in a different process?)
    ;(setf (current-moss-window *moss-panel*) win)
    
    ;; must not get out of this function otherwise it kills the process...
    (mp:process-wait "until window is closed" #'mp:gate-open-p gate)
    :done))


;;;-------------------------------------------------------- IW-NEW-WINDOW-ON-CLICK
;;; When we have closed the processing window and would like to reopen it

(defun iw-new-window-on-click (dialog widget)
  "if the application name is valid and package exists, opens a new window in a ~
   new process."
  (declare (ignore widget))
  (iw-clean-message dialog)
  (let ((application-name 
         (string-upcase (cg:value (cg:find-component :application dialog))))
        (package-name-string 
         (string-upcase (cg:value (cg:find-component :package dialog))))
        application-package gate)
    ;; complain if there is no application name
    (when (equal application-name "")
      (iw-print-message dialog "*Error: no application name*")
      (return-from iw-new-window-on-click))
    
    ;; check package name
    (when (or (equal package-name-string "?")
              (equal package-name-string ""))
      (setq package-name-string application-name)
      ;; update package slot
      (setf (cg:value (cg:find-component :package dialog)) application-name)
      )
    
    ;; if package does not exist quit
    (unless (setq application-package (find-package package-name-string))
      (cg:beep)
      (iw-print-message
       dialog 
       (format nil "*Error: package ~S does not exist." package-name-string))
      (return-from iw-new-window-on-click))
    
    ;; create a synchronizing object
    (setq gate (mp:make-gate nil))
    ;; OK try to create a new window in a new process
    (mp:process-run-function 
     `(:name ,(symbol-name (gentemp application-name))
             :initial-bindings 
             ,cg.base:*default-cg-bindings*)
     #'iw-new-window-on-click-run dialog application-name application-package
     gate ; a closed gate to let the process wait
     )
    ;; OK all set
    t))

;;;---------------------------------------------------- IW-NEW-WINDOW-ON-CLICK-RUN

(defun iw-new-window-on-click-run (dialog name package gate)
  "new process creates a MOSS window and wait until gate opens.
Arguments:
   dialog: current window
   application-name: a string, e.g. \"FAMILY\"
   package: a package object
   gate: a gate object to optimize CPU on waiting
Return:
   nothing special."
  (declare (special *package* *context* *language* *editor-window* *editing-box*)
           (ignore dialog))
  ;; we assume that context, language and version-graph exist in package
  ;; since we are restarting a window
  (let ((*package* package)
        *editing-box*
        *editor-window*
        )
    ;; create MOSS window
    
    (moss::make-moss-window 
     :name (intern name :keyword)
     :title (concatenate 'string "MOSS " *moss-version-number* " - " name)
     :gate gate)
    (format t "~%; iw-new-window-on-click-run /gate: ~S" gate)
    ;; must not get out of this function otherwise it kill the process...
    (mp:process-wait "until window is closed" #'mp:gate-open-p gate)
    :done))

;;;----------------------------------------------------- IW-OPEN-DATABASE-ON-CLICK
;;; When an old database is opened, we should first check if the corresponding
;;; package exists, create it eventually and change *package* to the new
;;; value. The name of the package has been normally saved into the file.

;(setq application-name "family")

(defUn iw-open-database-on-click (dialog widget)
  "opens or close the database that contains all the objects from an application."
  (declare (ignore widget))
  (iw-clean-message dialog)
  ;; application name should be a string (symbol name), e.g. "Family" that
  ;; corresponds to the database partition
  (let ((application-name 
         (string-upcase (cg:value (cg:find-component :application dialog))))
        answer database-pathname gate)
    
    ;; ask for confirmation of the database name (a directory)
    (setq answer
          (y-or-n-p 
           (format nil "Is \"MOSS-ONTOLOGIES\" the right database name?")))
    (cond
     ;; if no look for another directory (starting at top level)
     ((null answer) (setq database-pathname (cg:ask-user-for-directory)))
     ;; if yes, cook up pathname
     ((eq answer T) 
      (setq database-pathname (db-compute-pathname)))
     )
    
    ;(format t "~% mw-open-database-on-click /database-pathname: ~%  ~S"
    ;  database-pathname)
    
    ;; check if we quitted in ask-user-for-directory
    (unless database-pathname
      (return-from iw-open-database-on-click))
    
    ;; create a synchronizing object (closed)
    (setq gate (mp:make-gate nil))
    ;; create new process, loading the file catching possible errors
    (mp:process-run-function 
     `(:name ,application-name :initial-bindings ,cg.base:*default-cg-bindings*)
     #'iw-open-database-on-click-run dialog database-pathname application-name 
     gate ; a closed gate to let the process wait
     )
    t))

;;;------------------------------------------------ IW-OPEN-DATABASE-ON-CLICK-RUN

(defun iw-open-database-on-click-run 
    (dialog database-pathname application-name gate)
  "function implementing a new process in which we handle a recorded ontology.
Arguments:
   dialog: MOSS-INIT-WINDOW
   database-pathname: <self describing>
   application-name: e.g. \"FAMILY\"
   gate: a special object for optimizing process wait
Return:
   t"
  (declare (special *moss-output* *editor-window* *editing-box*))
  ;; values will be modified by db-init-app
  (let ((*package* *package*) ; to control a process variable
        (*context* 0)
        (*version-graph* '((0)))
        (*language* :en)
        ;; tell MOSS to output messages into message pane
        (*moss-output* (cg:find-component :message-pane dialog))
        *editing-box*
        *editor-window*)
    ;; load initial data from the partition with the same name as the application
    ;; application package is handled by db-init-app
    (db-init-app database-pathname (intern application-name :keyword))
    
    ;; create a moss window
    (moss::make-moss-window 
     :name (intern application-name :keyword)
     :title (concatenate 'string "MOSS " *moss-version-number* " - " 
              application-name)
     :gate gate)
    
    ;; must not get out of this function otherwise it kill the process...
    (mp:process-wait "until window is closed" #'mp:gate-open-p gate)
    t))

;;;--------------------------------------------------------- IW-PACKAGE-ON-CHANGE


;;;------------------------------------------------------------- IW-PRINT-MESSAGE

(defun iw-print-message (dialog text)
  "displays a message in the bottom message panel"
  (let ((pane (cg:find-component :message-pane dialog)))
    (setf (cg:value pane) text)))

;;;--------------------------------------------------------- IW-SAVE-ALL-ON-CLICK
;;; the semantics is the following: we just loaded or defined the ontology and
;;; we want to bulk-save it onto disk. The name of the database will probably 
;;; be the same as the name of the text file. In that case se do not modify the
;;; content of the application slot.
;;; We want a clean new file, this it should not exist already. Otherwise, it
;;; it should be removed manually.
;;; Assumptions: moss::*application-package* contains the package of the ontology

(defUn iw-save-all-on-click (dialog widget)
  "saves all entities into the database." 
  (declare (ignore dialog widget))
  (db-save-all)
  t)


(format t "~%;*** MOSS v~A - MOSS import window loaded ***" *moss-version-number*)

;;; :EOF