;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;===============================================================================
;;;20/01/29
;;;               P E R S I S T E N C Y (file persistency.lisp)
;;;
;;;===============================================================================
;;; This file contains functions to ensure persistency using AllegroCache.

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; Objects are saved into a hash table the key being the object-id and the 
;;; associated value a string representing the value. An object is recovered bu
;;; loading the corresponding string and doing a read-from-string.
;;; The hash table is located in a partition of the database reached by giving
;;; the keyword corresponding to the package of the application, e.g. :address
;;; The default name for the database is MOSS-ONTOLOGIES.odb and the file is
;;; installed in the MOSS/applications folder.

;;; Global variables that contain valuable data:
;;;   *DATABASE-PATHNAME* pathname of the currently opened database
;;;   *APPLICATION-PACKAGE* package of the application and partition name
;;;   *DATABASE-STUB* list of objects acting as proxies to MOSS
;;;   *MCLCACHE* contains the MCL database object (pheap pointer)
;;;   *ALLEGROCACHE* contains the ACL database object

;;; Some functions assume that a database is opened and do not require a name
;;; argument. A name argument is a filename for MCL or a directory name for ACL

;;; Functions without name argument:
;;;  DB-AREA-EMPTY? area ; returns T if area is empty
;;;  DB-AREA-EXISTS? area ; returns name of area if it exists
;;;  DB-CLEAR-ALL area ; clears the specified partition (area)
;;:  DB-CLOSE () ; closes the currently opened DB
;;;  DB-COMPUTE-PATHNAME &key name directory db-pathname
;;;  DB-ERASE key area ; erase a key
;;;  DB-EXISTS? db-pathname ; check for existence
;;;  DB-EXTEND new-area &key (if-exists :error) ; creates a new areadb-pathname
;;;  DB-HANDLE () return handle to opened DB (pheap or *allegrocache*)
;;;  DB-INITIALIZE area ; bulk loads initial data
;;;  DB-LOAD key area ; gets the values associated with the key
;;;  DB-OPENED? (db-pathname) checks if DB is opened
;;;  DB-READY? area ; returns the partition handle (ACL map - MCL T/NIL)
;;;  DB-SAVE-ALL () ;
;;;  DB-SHOW-STRUCTURE () ; prints the names of the existing partitions
;;;  DB-STORE key,value,area ; stores the value associated with the key
;;;  DB-VOMIT area &key output-file, print-values ; prints partition keys

;;; Functions that require a file or directory name
;;;  DB-CREATE &key name,directory,db-pathname,area,if-exists
;;;  DB-DUMP (area &opt file-name) dumps the content of a partition
;;;  DB-EXPORT (?) not implemented yet
;;;  DB-OPEN &key name,directory,db-pathname
;;;  DB-RESTORE dump-file-pathname,db-pathname,area
;;;  DB-UPLOAD deprecated, use DB-RESTORE

;;; Using Allegrostore, objects are stored in maps that are directly addressable
;;; in the database. The database is a set of files contained in a directory rather
;;; than a single file. When the base is opened the *allegrocache* variable
;;; contains the reference to the database object.

#|
2010
 0228 version 1.0
 0331 keys are set to keywords
 0514 adding ACL Allegrostore code
 0522 modifying db-compute-pathname for OMAS
 0823 adding no-commit to db-store
 0823 adding db-commit to avoid name conflicts
 0915 adding db-save-all-in-new-partition
 1004 bug in save-all-into-new-partition corrected
 1008 bug in db-compute-pathname when executing for OMAS
 1025 adding db-dump
 1124 adding *woodpheap* similar to *allegrocache* removing *database-info*
2011
 1105 making db-load return 2 values
2012
 0212 making keys as keywords rather than symbols load, store, erase, and changing 
      db-fuse-ep
 0615 adding db-export
 1127 adding dump as a key parameter to the vomit arguments, adding DB-RESTORE
      adding db-area-empty?
2013
 0112 correcting a bug in db-restore
 1110 adding utf8 external foramt to db-dump
2014
 0307 -> UTF8 encoding
2015
 0212 extending utf-! encoding to dump files in db-export and db-restore
 0629 removing MCL optional code since MCL is no longer available
2016
 0204 correcting db-init-app for correct load of *version-graph* and *context*
|#

(in-package :moss)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :ACACHE "acache-2.1.12.fasl")
  (use-package :db.allegrocache)
  ;(unless (find-package :moss)(make-package :moss))
  )

;;;================================== Globals ====================================

;;;============================= macros ==========================================

;;;----------------------------------------------------------- DB-WITH-TRANSACTION

(defMacro db-with-transaction (&rest body)
  `(progn ,@body (commit))
  )

;;;=============================== Creating functions ============================

;;;---------------------------------------------------------- DB-ABORT-TRANSACTION

(defUn db-abort-transaction (transaction)
  "to flush the cache to disk"
  (declare (ignore transaction))
  (unless *database-pathname*
    (error "no database opened"))
  nil
  )

;;;---------------------------------------------------------------- DB-AREA-EMPTY?

(defun db-area-empty? (area)
  "returns T if the count of object is 0 (db should be opened)."
  (unless (db-area-exists? area)
    (error "Partition: ~S does not exist." area))
  (let* ((map (retrieve-from-index 'ac-map-range 'ac-map-name area))
         (count (map-count map :max 5)))
    (if (and (numberp count)(> count 0)) nil t)))
  
;;;--------------------------------------------------------------- DB-AREA-EXISTS?

(defUn db-area-exists? (area)
  "returns map object if area exists in CURRENT database (should be opened), NIL ~
   otherwise."
  
  (unless *database-pathname* 
    (error "no database opened."))
  
  ;; return the map object
  (retrieve-from-index 'ac-map-range 'ac-map-name area)
  )

#|
? (MOSS::DB-AREA-EXISTS? :address)
#<AC-MAP-RANGE oid: 13, ver 5, trans: 7,  not modified @
  #x21769742>
? (MOSS::DB-AREA-EXISTS? :other)
NIL
|#
;;;------------------------------------------------------------------ DB-CLEAR-ALL

(defUn db-clear-all (area)
  "clears the content of a partition of the database.
Argument:
   area: a keyword specifying the partition to be cleared
Return:
   base-handle"
  (declare (special *allegrocache*))
  (let ((map (db-area-exists? area)))
    ;; error if base not opened
    (when map
      ;; if map exists, erase map, commit, return base object
      (delete-instance map)
      (commit)
      *allegrocache*)    
    ;; otherwise, partition does not exist, return nil
    ))

#|
 (moss::db-clear-all :address)
|#
;;;---------------------------------------------------------------------- DB-CLOSE
; (setq file-name *f*)

(defUn db-close ()
  "closes the currently opened database.
Arguments:
   none
Return:
   T if OK, NIL if no base opened"
  (declare (special *allegrocache*))
  
  (if (and *allegrocache* (db.ac::database-open-p *allegrocache*))
      (progn
        (close-database)
        ;; reset pathname
        (setq *database-pathname* nil)
        ;; *allegrocache* is suposed to be reset by the system
        T)
    ;; otherwise warn
    (progn
      (warn "calling db-close and no database opened.")
      nil))
  )

#|
(setq *database-info* nil)
(moss::db-close *f*)
NIL

(db-close)
NIL
|#
;;;--------------------------------------------------------------------- DB-COMMIT

(defUn db-commit ()
  "to avoid naming conflicts"
  (commit))

;;;--------------------------------------------------------- DB-COMMIT-TRANSACTION

(defUn db-commit-transaction (transaction)
  "to flush the cache to disk"
  (declare (ignore transaction))
  (commit)
  )

;;;----------------------------------------------------------- DB-COMPUTE-PATHNAME

(defUn db-compute-pathname (&key name directory)
  "computes a pathname for the database. 
   For ACL returns a directory name, e.g. ACL-<name>-ODB
   For MCL returns a file pathname, e.g. MCL-<name>.odb
   If no name or directory is provided, the pathname is in the MOSS applications
   directory and the name is MOSS-ONTOLOGIES.
Arguments:
   name (key): usually the name of the application, a string
   directory (key): the directory that will contain the database
Return:
   always a pathname."
  (declare (special *moss-directory-pathname*))
  ;; set up default values
  (let ((name (format nil "ACL-~A" (or name "MOSS-ONTOLOGIES-ODB")))
        (directory (or directory *moss-directory-pathname*)))
    (unless (stringp name)
      (error "name should be a string rather than ~S" name))
    (merge-pathnames 
     (make-pathname
      ;; when called from OMAS, directory refers to a folder in the applications
      ;; file. Should check for MOSS...
      #-OMAS :directory #-OMAS (list :relative "applications")
      :name name
      )
     directory)))

#|
ACL
===
(db-compute-pathname)
#P"C:\\Program Files\\Allegro CL 8.2\\OMAS-MOSS 8.0\\MOSS\\applications\\ACL-MOSS-ONTOLOGIES-ODB"
(db-compute-pathname :name "NEWS")
#P"C:\\Program Files\\Allegro CL 8.2\\OMAS-MOSS 8.0\\MOSS\\applications\\ACL-NEWS"
MCL
===
?
|#
;;;--------------------------------------------------------------------- DB-CREATE

(defUn db-create (&key name directory db-pathname area (if-exists :error))
  "creates the persistent file. Can supersede if it exists
Arguments:
   name (key): name of object store, e.g. \"test\"
   directory (key): directory to host database
   db-pathname (key): db-pathname
   area (key): name of the partition to initialize in the database
   if-exists (key): what to do if file exists (default :error)
Return:
   handle to the database MCL: Wood pheap, ACL *allegocache*"
  (declare (special *allegrocache* *database-pathname*))
  
  ;; if no partition name (area) or not a keyword complain
  (unless (and area (keywordp area))
    (error "area argument: ~S should be present as a keyword." area))
  
  ;; cook up db pathname
  (setq db-pathname (or db-pathname 
                        (db-compute-pathname :name name :directory directory)))
  
  (cond
   ;; if database already opened and partition exists, error
   ((and (db-opened? db-pathname)
         (db-area-exists? area))
    (error  "database already exists with are: ~S." area))
   ;; if base opened but area does no exists, create area
   ((db-opened? db-pathname)
    (db-extend area)
    (return-from db-create (db-handle)))
   ;; if a different base is opened, close it
   (*database-pathname*
    (db-close)))
 
  ;; now create new database
  ;; we create a folder in the application directory
  ;; filename must be the pathname of a directory, so we convert the file-pathname
  ;; into a directory pathname
  
  (open-file-database db-pathname :if-exists if-exists 
                      :if-does-not-exist :create)
  ;; record path into global variable
  (setq *database-pathname* db-pathname)
  ;; create now map
  (db-extend area)
  ;; return database object
  *allegrocache*
  )


#|
(setq *f* 
      (merge-pathnames 
       (make-pathname
        :directory '(:relative "applications")
        :name "ACL-MOSS-ONTOLOGIES")
       moss::*moss-directory-pathname*))
#P"C:\\Program Files\\Allegro CL 8.2\\OMAS-MOSS 8.0\\MOSS\\applications\\ACL-MOSS-ONTOLOGIES"

(db-create :filename *f* :area :address)
#<AllegroCache db "C:\\Program Files\\Allegro CL 8.2\\OMAS-MOSS 8.0\\MOSS\\applications\\ACL-MOSS-ONTOLOGIES"
@ #x21a5f11a>
|#
;;;----------------------------------------------------------------------- DB-DUMP
;;; *untested*

(defUn db-dump (area &optional file-name)
  (unless (keywordp area)
    (error "~%area arg to db-dump ~S should be a keyword" area))

  (let ((map (retrieve-from-index 'ac-map-range 'ac-map-name area)))
    (when map
      (with-open-file (stream (or file-name "moss-base-dump.lisp")
                              :direction :output :external-format :utf-8)
        ;; iterate over map
        (map-map #'(lambda (xx yy) (print (cons xx yy))) map)))
    :done)
  )

;;;---------------------------------------------------------------------- DB-ERASE

(defUn db-erase (key area)
  "removes a key from the specified partition of the object store.
Arguments:
   key: key to erase
   area: partition
Return:
   key or nil when something wrong"
  (unless (and key (symbolp key))
    (error "key ~S must be a non nil symbol." key))
  
  ;; compute a key that will be used instead of symbols for storing data to avoid
  ;; the package problem
  (if (and (symbolp key) (not (eql (symbol-package key) (find-package :keyword))))
      (setq key (intern (concatenate 'string "@" (symbol-name key)) :keyword)))
  
  (let ((map (db-area-exists? area)))
    (when (and map (map-value map key))
      (remove-from-map map key)
      (commit)
      ;; return key if it was there
      key)
    ;; otherwise return nil
    )
  )

#|
? (db-store 'K001 "K001" :address)
K001
? (moss::db-vomit :address)
 === MOSS::K001
 === MOSS::K003
NIL
? (moss::db-erase 'K001 :address)
K001
? (moss::db-vomit :address)
 === MOSS::K003
NIL
? (moss::db-erase 'K001 :address)
K001
|#
;;;-------------------------------------------------------------------- DB-EXISTS?

(defUn db-exists? (db-pathname)
  "checks if database already exists.
Argument:
   db-pathname: pathname as resulting from db-compute-pathname
Return:
   pathname if exists nil otherwise."
  ;; quick check if the database is opened
  (if (equal *database-pathname* db-pathname)
    (return-from db-exists? db-pathname))
  ;; otherwise check file
  (probe-file db-pathname))

;;;--------------------------------------------------------------------- DB-EXPORT
;;; This function should produce a flat file structured as follows:
;;; Name of the database, e.g. "ONTOLOGIES"
;;; Name of first partition, e.g. :address
;;; Sequence of pairs (<key> . <value>) content of the partition
;;; ...
;;; Name of the last partition
;;; Sequence of pairs (<key> . <value>) content of the partition
;;; :EOF

;;; If a partition is specified, then the name of the file should be 
;;; <partition>-ONTOLOGY.lisp and contain only the partition data (e.g. for an agent)

(defUn db-export (database-path area &key file-name file-path)
  "exporting the content of a specific database partition into a flat file.
Arguments:
   database-path: pathname of the database to dump
   area: partition to dump
   file-name: name of the resulting text file
   file-path: pathname of the resulting text file (supersede file-name if present
Return:
   :done"
  ;; check whether the database exists
  (unless (db-opened? database-path)
    (format t "~%; db-export /database with path: ~S is not opened."
      database-path)
    (cg:beep)
    (return-from db-export nil))
  ;; if database exists it must be opened
  
  ;; we dump the result into the specified file
  (with-open-file (ss (or file-path file-name) :direction :output
                      :if-does-not-exist :create :external-format :utf-8)
    (format ss ";;; Saved text output of the ~S partition of the ~S database." 
      area (or file-path file-name))
    
    ;; dump the specific partition
    (let ((map (retrieve-from-index 'ac-map-range 'ac-map-name area)))
      (if map
          ;; iterate over map
          (map-map #'(lambda (xx yy) (format ss "~%~S"  (cons xx yy)))
                   map))
      )
    (format ss "~%:EOF"))
    :done)
	
;;;--------------------------------------------------------------------- DB-EXTEND

(defUn db-extend (new-area)
  "extends a database by creating a new partition. Error if the partition exists.
Arguments:
   new-area: name to qualify the new partition, usually that of a package
Return:
   database-handle if OK, NIL otherwise."
  (unless *database-pathname* (error "no base opened."))
  
  (let ((map (db-area-exists? new-area)))
    (when map
      (cg:beep)
      (mformat "~%partition ~S in database ~S already exists." 
               new-area *database-pathname*)
      (return-from db-extend nil))
    
    ;; create new map, commit and return map object
    (setq map (make-instance 'ac-map-range :ac-map-name new-area))
    (commit)
    map)
  
  )
#|
(moss::db-extend :address)
|#
;;;------------------------------------------------------------------ DB-FUSE-EP

(defUn db-fuse-ep (ep area)
  "function called by db-initialize when opening an object base and reconnecting ~
   stub. Fuses application and MOSS entries.
Argument:
   ep: entry-point
   area:: a keyword specifying the application partition in the database
Return:
   ep value."
  (let (app-ep-l ep-l res)
    ;; first load the local interpretation of entry point
    ;(setq new-key (intern (concatenate 'string "M_" (symbol-name ep))))
    
    (setq app-ep-l (db-load ep area))
    ;; ep-l is the value of the entry-point defined in the MOSS package
    (setq ep-l (symbol-value ep))
    
    ;(if (eql ep '=make-entry)
    ;  (format t "~%*** db-fuse-ep 1/ =make-entry ep-l app-e-l: ~% ~S~% ~S" ep-l app-ep-l))
    ;; brute-force merge on inverse links
    (dolist (item ep-l)
      (if (%is-inverse-property? (car item))
        (push (cons (car item) 
                    (db-fuse-ep-values (cdr item)(cdr (assoc (car item) app-ep-l))))
              res)
        (push item res)
        ))
    ;(if (eql ep '=make-entry)
    ;  (format t "~%*** db-fuse-ep 2/ =make-entry res: ~% ~S" res))
    (reverse res)))

#|
;In package FAMILY:
? method
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 METHOD))
 (MOSS::$ENAM.OF (0 MOSS::$FN COMMON-LISP-USER::$E-FN))
 (MOSS::$EPLS.OF (0 MOSS::$SYS.1 COMMON-LISP-USER::$E-SYS.1)))
? (db-load 'M_method)
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 METHOD)) (MOSS::$ENAM.OF (0 $E-FN))
 (MOSS::$EPLS.OF (0 $E-SYS.1)))

? (moss::db-fuse-ep 'method)
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 METHOD))
 (MOSS::$ENAM.OF (0 MOSS::$FN COMMON-LISP-USER::$E-FN $E-FN))
 (MOSS::$EPLS.OF (0 MOSS::$SYS.1 COMMON-LISP-USER::$E-SYS.1 $E-SYS.1)))
|#
;;;--------------------------------------------------------- DB-FUSE-EP-FOR-OMAS
;;; when reconciling entry points from an agent ontology, because the saved values
;;; in the agent ontology can be noisy, we need to consider only values defined in
;;; the agent's package, i.e. values belonging to the agent. Indeed, assume that
;;; we consider the =make-entry entry point and we have saved values belonging to
;;; MOSS, e.g. moss::$FN.100; assume further that we have modified MOSS by defining
;;; additional methods; then the saved value no longer corresponds to the intended
;;; MOSS method, which will introduce some inconsistency in the system. The same
;;; is true for any saved object that does not specifically belongs to the agent,
;;; i.e. whose symbol was not defined in the agent package.

(defUn db-fuse-ep-for-omas (ep area)
  "function called by db-initialize when opening an object base and reconnecting ~
   stub. Fuses application and MOSS entries in the OMAS context.
Argument:
   ep: entry-point
   area:: a keyword specifying the application partition in the database
Return:
   ep value."
  ;; we should execute in the agent package (same as area for an agent)
  ;(format t "~%; db-fuse-ep-for-omas /area: ~S, *package*: ~S" area *package*)
  (let (app-ep-l ep-l obj-list res)
    ;; first load the local interpretation of entry point    
    (setq app-ep-l (db-load ep area))
    
    ;; ep-l is the value of the entry-point defined in the MOSS package
    (setq ep-l (symbol-value ep))
    
    ;(if (eql ep '=make-entry)
    ;  (format t "~%*** db-fuse-ep 1/ =make-entry ep-l app-e-l: ~% ~S~% ~S" ep-l app-ep-l))
    ;; brute-force merge on inverse links
    (dolist (item ep-l)
      (cond
       ((%is-inverse-property? (car item))
        ;; fuse the list of different versions
        (setq obj-list
              (db-fuse-ep-values-for-omas 
               (cdr item)(cdr (assoc (car item) app-ep-l))))
        ;; keep only if obj-list is not empty
        (if obj-list (push (cons (car item) obj-list) res)))
       ;; we keep any other property as it is
       (t (push item res)))
      )
    ;(if (eql ep '=make-entry)
    ;  (format t "~%*** db-fuse-ep 2/ =make-entry res: ~% ~S" res))
    (reverse res)))

;;;----------------------------------------------------------- DB-FUSE-EP-VALUES

(defUn db-fuse-ep-values (val app-val)
  "merges values, deleting duplivates across multiple versions. E.g.
   ((0 A B)(1 C)(3 D)) and ((0 A F G)(2 H J)) -> ((0 A B F G)(1 C)(2 H J)(3 D))
   No check is done on the validity of contexts. Order of values not preserved.
Arguments:
   val: version a-list
   app-val: application version a-list
Result:
   merged a-list."
  (let (res app-pair)
    (dolist (pair val)
      ;; get first pair of the value of the object
      (if (setq app-pair (assoc (car pair) app-val))
          ;; when it is also in the second argument
          (progn
            ;; buid a simgle value starting with the property
            (push (cons (car pair)
                        (delete-duplicates (append (cdr pair)(cdr app-pair))
                                           :test #'equal))
                res)
          (setq app-val (remove-if #'(lambda (xx) (eql xx (car pair))) app-val 
                                   :key #'car))
          )
        ;; otherwise keep value
        (push pair res))
      )
    (setq res (append (reverse res) app-val))
    ;; return result
    res))

#|
? (db-fuse-ep-values '((0 A B)(1 C)(3 D)) '((0 A F G)(2 H J)))
((0 B A F G) (1 C) (3 D) (2 H J))
|#
;;;-------------------------------------------------- DB-FUSE-EP-VALUES-FOR-OMAS

(defUn db-fuse-ep-values-for-omas (val app-val)
  "merges values, deleting duplivates across multiple versions. E.g.
   ((0 A B)(1 C)(3 D)) and ((0 A F G)(2 H J)) -> ((0 A B F G)(1 C)(2 H J)(3 D))
   No check is done on the validity of contexts. Order of values not preserved.
Arguments:
   val: version a-list
   app-val: application version a-list
   omas (opt): if true, means that we are in the OMAS context, e.g. agent ontology
Result:
   merged a-list."
  (let (res app-pair val-list-to-add)
    
    ;;=== process one version at a time
    (dolist (pair val)
      ;; get first pair of the value of the object
      (cond
       ((setq app-pair (assoc (car pair) app-val))
        ;; when it is also in the second argument
        ;; filter out app values that are not in the current package
        (setq val-list-to-add 
              (remove nil
                      (mapcar #'(lambda (xx) 
                                  (if (eql (symbol-package xx) *package*) xx))
                        (cdr app-pair))))
        ;; build a simgle value starting with the property
        (if val-list-to-add
            (push (cons (car pair)
                        (delete-duplicates (append (cdr pair) val-list-to-add)
                                           :test #'equal))
                  res)
          (push pair res))
        ;; remove the processed version from the application list
        (setq app-val (remove-if #'(lambda (xx) (eql xx (car pair))) app-val 
                                   :key #'car))
        )
       ;; otherwise keep value
       (t (push pair res)))
      )
    
    ;;=== then process application list versions not in the moss list
    (dolist (app-pair app-val)
      ;; remove anything not in the current package
      (setq val-list-to-add
            (remove nil
                      (mapcar #'(lambda (xx) 
                                  (if (eql (symbol-package xx) *package*) xx))
                        (cdr app-pair))))
      ;; if there is any thing left keep it
      (if val-list-to-add
          (push (cons (car app-pair) val-list-to-add) res)))
    
    ;; return result
    (reverse res)))

#|
Use string to avoid compiler complain about unknown packages
"
(moss::db-fuse-ep-values-for-omas
 '((0 moss::A contact::B)(1 moss::C)(3 contact::D)) 
 '((0 moss::A moss::F project::G)(2 contact::H project::J)(4 moss::K project::L)))
"
((0 MOSS::A CONTACT::B G) (1 MOSS::C) (3 CONTACT::D) (2 J) (4 L))
|#

;;;--------------------------------------------------------------------- DB-HANDLE

(defUn db-handle ()
  "return handle to the database.
Argument:
   none
Return:
   MCL pheap, ACL *allegrocache*"
  (declare (special *allegrocache*))
  ;; if not opened error
  (unless *database-pathname* (error "no database opened."))
  *allegrocache*
  )


;;;------------------------------------------------------------------- DB-INIT-APP
;;; when we are in the OMAS context and loading an agent minimal information, then
;;; we could have saved noisy data, in particular for entry points. We must then
;;; fuse the existing entry points with the ones depending solely on the agent
;;; package

(defUn db-init-app (db-pathname area)
  "opens and initialize the application environment for further processing. We ~
   load all concepts, attributes, properties, methods, with the exception of ~
   entry points. Current package must be application package.
Arguments:
   db-pathname: pathname of the application database to be opened as resulting
                from db-compute-pathname
                normally related to the name of the application, e.g. MAIL
   area: keyword, both the name of the partition and of the application package
Return:
   :done"
  (unless (and area (keywordp area))
    (cg:beep)
    (mformat "~%Area argument ~S should be a keyword." area)
    (return-from db-init-app))
  
  (let (package app-id moss-ep-list ep-list id val)
    ;; first open the base
    (db-open :db-pathname db-pathname)
    
    ;; check if partition exists, should have been set upon creation
    (unless (db-area-exists? area)
      (mformat "~%Area arg ~S does not exist in database ~S." 
               area (pathname-name db-pathname))
      (cg:beep)
      (return-from db-init-app))
    
    ;;=== OK here database is opened and partition exists
    
    ;; check package
    
    (setq package  
          (or (find-package area)
              ;; if not active, create it
              (make-package area :use '(:moss :cl :db.allegrocache))))
    
    ;; switch to application package
    (setq *package* package)
    
    ;; record package for MOSS (safety measure)
    (setq *application-package* *package*)
    ;(format t "~%; db-init-app /*package*: ~S" *package*)
    
    ;; need here to load minimal set of objects
    ;; first load the object symbol representing the application, e.g. $SYS.1
    (setq app-id (db-load (intern "*MOSS-SYSTEM*") area))
    
    (unless app-id
      (%beep)
      (mformat "~%Database problem: can't load *moss-system* from area ~S" area)
      (return-from db-init-app))
    
    ;; kludge: load local context and version-graph
    ;(%ldif (intern "*CONTEXT*"))
    ;(%ldif (intern "*VERSION-GRAPH*"))
    ;; does not work if they already have a value
     
    (setq val (db-load (intern "*VERSION-GRAPH*") area))
    ;; careful: NIL is a-list
    (if (and val (alistp val)) (set (intern "*VERSION-GRAPH*") val))
    (setq val (db-load (intern "*CONTEXT*") area))
    (if (integerp val) (set (intern "*CONTEXT*") val))
    
    ;; load stub, i.e. load $SYS.1 for example
    (set app-id (db-load app-id area))
    
    ;; get the list of MOSS entry points
    (setq moss-ep-list (%get-value *moss-system* '$EPLS))

    ;; and reconnect stub proxies (list of MOSS classes that must be connected
    ;; e.g. contact::$FN is a subclass of moss::$FN
    (dolist (obj-pair *database-stub*)
      ;; reinstall $IS-A links from disk objects to MOSS objects
      (%%relink-isa  (%ldif (intern (car obj-pair))) (cdr obj-pair)))
    
    ;; load variables (includes *moss-system* and *ontology*)
    (dolist (var (%get-value app-id '$SVL))
      (%ldif var))
    
    ;; load function definitions
    (dolist (fn (%get-value app-id '$DFXL))
      (setq id (%ldif fn))
      ;; eval function definition
      (eval (symbol-value id)))
    
    ;; load functions
    (dolist (fn (%get-value app-id '$SFL))
      (%ldif fn :function))
    ;; fuse system entry points, load only entry points common to MOSS and app
    ;; first get the list of application entry points
    (setq ep-list (%get-value app-id '$EPLS))
    ;(format t "~% db-init-app 1 / methods: ~% ~S" =make-entry)
    ;(format t "~% db-init-app 1 / common objects: ~% ~S" (intersection ep-list moss-ep-list))
    ;; kludge: special case for OMAS when loading ep for agent ontologies
    ;;********** might also be considered for MOSS applications!
    #+OMAS
    (dolist (ep (intersection ep-list moss-ep-list))
      (set ep (db-fuse-ep-for-omas ep area)))
    #-OMAS
    (dolist (ep (intersection ep-list moss-ep-list))
      (set ep (db-fuse-ep ep area)))
    ;(format t "~% db-init-app 2 / methods: ~% ~S" =make-entry)
    ;; load concepts
    (dolist (id (%get-value app-id '$ENLS))
      (%ldif id))
    ;; load attributes
    (dolist (id (%get-value app-id '$ETLS))
      (%ldif id))
    ;; load relations
    (dolist (id (%get-value app-id '$ESLS))
      (%ldif id))
    ;; load inverse properties
    (dolist (id (%get-value app-id '$EILS))
      (%ldif id))
    ;; load methods
    (dolist (var (%get-value app-id '$FNLS))
      (%ldif var :method))
    
    ;(mformat "~%;*** Application ~A connected to MOSS ***" area)
    :done))

;;;----------------------------------------------------------------------- DB-LOAD
;;; one problem occurs when the value stored in the database is NIL, in which case
;;; we cannot differentiate whther the value was not in the database or whether it
;;; was there and it is null.

(defUn db-load (key area)
  "load an object from the database. The partition is the package of the key.
Argument:
   key: a symbol, the package of which is the name of the database partition
   area: partition
Return:
   2 or 3 values
     - the value of key as stored in the database or nil
     - T if the value was found in the database, NIL otherwise
     - a message in case partition was not found."
  (declare (special *allegrocache*))
  
  (unless (and key (symbolp key))
    (error "db-load(~S): key ~S must be a non nil symbol." area key))
  
  ;; compute a key that will be used instead of symbols for storing data to avoid
  ;; the package problem
  ;; if not already a keyword, cook up a keyword starting with @
  (if (and (symbolp key) (not (eql (symbol-package key) (find-package :keyword))))
      (setq key (intern (concatenate 'string "@" (symbol-name key)) :keyword)))
  
  (let ((map (db-area-exists? area)) val disk)
    (cond
     (map
         ;; (map-value map key) returns two values: the value associated with the
         ;; key and t to indicate that the value was found. map-value returns just
         ;; nil if there is no value associated with key.
         (multiple-value-setq (val disk) (map-value map key))
       (values val disk))
     (t
      (mformat "Partition ~S of database ~A cannot be found." 
               area (pathname-name (slot-value *allegrocache* 'db.ac::name )))
      (values nil nil "*database partition not found*"))))
  )

;;;(defUn db-load (key area)
;;;  "load an object from the database. The partition is the package of the key.
;;;Argument:
;;;   key: a symbol, the package of which is the name of the database partition
;;;   area: partition
;;;Return:
;;;   the value of key as stored in the database or nil."
;;;  (declare (special *allegrocache*))
;;;  
;;;  (unless (and key (symbolp key))
;;;    (error "key ~S must be a non nil symbol." key))
;;;  
;;;  ;; compute a key that will be used instead of symbols for storing data to avoid
;;;  ;; the package problem
;;;  ;; if not already a keyword, cook up a keyword starting with @
;;;  (if (and (symbolp key) (not (eql (symbol-package key) (find-package :keyword))))
;;;      (setq key (intern (concatenate 'string "@" (symbol-name key)) :keyword)))
;;;
;;;  (let ((map (db-area-exists? area)))
;;;    (if map
;;;        ;; (map-value map key) returns two values: the value associated with the
;;;        ;; key and t to indicate that the value was found. map-value returns just
;;;        ;; nil if there is no value associated with key.
;;;      (values (map-value map key) "*loaded from database*")
;;;      (progn
;;;        (mformat "Partition ~S of database ~A cannot be found." 
;;;                 area (pathname-name (slot-value *allegrocache* 'db.ac::name )))
;;;        (values nil "*database partition not found*"))))
;;;  )

#|
ACL
===
? (db-load 'K001 :address)
"test K001"
T
? (db-load 'K001 :moss)
Partition :MOSS of database ACL-MOSS-ONTOLOGIES2 cannot be found.
NIL
? (db-load 'K004 :address)
NIL
|#
;;;----------------------------------------------------------------------- DB-OPEN

(defUn db-open (&key name directory db-pathname)
  "opens the persistent file, not supposed to fail. 
Arguments:
   name (key): a string to build the actual name, e.g. \"NEWS\"
   directory (key): the location of the objectstore
   db-pathname (key): the pathname if already known
Return:
   pheap for MCL, *allegrocache* for ACL."
  #+MICROSOFT-32
  (declare (special *allegrocache*))
  #+MCL
  (declare (special *woodpheap*))
  
  ;; get db-pathname
  (setq db-pathname (or db-pathname 
                        (db-compute-pathname :name name :directory directory)))
  ;; if opened, quit
  (when (db-opened? db-pathname)
    (mformat "~%;*** ~A database already opened ***" (pathname-name db-pathname))
    ;;********** returning db-handle for MCL is inconsistent since it returns 
    ;; *database-pathname* instead of *woodpheap* **********
    (return-from db-open (db-handle)))
  
  ;; if does not exist, error
  (unless (db-exists? db-pathname)
    (error "database does not exist."))
  ;; if another base is opened, close it
  (if *database-pathname* (db-close))
  
  ;; filename must be the pathname of a directory, so we convert the file-pathname
  ;; into a directory pathname
  (setq *database-pathname* db-pathname)
  (open-file-database db-pathname)
  *allegrocache*
  
  )

#|
? (setq *f* 
     (merge-pathnames 
             (concatenate 'string ":applications:" "ONTOLOGIES.odb")
             moss::*moss-directory-pathname*))
#P"Odin:Users:barthes:MCL:OMAS-MOSS 8.0:MOSS:applications:ONTOLOGIES.odb"

? (db-create "ONTOLOGIES" :address)

? (db-open *f*)

? (db-open)
#<AllegroCache db "C:\\Program Files\\Allegro CL 8.2\\OMAS-MOSS 8.0\\MOSS\\applications\\MOSS-ONTOLOGIES"
  @ #x21879422>
? (db-close)
:done
|#
;;;------------------------------------------------------------------- DB-OPENED?

(defUn db-opened? (db-pathname)
  "checks if database opened.
Argument:
   db-pathname: as resulting from db-compute-pathname
Return:
   db-pathname if opened, NIL otherwise."
  (if (equal *database-pathname* db-pathname) db-pathname))

;;;------------------------------------------------------------------ DB-RESTORE
;;; Works only with ACL

(defUn db-restore (dump-file-pathname db-pathname area)
  "restores a database partition by reloading it. Database must exist but ~
   partition should be empty. If partition is not empty, ask user to clear it.
Arguments:
   dump-file-pathname: file from which we want to restore
   db-pathname: db-pathname
   area: name of the partition to initialize in the database
Return:
   handle :done or NIL"
  
  ;; if no partition name not a keyword complain
  (unless (keywordp area)
    (error "area argument: ~S should be a keyword." area))
  
  ;;=== check that dumpfile exists and ask user to confirm
  (unless (probe-file dump-file-pathname)
    (error "Bad dump-file pathname: ~S" dump-file-pathname))
  
  ;;=== check if database exists
  (unless (db-exists? db-pathname)
    (error "Database ~S cannot be found." db-pathname)
    )
  
  ;; ask user to confirm reloading operation
  (unless
      (y-or-n-p 
       "Are you sure you want to reload partition ~S into ~S from:~%~S"
       area (pathname-name db-pathname) (pathname-name dump-file-pathname))
    (return-from db-restore nil))
  
  ;; open database
  (unless (db-opened? db-pathname)
    (db-open :db-pathname db-pathname))

  ;; check if area is empty
  (unless (db-area-empty? area)
    ;; no, ask user to clear it
    (if (y-or-n-p "Are you sure you want to clear partition ~S?" area)
        ;; yes, OK, clear it. Warning, we kill the partition...
        (db-clear-all area)
      ;; no return
      (return-from db-restore nil)))
  
  ;; check if partition exists
  (unless (db-area-exists? area)
    ;; no, create it
    (db-extend area))
  
  ;;=== load partition
  ;; loop on the dump file to reload the data base
  (let ((*package* (find-package area))
         pair)
    (with-open-file (ss dump-file-pathname :direction :input 
                        :external-format :utf-8)
      (loop
        (setq pair (read ss nil nil))
        (when (or (null pair)(eql pair :eof)) (return))
        (if (listp pair)
            (db-store (car pair)(cdr pair) area :no-commit t)
          (warn "+++ something wrong, pair is not a list: ~S" pair))
        ))
    ;; commit
    (db-commit)
    )
  ;; return
  :done)

#|
(db-restore "financing-121126.dmp" 
            :name "SERVER" 
            :directory (omas-application-directory *omas)
            :area :FINANCING)
|#
;;;*---------------------------------------------------------------- DB-SAVE-ALL
;;; executed in the ontology package

(defUn db-save-all ()
  "save the ontology and knowledge base into an object database. Normally, used ~
   after loading the ontology from a text file. This file should not exist.
   Used by MOSS in its standalone version.
Argument:
   none
Return:
   :done"
  (let (database-pathname answer message package-key)
    
    ;; ask for confirmation of the database name
    (setq answer
          (#+MCL y-or-n-dialog #+MICROSOFT-32 y-or-n-p
           (format nil "Is \"MOSS-ONTOLOGIES\" the right database name?")))
    (cond
     ;; if no look for another file
     ((null answer) 
      (setq database-pathname #+MCL (choose-file-dialog)
            #+MICROSOFT-32 (cg:ask-user-for-directory)))
     ;; if yes, cook up pathname, ACL-MOSS-ONTOLOGIES-ADB (folder) for ACL
     ;; or MCL-MOSS-ONTOLOGIES.odb (file) for MCL
     ((eq answer T) 
      (setq database-pathname (db-compute-pathname))))
    
    ;; we assume this is a cancel order
    (unless database-pathname
      (cg:beep)
      (mformat "~%Giving up saving world...")
      (return-from db-save-all :cancelled))
    
    ;; complain if *application-package* not defined; better to do it before testing
    ;; existence. Used in db-save-all-into-new-partition
    (unless *application-package*
      (cg:beep)
      (mformat 
       "~%Ontology package (*application-package*) should be defined...")
      (return-from db-save-all :error))
    
    ;; when the file exists, we may create a new partition or reinitialize it
    (when (probe-file database-pathname)
      (cg:beep)
      (mformat "~%Database file already exists...~%  ~S" database-pathname)
      (return-from db-save-all (db-save-all-into-new-partition database-pathname)))
    
    ;; create the database
    (mformat "~%Creating database:~%  ~S" database-pathname)
    
    ;; cook up package key
    (setq package-key (intern (package-name *application-package*) :keyword))
    
    (db-create :db-pathname database-pathname :area package-key)
    
    (with-package *application-package*
      ;; save application, if error then we catch a string error message
      (setq message 
            (catch :error 
              (send (symbol-value (intern "*MOSS-SYSTEM*" *application-package*))
                    '=save)
              nil))
      ;; report what happened
      (if (stringp message)
        (mformat message)
        (mformat ";*** application saved, database still opened ***"))
      ))
  :done)

;;;------------------------------------------------ DB-SAVE-ALL-INTO-NEW-PARTITION

(defUn db-save-all-into-new-partition (database-pathname)
  "we want to save environment into an existing database, in the partition ~
   corresponding to the current package. If it already exists, we ask user if it ~
   must be reinitialized.
Arguments:
   database-pathname: pathname of the current database
Return:
   nil in case of failure, :done otherwise."
  (unless (db-open :db-pathname database-pathname)
    (return-from db-save-all-into-new-partition))
  
  (let ((area (intern (package-name *application-package*) :keyword))
        answer message)
    ;; check if partition exists
    (when (db-area-exists? area)
      (setq answer
            (#+MCL y-or-n-dialog #+MICROSOFT-32 y-or-n-p
             (format nil "Partition ~S already exists. Do you want to erase it?"
                     area)))
      ;; if no, get out of here
      (unless answer
        (return-from db-save-all-into-new-partition))
      ;; otherwise erase partition
      (db-clear-all area)
      )
    
    ;; here database is open, but area does not exist or was erased. (Re)create area
    (db-extend area)
    ;; save world
    (with-package *application-package*
      ;; save application, if error then we catch a string error message
      (setq message 
            (catch :error 
              (send (symbol-value (intern "*MOSS-SYSTEM*" *application-package*))
                    '=save)
              nil))
      ;; report what happened
      (if (stringp message)
        (mformat message)
        (mformat ";*** application saved, database still opened ***"))
      )
    ;; and exit
    :done))

;;;------------------------------------------------------------- DB-SHOW-STRUCTURE

(defUn db-show-structure ()
  "displays the structure of the currently opened database (partitions).
Argument:
   none
Return:
   :done"
  (declare (special *allegrocache*))
  
  (when (database-open-p *allegrocache*)
    (mformat "~%===== Structure of the ~A database:" 
             (pathname-name (slot-value *allegrocache* 'db.ac::name)))
    (doclass (item 'ac-map-range)
             (mformat "~%  ~S" (ac-map-name item)))
    (mformat "~%=====")
    )
  
  :done)

#|
? (db-show-structure)
===== Structure of the "ONTOLOGIES" database: 
  :ADDRESS
=====
:DONE

? (db-extend "ONTOLOGIES" :project)
;*** ONTOLOGIES database opened ***
#<IO WOOD:PHEAP to
#P"Odin:Users:barthes:MCL:OMAS-MOSS 8.0:MOSS:applications:ONTOLOGIES.odb">

? (db-show-structure)
===== Structure of the "ONTOLOGIES" database: 
  :ADDRESS
  :PROJECT
=====
:DONE
|#
;;;---------------------------------------------------------- DB-START-TRANSACTION

(defUn db-start-transaction ()
  "starts a transaction when we want to modify a set of objects and keep the database ~
   consistent."
  (unless *database-pathname*
    (error "no database opened"))
  
  :to-do
  )

;;;---------------------------------------------------------------------- DB-STORE
;;; we may have problems with keys that are symbols inherited from different
;;; packages. We assume that we want to store elements from the current package
;;; therefore, we transform symbols into keys, mapping them with a starting "@"
;;; e.g. moss::keyword -> :@keyword This is to avoid possible conflicts that may
;;; occur with values recorded with a bona fide keyword

(defUn db-store (key value area &key no-commit)
  "store a key and associated value in the partition area (normally the symbol ~
   package of the key). Commits the result unless no-commit is true. We do not ~
   allow a NIL key.
Aruments:
   key: a symbol
   value: any expr
   area: partition name, a keyword
   no-commit (key): if t don't commit
Return:
   the key"
  
  (unless (and key (symbolp key))
    (error "db-store(~S): key ~S must be a non nil symbol." area key))
  
  ;; compute a key that will be used instead of symbols for storing data to avoid
  ;; the package problem
  (if (and (symbolp key) (not (eql (symbol-package key) (find-package :keyword))))
      (setq key (intern (concatenate 'string "@" (symbol-name key)) :keyword)))

  (let ((map (db-area-exists? area)))
    (unless map
      (warn "trying to save object into an unexisting partition: ~S" area)
      (return-from db-store))
    (when map
      (setf (map-value map key) value)
      (unless no-commit (commit))
      key))
  )

#|
(moss::db-store 'k001 "test K001" :address)
(moss::db-store 'k002 "test K002" :address)
(moss::db-store 'k003 "test K003" :address)

? (defpackage :address (:use moss :cl :ccl))
? (db-store 'address::key-1 "value of key-1")
ADDRESS::KEY-1
? (db-store 'address::key-2 '(value of key-1))
ADDRESS::KEY-2
? (db-store 'key-3 '(value of key-1))

? (db-store (intern "=IF-NEEDED" :address) 34 :package (find-package :address))
|#	
;;;---------------------------------------------------------------------- DB-VOMIT

(defUn db-vomit (area &key print-values dump)
  "prints the entire  content of a database partition (for checkpointing). Uses ~
   *moss-output* as the output stream.
Arguments:
   area: a keyword specifying the partition to be dumped, e.g. :PUBLISHER
   print-values (key): if t prints the value along with the key
   dump (key): if non nil, dumps as pointed pairs
Return:
   :done"
  (unless (keywordp area)
    (error "~%area arg to db-vomit ~S should be a keyword" area))
  
  (unless *database-pathname*
    (warn ";*** No database opened...")
    (return-from db-vomit))

  (let ((map (retrieve-from-index 'ac-map-range 'ac-map-name area)))
    (cond
     ;; dumping amount to print pairs 
     (dump
      (map-map #'(lambda (xx yy) (mformat "~%(~S . ~S)"  xx yy)) map))
     ;; print key and value for checking purposes
     (print-values
            (map-map #'(lambda (xx yy) 
                     (mformat "~% === ~S : ~S"  xx yy))
                     map))
     ;; print only the list of keys
     (t
      (map-map #'(lambda (xx yy) (declare (ignore yy))
                   (mformat "~% === ~S" xx)) map))
     )
    :done)
  )
#|
ACL
===
? (db-vomit :address)
 === :@K001
 === :@K002
 === :@K003
:DONE
? (db-vomit :address :print-values t)
 === :@K001 : "test K001"
 === :@K002 : "test K002"
 === :@K003 : "test K003"
:DONE

MCL
===
?
|#
;;;============================== tests ==========================================
;;; A special test file is available moss-persistency-test.lisp

(format t "~%;*** MOSS v~A - persistency loaded ***" *moss-version-number*)

;;; :EOF
