﻿;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;===============================================================================
;;;14/01/28
;;;            W - L A B E L S  (file W-labels.lisp)
;;;
;;;===============================================================================

#| This file should contains all the strings that must be displayed in the MOSS 
windows for the legal languages. 
However, for the time being all system windows use English.

2014
 0306 creation
2020
 0129 organizing it as an asdf system in a git repository
|#

(eval-when (compile load eval)
  (unless (find-package :omas) (make-package :moss))
  )

(in-package :moss)

;;;===============================================================================
;;;
;;;                             OVERVIEW WINDOW
;;;
;;;===============================================================================

;;; Agent window is in English

(defVar *wovr-internal-format* 
    '((:en "Internal format") 
      (:fr "Format interne")))
(defVar *wovr-show-concept* 
    '((:en "Show concept") 
      (:fr "Afficher concept")))
(defVar *wovr-make-instance* 
    '((:en "MK INST") 
      (:fr "CR INST")))
(defVar *wovr-edit*
    '((:en "EDIT") 
      (:fr "ÉDITER")))
(defVar *wovr-context* 
    '((:en "CONTEXT") 
      (:fr "CONTEXTE")))


(format t "~%;*** MOSS v~A - window labels loaded ***" moss::*moss-version-number*)
