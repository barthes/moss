;;;=================================================================================
;;;20/01/29
;;;		
;;;		U T I L S - (File utils.lisp)
;;;	
;;;=================================================================================
#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
;;;=================================================================================

;;; File defining some debug/service functions

#|
History
2020
 0129 Creation (imported from part of moss-macros.lisp)
|#

(in-package :moss)

;;;==========================================================================
;;; service functions after macros

;;;------------------------------------------------------------------- ALISTP

(defUn alistp (ll)
  "test if ard is an a-list."
  (and (listp ll)(every #'listp ll)))

#|
(alistp nil)
T
(alistp '((1 A)(B 2) ("C" 3)))
T
(alistp '((1 A)(B 2) ("C" 3) D))
NIL
(alistp 22)
NIL
|#
;;;---------------------------------------------------------------- ALIST-ADD

(defun alist-add (ll tag val)
  "basic function for adding a single value to an a-list.
Arguments:
   ll: a-list to modify
   tag: any lisp expr
   values: any lisp expr
Return:
   the modified a-list."
  (cond
   ((null ll) `((,tag ,val)))
   ((equal+ (caar ll) tag) 
    (cons (cons (caar ll) (append (cdar ll)(list val))) (cdr ll)))
   (t (cons (car ll)(alist-add (cdr ll) tag val)))))

#|
(alist-add '((1 A)(B 2) ("C" 3))  "c" "cc")
((1 A) (B 2) ("C" 3 "cc"))
(alist-add '((1 A)(B 2) ("C" 3))  'd "dd")
((1 A) (B 2) ("C" 3) (D "dd"))
(alist-add '((1 A)(B 2) ("C" 3))  1 'AA)
((1 A AA) (B 2) ("C" 3))
|#
;;;--------------------------------------------------------- ALIST-ADD-VALUES

(defun alist-add-values (ll tag values)
  "basic function for adding a list of values to an a-list.
Arguments:
   ll: a-list to modify
   tag: any lisp expr
   values: a list of any lisp expr
Return:
   the modified a-list."
  (cond
   ((null ll) (list (cons tag values)))
   ((equal+ (caar ll) tag) 
    (cons (cons (caar ll) (append (cdar ll) values)) (cdr ll)))
   (t (cons (car ll)(alist-add-values (cdr ll) tag values)))))

#|
(alist-add-values '((1 A)(B 2) ("C" 3))  "c" '("cc" ccc))
((1 A) (B 2) ("C" 3 "cc" CCC))
(alist-add-values '((1 A)(B 2) ("C" 3))  'd '("dd" dd))
((1 A) (B 2) ("C" 3) (D "dd" DD))
(alist-add-values '((1 A)(B 2) ("C" 3))  1 '())
((1 A AA) (B 2) ("C" 3))
(alist-add-values nil :a '(1 2))
((:A 1 2))
|#
;;;A--------------------------------------------------------------- ALIST-REM

(defun alist-rem (ll tag)
  (remove tag ll :key #'car :test #'equal+))

#|
(alist-rem '((1 A)(B 2) ("C" 3))  "c")
((1 A)(B 2))

(alist-rem '((1 A)(B 2) ("C" 3))  1)
((B 2) ("C" 3))
|#
;;;A----------------------------------------------------------- ALIST-REM-VAL

(defun alist-rem-val (ll tag val &aux temp)
  (cond
   ((null ll) nil)
   ((and (equal+ (caar ll) tag)(member val (cdar ll) :test #'equal+))
    (setq temp (remove val (cdar ll) :test #'equal+))
    (if temp (cons (cons (caar ll) temp)(cdr ll))
      (cdr ll)))
   (t (cons (car ll) (alist-rem-val (cdr ll) tag val)))))
    
#|
(alist-rem-val '((1 A)(B 2) ("C" 3))  "c" 3)
((1 A) (B 2))

(alist-rem-val '((1 A)(B 2) ("C" 3))  "c" 4)
((1 A) (B 2) ("C" 3))

(alist-rem-val '((1 A)(B "b" 2 22) ("C" 3))  'b "b")
((1 A) (B 2 22) ("C" 3))
|#
;;;A--------------------------------------------------------------- ALIST-SET

(defun alist-set (ll tag val) 
  "replaces value associated to a specific property in an a-list"
  (cond ((null ll) (list (list tag val)))
        ((equal+ tag (caar ll))
         (cons (list tag val)(cdr ll)))
        (t (cons (car ll) (alist-set (cdr ll) tag val)))))

#|
(alist-set '((1 A)(B 2) ("C" 3))  "c" 333)
((1 A) (B 2) ("c" 333))

(alist-set '((1 A)(B 2) ("C" 3))  :d 4)
((1 A) (B 2) ("C" 3) (:D 4))

(alist-set '((1 A)(B 2) ("C" 3))  1 :A)
((1 :A) (B 2) ("C" 3))

(alist-set nil  :d 4)
((:D 4))
|#
;;;A-------------------------------------------------------- ALIST-SET-VALUES

(defun alist-set-values (ll tag values) 
  "replaces value associated to a specific property in an a-list"
  (cond ((null ll) (list (cons tag values)))
        ((equal+ tag (caar ll))
         (cons (cons tag values)(cdr ll)))
        (t (cons (car ll) (alist-set-values (cdr ll) tag values)))))

#|
(alist-set-values '((1 A)(B 2) ("C" 3))  "c" '(333 3333))
((1 A) (B 2) ("c" 333 3333))

(alist-set-values '((1 A)(B 2) ("C" 3))  :d '(4))
((1 A) (B 2) ("C" 3) (:D 4))

(alist-set-values '((1 A)(B 2) ("C" 3))  1 '(:A :aa))
((1 :A :AA) (B 2) ("C" 3))

(alist-set-values nil  :d '(4 "dd"))
((:D 4 "dd"))

(alist-set-values'((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("cc" 3) ("dd" 4) ("ee" 5)))
                   'b '(("cc" 3) ("ee" 5)))
((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("cc" 3) ("ee" 5)))
|#
;;;A-------------------------------------------------------------- ALL-ALIKE?

(defUn all-alike? (ll)
  "T if all elements of the list are the same. Uses equal-gen to take care of 
   strings."
  ;; or less than2 args
  (or (or (null ll) (null (cdr ll)))
      ;; if 2 args or more should be equal
      (and (equal-gen (car ll) (cadr ll)) (all-alike? (cdr ll)))))

#|
(all-alike? ())
T

(all-alike? '(1))
T

(all-alike? '(1 2))
NIL

(all-alike? '("a" "a" "A"))
NIL

(all-alike? '("a" "a" 1))
NIL

(all-alike? '("a" "a" "a"))
T
|#
;;;A---------------------------------------------------------- ALL-DIFFERENT?

(defUn all-different? (ll)
  "T if all elements of the list are different"
  ;; should have at least 2 args
  (cond
   ((or (null ll)(null (cdr ll))))
   ;; more than 2 args
   ((member-gen (car ll) (cdr ll)) nil)
   ((null (cddr ll)) t)
   ((all-different? (cdr ll)))))

#|
(all-different? '())
T

(all-different? '(1 1))
NIL

(all-different? '(1 2))
T

(all-different? '(1 2 1))
NIL

(all-different? '(1 2 3))
T
|#
;;;------------------------------------------------------------------ DFORMAT

(defun dformat (tag level &rest ll)
  "allows selective tracing by inserting a keyword tag into the (debugging *omas*)~
   list of debugging tags (similar to *features*).
Arguments:
   tags: a tag or list of tags for inserting a format
   level: a level, the higher the more detailed (default is 0)
Return:
   nil"
  (declare (special *debug-io*))
  (if (and (get tag :trace)(<= level (get tag :trace-level)))
      (apply #'format *debug-io* ll))
  nil)

;;;------------------------------------------------------------ DFORMAT-RESET

(defun dformat-reset (tag)
  (setf (get tag :trace) nil)
  )

;;;-------------------------------------------------------------- DFORMAT-SET
;;; debugging function, needs to be in front

(defun dformat-set (tag level)
  (setf (get tag :trace) t)
  (setf (get tag :trace-level) level)
  )

;;;----------------------------------------------------------------- DMFORMAT
;;; deprecated use dformat with omas::omas-trace

(defun dmformat (tags level &rest ll)
  "dm for debug-moss allows selective tracing by inserting a keyword tag ~
   into the *debug-tags* list of debugging tags (similar to *features*).
Arguments:
   tags: a tag or list of tags for inserting a format
   level: a level, the higher the more detailed (default is 0)
Return:
   nil"
  (declare (special *debug-tags* *debug-level* *debug-io*))
  (let ()
    (cond
     ;; if tag-list is nil (no tags) quit
     ((null *debug-tags*))
     ((and (symbolp tags)(member tags *debug-tags*)
           (<= level *debug-level*))
      (apply #'format *debug-io* ll))
     ((and (listp tags)(intersection tags *debug-tags*)
           (<= level *debug-level*))
      (apply #'format *debug-io* ll))
     ))
  nil)

;;;----------------------------------------------------------------- DWFORMAT

(defun dwformat (tag level &rest ll)
  "allows selective tracing by inserting a keyword tag into the (debugging *omas*)~
   list of debugging tags (similar to *features*). Waits until each message for ~
   a character to be inputted to resume execution.
Arguments:
   tags: a tag or list of tags for inserting a format
   level: a level, the higher the more detailed (default is 0)
Return:
   nil"
  (declare (special *debug-io*))
  (when (and (get tag :trace)(<= level (get tag :trace-level)))
    (apply #'format *debug-io* ll)
    (print "Type any char to continue: " *debug-io*)
    (read-char *debug-io*))
  nil)

;;;---------------------------------------------------------------- EQUAL-GEN

(defun equal-gen (aa bb)
  "test if aa and bb are same, generalize to strings using string-equal"
  (if (and (stringp aa)(stringp bb))
      (string-equal aa bb)
    (equal aa bb)))

;;;A------------------------------------------------------------------ EQUAL+
;;; Extends equality to strings and MLNs (a string is an not an MLN)
;;; Two MLN are equal if
;;; - they are both strings and equal
;;; - if language L is specifies and not :all
;;;   - if one is a string and is present in the synonyms of the other in L
;;;   - if one is a string and is present in the other's unknown language
;;;   - if the two sets of synonyms for L share some
;;;   - if the synonyms for L in one share those of :unknown 
;;;   - if the two sets for unknown share some synonyms
;;; - if language is :all
;;;   - if all the synonyms of any language for both share some
;;; - if language is nil
;;;   - if *language* is unbound or equal to :unknown (NOT TRUE)
;;;     - if both sets of synonyms for a language are intersecting
;;;     - if a set of synonyms for a language is intersecting with the unknown 
;;;       set of the other
;;;   - if *language* is :all (same as when language is :all) not realistic!
;;;   - if *language* is a legal language
;;;     - if both sets of synonyms for *language* are intersecting
;;;     - if a set of synonyms for *language* is intersecting with the unknown 
;;;       set of the other

(defun equal+ (aa bb &key language)
  "test if aa and bb are same, generalize to strings using string-equal and MLNs
Arguments:
   aa: first arg
   bb: second arg
   language: if aa and bb are MLNs restricts the language, unless it is :all"
  ;; take car of old format
  (if (mln::%mln? aa)(setq aa (mln::make-mln aa)))
  (if (mln::%mln? bb)(setq bb (mln::make-mln bb)))
  (cond
   ((and (stringp aa)(stringp bb))
    (string-equal aa bb))
   ((equal aa bb))
   ((and (or (stringp aa)(mln::mln? aa))(or (stringp bb)(mln::mln? bb)))
    (mln::mln-equal aa bb :language language)) ; jpb 1406
   ))

#|
(equal+ nil nil)
T
(equal+ 2 2)
T
(equal+ '(a b) '(a b))
T
(equal+ "albert" "ALBERT")
T
(equal+ 3 "D")
NIL
(equal+ nil nil)
T
(let ((*language* :fr))
 (equal+ "ALBERT" '(:en "George; Arthur" :fr "Jean; Albert")))
("Albert")

(let ((*language* :en))
  (equal+ "ALBERT" '(:en "George; Arthur" :fr "Jean; Albert")))
NIL

(let ((*language* :en))
  (equal+ "ALBERT" '(:en "George; Arthur" :fr "Jean; Albert")
          :language :all))
("ALBERT")

(let ((*language* :en))
  (equal+ "ALBERT" '(:en "George; Arthur" :fr "Jean; Albert")
          :language :fr))
("ALBERT")

(let ((*language* :en))
  (equal+ '(:en "Allan" :fr "albert") '(:en "George; Arthur" :fr "Jean; Albert")
          :language :en))
T ; language context does not matter for comparing 2 mlns
|#
;;;----------------------------------------------------------- INTERSECT-SETS
;;; same as a function taking a list

(defUn intersect-sets (list)
  (if (null (cdr list)) (car list)
      (intersection (car list)(intersect-sets (cdr list)))))

;;;------------------------------------------------------------------ LAMBDAP
;;; Used to check for bounded method function names

(defUn lambdap (symbol)
  "check if symbol is a lambda list"
  (and (symbolp symbol)
       (boundp symbol)
       (listp (symbol-value symbol))
       (eql (car (symbol-value symbol)) 'lambda)
       t))

#|
(setq ff '(lambda (xx) (list xx)))
(LAMBDA (XX) (LIST XX))

(lambdap 'ff)
T

(lambdap 'tt)
NIL
|#
;;;------------------------------------------------------------------ LOB-GET
;;; lob-get is defined as a version of getv that checks for the nature of the
;;; object. It eventually can load objects from the secondary storage

(defUn lob-get (obj-id prop-id)
  (when (lob-typep obj-id)
    (getv prop-id (symbol-value obj-id))))

;;;---------------------------------------------------------------- LOB-TYPEP
;;; checks if the object is bound and its value is PDM, NIL will return nil

(defUn lob-typep (xx) (and 
                       (symbolp xx)
                       (boundp xx)
                       (alistp (symbol-value xx))
                       (assoc '$TYPE (symbol-value xx))))

;;;A----------------------------------------------------------------- MEMBER+

(defun member+ (xx ll)
  (member xx ll :test #'equal+))

#|
(member+ "a" '(& 2 C "A" d v "e" 23))
("A" D V "e" 23)
|#
;;;A-------------------------------------------------------------- MEMBER-GEN
;;; generalize member to strings using string-equal test otherwise uses equal

(defun member-gen (xx ll)
  (if (and (stringp xx)(every #'stringp ll))
      (member xx ll :test #'string-equal)
    (member xx ll :test #'equal)))

;;;--------------------------------------------------------------------- MEXP
;;; mexp expands completely an s-expr to the last bits. It is useful for
;;; checking the code that will be produced. E.g. using incf is not a good
;;; idea, since it expands to a let* and creates a temporary variable!

(defUn mexp (form)
  (if form (if (listp form)
             (macroexpand (cons (mexp (car form))(mexp (cdr form))))form)))

;;; The following functions are used to make entry-points for the various 
;;; object types of MOSS

;;;A--------------------------------------------------------------- MAKE-NAME

;;; make-name is defined for compatibility with UTC-Lisp; It creates a string
;;; uppercasing the names (NO). It takes any number of arguments.

(defUn make-name (&rest arg-list)
  (format nil "~{~A~}" arg-list))

#|
? (make-name 1. 'albert "a" 23 " ans ? Paris")
"1ALBERTa23 ans ? Paris"
|#

;;; next line can be used to print something like (aa 47 bb 47 cc)) where
;;; *string-hyphen* is 47 "/" i.e. is non nil. => AA/BB/CC
;;; should not use mapcan (buggy)
;;; (butlast (mapcan #'(lambda (xx) (list xx *string-hyphen*))))

#|
(format nil "~{~S~#[~;~:;~C~]~}" '(aa  47 bb  47 cc ))
"AA/BB/CC"
(apply #'make-name '(aa  47 bb  47 cc ))
"AA47BB47CC"
|#
;;;-------------------------------------------------------- MAKE-VALUE-STRING
;;; make-value-string is currently the same as make-entry-point but returns
;;; a string rather than an interned symbol. However, the make-entry-point
;;; could use an intermark strategy for example,
;;;    " Le  vilain petit   canard    "  => VILPC
;;; this is a sort of hash code with possible collisions however.

;;;********** JPB1507 I don't touch this function but it does not do what it
;;; advertises!

(defUn make-value-string (value &optional (interchar '#\.))
  "Takes an input string, removing all leading and trailing blanks, replacing ~
   all substrings of blanks and simple quote with a single underscore, and ~
   building a new string capitalizing all letters. The function is used ~
   to normalize values for comparing a value with an external one while ~
   querying the database.  
      French accentuated letter are replaced with unaccentuated capitals."
  (let* ((input-string (if (stringp value) value
                           (format nil "~S" value)))
         (work-list (map 'list #'(lambda(x) x) 
                         ;; remove leading and trailing blanks
                         (string-trim '(#\space) input-string))))
    (string-upcase   ; we normalize entry-point to capital letters
     (map
      'string
      #'(lambda(x) x) 
      ;; this mapcon removes the blocks of useless spaces and uses
      ;; a translation table to replace French letters with the equivalent
      ;; unaccentuated capitals
      (mapcon 
       #'(lambda(x) 
           (cond 
            ;; if we have 2 successive blanks we remove one
            ((and (cdr x)(eq (car x) '#\space)
                  (eq (cadr x) '#\space)) 
             nil)
            ;; if we have a single blank we replace it with an underscore
            ((eq (car x) '#\space) (copy-list (list interchar)))
            ;; if the char is in the translation table we use the table
            ;((assoc (car x) *translation-table*) ; JPB0901
            ; (copy-list(cdr(assoc (car x) *translation-table*))))
            ;; otherwise we simply list the char
            (t (list(car x)))))
       work-list)))))

#|
(make-value-string " Le  vilain petit   canard    ")
"LE.VILAIN.PETIT.CANARD"

(make-value-string "un jour d'�t� d'aujourd'hui � O�gour")
"UN.JOUR.D'�T�.D'AUJOURD'HUI.�.O�GOUR"
;;;compare with

(%string-norm "un jour d' �t�  d'aujourd'hui �  O�gour")
"UN-JOUR-D'-�T�-D'AUJOURD'HUI-�-O�GOUR"
(intern *)
|UN-JOUR-D'-�T�-D'AUJOURD'HUI-�-O�GOUR|
|#
;;;--------------------------------------------------------------------- MEMQ

(defUn memq (xx ll) (member xx ll))

;;;------------------------------------------------------------------ MFORMAT

;;; printing in MOSS can be done in different ways.
;;; when MOSS is used in stand alone, then we have 2 cases:
;;;  - there is no MOSS window, we print into the listener or *debug-io* stream
;;;  - there is a MOSS wndow and we use the display-text method of the MOSS
;;;    window to print the text
;;; the mformat function is a function whose output depends on the value of 
;;; the global variable *moss-output*
;;; one problem however occurs when we want to print into another channel, e.g.
;;; when using OMAS into the assistant window. In that case we cannot use
;;; mformat but must use the conversation object.
;;; consequently all printing should go throgh the conversation object, which
;;; means that the conversation object should be initialized at the very beginning

(defUn mformat (control-string &rest args)
  "prints to the output specified by *moss-output*. Can be a stream or a pane.
   First time around, if *moss-output but moss-window exists, the output is ~
   redirected to moss-windowanswer pane.
Arguments:
   same as format except for stream
Returns:
   NIL, same as format."
  ;; when moss-window is up, it has priority over standard output
  (when (and (eq t *moss-output*)(cg:find-window :moss-window))
    (setq *moss-output* 
          (cg:find-component :answer-pane (cg:find-window :moss-window))))
  ;; redirect output as specified
  (if (typep *moss-output* 'cg:dialog-item)
    (let ((val (concatenate 'string 
                            (cg:value *moss-output*) 
                            (apply #'format nil "~?" control-string (list args)))))
      (setf (cg:value *moss-output*) val))
    (apply #'format *moss-output* control-string args)))

#|
(mformat "~%test ~S" 1)
test 1
NIL
(mformat "~%test ~S~%test ~S" 2 3)
test 2
test 3
NIL
|#
;;;A------------------------------------------------------------ MOSS-SYMBOL?

(defUn moss-symbol? (obj-id)
  "check whether symbol is defined in the moss package"
  (when (symbolp obj-id)
    (eql (symbol-package obj-id)(find-package :moss))))

#|
(moss-symbol? '$ENT)
$ENT
:INTERNAL

(moss-symbol? 'test::person)
NIL
|#
;;;A------------------------------------------------------- NALIST-REPLACE-PV
;;; Put here for compatibility with UTC-Lisp. The function replaces 
;;; destructively a sublist within an a-list

(defUn nalist-replace-pv (obj-id prop-id value &aux obj-l)
  (if (setq obj-l (symbol-value obj-id)) ; check if obj-id represents a list
    (if (assoc prop-id obj-l) ; check if property is there
      (setf (cdr (assoc prop-id obj-l)) value)
      ;; otherwise add the pair to the a-list
      (set obj-id (append obj-l (list (cons prop-id value)))))
    ;; otherwise create a new a-list
    (set obj-id (list (cons prop-id value)))) ;*** should be an error
  ;; return name of modified list
  obj-id)

#|
(setq ll '((A 1)(B 2) (C 3)))

(nalist-replace-pv 'll 'B '(5 6))
ll
((A 1)(B 5 6) (C 3))

(nalist-replace-pv 'll 'D '(5 6))
ll
((A 1)(B 5 6) (C 3)(D 5 6))

(setq ll nil)
(nalist-replace-pv 'll 'D '(5 6))
ll
((D 5 6))
|#
;;;A--------------------------------------------------------- LIST-DIFFERENCE

(defun list-difference (l1 l2 &optional strict)
  "remove from l1 all elements appearing in l2 preserving the order in l1"
  (cond
   ((null l2) l1)
   ((list-difference 
     (if strict 
         (remove (car l2) l1 :test #'equal+ :count 1)
       (remove (car l2) l1 :test #'equal+))
     (cdr l2)
     strict))))

#|
(list-difference nil nil)
NIL

(list-difference '(1 a "b" 2 3 4) nil)
(1 A "b" 2 3 4)

(list-difference '(1 a "b" 2 4 3 4) '(4))
(1 A "b" 2 3)

(list-difference '(1 a "b" 2 4 3 4) '(4 2 "b"))
(1 A 3)

(list-difference '(1 a "b" 2 4 3 4) '(1 a "b"))
(2 4 3 4)

(list-difference '(1 a "b" nil 3 4) '(4 5))
(1 A "b" NIL 3)

(list-difference '("a" "a" "a" 2 2) '("a" 2 3) :strict)
("a" "a" 2)
|#
;;;A-------------------------------------------------------------------- NO-P

(defUn no-p (word) 
  "Returns some non nil value if arg is noor non or some substring"
  (member word '(N NO NON NEIN NIET)))

;;;A------------------------------------------------------------------- NPUTV

(defUn nputv (prop-id value ll)
  "nputv is defined for compatibility with UTC-Lisp. It is a destructive function
   it adds a value to the list of values corresponding to a property without
   duplicating it"
  (if ll 
    (if (not (member value (assoc prop-id ll)))
      (nputv-last prop-id value ll))
    (list (list prop-id value))))

;;;A-------------------------------------------------------------- NPUTV-LAST
;;; nputv-last is defined for compatibility with UTC-Lisp. It adds a value
;;; to the list of values associated with a property in an a-list at the
;;; last position. Destructive function.
;;; (nputv-last 'a 3 '((a 1 2)(b 5 ))) -> ((a 1 2 3)(b 5 ))

(defUn nputv-last (prop val ll)
  "nputv-last is defined for compatibility with UTC-Lisp. It adds a value
   to the list of values associated with a property in an a-list at the
   last position. Destructive function."
  (if (assoc prop ll)
    (setf (cdr (last (assoc prop ll))) (list val))
    (if ll (setf (cdr (last ll)) (list (list prop val)))
        (error "*** Can't do an nputv-last on a null list")))
  ll)

;;;A---------------------------------------------------------------------- PP

(defun pp (&optional xx)
  (print (or xx *)) :done)

;;;---------------------------------------------------------------------- REF
;;; the following function traces in what function a specific symbol appears
;;; by examining the code of the function in the corresponding file.

;;;(defUn ref (symbol &optional show-file)
;;;  "traces in what function a specific symbol appears by examining the code of the ~
;;;   function in the corresponding file."
;;;  (declare (special *moss-directory-pathname*))
;;;  (let ((omas-prefix "moss-")
;;;        expr)
;;;    (dolist (file-name *moss-file-names*)
;;;      (let ((file-path
;;;             (make-pathname
;;;              :device (pathname-device *moss-directory-pathname*)
;;;              :host (pathname-host *moss-directory-pathname*)
;;;              :directory (append (pathname-directory *moss-directory-pathname*)
;;;                                 (list "MOSS source"))
;;;              :name (concatenate 'string omas-prefix file-name)
;;;              :type "lisp")))
;;;        (with-open-file 
;;;          (ss file-path)
;;;          ;; if wanted, we print the path of the file we are checking
;;;          (when show-file
;;;            (print file-path))
;;;          (loop
;;;            (setq expr (read ss  nil :end))
;;;            (when (eql :end expr) (return :done))
;;;            (when (and (listp expr)
;;;                       (member (car expr)
;;;                               '(defUn macro defmethod definstmethod defownmethod
;;;                                 defuniversalmethod))
;;;                       (appears-in? symbol expr))
;;;              (print (cadr expr)))))))
;;;    :done))

(defUn appears-in? (symbol expr)
  (cond ((null expr) nil)
        ((equal symbol expr) t)
        ((listp expr)
         (or (appears-in? symbol (car expr))
             (appears-in? symbol (cdr expr))))))

;;;A------------------------------------------------------------------- YES-P

(defUn yes-p (word) 
  "Returns some non nil value if arg is yes or oui or some substring"
  (if (stringp word)
    (member (char (string-upcase (string-trim '(#\space) word)) 0) 
            '(#\Y #\O #\J #\D))
    (member word '(Y YE YES O OU OUI JA DA))))


(format t "~%;*** MOSS v~A - Utilities loaded ***" moss::*moss-version-number*)

;;; :EOF