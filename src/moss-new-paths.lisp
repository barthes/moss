﻿;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;07/10/16
;;;		
;;;		P A T H S - (File MOSS-PATHS.LISP)
;;;		Copyright, JP Barthes Copyright@UTC 07
;;;	
;;; First created September 2007 
;;;=======================================================================
;;;	The file contains functions to build a formal query from a list of
;;;     words.
;;;
;;; TO DO
;;; - take into account attributes and associated values in the list of words
;;;   (should be done as a pre processing of the sentence)
;;; - take into account operators when dealing with attribute value info
;;; - take into account conjonctons like ET or OU
;;;   (id.)
;;; 

;;; The goal of this package is to build a formal path using the semantic net of
;;; the ontology and starting with a list of words.
;;; First the words are transformed into class-ids, relation-ids, or entry-points.
;;; Thus, we obtain an ordered list of places (nodes, relations) that the path
;;; must traverse to obtain the formal query.
;;; Entry points have a different role: they contain a terminal property an 
;;; operator, and the class to which it may apply. If the class is missing from 
;;; the original marked items, then it can be recovered from the entry point frame.
;;; 
;;; Second, we solve special cases directly. E.g. when there is a single item, or
;;; a single entry point frame...
;;;
;;; The main algorithm is to build paths starting from a specific class, the 
;;; target, that will go through all the items of the ordered list.
;;; We proceed by starting with the target class, trying to merge elements of 
;;; the item list. This will work if the class of a class-frame is a subclass of
;;; the target class or if the target class blongs to the predecessors or the 
;;; successors of the link (relation of inverse-relation).

;;; In a second stage we do the same with all the successors or predecessor of 
;;; the target class.
;;; We then continue until we have exhausted all the elements of the item list.
;;;
;;; To avoid fanning, we limit the possible length of a path, quitting when
;;; it is reached.
;;;
;;; Path format
;;;    ({<class frame>+ <link>}) 
;;;  where
;;;   <class-frame>::=(<class-id> <filter>*)
;;;   <link>::=<relation-id>|<inverse-relation-id>
;;; example
;;;   (($E-UNIVERSITY ($T-ORGANIZATION-NAME :IS "UTC")) 
;;;             $S-PRESIDENT-OF ($E-PERSON))


#|
2007
 0910 Creation
 0916 transfer to ACL
 0930 revisiting the whole approach
|#

(in-package "MOSS")


;;;======================= Macros and functions ===============================
;;; We define user names for various macros

(defun %class-and-subclasses (class)
  (%sp-gamma class (%inverse-property-id '$IS-A)))

;;;------------------------------------------- %%GET-ALL-CLASS-INVERSE-RELATIONS

(defun %%get-all-class-inverse-relations (class-id &key (context *context*))
  "Collects all relations starting with class-id through superclasses.
   Relations of the same property tree can be included in the result.
Arguments:
   class-id: identifier of the class
   context (opt): context (default current)
Return:
   a list of class attributes." 
  (mapcar #'%inverse-property-id
          (delete-duplicates
           (reduce #'append 
                   (mapcar #'(lambda(xx) (%get-value xx '$SUC.OF :context context))
                           (%sp-gamma class-id '$IS-A :context context))))))

;;;----------------------------------------------------------- FIND-BEST-ENTRIES

(defun find-best-entries (word-list &key (max-count 5) keep-ref)
  "tries to extract from the sentence entry points by combining the maximum ~
      number of words. When a phrase is extracted, the sentence is broken into ~
      fragments, and each fragment is processed in turn.
Arguments:
   word-list: list of words corresponding to the initial sentence
   max-count (key): maximal number of words to be put together to form a phrase
Return:
   list of entry points of nil if none."
  (when word-list
    (let* ((index  (moss::generate-access-patterns word-list :max max-count))
           (patterns (mapcar #'car index))
           ;; make a list of =make-entry methods that define entry points
           (make-entry-method-list (send '=make-entry '=get-id 'moss::$MNAM.OF))
           ep entry-point-list prop-list fragment fragment-list start end)
      ;(print index)
      ;; cook up a list of properties corresponding to the list of the =make-entry 
      (setq prop-list 
            (remove 
             nil
             (mapcar #'car 
                     (broadcast make-entry-method-list '=get-id 'moss::$OMS.OF))))
      
      (catch :done
        ;; test each pattern starting with the longest one
        (dolist (pattern patterns)
          ;; for each property in turn
          (dolist (prop prop-list)
            (setq ep (car (moss::-> prop '=make-entry pattern)))
            (moss::vformat "...trying ~S as an entry-point for ~S...ep? ~S" 
                           ep prop (moss::%entry? ep))
            (when (moss::%entry? ep) 
              ;(print (list "ep" ep "pattern" pattern))
              (pushnew (if keep-ref (list ep (%make-word-list pattern)) ep) 
                       entry-point-list)
              ;; record start and end of pattern in sentence fragment
              (setq fragment (assoc pattern index :test #'string-equal)
                    start (cadr fragment)
                    end (caddr fragment))
              (throw :done nil)))))
      ;; remove pattern from current list, breaking list into fragments
      ;(print (list "word-list" word-list "start end" start end))
      (when (and start end)
        (setq fragment-list (list (subseq word-list 0 start)
                                  (subseq word-list end (length word-list))))
        ;(print (list "sentence" fragment-list))
        ;; recurse on each fragment, preserving the order
        (setq entry-point-list
              (append
               (find-best-entries (car fragment-list) :keep-ref keep-ref)
               entry-point-list
               (find-best-entries (cadr fragment-list) :keep-ref keep-ref)))
        )
      entry-point-list)))

#| 
===== MCL
? (moss::find-best-entries '("quel" "est" "le" "numro" "de" "tlphone" "du" 
                             "prsident" "de" "l" "utc"))
(HAS-NUMERO TELEPHONE HAS-PRESIDENT UTC)

? (moss::find-best-entries '("quel" "est" "l" "adresse" "prive" "du" "prsident" 
                             "de" "l" "utc"))
(ADRESSE-PRIVEE HAS-PRESIDENT UTC)

? (moss::find-best-entries '("quel" "est" "le" "numro" "de"  "portable" "du" 
                             "prsident" "de" "l" "utc"))
(HAS-NUMERO PORTABLE HAS-PRESIDENT UTC)

? (moss::path-process-word-list '("tlphone" 
    "dominique" "barthes" "biesel"))
($E-PHONE ($E-PERSON ($T-PERSON-NAME :IS "Barthes" "Biesel")))

===== ACL
SA-ADDRESS(9): (moss::find-best-entries 
                '("quel" "est" "le" "numéro" "de" "téléphone" "du" 
                             "président" "de" "l" "utc"))
(HAS-NUMERO TELEPHONE HAS-PRESIDENT UTC)

SA-ADDRESS(10): (moss::find-best-entries 
                 '("quel" "est" "l" "adresse" "privée" "du" "président" 
                             "de" "l" "utc"))
(ADRESSE-PRIVEE HAS-PRESIDENT UTC)

SA-ADDRESS(11): (moss::find-best-entries 
                 '("quel" "est" "le" "numéro" "de"  "portable" "du" 
                             "président" "de" "l" "utc"))
(HAS-NUMERO PORTABLE HAS-PRESIDENT UTC)
|#
#|
;;;----------------------------------------------------------- %IS-PATH-CONCEPT?

(defun %is-path-concept? (expr)
  "predicate to locate a concept in a path. A concept or class-frame is a list
   starting with the concept-id."
  (and (listp expr)
       (%is-concept? (car expr))))

;;;=============================================================================
;;;
;;;                            Functions
;;;
;;;=============================================================================

;;; to build the formal query we start from the target class, and follow the 
;;; links using the structural properties until we hit all the classes correspond-
;;; ing to the entry-points.
;;; Starting with the target class we proceed to the next element of the access 
;;; list. It may be a milestone (class), a bridge (relation), an attribute or an 
;;; entry point.
;;;  If it is an attribute the next value should be an entry point (currently it
;;; cannot be a simple value), thus attributes are discarded.
;;;  if it is a class then we try to link the temporary paths to the class.
;;;  if it is a relation, we look to see if it is not a relation of one of the
;;; leaf classes


;;;------------------------------------------------------- PATH-BUILD-QUERY-LIST
;;;
;;; should also return  milestones, i.e. classes that we must go through outside
;;; the first target one
;;; One question: is the directio of the crossing of a relation important ?

(defun path-build-query (word-list)
  "tries to build a formal query from a list of words, by first getting the ~
      entry points corresponding to combinations of the words, then using them ~
      to navigate in the ontology for building a query. We assume the list of ~
      words has been pre-processed to resolve references, eventually synonyms.
Arguments:
   word-list: list of words
Return:
   a list of possible formal parsed queries or nil if failure."
  (let ((max-distance 5)
        distance old-classes current-classes target
        path-list item work-list links-and-neighbors new-classes
        tag-list active-classes link-set end-points)
    ;; first process the input to build a working list of concepts, relations and
    ;; entry frames
    ;; e.g. ($E-CELL-PHONE $S-PRESIDENT 
    ;;                     ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))
    (setq work-list (path-process-word-list word-list))
    ;;=== special cases
    ;; e.g. single value (no path) can be entry point
    ;; or contains no class but at least a relation, ...
    (if (setq path-list (path-build-query-special-cases work-list))
      (return-from path-build-query path-list))
    
    ;; fuse neighbors class frames sharing the same class
    (setq work-list (path-fuse-filters work-list))
    
    ;; pop the work list to the first class
    (setq work-list (member-if #'%is-concept? work-list))
    (format *debug-io* "~%+++ path-buil-query-list /work-list:~&   ~S" work-list)
    ;; if empty, then quit
    (unless work-list (return-from path-build-query nil))
    ;; if it contains a single element, return it (can't look for paths...)
    (unless (cdr work-list)(return-from path-build-query work-list))
    
    ;; start with the list of current classes containing only the target class
    (setq target (pop work-list))
    (setq current-classes `((nil ,target))) ; no link from predecessor
    (setq distance 0) ; to avoid looping indefinitely
    (catch :next
      (loop
        ;; otherwise, try to check items
        (setq item (car work-list))
        (format *debug-io* "~%+++ path-build-query /distance: ~S" distance)
        (format *debug-io* "~%+++ path-build-query /item: ~S" item)
        (cond
         ;; if too far, quit
         ((> distance max-distance)
          (throw :next :too-far)
          )
         ;; if item is a class should check with one of the current classes
         ((%is-concept? item)
          (format *debug-io* "~%+++ pbql /item is concept.")
          (dolist (link-and-class current-classes)
            (format *debug-io* "~%+++ checking ~S vs ~S" item link-and-class)
            ;; check for same concept, i.e. item belongs to a subclass of class
            (when (member item (%class-and-subclasses (cadr link-and-class)))
              (format *debug-io* "~%+++ ~S is member of subclasses of ~S" 
                      item (cadr link-and-class))
              ;; mark items and return newly created tag
              ;; record tag for later removing the marks
              (push (path-mark-items item link-and-class) tag-list)
              ;; record link and class on the end-points
              (push link-and-class end-points)
              ))
          ;; remove item from the work-list if it was used
          (when end-points 
            (pop work-list))
          )
         ;; if item is a class frame
         ((and (listp item)(%is-concept? (car item)))
          (format *debug-io* "~%+++ pbql /item is class-frame.")
          (dolist (link-and-class current-classes)
            (format *debug-io* "~%+++ checking ~S vs ~S" (car item) link-and-class)
            ;; check for same concept
            (when (member (car item) (%class-and-subclasses (cadr link-and-class)))
              (format *debug-io* "~%+++ ~S is member of subclasses of ~S" 
                      item (cadr link-and-class))
              ;; mark items and return newly created tag
              (push (path-mark-items item link-and-class :add-filter) tag-list)
              ;; record link and class on the end-points
              (push link-and-class end-points)
              ))
          ;; remove item from the work-list if it was used
          (when end-points
            (pop work-list))
          )
         ;; if item is a relation or inverse relation, we check if we have the
         ;; relation in the list current-classes
         ((%is-relation? item)
          ;; item is a generic property, we need to gather all specific relations
          ;; and their inverses
          (format *debug-io* "~%+++ path-buil-query-list /item is relation.")
          (setq link-set (%get-value item (%inverse-property-id '$IS-A)))
          (setq link-set (append link-set
                                 (mapcar #'%inverse-property-id link-set)))
          (format *debug-io* "~%+++ pbql /relation set:~&   ~S" link-set)
          ;; everytime we find a property in the current classes, we save the
          ;; corresponding link and mark the classes
          (setq active-classes nil)
          (dolist (sp-id link-set)
            (when (assoc sp-id current-classes)
              (format *debug-io* "~%+++ ~S is  a link to: ~S" 
                      sp-id (cadr (assoc sp-id current-classes)))
              (push (path-mark-items item (assoc sp-id current-classes)) tag-list)
              ;; save the active links into the end-points
              (push (assoc sp-id current-classes) end-points)
              ;; if relation is found at this level we pop it out
              ))
          ;; if we found some links, pop up work-list
          (when end-points 
            (pop work-list)
            ;; remove non active links from the list of possible candidates
            ;; the rationale is that if we have found links we priviledge those paths
            (setq current-classes end-points))
          (break "checked the relation: ~S" item)
          )
         ;; we assume that the path-process-word-list function does not yield
         ;; inverse relations
         
         ;; otherwise warn but ignore element
         (t
          (warn "unknown element: ~S in the work-list" item)
          )
         )
        ;; if we ewhausted all items, then get out of loop
        (unless work-list (return nil))
        ;; save current classes
        (setq old-classes (append old-classes current-classes))
        (format *debug-io* "~%+++ pbql /old-classes:~&   ~S." old-classes)
        ;; build the list of successors of the current-classes, removing those that
        ;; were already explored
        (setq new-classes nil)
        (dolist (link-and-class current-classes)
          ;; get a list like (($S-PUBLICATION-AUTHOR $E-PERSON) ...)
          (setq links-and-neighbors
                (%get-class-links-and-neighbors (cadr link-and-class)))
          (format *debug-io* "~%+++ neighbors of ~S: ~&   ~S."
                  (cadr link-and-class) links-and-neighbors)
          (setq new-classes (append new-classes links-and-neighbors))
          ;; mark each remaining class with link to predecessor
          ;; one of the problems is that there could be several classes pointing to
          ;; a particular one, e.g. <person>, this the last pred clobbers the
          ;; previous ones. However, if we make a list we'll introduce some 
          ;; ambiguity, even false paths...
          (mapc #'(lambda(xx)(setf (get (cadr xx) :pred) (cadr link-and-class))) 
                links-and-neighbors)
          )
        ;; remove duplicates
        (setq current-classes (delete-duplicates new-classes :test #'equal))
        (format *debug-io* "~%+++ updated current-classes: ~&   ~S." current-classes)
        (incf distance)
        (break "we moved one step")
        ))
    ;; recover the formal queries starting from the target
    (break "~%+++ recover formal query from ~S" target )
    (setq query (path-build-formal-query target))
    ;; remove marks in a rather brutal way removing everything from the plist
    (let ((class-list (delete-duplicates 
                       (mapcar #'cadr (append current-classes old-classes)))))
      (mapc #'(lambda(xx)(setf (symbol-plist xx) nil)) class-list)) 
    query))

#|
? (in-package :biblio)
? (moss::path-build-query '( "utc"))
($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc"))

? (moss::path-build-query '("articles" "barthes" "jean-paul"))
($E-ARTICLE
 ($S-PUBLICATION-AUTHOR (> 0)
  ($E-PERSON ($T-PERSON-NAME :IS "Barthes") ($T-PERSON-FIRST-NAME :IS "Jean-Paul"))))

? (moss::path-build-query '( "personne"))
($E-PERSON)

? (in-package :sa-address)
? (moss::path-build-query '( #+MCL "tlphone" #-MCL "téléphone" "utc"))
($E-PHONE
 ($S-ORGANIZATION-PHONE.OF (> 0)
  ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc"))))


(moss::path-build-query '(#+MCL "tlphone" #-MCL "téléphone"
                                 #+MCL "prsident" #-MCL "président" "utc"))
|#

(defun path-build-formal-query (target-class)
  "rebuilds a query using the data on the plists of the various classes.
Argument:
   target-class: class-id of the king of things we are looking for
Return:
   formal query or nil on failure."
  (when target-class
    (let ((next-clause-list (get target-class :suc)))
      `(,target-class
        ;; if more than one sublist we have an OR
        ,(if (cdr next-clause-list)
           `(or ,@(mapcar #'path-build-formal-clause next-clause-list))
           (path-build-formal-clause (car next-clause-list))))
      )))

(defun path-build-formal-clause (clause)
  "takes a triple <relation class filter-pointer> and builds a formal clause like
      (relation (> 0) (class filter))
Arguments:
   clause: a triple relation class filter
Return: 
   a formal subquery."
  (let ((link (car clause))
        (successor (cadr clause))
        (filter-pointer (caddr clause)))
    (format *debug-io* "~%+++ path-build-formal-clause /filter: ~S." 
            (get successor :suc))
    `(,link             ; property (relation or inverse relation)
      (> 0)                     ; cardinality clause
      (,successor           ; class
       ,@(if filter-pointer     ; filter clauses
           (get successor filter-pointer)))
      ,@(if (get successor :suc)
          (path-build-formal-query successor)))))

(defun path-fuse-filters (work-list)
  "takes a list containing class-frames and fuses two adjacent class frames sharing ~
      the same class.
Argument:
   work-list: list to process
Return:
   fused work list."
  (cond 
   ((null work-list) nil)
   ((and (listp (car work-list))
         (listp (cadr work-list))
         (eql (caar work-list)(caadr work-list)))
    (path-fuse-filters
     (cons
      (cons (caar work-list)
            (append (cdar work-list)(cdadr work-list)))
      (cddr work-list))))
   (t 
    (cons (car work-list) (path-fuse-filters (cdr work-list))))
   ))

#|
? (moss::path-fuse-filters '((a 1) (b 2) (b 3) (c 4)))
((A 1) (B 2 3) (C 4))
? (moss::path-fuse-filters '((a 1) (b 2) (b 3) (b 5) (c 4) (b 6)))
((A 1) (B 2 3 5) (C 4) (B 6))
|#

(defun path-mark-items (item link-and-class &optional add-filter)
  "inserts marks on the classesplist for building the query later.
Arguments:
   item: item from the work-list e.g. ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS \"UTC\"))
   link-and-class: pair (property class), 
                   e.g. ($S-ORGANIZATION-PHONE.OF $E-ORGANIZATION)
Return:
   newly created tag"
  (let ((class-id  (if (listp item) (car item) item)) ; takes care of class frames
        (tag  (gensym))
        (pred  (get (cadr link-and-class) :pred)))
    ;; in the example UNIVERSITY is a subclass of ORGANIZATION. We must mark the
    ;; class UNIVERSITY and make the link to this new class rather than ORGANIZATION
    ;; mark ORGANIZATION that is the element of the spanning tree rather than
    ;; add a forward pointer to predecessor if it exists
    ;; note that the predecessors were marked for elements of current-classes
    (when pred
      (setf (get pred :suc) 
            (append
             (get pred :suc) 
             ;; e.g. (($S-ORGANIZATION-PHONE.OF $E-UNIVERSITY #:G2453))
             `((,(car link-and-class) ,class-id ,tag))))
      (format *debug-io* "~%+++ updated plist of ~S: ~S" 
              pred (symbol-plist pred))
      )
    ;; add filter to the current class
    (if add-filter
      (setf (get class-id tag) (cdr item)))
    ;; we must make sure that all classes between here and the target class
    ;; have a pointer to their successor so that we can rebuild the path
    (loop
      (setq pred-pred (get pred :pred))
      (unless pred-pred (return nil))
      ;; check that pred has a pointer to pred
      ;; problem: can't recontruct a pointer at this stage...
      )
    tag))


(defun %get-class-links-and-neighbors (class-id)
  "gets a list of pairs (relation neighbor) or (inverse-link neighbor) for all the ~
      relations and inverse links of the class and its children.
Argument:
   class-id: if
Return:
   a list of pairs  (property class-id)."
  (let (properties results neighbors)
    ;; get all the relations for a given class including the inherited ones and
    ;; the ones from the subclasses
    (setq properties (delete-duplicates 
                      (append
                       (%%get-all-class-relations class-id)
                       (%%get-all-subclass-relations class-id))))
    ;; each relation has a $suc property, get the successor classes and for
    ;; each class build a pair (relation class)
    (dolist (prop-id properties)
      (setq neighbors (%get-value prop-id '$SUC))
      (dolist (class-id neighbors)
        ;; add the pair to the results
        (push (list prop-id class-id) results)
        ))
    ;; do the same with inverse relations, but use direct relation and $PT.OF
    (setq properties (delete-duplicates 
                      (append
                       (%%get-all-class-inverse-relations class-id)
                       (%%get-all-subclass-inverse-relations class-id))))
    ;; property to access neighbor classes
    (dolist (prop-id properties)
      (setq neighbors (%get-value prop-id (%inverse-property-id '$PS)))
      (dolist (class-id neighbors)
        ;; add the pair to the results
        (push (list (%inverse-property-id prop-id) class-id) results)
        ))
    ;; delete duplicated pairs from the results and return the list
    (delete-duplicates results :test #'equal)
    ))

#|
? (MOSS::%GET-CLASS-LINKS-AND-NEIGHBORS '$E-publication)
(($S-ARTICLE-JOURNAL $E-JOURNAL) ($S-MANUAL-ORGANIZATION $E-ORGANIZATION)
 ($S-MANUAL-PUBLISHER $E-PUBLISHER) ($S-PROCEEDINGS-PUBLISHER $E-ORGANIZATION)
 ($S-PROCEEDINGS-EDITOR $E-PERSON) ($S-PROCEEDINGS-CONFERENCE $E-CONFERENCE)
 ($S-INPROCEEDINGS-EDITOR $E-PERSON) ($S-INPROCEEDINGS-CONFERENCE $E-CONFERENCE)
 ($S-BOOK-ADDRESS $E-ADDRESS) ($S-BOOK-PUBLISHER $E-ORGANIZATION)
 ($S-PUBLICATION-DOMAIN $E-THEME) ($S-PUBLICATION-AUTHOR $E-PERSON))

? (MOSS::%GET-CLASS-LINKS-AND-NEIGHBORS '$E-person)
(($S-PROCEEDINGS-EDITOR.OF $E-PROCEEDINGS)
 ($S-INPROCEEDINGS-EDITOR.OF $E-INPROCEEDINGS)
 ($S-PUBLICATION-AUTHOR.OF $E-PUBLICATION) ($S-PERSON-EMPLOYER $E-ORGANIZATION)
 ($S-PERSON-WEB-PAGE $E-WEB-ADDRESS) ($S-PERSON-EMAIL $E-EMAIL-ADDRESS)
 ($S-PERSON-ADDRESS $E-ADDRESS))
|#

#|
? (moss::path-build-query '( "utc"))
(($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))

? (moss::path-build-query '( "personne"))
(($E-PERSON))

? (moss::path-build-query '( #+MCL "tlphone" #-MCL "téléphone" "utc"))
(($E-PHONE
  ($S-ORGANIZATION-PHONE.OF (> 0)
       ($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc")))))

? (moss::path-build-query '("personne" "prsident" "utc"))
(($E-PERSON
  ($S-TEACHING-ORGANIZATION-PRESIDENT.OF (> 0)
   ($E-TEACHING-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc")))))

? (moss::path-build-query '(#+MCL "tlphone" #-MCL "téléphone"
                                 #+MCL "prsident" #-MCL "président" "utc"))
(($E-PHONE
  ($S-PERSON-PHONE.OF (> 0)
   ($E-PERSON
    ($S-TEACHING-ORGANIZATION-PRESIDENT.OF (> 0)
     ($E-TEACHING-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc")))))))
|#
;;;---------------------------------------------- PATH-BUILD-QUERY-SPECIAL-CASES

(defun path-build-query-special-cases (work-list)
  "processes all special cases when building a query. E.g. single class, single ~
      entry point, no class but relation,...
Arguments:
   work-list: a list of moss objects
Return:
   the query or nil if not a special case."
  (cond
   ;; single object that is a class
   ((and (null (cdr work-list))
         (%is-concept? (car work-list)))
    work-list)
   ;; single-object entry point (appears as a list)
   ((and (null (cdr work-list))
         (listp (car work-list)))
    (car work-list))
   ;; single object relation (to be done)
   ;; ... we use the relation to obtain the corresponding class
   ;; more than one object, no class, but relation (to be done)
   ;; otherwise return nil
   ))

;;;------------------------------------------------------------ PATH-CHECK-CLASS

(defun path-check-class (class-id path-list)
  "checks whether the class-id is one of the leaf classes of the path-list. ~
      If so, keeps the corresponding branch and tell the caller.
Arguments:
   class-id: identifier of class to be checked
   path-list: a list of paths in reverse order to the root
Return:
   2 values: t if there was a match and the eventually modified path-list."
  (let (results match?)
    (dolist (path path-list)
      (when (and
             (%is-path-concept? (car path))  ; play it safe
             ;; if we specify "person" we want a path that goes through any 
             ;; possible subclass of "person"
             (member class-id (%ancestors (caar path))))
        (setq match? t)
        (push path results)))
    (values match? (if match? (reverse results) path-list))))

#|
?  (moss::path-check-class 
   '$E-PERSON
   '((($E-Student)) (($E-person))))
T
((($E-STUDENT)) (($E-PERSON)))

? (moss::path-check-class 
   '$E-STUDENT
   '((($E-Student)) (($E-person))))
T
((($E-STUDENT)))

? (moss::path-check-class 
   '$E-STUDENT
   '((($E-Student)) (($E-UNIVERSITY))))
T
((($E-STUDENT)))

? (moss::path-check-class 
   '$E-STUDENT
   '((($E-organization)) (($E-UNIVERSITY))))
NIL
((($E-ORGANIZATION)) (($E-UNIVERSITY)))
|#
;;;------------------------------------------------------------ PATH-CHECK-ENTRY
;;; this guy needs to be improved
;;; It is assumed that the leaves or the path-list represent classes, which is
;;; not true all the time.
;;; In particular when a filter is introduced in a path the corresponding leaf
;;; looks like this:
;;;   (($T-ORGANIZATION-ACRONYM :IS "Utc") $E-UNIVERSITY)
;;; in that case the class is the first symbol upstream, after skiping all the
;;; recorded filters.
;;; There should be a better and simpler way to do that since filters are not 
;;; essential in determining the path but are associated with classes.
;;; One possible way would be to record classes as a list (class filter*)
;;; Hence a paht would look like this
;;;   (($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")) 
;;;                                 $S-PRESIDENT.OF ($E-PERSON))

(defun path-check-entry (ep-frame path-list)
  "checks whether one of the leaf classes of the path-list contains class of the ~
      entry frame. If so, keeps the corresponding branch and merge the entry-frame.
Argments:
   ep-frame: something like ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS \"Utc\"))
   path-list: a list of paths in reverse order to the root
Return:
   2 values: t if there was a match and the eventually modified path-list."
  (let (match? results)
    (dolist (path path-list)
      (when (and
             (%is-path-concept? (car path))  ; play it safe
             (or
              (eql (car ep-frame) (caar path))
              (member (car ep-frame) (%get-subclasses (caar path)))))
        (setq match? t)
        ;; insert the new filter into the path
        (push (cons 
               (cons (caar path)             ; class
                     (cons (cadr ep-frame)   ; new filter
                           (cdar path)))     ; old filters or nil
               (cdr path)) 
              results)
        ))
    (values match? (if match? (reverse results) path-list))))

#|
;; ("organization" "UTC") => ($ORGANIZATION ($T-ORGANIZATION-ACRONYM :is "UTC"))
? (moss::path-check-entry 
   '($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc"))
   '((($E-Student)) (($E-ORGANIZATION))))
T
((($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc"))))

? (moss::path-check-entry 
   '($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc"))
   '((($E-Student)) (($E-university))))
T
((($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc"))))

? (moss::path-check-entry 
   '($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc"))
   '((($E-Student)) (($E-university ($T-UNIVERSITY-FAME :is "high")))))
T
((($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")
   ($T-UNIVERSITY-FAME :IS "high"))))
|#
;;;--------------------------------------------------------- PATH-CHECK-RELATION

(defun path-check-relation (rel-id path-list)
  "checks whether one of the leaf classes of the path-list contains the relation. ~
      If so, keeps the corresponding branch expand the paths including the new ~
      relation and all successor classes, creating a new path for each of them.
Arguments:
   rel-id: identifier of relation to be checked
   path-list: a list of paths in reverse order to the root
Return:
   2 values: t if there was a match and the eventually modified path-list."
  (let ((possible-relations (%get-subclasses rel-id))
        results relation-list rel-list match?)
    (dolist (path path-list)
      ;; get all relations
      (setq relation-list  (%%get-all-class-relations (caar path)))
      (when (setq rel-list (intersection possible-relations relation-list))
        (setq match? t)
        (setq results (append results (path-make-all path rel-list)))
        )
      ;; now try inverse relations
      (setq relation-list (%%get-all-class-inverse-relations (caar path)))
      (when (setq rel-list (intersection 
                            (mapcar #'%inverse-property-id possible-relations)
                            relation-list))
        (setq match? t)
        (setq results (append results 
                              (path-make-all path rel-list)))
        )
      )
    (values match? (if match? (reverse results) path-list))))

#|
? (moss::path-check-relation 
   '$S-WIFE
   '((($E-Student)) (($E-teacher))))
T
((($E-PERSON) $S-PERSON-WIFE.OF ($E-STUDENT))
 (($E-PERSON) $S-PERSON-WIFE ($E-STUDENT)))

? (moss::path-check-relation 
   '$S-president
   '((($E-Student)) (($E-address))))
T
((($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-STUDENT))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-STUDENT)))

? (moss::path-check-relation 
   '$S-president
   '((($E-Student)) (($E-person))))
T
((($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-PERSON))
 (($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-STUDENT))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-STUDENT)))

? (moss::path-check-relation 
   '$S-president
   '((($E-phone)) (($E-address))))
NIL
((($E-PHONE)) (($E-ADDRESS)))
|#
;;;------------------------------------------------------- PATH-GET-ENTRY-POINTS
;;; **********
;;; we could have entry points that are the same as class names or property names
;;; OK fixed

(defun path-get-entry-points (ep-list)
  "looks into a list of entry points for entry points that are not for classes ~
      nor for properties.
Arguments:
   ep-list: entry-point list
Return:
   a list of what is left."
  (let ((pair-list 
         (reduce  ; JPB 140820 removing mapcan
          #'append
          (mapcar #'(lambda(xx)
                      (moss::%%get-objects-from-entry-point xx :keep-entry t))
            ep-list))))
    ;; remove class names and property names
    (reduce  ; JPB 140820 removing mapcan
     #'append
     (mapcar #'(lambda (xx) (unless (or (eql (cadr xx) (%inverse-property-id '$ENAM))
                                        (eql (cadr xx) (%inverse-property-id '$PNAM)))
                              (list xx)))
       pair-list))))

#|
? (moss::path-get-entry-points '(TELEPHONE HAS-TELEPHONE UTC))
((UTC $T-ORGANIZATION-ACRONYM.OF $E-UNIVERSITY.1))
|#
;;;---------------------------------------------------------- PATH-GET-RELATIONS

(defun path-get-relations (ep-list)
  "looks into a list of entry points for relations.
Arguments:
   ep-list: entry-point list
Return:
   a pair (<ep> . <class-id>) or nil on failure."
  (let (property-list result)
    (dolist (ep ep-list)
      ;; look for $PNAM.OF
      (setq property-list (%get-value ep (%inverse-property-id '$PNAM)))
      ;; must keep only relations, removing attributes
      (setq property-list 
            (remove nil 
                    (mapcar #'(lambda (xx) (if (%is-relation? xx) xx))
                            property-list)))
      (setq result (append result property-list))
      )
    result))

#|
? (moss::path-get-relations '(TELEPHONE HAS-TELEPHONE UTC))
($S-PHONE $S-PERSON-PHONE $S-ORGANIZATION-PHONE)
|#
;;;------------------------------------------------------- PATH-GET-TARGET-CLASS

(defun path-get-target-class (ep-list &aux class-id)
  "looks into a list of entry points for the first one that is the name of a class.
Arguments:
   ep-list: entry-point list
Return:
   a pair (<ep> . <class-id>) or nil on failure."
  (dolist (ep ep-list)
    ;; look for $ENAM.OF
    (setq class-id (%get-value ep (%inverse-property-id '$ENAM)))
    (if class-id (return-from path-get-target-class (cons ep (car class-id))))))

#|
? (moss::path-get-target-class '(TELEPHONE HAS-TELEPHONE UTC))
(TELEPHONE . $E-PHONE)
|#
;;;------------------------------------------------------------------- PATH-MAKE

(defun path-make (path rel)
  "takes a path and adds a relation with successors. If several successors, then ~
      splits the path into as many paths as successor classes.
Arguments:
   path: a path, e.g. (($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
   rel: relation, e.g. $S-COUNTRY
Result:
   a list of paths e.g. 
       ((($E-COUNTRY) $S-COUNTRY ($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
        (($E-REPUBLIC) $S-COUNTRY ($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON)))."
  (let ((successors (if (%is-relation? rel) 
                      (%get-value rel '$SUC)
                      (%get-value (%inverse-property-id rel) 
                                  (%inverse-property-id '$PS))))
        result)
    ;; for each successor build a new path
    (dolist (suc successors)
      (push (list* (list suc) rel path) result))
    ;; return result
    (reverse result)))

#|
? (in-package :sa-address)
#<Package "SA-ADDRESS">
? (moss::path-make '(($E-PERSON)) '$S-person-wife)
((($E-PERSON) $S-PERSON-WIFE ($E-PERSON)))
? (moss::path-make '(($E-PERSON)) '$S-TEACHING-ORGANIZATION-PRESIDENT.of)
((($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-PERSON)))
|#
;;;--------------------------------------------------------------- PATH-MAKE-ALL

(defun path-make-all (path rel-list)
  "combines path-make for several relations.
Arguments:
   path: a path, e.g. (($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
   rel-list: a list of relations and/or inverse-relations, e.g. ($S-COUNTRY.OF)
Result:
   a list of paths e.g. 
       ((($E-COUNTRY) $S-COUNTRY ($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
        (($E-REPUBLIC) $S-COUNTRY ($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON)))."
  (reduce #'append  ; JPB 140820 removing mapcan
          (mapcar #'(lambda (xx) (path-make path xx)) rel-list)))

#|
? (in-package :sa-address)
? (moss::path-make-all '(($E-PERSON)) '($S-person-wife $S-person-mother.of))
((($E-PERSON) $S-PERSON-WIFE ($E-PERSON))
 (($E-PERSON) $S-PERSON-MOTHER.OF ($E-PERSON)))

? (moss::path-make-all '(($E-PERSON)) 
                       '($S-person-wife $S-NON-PROFIT-ORGANIZATION-PRESIDENT.of))
((($E-PERSON) $S-PERSON-WIFE ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-PERSON)))
|#
;;;-------------------------------------------------------- PATH-PROCESS-RESULTS

(defun path-process-results (path-list)
  "turns the path lists into a series of formal queries"
  (mapcar #'(lambda (xx) (path-process-single-result (reverse xx)))
          path-list))

#|
? (moss::path-process-results
    '((($E-PERSON) $S-PERSON-WIFE ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-PERSON)))
  )
(($E-PERSON ($S-PERSON-WIFE (> 0) ($E-PERSON)))
 ($E-PERSON
  ($S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF (> 0) ($E-NON-PROFIT-ORGANIZATION)))) 
|#
;;;-------------------------------------------------- PATH-PROCESS-SINGLE-RESULT

(defun path-process-single-result (path)
  "takes the internal representation of a path and builds a formal query.
   the path alternates class frames and relations.
Arguments:
   path: the internal format for the path
   query, the partially built query
return:
   a full blown query."
  (cond
   ;; finished?
   ((null path) nil)
   ((null (cdr path)) (car path))
   ;; normally we should have a relation here
   (t (append (car path) 
              (list (list (cadr path) '(> 0)
                          (path-process-single-result (cddr path))))))))

#|
? (path-process-single-result
     '(($e-person)))
($E-PERSON)

? (moss::path-process-single-result
   '(($e-person ($t-PERSON-NAME :is "Barthes"))))
($E-PERSON ($T-PERSON-NAME :IS "Barthes"))

? (moss::path-process-single-result
   '(($e-person ($t-PERSON-NAME :is "Barthes")) 
     $s-is-president-of ($e-organization)))
($E-PERSON ($T-PERSON-NAME :IS "Barthes")
 ($S-IS-PRESIDENT-OF (> 0) ($E-ORGANIZATION)))
|#
;;;------------------------------------------------------ PATH-PROCESS-WORD-LIST

(defun path-process-word-list (word-list)
  "transforms the list into a sequence containing class ids, generic relation ids ~
      entry-point patterns (class-id <filter>), to be processed by the access ~
      function.
Arguments:
   word-list: list of words representing a sentence
Return:
   e.g. ($E-HOME-ADDRESS $S-PRESIDENT 
                        ($UNIVERSITY ($T-UNIVERSITY-ACRONYM :IS \"UTC\")))."
  (let (index-list results class-id prop-id triple)
    ;; first get the list of entry poins with references
    (setq index-list (find-best-entries word-list :keep-ref t))
    ;; if empty, then quit
    (unless index-list (return-from path-process-word-list nil))
    ;; now process each entry in turn to replace by vlass-id, generic property-id
    ;; or entry-point frame
    (dolist (entry index-list)
      (cond
       ;; priority is given to classes 
       ;;********** we do not handle multiple classes
       ((setq class-id (car (%get-value (car entry) (%inverse-property-id '$ENAM))))
        (push class-id results))
       ;; then relations
       ((and (setq prop-id (car (%get-value (car entry) (%inverse-property-id '$PNAM))))
             (%is-relation? prop-id))
        (push prop-id results))
       ;; discard attributes
       (prop-id)
       ;; must be entry point for some instance
       (t  
        ;; get triples for entry points
        ;;********** we assume the inverse property and class is the same
        ;; e.g. we do not handle the case when DUPUIS is both a person and a firm
        ;; nor instances belonging to several classes
        ;; (UTC $T-ORGANIZATION-ACRONYM.OF $E-UNIVERSITY.1)
        (setq triple (car (%%get-objects-from-entry-point (car entry) :keep-entry t)))
        (push 
         (list (car (%type-of (caddr triple)))
               ;; filter
               (list* 
                (%inverse-property-id (cadr triple))
                ':IS
                (cadr entry)))
         results)))
      )
    ;; return list
    (reverse results)))

#|
===== MCL
? (moss::path-process-word-list 
   '("quelle" "est" "l""adresse" "prive" "du" "prsident" "de" "l" "utc"))
($E-HOME-ADDRESS $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))

? (moss::path-process-word-list 
   '("quel" "est" "le" "numro" "de" "tlphone" "du" "prsident" "de" "l" "utc"))
($E-PHONE $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))

? (moss::path-process-word-list 
   '("quel" "est" "le" "numro" "de" "portable" "du" "prsident" "de" "l" "utc"))
($E-CELL-PHONE $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))

===== ACL
SA-ADDRESS(6): (moss::path-process-word-list 
   '("quel" "est" "le" "numéro" "de" "téléphone" "du" "président" "de" "l" "utc"))
($E-PHONE $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))
 
SA-ADDRESS(7): (moss::path-process-word-list 
   '("quel" "est" "le" "numéro" "de" "portable" "du" "président" "de" "l" "utc"))
($E-CELL-PHONE $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))
|#
;;;------------------------------------------------------------------- PATH-STEP

(defun path-step (path-list bridges)
  "takes a set of paths, a list of bridges and moves ~
      one step away at the leaf level.
Arguments:
   path-list: a list of paths modeling te spanning tree
   bridges; a list of relations
Return:
   2 values: the updated path-list, and the updated results."
  (let (relation-list results)
    ;; for each path
    (dolist (path path-list)
      ;; compute the list of relations and inverse-relations of the leaf class
      (setq relation-list
            (delete-duplicates
             (append 
              (%%get-all-class-relations (caar path))
              (%%get-all-class-inverse-relations (caar path)))))
      ;; if some of the properties are in the bridge use only those, otherwise
      ;; use them all
      (setq relation-list (or (intersection bridges relation-list) 
                              relation-list))
      ;(print relation-list)
      ;; return updated path-list
      (setq results (append results (path-make-all path relation-list)))
      )
    results))

#|
? (moss::path-step '((($e-person))) 
                   '(($S-PERSON-WEB-PAGE) $S-TEACHING-ORGANIZATION-PRESIDENT.OF))
((($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-PERSON)))

? (moss::path-step '((($e-person))) nil)
((($E-POSTAL-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
 (($E-PHONE) $S-PERSON-PHONE ($E-PERSON)) (($E-EMAIL) $S-PERSON-EMAIL ($E-PERSON))
 (($E-WEB-ADDRESS) $S-PERSON-WEB-PAGE ($E-PERSON))
 (($E-PERSON) $S-PERSON-HUSPAND ($E-PERSON))
 (($E-PERSON) $S-PERSON-WIFE ($E-PERSON)) (($E-PERSON) $S-PERSON-MOTHER ($E-PERSON))
 (($E-PERSON) $S-PERSON-HUSPAND.OF ($E-PERSON))
 (($E-PERSON) $S-PERSON-WIFE.OF ($E-PERSON))
 (($E-PERSON) $S-PERSON-MOTHER.OF ($E-PERSON))
 (($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-EXECUTIVE-COMMITTEE.OF
  ($E-PERSON))
 (($E-MEETING) $S-MEETING-ORGANIZER.OF ($E-PERSON)))
|#

(format t "~%;*** MOSS v~A - New Paths loaded ***" *moss-version-number*) 