;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=================================================================================
;;;20/01/29
;;;		
;;;		D E F I N I T I O N S - (File def.lisp)
;;;	
;;; First created July 1992 
;;;=================================================================================
;;;	The file contains functions to create classes, properties, and
;;;	methods, by a casual user
;;; 	They can be used only when MOSS has been bootstraped

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; SYNTAX
;;; ======
;;;   defconcept         calls %make-concept
;;;   defattribute       calls %make-attribute
;;;   defrelation        calls %make-relation
;;;   definstmethod      calls %make-method
;;;   defownmethod       calls %make-ownmethod
;;;   defunivmethod      calls %make-universalmethod
;;;   defobject          calls %make-individual
;;;   defvirtualconcept  calls %make-virtual-concept
;;;   defvirtual      
;;;
;;; Multilingual names (MLN)
;;; ------------------------
;;; Multilingual names were represented by a list with the following format:
;;;   (:fr "titre principal; titre de chapitre" :en "chapter title")
;;; now revised to
;;;   ((:fr "titre principal" "titre de chapitre") (:en "chapter title"))
;;; Each language has a tag and the associated string may contain synonyms.
;;; One of the synonyms is chosen to build the internal ID and the other synonyms 
;;; are used to define entry points.
;;; Multilingual names can be used to define attributes, relations, classes,...
;;;
;;; When a property is specified by a simple string, this string can be one of
;;; the many synonyms of the generic property. In that case, the name used for
;;; creating the local property should be obtained from the generic property.
;;; Actually, there is no requirement to do that since the role of the internal
;;; ID is to facilitate debugging and has no semantic content.
;;; Another point is when the new multilingual name contains new synonyms, i.e. 
;;; Synonyms that are not present at the generic property level. One has then 
;;; two problems: the first is to get to the generic property (especially when
;;; the first English synonym is new); the second one is to add the new synonyms
;;; to those of the generic property (merge the corresponding MLN).
;;; Example:
;;; assume the generic property name is:
;;;   (:en "person; people" :fr "personne; individu")
;;; and we have a new mln:
;;;   (:en "guy" :fr "individu; zouave") or "guy; people"
;;; we should merge them to obtain;
;;;   (:en "person; people; guy" :fr "personne; individu; zouave")
;;; and add the HAS-GUY, HAS-ZOUAVE entry-points to the generic property.
;;; The name associated with the local property should be $X-CLASS-PERSON rather
;;; than $X-CLASS-GUY, but this is not crucial. 
;;; Locally the entry points will be HAS-GUY, HAS-INDIVIDU, HAS-ZOUAVE but the
;;; inverse property is that of the generic one, namely $X-PERSON.OF (?)
;;;
;;; In this approach, the role of a generic property is to host all the possible 
;;; synonyms for that property. Another approach would be to give the generic 
;;; property only the canonical name. However, it would then be more difficult to
;;; access it through one of the synonyms. Also, more importantly, if the generic
;;; property is defined without sub-properties, it could not have synonyms. 
;;; *** Maybe, the inverse property should contain all possible inverse synonyms,
;;; namely: IS-PERSON-OF, IS-GUY-OF, ..., IS-ZOUAVE-OF, which is not the case 
;;; currently.

;;; Versions
;;; ========
;;; The rationale with versions is the following:
;;; If a property is created in a previous reachable context, then it is inherited 
;;; in the current context and a new property with the same name cannot be built
;;; Only changes can be made to that property (which is not advised since instances
;;; might not comply with the new changes).
;;; If a generic property exists, then it cannot be redefined, but a creating a
;;; concept with that property will use the generic property defined previously.
;;; A concept cannot be redefined in a child version or in a parent version.

#|
2003
 0514 renaming _%deftp to %make-tp, _%defsp to %make-sp, etc.
 0517 renaming file from SERVICE to BOOT, since it contains the functions to boot 
      the system
      modifying some functions so that the functions in this file only use primitives
      from the service functions file and no fancy message passing.
 0423 introduction of %%make-instance-from-class-id to create instances directly
      from the class-id rather than from the class name (entry-point). This avoids
      package name conflicts between MOSS and application package
 0624 correction to %%make-instance-from-class-id
 0712 P: prefixing conflicting symbols with MOSS-
 0716 exporting names in the %defmethod macro (no need to declare them)
 0725 allowing :var as an optoin in definstance
2004
 0810 supressing the option to give a "HAS-XXX" name to an inverse property
      this introduces the property IS-IS-A-OF!!!
      changing USER to MOSS-USER
 0913 adding eval-when to exports and defining names as global variables
      using proclaim
      Declaring basic MOSS components as global and exported entities
 1006 making provision for ACL compilerless environments (Birrrkkkhhh!)
2005
 0306 mw-format at the end of the file changed to mformat
      introducing %make-tp-get-accessor and %make-tp-set-accessor to build
      property accessors
 0413 correcting a bug in %make-prop-set-accessor
 0416 changing default in %make-generic-tp
 0418 introducing VALUE-RESTRICTION for properties. A value has the following
      syntax:
         (<restriction-operator> <arg-list>)
      where <restriction-operator> is a keyword for the 
      *allowed-restriction-operators* global list.
 0428 modifying %make-method
 0506 correcting %make-generic-tp to allow options in generic properties
 0512 defining English and French documentation
 0523 Letting $ENAM be multiple
 0618 we allow forward references in %make-sp
      if successor does not exist and *allow-forward-references* is true, then
      we add a defsp expression onto the *deferred-actions* list. The latter will
      be executed after all the defining file has been processed with a value 
      of *allow-forward-references* set to nil to cach errors.
      Adding m-load
 0629 correcting mistake in %%make-instance-from-class-id
 1212 Version 6.0
      ===========
 1219 introducing instances of several classes and the REF and ID properties
 1215 correcting bug in %%make-instance-from-class-id
2006
 0227 adding default $OBJNAM attribute for all objects, and :doc option for 
      instances
      :one-of option accepts a simplified format using default object names
 0304 creating %make-concept-isa-option, %make-concept-oneof-option, 
      %make-concept-sp-option, %make-concept-tp-option to make %make-concept slimer
      adding *allow-forward-instance-references*
 0305 adding %make-inverse-property to simplify %make-sp, %make-tp
 0312 introducing %process-doc-option to accomadate SOL :doc syntax
 0723 correcting error when defining attributes. Automatic variable now set to
      local property
 0806 %make-concept-sp-option takes now strings as successor refs
 0831 make a specific =make-entry method for documentation titles
 1102 replacing :exist by :exists in various places
 1205 adding the possibility of creating successors on the fly for instances.
2007
 0201 upgrading the defsetf for constructors so that local properties are used
 0221 removing INDEX as a synonym of ENTRY-POINT because of a conflict with
      CG-USER::index !
 0313 fixing %%make-sp-fix-args when *language* is :all
 0317 mending %make-concept-sp-option for the :one-of option
      %make-instance-name-option for a :context key arg
 0319 adding %make-ontology
 0724 correcting deferred IS-A
 1201 adding an :export option to the definitional macras, so that MOSS name can
      be exported but private names, i.e. agent ontology concepts are kept local
2008
 0130 modifying %%make-sp-fix-args and introducing %make-sp-extract-class-name
      so that we can have a more flexible syntax for defrelation
 0131 introducing deferred defaults, updating m-load
 0204 correcting misspelling in %%make-tp-from-ids (:iexport)
 0220 fixing %make-sp-extract-class-name for :any argument
 0517 introducing %make-method-resolve-selector to resolve complex selector
      references for creating own or instance methods
      We restrict selector in defxxxmethod to be a triple (entry att concept)
      More complex location of objects if needed can be sone using a query
 0520 rewriting %make-method and %make-own-method for cleaner code
 0528 adding the :concept and :concept-id options to %%MAKE-TP-FROM-IDS 
 0602 exporting =make-entry too late to do it in boot, try macros
 0604 === v5.11.2
 0621 === v7.0.0
2009
 0514 adding the possibility of having queries to link values in defindividual
      %make-instance-sp-option and %make-instance-link for deferred linking
 0720 fixing a bug in %make-instance concerning language tags
 1223 reinstalling :entry option for generic properties
 1230 adding $SML, $SFL (macro and function lists) to $SYS
 1231 adding %make-individual because %make-instance is a ccl function
2010
 0101 adding :do-not-save option to %make-concept
 0113 fixing %%make-instance-... when object id is specific (e.g. for a counter)
      fixing %make-method-resolve-selector-list
 0118 simplifying %make-object, correcting bug in %make-sp
 0329 adding %make-defexpr
 0513 merging with OMAS v7.16
 0521 adding a test on option-list for %make-sp and %make-tp
 0614 adding mechanism for saving objects when an OMAS agent is editing its KB
 0822 introducing saving functions save-new-id and save-old-value
 1006 modifying %make-ontology
 1025 resetting *deferred-mode* in m-load
 1028 introducing the possibility of having strings as method names when defining them
 1030 fix up %%make-sp-from-ids for :any arguments
2011
 0806 correcting a bug in %make-instance-sp-option JPB1108
 0828 skipping :rdx in %make-concept
2012
 0122 correcting bug for building instances belonging to multiple classes
 1029 adding %MAKE-SP-CHECK-ONE-OF-VALUES to check one-of option for relations
2013
 0416 correcting a bug in m-load on deferred-instance-links
2014
 0307 -> UTF8 encoding
 1119 changing %%make-instance-from-class-id to take into account MLN class names
2015
 1123 adding :one-of/:not-in option to relations (%%make-sp-from-ids)
2016
 0531 adding option check in %make-tp
 1129 pb with the order of the neighbors when creating individuals
2017
 0211 allowing *any* as the successor to a reation
|#

(in-package "MOSS")

;;;===============================  TEST data  ================================
#|
(defpackage :address (:use :moss :cl :ccl))
(in-package :address)

(with-package :address
  (set (intern  "*MOSS-SYSTEM*") 'address::$E-SYSTEM.1)
  (setq address::$E-SYSTEM.1 
        '((MOSS::$TYPE (0 MOSS::$SYS))
          (MOSS::$ID (0 MOSS::$SYS.1))
          (MOSS::$SNAM (0 (:EN "MOSS" :FR "MOSS"))) (MOSS::$PRFX (0 "moss"))
          (MOSS::$EPLS
           (0 MOSS::MOSS))
          (MOSS::$CRET (0 "J-P.A.BARTHES")) (MOSS::$DTCT (0 "January 10"))
          (MOSS::$VERT (0 6.0)) (MOSS::$XNB (0 0)) (MOSS::$FNAM (0 "MOSS8-KERNEL"))
          (MOSS::$CLCS (0 MOSS::$CLS.CTR)))))

(with-package :address (step (moss::%make-generic-tp "Title" '(:unique))))
|#
;;;======================= Macros and functions ===============================
;;; the following definitions were introduced to avoid confusion with the CL
;;; functions creating instances, classes or else. 
;;; They were not really used and should be removed from MOSS

(defUn m-make-attribute (name &rest option-list)
  (apply #'%make-tp name option-list))

(defUn m-make-class (name &rest option-list)
  (apply #'%make-concept name option-list))

(defUn m-make-concept (name &rest option-list)
  (apply #'%make-concept name option-list))

(defUn m-make-individual (name &rest option-list)
  (apply #'%make-instance name option-list))

(defUn m-make-instance (name &rest option-list)
  (apply #'%make-instance name option-list))

(defUn m-make-method (name selector arg-list &rest body)
  (apply #'%make-method name selector arg-list body))

(defUn m-make-own-method (name selector arg-list &rest body)
  (apply #'%make-ownmethod name selector arg-list body))

(defUn m-make-object (&rest option-list)
  (apply #'%make-object option-list))

(defUn m-make-relation (name pred suc &rest option-list)
  (apply #'%make-sp name pred suc option-list))

(defUn m-make-sp (name pred suc &rest option-list)
  (apply #'%make-sp name pred suc option-list))

(defUn m-make-tp (name &rest option-list)
  (apply #'%make-tp name option-list))

(defUn m-make-universal-method (name arg-list doc body)
  (funcall #'%make-universal name arg-list (cons doc body)))

;;;========================== service functions ================================

;;;*-------------------------------------------- %CHECK-PACKAGE-VERSION-LANGUAGE

(defUn %check-package-version-language (package context language ref)
  "check that package is an existing package, version is an allowed context ~
   and language is a legal language. If not, throws to :error.
Arguments:
   package: a package
   version: a number
   language: a language tag
   ref: reference of the object being defined (used by error messages)
Return:
   the package context or tag if OK, throws to :error otherwise."
  ;;=== check validity of context, package and language
  ;; check context (uses mthrow to :error)
  (%%allowed-context? context)
  ;; check package (should use mthrow when MOSS window is active)
  (setq package (find-package package))
  (unless (typep package 'package)
    (terror "unexisting package ~S when defining ~S" package ref))
  ;; check language, include :all
  (unless (member language (cons :all *LANGUAGE-TAGS*))
    (terror "unknown language ~S when defining ~S ~%; legal ones: ~S"
            language ref *LANGUAGE-TAGS*))
  )

;;;---------------------------------------------------------------------- M-LOAD
;;;
;;; The idea is that we can have forward references. However, we do not allow 
;;; forward references for defattribute or defrelation for which class arguments
;;; must precede the call.
;;; When we read the ontology file with a deferred flag, we create:
;;;   - all classes and attributes
;;;   - all links among classes (*deferred-links*)
;;;   - all methods referring to classes (*deferred-methods*)
;;;   - all individuals and attributes (*deferred-instances*)
;;;   - all links among individuals (*deferred-instance-links*)
;;;   - all methods applying to individuals (*deferred-instance-methods*)
;;;   - anything that must be executed last (*deferred-defaults*)
;;; When reading the file and creating the classes, we are in  the file package, 
;;; using the specified context and language. When we are executing the deferred
;;; list we are in an unknown environment. Therefore, we save the environment 
;;; and execute the deferred commands within a with-environment macro.
;;; The reading mode (*deferred-mode*) tells whether we are reading classes
;;; value :class or instances value :instance.
;;; Within a file concepts (classes) and related mmethods must appear before 
;;; individuals and related methods.

;********** Virtual classes and properties must be taken care of...
#+(AND MICROSOFT-32 OMAS)
(defun cg-user::ml ()
  (setq moss::*debug* t)
  (moss::m-load (make-pathname
                 :device (pathname-device omas::*omas-directory*)
                 :directory
                 (append (pathname-directory omas::*omas-directory*)
                         (list "OMAS" "applications" "UTC-SERVER"))
                 :name "mission-ontology"
                 :type "lisp"
                 )
                :external-format :utf-8))

(defUn m-load (file-name &key external-format)
  "loads MOSS classes allowing forward references in classes and instances.
Arguments:
   file-name: name or pathname of the file containing the file definitions.
   external-format (key): :utf-8 for utf-8 encoded files
Return:
   t"
  (let* ((*allow-forward-references* t)
         ;; when executing defindividual, must take care of forward links to other
         ;; instances (*allow-forward-instance-references* t)
         (*allow-forward-instance-references* t)
         (*deferred-links* nil)
         (*deferred-methods* nil)
         (*deferred-instances* nil)
         (*deferred-instance-links* nil)
         (*deferred-instance-methods* nil)
         (*deferred-defaults* nil)
         ;; every time we load a new file, we assume that the file contains an
         ;; ontology or starts with classes. We set the mode to :class, which
         ;; will allow own-method referring to classes or properties to be loaded
         ;; before the own-methods referring to instances
         (*deferred-mode* :class)
         (*language* (or *language* :en))  ; language defaults to English
         message)
    (declare (special *language*))
    ;(setq *debug* t)     
    ;(break "m-load /file: ~S, external-format: ~S" file-name external-format)
    ;; first load file as usual, taking care of the external-format argument
    (if external-format
        (load file-name 
              :external-format (excl::find-external-format external-format))
      (load file-name))
    ;((stringp message) "m-load /file ~S loaded (~S)." file-name external-format)
    ;; once loaded, reset non existing reference error mechanism
    (setq *allow-forward-references* nil
          *deferred-links* (reverse *deferred-links*)) ; JPB060228
    ;(format t "~%; m-load /*deferred-instance-links*: ~%~S" *deferred-instance-links*)
    ;;=== any deferred link definition is first executed
    (setq message
          (catch :error
            (dolist (def-expr *deferred-links*)
              (dformat :mload 0 "m-load: deferred link:~&===> ~S" def-expr)
              (eval def-expr)
              )))
    (when (stringp message)
      (error "~A" message))
    ;; reset delayed definition list
    (setq *deferred-links* nil)
    
    ;;=== now we create methods
    (setq message
          (catch :error
            (dolist (def-expr (reverse *deferred-methods*))
              (dformat :mload 0 "m-load: deferred method:~&===> ~S" def-expr)
              (eval def-expr)
              )))
    (when (stringp message)
      (error "~A" message))
    ;; reset delayed definition list
    (setq *deferred-methods* nil)

    ;(break "m-load /ready to create instances.")
    ;;=== now we create instances (without links), building *deferred-instance-links*
    ;(format t "~%; m-load / *deferred-instances*:~%  ~S" *deferred-instances*)
    (setq message
          (catch :error
            (dolist (def-expr (reverse *deferred-instances*))
              (dformat :mload 0 "m-load: deferred instance:~&===> ~S" def-expr)
              (eval def-expr)
              )))
    (when (stringp message)
      (error "~A" message))
    ;; reset delayed definition list
    (setq *deferred-instances* nil)
    
    ;; now we execute the deferred instance links
    (setq *allow-forward-instance-references* nil)
    
    ;;=== now link instances
    (setq message
          (catch :error
            (dolist (def-expr (reverse *deferred-instance-links*))
              (dformat :mload 0 "m-load ; deferred instances link:~&===> ~S" def-expr)
              (eval def-expr)
              )))
    (when (stringp message)
      (error "~A" message))
    
   (setq *deferred-instance-links* nil)

    ;;***** Here we must create own-methods related to properties, so that entry points
    ;; are created. However, we cannot create own-methods related to instances, 
    ;; before creating the instance. Therefore, we must distinguish own methods
    ;; that apply to models and own-methods that apply to instances
    ;;=== now create own methods
    (setq message
          (catch :error
            (dolist (def-expr (reverse *deferred-instance-methods*))
              (dformat :mload 0 "m-load ; deferred instances method:~&===> ~S" def-expr)
              (eval def-expr)
              )))
    (when (stringp message)
      (error "~A" message))
    
    (setq *deferred-instance-methods* nil)

    ;(mformat "~2%;m-load /*** objects loaded ***")
    ;(sleep 1) ; let the guy print the stuff
    ;; reset language defaulting to English
    ;(setq *language* language)
    
    ;;=== now execute the deferred default actions, anything that must be executed last
    (setq message
          (catch :error
            (dolist (def-expr (reverse *deferred-defaults*))
              (dformat :mload 0 "mload ; deferred default creation:~&===> ~S" def-expr)
              ;(print (eval def-expr))
              (eval def-expr)
              )))
    (when (stringp message)
      (error "~A" message))
    (setq *deferred-defaults* nil)
    ;(break "m-load")
    )
  :done)

;(m-load "forward-ref-test")
; (moss::m-load (choose-file-dialog))

;;;=============================================================================
;;;
;;;                       ATTRIBUTE (Terminal Property) 
;;;
;;;=============================================================================

;;; GENERIC PROPERTY
;;; ================
;;; A generic property is
;;;   - a property created when one defines a property linked to a class
;;;     e.g., when defining the attribute "name" for the class "Person" the 
;;;     generic property $T-NAME is created if it does not exist and a sub-property
;;;     $T-PERSON-NAME is created and attached to the class "Person"
;;;   - a property defined when no class is specified
;;; Defaults
;;; --------
;;; Generic properties may have defaults when defined independently of a class 
;;; (class-id is NIL)
;;; when a class is specified the :default option is attached to the ideal
;;;
;;; Options
;;; -------
;;; When the generic property is created from a class property, it inherits the
;;; :entry option. E.g. if we write (defconcept "test" (:att "color" (:entry)))
;;; then generic property $T-COLOR will have a =make-entry own method.

;;;------------------------------------------------------------ %MAKE-GENERIC-TP

;;; If the function is called while loading a file, then the corresponding 
;;; package is stable during the execution. If the function is called while
;;; agents are working, then a context switch may break the package reference
;;; (not sure about that...)

(defUn %make-generic-tp (tp-ref &rest option-list)
  "creates a generic terminal property. Options are taken into account only ~
   when defining a property independently from a class.
Arguments:
   tp-ref: a symbol, string or multilingual name of the attribute, 
               e.g. TITLE, \"title\", (:fr \"Titre\" :en \" title ; header\")
           Will produce HAS-TITLE and a settable HAS-TITLE accessor.
               (:fr \"Titre\") will produce HAS-TITRE !
   Option list:
     (:class-ref <class-ref>)  name of the class to which the property is attached
     (:concept <class-ref>) id.
     (:context <number>) must be a legal context
     (:entry {<function-descriptor>})	specify entry-point
			                if no arg uses make-entry, otherwise uses
			                specified function
                     where 
                        <function-descriptor>::=<arg-list><doc><body>
	               e.g.  (:entry (value-list) \"Entry for Company name\"
	       			     (intern (make-name value-list)))
     (:min <number>)	       minimal cardinality
     (:max <number>)	       maximal cardinality
     (:unique)	               minimal and maximal cardinality are each 1
     (:doc <documentation>)    documentation ref
     (:type <type>)            values should be of type :number, :string, or :mln
     (:not-type <type>)        values should not be of that type
     (:value <value>)          value should be that value, a way of imposing a value
                               defined at the property level
     (:one-of <instances>+)    list of specific values
     (:forall)                 all values should be taken from the one-of list or of
                               the specified type
     (:exists)                 there should be at least one value taken from the one-of
                               list or of the specified type        
     (:not-in)                 no value should be in the one-of list
     (:same)                   all values should be identical
     (:different)              all values should be different
  options type, not-type, value, one-of/forall are strict, meaning that if the values
  do not comply with the restriction, they are not recorded.
Return:
   the id of the generic property in the specific package in the specific context"
  
  ;; thereafter tp-name is the name of the property, e.g. HAS-TITLE
  ;; id is the id of the property, e.g. $T-TITLE
  (let* ((class-ref (or (car (getv :class-ref option-list))
                        (car (getv :concept option-list))))
         (context (car (getv :context option-list)))
         tp-name tp-mln id class-id pair ;tp-synonym-list
         )
    ;; first check context if not an option, get the current one
    (unless context 
      (setq context (symbol-value (intern "*CONTEXT*"))))
    
    ;; check if allowed. Throw to :error if illegal
    (%%allowed-context? context)
    
    (with-context context
      (setq tp-name (%make-name-for-attribute tp-ref))
      ;; check then if we have already a terminal property with this name
      (setq id (%%get-id tp-name :tp))
      
      ;; if property exists, return it (e.g., to use in a class)
      (when id 
        (warn "reusing previously defined generic attribute ~A, in package ~S ~
               and context ~S ignoring eventual options (class: ~S)." 
          tp-name *package* context class-ref)
        ;; property is not modified
        (return-from %make-generic-tp id))
      
      ;; build mln from input arg, preserving multilingual data
      (setq tp-mln (mln::make-mln tp-ref)) ; jpb1406
      
      ;; check for existing class (must exist if mentioned).
      (when class-ref 
        ;; recover the class-id if there
        (setq class-id (%%get-id class-ref :class))
        (unless (%is-class? class-id)
          (terror "Cannot find the specified class ~S with id ~S when defining prop ~S"
                  class-ref class-id tp-name)))
      
      ;; here, the property does not exist, we must create it 
      ;; first cook up the tp key.
      (setq id (%make-id-for-tp tp-ref))
      ;; then check if id already exists
      ;; if so, there is a problem since we know that the property does not exist
      ;; the id symbol must be used for something else...
      (when (%pdm? id)
        (terror "when defining terminal property ~A in package ~S, ~
               internal identifier ~A already exists" 
                tp-name *package* id))
      
      ;; otherwise create property object
      (set id `(($TYPE (,context $EPT))
                ($ID (,context ,id))
                ($PNAM (,context ,tp-mln))))
      
      ;; when editing declare it as new to avoid saving it as old in %link
      (save-new-id id)
      ;; add new property-id to local system-list
      (%link (symbol-value (intern "*MOSS-SYSTEM*"))
             '$ETLS id) ; add to system list of tp
      
      ;; build entry-point if it does not exist already or add entry
      ;; we must build an entry point for each synonym in each language
      (%make-ep-from-mln tp-mln '$PNAM id :type '$EPT)
      
      ;; Now we consider inverse property: id provides the package info, tp-mln the 
      ;; language info
      (%make-inverse-property id tp-mln)
      
      ;;=== process now OPTION LIST unless it is a user's property with a specified
      ;; class, in which case the options will be attached to the specific class
      ;; property
      
      ;; an exception id the default option
      ;; default is normally attached to the ideal of the corresponding class
      ;; if the property is defined without reference to a class, then the
      ;; default is attached to the property itself. However, if the property is used
      ;; later to support a local property with a different default, the default
      ;; attached to ideal will shadow the default attached to the property. 
      ;; we only do that for system properties, or for generic properties that
      ;; do not belong to a class. Otherwise, the default option is processed in the
      ;; %make-tp function
      (cond
       ((and (assoc :default option-list)(null class-id))
        ;; sytem property, or simply generic one
        (%%set-value-list id (getv :default option-list) '$DEFT context)))
      
      ;; when defining a new generic property, check the :entry option and, if there,
      ;; build the entry function (outside the case loop)
      (when (setq pair (or (assoc :entry option-list) (assoc :index option-list)))
        (if (cdr pair)
            ;; specific function defined in the option
            (apply #'%make-ownmethod '=make-entry id (cdr pair))
          ;; no function specified, attach default function
          (%make-ownmethod :default-make-entry id nil nil)))
      
      ;; when we have a property without reference to a class
      ;; i.e. when we are defining a generic property directly, we build all options
      (unless class-id
        (while option-list
               (case (caar option-list)
                 (:min
                  (%%set-value id (cadar option-list) '$MINT context)
                  )
                 (:max
                  (%%set-value id (cadar option-list) '$MAXT context)
                  )
                 (:unique	;; has both min and max to 1
                  (%%set-value id 1 '$MINT context)
                  (%%set-value id 1 '$MAXT context)
                  )
                 ;; Record documentation if specified
                 (:doc	
                  (%process-doc-option id (cdar option-list))
                  )
                 (:type   ;; set type of value
                  (%%set-value id (cadar option-list) '$TPRT context)
                  )
                 (:not-type  ;; must not be of that type
                  (%%set-value id (cadar option-list) '$NTPR context)
                  )
                 (:value  ;; a way to impose a value defined at property level
                  (%%set-value id (cadar option-list) '$VALR context)
                  )
                 ;; if values are specified, record them
                 (:one-of
                  ;; we should make sure that the values are of the right type and/or
                  ;; execute the =xi method
                  (%%set-value-list id (cdar option-list) '$ONEOF context)
                  ;; following test sets :one-of as a default selection
                  (unless (or (%%has-value id '$SEL context)
                              (assoc :exists option-list)
                              (assoc :forall option-list))
                    (%%set-value id :exists '$SEL context)))
                 ;; :exists and :forall make sense only when :one-of is specified
                 (:exists
                  (if (or (%%has-value id '$ONEOF context)
                          (assoc :one-of option-list))
                      (%%set-value id :exists '$SEL context)
                    (terror ":exists option requires the use of the :one-of option ~
                             for generic attribute ~S" tp-name)))
                 (:forall 
                  (if (or (%%has-value id '$ONEOF context)
                          (assoc :one-of option-list))
                      (%%set-value id :forall  '$SEL context)
                    (terror ":forall option requires the use of the :one-of option ~
                             for generic attribute ~S" tp-name)))
                 (:not-in 
                  (if (or (%%has-value id '$ONEOF context)
                          (assoc :one-of option-list))
                      (%%set-value id :not '$SEL context)
                    (terror ":not-in  option requires the use of the :one-of option
                             for generic attribute ~S" tp-name))
                  )
                 ((:between :outside)
                  (%%set-value-list id (car option-list) '$OPR context)
                  )
                 ((:same :different)
                  (%%set-value-list id (car option-list) '$OPRALL context)
                  )
                 
                 ) ; end case
               ;; all other options are ignored
               (pop option-list)
               )
        )
      ;(format t "~%== id: ~S - flag: ~S - tp-name: ~S" id *moss-engine-loaded* tp-name)
      ;; build external-name for temporary use only (current session), e.g. _HAS-NAME
      ;; not really useful in case of multilingual definitions with synonyms
      (set (%make-name-for-variable tp-name) id)
      ;; create access function to allow to use property name in methods
      ;; If already defined (and exported) in MOSS, do not redefine it
      (unless (fboundp tp-name)
        (%make-prop-get-accessor id tp-name)
        (%make-prop-set-accessor id tp-name)
        )
      
      ;; when editing
      (save-new-id id)
      
      ;; return tp-id
      id)))

#|
(setq *language* :en)
(catch :error 
       (with-context 0
       (moss::%make-generic-tp 
        '(:fr "titre principal; titre de chapitre" :en "chapter title")
        '(:default :none) '(:doc (:fr "texte" :en "text")) '(:unique))))
$T-CHAPTER-TITLE
(($TYPE (0 $EPT)) ($ID (0 $T-CHAPTER-TITLE))
 ($PNAM (0 ((:FR "titre principal" "titre de chapitre") (:EN "chapter title"))))
 ($ETLS.OF (0 $SYS.1))
 ($INV (0 $T-CHAPTER-TITLE.OF)) ($DEFT (0 :NONE)) 
 ($DOCT (0 ((:FR "texte") (:EN "text"))))
 ($MINT (0 1)) ($MAXT (0 1)))

HAS-CHAPTER-TITLE
(($TYPE (0 $EP)) ($ID (0 HAS-CHAPTER-TITLE)) ($PNAM.OF (0 $T-CHAPTER-TITLE))
 ($EPLS.OF (0 $SYS.1)))

HAS-TITRE-DE-CHAPITRE
(($TYPE (0 $EP)) ($PNAM.OF (0 $T-CHAPTER-TITLE)) ($EPLS.OF (0 $SYS.1)))
(($TYPE (0 $EP)) ($ID (0 HAS-TITRE-DE-CHAPITRE)) ($PNAM.OF (0 $T-CHAPTER-TITLE))
 ($EPLS.OF (0 $SYS.1)))

(catch :error
 (with-package :test 
  (with-language :all 
    (moss::%make-generic-tp '(:fr "essai 1" :en "test 1")))))

TEST::$T-TEST-1
(($TYPE (0 $EPT)) ($ID (0 TEST::$T-TEST-1)) 
 ($PNAM (0 ((:FR "essai 1") (:EN "test 1"))))
 ($ETLS.OF (0 TEST::$SYS.1)) ($INV (0 TEST::$T-TEST-1.OF)))

TEST::$T-TEST-1
(($TYPE (0 $EPT)) ($ID (0 TEST::$T-TEST-1)) 
 ($PNAM (0 ((:FR "essai 1") (:EN "test 1"))))
 ($ETLS.OF (0 TEST::$SYS.1)) ($INV (0 TEST::$T-TEST-1.OF)))

TEST::HAS-TEST-1
(($TYPE (0 $EP)) ($ID (0 TEST::HAS-TEST-1)) ($PNAM.OF (0 TEST::$T-TEST-1)) 
 ($EPLS.OF (0 TEST::$SYS.1)))

TEST::HAS-ESSAI-1
(($TYPE (0 $EP)) ($ID (0 TEST::HAS-ESSAI-1)) ($PNAM.OF (0 TEST::$T-TEST-1))
 ($EPLS.OF (0 TEST::$SYS.1)))

TEST::IS-TEST-1-OF
(($TYPE (0 $EP)) ($ID (0 TEST::IS-TEST-1-OF)) ($INAM.OF (0 TEST::$T-TEST-1.OF))
 ($EPLS.OF (0 TEST::$SYS.1)))

TEST::IS-ESSAI-1-OF
(($TYPE (0 $EP)) ($ID (0 TEST::IS-ESSAI-1-OF)) ($INAM.OF (0 TEST::$T-TEST-1.OF))
 ($EPLS.OF (0 TEST::$SYS.1)))

(catch :error 
         (moss::%make-generic-tp 
          '(:fr "titre principal; titre de chapitre" :en "chapter title")
          '(:default :none) '(:doc (:fr "texte" :en "text")) '(:unique)))
Warning: reusing previously defined generic attribute HAS-CHAPTER-TITLE, in package
         #<The MOSS package> and context 0 ignoring eventual options (class: NIL).
$T-CHAPTER-TITLE

(%make-generic-tp "Second-name" '(:entry))
$T-SECOND-NAME
(($TYPE (0 $EPT)) ($ID (0 $T-SECOND-NAME)) ($PNAM (0 ((:EN "Second-name")))) 
 ($ETLS.OF (0 $SYS.1))
 ($INV (0 $T-SECOND-NAME.OF)) ($OMS (0 $FN.133)))

$FN.133
(($TYPE (0 $FN)) ($ID (0 $FN.133)) ($MNAM (0 =MAKE-ENTRY)) ($FNLS.OF (0 $SYS.1))
 ($OMS.OF (0 $T-SECOND-NAME)) ($FNAM (0 DEFAULT-MAKE-ENTRY)))

Version tests
(setq *version-graph*'((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))
*version-graph*
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3
(with-context 2
  (catch :error 
       (%make-generic-tp 
        '(:fr "titre principal; titre de chapitre" :en "chapter title")
        '(:default :none) '(:doc (:fr "texte" :en "text")) '(:unique))))
Warning: reusing previously defined generic attribute HAS-CHAPTER-TITLE, in package
         #<The MOSS package> and context 2 ignoring eventual options (class: NIL).
$T-CHAPTER-TITLE

(catch :error
       (%make-generic-tp '(:fr "section2") '(:min 1) '(:context 2)))
$T-SECTION
(($TYPE (2 $EPT)) ($ID (2 $T-SECTION)) ($PNAM (2 ((:FR "section") (:EN "section"))))
 ($ETLS.OF (0 $SYS.1)) ($INV (0 $T-SECTION.OF)) ($MINT (2 1)))

HAS-SECTION
(($TYPE (0 $EP)) ($ID (0 HAS-SECTION)) ($PNAM.OF (0 NIL)) ($EPLS.OF (0 $SYS.1)))
|#
;;;-------------------------------------------------------------------- %MAKE-TP

;;; make-tp creates a terminal property. Several cases occur:
;;;  - if the property does not exist, then
;;;    - first a generic property is created
;;;    - then if a class is specified, we create a specific local class property,
;;;      sub-property of the generic property with the same property name.

;;; The first argument to %make-tp is either a simple string or a multilingual-
;;; name.
;;;    The latter case is reserved for creating a new property.
;;;    The first case is for reusing an already existing property. Thus the
;;; simple string must reference the generic property.
;;; A problem may occur when defining a new property with a list of synonyms that
;;; are not already taken into account by the generic property. The new synonyms
;;; should be merged at the generic property level. Not implemented yet.

;;; when used at run time in competing environments, context and package must
;;; be specified as local variables, since they are shared by concurrent processes

(defUn %make-tp (tp-ref &rest option-list)
  "(%make-tp tp-ref &rest option-list) - tp-ref is the external attribute name ~
   (should be a reference).
Arguments:
   tp-ref: string, symbol or mln (:fr \"Titre\" :en \" title ; header\") ~
   Will produce HAS-TITLE and a settable HAS-TITLE accessor.
           (:fr \"Titre\") will produce HAS-TITRE !
   Option list:
     (:class-ref <class-ref>) to attach to a class
     (:context <number>       must be a legal context (currently ignored)
     (:default <value>)	      default value
     (:doc <doc-string>)      documentation
     (:entry {<function-descriptor>})	specify entry-point
           if no arg uses make-entry, otherwise uses specified function
               where 
               <function-descriptor>::=<arg-list><doc><body>
	       e.g.  (:entry (value-list) \"Entry for Company name\"
	       		(intern (make-name value-list)))
     (:min <number>)	       minimal cardinality
     (:max <number>)	       maximal cardinality
     (:type <type>)            values should be of type :number, :string, or :mln
     (:not-type <type>)         values should not be of that type
     (:value <value>)          value should be that value, a way of imposing a value
                               defined at the property level
     (:unique)		       minimal and maximal cardinality are each 1
     (:one-of <instances>+)    list of specific values
     (:forall)                 all values should be taken from the one-of list or of
                               the specified type
     (:exists)                 there should be at least one value taken from the one-of
                               list or of the specified type        
     (:not-in)                 no value should be in the one-of list
     (:same)                   all values should be identical
     (:different)              all values should be different
     (:between <value> <value>) all values should be numerical and in this range
     (:outside <value> <values>) all values should be numerica and outside that range
  options type, not-type, value, one-of/forall are strict, meaning that if the values
  do not comply with the restriction, they are not recorded.
  Done as follows
	- check if local property already exists - if so, then error
Return:
   the prop id."
  (declare (special *attribute-options* *language*))
  ;; check for format error in the list of options
  (unless (every #'listp option-list)
    (error "every option should be a list in: ~%  ~S" option-list))
  
  ;(format t "~%; %make-tp / option-list: ~S" option-list)
  ;(format t "~%; %make-tp / *attribute-options* ~S" *attribute-options*)
  ;(format t "~%; %make-tp / intersection: ~S" 
  ;  (set-difference (mapcar #'car option-list) *attribute-options*))
  
  (when (set-difference (mapcar #'car option-list) *attribute-options*)
    (error 
      (format nil "Illegal attribute options: ~S when defining ~S"
        (set-difference (mapcar #'car option-list) *attribute-options*)
        tp-ref)))             
  
  (let* ((context (symbol-value (intern "*CONTEXT*"))) ; context is current
         (class-ref (car (or (getv :class-ref option-list)
                             (getv :concept option-list))))
         (id (car (getv :id option-list))) ; JPB1607
         tp-mln tp-name class-id)
    ;; if tp-ref is not a string or symbol make sure it has the right mln format
    (unless (or (stringp tp-ref)(symbolp tp-ref))
      (setq tp-ref (mln::make-mln tp-ref)))
    
    ;;=== then check global parameters
    ;; throw to :error when something wrong, tp-ref used in error messages
    (%check-package-version-language *package* context *language* tp-ref)
    
    ;; get something like (:fr "sous titre") - result is garanteed
    ;; this allows using simple strings with synonyms
    (setq tp-mln (mln::make-mln tp-ref)) ; jpb1406
    
    ;; pick up one of the names for creating attribute name, e.g. HAS-TITLE
    ;; works with string, mln and symbols like TITLE or HAS-TITLE
    (setq tp-name (%%make-name tp-mln :tp))
    
    
    ;; then, if we have no class option, we return after creating generic prop
    (unless class-ref 
      (return-from %make-tp (apply #'%make-generic-tp tp-mln option-list)))
    
    ;;=== Possible errors when a class is specified
    ;;== 1. inexisting class
    (setq class-id (%%get-id class-ref :class))
    ;; if id is not that of a class, then error
    (unless class-id 
      (terror "when defining attribute ~S in package ~S, unknown class reference: ~S" 
              tp-ref (package-name *package*) class-ref))
    
    ;;== 2. internal local tp-id associated with class already exists
    ;; Now produce the local ID, e.g. $T-BOOK-TITLE
    (setq id (or id (%make-id-for-class-tp tp-ref class-ref))) ; JPB1607
    ;; then check if id already exists. 
    (when (%pdm? id)
      (terror "when defining attribute ~S in package ~S, ~
               for class ~A, ~&internal id ~S already exists" 
              tp-ref (package-name *package*) class-ref id))
    ;;=== End major errors
    
    ;; we create a specific terminal property for the specified class
    ;; and link it as a sub-property to the generic property 
    ;; Property name is the same as the generic one, id is different. Options
    ;; may be different and normally SHOULD shadow those of the generic options.
    ;; However, we do not check 
    ;; Inverse terminal property is that of the local property, however name is 
    ;; the same as generic property, e.g. is-first-name-of, and not 
    ;; is-person-first-name-of
    
    ;; call the required function 
    (apply #'%%make-tp-from-ids tp-mln tp-name id class-id option-list)
    ))

#|
(catch :error 
    (%make-tp '(:fr "titre principal; titre de chapitre" :en "chapter title")
              '(:default :none) '(:doc (:fr "texte" :en "text")) '(:unique)))
$T-CHAPTER-TITLE

(catch :error 
    (%make-tp '(:fr "titre principal; titre de chapitre" :en "chapter title")
              '(:default :none) '(:class-ref "livre") '(:doc (:fr "texte" :en "text")) 
              '(:unique)))
";***Error when defining attribute ((:FR
                                    \"titre principal\"
                                    \"titre de chapitre\")
                                   (:EN
                                    \"chapter title\")) in package \"MOSS\",
  unknown class reference: \"livre\""

(%deftp "first-name" (:one-of "Albert" "Victoria" "Julie")(:exists)
         (:class-ref "butcher"))
$T-BUTCHER-FIRST-NAME

(($TYPE (0 $EPT)) ($ID (0 $T-BUTCHER-FIRST-NAME)) ($PNAM (0 (:EN "first-name")))
 ($ETLS.OF (0 $SYS.1)) ($IS-A (0 $T-FIRST-NAME)) ($INV (0 $T-FIRST-NAME.OF))
 ($ONEOF (0 "Albert" "Victoria" "Julie")) ($SEL (0 :EXISTs)) ($PT.OF (0 $E-BUTCHER)))
|#
;;;---------------------------------------------------------- %%MAKE-TP-FROM-IDS
;;; replacing %make-name-for-class
 
(defUn %%make-tp-from-ids 
    (tp-mln tp-name id class-id &rest option-list)
  "low level function to create property and handle options ~
   while creating an attribute of a specific class. Called by %make-tp.
   Should not be called directly.
Arguments:
   tp-mln: MLN of the attribute being created, e.g. ((:en \"title\"))
   tp-name: symbol specifying the attribute being created, e.g. HAS-TITLE
   id: local TP id, e.g. $T-BOOK-TITLE
   class-id: identifier of the class of the attribute, e.g. $E-BOOK
   option-list (rest): list of original options
Return:
   the attribute id or NIL in case of error"
  ;; check just in case
  (unless class-id
    (warn "missing class-id when trying to link attribute ~S in package ~S context ~S"
      tp-name (package-name *package*) (symbol-value (intern "*CONTEXT*")))
    (return-from %%make-tp-from-ids nil))
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        ;; now get class-name associated with $ENAM (multilingual-string) -> BOOK
        (class-name (%make-name-for-class (car (%get-value class-id '$ENAM))))
        gen-id prop-list inv-id)
    
    ;; Now check if we have properties with same prop name, e.g. HAS-TITLE
    ;; works even when HAS-TITLE is unbound (not yet created)
    (setq prop-list (%extract-from-id tp-name '$PNAM `$EPT))
    ;; entry-points like HAS-TITLE are valid across packages, thus we get a list
    ;; of properties in various packages, that we must filter 
    (setq prop-list (%filter-against-package prop-list *package*))
    ;; we must also eliminate any property that is not an attribute
    ;; we could obtain ($T-TITLE $T-MAGAZINE-TITLE $T-PERSON-TITLE) or NIL
    (setq prop-list (remove-if #'(lambda (xx) (not (%is-attribute? xx))) prop-list))
    
    ;; ... then get generic one, otherwise create it
    (setq gen-id
          (or (%get-generic-property prop-list) ; nil, if prop-list is nil
              (apply #'%make-generic-tp tp-mln option-list)))
    
    ;; create property object
    (set id  `(($TYPE (,context $EPT))
               ($ID (,context ,id))
               ($PNAM (,context ,tp-mln))))
    
    ;; save new object id when editing
    (save-new-id id)   
    
    ;; add new property-id to system-list
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ETLS id)
    ;; declare it as a sub-property of generic property
    (%link id '$IS-A gen-id)
    ;; build all entry-points for each synonyms if they do not exist already
    (%make-ep-from-mln tp-mln '$PNAM id :type '$EPT)
    
    ;; ...
    ;; Now we consider inverse property. Simply build it with entry points
    (setq inv-id (%make-inverse-property id tp-mln))
    ;; when editing
    (save-new-id inv-id)
    
    ;; process now option list
    (while option-list
           (case (caar option-list)
             ;; link to class if it was specified in the options
             ((:class-ref :concept) 
              ;; we already processed those options
              (%link class-id '$PT id)
              )
             (:default
                 ;; default is normally attached to the ideal of the corresponding class
                 ;; Defaults attached to ideals shadow defaults attached to properties. 
                 (%%set-value-list (%make-id-for-ideal class-id)
                                   (cdar option-list) id context)
                 ;; when editing saved in %%set function
                 ;;;          #+OMAS
                 ;;;          (%save-id-when-omas-agent-is-editing (%make-id-for-ideal class-id))
                 )
             ;; Record documentation if specified
             (:doc	
              (%process-doc-option id (cdar option-list))
              )
             ;; entry option allows us to associate entry-points with attributes
             ;; can be different from the generic one
             
             ((:entry :index)
              (if (cdar option-list)
                  ;; specific function defined in the option
                  (apply #'%make-ownmethod '=make-entry id (cdar option-list))
                ;; no function specified, attach default function
                (%make-ownmethod :default-make-entry id nil nil))
              )
             (:min
              (%%set-value id (cadar option-list) '$MINT context)
              )
             (:max
              (%%set-value id (cadar option-list) '$MAXT context)
              )
             (:unique	;; has both min and max to 1
              (%%set-value id 1 '$MINT context)
              (%%set-value id 1 '$MAXT context)
              )
             (:type   ;; set type of value
              (%%set-value id (cadar option-list) '$TPRT context)
              )
             (:not-type  ;; must not be of that type
              (%%set-value id (cadar option-list) '$NTPR context)
              )
             (:value  ;; a way to impose a value defined at property level
              (%%set-value id (cadar option-list) '$VALR context)
              )
             ;; if values are specified, record them
             (:one-of
              ;; we should make sure that the values are of the right type and/or
              ;; execute the =xi method
              (%%set-value-list id (cdar option-list) '$ONEOF context)
              ;; following test sets :one-of as a default selection
              (unless (or (%%has-value id '$SEL context)
                          (assoc :exists option-list)
                          (assoc :forall option-list)
                          (assoc :not-in option-list))
                (%%set-value id :exists '$SEL context)))
             ;; :exists and :forall make sense only when :one-of is specified
             (:exists
              (if (or (%%has-value id '$ONEOF context)
                      (assoc :one-of option-list))
                  (%%set-value id :exists '$SEL context)
                (terror ":exists option requires the use of the :one-of option in ~S ~
                    for ~S" tp-name class-name))
              )
             (:forall 
              (if (or (%%has-value id '$ONEOF context)
                      (assoc :one-of option-list))
                  (%%set-value id :forall  '$SEL context)
                (terror ":forall  option requires the use of the :one-of option in ~S ~
                    for ~S" tp-name class-name))
              )
             (:not-in 
              (if (or (%%has-value id '$ONEOF context)
                      (assoc :one-of option-list))
                  (%%set-value id :not '$SEL context)
                (terror ":not-in  option requires the use of the :one-of option in ~S ~
                    for ~S" tp-name class-name))
              )
             ((:between :outside) ; apply to a single value
                  (%%set-value-list id (car option-list) '$OPR context)
              )
             ((:same :different) ; apply to a set of values
                  (%%set-value-list id (car option-list) '$OPRALL context)
              )
             ) ; end case
           (pop option-list)
           )
    ;; build external-name for temporary use only (current session)
    ;; build names for each synonym associated with the local property
    ;; we save temporary variables, in case of abort
    (mapc #'(lambda (xx)
              (let ((var-id 
                     (%make-name-for-variable 
                      (%make-name-string-for-concept-property xx class-name)
                      )))
                (set var-id id)
                ;; when editing
                (save-new-id var-id)
                ))
      (mln::extract-all-synonyms tp-mln))
    
    ;; return tp-id
    id))

#|
(catch :error (defconcept "person"))
$E-PERSON

(defattribute "age") ; create the generic property
$T-AGE

(moss::%%make-tp-from-ids '((:en "age")) 'HAS-AGE '$T-PERSON-AGE
                    '$E-PERSON '(:max 5) '(:entry))
$T-PERSON-AGE
(($TYPE (0 $EPT)) ($ID (0 $T-PERSON-AGE)) ($PNAM (0 ((:EN "age")))) 
 ($ETLS.OF (0 $SYS.1)) ($IS-A (0 $T-AGE)) ($INV (0 $T-PERSON-AGE.OF))
 ($MAXT (0 5)) ($OMS (0 $FN.133)))

(defconcept "warning" (:att "color" (:one-of "red" "orange") (:exists)))
$E-WARNING
(($TYPE (0 $ENT)) ($ID (0 $E-WARNING)) 
 ($ENAM (0 ((:EN "warning")))) 
 ($RDX (0 $E-WARNING))
 ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-WARNING.CTR))
 ($PT (0 $T-WARNING-COLOR)))

$T-WARNING-COLOR
(($TYPE (0 $EPT)) ($ID (0 $T-WARNING-COLOR)) 
 ($PNAM (0 ((:EN "color")))) ($ETLS.OF (0 $SYS.1))
 ($IS-A (0 $T-COLOR)) ($INV (0 $T-WARNING-COLOR.OF)) 
 ($PT.OF (0 $E-WARNING))
 ($ONEOF (0 "red" "orange")) ($SEL (0 :EXISTS)))

(with-package :test
  (with-context 3
    (moss::%%make-tp-from-ids '((:en "age")) 'HAS-AGE '$T-PERSON-AGE
                              '$E-PERSON '(:max 5) '(:entry))))
$T-PERSON-AGE
(($TYPE (3 $EPT)) ($ID (3 $T-PERSON-AGE)) ($PNAM (3 ((:EN "age"))))
 ($ETLS.OF (0 TEST::$SYS.1))
 ($IS-A (0 TEST::$T-AGE)) ($INV (0 $T-PERSON-AGE.OF)) ($MAXT (3 5)) 
 ($OMS (0 TEST::$FN.10)))
;; BUG?: overwrites the previous value of the attribute
|#
;;;=============================================================================
;;;
;;;                RELATIONS (Structural Properties / sp)
;;;
;;;=============================================================================

;;; (%defsp name key class suc &rest option-list) - creates a terminal property
;;; which is a link between class and suc. If classes do not exist it is an
;;; error
;;;     class can be the name of the class or the list (:class-id class-id). This
;;;     is used by defclass when creating new relationships
;;; Syntax of options is
;;;	(:is-a <class-id>*)	for defining inheritance
;;;	(:min <number>)		minimal cardinality
;;;	(:max <number>)		maximal cardinality
;;;	(:unique)		minimal and maximal cardinality are each 1
;;;     (:var <var-name>)       variable name to record prop id
;;;     (:doc <documentation>)  documentation (can be multilingual)
;;;  and (?)
;;;     (:one-of ...)
;;;     (:default ...)
;;;
;;; The :one-of option implies that the instances of objects are available at the 
;;; time the class is created, which is difficult since the instances are of 
;;; the same class as the one being defined.
;;; Example: creating the class professor with a default as $E-PROFESSOR.1
;;; The problem may be solved if one first creates the class and then the instances
;;; using a :new option, and then links the instances to the class...
;;; The ;default option has the same problem, however, one must link the instances
;;; to the ideal of the class.

;;;------------------------------------------------------------ %MAKE-GENERIC-SP

(defUn %make-generic-sp (sp-ref &rest option-list)
  "creates a generic structural property, able to link any object to any object.
   This function is only called while processing a local property.
   Options are ignored.
Arguments:
   sp-ref: name of property, e.g. FATHER, \"father\" or (:en \"father\")
   option-list (opt): list of options (ignored)
Returns:
   the id of the generic property, e.g. $S-SON (applications)"
  ;(declare (ignore option-list))

  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (sp-mln (mln::make-mln sp-ref))
         (sp-name (%make-name-for-relation sp-ref))
         id)
    ;; check first if we have a structural property with this name
    (setq id (%%get-id sp-name :sp))

    ;; for the time being we are happy if property exists
    (when id
      (warn "reusing previously defined generic relation ~A, in package ~S ~
             and context ~S ignoring eventual options (class-ref: ~S)." 
            sp-name (package-name *package*) context (cadr (assoc :from option-list)))
      (return-from %make-generic-sp id))

    ;; here, the property does not exist, we must create it 
    
    ;; first cook up the sp key. 
    (setq id (%make-id-for-sp sp-mln))
    ;; check if id already exists defining a different property
    ;; it is allowed to exist but only as a structural property
    (when (%pdm? id)
      (terror "~&;***Error when defining relation ~& ~S in package ~S, ~
               ~& internal name ~S already exists but is not a relation: ~S"
              sp-ref (package-name *package*) id (eval id)))
    
    ;; otherwise create new object
    (set id  `(($TYPE (,context $EPS))
               ($ID (,context ,id))
               ($PNAM (,context ,sp-mln))
               ))			; inverse prop is missing yet
    
    ;; when editing
    (save-new-id id)

    ;; add new property-id to system-list
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ESLS id)

    ;; add it into universal class (representing any object)
    ;; the rationale is that the relation can be specialized to hold between any
    ;; two objects
    (%link (intern "*ANY*") '$PS id)
    ;; and into suc
    (%link id '$SUC (intern "*ANY*"))

    ;; build entry-points one for each synonym
    (%make-ep-from-mln sp-mln '$PNAM id :type '$EPS)
    
    ;; Now we consider inverse property
    (%make-inverse-property id sp-mln)
    
    ;; build external-name for temporary use only (current session)
    (set (%make-name-for-variable sp-name) id)

    ;; when editing
    (save-new-id (%make-name-for-variable sp-name))
    
    ;; create access function unless engine is not loaded yet 
    ;; add now a macro to allow to use property name as a function in methods
    (unless (fboundp sp-name)
      (%make-prop-get-accessor id sp-name)
      (%make-prop-set-accessor id sp-name)
      )

    ;; return sp-id
    id))

#|
(moss::%make-generic-sp "cousin")
$S-COUSIN

((MOSS::$TYPE (0 MOSS::$EPS)) (MOSS::$ID (0 $S-COUSIN))
 (MOSS::$PNAM (0 (:EN "cousin"))) (MOSS::$ESLS.OF (0 $E-SYSTEM.1))
 (MOSS::$PS.OF (0 MOSS::*ANY*)) (MOSS::$SUC (0 MOSS::*ANY*))
 (MOSS::$INV (0 $S-COUSIN.OF)))

HAS-COUSIN
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 HAS-COUSIN))
 (MOSS::$PNAM.OF (0 $S-COUSIN)) (MOSS::$EPLS.OF (0 $E-SYSTEM.1)))

(moss::%make-generic-sp '(:fr "voisin" :en "neighbour"))
$S-NEIGHBOUR ; does not exist if the language is not :EN, but HAS-NEIGHBOUR exists
If *language* is :EN, then:

$S-NEIGHBOUR
(($TYPE (0 $EPS)) ($PNAM (0 (:FR "voisin" :EN "neighbour"))) ($PS.OF (0 *ANY*))
 ($SUC (0 *ANY*)) ($ESLS.OF (0 $SYS.1)) ($INV (0 $S-NEIGHBOUR.OF)))

$S-NEIGHBOUR.OF
(($TYPE (0 $EIL)) ($INAM (0 "IS-NEIGHBOUR-OF")) ($INV.OF (0 $S-NEIGHBOUR))
 ($EILS.OF (0 $SYS.1)))

IS-NEIGHBOUR-OF
(($TYPE (0 $EP)) ($INAM.OF (0 $S-NEIGHBOUR.OF)) ($EPLS.OF (0 $SYS.1)))

HAS-voisin
(($TYPE (0 $EP)) ($PNAM.OF (0 $S-NEIGHBOUR)) ($EPLS.OF (0 $SYS.1)))
|#
;;;------------------------------------------------------------- %MAKE-GLOBAL-SP
;;; this function is called if
;;;   - we have :any as a domain specification and range specification
;;; in both cases, we process the options
;;; The function is similar to %make-generic-sp, but processes the option list.
;;; When the function is called, class-id and suc-id should be defined; thus, the
;;; default option can be processed
;;; Used by %make-sp

(defUn %make-global-sp (multilingual-name class-id suc-id &rest option-list)
  "creates a global structural property taking options into account.
Arguments:
   multilingual-name: name of property, e.g. FATHER
   option-list (opt): list of options
 Syntax of options is
        (:id <id>)              prop id (synthesized if not present)
                                     reserved for system
	(:min <number>)		minimal cardinality
	(:max <number>)		maximal cardinality
	(:unique)		minimal and maximal cardinality are each 1
	(:inv <prop-name>)	inverse-property name
        (:var <var-name>)       variable name to record prop id
        (:class-id <class-id>)  id of owner of property
        (:class <class-name>)   name of owner class
        (:suc-id <suc-id>)      id of the successor class
Returns:
   the id of the generic property, e.g. $ELS (system) or $S-SON (applications)"
  (unless (mln::%mln? multilingual-name)
    (setq multilingual-name (mln::make-mln multilingual-name)))
  
  ;; error if not mln format
  (unless (mln::mln? multilingual-name) ; jpb 1406
    (terror "(%make-generic-sp) property; ~S is not a multilingual name." 
            multilingual-name))
  
  ;; if forward references are allowed record action onto the *deferred-links* list
  (when *allow-forward-references*
    (push `(apply #'%make-global-sp ',multilingual-name ',class-id ',suc-id 
                  ',option-list)
          *deferred-links*)
    (return-from %make-global-sp nil))
  
  ;; otherwise create property
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (sp-mln (mln::extract-mln-from-ref multilingual-name *language*))
         (sp-name (%make-name-for-property sp-mln))
         prop-list id)
    
    ;; check first if we have a structural property with this name
    (setq prop-list (%extract-from-id sp-name '$PNAM '$EPS))
    ;; return generic property, if property exists
    (if prop-list
      (return-from %make-global-sp (%get-generic-property prop-list)))
    
    ;;=== create property
    ;; first cook up the sp key. We can leave the :id option on the option-list
    ;; it will be ignored during the processing of options
    (setq id (or (car (getv :id option-list)) 
                 (%make-id-for-sp sp-mln)))
    
    ;; check if id already exists defining a different property
    ;; it is allowed to exist but only as a structural property
    (if (boundp id)
      (terror "~&;***Error when defining structural property~& ~S in package ~S, ~
               ~& internal symbol ~S already exists and bound : ~&~S" 
              multilingual-name *package* id (eval id)))
    
    ;; create new object
    (set id  `(($TYPE (,context $EPS))
               ($ID (,context ,id))
               ($PNAM (,context ,sp-mln))
               ))			; inverse prop is missing yet
    
    ;; when editing
    (save-new-id id)

    ;; add it into universal class (representing any object)
    ;; the rationale is that the relation can be specialized to hold between any
    ;; two objects
    (%link class-id '$PS id)
    ;; and into suc
    (%link id '$SUC suc-id)
    ;; add new property-id to system-list
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ESLS id)
    
    ;; build entry-points one for each synonym
    (%make-ep-from-mln sp-mln '$PNAM id :type '$EPS)
    
    ;; ...
    ;; Now we consider inverse property
    (%make-inverse-property id sp-mln)
    
    ;;=== process now OPTION LIST; not when defining a generic property
    (while option-list
      (case (caar option-list)
        ;; skip some options
        ((:language :package :context :export))
        (:is-a	;; superproperty must exist (this is done to catch
         ;; spelling mistakes)
         (warn ":is-a option disallowed as yet ***")
         )
        (:min
         (%%set-value id (cadar option-list) '$MINT context)
         )
        (:max
         (%%set-value id (cadar option-list) '$MAXT context)
         )
        (:unique	;; has both min and max to 1
         (%%set-value id 1 '$MINT context)
         (%%set-value id 1 '$MAXT context)
         )
        ;; we can specify an external variable name
        (:var  
         (if (symbolp (cadar option-list))
           (set (cadar option-list) id))
         )
        ;; Record documentation if specified
        (:doc	 
         (%process-doc-option id (cdar option-list))
         )
        ;; process default option
        (:default
          (apply #'%%make-sp-default-id id class-id suc-id sp-name option-list))
        ;; because generic property is linked with *any* no check on defaults
        ;; when there is an owner class, default is attached to ideal
        
        ) ; end case
      (pop option-list))
    
    ;; build external-name for temporary use only (current session)
    (set (%make-name-for-variable sp-name) id)
    
    ;; when editing
    (save-new-id (%%make-name sp-name :var)) ; JPB1507
    
    ;; create access function unless engine is not loaded yet 
    ;(when *moss-engine-loaded*
    ;; this is for defining application properties, engine is loaded
    ;; add now a function to allow to use property name in methods
    ;; If already defined (e.g. by MOSS) do not redefine it
    (unless (fboundp sp-name)
      (%make-prop-get-accessor id sp-name)
      (%make-prop-set-accessor id sp-name)
      )
    ;; return sp-id
    id))

#|
(%make-global-sp '(:en "test" :fr "F-test") '*any* 'test::$E-PERSON
                   '(:max 2) '(:doc :en "test function")'(:var _t1))
$S-TEST

? $S-TEST
(($TYPE (0 $EPS)) ($ID (0 $S-TEST)) ($PNAM (0 (:EN "test"))) ($PS.OF (0 *ANY*))
 ($SUC (0 FAMILY::$E-PERSON)) ($ESLS.OF (0 $SYS.1)) ($INV (0 $S-TEST.OF))
 ($MAXT (0 2)) ($DOCT (0 (:EN "test function"))))
? has-test
(($TYPE (0 $EP)) ($ID (0 HAS-TEST)) ($PNAM.OF (0 $S-TEST)) ($EPLS.OF (0 $SYS.1)))

|#
;;;-------------------------------------------------------------------- %MAKE-SP
#|
We have a problem with the current defrelation format.
Indeed, defrelation requires that a class and a successor be given, meaning that 
defrelation cannot be defined as a generic relation directly.
Furthermore, we should have a more flexible syntax like:
 (defrelation NEAR (:from town) (:to town)...)
or
 (defrelation NEAR (:from town) (:to town) (:one-of _bordeaux _paris)...)
or
 (defrelation NEAR (:from :any) (:to :any)...)
or
 (defrelation NEAR ...) ; equivalent to the last one
If the relation does not contain domain nor range, then it is an error.

Another point is: do we impose restrictions only on the range of the property or
also on the domain?
Can we write 
 (defrelation DISLIKE (:from CLYDE) (:to MOUSE)...)
where CLYDE is an instance of ELEPHANT or should we write
 (defrelation DISLIKE (:from ELEPHANT) (:to MOUSE)...)
and link CLYDE to the typical MOUSE?

A first answer is that the relation is an abstraction of the link between two objects
Hence, it cannot specify an instance as a domain unless the interpretation is 
:one-of. A better approach would be to use an option like :domain-one-of

If we write
 (defrelation DISLIKE (:from ELEPHANT) (:to :any)...)
then this allows us to state that CLYDE does not like MICKEY or the class MOUSE, 
which is not the same as linking CLYDE to the MOUSE ideal representing a typical 
mouse.

100128 We impose the (:from xxx)(:to YYY) syntax. Positional syntax is now discarded

Finally we end up with several possible formats:
 - if specified within a class: domain is a class, range is class or :any meaning 
any class or any object
 - if defined directly using defrelation, then domain can be class or :any and 
range can be class or any. For generic properties domain and range are :any.

When the domain is different from :any the id of the relation is $S-<class>-<prop>
When the domain is :any, then the relation id is $S-<prop>, meaning that the relation
is not attached to anything.
|#

(defUn %make-sp (sp-ref &rest option-list)
  "(%make-sp name key class suc &rest option-list) - creates a structural property, ~
   which is a link between class and suc. If class or successor does not exist, it ~
   is an error. class is contained in the :from option, suc in the :to option.
arguments:
   sp-ref: reference, e.g. symbol, string or mln
   option list:
        (:from <class-ref>)   ref of domain class
        (:to <suc-ref>)       ref to identify successor class
	(:min <number>)	      minimal cardinality
	(:max <number>)	      maximal cardinality
	(:unique)             minimal and maximal cardinality are each 1
        (:one-of <list>)      successor must be one of the objects
        (:exists)             indicates that one of the values should be in the list
        (:forall)             indicates that all successors should be in the list
        (:default <list>)     gives defaults successors
Return:
   the id of the property (generic or local)."
  (declare (special *any*))
  ;; check for format error in the list of options
  (unless (every #'listp option-list)
    (error "every option should be a list in: ~%  ~S" option-list))

  (let ((context (symbol-value (intern "*CONTEXT*")))
        (class-ref (cadr (assoc :from option-list)))
        (suc-ref (cadr (assoc :to option-list)))
        id class-id suc-id sp-mln sp-name)
    
    ;; if we use defsp or defrelation while allowing forward references, then we must
    ;; defer the links
    (when *allow-forward-references*
      (setq *deferred-mode* :class)
      (push `(with-environment ',*package* ',context ',*language*
                   (apply #'%make-sp ',sp-ref ',option-list))
            *deferred-links*)
      (return-from %make-sp nil))
    
    ;; make sure sp-ref has the proper format
    (unless (or (stringp sp-ref)(symbolp sp-ref))
      (setq sp-ref (mln::make-mln sp-ref)))
    ;; throw to :error when something wrong
    (%check-package-version-language *package* context *language* sp-ref)
    
    ;; first take care of sp reference, e.g. "sous titre" 'SOUS-TITRE or mln
    (setq sp-mln (mln::make-mln sp-ref)) ; jpb1406
    
    ;; make internal relation name, e.g. HAS-BROTHER
    (setq sp-name (%make-name-for-relation sp-ref))
    
    ;; get domain class and range class
    (setq class-id (%%get-id class-ref :class :include-moss t)
          suc-id (%%get-id suc-ref :class :include-moss t))
    
    ;; declare an error if suc-id or class-id is null. The rationale is:
    ;; - either the function is called from a defrelation and the class
    ;;   and successor should have already been defined, or be :any
    ;; - or the function is called from defconcept, and if suc-id does not
    ;;   exist, then it will be deferred by %make-concept, and called at
    ;;   deferred time.
    ;;   If suc-id does not exist at deferred time, then this is an error
    (cond
     ((null suc-id) ; should be *any* or something
      (terror "while defining relation ~A, unknown successor class ~A."
              sp-name suc-ref))
     ((null class-id) ; should be *any* or something
      (terror "while defining relation ~A, unknown owner class ~A."
              sp-name class-ref)))
    
    ;;=== check special case
    ;; if the domain is :any or UNIVERSAL-CLASS, then we want to define a generic
    ;; relation, i.e. a relation valid for any object
    ;; if the relation is a system relation we create a global relation
    ;(format t "~%; %make-sp /class-id: ~S suc-id: ~S" class-id suc-id)
    (if (and (eql class-id (intern "*ANY*"))(eql suc-id (intern "*ANY*")))
      (return-from %make-sp
        (apply #'%make-global-sp sp-mln class-id suc-id option-list)))
    
    ;; we do not allow the use of *any* on the side of the class since it would
    ;; mean that any class could have this property
    (if (eql class-id (intern "*ANY*"))
        (terror "while defining relation ~A, illegal class (~A) argument.~
                 Can't add a relation to the universal class."
                sp-name class-ref))
    ;; however, we allow *any* on the successor side, meaning that the successor 
    ;; with this property could be any object
    
    ;;----- old strategy
    ;; we don't allow the use of *any* on one side only (currently)
    ;; one problem would be the position of the property wrt the generic one
    ;; if the generic prop was created with a specific class
    ;(if (or (eql class-id (intern "*ANY*"))(eql suc-id (intern "*ANY*")))
    ;    (terror "while defining relation ~A, illegal class (~A) or suc (~A) argument.~
    ;             Can't use :any on one side only."
    ;            sp-name class-ref suc-ref))
    ;;-----
    
    ;; otherwise, if domain is specified, we want to define a local relation
    
    ;;==  Check for an already existing local property (not system)
    ;; first cook up the sp key (we need class-ref)
    (setq id (%make-id-for-class-sp sp-mln class-ref))
    ;; check if id already exists defining a different property
    ;; it is allowed to exist but only as a structural property
    (when (boundp id)
      (terror "when defining relation ~S in package ~A,~& ~
               identifier ~A already exists." 
              sp-ref (package-name *package*) id)) 
    
    ;; when editing
    (save-new-id id)
    
    (apply #'%%make-sp-from-ids sp-mln sp-name id class-id suc-id option-list)
    ))

#|
(setq *language* :en)
(defconcept (:fr "Libraire" :en "Bookstore")
    (:att (:fr "nom" :en "name")(:unique)))
$E-BOOKSTORE
(($TYPE (0 $ENT)) ($ENAM (0 (:FR "Libraire" :EN "Bookstore")))
 ($RDX (0 $E-BOOKSTORE)) ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-BOOKSTORE.CTR))
 ($PT (0 $T-BOOKSTORE-NAME)))
? (defconcept (:fr "Boucher" :en "Butcher")
               (:att (:fr "nom" :en "name")(:unique)))
$E-BUTCHER
? (%make-sp "neighbour" '(:from "bookstore") '(:to "butcher"))
$S-BOOKSTORE-NEIGHBOUR
? $S-BOOKSTORE-NEIGHBOUR
(($TYPE (0 $EPS)) ($PNAM (0 (:EN "neighbour"))) ($ESLS.OF (0 $SYS.1))
 ($IS-A (0 $S-NEIGHBOUR)) ($INV (0 $S-NEIGHBOUR.OF)) ($PS.OF (0 $E-BOOKSTORE))
 ($SUC (0 $E-BUTCHER)))

? (%defsp (:fr "voisin") (:from "libraire") (:to "bookstore"))
;***Error (mln::extract-mln-from-ref) no info for language :EN in 
 ref: (:FR "voisin")
> Error: Can't throw to tag :ERROR .
> While executing: %MLN-EXTRACT-MLN-FROM-REF
> Type Command-. to abort.
See the Restart menu item for further choices.
; cant't do that since default language is English

? $E-BOOKSTORE
(($TYPE (0 $ENT)) ($ENAM (0 (:FR "Libraire" :EN "Bookstore")))
 ($RDX (0 $E-BOOKSTORE)) ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-BOOKSTORE.CTR))
 ($PT (0 $T-BOOKSTORE-NAME)) ($PS (0 $S-BOOKSTORE-NEIGHBOUR $S-BOOKSTORE-VOISIN))
 ($SUC.OF (0 $S-BOOKSTORE-VOISIN)))
? HAS-VOISIN
(($TYPE (0 $EP)) ($PNAM.OF (0 $S-NEIGHBOUR $S-BOOKSTORE-VOISIN))
 ($EPLS.OF (0 $SYS.1)))
? (catch :error (%make-sp  '(:en " the unfriendly neighbor") 
                          '(:from "bookstore") (:to "butcher")))
$S-BOOKSTORE-THE-UNFRIENDLY-NEIGHBOR

? (%make-sp "residence" '(:from person) '(:to "place") '((:max 3)))
;***Error (mln::extract-mln-from-ref) bad ref format: (:TO "place")
$S-PERSON-RESIDENCE
? $S-PERSON-RESIDENCE
(($TYPE (0 $EPS)) ($ID (0 $S-PERSON-RESIDENCE)) ($PNAM (0 (:EN "residence")))
 ($ESLS.OF (0 $SYS.1)) ($IS-A (0 $S-RESIDENCE)) ($INV (0 $S-PERSON-RESIDENCE.OF))
 ($PS.OF (0 $E-PERSON)) ($SUC (0 $E-PLACE)))
? $E-PERSON
(($TYPE (0 $ENT)) ($ID (0 $E-PERSON)) ($ENAM (0 (:EN "PERSON")))
 ($RDX (0 $E-PERSON)) ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-PERSON.CTR))
 ($PT (0 $T-PERSON-NAME)) ($PS (0 $S-PERSON-RESIDENCE)))
? $E-PLACE
(($TYPE (0 $ENT)) ($ID (0 $E-PLACE)) ($ENAM (0 (:EN "PLACE"))) ($RDX (0 $E-PLACE))
 ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-PLACE.CTR)) ($PT (0 $T-PLACE-NAME))
 ($SUC.OF (0 $S-PERSON-RESIDENCE)))
? has-RESIDENCE
(($TYPE (0 $EP)) ($ID (0 HAS-RESIDENCE))
 ($PNAM.OF (0 $S-RESIDENCE $S-PERSON-RESIDENCE)) ($EPLS.OF (0 $SYS.1)))
? is-residence-of
(($TYPE (0 $EP)) ($ID (0 IS-RESIDENCE-OF))
 ($INAM.OF (0 $S-RESIDENCE.OF $S-PERSON-RESIDENCE.OF)) ($EPLS.OF (0 $SYS.1)))

;; additional tests
? (defconcept "Test")
$E-TEST
? (defconcept "CCC")
$E-CCC

? (defrelation "rrr21" (from :any) (to :any) (:min 2)) ;global
$S-RRR21
? $S-RRR21
((MOSS::$TYPE (0 MOSS::$EPS)) (MOSS::$ID (0 $S-RRR21))
 (MOSS::$PNAM (0 (:EN "rrr21"))) (MOSS::$PS.OF (0 MOSS::*ANY*))
 (MOSS::$SUC (0 MOSS::*ANY*)) (MOSS::$ESLS.OF (0 MOSS::$SYS.1))
 (MOSS::$INV (0 $S-RRR21.OF)) (MOSS::$MINT (0 2)))
|#
;;;-------------------------------------------------------- %%MAKE-SP-DEFAULT-ID
;;; could introduce the option (:new "Condition" ...) for specifyng defaults
;;; could however lead to a deadlock
;;;**********

(defUn %%make-sp-default-id (sp-id class-id suc-id sp-name &rest option-list)
  "called o process default option. If class-id is nil or *any* we ~
   do not check the default objects class and attach them to the ~
   property. Otherwise, default objects must be instances of the ~
   class and are attached to on the ideal of class-id.
   Error if some or the defaults are not PDM objects or not instances ~
   of the specified class.
Arguments:
   sp-id: relation
   class-id: class with default
   suc-id: class of the defaults
   sp-name: name of the relation
   option-list: rest of the options (contains the defaults)
Return:
   sp with the defaults or ideal with the defaults or nil."
  ;; each default spec must evaluate to a PDM object, e.g. _jpb. Internal ids
  ;; (e.g. $E-PERSON.32) cannot be used directly or should be quoted
  ;;*** there is a risk of system error while evaluating defaults
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (any-symbol (intern "*ANY"))
        defaults)
    (catch-system-error
     ;; prepare message header in case of system error
     (format nil "while defining defaults for relation ~A" sp-name)
     
     
     (setq defaults (mapcar #'eval (getv :default option-list))))
    
    ;(print `(===> %%make-sp-default-id defaults ,defaults))
    (unless defaults 
      (warn "some of the specified defaults ~% ~
             ~S ~% might be unbound~% ~
             while defining relation ~S. We skip adding defaults..."
            (getv :default option-list) sp-name)
      (return-from %%make-sp-default-id nil))
    
    (cond
     ;; check if all defaults are PDM objects
     ((not (every #'%pdm? defaults))
      (terror "while adding relation ~A, some of the defaults are not PDM objects"
              sp-name))
     
     ;; check if domain and range are not specified, then attach defaults to relation
     ((and (or (null class-id)(eql class-id any-symbol)(eql class-id :any))
           (or (eql suc-id any-symbol)(eql suc-id :any)))
      (%%set-value-list sp-id defaults '$DEFS context)
      (symbol-value sp-id))
     
     ;; check if domain is not specified, range is specified, all defaults are
     ;; instances of range class
     ((and (or (null class-id)(eql class-id any-symbol)(eql class-id :any))
           (every #'(lambda(xx) (%is-instance-of? xx suc-id)) defaults))
      (%%set-value-list sp-id defaults '$DEFS context)
      (symbol-value sp-id))
     
     ;; check if domain is not specified, range is specified, some defaults are not
     ;; instances of range class. Give a warning
     ((or (null class-id)(eql class-id any-symbol)(eql class-id :any))
      (warn "some of the specified defaults ~% ~
             ~A ~% are not instances of the class ~A~% ~
             while defining relation ~A. We add them anyway..."
            (getv :default option-list) suc-id sp-name)
      (%%set-value-list sp-id defaults '$DEFS context)
      (symbol-value sp-id))
     
     ;; check: class is specified, but range is not, defaults are attached to
     ;; ideal
     ((or (eql suc-id any-symbol)(eql suc-id :any))
      (let ((ideal-id (%make-id-for-ideal class-id)))
        ;; when editing
        (save-new-id ideal-id)
        (%%set-value-list ideal-id defaults sp-id context)
        (symbol-value ideal-id)))
     
     ;; check that specified default are instances of suc-id or of subclasses
     ;; (could they be orphans?)
     ((every #'(lambda(xx) (%is-instance-of? xx suc-id)) defaults)
      ;; defaults attached to ideals shadow defaults attached to properties. 
      (let ((ideal-id (%make-id-for-ideal class-id)))
        ;; when editing
        (save-new-id ideal-id)
        ;; yes, attach default to ideal
        (%%set-value-list ideal-id defaults sp-id context)
        (symbol-value ideal-id)))
     
     ;; else send a warning, but record defaults anyway
     (t (warn "some of the specified defaults ~% ~
               ~A ~% are not instances of the class ~A~% ~
               while defining relation ~A. We add them anyway..."
              (getv :default option-list) suc-id sp-name)
        (let ((ideal-id (%make-id-for-ideal class-id)))
          ;; when editing
          (save-new-id ideal-id)
          ;; yes, attach default to ideal
          (%%set-value-list ideal-id 
                            (mapcar #'eval (cdar option-list)) sp-id context)
          (symbol-value ideal-id))
        ))))

#|
? (in-package :colombo)
? $E-vanne.0
(($TYPE (0 COLOMBO::$E-VANNE)) (COLOMBO::$T-VANNE-ORIENTATION (0 '(1 0 0 0)))
 (COLOMBO::$T-ETAT (0 ("normal"))) (COLOMBO::$T-VANNE-ETAT (0 "normal"))
 (COLOMBO::$T-STATUT (0 ("inconnu"))) (COLOMBO::$T-VANNE-STATUT (0 "inconnu")))

? (moss::%%make-sp-default-id '$S-VANE-MV-ACTION '$E-VANNE '$E-MV-ACTION 
                        '(:default _ouvrir-vanne _fermer-vanne))
((MOSS::$TYPE (0 $E-VANNE)) ($T-VANNE-ORIENTATION (0 '(1 0 0 0)))
 ($T-ETAT (0 ("normal"))) ($T-VANNE-ETAT (0 "normal")) ($T-STATUT (0 ("inconnu")))
 ($T-VANNE-STATUT (0 "inconnu"))
 ($S-VANE-MV-ACTION (0 $E-MV-ACTION.2 $E-MV-ACTION.3)))
|#

;;;--------------------------------------------------- %MAKE-SP-EXTRACT-CLASS-ID
;;; use rather %%get-id

#|
? (defconcept "test")
$E-TEST

? (%make-sp-extract-class-name "zzz" () ())
> Error: missing class-ref or class-id argument.
> While executing: %MAKE-SP-EXTRACT-CLASS-NAME

? (%make-sp-extract-class-name "zzz" "test" '$E-TEST)
> Error: can't specify both class-ref ("test") and class-id ($E-TEST).
> While executing: %MAKE-SP-EXTRACT-CLASS-NAME

? (%make-sp-extract-class-name "zzz" nil '$E-TEST)
MOSS::TEST
(:EN "test")
MOSS::$E-TEST

? (%make-sp-extract-class-name "zzz" nil 'colombo::$E-TEST1 :package :colombo)
; Warning: (mln::extract-mln-from-ref) default language :EN not part of ref: 
;           (:FR #1="test1")
;          We use first entry: (:FR #1#)
; While executing: %MLN-EXTRACT-MLN-FROM-REF
COLOMBO::TEST1
(:FR "test1")
COLOMBO::$E-TEST1

? (%make-sp-extract-class-name "zzz" nil 'colombo::$E-TEST1 :package :colombo 
                               :language :fr)
COLOMBO::TEST1
(:FR "test1")
COLOMBO::$E-TEST1

? (%make-sp-extract-class-name "zzz" "test1" () :package :colombo :language :fr)
TEST1
(:FR "test1")
$E-TEST1

? (%make-sp-extract-class-name "zzz" 'test ())
MOSS::TEST
(:EN "TEST")
MOSS::$E-TEST

? (%make-sp-extract-class-name "zzz" '(:name :en "test") ())
MOSS::TEST
(:EN "test")
MOSS::$E-TEST

? (%make-sp-extract-class-name "zzz" :any ())
UNIVERSAL-CLASS
(:EN "UNIVERSAL-CLASS")
MOSS::*ANY*

? (%make-sp-extract-class-name "zzz" () '*any*)
UNIVERSAL-CLASS
(:EN "UNIVERSAL-CLASS")
MOSS::*ANY*

? (%make-sp-extract-class-name "zzz" ''(:name :en "test") ())
> Error: bad syntax while defining property "zzz"
> While executing: %MAKE-SP-EXTRACT-CLASS-NAME
|#
;;;------------------------------------------------ %MAKE-SP-CHECK-ONE-OF-VALUES

(defun %make-sp-check-one-of-values (value-list suc-id)
  "checks that all values are instances of the suc-id class, no check is done ~
   on input arguments.
Arguments:
   value-list: a list of entries to be used for locating objects
               can be strings, variables, ids
   suc-id: the successor class of the relation.
Return:
   a list of object ids of the right type (or sub-type) or nil"
  (let (result query)
    (dolist (item value-list)
      ;(format t "~%; %make-sp-check-one-of-values /item: ~S" item)
      ;; try to get la list of objects
      (cond
       ((null item))
       ;; if item is an objet id leave it as it is
       ((%pdm? item) (push item result)
        )
       ;; if item is a string try for en entry point
       ((stringp item)
        (setq result (append (access item) result))
        )
       ;; if list could be a query
       ((and (listp item)(setq query (parse-user-query item)) )
        (setq result (append (access query :already-parsed t) result)))
       ;; if item is a variable, check if bound
       ((and (%is-variable-name? item)(boundp item))
        (push (symbol-value item) result)
        )
       )
      )
    ;; remove null references and reverse list
    (setq result (reverse (remove nil result)))
    ;(format t "~%; %make-sp-check-one-of-values /result: ~S" result)
    ;; check if the objects are instances of suc-id
    (setq result 
          (remove nil
                  (mapcar #'(lambda (xx) (if (%type? xx suc-id) xx)) result)))
      
    ;; return result
    result))
       
#|
ADDRESS(72): (defconcept "c1" (:att "a1" (:entry)))
$E-C1
ADDRESS(73): (definstance "c1" ("a1" "value 1")(:var _a1))
$E-C1.1
ADDRESS(74): (definstance "c1" ("a1" "value 2")(:var _a2))
$E-C1.2
ADDRESS(75): (definstance "c1" ("a1" "value 3")(:var _a3))
$E-C1.3

ADDRESS(85): (moss::%make-sp-check-one-of-values '($E-C1.1 "value 2" _a3) '$E-C1)

; %make-sp-check-one-of-values /item: $E-C1.1
; %make-sp-check-one-of-values /item: "value 2"
; %make-sp-check-one-of-values /item: _A3
; %make-sp-check-one-of-values /result: ($E-C1.1 $E-C1.2 $E-C1.3)
($E-C1.1 $E-C1.2 $E-C1.3)

ADDRESS(92): (moss::%make-sp-check-one-of-values '($E-C1.1 ("C1" ("a1" :is "value 2")) _a3) '$E-C1)

; %make-sp-check-one-of-values /item: $E-C1.1
; %make-sp-check-one-of-values /item: ("C1" ("a1" :IS "value 2"))
; %make-sp-check-one-of-values /item: _A3
; %make-sp-check-one-of-values /result: ($E-C1.1 $E-C1.2 $E-C1.3)
($E-C1.1 $E-C1.2 $E-C1.3)
|#
;;;----------------------------------------------------------- %MAKE-SP-FROM-IDS

(defUn %%make-sp-from-ids (sp-mln sp-name id class-id suc-id &rest option-list)
  "called by %make-sp to do the job. No check on the arguments. 
   Should not be called directly"
  
  ;; check just in case
  (unless class-id
    (warn "missing class-id when trying to link relation ~S in package ~S context ~S"
      sp-name *package* (symbol-value (intern "*CONTEXT*")))
    (return-from %%make-sp-from-ids nil))
  
  ;; if class-id is :any, replace it with *any*  JPB1010
  ;;********** should br *ANY* in the current package ?? (intern "*ANY*") ??
  (if (eql class-id :any) (setq class-id (intern "*ANY*")))
  ;; same for suc
  (if (eql suc-id :any) (setq suc-id (intern "*ANY*")))
  
  (let ((class-name (%make-name-for-class (car (%get-value class-id '$ENAM))))
        (context (symbol-value (intern "*CONTEXT*")))
        gen-id prop-list suc-list)
    
    ;; Check if we have structural properties with same prop name
    ;; works even when sp-name is unbound (not yet created)
    (setq prop-list (%extract-from-id sp-name '$PNAM '$EPS))
    ;; entry-points like HAS-POSITION are valid across packages, thus we get a list
    ;; of properties in various packages, that we must filter 
    (setq prop-list (%filter-against-package prop-list *package*))
    ;; we must also eliminate any property that is not a relation
    (setq prop-list (remove-if #'(lambda (xx) (not (%is-relation? xx))) prop-list))
    
    ;; ... then get generic one, otherwise create it
    (setq gen-id
          (or (%get-generic-property prop-list)
              (apply #'%make-generic-sp sp-mln option-list)))
    
    ;; create object (same name as generic property)
    (set id `(($TYPE (,context $EPS))
              ($ID (,context ,id))
              ($PNAM (,context ,sp-mln))
              ))  ; inverse prop is missing yet
    
    ;; when editing
    (save-new-id id)
    
    ;; add new property-id to system-list
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ESLS id)
    ;; declare it as a sub-property 
    (%link id '$IS-A gen-id)
    ;; build entry-points one for each synonym
    (%make-ep-from-mln sp-mln '$PNAM id :type '$EPS)
    
    ;; ...
    ;; Now we consider inverse property
    (%make-inverse-property id sp-mln)
    
    ;; link to class and to successor (in generic property if system)
    (%link class-id '$PS id)
    (%link id '$SUC suc-id)
    
    ;; process option list
    (while 
     option-list
     (case (caar option-list)
       ;; skip some options
       ((:from :to))
       (:is-a	;; superclasses must exist (this is done to catch
        ;; spelling mistakes)
        (warn ":is-a option disallowed as yet ***")
        )
       (:min
        (%%set-value id (cadar option-list)'$MINT context)
        )
       (:max
        (%%set-value id (cadar option-list)'$MAXT context)
        )
       (:unique	;; has both min and max to 1
        (%%set-value id 1 '$MINT context)
        (%%set-value id 1 '$MAXT context)
        )
       ;; Record documentation if specified
       (:doc	
        (%process-doc-option id (cdar option-list))
        ;(%%set-value-list id (cdar option-list) '$DOCT context)
        )
       (:one-of ; JPB121029
        ;; following function will try to locate instances of objects of class
        ;; suc-id. If none is found, then no link is done and we send a warning
        (setq suc-list (%make-sp-check-one-of-values (cdar option-list) suc-id))
        ;; we add result only if result is non nil
        (cond
         (suc-list
          (%%set-value-list id suc-list '$ONEOF context)
          ;; following test sets :exists as a default selection
          (unless (or (%%has-value id '$SEL context)
                      (assoc :exists option-list)
                      (assoc :forall option-list)
                      (assoc :not-in option-list))
            (%%set-value id :exists '$SEL context))
          )
         (t (warn "illegal values in the one-of option of ~S for class ~S"
              sp-name class-name)))
        )
       (:exists
        ;; can use one-of or simply the successor type
        (if (or (%%has-value id '$ONEOF context)
                (assoc :one-of option-list))
            (%%set-value id :exists '$SEL context)
          (terror ":exists option requires the use of the :one-of option in ~S ~
                    for ~S" sp-name class-name))
        )
       (:forall 
        (if (or (%%has-value id '$ONEOF context)
                (assoc :one-of option-list))
            (%%set-value id :forall '$SEL context)
          (terror ":forall  option requires the use of the :one-of option in ~S ~
                    for ~S" sp-name class-name))
        )
       (:not-in 
        (if (or (%%has-value id '$ONEOF context)
                (assoc :one-of option-list))
            (%%set-value id :not-in '$SEL context)
          (terror ":not-in  option requires the use of the :one-of option in ~S ~
                    for ~S" sp-name class-name))
        )
       (:default
           (apply #'%%make-sp-default-id id class-id suc-id sp-name option-list)
           )        
       ) ; end case
     (pop option-list))
    
    ;; build external-name for temporary use only (current session)
    (mapc #'(lambda (xx)
              (let ((name (%make-name-for-variable
                           (%make-name-string-for-concept-property xx class-name))))
                
                (set name id)
                ;; when editing
                (save-new-id name)
                ))
      (mln::extract-all-synonyms sp-mln))
    ;; return sp-id
    id))

#|;;********** � revoir
(%defconcept "person")
(%%make-sp-from-ids '((:en "neighbor")) 'HAS-NEIGHBOR '$S-PERSON-NEIGHBOR
                    '$e-PERSON '$E-PERSON '(:max 5))
$S-PERSON-NEIGHBOR
(($TYPE (0 $EPS)) ($ID (0 $S-PERSON-NEIGHBOR)) ($PNAM (0 ((:EN "neighbor"))))
 ($ESLS.OF (0 $SYS.1)) ($IS-A (0 $S-NEIGHBOR)) ($INV (0 $S-PERSON-NEIGHBOR.OF))
 ($PS.OF (0 $E-PERSON)) ($SUC (0 $E-PERSON)) ($MAXT (0 5)))

(defindividual "person" (:var _person2))
$E-PERSON.2
(defrelation "rel06" (:from "person") (:to "person") (:default _person1 _person2))
(NIL #<UNBOUND-VARIABLE @ #x2275917a>) 
... system error while defining defaults for relation HAS-REL06: 
#<UNBOUND-VARIABLE @ #x2275917a>
(===> %%MAKE-SP-DEFAULT-ID DEFAULTS NIL) 
Warning: some of the specified defaults 
 (_PERSON1 _PERSON2) 
 might be unbound
 while defining relation HAS-REL06. We skip adding defaults...

$E-PERSON.0
(($TYPE (0 $E-PERSON)) ($S-PERSON-REL04 (0 _PERSON1))
 ($S-PERSON-REL05 (0 $E-PERSON.1)) ($S-PERSON-REL06 (0 $E-PERSON.2 $E-PERSON.3)))

(send _person1 '=get "rel06")
($E-PERSON.2 $E-PERSON.3)
|#

;;;=============================================================================
;;;
;;;                             CONCEPT (Class) 
;;;
;;;=============================================================================

;;; Next thing to create in the bootstrap are classes

;;;--------------------------------------------------------------- %MAKE-CONCEPT
;;; resets *context* to 0!!!

(defUn %make-concept (class-ref &rest option-list)
  "creates a class.
 (defconcept name &rest option-list) - creates a new class
Arguments:
   class-ref: symbol, string or multilingual name
   option-list (opt): a list of option pairs:
       (:is-a <class-id>)	   for defining inheritance
       (:att <att-description>)    specifies attribute (formerly terminal prop)
       (:rel <rel-description>)    specifies relationship (formerly structural prop)
       (:doc <documentation>)      text expressed as a multilingual list
       (:do-not-save)              if there does not save onto *moss-system*
       (:if-exists :return)        if exists, skip definition (so that we can reload)
       (:id <symbol>)              make symbol the id of the class (e.g. *none*)
       (:rdx <symbol>)             radix applying to instances"
  (declare (special *language*))
  ;(format t "~2%;---------- Entering %make-concept")
  ;(format t "~%;--- class-ref: ~S" class-ref)
  ;(format t "~%;--- *package*: ~S" *package*)
  ;(format t "~%;--- option-list: ~S" option-list)
 
  (let ((do-not-save (assoc :do-not-save option-list))
        (id (cadr (assoc :id option-list)))
        (radix (cadr (assoc :rdx option-list)))
        (context (symbol-value (intern "*CONTEXT*")))
        class-name ctr-id class-mln control)
    (declare (ignorable control))
    
    ;(format t "~%;--- context: ~S" context)
    
    ;; throw to :error when something wrong class-ref used by error messages
    (%check-package-version-language *package* context *language* class-ref)
    
    ;; cook up a multilingual name.
    (setq class-mln (mln::make-mln class-ref)) ; jpb1406
    ;(format t "~%;--- class-mln: ~S" class-mln)
    
    ;; create class name: if class-ref is a symbol keep it in its own package 
    ;(trformat "=== *package* : ~S" *package*)
    (setq class-name (%make-name-for-class class-mln))
    ;(vformat "=== creating class with name: ~S in package ~S" class-name *package*)
    ;(format t "~%;=== creating class with name: ~S in package ~S" class-name *package*)
	
    (unless class-name 
      (error "class-name should not be nil"))
	  
    ;; then cook up the class id. We can leave the :id option on the option-list
    ;; it will be ignored during the processing of options
    (setq id (or id (%make-id-for-concept class-mln)))
    (when (boundp id)
      ;; if id already there, check whether we allow it 
      (if (eql :return (car (getv :if-exists option-list)))
        ;; if allowed send warning, just in case
        (progn
          (vformat "~&;*** Warning while trying to define class ~S~
                    ~%; in package ~S, ~
                    ~%; internal id ~A exists with value: ~
                    ~%; ~S ~
                    ~%; Using old value..." 
                   class-mln *package* id (symbol-value id))
          (return-from %make-concept id))
        ;; otherwise, throw to :error
        (terror  "while trying to define class:~%; ~S~%; in package ~S, ~
                  ~%; internal id (~A) exists with value:~%  ~S"
                 class-mln *package* id (symbol-value id))))
    
    ;; otherwise, proceed...
    (eval-when (:compile-toplevel :load-toplevel :execute) 
      (proclaim `(special ,class-name))
      )
    ;(when (equal+ class-ref "keyword") (break "%make-concept keyword")) 
    ;; OK we create class
    (set id `(($TYPE (,context $ENT))
              ($ID (,context ,id))
              ($ENAM (,context ,class-mln))
              ($RDX (,context ,(or radix id)))))
    ;(format t "~%;--- *package*: ~S" *package*)
    ;(format t "~%;--- id: ~S" id)
    ;(format t "~%;---  (eval id): ~S" (eval id))    
    ;; when editing
    (save-new-id id)
    ;; add it to MOSS
    (unless do-not-save
      (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ENLS id))
    ;; don't forget entry point
    (setq control
          (%make-ep-from-mln class-mln '$ENAM id :type '$ENT))
    ;(format t "~%;--- entry points: ~S" control)
    ;; create now associated counter unless option (:no-counter) is there
    
    ;; make new symbol
    (setq ctr-id (%make-id-for-counter id))
    ;; build counter. Counters are created separate from classes because the content
    ;; of a class is rather static and the value of the counter changes. This was 
    ;; done to optimize storage in the early versions of MOSS (i.e. not to be obliged
    ;; to update the whole class whenever a new instance was created). We create 
    ;; counters in context 0, so that we can upgrade them easily from any context
    (set ctr-id `(($TYPE (0 $CTR))
                  ($VALT (0 ,1)))) ; requires comma before 1, otherwise replaces 1
                                   ; by some previous number
    ;(format t "~%; %make-concept /ctr-id: ~S, (sv ...): ~S" ctr-id (symbol-value ctr-id))
    
    ;; when editing
    (save-new-id ctr-id)
    ;; link to class
    (%link id '$CTRS ctr-id context)
    ;; we record instance at the system level
    ;(%link *moss-system* '$SVL ctr-id context)
    
    ;; we create the ideal associated with the class
    (set (%make-id-for-ideal id) 
         `(($TYPE (,context ,id))))
    
    ;; when editing
    (save-new-id (%make-id-for-ideal id))
    
    ;; process options
    (while option-list
      (case (caar option-list)
        ;; skip some options
        ((:if-exists :id :rdx)) ; JPB1006 adding :id, JPB110828
        
        (:is-a	;; defines super classes (specified by strings, each string
         ;; being any legal synonym referring to the class
         (%make-concept-isa-option class-mln id (cdar option-list)))
        
        ((:tp :att)  ; call the slave adding context and package info
         ; will eventually be shadowed
         (apply #'%make-tp (cadar option-list) 
                (cons `(:class-ref ,class-name) (cddar option-list))))
         ;(%make-concept-tp-option class-name (cdar option-list)))
        
        ((:sp :rel)  ; call the slave (:rel <sp-ref> <options>)
         (apply #'%make-sp (cadar option-list) 
                (cons `(:from ,class-name) (cddar option-list))))
         ;(%make-concept-sp-option class-name id (cdar option-list)))
                
        ;; Record documentation if specified, :doc takes a single value JPB060228
        (:doc	
         (%process-doc-option id (cdar option-list))
         ;(%%set-value-list id (cdar option-list) '$DOCT context)
         )
        
        (:one-of
         ;; we cannot execute the following lines until the MOSS engine is loaded
         ;; thus the :one-of option cannot be used in the boot file
         (%make-concept-oneof-option id (cdar option-list)))
        
        (otherwise
         (verbose-warn "unknown option ~S while defining class ~S.~&We ignore it." 
                       (car option-list) class-name))	
        )  ; end case
      (pop option-list))
    
    ;; build external-name for temporary use only (current session)
    (mapc #'(lambda (xx) (set (%make-name-for-variable xx) id)
              ;; when editing
              (save-new-id (%make-name-for-variable xx))
              )
      (mln::extract-all-synonyms class-mln))
    ;(format t "~%;--- exiting make-concept: ~S" *package*)
    ;; return entity-id
    id))

#|
(catch :error
       (defconcept "Person" (:att "Name" (:entry)) (:att "Age") 
         (:rel "Brother" (:to "person"))))
$E-PERSON
(($TYPE (0 $ENT)) ($ID (0 $E-PERSON)) ($ENAM (0 (:EN "Person")))
 ($RDX (0 $E-PERSON)) ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-PERSON.CTR))
 ($PT (0 $T-PERSON-NAME $T-PERSON-AGE)) ($PS (0 $S-PERSON-BROTHER))
 ($SUC.OF (0 $S-PERSON-BROTHER)))

(catch :error
       (%defconcept (:fr "Libraire" :en "Bookstore")
             (:att (:fr "nom" :en "name")(:unique))))
$E-BOOKSTORE
(($TYPE (0 $ENT)) ($ID (0 $E-BOOKSTORE))
 ($ENAM (0 (:FR "Libraire" :EN "Bookstore"))) ($RDX (0 $E-BOOKSTORE))
 ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-BOOKSTORE.CTR)) ($PT (0 $T-BOOKSTORE-NAME)))

(%defconcept (:fr "Boucher" :en "Butcher")
               (:att (:fr "nom" :en "name")(:unique)))
$E-BUTCHER
(($TYPE (0 $ENT)) ($ID (0 $E-BUTCHER)) ($ENAM (0 (:FR "Boucher" :EN "Butcher")))
 ($RDX (0 $E-BUTCHER)) ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-BUTCHER.CTR))
 ($PT (0 $T-BUTCHER-NAME)))

HAS-NOM
(($TYPE (0 $EP))
 ($PNAM.OF (0 $T-NOM $T-PERSONNE-NOM $T-NAME $T-BOOKSTORE-NAME $T-BUTCHER-NAME))
 ($EPLS.OF (0 $SYS.1)))

(%defconcept (:fr "plombier;barbouze" :en "G-man")
               (:att (:fr "nom" :en "name")(:unique)))
$E-G-MAN

plombier
(($TYPE (0 $EP)) ($ENAM.OF (0 $E-G-MAN)) ($EPLS.OF (0 $SYS.1)))

barbouze
(($TYPE (0 $EP)) ($ENAM.OF (0 $E-G-MAN)) ($EPLS.OF (0 $SYS.1)))

$E-G-MAN
(($TYPE (0 $ENT)) ($ID (0 $E-G-MAN))
 ($ENAM (0 (:FR "plombier;barbouze" :EN "G-man"))) ($RDX (0 $E-G-MAN))
 ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-G-MAN.CTR)) ($PT (0 $T-G-MAN-NAME)))

(catch :error (defconcept zoo-keeper))
$E-ZOO-KEEPER
(($TYPE (0 $ENT)) ($ID (0 $E-ZOO-KEEPER)) ($ENAM (0 (:EN "ZOO-KEEPER")))
 ($RDX (0 $E-ZOO-KEEPER)) ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-ZOO-KEEPER.CTR)))

(defconcept "baker" (:att "name" (:entry)) (:att "age")
    (:one-of
     (("name" "Mitron Albert")("age" 30))
     (("name" "Mitron Jules") ("age" 75))))
$E-BAKER
(($TYPE (0 $ENT)) ($ID (0 $E-BAKER)) ($ENAM (0 (:EN "baker"))) ($RDX (0 $E-BAKER))
 ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-BAKER.CTR))
 ($PT (0 $T-BAKER-NAME $T-BAKER-AGE)) ($ONEOF (0 $E-BAKER.1 $E-BAKER.2)))

? $E-BAKER.1
(($TYPE (0 $E-BAKER)) ($ID (0 $E-BAKER.1)) ($T-BAKER-NAME (0 "Mitron Albert"))
 ($T-BAKER-AGE (0 30)))

? $E-BAKER.2
(($TYPE (0 $E-BAKER)) ($ID (0 $E-BAKER.2)) ($T-BAKER-NAME (0 "Mitron Jules")) 
 ($T-BAKER-AGE (0 75)))

? MITRON-ALBERT
(($TYPE (0 $EP)) ($ID (0 MITRON-ALBERT)) ($T-BAKER-NAME.OF (0 $E-BAKER.1))
 ($EPLS.OF (0 $SYS.1)))

;;; ONE-OF tests
(defconcept "Stranger1"
    (:att Name (:entry))
    (:one-of   ; full format
     (("name" "Jess"))
     (("name" (:en "jose" :fr "joseph"))(:doc :en "a Portuguese fellow"))))
$E-STRANGER1
(($TYPE (0 $ENT)) ($ID (0 $E-STRANGER1)) ($ENAM (0 ((:EN "Stranger1")))) 
 ($RDX (0 $E-STRANGER1))
 ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-STRANGER1.CTR)) ($PT (0 $T-STRANGER1-NAME))
 ($ONEOF (0 $E-STRANGER1.1 $E-STRANGER1.2)))

$E-STRANGER1.1
(($TYPE (0 $E-STRANGER1)) ($ID (0 $E-STRANGER1.1)) ($T-STRANGER1-NAME (0 "Jess")))

(defconcept "Stranger2"
  (:att Name (:entry))
  (:one-of   ; full format
   (("name" "Jess"))
   (("name" "jose")(:doc :en "a Portuguese fellow"))))

$E-STRANGER2
(($TYPE (0 $ENT)) ($ID (0 $E-STRANGER2)) ($ENAM (0 ((:EN "Stranger2"))))
 ($RDX (0 $E-STRANGER2))
 ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-STRANGER2.CTR)) ($PT (0 $T-STRANGER2-NAME))
 ($ONEOF (0 $E-STRANGER2.1 $E-STRANGER2.2)))

$E-STRANGER2.2
(($TYPE (0 $E-STRANGER2)) ($ID (0 $E-STRANGER2.2)) ($T-STRANGER2-NAME (0 "jose"))
 ($DOCT (0 ((:EN "a Portuguese fellow")))))

(defconcept
    (:en "outcome type 1; output type" :fr "type de r�sultat")
    (:one-of  ; simplified format
     (:en "consequence" :fr "cons�quence")
     (:en "postcondition" :fr "postcondition")
     (:en "output" :fr "sortie"))
    (:doc :en "Outcome Type characterizes each outcome, thus maintaining metadata ~
               information for the instances of outcomes that result from a specific ~
               Service Interaction."))
BUG on one-of:
Warning: while trying to define instance of class "outcome type 1" in package #<The MOSS package>
         context 0, property "object name" does not exist. We ignore it.
$E-OUTCOME-TYPE-1
((MOSS::$TYPE (0 MOSS::$ENT)) (MOSS::$ID (0 $E-OUTCOME-TYPE-1))
 (MOSS::$ENAM (0 (:EN "outcome type 1 ; output type" :FR "type de r�sultat")))
 (MOSS::$RDX (0 $E-OUTCOME-TYPE-1)) (MOSS::$ENLS.OF (0 MOSS::$SYS.1))
 (MOSS::$CTRS (0 $E-OUTCOME-TYPE-1.CTR)) (MOSS::$PT (0 MOSS::$OBJNAM))
 (MOSS::$ONEOF (0 $E-OUTCOME-TYPE-1.1 $E-OUTCOME-TYPE-1.2 $E-OUTCOME-TYPE-1.3))
 (MOSS::$DOCT
  (0 :EN "Outcome Type characterizes each outcome, thus maintaining metadata ~
             information for the instances of outcomes that result from a specific ~
             Service Interaction.")))

$E-OUTCOME-TYPE-1.1
((MOSS::$TYPE (0 $E-OUTCOME-TYPE-1)) (MOSS::$ID (0 $E-OUTCOME-TYPE-1.1))
 (MOSS::$OBJNAM (0 (:EN "consequence" :FR "cons�quence"))))
consequence
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 CONSEQUENCE))
 (MOSS::$OBJNAM.OF (0 $E-OUTCOME-TYPE-1.1)) (MOSS::$EPLS.OF (0 MOSS::$SYS.1)))
? $E-OUTCOME-TYPE-1.3
((MOSS::$TYPE (0 $E-OUTCOME-TYPE-1)) (MOSS::$ID (0 $E-OUTCOME-TYPE-1.3))
 (MOSS::$OBJNAM (0 (:EN "output" :FR "sortie"))))
? sortie
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 SORTIE))
 (MOSS::$OBJNAM.OF (0 $E-OUTCOME-TYPE-1.3)) (MOSS::$EPLS.OF (0 MOSS::$SYS.1)))
|#
;;;---------------------------------------------------- %MAKE-CONCEPT-ISA-OPTION

(defUn %make-concept-isa-option (class-mln class-id isa-list)
  "handles the :is-a option in a concept definition. 
Arguments:
   class-mln: symbol designating the class
   class-id: class id
   isa-list: list of super-classes
Return:
   nothing of interest."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        super-class-id)  
    ;;if deferred record it
    (when *allow-forward-references*
      (setq *deferred-mode* :class)
      (push `(with-environment ',*package* ',context ',*language*
               (apply #'%make-concept-isa-option ',class-mln ',class-id
                      ',isa-list nil))
            *deferred-links*)
      (return-from %make-concept-isa-option nil))
    
    ;; otherwise do the job
    (while isa-list
      (setq super-class-id (%%get-id (car isa-list) :class))
      
      (if super-class-id
        ;; if class exists link it
        (%link class-id '$IS-A super-class-id)
        ;; otherwise send a warning
        (warn "when defining class ~S in package ~S: ~&class ~
               ~A in the :is-a option is not (yet?) defined. Discarded."
              class-mln *package* (car isa-list)))
      (pop isa-list)
      ) ; end while
    class-id))

#|
(moss::%make-concept-isa-option '(:name :en "ATER") '$E-ATER '("person"))
$E-ATER
|#
;;;-------------------------------------------------- %MAKE-CONCEPT-ONEOF-OPTION

;;;********** revoir

(defUn %make-concept-oneof-option (class-id obj-list)
  "handles the :one-of option in a concept definition. 
Arguments:
   class-id: class id
   obj-list: list of the possible class instances
Return:
   nothing of interest."
  
  (let ((context (symbol-value (intern "*CONTEXT*"))))
  ;;if deferred record it
  (when *allow-forward-references*
    (setq *deferred-mode* :class)
    (push `(with-environment ',*package* ',context ',*language*
             (apply #'%make-concept-oneof-option ',class-id ',obj-list nil))
          *deferred-links*)
    (return-from %make-concept-oneof-option nil))
  
  (let (result)
    ;; if a list of simple lists, simplified format; if a list of alists, full 
    ;; format. Transform into full format using default OBJNAM property
    (unless (every #'alistp obj-list)
      ;; add default $OBJNAM property to class
      (%%add-value class-id '$PT '$OBJNAM context)
      (setq obj-list (mapcar #'(lambda (xx) (list (list "object name" xx))) 
                             obj-list)))
    ;; create each instance and keep them in a list
    (dolist (item obj-list)
      (catch :error 
        (push (apply #'%%make-instance-from-class-id class-id item) result)))
    ;; create the property
    (%%set-value-list class-id (reverse result) '$ONEOF context)
    result)))

#|
? (moss::%make-concept-oneof-option '$E-PERSON '("Albert" "George"))
($E-PERSON.2 $E-PERSON.1)
? $E-PERSON.2
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.2))
 (MOSS::$OBJNAM (0 "George")))
|#
;;;----------------------------------------------------- %MAKE-CONCEPT-SP-OPTION
;;; We remove the possibility of having a positional notation, meaning that
;;; target successors must be specfied with a (:to <suc>) option

(defUn %make-concept-sp-option (name id sp-options)
  "handles the :rel (:sp) option in a concept definition (C). If :one-of option is ~
   present, then creates a new <Cname>-type class, with the specified instances.
Arguments:
   name: symbol designating the class
   id: class-id
   sp-options: list corresponding to the :rel option, e.g. (BROTHER PERSON (:min 1))
Return:
   nothing of interest."
  
  (let ((context (symbol-value (intern "*CONTEXT*"))))
  
  ;; when we allow formard references, we defer linkings
  (when *allow-forward-references*
    (setq *deferred-mode* :class)
    (push `(with-environment ',*package* ',context ',*language*
             (funcall #'%make-concept-sp-option ',name ',id ',sp-options))
          *deferred-links*)
    (return-from %make-concept-sp-option nil))
  
  ;;***** we assume that we have a single successor
  ;; SOL syntax: (:rel (:name :en "Great aunt") (:to "Person") ...)
  (let ((prop-ref (pop sp-options))
        ;; property ref is first item of the list
        ;; this works even if property name is yet unbound (relation not created)
        sp-id successor suc-id message)
    
    ;; deal first with the property range
    (cond
     ;; if sp-option is not an a-list, then error, else args can be in any order
     ((not (alistp sp-options))
      (mformat "~%;***Warning while creating entity ~A. ~S ~
                syntax error in :rel option:~&" 
               name sp-options)
      (return-from %make-concept-sp-option nil))
     
     ;; if there is a :to option, then this specifies a class
     ((assoc :to sp-options)
      (setq successor (cadr (assoc :to sp-options))
          sp-options (remove (assoc :to sp-options) 
                             sp-options :test #'equal)))
     
     ;; if there is a :one-of option, then we must create the structure
     ;; :one-of defines the range of the property
     ((assoc :one-of sp-options)
      ;; then we must create a new class ($E-<prop-name>-type)
      ;;**********
      ;; Pb: if prop-ref is an MLN and *language* is :all, then if we produce a 
      ;; string, the system won't be able to cook up a new MLN 
      
      ;; what means (defconcept "zzz") or (defconcept 'ZZZ) when language is :all ?
      ;; we must create a concept whose name is ZZZ for any of the languages ?
      ;; can we use (:all "ZZZ") or (:* "ZZZ") as a valid MLN ?
      ;; Currently the created name is (:unknown "ZZZ")
      
      (multiple-value-bind (prop-string *language*) ; establish a language context
          (mln::get-canonical-name prop-ref)
        (declare (special *language*))
        (setq successor (concatenate 'string (%string-norm prop-string) "-type"))
        (apply #'%make-concept successor '(:if-exists :return)
               (remove (assoc :language sp-options) sp-options :test #'equal))))
     
     ;; coming here indicates a probable syntax error; missing :to or :one-of
     (t
      (mformat "~&;***Warning while creating entity ~A. ~S ~
                syntax error in :rel option:~&" 
               name sp-options)
      (return-from %make-concept-sp-option nil))
     )
    
    ;; if successor is a string, we need to turn it into an entry point JPB060806
    (if (stringp successor)
        (setq successor (%%make-name successor :class))) ; JPB1507
        ;(setq successor (%make-name '$ENT :name successor)))
    
    ;; then determine the prop-id
    (setq sp-id (%make-id-for-sp prop-ref))
    ;(trformat "prop-name: ~S, successor: ~S, prop-id: ~S ~&more-options: ~S"
    ;          prop-ref successor sp-id sp-options)
    
    ;; then, check for presence of successor and deferred mode
    (setq suc-id (car (%extract-from-id successor '$ENAM '$ENT)))
    
    (if (and (null suc-id) *allow-forward-references*)
        ;; if not there but if *allow-forward-references* is true, save the 
        ;; definition onto the *deferred-actions* list
        ;; the entry will be executed in the loading package...
        (push `(with-package ,*package*
                 (m-defsp ,prop-ref (:class-id ,id) ,successor ,@sp-options))
              *deferred-actions*)
      ;; otherwise try to create sp, which might result in error
      (unless 
          (symbolp 
           (setq message 
                 (catch :error 
                        (apply #'%make-sp `(,prop-ref (:class-id ,id) ,successor
                                                      ,@sp-options)))))
        (mformat "~&;***Warning while creating entity ~A. ~S" name message))
      )
    ;; return property id
    sp-id)
  ))

;(TRACE MOSS::%MAKE-CONCEPT-SP-OPTION)

#|
;;; could use this macro instead of last lines
(defMacro with-mformat-if-error (fstring fargs &rest body)
  `(let ((message (catch :error ,@body)))
     (unless (symbolp message)
       (mformat ,fstring ,@fargs))
     message))
(with-mformat-if-error 
  "~&;***Warning while creating entity ~A. ~S"
  (name message)
  (apply #'%make-sp prop-name `(:class-id ,id) successor sp-options))
|#
#|
? (moss::%make-concept-sp-option "person" '$E-PERSON '("sibling" "person"))
$S-SIBLING
? (send '$E-person '=print-self)

----- $E-PERSON
CONCEPT-NAME: person
RADIX: $E-PERSON
TERMINAL-PROPERTY: NAME/person
STRUCTURAL-PROPERTY: SIBLING/person
COUNTER: 1
-----
:DONE
? (moss::%make-concept-sp-option "person" '$E-PERSON '((:en "brother") "person"))
$S-BROTHER
? (moss::%make-concept-sp-option "person" '$E-PERSON 
                                 '((:en "brother") (:min 1) (:to "person")))
$S-BROTHER
? (moss::%make-concept-sp-option "person" '$E-PERSON 
                                 '("gender" (:one-of "m" "f")))
$S-GENDER
? (send '$E-person '=print-self)

----- $E-PERSON
CONCEPT-NAME: person
RADIX: $E-PERSON
TERMINAL-PROPERTY: NAME/person
STRUCTURAL-PROPERTY: SIBLING/person, GENDER/person
COUNTER: 1
-----
:DONE
|#
;;;----------------------------------------------------- %MAKE-CONCEPT-TP-OPTION

(defUn %make-concept-tp-option (class-name tp-options)
  "handles the :att (:tp) option in a concept definition. If :one-of option is ~
   present, then creates a new <name>-type class, with the specified instances.
Arguments:
   class-name: symbol, e.g. PERSON
   class-id: identifier of the class, symbol, e.g. $E-PERSON
   tp-options: list corresponding to the :att option, e.g. (AGE (:unique) (:entry))
Return:
   attribute identifier on success, nil on failure."
  ;; att option, e.g. (:att 
  ;; first create or obtain property. If an error tp-id is a message
  (let (tp-ref tp-id)
    ;; property name should be the first item of the list it can be an MLN
    ;; this works even if property name is yet unbound (relation not created)
    (setq tp-ref (pop tp-options)) ; agreed by %make-tp
    ;(trformat "prop-name: ~S, ~&more-options: ~S" tp-name tp-options)
    
    ;; check format: should now be alist
    (unless (alistp tp-options)
      (mformat "~&;***Warning while creating entity ~A. Bad syntax in:~& ~S" 
               class-name tp-options)
      (return-from %make-concept-tp-option nil))
    
    ;; go create the property
    (catch-error 
     (format nil "while creating attribue ~S for entity ~A in package ~S."  
             tp-ref class-name *package*)
     (setq tp-id 
           (apply #'%make-tp `(,tp-ref (:class-ref ,class-name) ,@tp-options))))
    ;; return attribute identifier
    tp-id))

#|
? (moss::%make-concept-tp-option 'person '("birth date" (:unique)))
$T-PERSON-BIRTH-DATE
? (send '$e-person '=print-self)

----- $E-PERSON
 CONCEPT-NAME: person
 RADIX: $E-PERSON
 TERMINAL-PROPERTY: NAME/person, BIRTH-DATE/person
 STRUCTURAL-PROPERTY: SIBLING/person, BROTHER/person, GENDER/person
 COUNTER: 1
-----
:DONE
? (moss::%make-concept-tp-option 'person '$E-PERSON 
     '("size" (:one-of "small" "medium" "large")))
$T-PERSON-SIZE
? $T-PERSON-SIZE
((MOSS::$TYPE (0 MOSS::$EPT)) (MOSS::$ID (0 $T-PERSON-SIZE))
 (MOSS::$PNAM (0 (:EN "SIZE"))) (MOSS::$ETLS.OF (0 MOSS::$SYS.1))
 (MOSS::$IS-A (0 $T-SIZE)) (MOSS::$INV (0 $T-SIZE.OF)) (MOSS::$PT.OF (0 $E-PERSON))
 (MOSS::$ONEOF (0 "small" "medium" "large")) (MOSS::$SEL (0 :ONE-OF)))
|#
;;;=============================================================================
;;;
;;;                            VIRTUAL CONCEPT 
;;;
;;;=============================================================================

;;;------------------------------------------------------- %MAKE-VIRTUAL-CONCEPT

;;; virtual concepts are not instantiable, therefore, in deferred mode, we can
;;; instantiate them last.

(defUn %make-virtual-concept (class-ref &rest option-list)
  "Virtual classes are created without counters since they cannot have instances. ~
   Virtual classes are a view on some instance of a class. They contain a ~
   set of constraints expressed as triples to filter some of the instances of ~
   the class they refer to.

 (%defvirtualclass name &rest option-list) - creates a new MOSS virtual class
Arguments:
   class-ref: symbol, string or multilingual name
   option-list (opt): a list of option pairs:
	(:is-a <class-id>)	   for defining class reference
	(:def <att-description>)   specifies definition
	(:rel <rel-description>)   specifies relationship (formerly structural prop)
	(:doc <documentation>)	text expressed as a multilingual list"
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        name id multilingual-name ref-class-id)
    
    ;;=== first check global parameters
    ;; throw to :error when something wrong
    (%check-package-version-language *package* context *language* class-ref)
    
    ;; the is-a option should be there referencing a valid class, otherwise error
    (unless (assoc :is-a option-list)
      (terror "~&;***Error while trying to define virtual class~& ~S, ~
               missing class reference (:is-a option)"
              class-ref))
    
    ;; get reference class id
    (unless
      (setq ref-class-id 
            (%%get-id (cadr (assoc :is-a option-list)) :class))
      (terror "while trying to define virtual class~& ~S, ~
               bad class reference: ~S"
              class-ref (cadr (assoc :is-a option-list))))
    
    ;; the virtual class should also have a definition
    (unless (assoc :def option-list)
      (terror "while trying to define virtual class~& ~S, ~
               missing definition (:def option)"
              class-ref))
    
    ;; cook up a multilingual name, remove :name from multilingual name
    (setq multilingual-name (mln::make-mln class-ref)) ; jpb1406
    
    ;; create virtual class name (same as concept name)
    ;(trformat "=== *package* : ~S" *package*)
    (setq name (%make-name-for-concept multilingual-name))
    (vformat "=== creating virtual class with name: ~S" name)
    
    ;; then cook up the class id. We can leave the :id option on the option-list
    ;; it will be ignored during the processing of options
    (setq id (%make-id-for-virtual-concept multilingual-name))
    
    (when (boundp id)
      ;; if id already there, check whether we allow it 
      (if (eql :return (car (getv :if-exists option-list)))
        ;; if allowed send warning, just in case
        (progn
          (vformat "~&;***Warning while trying to define virtual class~& ~S, ~
                    internal id ~A exists with value~& ~A.~&Using old value..." 
                   multilingual-name id (symbol-value id))
          (return-from %make-virtual-concept id))
        ;; otherwise, throw to :error
        (terror "while trying to define virtual class~& ~S, ~
                 internal id ~A exists with value~& ~A."
                multilingual-name id (symbol-value id))))
    
    ;; OK we create class
    (set id  `(($TYPE (,context $VENT))
               ($ID (,context ,id))
               ($RFC (,context ,ref-class-id))
               ($ENAM (,context ,multilingual-name))
               ))
    ;; when editing
    (save-new-id id)
    
    ;; add it to MOSS
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$VELS id)
    
    ;; don't forget entry point
    (%make-ep-from-mln multilingual-name '$ENAM id :type '$ENT)
    
    ;; process options
    (dolist (option option-list)
      (case (car option)
        
        (:is-a)	;; skip it, already processed
        
        (:def
          ;; currently simply store the definition
          (%%set-value id (cdr option) '$DEF context))
        
        ;; Record documentation if specified, :doc takes a single value JPB060228
        (:doc	
         (%process-doc-option id (cdr option))
         )
        
        (otherwise
         (verbose-warn "unknown option ~S while defining virtual class ~S.~
                        ~&We ignore it." 
                       (car option-list) name))	
        )  ; end case
      (pop option-list))
    
    ;; build external-name for temporary use only (current session)
    ;; should build a variable for every synonym???
    (set (%make-name-for-variable name) id)
    ;; when editing
    (save-new-id (%make-name-for-variable name))

    ;; return entity-id
    id))

#|
(defvirtualconcept "adult"
    (:is-a "person")
  (:def ("age" :gte 18)))
:$V-ADULT

(with-package :test
    (moss::%make-virtual-concept "adult"
      '(:is-a "person")
      '(:def ("age" :gte "18") )
      ))
TEST::$V-ADULT
(($TYPE (0 $VENT)) ($ID (0 TEST::$V-ADULT)) ($RFC (0 TEST::$E-PERSON))
 ($ENAM (0 ((:EN "adult")))) ($VELS.OF (0 TEST::$SYS.1))
 ($DEF (0 (("age" :GTE "18")))))
|#
;;;=============================================================================
;;;
;;;                            INSTANCE METHOD 
;;;
;;;=============================================================================

;;; defmethod is used first in bootstrap for building new methods, in particular
;;; that of $ENT, otherwise we could not build any new object
;;; It needs the existence of $CTR.0 (method counter) to do so, but not 
;;; much else
;;; This version attaches methods only to existing classes and not to any
;;; other object - Anyway at this level we do not know how to create instances.
;;; Modified on 21/11 to use the concept of selector allowing to attach own
;;; methods to any object
;;; Also the way we record them onto the plist of classes has been changed

;;;---------------------------------------------------------------- %MAKE-METHOD
;;; 1. Definstmethod: method is attached to the class and applies to instances
;;;   if selector is a symbol, then it can be 
;;;     1.1 the name (ep) of the class (PERSON, ENTITY)
;;;     1.2 the name of a variable containing the class id (_person),
;;;     1.3 the class id ($E-PERSON, $ENT)

(defUn %make-method (name-ref selector arg-list body &rest option-list)
  "Used to define methods attached to an existing class. it is an error if ~
   the class is not already defined. When the method does not already exist, ~
   then a method object is first created; then the method is compiled and ~
   attached to an internal name built from the class-id, the current version, ~
   and the method-name. If the method already exists, then the code and arguments ~
   are updated, and the method is recompiled.
Arguments:
   name: name of the method
   selector: class ref (symbol, string or mln)
   arglist: argument of the method
   body: code for the method
   option-list (rest): contains info about language (?)
Return:
   id of the method."
  (declare (special *package*))
  (let ((context (symbol-value (intern "*CONTEXT*"))))
  
  ;; if forward references are allowed record action onto the *deferred-methods* list
  ;; watch %make-method is a system function
  (when *allow-forward-references*
    (push `(with-environment ',*package* ',context ',*language*
             (apply #'moss::%make-method ',name-ref ',selector ',arg-list ',body 
                    ',option-list))
          *deferred-methods*)
    (return-from %make-method nil))
  
  (let (name fn-id fn-id-list class-id method-function-name)
    
    ;; build name from name-ref (simple normed value)
    (setq name (%make-name-for-method name-ref)) ; JPB1507
    ;; get class-id from selector
    (setq class-id (%%get-id selector :class))
    ;; if we have no value, complain
    (unless class-id
      (terror "while defining method ~A, selector is not a valid class reference~%:~
               ~A ~%; in context ~S and package ~S."
              name selector context *package*))

    ;(format t "~%; %make-method /method: ~S, *package*: ~S" name *package*)
    
    ;; otherwise check if method already exists for the class
    (if (and (%pdm? name)       ; method-name is enfry point?
             (setq fn-id-list   ; usurp value for a short while
                   (%get-value name (%make-id-for-inverse-property '$MNAM)))
             ;; look for presence of class-id in the list
             (dolist (item fn-id-list)
               (if (member class-id 
                           (%get-value item (%make-id-for-inverse-property '$IMS)))
                 (return (setq fn-id item))))
             )
      (progn
        ;; here method already exists we simply patch it
        ;;********** we should remove it from the p-list of all classes and orphans
        (verbose-warn "redefining method ~A for class ~A" name selector)
        ;; when editing
        (save-old-value fn-id)
        ;; upgrade documentation
        (if (stringp (car body))
          (%%set-value fn-id (car body) '$DOCT context))
        )
      (progn
        ;; otherwise method does not exist we must create it
        (setq fn-id 
              (if (eql *package* (find-package :moss))
                ;; make MOSS methods
                (%make-id-for-instance-method (%get-and-increment-counter '$FN))
                ;; otherwise make application methods
                (%make-id-for-instance-method
                 (%get-and-increment-counter (intern "$FN")))))
        ;; build new entity (code and args are available in function object)
        ;(print `(+++ ,*package* ,fn-id))
        (set fn-id 
             `(($TYPE ,(if (eql *package* (find-package :moss))
                         (list context '$FN)
                         (list context (intern "$FN")))) ; JPB 0912
               ($ID (,context ,fn-id))
               ($MNAM (,context ,name))
               ,@(if (stringp (car body))
                   `(($DOCT (,context ,(car body)))))
               ))
        
        ;; when editing
        (save-new-id fn-id)
        ;; build entry-point if it does not exist already and export it
        (%make-ep name '$MNAM fn-id :export t)
        ;; add new method-name to system-list
        (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$FNLS fn-id)
        ;; link to class
        (%link class-id '$IMS fn-id)))
    
    ;; next is common code to cases where method is created or simply patched
    ;; define or redefine a global symbol for holding method code
    (setq method-function-name 
          (%make-name-for-instance-method-function name class-id))

    ;; when editing
    (save-new-id method-function-name)
    ;; record internal method name
    (%%set-value fn-id method-function-name '$FNAM context)
    ;; ...build and attach code to method-name and compile it
    (%build-method method-function-name arg-list body)
    
    ;; put reference onto p-list of class (***** only if methods cached???)
    (%putm class-id method-function-name name context :instance)
    ;; return method-name
    fn-id
    )))

#|
;;; while defining methods in other packages, must be careful of the symbols
;;; symbols are adssigned while reading the file...

? (with-package :address
    (definstmethod =summary person ()
      (address::has-name)))
; Warning: redefining method =SUMMARY for class PERSON
; While executing: MOSS::%MAKE-METHOD
$FN.90
(progn
  (in-package :cl-user)
  (definstmethod =summary person ()
    (has-name))
  (in-package :moss))
; Warning: redefining method =SUMMARY for class PERSON
; While executing: MOSS::%MAKE-METHOD
;Compiler warnings :
;   Undefined function MOSS::HAS-NAME, in $E-PERSON=I=0=SUMMARY.

? $FN.90
(($TYPE (0 $FN)) ($ID (0 $FN.90)) ($MNAM (0 =SUMMARY)) ($ARG (0 NIL))
 ($CODT (0 ((HAS-NAME)))) ($FNLS.OF (0 $SYS.1)) ($IMS.OF (0 $E-PERSON))
 ($FNAM (0 $E-PERSON=I=0=SUMMARY)))
? $E-PERSON=I=0=SUMMARY
(LAMBDA NIL (HAS-NAME))
? (definstance person ("name" "Albert")("age" 23)(:var _albert))
$E-PERSON.25
? (send _albert '=summary)
("Albert")

? (definstmethod =test "concept" () (print "test"))
MOSS::$FN.199

? (definstmethod =test 'concept () (print "test"))
;***Error while defining method =TEST, bad selector syntax:
; 'CONCEPT 
; selector should be a symbol, a string or an mln.

? (definstmethod =test concept () (print "test"))
; Warning: redefining method =TEST for class CONCEPT
; While executing: %MAKE-METHOD
$FN.199
|#
;;;------------------------------------------ %MAKE-METHOD-RESOLVE-SELECTOR-LIST
;;; We have 2 cases
;;; 1. Definstmethod: method is attached to the class and applies to instances
;;;   if selector is a symbol, then it can be 
;;;     1.1 the name (ep) of the class, e.g. PERSON, ENTITY
;;;     1.2 the name of a variable containing the class id, e.g. _person
;;;     1.3 the class id, e.g. $E-PERSON, $ENT
;;; 2. Defownmethod: method is attached to object
;;;   if selector is a symbol, then it can be
;;;     2.1 the name of a variable containing the object id
;;;     2.2 the id of the object, e.g. PERSON, $E-PERSON.12
;;; The problem is the ambiguity between case 1.1 and case 2.2 when selector is
;;; an entry. In case 1.1 we must return the class id, in case 2.2 we return the 
;;; symbol.
;;; To distinguish between the cases, we introduce the method-type argument

(defUn %make-method-resolve-selector-list (selector method-name)                                                      
  "takes a selector list and tries to return a MOSS object id. Input may be:
        (<entry ref> <attribute ref> <concept ref> {:class-ref <class ref>}
                     {:select <fcn>)}
where ref indicates mln, string, symbol.
   When there is an ambiguity, tries first a :select function. If none, filters ~
   against the current package, if it does not work, filter against the MOSS ~
   package.
Arguments:
   selector: as described
   method-name: method we are defining
Return:
   a MOSS object-id or a list possibly empty."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (entry-ref (car selector))
        (att-ref (cadr selector))
        (class-ref (caddr selector))
        (options (cdddr selector))
        entry att-id class-id obj-id-list fn
        ref-class-ref ref-class-id result method-list object-list method-id)

    ;; first get the class id from the ref (returns a list)
    ;;********** how about virtual classes ??? Can we attach methods to them ??
    (setq class-id (%%get-id class-ref :class :include-moss t))
    ;; if nil, bad class-ref
    (unless class-id
      (terror "while trying to create method ~A, the selector~%;   ~S~%; does not ~
               reference a valid class in context ~S and package ~S." 
              method-name selector context *package*))

    ;; get attribute corresponding to the specific class
    (setq att-id 
          (%%get-id att-ref :tp :class-ref class-ref :include-moss t))

    ;; if nil, bad attribute ref (error from %get-tp... before getting here)
    (unless att-id 
      (terror "while trying to create method ~A, the selector~%;   ~S~%; does not ~
               reference a valid attribute." method-name selector))
    
    ;; get make-entry method from attribute
    ;; can't use get-method while bootstraping
    ;(setq fn (get-method att-id '=make-entry))
    ;; get all the attribute own-methods
    (setq method-list (%get-value att-id '$OMS))
    ;; get the list of =make-entry methods
    (setq object-list 
          (if (%pdm? '=make-entry)
            (%get-value '=make-entry (%make-id-for-inverse-property '$MNAM))))
    ;; is there a method common to both lists?
    (setq method-id (car (intersection method-list object-list)))
    
    ;; if OK get it, if does not exist, then problem
    (if method-id
      (setq fn (car (%get-value method-id '$FNAM)))
      (terror "while trying to create method ~A, the attribute in the selector~%~
               ;   ~S ~%; has no entry points in package ~S context ~S." 
              method-name selector (package-name *package*) context))

    ;; apply fn to entry-ref
    (setq entry (car (funcall fn entry-ref)))
    
    ;; then, get object id
    (setq obj-id-list (%extract-from-id entry att-id class-id))
    
    ;;=== if more than one object, then
    ;; - if objects are properties try the :class-ref option
    ;; - otherwise try the :select option
    (cond
     ((null obj-id-list)
      (terror "while trying to create method ~A, with selector~%;   ~S~%~
               ; no object can be found in package ~S context ~S." 
              method-name selector (package-name *package*) context ))
     ((and (cdr obj-id-list)
           (or (%is-attribute? (car obj-id-list))
               (%is-relation? (car obj-id-list))))
      ;; get class-ref
      (setq ref-class-ref (cadr (member :class-ref options)))
      (if ref-class-ref
          (setq ref-class-id (%%get-id ref-class-ref :class :include-moss t)))
      (unless ref-class-id
        (terror "while trying to create method ~A, the :class-ref option in the ~
                 selector~%;   ~S~%; is missing for disambiguation of~%;   ~S~&~
                 ; or argument is improper, in package ~S context ~S." 
                method-name selector obj-id-list (package-name *package*) context))
      
      ;; otherwise try to get the right property for the class
      (setq result (%determine-property-id-for-class obj-id-list ref-class-id)))

     ;; objects are not properties
     ((cdr obj-id-list)
      ;; try to get discriminationg function
      (setq fn (cadr (member :select options)))
      (unless fn
        (prog (new-list) 
          ;; try to restrict objects to current package JPB 1001
          (setq new-list (copy-tree obj-id-list)) ; careful mapcan is destructive
          (setq new-list 
                (mapcan #'(lambda(xx) 
                            (if (and (symbolp xx)
                                     (eql (symbol-package xx) *package*))
                              (list xx)))
                        new-list))
          ;; when result is exactly one arg, we win
          (if (and new-list (null (cdr new-list)))
            (return (setq result new-list)))
          ;; did not work try MOSS package JPB 1001
          (setq new-list (copy-tree obj-id-list)) ; careful mapcan is destructive
          (setq new-list 
                (mapcan #'(lambda(xx) 
                            (if (and (symbolp xx)
                                     (eql (symbol-package xx) (find-package :moss)))
                              (list xx)))
                        new-list))
          ;; when result is exactly one arg, we win
          (if (and new-list (null (cdr new-list)))
            (return (setq result new-list)))

          ;; otherwise we definitely loose
          (terror "while trying to create method ~A, the :select option in the ~
                   selector~%;    ~S~%; is missing for disambiguation of~%;    ~S" 
                  method-name selector obj-id-list)
          (setq result (funcall fn obj-id-list))
          )))
     ((setq result (car obj-id-list))))
    
    ;; if still more than one object, then error
    (cond
     ((and (listp result)(cdr result))
      (terror "while trying to create method ~A, with selector~%;   ~S, ~
               ~%; more than one object selected:~%;   ~S." 
              method-name selector obj-id-list))
     ((listp result) (car result))
     ((symbolp result) result)
     ((terror "while trying to create method ~A, the selector~%;   ~S~%; does not ~
               reference a valid object identifier:~%;   ~S"
              method-name selector result)))
    ))

#|
(setq *verbose* t)
(moss::%make-method-resolve-selector-list 
 '(MOSS-COUNTER HAS-MOSS-ENTITY-NAME MOSS-ENTITY) "test")
$CTR

(moss::%make-method-resolve-selector-list 
 '("moss counter" HAS-MOSS-ENTITY-NAME MOSS-ENTITY) "test")
$CTR

(moss::%make-method-resolve-selector-list 
 '("moss counter" HAS-MOSS-ENTITY-NAME "moss entity") "test")
$CTR

(moss::%make-method-resolve-selector-list 
 '("moss counter" "MOSS ENTITY Name" " moss entity") "test")
$CTR

(catch :error (moss::%make-method-resolve-selector-list 
       '("first name" " property Name" "attribute") "test"))
";***Error while trying to create method test, with selector
;   (\"first name\" \" property Name\" \"attribute\")
; no object can be found in package \"MOSS\" context 0."

(catch :error (moss::%make-method-resolve-selector-list 
       '("first name" "moss property Name" "moss attribute") "test"))
";***Error while trying to create method test, the :class-ref option in the selector
;   (\"first name\" \"moss property Name\" \"moss attribute\")
; is missing for disambiguation of
;   ($T-PERSON-FIRST-NAME $T-FIRST-NAME)
; or argument is improper, in package \"MOSS\" context 0."

;;;=== tests in package :cl-user
(moss::%make-method-resolve-selector-list 
   '("first name" "moss property Name" "moss attribute" :class-ref "person")
   "test")
$T-PERSON-FIRST-NAME

(catch :error 
       (moss::%make-method-resolve-selector-list 
        '("pile" "  Name" "person") "test"))
;***Error while trying to create method test, the :select option in the selector
;    ("barthes" "  Name" "person")
; is missing for disambiguation of
;    ($E-PERSON.11 $E-PERSON.10 $E-PERSON.9 $E-PERSON.8 $E-PERSON.7 $E-PERSON.6
      $E-PERSON.5 $E-PERSON.4 $E-STUDENT.1 $E-PERSON.3 $E-PERSON.2 $E-PERSON.1)

? (moss::%make-method-resolve-selector-list 
      '("barthes" "  Name" "person" :select cadr) "test")
$E-PERSON.10

?  (moss::%make-method-resolve-selector-list 
      '("barthes" "  Name" "person" :select cddr) "test")
;***Error while trying to create method test, with selector
;   ("barthes" "  Name" "person" :SELECT CDDR), 
; more than one object selected:
;   ($E-PERSON.11 $E-PERSON.10 $E-PERSON.9 $E-PERSON.8 $E-PERSON.7 $E-PERSON.6
     $E-PERSON.5 $E-PERSON.4 $E-STUDENT.1 $E-PERSON.3 $E-PERSON.2 $E-PERSON.1).

? (moss::%make-method-resolve-selector-list 
      '("albert" "first name" " person") "test")
;***Error while trying to create method test, the attribute in the selector
;   ("albert" "first name" " person") 
; has no entry points.

? (moss::%make-method-resolve-selector-list 
      '("albert" "first name" " pers") "test")
;***Error while trying to create method test, the selector
;   ("albert" "first name" " pers")
; does not reference a valid class.

? (moss::%make-method-resolve-selector-list 
      '("albert" "first nammme" " person") "test")
; Warning: bad property name HAS-FIRST-NAMMME (in package: #<Package "COMMON-LISP-USER">)
; While executing: MOSS::%GET-PROP-ID-FROM-NAME-AND-CLASS-REF

? (moss::%make-method-resolve-selector-list 
      '(FIRST-NAME has-property-Name attribute :class-ref "PERSON") '=if-needed)
$T-PERSON-FIRST-NAME
|#
;;;=============================================================================
;;;
;;;                            OWN METHOD 
;;;
;;;=============================================================================

;;;------------------------------------------------------------- %MAKE-OWNMETHOD
;;;********** to change
;;; if method exists and is modified we should store the old value of the 
;;; function, otherwise the function is a newly created object

(defUn %make-ownmethod (ref selector arg-list body &rest option-list)
  "Defines an own-method, i.e. a method specific to an object and directly ~
   attached to the object. It is an error if the object is not already defined. ~
   When selector is a symbol then it is considered an object-id.
    Otherwise syntax is:
 	(entry tp-name class {:select function}{:class-ref <class-ref>} )
    where function is used when more than one object is reached via the ~
   access path entry-tp-name-class, and class-ref to select properties.
    When the method does not already exist, ~
   then a method object is first created; then the method is compiled and ~
   attached to an internal name build from the class-id, the current version, ~
   and the method-name. If the method already exists, then the code and arguments ~
   are updated, and the method is recompiled.
Arguments:
   ref: method-name (symbol, string, :default-make-entry)
   selector: objec-id or selector
   arg-list: typical lisp arg-list
   body: list: body of the function
   option-list (rest): possible :export option
Return:
   id of the method."
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    
    ;; if forward references are allowed record action onto the *deferred-methods* list
    (when *allow-forward-references*
      (if (eql *deferred-mode* :class) 
          (push `(with-environment ',*package* ',context ',*language*
                   (apply #'%make-ownmethod ',ref ',selector ',arg-list ',body 
                          ',option-list)) 
                *deferred-methods*)
        (push `(with-environment ',*package* ',context ',*language*
                 (apply #'%make-ownmethod ',ref ',selector ',arg-list ',body 
                        ',option-list)) 
              *deferred-instance-methods*))
      (return-from %make-ownmethod nil))
    
    (let ((name (if (eql ref :default-make-entry) 
                    '=make-entry 
                  (%make-name-for-method ref))) ; JPB 1507
          (export (car (getv :export option-list)))
          fn-id obj-id method-function-name)
      ;; when selector is a symbol, it must be an object identifier or the
      ;; name of a variable whose value is an object identifier
      (when (symbolp selector)
        ;; if selector is an object id, attach method to the object
        (cond
         ((%pdm? selector)
          (setq obj-id selector))
         ;; if the name of a variable containing the id of an object
         ((and (boundp selector)
               (%pdm? (symbol-value selector)))
          (setq obj-id (symbol-value selector)))
         ;; otherwise, we don't know what selector is
         ((terror "non existing object ~A, while attempting ~
                 to create method ~A" selector name)))
        ;+++ issue warning for own methods attached to objects directly
        (if *warning-for-own-methods*
            (verbose-warn "attaching method ~A, to object ~A" name selector))
        )
      
      ;; ... otherwise selector has a more complex syntax
      ;; e.g. (ALBERT HAS-NAME PERSON :select $$agemax)
      ;; or   (HAS-NAME HAS-PROPERTY-NAME TERMINAL-PROPERTY :class-ref PERSON)
      ;; we use class-id for designating the object to cope with property trees
      (when (listp selector)
        (setq obj-id (%make-method-resolve-selector-list selector name)))
      ;; If method exists for the specific object we will redefine it!
      (if (and (%pdm? name)	; entry-point?
               (setq fn-id	; usurp value for a short while
                     (car
                      (intersection
                       ;; list of all methods with same name
                       (%get-value name (%make-id-for-inverse-property '$MNAM))  
                       ;; list of all methods belonging to obj-id
                       (%get-value obj-id '$OMS)))))
          (progn
            ;; here method is redefined for the object
            (verbose-warn "redefining method ~A for object ~A" name 
                          (if (listp selector) (car selector) selector))
            ;; when editing
            (save-old-value fn-id)
            
            (if (stringp (car body))
                (%%set-value fn-id (car body) '$DOCT context))
            )      
        (progn
          ;; otherwise create it
          ;; export name
          #-MICROSOFT
          (when export
            (proclaim `(special ,name)) ; JPB040913
            (eval-when (compile load eval) 
              (export (list name) (symbol-package name))) ; JPB040913
            )
          ;; there is something wrong with the previous 3 lines that must be used
          ;; in the OMAS context?
          ;(trformat "%make-own-method /name: ~S for ~S" name obj-id)
          #+MICROSOFT
          (eval-when (compile load eval) 
            (when export
              (proclaim `(special ,name))
              (export (list name))))
          ;(trformat "%make-own-method /exported name: ~S" name)
          ;; make new symbol JPB 0912
          (setq fn-id 
                (if (eql *package* (find-package :moss))
                    ;; make MOSS methods
                    (%make-id '$FN :value (%get-and-increment-counter '$FN))
                  ;; otherwise make application methods
                  (intern 
                   (format nil "$FN.~S"
                     (%get-and-increment-counter 
                      (intern "$FN" *package*)))
                   *package*)))
          ;; build new entity (code and args are available in function object)
          ;(print `(+++ ,*package* ,fn-id))
          ;; build new entity
          (set fn-id 
               `(($TYPE ,(if (eql *package* (find-package :moss))
                             (list context '$FN)
                           (list context (intern "$FN" *package*)))) ; JPB 0912
                 ($ID (,context ,fn-id))
                 ($MNAM (,context ,name))
                 ,@(if (stringp (car body))
                       `(($DOCT (,context ,(car body)))))
                 ))
          
          ;; when editing
          (save-new-id fn-id)
          
          ;; build entry-point if it does not exist already
          (%make-ep name '$MNAM fn-id :export export)
          ;; add new method-name to system-list
          (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$FNLS fn-id)
          ;; link to class
          (%link obj-id '$OMS fn-id)
          ))
      
      ;; define a global symbol for holding method code
      (if (eql ref :default-make-entry)    
          (progn
            (setq method-function-name 'default-make-entry)
            ;; save as an old value to prevent saving as a new value that will be erased
            ;; in case of abort
            (save-old-value 'default-make-entry)
            (%build-method method-function-name '(value-list) 
                           '("default make-entry fonction"
                             (%make-entry-symbols value-list)))
            )
        (progn
          (setq method-function-name 
                (%make-name-for-own-method-function name obj-id))
          ;; ...build and attach code to method-name and compile it
          (%build-method method-function-name arg-list body)
          ))
      
      ;; record internal method name
      (%%set-value fn-id method-function-name '$FNAM context)
      
      ;; put reference onto p-list of object
      (%putm obj-id method-function-name name context :own)
      
      ;; return method-name
      fn-id)))

#|
(defownmethod =xi (HAS-AGE HAS-MOSS-PROPERTY-NAME MOSS-ATTRIBUTE :class-ref PERSON) (data)
    (if (numberp data) data))
$FN.91
(($TYPE (0 $FN)) ($ID (0 $FN.91)) ($MNAM (0 =XI)) ($ARG (0 (DATA)))
 ($CODT (0 ((IF (NUMBERP DATA) DATA)))) ($FNLS.OF (0 $SYS.1))
 ($OMS.OF (0 $T-PERSON-AGE)) ($FNAM (0 $T-PERSON-AGE=S=0=XI)))
$T-PERSON-AGE=S=0=XI
(LAMBDA (DATA) (IF (NUMBERP DATA) DATA))

$T-PERSON-AGE
(($TYPE (0 $EPT)) ($ID (0 $T-PERSON-AGE)) ($PNAM (0 (:EN "Age")))
 ($ETLS.OF (0 $SYS.1)) ($IS-A (0 $T-AGE)) ($INV (0 $T-AGE.OF))
 ($PT.OF (0 $E-PERSON)) ($OMS (0 $FN.91)))

;;;===  tests in package cl-user
(defownmethod =xi (AGE HAS-PROPERTY-NAME ATTRIBUTE :class-ref PERSON) (data)
    (if (numberp data) data))
$FN.200 
Aborted

(defownmethod =xi ("AGE" "PROPERTY NAME" "ATTRIBUTE" :class-ref "PERSON") (data)
    (if (numberp data) data))
; Warning: redefining method =XI for object AGE
; While executing: MOSS::%MAKE-OWNMETHOD
$FN.200
|#
;;;=============================================================================
;;;
;;;                            UNIVERSAL METHOD 
;;;
;;;=============================================================================

;;; Defining first defuniversal function
;;; It needs the existence of $UNI.CTR (method counter) to do so, but not much 
;;; else (defined below as defsysvar, and so is recorded???)
;;; BUG. does not accept ref as names, e.g. string or MLN

;;;------------------------------------------------------------- %MAKE-UNIVERSAL

(defUn %make-universal (ref arg-list body &rest option-list) 
  
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    
    ;; if forward references are allowed record action onto the *deferred-methods* list
    ;; should not we keep the instantiation for the end?
    (when *allow-forward-references*
      (push `(with-environment ',*package* ',context ',*language*
               (apply #'%make-universal ',ref ',arg-list ',body ',option-list))
            *deferred-methods*)
      (return-from %make-universal nil))
    
    (let ((export (car (getv :export option-list)))
          (uni-id (moss::%%get-id "moss universal method" :concept))
          (name (%make-name-for-method ref)) ; JPB 1507
          fn-id method-id doc)
      ;; try to get the universal method if it exists
      (setq fn-id (car (%extract-from-id  name '$UNAM uni-id)))
      ;(trformat "fn-id: ~S" fn-id)
      ;; check if universal method already exists
      (if fn-id
          (progn
            ;;== when universal method already exists...
            (verbose-warn "~&redefining universal method ~A: " name)
            
            ;; we must save method and associated function
            ;; when editing
            (save-old-value fn-id)
            ;; must update doc, otherwise the rest remains unchanged
            (if (stringp (car body))
                (%%set-value fn-id (car body) '$DOCT context))
            ;; get the method-id, actually the internal method function name
            (setq method-id (car (%%get-value fn-id '$FNAM context)))
            ;; when editing
            (save-old-value method-id)
            )
        
        ;;== otherwise create the method
        (progn
          (eval-when (compile load eval) 
            (when export
              (proclaim `(special ,name))
              (export (list name)))
            )
          ;(trformat "%make-universal /exported name: ~S" name)
          
          ;; make new symbol
          (setq fn-id (%make-id uni-id :value (%get-and-increment-counter uni-id)))
          ;(trformat "fn-id: ~S" fn-id)
          ;; look for documentation string
          (setq doc (if (stringp (car body)) (car body)))
          ;; build new entity (method object)
          (set fn-id (copy-list
                      `(($TYPE (,context ,uni-id))
                        ($ID (,context ,fn-id))
                        ($UNAM (,context ,name))
                        ,@(if doc `(($DOCT (,context ,doc))))
                        ;($CODT (,context ,(if (stringp (car body)) (cdr body) body)))
                        )))
          ;(trformat "~S: ~S" fn-id (eval fn-id))
          ;; when editing
          (save-new-id fn-id)
          ;; add new method-id to system-list
          (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$FNLS fn-id)
          (%make-ep name '$UNAM fn-id :export export)
          
          ;; cook up method name
          (setq method-id (%make-name-for-universal-method-function name))
          ;; when editing
          ;(save-new-id method-id)
          ;; record internal method name
          (%%set-value fn-id method-id '$FNAM context)
          ;(trformat "~S: ~S" fn-id (eval fn-id))
          )
        )
      
      ;;=== now build or rebuild the function
      ;;********** watch %build-method saves method-id (useful when redefining?)
      (%build-method method-id arg-list body)
      
      ;; put reference onto p-list of name!
      (%putm name method-id name context :universal)
      ;; return method-name
      fn-id)))


;;;=============================================================================
;;;
;;;                            INDIVIDUAL (Instance) 
;;;
;;;=============================================================================

;;; This is a simple default method to create instances of kernel objects
;;; We have two problems when making instances
;;;   - the corresponding class and properties must have been defined
;;;   - other instances that we want to link must have been created
;;; To resolve the issue, we must
;;;   - defer creating instances after all concepts and properties have been
;;;     created
;;;   - assign unique variables to the instances so that we can find them later
;;;     through the :name or :var option. We must defer linking the instances.
;;; Note. SOL uses thhe :name option as an MLN

;;;-------------------------------------------------------------- %MAKE-INSTANCE
;;;
;;; Ex. (%make-instance "person" ("name" "john")("age" 32))
;;; Multiple class belonging
;;;     (%make-instance '("student" "teacher") ...)

(defUn %make-individual (&rest ll) (apply #'%make-instance ll))
(defUn make-individual (&rest ll) (apply #'%make-instance ll))

(defUn %make-instance (class-ref &rest option-list)
  "checks whether name is the name of an actual class and if so calls ~
   %%make-instance-from-class-id. Otherwise, sends a warning and throws ~
   an :error label.
   Note that it is used to create orphans, instances of the MOSS-NULL-CLASS.
Arguments:
   class-ref: symbol, string or multilingual name of the class 
              of the object being created, or a list of them
   option-list (opt): a list of properties for the instance
     - (:one-of <list of objects>)
     - (:no-warning t/nil)
     - <property>
     - (:doc <doc MLN>)
     - (:var <var-name>)
     - (:is-a <obj-id>)
     - (:name <string or MLN>)  used by SOL to name MLN object
Return:
   the id of the created instance."
 
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    ;; we do not check context since it corresponds to the current context, and thus
    ;; should be legal. However, some if the invoked functions do it.
    
    ;; When we are loading a file allowing forward references, we must postpone all
    ;; instance creations until the deferred list has been executed. Thus, we simply
    ;; add the instance creation to the list of deferred instance creation
    (when *allow-forward-references*
      ;; indicate that we are in the instance part of the file
      (setq *deferred-mode* :instance)
      ;; if there is a :var option we should declare the variable to be global to
      ;; avoid compiler messages (undeclared free variable...)
      (if (assoc :var option-list)
          (proclaim `(special ,(cadr (assoc :var option-list))))) ; JPB0702
      ;; then, create entry in the *deferred-instances* list
      (push `(with-environment ',*package* ',context ',*language*
               (apply #'%make-instance ',class-ref ',option-list))
            *deferred-instances*)
      (return-from %make-instance :made-deferred-instance))
    
    (let (class-id id)
      
      ;;=== if we have a list, then we have a multiple class belonging
      (when (listp class-ref)
        ;; call a function to deal with an instance of multiple class
        (setq id (%make-multiple-instance class-ref option-list))
        ;;********** build main object 
        ;; this is plain wrong. we must build the object using the properties and values
        ;; corresponding to each different class.
        ;; we can build the object as an instance of the first class using the properties
        ;; of this class, but then we must add the properties and values of the others
        ;; classes and finally, any property not belonging to any class.
        ;; The precedence list is the order of the classes
        ;(setq id (apply #'%make-instance (car class-ref) option-list))
        ;; for the other classes, build references
        ;(dolist (next-class-ref (cdr class-ref))
        ;  (multiple-value-bind (next-id class-id) (%make-ref-object next-class-ref id)
        ;    ;; add reference to object
        ;    (set id (%%add-value id '$ID next-id context))
        ;    (set id (%%add-value id '$TYPE class-id context))))
        ;; return something?
        (return-from %make-instance id))
      
      ;; OK here we have a single class-ref
      ;; check if the class exists (does not include MOSS classes)
      ;(format t "~%; %make-instance /*package*: ~S" *package*)
      (setq class-id (%%get-id class-ref :class))
      ;(format t "~%; %make-instance /class-id: ~S" class-id)
      
      (unless class-id
        (mthrow " while trying to define instance of class ~A, ~
                 class does not exist in package ~S and context ~S." 
                class-ref (package-name *package*) context))
      
      ;; if the class contains the property $ONEOF, then we are not allowed to
      ;; create new instances, send a warning but do it nevertheless
      ;; (it is interesting to note that we cannot specify the values of the ONE-OF
      ;; option while defining the class, because we cannot create instances
      ;; before the class is actually created... However, we can add them later.)
      (if (%get-value class-id '$ONEOF)
          (warn "trying to create a new instance for the class ~A violating its ~
                 ONE-OF restriction, in package ~S and context ~S. We do it anyway."
            class-ref (package-name *package*) context))
      
      ;; now call the function that does the job
      (apply #'%%make-instance-from-class-id class-id option-list))))

#|
;;; tests for multipl class belonging. This is currently not recommended. It is 
;;; better to use a class with multiple inheritance.
(defconcept "Student" (:att "Name" (:entry)))
$E-STUDENT
(%make-instance '("student"))
(%make-instance '("student" "person"))
$E-STUDENT.2
(($TYPE (0 $E-STUDENT $E-PERSON)) ($ID (0 $E-STUDENT.2 $E-PERSON.3)))

$E-PERSON.CTR
(($TYPE (0 $CTR)) ($VALT (0 9)) ($CTRS.OF (0 $E-PERSON)) ($SVL.OF (0 $SYS.1)))

(defconcept "butcher")
$E-BUTCHER

(defconcept "baker")
$E-BAKER

(defconcept "seller")
$E-SELLER

(defindividual ("butcher" "baker" "seller"))
$E-BUTCHER.10
(($TYPE (0 $E-BUTCHER)) ($ID (0 $E-BUTCHER.10 $E-BAKER.9 $E-SELLER.1)))

Versions

|#
;;;------------------------------------------------ %%MAKE-INSTANCE-ADD-PROPERTY

(defun %%make-instance-add-property (key class-ref prop-ref value-list no-warning)
  "adds a property to an object with associated values.
Arguments:
   key: id of the object to be modified
   class-ref: ref of the corresponding class
   prop-ref: reference of property
   value-list: list of values associated with property
   no-warning: a flag to prevent printing warnings
Return:
   the content of the object"
  (declare (special *language*))
  (let* ((class-id (%%get-id class-ref :class))
         (context (symbol-value (intern "*CONTEXT*")))
         (name (car (%%get-value class-id '$ENAM context)))
         (prop-list (%get-properties key))
         tp-id sp-id) 
    ;(format t "~%;--- class-id: ~S" class-id)
    ;(format t "~%;--- *package*: ~S" *package*)
    ;(format t "~%;--- context: ~S" context)
    ;(format t "~%;--- name: ~S" name)
    ;(format t "~%;--- prop-list: ~S" prop-list)
    (case prop-ref
      ;; ignore control options
      (:no-warning)
      
      (:name  ; JPB060228
       ;; if name is a simple string add current language tag
       (%make-instance-name-option key name 
                                   (if (cdr value-list)
                                       value-list
                                     (cons *language* value-list))))
      
      ;; check for external variable name option (should be unique)
      (:var
       (if (%is-variable-name? (car value-list))
           (set (car value-list) key)
         (warn "~S is not a valid variable name. We ignore it." (car value-list))))
      ;; :name option is used by SOL to give an "object name" to the instance
      ;; may not be unique... MOSS should use :var option
      ;; e.g. (:name "aquitaine") or (:name :en "London" :fr "Londres")
      
      ;; doc? JPB060228
      (:doc	
       (%process-doc-option key value-list)
       )
      
      ;; must be a property
      (otherwise
       (cond
        ;; is it an attribute attached to the class? Use current package
        ((setq tp-id (%%get-id prop-ref :tp :class-ref class-id :include-moss t))
         (%make-instance-tp-option name key prop-ref tp-id value-list prop-list))
        
        ;; is it a generic attribute?
        ((%is-attribute?
          (setq tp-id (%%get-id prop-ref :tp :include-moss t)))
         (unless no-warning
           (warn "property ~S is not an attribute of class ~S. We add it anyway."
             prop-ref (mln::get-canonical-name name)))
         ;; we trick the following function by making the class-list contain the
         ;; generic property
         (%make-instance-tp-option name key prop-ref tp-id value-list 
                                   (list tp-id)))
        
        ;; structural property ?
        ((setq sp-id (%%get-id prop-ref :sp :class-ref name :include-moss t))
         
         ;(format t "~&; %%make-instance-from-class-id/ class: ~S sp: ~S deferred: ~S"
         ;        name prop-name *allow-forward-instance-references*)
         ;; Here, if we are allowing forward references, we defer creation
         (if *allow-forward-instance-references*
             (push
              `(with-environment ',*package* ',context ',*language*
                 (funcall #'%make-instance-sp-option ',key ',class-id ',sp-id
                          ',value-list))
              *deferred-instance-links*)
           ;; otherwise make link
           (%make-instance-sp-option key class-id sp-id value-list )))
        
        ;; here we did not recognize a property
        (t
         (warn 
             "while trying to define instance of class ~S in package ~S context ~S, ~
               property ~S does not exist. We ignore it." 
           (mln::get-canonical-name name) *package* context prop-ref))
        )))
    ;; return the content of the modified object (debugging purposes)
    (symbol-value key)))

;;;----------------------------------------------- %%MAKE-INSTANCE-FROM-CLASS-ID
;;; make-instance should check for global object constraints
;;; Currently constraints at the class level is
;;;   :one-of 
;;; meaning that the object must be one among the list
;;; The goal is not to restrict the user to create new instances of the class
;;; but to warn that this could violate a restriction.
;;; Therefore the :one-of constraint shold be considered as a usual or typical 
;;; constraint not as a strong constraint, like cardinality constraints.

;;; Missing the :is-a property should do something like for concepts
  
(defUn %%make-instance-from-class-id (class-id &rest option-list)
  "assume that class-id has been checked and coresponds to an actual class.
   Does not create instances of system objects.
Arguments:
   class-id: identifier of class for which we want an instance
   option-list: list of property/values for the class
Return:
   id of instance"
  ;(format t "~2%;---------- ~A: Entering %%make-instance-from-class-id" 
  ;  (package-name *package*))
  ;(format t "~%;--- class-id: ~S" class-id)
  ;format t "~%;--- option-list: ~%   ~S" option-list)
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (name (car (%%get-value class-id '$ENAM context)))
         (no-warning (assoc :no-warning option-list))
         key prop-list option-tag
         tp-id sp-id value-list)
    ;(format t "~%;---------- %%make-instance-from-class-id")
    ;(format t "~%;--- class-id: ~S" class-id)
    ;(format t "~%;--- option-list ~S" option-list)
    ;(format t "~%;--- *package*: ~S" *package*)
    ;(format t "~%;--- *context*: ~S" *context*)
    ;(format t "~%;--- context: ~S" context)
    ;(format t "~%;--- name: ~S" name)
    
    ;; when the name of the class is an MLN should recover the right string
    ;; we use get-canonical-name to avoid the *language* = :ALL situation
    (if (or (mln::mln? name)
            (mln::%mln? name)
            )
        (setq name (mln::get-canonical-name name))) ; jpb 1411
    ;; question: could language be part of the option-list and if so what is the
    ;; associated semantics?
    ;(format t "~%; %%make-instance-from-class-id /*language*: ~S" *language*)
    ;(format t "~%; %%make-instance-from-class-id /name: ~S" name)
    
    ;(format t "~%;--- 1 (sv (intern...)) ~S" (symbol-value (intern "*CONTEXT*")))
    ;; Then we ask for the creation of an object (adding :id JPB 1001)
    (setq key (%create-basic-new-object class-id context))
    ;(format t "~%;--- key: ~S" key)
    
    ;(format t "~%;--- (sv (intern ...)) ~S" (symbol-value (intern "*CONTEXT*")))
    ;; get list of all possible properties from classes and superclasses
    (setq prop-list (%get-properties key)) 
    ;(format t "~%;--- prop-list: ~S" prop-list)
    
    ;;======== then we check options (tp and sp)
    ;(format t "~2%;--- loop on the option list: ~S" option-list)
    (dolist (option option-list)
      (setq option-tag (car option) 
          value-list (cdr option)
          sp-id nil 
          tp-id nil
          )
      ;(format t "~%;=== option-tag: ~S" option-tag)
      ;(format t "~%;--- (sv (intern...)) ~S" (symbol-value (intern "*CONTEXT*")))
      ;(format *debug-io* "~&+++ %%make-instance-from-class-id; option-tag: ~S ~
      ;                    in package: ~S" option-tag *package*)
      (catch 
       :start-of-loop
       (case option-tag
         ;;== ignore control options
         (:no-warning)
         
         ;;== doc? JPB060228
         (:doc	
          (%process-doc-option key value-list)
          )
         
         ;;== :is-a is somehow similar to a relation
         (:is-a
          ;; Here, if we are allowing forward references, we defer creation
          (if *allow-forward-instance-references*
              (push
               `(with-environment ',*package* ',context ',*language*
                  (funcall #'%make-instance-isa-option ',key ',value-list))
               *deferred-instance-links*)
            ;; otherwise make link
            (%make-instance-isa-option key value-list))
          )
         
         ;;== :var check for external variable name option (should be unique)
         (:var
          (if (%is-variable-name? (cadr option))
              (set (cadr option) key)
            (warn "~S is not a valid variable name. We ignore it." (cadr option))))
         
         ;;== :name option is used by SOL to give an "object name" to the instance
         ;; may not be unique... MOSS should use :var option
         ;; e.g. (:name "aquitaine") or (:name :en "London" :fr "Londres")
         (:name  ; JPB060228
          ;; if name is a simple string add current language tag
          (%make-instance-name-option key name 
                                      (if (cdr value-list)
                                          value-list
                                        (cons *language* value-list))))
         ;;== must be a property
         (otherwise
          (cond
           ;; is it an attribute attached to the class? Use current package
           ;((prog1 nil
           ;   (format t "~%;--- class-ref: ~S" name)
           ;   (format t "~%;--- tp-id: ~S" 
           ;     (%%get-id option-tag :tp :class-ref name :include-moss t))))
           ((setq tp-id (%%get-id option-tag :tp :class-ref name :include-moss t))
            (%make-instance-tp-option name key option-tag tp-id value-list prop-list)
            ;(format t "~%;---1 (sv (intern...)) ~S" (symbol-value (intern "*CONTEXT*")))
            )
           
           ;; is it a generic attribute? i.e. no class-ref: does not belong to class
           ;((prog1 nil (format t "~%--- tp-id (2): ~S"
           ;              (%%get-id option-tag :tp :include-moss t))))
           ((%is-attribute?
             (setq tp-id (%%get-id option-tag :tp :include-moss t)))
            (unless no-warning
              (warn "property ~S is not an attribute of class ~S. We add it anyway."
                option-tag (mln::extract-to-string name :canonical t)))
            ;; we trick the following function by making the class-list contain the
            ;; generic property
            (%make-instance-tp-option name key option-tag tp-id value-list 
                                      (list tp-id))
            ;(format t "~%;---2 (sv (intern...)) ~S" (symbol-value (intern "*CONTEXT*")))
            )
           
           ;;== structural property ?
           ;((prog1 nil
           ;   (format t "~%;--- sp-id: ~S"
           ;     (%%get-id option-tag :sp :class-ref name :include-moss t))))
           
           ((setq sp-id (%%get-id option-tag :sp :class-ref name :include-moss t))
            
            ;(format t "~&; %%make-instance-from-class-id/ class: ~S sp: ~S deferred: ~S"
            ;        name option-tag *allow-forward-instance-references*)
            ;; Here, if we are allowing forward references, we defer creation
            ;;********** we should defer linking only for suc-ids we can't resolve!
            (if *allow-forward-instance-references*
                (push
                 `(with-environment ',*package* ',context ',*language*
                    (funcall #'%make-instance-sp-option ',key ',class-id ',sp-id
                             ',value-list))
                 *deferred-instance-links*)
              ;; otherwise make link
              (%make-instance-sp-option key class-id sp-id value-list )))
           
           ;; here we look for a generic property in case of orphans
           ((setq sp-id (%%get-id option-tag :sp :include-moss t))
            
            ;(format t "~&; %%make-instance-from-class-id/ class: ~S sp: ~S deferred: ~S"
            ;        name option-tag *allow-forward-instance-references*)
            ;; Here, if we are allowing forward references, we defer creation
            ;;********** we should defer linking only for suc-ids we can't resolve!
            (if *allow-forward-instance-references*
                (push
                 `(with-environment ',*package* ',context ',*language*
                    (funcall #'%make-instance-sp-option ',key ',class-id ',sp-id
                             ',value-list))
                 *deferred-instance-links*)
              ;; otherwise make link
              (%make-instance-sp-option key class-id sp-id value-list )))
           
           ;; here we did not recognize the option viewed as a property
           (t
            (warn 
                "while trying to define instance of class ~S in package ~S ~
                 context ~S, property ~S does not exist. We ignore it." 
              (if (or (mln::mln? name)(mln::%mln? name))
                  (mln::get-canonical-name name)
                name)
              (package-name *package*) context option-tag)) ; jpb1406
           )))))
    
    ;; overall check on the entity
    ;; check the :one-of class constraint
    (let ((oneof-list (%%get-value class-id '$ONEOF context)))
      (if (and oneof-list (not (member key oneof-list)))
          (warn "instances of ~S should be one of: ~%~S~%We create the new instance ~
               ~S anyway..." class-id oneof-list key)))
    
    ;; if class is tagged with a :dont-save-instances mark, we tag instance with a
    ;; :no-save mark to avoid saving it when the agent is persistent
    ;; this will unling the entry points JPB 1001
    (if (get class-id :dont-save-instances)
        (setf (get key :no-save) t))
    ;; we record instance at the system level
    ;(%%set-value *moss-system* key '$SVL context) ; JPB0406 strange line...
    ;; when editing
    (save-new-id key)
    
    key  ;; return key
    ))

#|
(%defconcept (:en "city ; town" :fr "ville")
               (:att (:en "name" :fr "nom")(:entry)))
$E-CITY

(%definstance "ville" ("name" "London"))
$E-CITY.6

London
(($TYPE (0 $EP)) ($T-NAME.OF (0 $E-CITY.6)) ($EPLS.OF (0 $SYS.1)))

(%definstance "ville" ("nom" "Paris"))
$E-CITY.7
(($TYPE (0 $E-CITY)) ($T-CITY-NAME (0 "Paris")))

Paris
(($TYPE (0 $EP)) ($T-NAME.OF (0 $E-CITY.7)) ($EPLS.OF (0 $SYS.1)))

(%definstance "town" ("name" "Manchester"))
$E-CITY.8

Manchester
(($TYPE (0 $EP)) ($T-NAME.OF (0 $E-CITY.8)) ($EPLS.OF (0 $SYS.1)))

$E-CITY.8
(($TYPE (0 $E-CITY)) ($T-CITY-NAME (0 "Manchester")))

(defconcept "Stranger2"
    (:att Name (:entry))
    (:one-of (("name" "Jess"))(("name" "jose"))))

(%%make-instance-from-class-id '$E-STRANGER2 '("name" "Jules"))

; Warning: instances of $E-STRANGER2 should be one of: 
;          ($E-STRANGER2.1 $E-STRANGER2.2)
;          We create the new instance $E-STRANGER2.3 anyway...
; While executing: %%MAKE-INSTANCE-FROM-CLASS-ID
$E-STRANGER2.3

(defobject ("name" "N�1")(:var _o1))

(defobject ("name" "N�2")(:is-a "N�1")(:var _O2))

(defobject ("name" "N�3")(:is-a _O2)(:var _O3))

(defobject ("name" "N�4")(:is-a '*none*.17))
|#
;;;-------------------------------------------------- %MAKE-INSTANCE-ISA-OPTION
;;; any instance may be linked to any onter object that will be used as a 
;;; prototype. The function is similar to sp-option without the special checking

(defun %make-instance-isa-option (key value-list)
  "builds an is-a for an instance object.
Arguments:
   key: id of the instance
   value-list: list of values designating objects to link
                    or list starting with :new to create a new object
Return:
   nil if error, :done otherwise"
  (let ((context (symbol-value (intern "*CONTEXT*")))
        suc-list)
    (dolist (value value-list)
      
      ;(format *debug-io* "~&+++ %%make-instance-isa-option; value: ~S ~S~
      ;                   ~&in package: ~S" value (eval value) *package*)
      
      ;; value is a list starting with :new, e.g. (:new "address" ("street" "..."))
      (when (and (listp value)(eql (car value) :new))
        ;; we must create the corresponding instance (make-instance "address" ...)
        ;; if it does not work then we get a string
        ;; if it works then we get an object identifier that we xan use as a value
        (setq value (apply #'%make-instance (cdr value)))
        ;(format *debug-io* "~&+++ MOSS:%make-instance-sp-option value: ~S" value)
        )
      
      (cond
       ;;=== first case we have a variable name, the variable is unbound and
       ;; we allow forward references (for batch files)
       ((and (%is-variable-name? value)
             (not (boundp value))
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-isa-option ',key ',value)))
              *deferred-instance-creations*)
        ;;********** we cannot return here if there are several values...
        (return-from %make-instance-isa-option :isa-deferred-creation))
       
       ;;=== second case, no forward references allowed, skip the value
       ;; send a warning
       ;; should not happen since we are linking instances after creating them
       ((and (%is-variable-name? value)
             (not (boundp value))
             (not *allow-forward-instance-references*))
        ;; then we skip the link and send warning
        (warn "unbound reference ~S for property ~S. We skip the link." 
          value "MOSS-IS-A"))
       
       ;;=== third case we have a variable bound to a PDM object
       ((and (%is-variable-name? value)
             (boundp value)
             (symbolp (symbol-value value))
             (boundp (symbol-value value))
             (%pdm? (symbol-value value)))
        ;(print `(+++ ref ,(symbol-value value)))
        ;; collect successor
        (push (symbol-value value) suc-list)
        ;; then we link the value
        (%link key '$IS-A (symbol-value value)))
       
       ;;=== fourth case, we have the identifier of a PDM object
       ((and (symbolp value)
             (not (%is-variable-name? value))
             (boundp value)
             (%pdm? value)) ;JPB0506 (symbol-value value) -> value
        ;; then, collect successor
        (push value suc-list)
        )
       
       ;;=== fifth case we have a string (SOL) linked to an object name
       ;; should not happen (done at the %make-instance level)
       ((and (stringp value) 
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-isa-option ',key ,value)))
              *deferred-instance-creations*)
        ;;********** we cannot return here if there are several values...
        (return-from %make-instance-isa-option :isa-deferred-binding))
       
       ;;=== sixth case we have a string, no deferring linking, but $IS-A prop
       ((stringp value)
        ;; we assume here that all values returned by access are PDM objects
        (setq suc-list (access value))
        )
       
       ;;=== eighth case, try the possibility that value is a query
       ((parse-user-query value)
        ;(format *debug-io* "~&+++ MOSS:%make-instance-sp-option value: ~S result: ~S"
        ;  value (access value))
        (setq suc-list (append (access value) suc-list)))
       
       ;;=== any other case is invalid
       (t (warn "something wrong: ~S cannot be is-a of ~S." key value)))
      )
    
    ;(format *debug-io* "~&+++ MOSS:%make-instance-sp-option suc-list: ~S" 
    ;        suc-list)
    
    ;; there are no constraints on the $IS-A property, link objects
    (dolist (suc-id (reverse suc-list))
      ;; link only if object is alive for the current context
      (if (%alive? suc-id context)
          (%link key '$IS-A suc-id)
        (warn "Object ~S is not alive in this context, cannot be is-a of ~S"
          suc-id key)))
    )
  :done)

#|
(defconcept "person" (:att "name" (:entry))(:att "first name"))
$E-PERSON

Various modes of specifying an :is-a object

(defobject ("name" "N�1")(:var _o1))
*NONE*.1

(defobject ("name" "N�2")(:is-a "N�1")(:var _o2))
*NONE*.2
(($TYPE (0 *NONE*)) ($ID (0 *NONE*.2)) ($T-NAME (0 "N�2")) ($IS-A (0 *NONE*.1)))

(defobject ("name" "N�3")(:is-a _O2)(:var _o3))
*NONE*.3
(($TYPE (0 *NONE*)) ($ID (0 *NONE*.3)) ($T-NAME (0 "N�3")) ($IS-A (0 *NONE*.2)))

(defobject ("name" "N�4")(:is-a *none*.1)(:var _o4))
*NONE*.4
(($TYPE (0 *NONE*)) ($ID (0 *NONE*.4)) ($T-NAME (0 "N�4")) ($IS-A (0 *NONE*.1)))

(defobject ("name" "N�5")(:is-a (:new "person" ("name" "N�0"))))
*NONE*.5
(($TYPE (0 *NONE*)) ($ID (0 *NONE*.5)) ($T-NAME (0 "N�5")) ($IS-A (0 $E-PERSON.1)))

(defobject ("name" "N�6")(:is-a ("person" ("name" :is "N�0"))))
*NONE*.6
(($TYPE (0 *NONE*)) ($ID (0 *NONE*.6)) ($T-NAME (0 "N�6")) ($IS-A (0 $E-PERSON.1)))

Now with versions:
(setq *version-graph*'((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))
*version-graph*
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3

(with-context 3
  (defobject ("name" "N�7")(:var _O7)))
*NONE*.7
(($TYPE (3 *NONE*)) ($ID (3 *NONE*.7)) ($T-NAME (0 "N�7")))
|#
;;;--------------------------------------------------------- %MAKE-INSTANCE-LINK

(defUn %make-instance-link (obj1-id sp-id obj2-ref class-id)
  "links two objects namely an instance to another instance.
Arguments:
   obj1-ref: id of the instance to link
   sp-id: id of the relation
   obj2-ref: id of the instance, or ref (:var option in MOSS, :name in SOL)
   context (opt): context
Return:
   internal format of first object"
  (let (entry suc-list)
    (cond
     ;;== first case, we have a sttring
     ((stringp obj2-ref) ; possible with SOL
      ;; we should try to recover objects correponding to the string
      (setq entry (send '$OBJNAM '=make-entry obj2-ref))
      (setq suc-list (%extract-from-id (car entry) '$OBJNAM (intern "*ANY*")))
      )
     ;;== second case we have an object identifier
     ((and (%is-variable-name? obj2-ref)
           (not (boundp obj2-ref)))
      ;; then we skip the link and send warning
      (warn "unbound reference ~S for property ~S while creating an ~
             instance of ~S. We skip the link." 
        obj2-ref sp-id class-id)
      )
     ;;== third case we have a variable bound to a PDM object
     ((and (%is-variable-name? obj2-ref)
           (boundp obj2-ref)
           (symbolp (symbol-value obj2-ref))
           (boundp (symbol-value obj2-ref))
           (%pdm? (symbol-value obj2-ref)))
      ;(print `(+++ ref ,(symbol-value value)))
      ;; collect successor
      (push (symbol-value obj2-ref) suc-list)
      )
     ;;== fourth case, we have the identifier of a PDM object
     ((and (not (%is-variable-name? obj2-ref))
           (symbolp obj2-ref)
           (boundp obj2-ref)
           (%pdm? obj2-ref)) ;JPB0506 (symbol-value value) -> value
      ;; then, collect successor
      (push obj2-ref suc-list)
      )
     ;;== fifth case is we have a query
     ((parse-user-query obj2-ref)
      (setq suc-list (append (access obj2-ref) suc-list)))
     )
    
    ;;== if result is non nil link object1 to list of successors
    (if suc-list
      ;; link objects
      (dolist (suc-id suc-list)
        (%link obj1-id sp-id suc-id))
      ;; otherwise issue a warning
      (warn "something wrong with ~S for property ~S while creating an ~
             instance of ~S. Cannot find the referenced object(s)." 
            obj2-ref sp-id class-id))
    ;; return obj1-l
    (symbol-value obj1-id))
  )

#|
aquitaine
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 AQUITAINE))
 (MOSS::$OBJNAM.OF (0 $E-REGION.1)) (MOSS::$EPLS.OF (0 MOSS::$SYS.1)))

gironde
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 GIRONDE))
 (MOSS::$OBJNAM.OF (0 $E-FRENCH-DEPARTMENT.2)) (MOSS::$EPLS.OF (0 MOSS::$SYS.1)))

(%make-instance-link '$E-REGION.1 '$S-REGION-FRENCH-DEPARTMENT "gironde"
     '$S-REGION)
((MOSS::$TYPE (0 $E-REGION)) (MOSS::$ID (0 $E-REGION.1))
 (MOSS::$OBJNAM (0 (:FR "aquitaine")))
 ($S-REGION-FRENCH-DEPARTMENT (0 $E-FRENCH-DEPARTMENT.2)))


|#
;;;-------------------------------------------------- %MAKE-INSTANCE-NAME-OPTION

(defUn %make-instance-name-option (key name value &key export)
  "builds a name for the object attached to property $OBJNAM. 
   Name is an entry (used by SOL).
Arguments:
   key: id of the instance
   name: name of the class
   value: value associated with :name option may be MLN
   export (key): if t we esport the name
Return:
   nil if error, :done otherwise"
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (tp-id '$OBJNAM)
        entry-point)
    ;;ok, tp is valid. Norm value
    (setq value (-> tp-id '=xi value))
    ;; nil is considered an error 
    (unless value
      (warn
       "while trying to define instance of class ~A, ~
        the :name option ~A has not the proper format."
       name (list :name value))
      ;; on error skip property
      (return-from %make-instance-name-option nil))
    ;; no restrictions are attached to $OBJNAM
    ;; Build entry point if necessary
    (when (get-method tp-id '=make-entry)
      (setq entry-point (-> tp-id '=make-entry value))
      (%make-ep entry-point tp-id key :export export)
      )
    ;; add value to key
    (%add-value key tp-id value context)
    :done))

;;;---------------------------------------------------- %MAKE-INSTANCE-SP-OPTION

(defUn %make-instance-sp-option (key class-id sp-id value-list)
  "builds a relation for an instance object.
Arguments:
   key: id of the instance
   class-id: id of the class
   sp-id: id of the relation
   value-list: list of values designating objects to link
               or list starting with :new to create a new object
Return:
   nil if error, :done otherwise"
  (declare (special *allow-forward-instance-references* *deferred-instance-creations*))
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (class-id-list (%get-value sp-id '$SUC))
         suc-list access-list)
    
    (dolist (value value-list)
      ;(format *debug-io* "~&+++ %%make-instance-sp-option; value: ~S ~
      ;    in package: ~S" value  (package-name *package*))
      
      ;; should apply =filter method here
      (setq value (-> sp-id '=filter value key))
      
      (cond
       ;; value is a list starting with :new, e.g. (:new "address" ("street" "..."))
       ((and (listp value)(eql (car value) :new))
        ;; we must create the corresponding instance (make-instance "address" ...)
        ;; if it does not work then we get a string
        ;; if it works then we get an object identifier that we can use as a value
        (setq value (apply #'%make-instance (cdr value)))
        ;(format *debug-io* "~&+++ MOSS:%make-instance-sp-option value: ~S" value)
        ;; collect suc-id
        (setq suc-list (append suc-list (list value)))  ; JPB1611 to keep order (?)
        ;(push value suc-list)
        )
       
       ;;=== first case we have a variable name, the variable is unbound and
       ;; we allow forward references (for batch files, i.e. loading ontologies)
       ;; when %make-instance is executed we never have this case since when
       ;; *allow-forward-instance-references* is true we do not call this, function

       ((and (%is-variable-name? value)
             (not (boundp value))
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-link ',key ',sp-id ,value ',class-id)))
              *deferred-instance-creations*)
        (return-from %make-instance-sp-option :sp-deferred-creation))
       
       ;;=== second case, no forward references allowed, skip the value
       ;; send a warning
       ;; should not happen since we are linking instances after creating them
       ((and (%is-variable-name? value)
             (not (boundp value)))
        ;; then we skip the link and send warning
        (warn "unbound variable reference ~S for property ~S while creating an ~
               instance of ~S. We skip the link." 
          value sp-id class-id))
       
       ;;=== third case we have a variable bound to a PDM object
       ((and (%is-variable-name? value)
             (boundp value)
             (symbolp (symbol-value value))
             (boundp (symbol-value value))
             (%pdm? (symbol-value value)))
        ;(print `(+++ ref ,(symbol-value value)))
        ;; collect successor
        (push (symbol-value value) suc-list)
        ;; then we link the value
        ;(%link key sp-id (symbol-value value))
        )
       
       ;;=== fourth case, we have the identifier of a PDM object
       ((and (symbolp value)
             (boundp value)
             (not (%is-variable-name? value))
             (%pdm? value) ;JPB0506 (symbol-valsymbol-valueue value) -> value
             ;; object must be instance of a class allowed by $SUC
             ;; otherwise, will be returned as it is by parse-user-query
             ;; should be filtered out by %validate-sp?
             (some #'(lambda(xx)(%type? value xx)) class-id-list)
             )
             
        ;; then, collect successor
        (push value suc-list)
        )
       
       ;;=== fifth case we have a string (SOL) linked to an object name
       ;; should not happen (done at the %make-instance level)
       ((and (stringp value) 
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-link ',key ',sp-id ,value ',class-id)))
              *deferred-instance-creations*)
        ;; why should we return here? Probably assuming single string value...
        ;(return-from %make-instance-sp-option :sp-deferred-binding))
        )
       
       ;;=== sixth case we have a string, no deferring linking, but $IS-A prop
       ((and (stringp value)(eql sp-id '$IS-A))
        ;; we cannot filter on the successor class of $IS-A since it is $ENT
        ;; for prototyping successor may be any object,
        ;; Again, we assume a single string for the $IS-A reference
        (setq suc-list (append suc-list (access value)))
        )
       
       ;;=== seventh case we have a string or a MOSS query but not deferred linking
       ((and (parse-user-query value)
             (setq access-list (access value)))
        ;(format t "~%; %make-instance-sp-option / class-id ~S, access-list: ~S" 
        ;  class-id access-list)
        ;; in fact we need not remove bad successors, %validate-sp will do it
        #|
        ;; filter out those successors that are not instances of one of the classes
        ;; keep instances of subclasses (subsumption)
        (setq access-list
              (remove nil 
                      (mapcar #'(lambda (xx)
                                  (if (some #'(lambda (yy) (%type? xx yy)) 
                                            class-id-list)
                                      xx))
                        access-list)))
|#
        ;(format t "~%; %make-instance-sp-option / access-list after cleaning: ~S" 
        ;  access-list)
        (setq suc-list (append suc-list (reverse access-list)))
        ;(format t "~%; %make-instance-sp-option / suc-list: ~S" suc-list)
        )
              
       ;;=== any other case is invalid
       (t (warn "something wrong with ~S for property ~S while creating an ~
                 instance of ~S. Wrong variable name? We skip the link." 
            value sp-id class-id)))
      ) ; end of dolist
    
    ;(format *debug-io* "~&+++ MOSS:%make-instance-sp-option suc-list: ~S" suc-list)
    
    ;; check constraints on the values that were put into the suc-list
    ;; deferred values will be processed later
    (multiple-value-bind (result error-list) 
        (%validate-sp sp-id suc-list :obj-id key) ; JPB060215
      (declare (ignore error-list))
      ;; here result contains the list of filtered successors
      ;; warning messages were printed by %validate-sp
      
      ;; link whatever is left of the successor list
      ;(dolist (suc-id (reverse result))
      (dolist (suc-id result) ; JPB1611 to keep neighbors in order (?)
        (%link key sp-id suc-id)))
    )
  :done)

#|
;; tests for possible successors
(defobject ("name" "Nb1")(:var _nb1))
*none*.10
(($TYPE (0 *NONE*)) ($ID (0 *NONE*.10)) ($T-NAME (0 "Nb1")))

(defobject ("name" "Nb2")(:is-a *none*.10)(:var _nb2))
*NONE*.11
|#
;;;---------------------------------------------------- %MAKE-INSTANCE-TP-OPTION
;;; test for replacing this function by add-values

(defUn %make-instance-tp-option (name key prop-name tp-id value-list prop-list
                                      &key export)
  "builds an attribute for an instance object.
Arguments:
   name: name (ref) of the instance class 
   key: id of the instance
   prop-name: name (ref) of the attribute
   tp-id: id of the attribute
   value-list: list of values for the attribute
   prop-list: list of attributes attached to the class
   export (key): if t we export the name
Return:
   1. internal modified list of the instance (eval key)
   2. list of error messages"
  
  ;; here property exists and we have to check if it belongs to the class
  ;; this should be changed to accept "O2-like exceptional properties"
  ;; However it is not sensible to put it here since we are using the
  ;; class as a generator. Exceptional properties will be added through
  ;; =add-tp and =add-sp method.
  (unless (member tp-id prop-list)
    (mformat 
        "while trying to define instance of class ~A, property ~A does not belong ~
         to class in package ~S with context ~S. We ignore it. Use =add to ~
         overcome this test." 
      name prop-name (package-name *package*) (symbol-value (intern "*CONTEXT*")))
    (return-from %make-instance-tp-option nil))
  
  ;; we call add-values, recovering error messages
  (multiple-value-bind (obj-l message-list)
      (add-values key tp-id value-list :export export :no-warning t)
    (when message-list
      ;; add header to the list of messages
      (push
       (format nil "Warning: while adding values to the instance ~S in package ~S ~
        and context ~S" 
         key (package-name *package*) (symbol-value (intern "*CONTEXT*")))
       message-list)
      (mformat "~{~%~A~}" message-list))
    ;; return modified instance
    obj-l)
  )
  
#|
(defconcept "French Department" (:att "name" (:entry))(:att "code"))
$E-FRENCH-DEPARTMENT
(defindividual "French Department"
      (:var _oise)
  ("name" "OISE") ("code" "60"))
$E-FRENCH-DEPARTMENT.1
(($TYPE (0 $E-FRENCH-DEPARTMENT)) ($ID (0 $E-FRENCH-DEPARTMENT.1))
 ($T-FRENCH-DEPARTMENT-NAME (0 "OISE")) ($T-FRENCH-DEPARTMENT-CODE (0 "60")))

(defconcept "person" (:att "name" (:entry)) (:att "first-name"))

(make-individual "person" '("name" "Doe"))
$E-PERSON.1
(($TYPE (0 $E-PERSON)) ($ID (0 $E-PERSON.1)) ($T-PERSON-NAME (0 "Doe")))

(%MAKE-INSTANCE-TP-OPTION 
   "person" '$E-PERSON.1 "name" '$T-PERSON-FIRST-NAME nil
 '($T-PERSON-NAME $T-PERSON-FIRST-NAME ))
"Warning: while adding values to the instance $E-PERSON.1 in package \"MOSS\" and context 0"
"No value to add to $E-PERSON.1 for property $T-PERSON-FIRST-NAME in package \"MOSS\" and context 0"
(($TYPE (0 $E-PERSON)) ($ID (0 $E-PERSON.1)) ($T-PERSON-NAME (0 "Doe"))
 ($T-PERSON-FIRST-NAME (3 "John")))

(with-context 3
  (%MAKE-INSTANCE-TP-OPTION 
   "person" '$E-PERSON.1 "name" '$T-PERSON-FIRST-NAME '("John")
   '($T-PERSON-NAME $T-PERSON-FIRST-NAME )))
(($TYPE (0 $E-PERSON)) ($ID (0 $E-PERSON.1)) ($T-PERSON-NAME (0 "Doe"))
 ($T-PERSON-FIRST-NAME (3 "John")))
|#
;;;------------------------------------------------------ %MAKE-MULTIPLE-INSTANCE

(defun %make-multiple-instance (class-ref-list option-list &optional no-warning)
  "the instance is an instance of several classes. We must create an id for each ~
   class and record each property/values according to the class where the property ~
   is defined.
Arguments:
   class-list: list of class ids
   option-list: list of options
Return:
   the id corresponding to the first class."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (class-ref (car class-ref-list))
         id rest-of-option-list)
    ;; filter properties belonging to the first class
    (multiple-value-setq (option-list rest-of-option-list)
      (%make-multiple-instance-filter-prop class-ref option-list))
    ;; create first instance
    (setq id (apply #'%make-instance class-ref option-list))
    ;; for each remaining class
    (dolist (next-class-ref (cdr class-ref-list))
      ;; create a new id
      (multiple-value-bind (next-id class-id) (%make-ref-object next-class-ref id)
          ;; add reference to object
          (set id (%%add-value id '$ID next-id context))
          (set id (%%add-value id '$TYPE class-id context))
      ;; filter out properties belonging to that class
      (multiple-value-setq (option-list rest-of-option-list)
        (%make-multiple-instance-filter-prop next-class-ref rest-of-option-list))
      ;; add each property to the instance beiong built
      (dolist (pair option-list)
        ;; last arg is a no-warning argument to cancel warning messages
        (%%make-instance-add-property id class-id (car pair) (cdr pair) no-warning))
      ))
    ;; if anything left in option list must add id as an uncharted prop
    (dolist (pair rest-of-option-list)
      (%%make-instance-add-property id class-ref (car pair) (cdr pair) no-warning))
    ;; return the instance id
  id))

#|
(defconcept "C1" (:att "C1-att1")(:att "C1-att2")(:rel "C1-rel2" (:to "C1")))
(defconcept "C2" (:att "C2-att1")(:rel "C2-rel1" (:to "C1"))(:rel "C2-rel2" (:to "C2")))
(defconcept "C3" (:is-a "C1") (:att "C3-att1")(:rel "C3-rel1" (:to "C1")))
(moss::%make-multiple-instance
 '("C1" "C2")
 '(("C1-att1" "v1-a1") ("C2-att1" "v2-att1")(:var _ind12)))
(($TYPE (0 $E-C1 $E-C2)) ($ID (0 $E-C1.1 $E-C2.1)) ($T-C1-C1-ATT1 (0 "v1-a1"))
 ($T-C2-C2-ATT1 (0 "v2-att1")))

 _ind12
$E-C1.1

$E-C2.1
(($REF (0 $E-C1.1)))
;;- this is a reference to the unique representation of the object. This way the
;; object can be considered an instance of $E-C2
|#
;;;----------------------------------------- %MAKE-MULTIPLE-INSTANCE-FILTER-PROP

(defun %make-multiple-instance-filter-prop (class-ref option-list)
  "extracts from option-list the props corresponding to class-ref and returns ~
   what is left of option-list as a second value.
Arguments:
   class-ref: reference of a class
   option-list: list of pairs (prop-ref values)
Return:
   1st value: list of pairs associated with the class
   2nd value: rest of option-list"
  (let ((class-id (%%get-id class-ref :class))
        result)
    ;; get the list of properties attached to the class
    ;(setq prop-list (%%get-all-class-properties class-id))
    (setq result 
          (remove-if-not  
           #'(lambda (xx) (%%get-id xx :property :class-ref class-id))
           option-list :key #'car))
    (values
     result
     (set-difference option-list result 
                     :test #'(lambda (xx yy)
                               (if (and (stringp xx)(stringp yy))
                                   (string-equal xx yy)
                                 (equal xx yy)))
                     :key #'car))
    ))

#|
(defconcept "C1" (:att "C1-att1")(:att "C1-att2")(:rel "C1-rel2" (:to "C1")))
(defconcept "C2" (:att "C2-att1")(:rel "C2-rel1" (:to "C1"))(:rel "C2-rel2" (:to "C2")))
(defconcept "C3" (:is-a "C1") (:att "C3-att1")(:rel "C3-rel1" (:to "C1")))
(setq option-list '(("C1-att1" "v1-a1") ("C1-rel1" "v1-r1")
                    ("C2-att1" "v2-a1") ("C2-rel1" "v2-r1")
                    ("C3-att1" "v3-a1") ("C3-rel1" "v3-r1")))

(moss::%make-multiple-instance-filter-prop "C1" option-list)
(("C1-att1" "v1-a1"))
(("C3-rel1" "v3-r1") ("C3-att1" "v3-a1") ("C2-rel1" "v2-r1") ("C2-att1" "v2-a1") 
 ("C1-rel1" "v1-r1"))
;;- C1-rel1 is not a property defined at the class level, which is why it is not
;; returned

(moss::%make-multiple-instance-filter-prop "C2" option-list)
(("C2-att1" "v2-a1") ("C2-rel1" "v2-r1"))
(("C3-rel1" "v3-r1") ("C3-att1" "v3-a1") ("C1-rel1" "v1-r1") ("C1-att1" "v1-a1"))

(moss::%make-multiple-instance-filter-prop '$e-c3 option-list)
(("C1-att1" "v1-a1") ("C3-att1" "v3-a1") ("C3-rel1" "v3-r1"))
(("C2-rel1" "v2-r1") ("C2-att1" "v2-a1") ("C1-rel1" "v1-r1"))
|#
;;;=============================================================================
;;;
;;;                         ORPHANS (Classless Objects) 
;;;
;;;=============================================================================

;;;---------------------------------------------------------------- %MAKE-OBJECT
;;; when allowing forward references we create objects with the instances and
;;; link them afterwards

(defUn %make-object (&rest option-list)
  "function that creates orphans (classless objects, instances of the NULL-CLASS).
   syntax of option-list is
   	(<tp-name> value[list])
   	(<sp-name> <internal-reference>)
   	(:name <var-name>)
    Properties are checked but can be any property in system 
    Values for terminal properties are checked (=xi) and also for entry-point
    generation
    Internal references to MOSS entities must correspond to existing objects
    and of the proper type. If not, then an error message is sent.
    Example
   	(defobject 
   		(HAS-NAME \"Barthes\")
   		(HAS-BROTHER _p1))
    will work if _p1 is the internal reference to an already defined person
    or specialization of person.
    Returns the internal reference to the new object
    Once created objects must be modified directly, using access methods
    Integrity constraints are checked to see whether the properties are
    correctly filled."
  ;; orphans are instances or the NULL-CLASS (id = *none*)
  (apply #'%make-instance "MOSS-NULL-CLASS" (cons '(:no-warning) option-list)))

#|
(defconcept "Car" (:att "brand" (:entry)(:max 1)))
$E-CAR

(defindividual car (brand "Mercedes"))
$E-CAR.1

Mercedes
(($TYPE (0 $EP)) ($T-BRAND.OF (0 $E-CAR.1)) ($EPLS.OF (0 $SYS.1)))

$E-CAR.1
(($TYPE (0 $E-CAR)) ($ID (0 $E-CAR.1)) ($T-CAR-BRAND (0 "Mercedes")))
|#
;;;=============================================================================
;;;
;;;                               ONTOLOGY 
;;;
;;;=============================================================================

;;;-------------------------------------------------------------- %MAKE-ONTOLOGY

(defUn %make-ontology (&rest option-list)
  "creates a new ontology, package and stub if needed. If it already exists, does ~
   nothing.
Arguments:
   option-list:
    :language one of the legal languages (:fr, :en,...) or :all meaning that we 
                have multiple languages (default is English)
    :package specifies a package (default: *package*)
    :title a string title of the ontology (default: \"Anonymous Ontology\")
    :version a string, e.g. \"2.0\" (default: \"0.0\")
    :keep-package means that we do not set *package* to ontology package
Return:
   the id of the moss-system instance in the ontology package."
  (declare (special *application-package* *language*))
  
  (let* ((language-option (assoc :language option-list))
         (package (or (cadr (assoc :package option-list)) *package*))
         (title (or (cadr (assoc :title option-list)) "Anonym Ontology"))
         ;(version (or (cadr (assoc :version option-list)) "0.0"))
        )
    (when language-option
      (setq *language* (or (cadr language-option) :en)))
    
    (setq *application-package* 
          (or (find-package package)
              ;; omas is there although for MOSS is does not matter
              (make-package package :use (list :moss :omas :cl))))
    
    ;; create stub for the ontology, unless already there
    (unless (boundp (intern "*MOSS-SYSTEM*" *application-package*))
      (format t "~%; creating an ontology stub in package ~S"
        (package-name *application-package*))
      (%create-new-package-environment *application-package* title))
    
    ;; set global package to application package
    (unless (assoc :keep-package option-list)
      (setq *package* *application-package*))
    
    ;; return the id of the ontology stub
    (symbol-value (intern "*MOSS-SYSTEM*" *application-package*))))

#|
? (%make-ontology 
    '(:title "Family Test Ontology")
    '(:language :all)
    '(:package :family-test)
    )
FAMILY-TEST::$E-MOSS-SYSTEM.1

? FAMILY-TEST::$E-MOSS-SYSTEM.1
(($TYPE (0 FAMILY-TEST::$E-ADDRESS-SYSTEM)) ($ID (0 FAMILY-TEST::$E-MOSS-SYSTEM.1))
 ($SNAM (0 (:EN "Family Test Ontology")))
 ($PRFX (0 FAMILY-TEST::|Family Test Ontology|))
 ($EPLS
  (0 FAMILY-TEST::|Family Test Ontology| FAMILY-TEST::FAMILY-TEST-ONTOLOGY-SYSTEM
   MOSS-SYSTEM METHOD COUNTER NULL-CLASS UNIVERSAL-CLASS))
 ($CRET (0 "J-P.A.BARTHES")) ($DTCT (0 3474702265)) ($VERT (0 "8.0")) ($XNB (0 0))
 ($ENLS
  (0 FAMILY-TEST::$E-FAMILY-TEST-ONTOLOGY-SYSTEM FAMILY-TEST::$E-FN
   FAMILY-TEST::$E-COUNTER FAMILY-TEST::*NONE* FAMILY-TEST::*ANY*))
 ($SVL (0 FAMILY-TEST::*MOSS-SYSTEM*)))
|#
#|
;;;=========================== cloning objects =================================

;;;---------------------------------------------------------------------- %CLONE
;;; should not be here be should be %make-clone like %make-instance

(defun %make-clone (obj-id &optional context)
  "clone an object.
Arguments:
   obj-id: identifier of the object to be cloned
Returns:
   identifier of the cloned object"
  (let ((property-list (cons '$TYPE (%%has-properties obj-id context)))
        (new-object-id
           (if (%orphan? obj-id context)
             (%create-basic-orphan context)
             (%create-basic-new-object (car (%get-value obj-id '$TYPE context))
                                       context)
             ))
        prop-id value-list entry)
    ;; Initialize new key to nil, wiping out the $TYPE field
    (set new-object-id nil)
    ;; Now look at each prop in turn
    (while property-list
      (setq prop-id (pop property-list))
      ;; very low level, copy value into new object
      (setq value-list (%%has-value obj-id prop-id context))
      (when value-list 
        (when (%is-attribute? prop-id context)
          (%%set-value-list new-object-id value-list prop-id context)
          (when 
            (get-method prop-id '=make-entry context)
            ;; make new entry-point for corresponding property
            (while value-list
              (setq entry (=> prop-id '=make-entry (pop value-list)))
              (%make-ep 
               entry
               prop-id
               new-object-id
               context
               )
              ;; when LOB is active then we must record entry-points
              ))))
        (when (%is-relation? prop-id context)
          (=> new-object-id '=add-sp-id prop-id value-list))
        )
    ;; cloned classes are recorded at system level
    ;; ***** don't know what to do when LOB is active with classes
    (when (%model? new-object-id context)
      (send '$ENSL '=link *moss-system* new-object-id)
      )
    ;; return new object-id
    new-object-id
    ))
|#

(format t "~%;*** MOSS v~A - Definitions loaded ***" *moss-version-number*) 

;;; :EOF