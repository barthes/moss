﻿;;;-*- Mode: Lisp; Package: "SOL-OWL" -*-
;;;=================================================================================
;;;20/01/30	
;;;		S O L 2 O W L - (File sol2owl.LiSP)
;;;	
;;;=================================================================================
;;;This file contains a parser for the simplified OWL language that translates SOL
;;;expressions into OWL FULL expressions.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#


#|
List of macros
  +result
  result+
  cformat
  mark-section
  noformat
  tkformat
  trformat
  twarn
  vformat
  with-catching-error
  with-warning
  
List of functions
  ALIST?
  COMPILE-SOL
  COMPILE-SOL-BODY
  LTAB
  RTAB
  RP
  ERRORS
  MARK-CENTER
  TT
  TTF
  
  CHECK-TYPE
  CHECK-SYNTAX
  COMPUTE-TRANSITIVE-CLOSURE
  EXTRACT-DOCUMENTATION-FROM-FRAGMENTS
  EXTRACT-ATTRIBUTE-FRAGMENTS
  EXTRACT-RELATION-FRAGMENTS
  EXTRACT-NAMES
  GET-ALL-NAME
  GET-ATTRIBUTE-DOMAIN
  GET-ATTRIBUTE-ID-FOR-CLASS
  GET-ATTRIBUTE-ONE-OF
  GET-ATTRIBUTE-TYPE
  GET-ID-STRING
  GET-RELATION-DOMAIN
  GET-RELATION-ID-FOR-CLASS
  GET-XSD-ATTRIBUTE-TYPE
  IS-INSTANCE-OF?
  IS-MLN?
  MAKE-ATTRIBUTE
  MAKE-ATTRIBUTE-ID
  MAKE-CHAPTER
  MAKE-CLASS
  MAKE-CONCEPT
  MAKE-CONCEPT-ID
  MAKE-CONCEPT-ONE-OF
  MAKE-CONCEPT-ONE-OF-FROM-INDIVIDUALS
  MAKE-CONCEPT-STRING
  MAKE-STANDARD-NAME
  MAKE-DOC
  MAKE-DOMAINS
  MAKE-HEADER
  MAKE-INDEX-STRING
  MAKE-INDICES
  MAKE-INDIVIDUAL
  MAKE-INDIVIDUAL-ID
  MAKE-INDIVIDUAL-STRING
  MAKE-INVERSE-NAMES
  MAKE-INVERSE-RELATION-ID
  MAKE-LABELS
  MAKE-ONTOLOGY
  MAKE-RANGES
  MAKE-RELATION
  MAKE-RELATION-ID
  MAKE-SECTION
  MAKE-SINGLE-STRING
  MAKE-SUBCLASS
  MAKE-TRAILER
  MAKE-TYPE
  MAKE-VALUE-STRING
  MAKE-VIRTUAL-ATTRIBUTE
  MAKE-VIRTUAL-CONCEPT
  MAKE-VIRTUAL-RELATION
  ONE-OF-ALL-INSTANCES?
  PROCESS-ATTRIBUTES
  PROCESS-ATTRIBUTES-CLASS-RESTRICTIONS
  PROCESS-ATTRIBUTES-GLOBAL-DEFINITION
  PROCESS-ATTRIBUTES-ONE-OF
  PROCESS-ATTRIBUTES-ONE-OF-LIST
  PROCESS-ATTRIBUTES-ONE-OF-LIST-REST
  PROCESS-INDEXES
  PROCESS-INDIVIDUALS
  PROCESS-INDIVIDUALS-ATT
  PROCESS-INDIVIDUALS-REL
  PROCESS-RELATIONS
  PROCESS-RELATIONS-LOCAL-OPTIONS
  PROCESS-RELATIONS-ONE-OF
  PROCESS-VIRTUAL-ATTRIBUTES
  PROCESS-VIRTUAL-COMPOSED-ATTRIBUTES
  PROCESS-VIRTUAL-CONCEPTS
  PROCESS-VIRTUAL-RELATIONS
  READ-NEXT-VALID-LINE
  RESET
  SAME-NAME
  SET-RULE-NAME-SPACE
  TRIM-FILE
  
  INDEX
  INDEX-ADD
  INDEX-CLEAR
  INDEX-REMOVE
  %INDEX-SET
  INDEX-GET
|#

;;;;;;============================================================================
;;;
;;;This file contains a parser for the simplified OWL language that translates SOL
;;;expressions into OWL FULL expressions.
;;;The grammar is the following:
;;;
;;;<concept> ::= (defconcept <concept-id> {(:is-a <concept-ref>)}
;;;                          {<attribute>}* {<relation>}* {<documentation>}
;;;                          ))
;;;<attribute> ::= (:att <attribute-id> {(:type XSL-TYPE)}
;;;                                    {(:unique) | {(:min <nn>)} {(:max <nn>)}}
;;;                                    {(:one-of {<value>}+)}
;;;                                    {<documentation>}
;;;                                    )
;;;<relation> ::= (:rel <relation-id>  (:to <concept-ref>)
;;;                                    {(:unique) | {(:min <nn>)} {(:max <nn>)}
;;;                                    {(:one-of {<instance-ref>}+)})
;;;                                    {<documentation>}
;;;                                    )
;;;<concept-id> ::= ({(:name} <multilingual-string>)
;;;<concept-ref> ::= STRING
;;;<attribute-id> ::= ({(:name} <multilingual-string>)
;;;<instance-ref> ::= STRING
;;;<relation-id> ::= ({(:name} <multilingual-string>)
;;;<documentation> ::= (:doc <multilingual-string>)
;;;<multilingual-string> ::= ({(<language-marker> STRING*)}*)
;;;<language-marker> ::= :en | :fr | :it | :pl
;;;<value> ::= SIMPLE-TYPE-VALUE (that do not depend on the language)
;;;
;;;
;;;Processing a definition file xxx.sol is done as follows:
;;;1. clear all ancillary lists
;;;1.1 make first pass, collecting the names of all classes (in the definitions)
;;;    build the index allowing access to the classes by any name.
;;;2. read all definitions
;;;   2.1 For each concept produce the lines to be printed
;;;   2.2 Make a hash-table with index entries *index*
;;;   2.3 Save the attribute and relation definitions
;;;3. Once all definitions have been read
;;;   3.1 check for the presence of superclasses
;;;   If some are missing, complain
;;;   3.2 process attributes, grouping domains?
;;;       produce local restrictions using about option
;;;   3.3 process relations
;;;       check for the presence of domain and range
;;;       check for the existence of range concepts, when missing complain
;;;       group domain concepts
;;;       group range concepts into collections
;;;       if domain and range are both multiply present, complain (creates phantom 
;;;          relations)
;;;       produce local restrictions using the about option
;;;   3.4 process individuals
;;;4. print the result into the output file xxx.owl
;;;
;;;Inverse relations
;;;=================
;;;Can be done at the level of the :rel option.
;;;Ex:
;;;    (:rel (:fr "région") (:to "région"))
;;;produces 
;;;    ("ZFrenchDepartment" (:FR "région") (:TO "région"))
;;;  in the *relation-list*
;;;One could add
;;;    ("ZRegion" (:fr "région de") (:to "ZFrenchDepartment"))
;;;To do so requires to recover the ID of the "région" class, and to cook up the
;;;"région de" name.
;;;Difficulties
;;;============
;;;1. Relation has multiple names
;;;     (:rel (:name :en "region" :fr "région; province") (:to "région"))
;;;  We need to cook up proper names
;;;     ("ZRegion" (:en "region of" :fr "région de; province de") 
;;;                (:to "ZFrenchDepartment")) 
;;;2. cardinality constraints
;;;    (rel (:fr "région") (:to "région")(:unique))
;;;  We simply ignore them.
;;;3. one-of option
;;;    (:rel  (:en "service" :fr "service")
;;;           (:one-of
;;;              (:en "authorization")
;;;              (:en "control")
;;;              (:en "production")
;;;              (:en "certification")))
;;;  We insert the name of the synthetic class, e.g.
;;;    ("ZPublicServiceType" (:en "service of" :fr "service de") (:to "ZPublicService"))
;;;4. multiple ranges
;;;   (:rel (:en "origin" :fr "origine") 
;;;        (:to "Authority" "municipality" "organization"))
;;;  We insert one definition for each range.
;;;5. documentation option
;;;   Ignored.
;;;Conclusion
;;;==========
;;;The problem seems fairly simple, we need a function for making inverse names, 
;;;and one to make an inverse ID.
;;;However, the question of linking inverse properties remains. Do we make an extension
;;;of OWL, introducing the "inverse" property, a symmetric property.
;;;
;;;
;;;Index
;;;=====
;;;The index is a table used by the compiler to store temporary information, e.g. 
;;;all index entries. The key is a normed string, e.g. "MenuDuJour"
;;;the corresponding value is an alternated list like:
;;;  (:att "ZhasMenuOfTheDay" :class "ZMenuOfTheDay")

#|
History
-------
2005
 0624 Creation
 0724 V 0.1 alpha completed
      Missing Generalized labels and autolatic inverse properties
 0901 Adding defontology and make-ontology
      There should be a check for same object in range (and domain?) when using
      different language tags (e.g. "pays" and "country" are considered separate
      classes)
 0902 doing some clean up
      adding check-sol-type
 0907 adding inverse relations
 0921 adding Index class definition to the header
 0922 replacing $ with Z (Swoop does not allow $ characters in OWL ontologies)
 0924 correcting /< -> </ and owl:inverse -> owl:inverseOf
      changing the syntax of inverse properties
 0930 introducing defontology and changing Z prefix to Z-
 1009 version v6 
      grouping indexes to produce OWL compatible output
 1013 correcting some errors, comments on imports
 1015 small case for xmlns tags
 1018 Commenting imports inside the ontology tag
 1027 adding get-xsd-attribute-type to check attribute type
 1029 adding :doc option for att and rel, correcting get-relation-range to avoid
      multiple return of the same class-ID, :is-a can be multiple valued
 1115 adding UTF8 option in the external format
2006
 0404 setting the package to SOL-OWL so that we can build a single combined
      execution file
 0605 version 1.5
      Many changes were done to adapt to CLisp
      The SOL input file is compiled using the corresponding macros
      vformat is used for outputting data to the TK window
 0601 replacing *tk* test with :tk to be added t *features*
 0613 changing index-add to avoid duplicating same values
 0624 Bad ontology when :one-of for a relation refers to existing instances
      E.g. EU contries for defining a European Citizen
 0727 ***** get-relation-one-of should be improved
 0925 trying to remove o-with-acute-accent from the Polish strings causing
      hangup in tkl #\LATIN_SMALL_LETTER_O_WITH_ACUTE :does not work
 0929 introducing fake restriction to add "indexing" annotation
2007
 0210 removing the ltk interface
 1022 adding sparql for rules
 1229 adding make-section to separate sections in the OWL output
 1231 modifying make-virtual-class and adding process-virtual-classes
2008
 0106 improving the virtual property mechanism
 0109 cleaning some loose ends
 0227 Adding make-standard-name
2018
 0128 Adapting the file to insert into the MOSS environment
 0228 Removing inverse properties, since MOSS inverse links have not the same
      semantics as OWL inverse properties
|#

(in-package :SOL-OWL)

;;;(eval-when (:compile-toplevel :load-toplevel :execute)
;;;  (import 
;;;   '(util.string:string+ ; replace (format nil ...)
;;;     moss::equal+
;;;     )))

;;;---------------------- export main functions -------------------------------

;;;(eval-when (:compile-toplevel :load-toplevel :execute)
;;;  (export '(defconcept defontology defindividual
;;;             definstmethod defownmethod defuniversalmethod
;;;             defvirtualconcept defvirtualattribute defvirtualrelation
;;;             defruleheader defrule
;;;             defchapter defsection
;;;             make-ontology make-concept make-individual)))

;;;--------------------- macros for compiling the SOL file --------------------
;;; Those macros are used when compiling the input sol file and are defined
;;; for producing an OWL output

(defMacro defchapter (&rest mls)
  (declare (ignore mls))
  nil)

(defMacro defconcept (concept-name &rest option-list)
  `(apply #'make-concept ',concept-name ',option-list))

(defMacro defindividual (individual-name &rest option-list)
  `(apply #'make-individual ',individual-name ',option-list))

(defMacro defontology (&rest option-list)
  `(apply #'make-ontology ',option-list))

(defMacro defsection (&rest mls)
  (declare (ignore mls))
  nil)

;;;---------- macros for skipping methods

(defmacro definstmethod (&rest ll)
  (declare (ignore ll))
  nil)

(defmacro defownmethod (&rest ll)
  (declare (ignore ll))
  nil)

(defmacro defuniversalmethod (&rest ll)
  (declare (ignore ll))
  nil)

;;;---------- macros for processing virtual concepts and rules

(defMacro defruleheader (&rest option-list)
  `(apply #'make-rule-header ',option-list))

(defMacro defvirtualconcept (concept-name &rest option-list)
  `(apply #'make-virtual-concept ',concept-name ',option-list))

(defMacro defrule (&rest option-list)
  `(apply #'record-rule ',option-list))

(defMacro defvirtualattribute (property-name &rest option-list)
  `(apply #'make-virtual-attribute ',property-name ',option-list))

(defMacro defvirtualrelation (property-name &rest option-list)
  `(apply #'make-virtual-relation ',property-name ',option-list))

#|
(defMacro defvirtualproperty (property-name &rest option-list)
  `(apply #'make-virtual-property ',property-name ',option-list))
|#

;;;----------------------------------------------------------------------------
;;; +result appends a list in front of the result list
;;; result+ appends a list at the end of the result list

(defMacro +result (ll)
  `(setq result (append ,ll result)))
(defMacro result+ (ll)
  `(setq result (append  result ,ll)))

(defMacro terror (cstring &rest args)
  `(throw :error (format nil ,cstring ,@args)))

;;;-------------------------------------------------------------------- cformat
;;; simplify internal code to take care of indentations

(defMacro cformat (control-string &rest args)
  `(push (indent *left-margin* (format nil ,control-string ,@args))
         result))

(defMacro mark-section (text nn)
  `(cformat "<!-- ~A -->~2%" (make-center ,text (max 0 (- ,nn 8)))
            ))

#|
? (mark-section "test" *separation-width*)
Expands as
(setq RESULT
      (cons (INDENT *LEFT-MARGIN*
                    (format NIL
                            "<!-- ~A --!>

"
                            (MAKE-CENTER "test" (max 0 (- *SEPARATION-WIDTH* 8)))))
            RESULT))
("  <!-- +++++++++++++++++++++++++++++++++++++ test +++++++++++++++++++++++++++++++++++++ --!>

|#

(defMacro noformat (&rest ll)
  "sink: does not print anything. used to check UNICODE problems"
  (declare (ignore ll))
  nil)

(defMacro tkformat (cstring &rest args)
  "If Tk window is used, prints to Tk window, otherwise use standard output."
  ;; if using Tk, call the proper printing function
  #+tk`(ltk::append-text cl-user::*trace-window* (format nil ,cstring ,@args))
  ;; otherwise use standard output
  #-tk`(format *log-file* ,cstring ,@args)
  )

(defMacro trformat (fstring &rest args)
  "debugging macro"
  `(tkformat ,(concatenate 'string "~&;********** " fstring) ,@args))
 
(defMacro twarn (cstring &rest args)
    "inserts a message into the *error-message-list* making sure to return nil"
  `(progn (push (format nil ,cstring ,@args) *error-message-list*) nil))
 
(defMacro vformat (cstring &rest args)
    "prints something if verbose flag on only."
  `(if *cverbose* (tkformat ,cstring ,@args)))
 
 ;(terror "error in ~A and N S" aaa sss)
 
 ;;; the following macro allows to call a set of exprs and catch any error in them
 ;;; the result if not in error should not be a string.
 ;;; The result of the macro is a list containing a string, whether a value or an error
 
(defMacro with-catching-error (error-context &rest body)
    "the result of body should not be a string."
  `(let (message)
     (setq message (catch :error ,@body))
     (if (stringp message)
       (list (format nil "error when ~A: ~A" ,error-context message))
       nil)))
 
 ;;; if we throw to :error in body, catches the resulting sring and inserts it into
 ;;; the error list to be printed later. Does not produce any other output
 ;;; if no error then OWL output is produced if *errors-only* is not set
 
(defMacro with-warning (arg-list &rest body)
  `(let (message)
     (setq message (catch :error ,@body :done))
     (if (stringp message)
       (twarn "~&;***** Error in:~S; ~A" ,(car arg-list) message)
       ;; otherwise print result
       (unless *errors-only* (rp result)))))

;;;================================================================================
;;; Global list to save definitions while processing data
;;;---------------------------- Globals ---------------------------------------

(defParameter *directory* *load-pathname*)

;(defParameter *xmlschema* "http://www.w3.org/2001:XMLSchema#")
;(defParameter *xmlschema-string* "http://www.w3.org/2001:XMLSchema#string")

; inherited from :sol
;(defParameter *ontology-title* "ONTOLOGY" "default ontology title")

;;; by default print into the listener
;(defParameter *log-file* t "contains the path to the error log file")

(defparameter *language* :EN "default language is English")
;(defParameter *language-tags* '(:en :fr :it :pl :*))
;(defParameter *language-index-property* 
;  '((:en . "isEnIndexOf") (:fr . "isFrIndexOf") (:it . "isItIndexOf")
;    (:pl . "isPlIndexOf")))

;;; the following types are xsd types built in in OWL
; (defParameter *attribute-types*
  ; '((:string . "string") (:boolean . "boolean") (:decimal . "decimal") 
    ; (:float . "float") (:double . "double") (:date-time . "dateTime")
    ; (:time . "time") (:date . "date") (:g-year-month . "gYearMonth")
    ; (:g-year . "gYear") (:g-month-day . "gMonthDay") (:g-day . "gDay")
    ; (:g-month . "gMonth") (:hex-Binary . "hexBinary") 
    ; (:base-64-binary . "base64Binary")
    ; (:any-uri . "anyURI") (:normalized-string . "normalizedString") 
    ; (:token . "token")
    ; (:language . "language") (:nm-token . "NMTOKEN") (:name . "Name")
    ; (:nc-name . "NCName") (:integer . "integer")  
    ; (:non-positive-integer . "nonPositiveInteger") 
    ; (:negative-integer . "negativeInteger") (:long . "long") (:int . "int")
    ; (:short . "short") (:byte . "byte") 
    ; (:non-negative-integer . "nonNegativeInteger")
    ; (:unsigned-long . "unsignedLong") (:unsigned-int . "unsignedInt")
    ; (:unsigned-short . "unsignedShort") (:unsigned-byte . "unsignedByte")
    ; (:positive-integer . "positiveInteger")))

; (defParameter *attribute-list* ())
; (defParameter *index-list* ()) ; JPB051009
; (defParameter *relation-list* ())
; (defParameter *concept-list* ())
; (defParameter *individual-list* ())
; (defParameter *super-class-list* ())
; (defParameter *virtual-attribute-list* ()) ; JPB071231
; (defParameter *virtual-composed-attribute-list* ()) ; JPB080105
; (defParameter *virtual-concept-list* ()) ; JPB071231
; (defParameter *virtual-property-list* ()) ; JPB080105
; (defParameter *virtual-relation-list* ()) ; JPB071231
; (defParameter *rule-list* ()) ; JPB080114
; (defparameter *rule-format* :jena "default rule format")
; (defParameter *error-message-list* ())

;(defParameter *index* (make-hash-table :test #'equal) "internal compiler table")

;(defParameter *compiler-pass* 1 "compiler has 2 passes")
;(defParameter *left-margin* 0)
;(defParameter *output* t "default output is listener")
;(defparameter *rule-output* t "default output for rules") ; defined in :sol
;(defParameter *indent-amount* 2)
;(defParameter *errors-only* nil)
;(defParameter *cverbose* nil)

;(defParameter *translation-table* '((#\' #\space)))

;(defParameter *separation-width* 90)

;;; globals for the first syntax check compiler pass
;(defParameter *line* nil)
;(defParameter *line-number* 0 "counting lines being read")
;(defParameter *text*  "")
; (defParameter *breaks* '("(defontology" "(defchapter" "(defsection"
                         ; "(defconcept" "(defindividual" ":EOF"
			 ; "(defruleheader" "(defvirtualconcept"
			 ; "(defvirtualrelation" "(defrule"))
; (defParameter *syntax-errors* nil)

;;; globals for rules

; (defParameter *r-output* t "channel to rule file, default is listener")
; (defParameter *rule-error-list* nil "list of error strings")
; (defParameter *rule-file* nil)
; (defParameter *rule-builtin-operator-list*
  ; '((:jena (:gt . "greaterThan")(:lt . "lessThan")(:ge . "ge")(:le . "le")
           ; (:equal . "equal")(:not-equal . "notEqual")
           ; (:all . "all"))
    ; (:sparql (:gt . ">")(:lt . "<")(:ge . ">=")(:le . "<=")
             ; (:equal . "=")(:not-equal . "!="))))
; (defParameter *rule-prefix-ref* nil)
; (defParameter *user-defined-rule-operators* ())


;;;============================= Service Functions ================================

;;;--------------------------------------------------------------------- ALIST?

(defun alist? (expr)
  "test whether expr is an a-list.
Arguments:
   expr: any lisp expr
Return:
   T if a-list, nil otherwise."
  (or (null expr)  ; nil is an alist
      (and (listp expr) (consp (car expr)) (alist? (cdr expr)))))

#|
? (alist? nil)
T
? (alist? '(1 2 3))
NIL
? (alist? '((1 . A)(2 B C D)))
T
|#    
;;;---------------------------------------------------------------- COMPILE-SOL

(defun compile-sol (infile &key outfile rule-outfile errors-only verbose 
                           rule-output (rule-format :Jena) (language :EN))
  "compile a file containing SOL definition into an OWL file
Arguments:
   infile: SOL file pathname
   outfile (key): OWL file pathname, if nil infile is used, with OWL extension
   errors-only (key): if t does not generate output
   verbose (key): if t prints what it is doing
   rule-format: default :Jena
   language (key): language of the output ontology
Return:
   :EOC"
  (declare (special *r-output* *log-file* *language* *output* *rule-output*
                    *rule-format*)
           (ignore outfile))
  (setq *language* language)
  (setq *rule-output* rule-outfile)
  (setq *rule-format* rule-format)
  (format t "~%; compile-sol /*rule-format*: ~S" so::*rule-format*)
  
  (let* ((sol-file infile)
         (tmp-file  ; ancillary file
          (make-pathname :defaults sol-file :type "tmp"))
         (fas-file ; ancillary file
          (make-pathname :defaults sol-file :type "fasl"))
         (log-file ; output of the OWL compiler
          (make-pathname :defaults sol-file :type "log"))
         (owl-file 
          (make-pathname :defaults sol-file :type "owl"))
         (rule-file rule-output)
         )
    ;; MCL has some problems when files already exist
    (print 
     (catch 
      :error
      ;; create a working file without the leading UTF-8 mark
      (trim-file sol-file tmp-file)
      
      ;(break "compile-sol check tmp-file")
      ;; open the output log file
      (with-open-file (*log-file* (or log-file t) :direction :output 
                                  :if-exists :supersede
                                  :external-format :utf-8)
        (setq *log-file* t) ; send trace to listener
        ;(print `(+++ log-file/ ,log-file *log-file*/ ,*log-file*))
        ;; then check parents and quote syntax errors, print statistics
        ;; tkformat prints into *log-file*
        (tkformat "~%Checking Ontology Syntax~&~
                     ========================")
        (tkformat (check-syntax tmp-file))
        (tkformat "~%")
        ;; if errors, then quit
        (if *syntax-errors* (return-from compile-sol nil))
        
        ;; compile SOL file to expand macro definitions
        (multiple-value-bind (compiled-file warnings errors) 
            (compile-file tmp-file :external-format :utf8)
          (declare (ignore warnings))
          (format t "~%;--- errors: ~S" errors)
          (format t "~%;--- compiled-file: ~S" compiled-file)
          (when (or errors (null compiled-file))
            (tkformat "~&Some format errors in the SOL file... ~
                         ~%Recompiling with trace on")
            (compile-file sol-file :verbose t :external-format :utf8)
            (return-from compile-sol nil))
          
          ;; no compilation errors, we proceed using the compiled file
          (cond
           ;;== simply check syntax errors no OWL file no RULE file
           ((and errors-only (not rule-output))
            (let ((*output* nil)) ; does not output anything
              (compile-sol-body compiled-file errors-only verbose language)))
           
           ;;== no OWL output but output rule file
           ((and errors-only rule-output)
            (let ((*output* nil))
              (with-open-file (*r-output* (or rule-file t) :direction :output 
                                          :if-exists :supersede
                                          :external-format :utf-8)
                (compile-sol-body compiled-file errors-only verbose language))))
           
           ;;== output OWL file but not RULE file
           ((and (not errors-only) (not rule-output ))
            (format t "~%;--- ouputting OWL but no rule files")
            (format t "~%;--- verbose: ~S" verbose)
            (with-open-file (*output* (or owl-file t) :direction :output 
                                      :if-exists :supersede
                                      :external-format :utf-8)
              (compile-sol-body compiled-file errors-only verbose language)))
           
           ;;== output both OWL and RULE files
           ((and (not errors-only) rule-output )
            (with-open-file (*output* (or owl-file t) :direction :output 
                                      :if-exists :supersede
                                      :external-format :utf-8)
              (with-open-file (*r-output* (or rule-file t) :direction :output 
                                          :if-exists :supersede
                                          :external-format :utf-8)
                (compile-sol-body compiled-file errors-only verbose language)))))
          )
        ) ; end letf
      ;; clean up only in non error cases
      (sleep .5) ; must wait to clean up intermediate file...
      ; this only happens when the directory is visible
      (delete-file fas-file)
      (delete-file tmp-file)
      ;(sleep 5)
      :EOC))))

#|
;; for debugging purposes output is listener
(setq cl-user::*tk* nil)
;; compiling, no owl file output
(compile-sol "c:/SOL/tg-ontology15pc-utf8.sol" :verbose nil :errors-only t)
;; compiling, no OWL output, verbose
(compile-sol "c:/SOL/tg-ontology15pc-utf8.sol" :verbose t :errors-only t)
;;; compiling, OWL output, terse
(compile-sol "c:/SOL/tg-ontology15pc-utf8.sol")

;;; compile errors only, verbose
(compile-sol-body "c:/SOL/tg-ontology15pc-utf8.fas" t t)
;;; compile errors only, terse
(let ((*output* nil))
  (compile-sol-body "c:/SOL/tg-ontology15pc-utf8.fas" t nil))
|#
;;;----------------------------------------------------------- COMPILE-SOL-BODY

(defun compile-sol-body (sol-file errors-only verbose language)
  "main body of the compiler doing the job."
  (declare (special *cverbose* *compiler-pass* *left-margin* *indent-amount*
                    *separation-width* *attribute-list* *RELATION-list*
                    *individual-list* *virtual-attribute-list*
                    *virtual-composed-attribute-list* *virtual-relation-list*
                    *virtual-concept-list* *rule-list* *language*))
  (let ((*cverbose* verbose)
        (*language* language)
        result)
    
    (reset)
    (vformat "~%ONTOLOGY~&~
              ========~2&")
    
    ;(unless errors-only (make-header))
    ;(setq *cverbose* verbose)
    (setq *errors-only* errors-only)
    (vformat "~&Compiler Pass : 1~&~
              =================~2&")
    (load sol-file)
    
    ;(break "compile-sol-body 1")
    
    ;; second pass
    (unless errors-only 
      (mark-section "Classes" *separation-width*)
      (rp result)
      )
    (setq result nil)
    (sleep 1)
    
    (setq *compiler-pass* 2)
    (vformat "~2%Compiler Pass : 2~&~
              =================~2%")
    (load sol-file)
    (unless errors-only
      (mark-section "End Classes" *separation-width*)
      (rp result)
      )
    
    (break "compile-sol-body 2")
    
    ;(index) ; debug control
    ;; now process attributes
    ;(print `(+++ att ,*attribute-list*))
    ;; reset left margin
    (setq *left-margin* *indent-amount*)
    (vformat "~2%Processing Attributes~&~
              =====================~&~S~2&"
             *attribute-list*)
    (process-attributes)
    ;(print `(+++ REL ,*RELATION-list*))
    ;; reset left margin
    (setq *left-margin* *indent-amount*)
    (vformat "~2%Processing Relations~&~
              ====================~&~S~2&"
             *RELATION-list*)
    (process-relations)
    
    ;; third pass (for individuals)
    ;; reset left margin
    (setq *left-margin* *indent-amount*)
    (vformat "~2%Processing Individuals~&~
              ======================~&~S~2&"
             *individual-list*)
    (process-individuals)
    
    ;; now indexes
    (setq *left-margin* *indent-amount*)
    (vformat "~2%Processing Indexes~&~
                ==================~&~S~2&"
             *index-list*)
    (process-indexes)
    
    (unless errors-only 
      (setq result nil)
      (mark-section "End Ontology" *separation-width*)
      (rp result))
    ;; reset left margin
    (setq *left-margin* *indent-amount*)
    
    ;(index) ; debug control
    
    ;; and virtual attributes (2 kinds)
    (vformat "~2%Processing Virtual Attributes~&~
              =============================~&~S~2&"
             (append *virtual-attribute-list*
                     *virtual-composed-attribute-list*))
    (process-virtual-attributes)
    
    ;; and virtual relations
    (vformat "~2%Processing Virtual Relations~&~
              ============================~&~S~2&"
             *virtual-relation-list*)
    (process-virtual-relations)
    
    ;; print now virtual classes
    (vformat "~2%Processing Virtual Concepts~&~
              ===========================~&~S~2&"
             *virtual-concept-list*)
    (process-virtual-concepts)
    
    ;; print now additional rules
    (vformat "~2%Processing Rules~&~
              ================~&~S~2&"
             *rule-list*)
    (process-rules)
    
    ;; close RDF section
    (unless errors-only 
      (make-trailer))
    
    (tkformat "~2%Ontology Errors~&~
               ===============")
    (errors)
    
    (tkformat "~2%End~&~
               ===")
    :eoc))

;;;----------------------------------------------------------------------- LTAB
;;; introduces left tab into printed output
(defun ltab ()
  (decf *left-margin* *indent-amount*))

;;;----------------------------------------------------------------------- RTAB
;;; introduces right tab into printed output
(defun rtab ()
  (incf *left-margin* *indent-amount*))

;;;------------------------------------------------------------------------- RP
;;; reverse print the argument list

(defun rp (ll) (progn (mapcar #'(lambda(xx) ;(terpri)(princ xx)) 
                                  (format *output* "~&~A" xx))
                              (reverse ll)) :END))

;;;--------------------------------------------------------------------- ERRORS

(defun errors () 
  "print a list of error messages when there are some (global *error-message-list*)"
  (if *error-message-list*
    (progn (mapc #'(lambda (xx) (tkformat "~%~A" xx))
                 (reverse *error-message-list*)) :END)
    (tkformat "~%None.")))

;;;---------------------------------------------------------------- MAKE-CENTER

(defun make-center (text nn)
  "builds a string nn char long with centered text between + signs."
  (let* ((ii (max 0 (floor (/ (- nn 4 (length text)) 2))))
         (jj (max 0 (- nn 4 ii (length text)))))
    (format nil "~A ~A ~A"
            (make-string ii :initial-element #\+)
            text
            (make-string jj :initial-element #\+))))

#|
? (make-center "Centered Title" 60)
"----------------------------- Centered Title -----------------------------"
|#

;;;------------------------------------------------------------------------ TT
;;; test the compiler on the sol-test.lisp file

(defun tt (&key (verbose t) (errors-only nil))
  (catch :error (compile-sol (merge-pathnames "sol-test.sol" *directory*) 
                             :outfile (merge-pathnames "sol-test.owl" *directory*)
                             :verbose verbose :errors-only errors-only))
  (format t "~2&Internal Index table~&====================")
  (index))

;;;----------------------------------------------------------------------- TTF
;;; test the compiler on the sol-test-full.lisp file

(defun ttf (&key (verbose t) (errors-only nil))
  (catch :error 
    (compile-sol (merge-pathnames "sol-test-full.sol" *directory*)
		 :outfile (merge-pathnames "sol-test-full.owl" *directory*)
		 :verbose verbose :errors-only errors-only)))

(defun tt15 (&key (verbose t) (errors-only nil))
  (catch :error 
    (compile-sol (merge-pathnames "TG-ontology15-utf8pc.sol" *directory*)
		 :outfile (merge-pathnames "TG-ontology15pc-utf8.owl" *directory*)
		 :verbose verbose :errors-only errors-only)))

;;;=============================== functions ==================================

;;;------------------------------------------------------ CHECK-REL-CARDINALITY

(defun check-rel-cardinality (value-list option-list prop-id ind-id)
  "checks that the list of values obeys the cardinality conditions. Otherwise ~
   throws to :error.
Arguments:
   value-list: list to check
   option-list: list of options associated with the property
   prop-id: e.g. \"hasVillePays\"
   ind-id: e.g. \"z-ind-234\"
Return:
   t if OK"
  (let (pair)
    (cond
     ((setq pair (assoc :min option-list))
      (unless (>= (length value-list) (cadr pair))
        (terror "not enough values ~S for property ~S of object ~S"
                value-list prop-id ind-id)))
     ((setq pair (assoc :max option-list))
      (unless (<= (length value-list) (cadr pair))
        (terror "too many values ~S for property ~S of object ~S"
                value-list prop-id ind-id)))
     ((assoc :unique option-list)
      (unless (= (length value-list) 1))
      (terror "value ~S for property ~S of object ~S should be unique"
              value-list prop-id ind-id)))
    t))

#|
(check-rel-cardinality '("France" "Italy")'((:min 1)) "hasVillePays" "z-ind-333")
T

(catch :error
       (check-rel-cardinality 
        '("France" "Italy")'((:min 3)) "hasVillePays" "z-ind-333"))
"not enough values (\"France\"
                   \"Italy\") for property \"hasVillePays\" of object \"z-ind-333\""
|#
;;;------------------------------------------------------------ CHECK-REL-CLASS

(defun check-rel-class (ref class-id)
  "checks that the individual specified by ref is of the right class or is of ~
   its subclasses. Throws to :error if not true.
Arguments:
   ref: a string \"france\" or a variable _FR or a symbol BIESEL (unique entry)
   class-id: the OWL class-id, e.g. \"Z-Pays\"
Return:
   the OWL reference of the object."
  (let (suc-id suc-class-id)
    (cond
     ((symbolp ref)  ; to avoid processing rreferences like _I-23
      (setq suc-id
            (car (index-get (make-value-string ref) :inst :id))))
     ((stringp ref)
      (setq suc-id
           (car (index-get (make-index-string (substitute #\space #\- ref))
                           :inst :id)))))
    ;(format t "~%; check-rel-class / suc-id ~S" suc-id)

    ;; get suc-id class
    (setq suc-class-id (car (index-get suc-id :inst :class-id)))
    ;; check that it is a sub-class of class-id
    (unless (is-subclass-of? suc-class-id class-id)
      (terror "~S is not a reference to an object of class ~S" ref class-id))
    suc-id))
                  
#|
(check-rel-class "france" "Z-Pays")
"z-ind-23"

(check-rel-class '_fr "Z-Pays")
"z-ind-23"

(catch :error
       (check-rel-class "france" "Z-Ville"))
"\"france\" is not a reference to an object of class \"Z-Ville\""

(check-rel-class '_I-0 "Z-Person")
"z-ind-346"

(catch :error
       (check-rel-class '_I-0 "Z-Student"))
"_I-0 is not a reference to an object of class \"Z-Student\""

(check-rel-class "DE-AZEVEDO" "Z-Student")
"z-ind-398"

(check-rel-class "DE-AZEVEDO" "Z-Person")
"z-ind-398"

(catch :error (check-rel-class "DE-AZEVEDO" "Z-Organization"))
"\"DE-AZEVEDO\" is not a reference to an object of class \"Z-Organization\""
|#
;;;----------------------------------------------------------- CHECK-REL-ONE-OF

(defun check-rel-one-of (suc-list option-list prop-id ind-id)
  "checks if suc-list obey the one-of restriction.
Arguments:
   suc-list: list of successors, e.g. (\"z-ind-2\" \"z-ind-124\")
   option-list: list of option of the property
   prop-id: e.g. \"hasVillePays\"
   ind-id: e.g. \"z-ind-234\"
Return:
   t or throws to :error if false"
  (let (value-list)
    (unless (assoc :one-of option-list)
      (return-from check-rel-one-of t))
    ;; if option :forall is there, all values must be among those of the option
    ;; list
    ;; we must convert values of the option-list oto OWL ind-id
    (setq value-list 
          (mapcar #'(lambda (xx) (car (index-get (make-value-string xx) :inst :id)))
            (cdr (assoc :one-of option-list))))
    
    (cond
     ((assoc :for-all option-list)
      (unless (every #'(lambda(xx) (member xx value-list :test #'moss::equal+))
                     suc-list)
        (terror "some values in ~S do not belong to ~S for property ~S of object ~S"
                suc-list value-list prop-id ind-id)))
     ((assoc :exists option-list)
      (unless (some #'(lambda(xx) (member xx value-list :test #'moss::equal+))
                     suc-list)
        (terror "no value in ~S belong to ~S for property ~S of object ~S"
                suc-list value-list prop-id ind-id)))
     (t
      (unless (every #'(lambda(xx) (member xx value-list :test #'moss::equal+))
                     suc-list)
        (terror "some values in ~S do not belong to ~S for property ~S of object ~S"
                suc-list value-list prop-id ind-id))))
    t))
    
#|
(check-rel-one-of '("z-ind-23" "z-ind-24") '((:one-of "france" "italie")) 
                  "hasVillePays" "z-ind-2")
T
(catch :error
 (check-rel-one-of '("z-ind-23" "z-ind-27") '((:one-of "france" "italie")) 
                   "hasVillePays" "z-ind-2"))
"some values in (\"z-ind-23\" \"z-ind-27\") do not belong to (\"z-ind-23\"
                                                         \"z-ind-24\") for 
 property \"hasVillePays\" of object \"z-ind-2\""

(catch :error
 (check-rel-one-of '("z-ind-23" "z-ind-27") '((:one-of "france" "italie")(:exists)) 
                   "hasVillePays" "z-ind-2"))
T

(catch :error
 (check-rel-one-of '("z-ind-23" "z-ind-27") '((:one-of "france" "italie")(:forall)) 
                   "hasVillePays" "z-ind-2"))
"some values in (\"z-ind-23\" \"z-ind-27\") do not belong to (\"z-ind-23\"
                                                         \"z-ind-24\") for 
 property \"hasVillePays\" of object \"z-ind-2\""
|#          
;;;--------------------------------------------------------------- CHECK-SYNTAX

(defun check-syntax (sol-file)
  "checks structural syntax of SOL definitions, i.e. parents and quotes.
   In case of error *syntax-errors* is set to T.
Argument:
   sol-file: name of sol-file
Return:
   string stating how many entries were checked."
  (let ((count 0)
	(syntax-error-count 0)
	expr text-string (line-mark 0))
    ;; reset text and syntax error flag and line count
    (setq *text* nil *syntax-errors* nil *line-number* 0)
    ;; open file
    (with-open-file (ss sol-file :direction :input)
      ;; initialize first line
      (setq *line* (read-next-valid-line ss))
      ;(print *line*)
      (setq *text* (list *line*))
      ;; loop on definitions
      (catch :done
        (loop
          ;; read next line
          (setq *line* (read-next-valid-line ss))
          ;(print `(===> ,*line*))
          ;; read-next-valid-line returns :eof on when end of file is reached
	  (if (eql *line* :eof) (return))
          ;; otherwise check for breaks (lines starting with defxxx)
          (if (member t (mapcar 
		         #'(lambda (xx) (and 
				         (search xx *line* :test #'string-equal)
				         t))
		         *breaks*))
	    ;; check for valid definition
	    (progn
	      ;; we got something
	      ;; mark line position
	      (setq line-mark *line-number*)
	      (incf count)
	      (setq text-string (format nil "~{~A~&~}" (reverse *text*)))
	      ;(format t "~&~{~A~&~}" (reverse *text*))
	      ;; catch the error condition while reading from string
	      ;; this one catches missing closing parents or unbalanced quotes
	      (setq expr (handler-case (read-from-string text-string)
			   (error () :error)))
	      (case expr
	        (:error
		 (tkformat
		  "~2%*** ERROR in expression starting at line ~S of the .tmp file"
                  line-mark)
		 ;; we assume that the first line is defxx, and the second
		 ;; one is a mln, and hope that no special char will cause a hangup
		 (tkformat 
		  "~%   check parentheses and quotes in:~&~{~A~&~}~&..." 
		  (subseq (reverse *text*) 0 (min 5 (length *text*))))
		 (setq *syntax-errors* t)
		 (incf  syntax-error-count))
	        )
              
	      ;; if last read line was :EOF or we reached end of file
	      ;; then we are finished
	      (if (or (string-equal *line* ":EOF") ; last line contained :EOF
		      (eql *line* :eof)) ; we reached end of file?
                (throw :done nil))
	      ;; reset *text*
	      (setq *text* (list *line*))
	      )
	    ;; otherwise simply push line into text
	    (push *line* *text*))
          )))
    ;; quit, returning a string to be printed
    (format nil "~%We checked ~S entries, and found ~S error(s) ~
                 with parents or quotes." 
	    count syntax-error-count)))

;(check-syntax "c:/SOL/test.sol")

;;;----------------------------------------------------------------- CHECK-TYPE

(defun check-sol-type (value type)
  "check if the value has the type specified by type. If not, throws an error.
Arguments:
   value: any expression
   type: one of the allowed SOL types, e.g. :integer
Return:
   T if OK, nil otherwise."
  (case type
    (:integer (integerp value))
    (:float (floatp value))
    (:string (stringp value))
    (:date (numberp value))
    (:name (stringp value))
    (:g-year (numberp value))
    (t (terror "unknown attribute type: ~S" type))))

#|
? (check-sol-type 12 :integer)
T
? (check-sol-type "12" :integer)
NIL
|#
;;;------------------------------------------------- COMPUTE-TRANSITIVE-CLOSURE

(defun compute-transitive-closure (candidates property &optional result)
  "computes the transitive closure of a class using *index*
Arguments:
   class: a class OWL ID; can be a list
   property: the SOL id e.g. :is-a
   result: a list of classes already explored
Return:
   the list of OWL IDs."
  (cond ((null candidates) (reverse result))
        ((not (listp candidates)) 
         (compute-transitive-closure (list candidates) property result))
        ;; when 1st element is not a class, complain
        ((not (index-get (car candidates) :class :name))
         (terror "While computing transitive closure ~S is not a class." 
                 (car candidates)))
        ((member (car candidates) result :test #'equal)
         (compute-transitive-closure (cdr candidates) property result))
        (t
         (compute-transitive-closure
          (append (index-get (car candidates) :class property)
                  (cdr candidates))
          property
          (cons (car candidates) result)))))

#|
? (index)

("Ville" ((:REL (:ID "hasTown")) (:CLASS (:ID "Z-City")))) 
("Z-City" ((:CLASS (:IS-A "Z-Territory") (:NAME :EN "City; town" :FR "ville; cité")))) 
("Name" ((:ATT (:ID "hasName")))) 
("Conurbation" ((:CLASS (:ID "Z-Conurbation" "Z-Conurbation")))) 
("Cité" ((:CLASS (:ID "Z-City")))) 
("Z-Territory" ((:CLASS (:NAME :EN "territory")))) 
("Z-Conurbation" ((:CLASS (:NAME :EN "Conurbation" :FR "conurbation")))) 
("City" ((:CLASS (:ID "Z-City")))) 
("Territory" ((:CLASS (:ID "Z-Territory")))) 
("Town" ((:REL (:ID "hasTown")) (:CLASS (:ID "Z-City")))) 
NIL

? (compute-transitive-closure "Z-City" :is-a)
("Z-City" "Z-Territory")
? (catch :error (compute-transitive-closure "Z-City" :isa))
("Z-City")
? (catch :error (compute-transitive-closure "City" :isa))
"While computing transitive closure \"City\" is not a class."
|#
;;;--------------------------------------- EXTRACT-DOCUMENTATION-FROM-FRAGMENTS

(defun extract-documentation-from-fragments (fragments)
  "takes a list of fragments (attributes or relations) and extract the ~
   documentation  multilingual strings. Fuses the documentation into a single ~
   multilingual string.
Arguments:
   fragments: a list of fragments
Return:
   a multi-lingual string or nil if there is no documentation."
  (let (doc result language language-doc)
    ;; get the list of strings
    (setq doc (mapcar #'(lambda(xx) (cdr (assoc :doc (cdr xx)))) fragments))
    ;; merge fragments
    (dolist (tag *language-tags*)
      (setq language (remove nil
                             (mapcar #'(lambda(xx) (cadr (member tag xx))) doc)))
      ;(print language)
      (setq language-doc (make-single-string 
                          (delete-duplicates language :test #'string-equal)))
      (unless (string-equal "" language-doc)
        (+result (list tag language-doc))))
    result))

#|
? (let ((fragments '(("Z-Person" (:EN "referent" :FR "référent") (:TO "person")
             (:DOC :EN "A Person is a REFERENT for another person if this person is in ~
               charge of keeping track of what happens to the other person."))
           ("Z-Mayor" (:en "referent") (:doc :en "a Mayor is a REFERENT ~
                 for the prefect of the department." :fr "le maire est ..."))
           ("Z-xxx" (:fr "le maire est ...")))))
  (extract-documentation-from-fragments fragments))
(:FR "le maire est ..." :EN
 "A Person is a REFERENT for another person if this person is in ~
  charge of keeping track of what happens to the other person. ; a Mayor is a REFERENT ~
  for the prefect of the department.")
|#
;;;------------------------------------------------ extract-attribute-fragments
;;; *attribute-list* can contain a number of entries for the same attribute
;;; e.g. "name" can stand for a number of concepts. extract-attribute-fragments
;;; gathers all the different fragments, merges the different MLN attribute names
;;; and returns a list with the name in front, followed by the fragments
;;; an entry of the *attribute-list* looks like
;;;  (("Z-Territoire" (:EN "name" :FR "nom") (:TYPE :NAME) (:UNIQUE) (ENTRY)))
;;; the class id and option list should be retained for processing individuals

(defun extract-attribute-fragments ()
  "extracts from the *attribute-list* all fragments defining the same attribute.
   To do so, used the names. If two fragments share the same name they apply to ~
      the same attribute. E.g. (:en \"sex\" :fr \"genre\") (:fr \"sexe; genre\")
Arguments:
  none
Return:
  a list of fragments with merged names in front."
  (declare (special *attribute-list*))
  ;; when the attribute list is empty return nil (should not have been called ?)
  (unless *attribute-list* (return-from extract-attribute-fragments nil))
  
  (let (found-some?
        (result (list (pop *attribute-list*)))
        (attributes *attribute-list*)
        att)
    
    ;; extract first value e.g. ((:en "sex" :fr "sex; genre") (:unique) ...)
    (setq att (mln::make-mln (cadar result)))
    ;; the extracting loop may have several passes
    (loop
      (dolist (item attributes)
        (when (mln::mln-equal att (cadr item))
          ;; record fragment
          (push item result)
          (setq attributes (delete item attributes :test #'equal))
          (setq *attribute-list* (remove item *attribute-list*))
          (setq att (mln::mln-merge att (cadr item)))
          (setq found-some? t)))
      (if found-some?
        (setq found-some? nil)
        (return)
        ))
    ;; exit
    (cons att result)))

#|
(let ((*attribute-list* 
       '(("Z-Person" (:EN "referent" :FR "référent") (:TO "person")
            (:DOC :EN "A Person is a REFERENT for another person if this person is in ~
               charge of keeping track of what happens to the other person."))
         ("Z-Mayor" (:en "referent") (:doc :en "a Mayor is a REFERENT ~
                 for the prefect of the department." :fr "le maire est ..."))
         ("Z-xxx" (:fr "le maire est ...")))))
  (extract-attribute-fragments))
(((:FR "référent") (:EN "referent"))
 ("Z-Mayor" (:EN "referent")
  (:DOC :EN "a Mayor is a REFERENT ~
                 for the prefect of the department." :FR "le maire est ..."))
 ("Z-Person" (:EN "referent" :FR "référent") (:TO "person")
  (:DOC :EN "A Person is a REFERENT for another person if this person is in ~
               charge of keeping track of what happens to the other person.")))

? (let ((*attribute-list* 
         '(("Z-Territory" (:EN "name" :FR "nom") (:TYPE :NAME) (:UNIQUE))
           ("Z-City" (:IT "nome" :FR "nom") (:TYPE :NAME))
           ("Z-Person" (:IT "nome")))))
    (extract-attribute-fragments))
(((:EN "name") (:FR "nom") (:IT "nome")) 
 ("Z-Person" (:IT "nome"))
 ("Z-City" (:IT "nome" :FR "nom") (:TYPE :NAME))
 ("Z-Territory" (:EN "name" :FR "nom") (:TYPE :NAME) (:UNIQUE)))

? (let ((*attribute-list* 
         '(("Z-Territory" (:EN "name" :FR "nom") (:TYPE :NAME) (:UNIQUE)))))
  (extract-attribute-fragments))
(((:EN "name") (:FR "nom"))
 ("Z-Territory" (:EN "name" :FR "nom") (:TYPE :NAME) (:UNIQUE)))
|#
;;;------------------------------------------------- extract-relation-fragments

(defun extract-relation-fragments ()
  "extracts from the *relation-list* all fragments defining the same relation
   To do so, used the names. If two fragments share the same name they apply to ~
      the same relation 
   E.g. (:en \"brother\" :fr \"frère\") (:fr \"frangin; frère\")
Arguments:
   none
Return:
   a list of relations with merged names in front.
Side-effect:
   all fragments have been removed from *relation-list*"
  (declare (special *relation-list*))
  (when *relation-list*
    (let* ((result (list (pop *relation-list*)))
           rel-mln found-some?)
      ;; extract first value e.g. ((:en "sex" :fr "sex; genre") (:unique) ...)
      (setq rel-mln (cadar result))
      ;; when the relation list is empty return nil (should not have been called ?
      (unless *relation-list* (return-from extract-relation-fragments 
                                (cons rel-mln result)))
      ;; the extracting loop may have several passes, because we could have
      ;; missed an mln by lack of common synonyms
      (loop
        (dolist (item *relation-list*)
          (when (mln::mln-equal rel-mln (cadr item))
            ;; merge the two mlns to gather synonyms
            (setq rel-mln (mln::mln-merge rel-mln (cadr item)))
            ;; record entry
            (push item result)
            ;(setq relations (delete item relations :test #'equal))
            (setq *relation-list* (remove item *relation-list* :test #'equal))
            ;; indicate that we found a new fragment
            (setq found-some? t)))
        ;; if we did not find any new fragment, exit the loop
        (unless found-some? (return))
        ;; otherwise, reset flag
        (setq found-some? nil)
        )
      ;; exit
      (cons rel-mln result))))

#|
? (setq *relation-list* 
         '(("Z-Person" (:en "brother" :fr "frangin; frère") "Person")
           ("Z-Département" (:fr "région") "région")
           ("Z-Tiger" (:it "fratello")(:min 1) "tiger")
           ("Z-Unicorn" (:fr "  frère" :it "fratello") "unicorn")))
;; note that the Tiger entry cannot be caught the first time around

? (catch :error (extract-relation-fragments))
(((:EN "brother") (:FR "frangin" "frère") (:IT "fratello"))
 ("Z-Tiger" (:IT "fratello") (:MIN 1) "tiger")
 ("Z-Unicorn" (:FR "  frère" :IT "fratello") "unicorn")
 ("Z-Person" (:EN "brother" :FR "frangin; frère") "Person"))

? *relation-list*
(("Z-Département" (:FR "région") "région"))

? (catch :error (extract-relation-fragments))
((:FR "région") ("Z-Département" (:FR "région") "région"))

? *relation-list*
NIL

? (catch :error (extract-relation-fragments))
NIL
|#
;;;-------------------------------------------------------------- extract-names
;;; deprecated use mln package

(defun extract-names (name-string)
  "extract names separated by a semi column from name-string.
Argument:
   name-string: a string of names possible separated be semi-columns
Return:
   a list of strings corresponding to each single name
Error:
   if not a string or empty throws a message string to :error."
  (let (text result end word)
    (when (or (not (stringp name-string))
              (equal "" (setq text (string-trim '(#\space) name-string))))
      (terror "Bad name string: ~S" name-string))
    ;; extract names
    (loop
      (setq end (position #\; text))
      (cond 
       ;; still some semi-column?
       ((numberp end)
        ;; yes extract word
        (setq word (string-trim '(#\space) (subseq text 0 end))
              text (subseq text (1+ end)))
        (unless (equal "" word) (push word result)))
       ;; finished.
       (t
        (setq word (string-trim '(#\space) text))
        (unless (equal "" word) (push word result))
        (unless result
          (terror "Empty name string: ~S" name-string))
        (return (reverse result)))))))

#|
? (EXTRACT-NAMES "albert le voisin")
("albert le voisin")
? (EXTRACT-NAMES "albert le voisin ; albertine")
("albert le voisin" "albertine")
? (EXTRACT-NAMES "albert le voisin ; albertine;  Zoe oui   ")
("albert le voisin" "albertine" "Zoe oui")
? (catch :error (MOSS::EXTRACT-NAMES " ;    "))
"Empty name string: \" ;    \""
|#
;;;--------------------------------------------------------------- get-all-name
;;; deprecated, use mln package

;;;(defun get-all-names (name-string)
;;;  "extract all names from the name string. Names are separated by semi-columns.
;;;   E.g. \"identity card ; papers\" returns (\"identity card \" \"papers\"
;;;Argument:
;;;   name-string
;;;Return:
;;;   string with the first name." 
;;;  (let ((remaining-string name-string) result pos)
;;;    (loop
;;;      ;; remove trailing spaces
;;;      (setq remaining-string (string-trim '(#\space) remaining-string))
;;;      ;; get position of the next ";"
;;;      (setq pos (position #\; remaining-string))
;;;      (cond
;;;       ;; if non nil, extract first name
;;;       (pos
;;;        (push (subseq remaining-string 0 pos) result)
;;;        (setq remaining-string (subseq remaining-string (1+ pos))))
;;;       ;; otherwise, last name return
;;;       (t
;;;        (push remaining-string result)
;;;        (return))))
;;;    (reverse result)))

#|
? (GET-ALL-NAMES "frangin; frère")
("frangin" "frère")
|#
;;;------------------------------------------------------- GET-ATTRIBUTE-DOMAIN

(defun get-attribute-domain (att-fragments)
  "gett the list of domains from the fragments.
Arguments:
   att-fragments: list of att specs
Return:
   list of domain OWL IDs."
  (delete-duplicates (mapcar #'car  att-fragments) :test #'string-equal))

#|
? (setf (gethash "Person" *index*) "Z-Person")
"Z-Person"
? (setf (gethash "Département" *index*) "Z-Department")
"Z-Département"
? (setf (gethash "Tiger" *index*) "Z-Tiger")
"Z-Tiger"
? (setf (gethash "Unicorn" *index*) "Z-Unicorn")
"Z-Unicorn"
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique))
          ("Département" (:fr "département"))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1))
          ("Unicorn" (:pl "?" :en "  sex"))))
(("Person" (:EN "sex" :FR "sexe; genre") (:UNIQUE))
 ("Département" (:FR "département")) ("Tiger" (:FR " Genre " :IT "sexo") (:MIN 1))
 ("Unicorn" (:PL "?" :EN "  sex")))
? (get-attribute-domain *attribute-list*)
("Person" "Department" "Tiger" "Unicorn")
|#
;;;------------------------------------------------- GET-ATTRIBUTE-ID-FOR-CLASS

(defun get-attribute-id-for-class (att-name class-id)
  "tries to get the proper attribute id from a name and for the specific class. ~
   If not available tries superclass. If not found, throws to :error.
Argument:
   att-name: e.g. \"FirstName\"
   class-id: e.g. \"Z-Student\"
Return:
   the relation is e.g. \"hasPersonFirstName\" or throws to :error"
  (let ((class class-id)
        att-id)
    (loop
      (unless class 
        (terror "attribute ~S not available for class ~S" att-name class-id))
      (setq att-id (car (index-get att-name class :id)))
      (if att-id (return-from get-attribute-id-for-class att-id))
      ;; otherwise try superclass
      (setq class (car (index-get class :CLASS :IS-A)))
      )
    ))

#|
(get-attribute-id-for-class "Name" "Z-Person")
"hasPersonName"

(catch :error
       (get-relation-id-for-class "Name" "Z-Student"))
"hasPersonName"
|#
;;;------------------------------------------------------- GET-ATTRIBUTE-ONE-OF

(defun get-attribute-one-of (att-fragments)
  "get the one-of option, should be the same. Type must be checked separatly.
Arguments:
   att-fragments: list of specs for a given attribute
Return:
   nil if no, :one-of option <option> if same for all
Error:
   throws to an :error tag"
  ;; extract all the fragments with a :one-of option (should be only one)
  (let ((range (remove nil
                       (mapcar #'(lambda (xx) (if (assoc :one-of (cddr xx)) xx))
                 att-fragments))))
    ;; range is e.g. ((:ONE-OF "aristocratique" "ouvrière" "bourgeoise"))
    ;; eventually with mln values
    
    (format t "~%; get-attribute-one-of /range: ~S" range)
    
    ;(break "get-attribute-one-of")
    
    ;; check possible errors
    (cond
     ;; (nil) indicates no one-of option; OK, no error
     ((null (car range)))
     ;; if more than one element, error
     ((cdr range)
      (terror "one-of option should appear only once in:~&~S" 
              range))
     ;; if single option, elements of the list cannot be MLS
;;;     ((multilingual-string? (cadar range))
;;;      (terror "range cannot be a multilingual string in: ~&~S"
;;;              att-fragments))
     ;; range must not be nil
     ((null (cdar range))
      (terror "null range in :one-of option: ~&~S"
              att-fragments)))
    ;; return range, e.g. (:ONE-OF "aristocratique" "ouvrière" "bourgeoise")
    (car range)))

#|
(catch :error (get-attribute-one-of 
    '(("Person" (:en "sex" :fr "sexe; genre") (:unique)))))
NIL

(catch :error (get-attribute-one-of 
     '(("Person" (:en "sex" :fr "sexe; genre") (:unique)(:one-of 1 2 3)))))
(:ONE-OF 1 2 3)

(catch :error 
       (get-attribute-one-of 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique)(:one-of)))))
"null range in :one-of option: 
((\"Person\" (:EN \"sex\" :FR \"sexe; genre\") (:UNIQUE) (:ONE-OF)))"
                  
(catch :error 
       (get-attribute-one-of 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique)(:one-of 1 2))
          ("Town" (:en "name" :fr "nom") (:one-of "Paris" "Amsterdam"))
          ("Plant" (:en "sex" :fr "genre")))))
                 
|#
;;;--------------------------------------------------------- GET-ATTRIBUTE-TYPE

(defun get-attribute-type (att-fragments)
  "get the various definitions for the attribute range. Should be identical. ~
   If not then we have an error. Literal is given by the :type option. ~
   Default is (:type :string). Option :one-of is incompatble with :type.
Arguments:
   att-fragments: list of att specs
Return:
   nil or range spec if all identical, e.g. :integer"
  (let ((range (mapcar #'(lambda (xx)(or (assoc :type (cdr xx))
                                         '(:type :string) ; default value
                                         ))
                       att-fragments)))
    ;; if all the same next line should condense into one element
    (setq range (delete-duplicates range :test #'equal))
    ;; if more than one element, error
    (when (cdr range)
      (terror "inconsistent range definitions ~S" att-fragments))
    ;; return single attribute
    (cadar range)))

#|
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique))
          ("Département" (:fr "département"))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1)(:type :string))
          ("Unicorn" (:pl "?" :en "  sex"))))
(("Person" (:EN "sex" :FR "sexe; genre") (:UNIQUE))
 ("Département" (:FR "département"))
 ("Tiger" (:FR " Genre " :IT "sexo") (:MIN 1) (:TYPE :STRING))
 ("Unicorn" (:PL "?" :EN "  sex")))
? (get-attribute-type *)
:STRING
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique))
          ("Département" (:fr "département")(:type :integer))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1)(:type :string))
          ("Unicorn" (:pl "?" :en "  sex"))))
(("Person" (:EN "sex" :FR "sexe; genre") (:UNIQUE))
 ("Département" (:FR "département") (:TYPE :INTEGER))
 ("Tiger" (:FR " Genre " :IT "sexo") (:MIN 1) (:TYPE :STRING))
 ("Unicorn" (:PL "?" :EN "  sex")))
? (get-attribute-type *)
> Error: Can't throw to tag :ERROR .
> While executing: GET-ATTRIBUTE-RANGE
> Type Command-. to abort.
See the RestartsÉ menu item for further choices.
|#
;;;------------------------------------------------------------- GET-FIRST-NAME

(defun get-first-name (name-string)
  "extracts the first name from the name string. Names are separated by ~
      semi-columns.
   E.g. \"identity card ; papers\" returns \"identity card \"
Argument:
   name-string
Return:
   string with the first name."
  (subseq name-string 0 (position #\; name-string)))

#|
? (get-first-name "frangin; frère")
"frangin"
|#
;;;-------------------------------------------------------------- GET-ID-STRING

(defun get-id-string (ll)
  "extracts from a multilingual list the string that will be used to build ~
      an OWL ID.
The algorithm is to take the first English name, of else the first name in the ~
      list.
Arguments:
   ll: a mls, e.g. (:fr \"masculin\" :en \"male   AGAIN\")
Return:
   a candidate string to be further processed."
  (declare (special *language*))
  (mln::get-canonical-name ll *language*))

;;;  (let ((ml-string (multilingual-name? ll)))
;;;    (unless ml-string
;;;      (terror "bad MLS format in ~S" ll))
;;;    (or (get-first-name (cadr (member :en ml-string))) 
;;;        (get-first-name (cadr ml-string)))))

#|
? (get-id-string '(:fr "masculin" :en "male   AGAIN; man"))
"masculin"
:FR

? (let ((*language* :EN))
  (get-id-string '(:fr "masculin" :en "male   AGAIN; man")))
"male   AGAIN"

? (let ((*language* :EN))
  (get-id-string '(:fr "masculin; mec" :it "male   AGAIN")))
"masculin"
:FR
|#
;;;------------------------------------------------------------- GET-LAN-VALUES

(defun get-lan-values (val &optional always &aux result)
  "takes the val argument and if MLN, extracts the current language value. If ~
   the resulting value is nil, will get a canonical value when always is true."
  (cond
   ((not (is-mln? val)) val)
   ((and (null (setq result (mln::filter-language val *language*))) always)
    (list (mln::get-canonical-name val *language*)))
   (result)))

#|
(get-lan-values nil)
NIL

(get-lan-values 22)
22

(get-lan-values '(1 2 3))
(1 2 3)

(let((*language* :en))
  (get-lan-values '(:fr "Albert" :en "George; John")))
("George" "John")

(let((*language* :it))
  (get-lan-values '(:fr "Albert" :en "George; John")))
NIL

(let((*language* :it))
  (get-lan-values '(:fr "Albert" :en "George; John") :always))
("George")
|#
;;;?------------------------------------------------------- GET-LANGUAGE-STRING
;;; deprecated, use mln::filter-language

;;;(defun get-language-string (multilingual-string language-tag)
;;;  "get the string corresponding to the language tag (return English as default).
;;;Arguments:
;;;   multilingual-string: e.g. (:en \"town\" :fr \"ville\)
;;;   language-tag: e.g. :fr 
;;;Return:
;;;   string or empty-string"
;;;  (or (cadr (member language-tag multilingual-string)) 
;;;      (cadr (member :en multilingual-string))
;;;      (cadr multilingual-string)
;;;      "?"))
;;;
;;;#|
;;;(get-language-string '(:en "town" :fr "ville") :fr)
;;;"ville"
;;;
;;;(get-language-string '(:en "town" :fr "ville; cité") :fr)
;;;"ville; cité"
;;;
;;;(get-language-string '(:en "town" :fr "ville") :it)
;;;"town"
;;;|#
;;;------------------------------------------------------------------- GET-NAME

(defun get-name (class-id)
  "recovers one of the class names from the class-id by looking into the index ~
      table.
Argument:
   class-id: e.g. \"Z-FrenchDepartment\"
Return:
   one of the class names, e.g. \"French Department\"."
  (get-id-string (index-get class-id :class :name)))

#|
? (get-name "Z-FrenchDepartment")
"French department"
|#
;;;-------------------------------------------------------- GET-RELATION-DOMAIN

(defun get-relation-domain (rel-fragments)
  "gets the list of domains from the fragments.
Arguments:
   att-fragments: list of rel specs
Return:
   list of domain OWL IDs."
  (delete-duplicates (mapcar #'car  rel-fragments) :test #'string-equal))

#|
? (get-relation-domain '(
 ("Z-Unicorn" (:PL "?" :EN "  brother") "unicorn")
 ("Z-Tiger" (:FR " frère " :IT "fratello") (:MIN 1) "tiger")
 ("Z-Person" (:EN "brother" :FR "frangin; frère") "Person")))
("Z-Unicorn" "Z-Tiger" "Z-Person")
|#
;;;-------------------------------------------------- GET-RELATION-ID-FOR-CLASS

(defun get-relation-id-for-class (rel-name class-id)
  "tries to get the proper relation id from a name and for the spcific class. ~
   If not available tries superclass. If not found, throws to :error.
Argument:
   rel-name: e.g. \"Brother\"
   class-id: e.g. \"Z-Student\"
Return:
   the relation is e.g. \"hasPersonBrother\" or throws to :error"
  (let ((class class-id)
        rel-id)
    (loop
      (unless class 
        (terror "relation ~S not available for class ~S" rel-name class-id))
      (setq rel-id (car (index-get rel-name class :id)))
      (if rel-id (return-from get-relation-id-for-class rel-id))
      ;; otherwise try superclass
      (setq class (car (index-get class :CLASS :IS-A)))
      )
    ))

#|
(get-relation-id-for-class "Brother" "Z-Person")
"hasPersonBrother"

(catch :error
       (get-relation-id-for-class "Brother" "Z-Student"))
"hasPersonBrother"
|#
;;;-------------------------------------------------------- GET-RELATION-ONE-OF
;;; unused

;;;(defun get-relation-one-of (att-fragments)
;;;  "get the one-of option, should be the same. Type must be checked separatly.
;;;   Since the one-of options are difficult to compare, due tot the possibility ~
;;;   of synonyms in the multilingual names, when they are used at different ~
;;;   places, they should be made a separate class. Thus, if not defined in ~
;;;   exactly the same terms we declare an error.
;;;Arguments:
;;;   att-fragments: list of att specs
;;;Return:
;;;   nil if no, :one-of option <option> if same for all
;;;Error:
;;;   throws to an :error tag"
;;;  (let ((range (mapcar #'(lambda (xx) (get-relation-one-of-local (cddr xx)))
;;;                       att-fragments)))
;;;    ;; if all the same next line should condense into one element
;;;    ;;***** this test is too crude since the spelling could be different and in case
;;;    ;; of relerences to existing instance, the string may be different but refer
;;;    ;; to the same object
;;;    (setq range (delete-duplicates range :test #'equal))
;;;    ;(print range)
;;;    ;; check possible errors
;;;    (cond
;;;     ;; if more than one element, error
;;;     ((cdr range)
;;;      (terror "lacking or possibly different one-of options in:~&~S~
;;;               ~&make a separate class \"xxx Type\" for cases." 
;;;              att-fragments))
;;;     ;; (nil) indicates no one-of option
;;;     ((null (car range)))
;;;     ;; if single option, elements of the list should strings
;;;     ((every #'stringp (cdar range)))
;;;     ;; ... or MLS
;;;     ;((some #'null (mapcar #'multilingual-string? (cdar range)))
;;;     ((some #'null (mapcar #'is-mln? (cdar range)))
;;;      (terror "range ~S should be a multilingual string in: ~&~S"
;;;              (cdar range) att-fragments)))
;;;    ;; return range
;;;    (car range)))
;;;
;;;#|
;;;? (catch :error
;;;    (get-relation-one-of '(
;;; ("Z-Unicorn" (:PL "?" :EN "  brother") "day" (:min 1))
;;; ("Z-Person" (:en "sex") (:one-of (:en "male")(:en "female")))
;;; ("Z-Tiger" (:FR " genre " :IT "sexo") (:unique) (:one-of (:en "male")(:en "female")))
;;; )))
;;;"possible different one-of options:
;;;((\"Z-Unicorn\" (:PL \"?\" :EN \"  brother\") \"day\" (:MIN 1))
;;; (\"Z-Person\" (:EN \"sex\") (:ONE-OF (:EN \"male\") (:EN \"female\")))
;;; (\"Z-Tiger\" (:FR \" genre \" :IT \"sexo\") (:UNIQUE)
;;;  (:ONE-OF (:EN \"male\") (:EN \"female\"))))
;;;make a separate class \"xxx Type\" for cases."
;;;? (catch :error
;;;    (get-relation-one-of '(
;;; ("Z-Person" (:en "sex") (:one-of (:en "male")(:en "female")))
;;; ("Z-Tiger" (:FR " genre " :IT "sexo") (:unique) (:one-of (:en "male")(:en "female")))
;;; )))
;;;(:ONE-OF (:EN "male") (:EN "female"))
;;;? (catch :error
;;;    (get-relation-one-of '(
;;; ("Z-Person" (:en "sex") (:one-of (:en "male")(:en "female")))
;;; ("Z-Tiger" (:FR " genre " :IT "sexo") (:unique) (:one-of (:en "male")(:en "female" :fr "féminin")))
;;; )))
;;;"lacking or possibly different one-of options in:
;;;((\"Z-Person\" (:EN \"sex\") (:ONE-OF (:EN \"male\") (:EN \"female\")))
;;; (\"Z-Tiger\" (:FR \" genre \" :IT \"sexo\") (:UNIQUE)
;;;  (:ONE-OF (:EN \"male\") (:EN \"female\" :FR \"féminin\"))))
;;;make a separate class \"xxx Type\" for cases."
;;;|#
;;;-------------------------------------------------- GET-RELATION-ONE-OF-LOCAL
;;; unused

;;;(defun get-relation-one-of-local (option-list)
;;;  "extract the :one-of option from the option list. Required since some options
;;;are not lists.
;;;Arguments:
;;;   option-list: e.g. (\"day\" (:min 1)) 
;;;                or ((:unique) (:one-of (:en \"male\")(:en \"female\")))
;;;Result:
;;;   nil or :one-of clause."
;;;  (unless (listp option-list)
;;;    (terror "bad relation format"))
;;;  (dolist (option option-list)
;;;    (if (and (listp option)(eql :one-of (car option)))
;;;      (return-from get-relation-one-of-local option))))
;;;
;;;#|
;;;? (get-relation-one-of-local (cddr '("Z-Unicorn" (:PL "?" :EN "  brother") "day" (:min 1))))
;;;NIL
;;;? 
;;;GET-RELATION-ONE-OF-LOCAL
;;;? (get-relation-one-of-local (cddr '("Z-Unicorn" (:PL "?" :EN "  brother") "day" (:min 1))))
;;;NIL
;;;? (get-relation-one-of-local (cddr '("Z-Person" (:en "sex") (:one-of (:en "male")(:en "female")))))
;;;(:ONE-OF (:EN "male") (:EN "female"))
;;;? (get-relation-one-of-local (cddr '("Z-Tiger" (:FR " genre " :IT "sexo") (:unique) (:one-of (:en "male")(:en "female")))))
;;;(:ONE-OF (:EN "male") (:EN "female"))
;;;|#
;;;--------------------------------------------------------- GET-RELATION-RANGE
;;; unused

;;;(defun get-relation-range (rel-fragments)
;;;  "get the list of ranges from the fragments.
;;;Arguments:
;;;   att-fragments: list of rel specs
;;;Return:
;;;   list of domain OWL IDs."
;;;  (let (class-list class-id lres)
;;;    ;; each cdr of fragment should be an a-list, otherwise error
;;;    (dolist (fragment rel-fragments)
;;;      (unless (alist? (cdr fragment))
;;;        (terror "bad syntax in fragment ~S for relation ~S" 
;;;                fragment (car fragment))))
;;;    ;; get the class-list
;;;    (setq class-list (mapcan #'(lambda(xx)(cdr (assoc :to (cdr xx)))) 
;;;                             rel-fragments) )
;;;    ;; if no range, then we might have a one-of option
;;;    (unless class-list (return-from get-relation-range nil))
;;;    ;; check that syntax was OK all must be strings
;;;    (dolist (item class-list)
;;;      ;; we use dolist to locate possible error
;;;      (if (stringp item)
;;;        (push item lres)
;;;        (terror "bad range class format ~S in some relations: ~&~S" 
;;;                item rel-fragments)))
;;;    ;; clean up list normalizing strings
;;;    (setq class-list (delete-duplicates 
;;;                      (mapcar #'make-index-string lres) :test #'string-equal))
;;;    (setq lres nil)
;;;    ;; check now that strings correspond to actual classes
;;;    (dolist (item class-list)
;;;      (if
;;;        (setq class-id (car (index-get item :class :id)))
;;;        (push class-id lres)
;;;        (terror "undefined range class ~S in some relations: ~&~S" 
;;;                item rel-fragments)))
;;;    ;; make sure we do not return the same class id several times
;;;    (delete-duplicates lres :test #'string-equal) ; JPB051029
;;;    ))
;;;
;;;#|
;;;? (tt)
;;;...
;;;? (catch :error (get-relation-range '(
;;; ("Z-Unicorn" (:PL "?" :EN "  brother") (:min 1)(:to "day"))
;;; ("Z-Tiger" (:FR " frère " :IT "fratello") (:to "time unit") (:MIN 1) )
;;; ("Z-Person" (:EN "brother" :FR "frangin; frère") (:to "  time Unit")))))
;;;("Z-Day" "Z-TimeUnit")
;;;? (catch :error (get-relation-range '(
;;; ("Z-Unicorn" (:PL "?" :EN "  brother") (:to "day") (:min 1))
;;; ("Z-Tiger" (:FR " frère " :IT "fratello") (:to "tiger") (:MIN 1) )
;;; ("Z-Person" (:EN "brother" :FR "frangin; frère") (:to "  time Unit")))))
;;;"undefined range class \"Tiger\" in some relations: 
;;;((\"Z-Unicorn\" (:PL \"?\" :EN \"  brother\") (:TO \"day\" \"tiger\" \"  time Unit\")
;;;                (:MIN 1))
;;; (\"Z-Tiger\" (:FR \" frère \" :IT \"fratello\") (:TO \"tiger\" \"  time Unit\") (:MIN 1))
;;; (\"Z-Person\" (:EN \"brother\" :FR \"frangin; frère\") (:TO \"  time Unit\")))"
;;;? (get-relation-range '(("Z-Address" (:FR "lieu") (:TO "pays"))
;;;    ("Z-PostalAddress" (:EN "location" :FR "lieu") (:TO "country"))))
;;;("Z-Country")
;;;|#
;;;----------------------------------------------------- GET-XSD-ATTRIBUTE-TYPE

(defun get-xsd-attribute-type (type-keyword prop-id concept-id)
  "get the string representing the xsd type from the equivalent keyword. ~
      All keywords are in the *attribute-types* global variable.
   E.g. :g-year returns (\"gYear\"
   Throws an error to :error when type is unknown.
Argument:
   type-keyword: a keyword specifying the type
   prop-id: OWL id of the property, e.g. \"hasYear\"
   concept-id: OWL id of the concept, e.g. \"Z-Decree\"
Return:
   xsd type string." 
  (let ((xsd-type-string (cdr (assoc type-keyword *attribute-types*))))
    (unless xsd-type-string
      (terror "illegal attribute type: ~S for ~S of ~S ~
               ~&Legal attribute types are: ~&  ~S"
              type-keyword prop-id concept-id (mapcar #'car *attribute-types*)))
    ;; when legal return type-string
    xsd-type-string))

#|
? (catch :error (get-xsd-attribute-type :g-year "has-year" "Z-Decree"))
"gYear"
? (catch :error (get-xsd-attribute-type :g-years "has-year" "Z-Decree"))
"illegal attribute type: :G-YEARS for \"has-year\" of \"Z-Decree\" 
Legal attribute types are: 
  (:STRING :BOOLEAN :DECIMAL :FLOAT :DOUBLE :DATE-TIME :TIME :DATE
   :G-YEAR-MONTH :G-YEAR :G-MONTH-DAY :G-DAY :G-MONTH :HEX-BINARY
   :BASE-64-BINARY :ANY-URI :NORMALIZED-STRING :TOKEN :LANGUAGE :NM-TOKEN :NAME
   :NC-NAME :INTEGER :NON-POSITIVE-INTEGER :NEGATIVE-INTEGER :LONG :INT :SHORT
   :BYTE :NON-NEGATIVE-INTEGER :UNSIGNED-LONG :UNSIGNED-INT :UNSIGNED-SHORT
   :UNSIGNED-BYTE :POSITIVE-INTEGER)"
|#
;;;--------------------------------------------------------------------- INDENT

(defun indent (col text)
  "add col spaces in front of the text. In case of error returns text.
Arguments:
   col: integer
   text: text to indent (string)
Return:
   the string with col space in front of it."
  (unless (and (numberp col)(stringp text))
    (return-from indent text))
  (concatenate 'string (make-string col :initial-element '#\space) text))

;;;------------------------------------------------------------ IS-INSTANCE-OF?
;;; unused

(defun is-instance-of? (object class-list)
  "test whether the object is an instance of one of the classes or superclasses ~
      of the class list.
Arguments:
   object: any individual
   class-list: a list of class OWL Ids
Return:
   class of which the individual is a member or nil"
  (let ((class-id  (index-get object :inst :class-id))
        super-classes)
    ;; compute all superclasses of the classes of the object 
    (setq super-classes (compute-transitive-closure class-id :is-a))
    ;; see whether we have some classes in common with class-list
    (intersection super-classes class-list :test #'equal)))

#|
? (index)
                    
("Ville" ((:REL (:ID "hasTown")) (:CLASS (:ID "Z-City")))) 
("Z-City" ((:CLASS (:IS-A "ZTerritory") (:NAME :EN "City; town" :FR "ville; cité")))) 
("Name" ((:ATT (:ID "hasName")))) 
("Conurbation" ((:CLASS (:ID "Z-Conurbation" "Z-Conurbation")))) 
("Cité" ((:CLASS (:ID "Z-City")))) 
("Z-Territory" ((:CLASS (:NAME :EN "territory")))) 
("Z-Conurbation" ((:CLASS (:NAME :EN "Conurbation" :FR "conurbation")))) 
("City" ((:CLASS (:ID "Z-City")))) 
("Territory" ((:CLASS (:ID "Z-Territory")))) 
("Town" ((:REL (:ID "hasTown")) (:CLASS (:ID "Z-City")))) 
("Bordeaux" ((:INST (:CLASS-ID "Z-City")))) 
NIL
? (is-instance-of? "Bordeaux" '("Z-Territory"))
("Z-Territory")
? (is-instance-of? "Bordeaux" '("Z-City"))
("Z-City")
|#
;;;-------------------------------------------------------------------- IS-MLN?

(defun is-mln? (xx)
  "tests first for old format, then for new one"
  (or (mln::%mln? xx)(mln::mln? xx)))

#|
(is-mln? nil)
NIL

(is-mln? '(:name :en "Youpee" :fr "Bof"))
T

(is-mln? '((:en "Youpee")(:fr "Bof")))
T
|#
;;;------------------------------------------------------------ IS-SUBCLASS-OF?

(defun is-subclass-of? (class-name super-name)
  "checks if a class is a subclass of another one using the index table. If ~
   equal result is true.
Arguments:
   class-name: string (can be \"Z-Class\") or MLN
   super-name: id.
Return:
   nil or something not nil"
  (cond
   ((equal+ class-name super-name))
   ((let (class-id super-id)
      (setq class-id 
            (cond
             ((index-get class-name :class :name) class-name)
             ((stringp class-name) 
              (make-concept-id (list *language* class-name)))
             ((is-mln? class-name)
              (make-concept-id class-name))
             (t class-name)))
      (setq super-id 
            (cond
             ((index-get super-name :class :name) super-name)
             ((stringp super-name)
              (make-concept-id (list *language* super-name)))
             ((is-mln? super-name)
              (make-concept-id super-name))
             (t super-name)))
      ;; convert to internal id
      (member super-id (compute-transitive-closure class-id :is-a)
              :test #'equal+)))))
  
  #|
(is-subclass-of? "Student" '(:en "Person" :fr "Personne"))
("Z-Person")

(is-subclass-of? "Person" '(:en "Person" :fr "Personne"))
T

(is-subclass-of? "Student" "Person")
("Z-Person")

(is-subclass-of? "father" "Person")
|#
;;;------------------------------------------------------------- MAKE-ATTRIBUTE
;;; We reserve make-attribute to be the function called by defattribute
;;; attributes are built by process-attributes

;;;+--------------------------------------------------------- MAKE-ATTRIBUTE-ID

(defun make-attribute-id (name-list &optional class-name)
  "get a multilingual list and build an anttribute id, e.g. \"$hasSex\".
   To do so, uses the first name of the :en option, or if there is no :en option ~
      use the first available name, building a hybrid name!
Arguments:
   name-list: a multi-lingual name list
   class-name (opt): e.g. \"Z-Ville\"
Return:
   a string that is the attribute ID."
  (declare (special *language*)) 
  (unless (is-mln? name-list) 
    (terror "attribute name should be an MLN, not: ~S" name-list))
  
  (concatenate 'string "has" (or class-name "")
    (make-index-string (mln::get-canonical-name name-list *language*))))

#|
(let((*language* :en))
  (MAKE-ATTRIBUTE-ID '(:IT "sexo" :EN " sex ; genre" :FR "Sexe; Genre")))
"hasSex"

(catch :error
    (MAKE-ATTRIBUTE-ID '(:IT "sexo" :EZ " sex ; genre" :FR "Sexe; Genre" :PL "?")))
"bad name-list for making attribute-id:
   (:IT \"sexo\" :EZ \" sex ; genre\" :FR \"Sexe; Genre\" :PL \"?\")"

(let((*language* :fr))
  (MAKE-ATTRIBUTE-ID '(:IT "sexo" :FR "Sexe; Genre" )))
"hasSexe"

(let((*language* :en))
  (MAKE-ATTRIBUTE-ID '(:IT "sexo" :EN " sex ; genre" :FR "Sexe; Genre") "Person"))
"hasPersonSex"

(let((*language* :fr))
  (MAKE-ATTRIBUTE-ID '(:IT "sexo" :FR "Sexe; Genre" )"Personne"))
"hasPersonneSexe"

(let((*language* :fr))
  (catch :error
         (MAKE-ATTRIBUTE-ID "Genre" "Personne")))
"attribute name should be an MLN, not: \"Genre\""
|#
;;;---------------------------------------------- MAKE-CARDINALITY-RESTRICIONS

(defun make-cardinality-restrictions (option-list)
  "builds OWL definitions corresponding to :max :min and :unique options.
Arguments:
   prop-id: OWL id or attribute or relation
   option-list: list of options (may  no contain cardinality restrictions
   value-list
Return:
   an OWL string possibly empty"
  (let (result nn)
  (cond
   ((setq nn (cadr (assoc :min option-list)))
    (cformat 
     "<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:minCardinality>"
     nn))
   ((setq nn (cadr (assoc :max option-list)))
    (cformat 
     "<owl:maxCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:minCardinality>"
     nn))
   ((assoc :unique option-list)
    (cformat 
     "<owl:Cardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:minCardinality>"
     )))
   result))
      
#|
(make-cardinality-restrictions '((:entry)(:min 2)(:var _jp)))
("<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">2</owl:minCardinality>")

(make-cardinality-restrictions '((:entry)(:var _jp)(:unique)))
("<owl:Cardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:minCardinality>")

(make-cardinality-restrictions '((:entry)(:var _jp)))
NIL
|#
;;;--------------------------------------------------------------- MAKE-CHAPTER

(defun make-chapter (&rest ll)
  "used to ignore the corresponding macro in the input text"
  (declare (ignore ll))
  nil)

;;;----------------------------------------------------------------- MAKE-CLASS

(defun make-class (class-list)
  "builds the list of classes for the concept.
Arguments:
   class-list: if string, then only one super-class otherwise list
Return:
   list of OWL specs
Error:
   throws to :error tag."
  ;; check syntax
  (cond
   ((listp class-list))
   ((stringp class-list) (setq class-list (list class-list)))
   (t (terror "bad format for :is-a option: ~S" class-list)))
  (let (index id result)
    ;; process all classes
    (dolist (item class-list)
      ;; build index from entry
      (setq index (make-index-string item))
      ;; check for existence using the *index* table
      (setq id (car (index-get index :class :id)))
      ;; if nil error
      (unless id
        (terror "unknown class: ~S in ~S" item class-list))
      ;; otherwise
      (cformat  "<rdfs:domain rdf:resource=\"#~A\"/>" id))
    ;; return spec list
    result))

#|
? (index)
(...
("Person" ((:IDX (:EN "Z-Person")) (:CLASS (:ID "Z-Person"))))
...
("Z-Person" ((:CLASS (:NAME :EN "Person" :FR "personne" :IT "Persona"))))
...)

? (make-class "Person")
("<rdfs:domain rdf:resource=\"#Z-Person\"/>")
|#
;;;+-------------------------------------------------------------- MAKE-CONCEPT

(defun make-concept (concept-name &rest option-list 
                                  &aux message result index-specs)
  "produces a list of strings corresponding to the OWL format for defining the ~
      concept.
   This function is called twice: once during the first pass, another time ~
      during the second pass.
Arguments:
   concept-name: a multi-lingual :name list
   option-list (rest): a possible sequence of SOL options
Return:
   a list of strings corresponding to the OWL format."
  ;(vformat "~2%;========== Entering make-concept")
  ;(format  t "~%;--- *language*: ~S" *language*)
  (vformat "~&===== ~S" (make-concept-id concept-name))
  (cond 
   ;;=== first compiler pass, we collect classes and build index entries
   ((eql *compiler-pass* 1)
    (let (id)
      (setq 
       message
        (catch 
         :error
         ;; rough check of the name input format
         (unless (is-mln? concept-name)
           (terror "Bad concept name format: ~S" concept-name))
         ;; normalize concept name according to new format
         (setq concept-name (mln::make-mln concept-name))
         ;; we use the :en label whenever possible, otherwise the first of the list
         ;;     here the id should be "Z-Person"
         ;; if an error, throws to :error
         (setq id (make-concept-id concept-name)) ; id is actually a string
         ;; check for existing id
         (if (index-get id)
             (terror "Internal ID ~S already exists. Can't use it for concept." id))
         
         ;; save it into the concept list
         (push id *concept-list*)
         ;; save into index  "Z-Class" ((:CLASS (:NAME <concept-name>)))
         (%index-set id :class :name concept-name)
         ;; update index entries
         (make-indices concept-name id :class)
         ;; debug print
         ;(mapcar #'(lambda (xx) (terpri) (princ xx))  result)
         ;(print `(+++ att list ,*attribute-list*))
         ))
      
      ;; is message is a string then we have an error otherwise "todo bem"
      (if (stringp message)
          (twarn "~&;***** Error in:~S; ~A" concept-name message)
        ;; otherwise print result
        (unless *errors-only* (rp result))))
    )
   ;;=== second compiler pass
   ((eql *compiler-pass* 2)
    (let (id att-id rel-id)
      (setq 
       message
        (catch 
         :error
         ;; build id again, no check, done during the first pass
         (setq id (make-concept-id concept-name))
         ;; verify that id is part of the *concept-list*, otherwise error
         (unless (member id *concept-list* :test #'equal)
           (terror "concept id ~S disappeared from the concept list." id))
         
         ;; build the OWL id line
         (cformat "<owl:Class rdf:ID=~S>" id)
         (rtab) ; indent
         ;; now process labels 
         (+result (make-labels concept-name))
         ;(format t "~%;--- result 1: ~S" result)
         
         ;; process now options of the concept definition
         (dolist (option option-list)
           ;(format t "~%;--- option: ~S" option)
           (cond
            ;;== first take care of subclass
            ((eql (car option) :is-a)
             (+result (make-subclass (cdr option) id)) ; JPB 051029
             ;(format t "~%;--- result 2: ~S" result)
             )
            ;;== then of documentation
            ((eql (car option) :doc)
             (+result (make-doc (cdr option)))
             ;(format t "~%;--- result 3: ~S" result)
             )
            ;;== then attributes
            ((eql (car option) :att)
             (setq att-id (make-attribute-id (cadr option)(subseq id 2)))
             ;; add indices to the index table, e.g.
             (make-property-index (cadr option) :att)
             (make-indices 
              (cadr option) ; att name (mln, possibly string)
              att-id ; e.g. "hasPersonSex"
              id)
             ;; add info
             (push (cons id (cdr option)) *attribute-list*)
             )
            ;;== then relations
            ((eql (car option) :rel)
             (setq rel-id (make-relation-id (cadr option) (subseq id 2)))
             ;; add indices to the index table
             (make-property-index (cadr option) :rel)
             (make-indices (cadr option) rel-id id)
             (push (cons id (cdr option)) *relation-list*)
             ;; include here inverse relations (not enough)
             ;(push (make-inverse-relation id option) *relation-list*)
             )
            ;;== otherwise error
            (t (terror "unknown option ~S while defining ~S" option concept-name))
            )
           )
         (decf *left-margin* *indent-amount*)
         ;; closes the class definition
         (cformat "</owl:Class>")
         (push " " result)
         (+result index-specs)
         ;; debug print
         ;(mapcar #'(lambda (xx) (terpri) (princ xx)) (reverse result))
         ;; return result
         result))
      ;; is message is a string then we have an error otherwise todo bem
      (if (stringp message)
          (twarn "~&;***** Error in:~S; ~A" concept-name message)
        ;; otherwise print result
        (unless *errors-only* (rp result)))
      ;(format t "~%;--- result ~S" result)
      ))
   ))


#|
? (index-clear)
T
? (let ((*compiler-pass* 1))
    (make-concept '(:name :en "time unit")))

<$Index rdf:ID="TimeUnit">
<isEnIndexOf rdf:resource="ZTimeUnit"/>
</$Index>

:END
? (let ((*compiler-pass* 1)
        (*error-message-list* nil))
    (make-concept '(:name :en "time unit")))
NIL

? (let ((*compiler-pass* 2))
    (make-concept '(:name :en "time unit")))

<owl:Class rdf:ID="ZTimeUnit">
<rdfs:label xml:lan="en">time unit</rdfs:label>
</owl:Class>

:END

? (index-clear)
T
? (progn 
    (print (setq *compiler-pass* 1))
    (defconcept (:en "gender type")
      (:one-of (:fr "masculin" :en "male")
               (:en "female" :fr "féminin")))
    (print (setq *compiler-pass* 2))
    (defconcept (:en "gender type")
      (:one-of (:fr "masculin" :en "male")
               (:en "female" :fr "féminin"))))

1 
2 
<owl:Class rdf:ID="Z-GenderType">
<rdfs:label xml:lang="en">gender type</rdfs:label>
<owl:equivalentClass>
<owl:Class>
<owl:oneOf rdf:parseType="Collection">
<Z-GenderType rdf:ID="z-male">
<rdfs:label xml:lang="fr">masculin</rdfs:label>
<rdfs:label xml:lang="en">male</rdfs:label>
</Z-GenderType>
<Z-GenderType rdf:ID="z-female">
<rdfs:label xml:lang="en">female</rdfs:label>
<rdfs:label xml:lang="fr">féminin</rdfs:label>
</Z-GenderType>
</owl:oneOf>
</owl:Class>
</owl:equivalentClass>
</owl:Class>

:END
? (index)

("Female" ((:IDX (:EN "z-female")) (:INST (:ID "z-female")))) 
("Féminin" ((:IDX (:FR "z-female")) (:INST (:ID "z-female")))) 
("GenderType"
 ((:IDX (:EN "Z-GenderType")) (:CLASS (:ID "Z-GenderType")))) 
("Male" ((:IDX (:EN "z-male")) (:INST (:ID "z-male")))) 
("Masculin" ((:IDX (:FR "z-male")) (:INST (:ID "z-male")))) 
("Z-GenderType" ((:CLASS (:NAME :EN "gender type")))) 
("z-female" ((:INST (:CLASS-ID "Z-GenderType")))) 
("z-male" ((:INST (:CLASS-ID "Z-GenderType")))) 
:END
|#
;;;+----------------------------------------------------------- MAKE-CONCEPT-ID

(defun make-concept-id (name-option)
  "Parses the input, e.g. ({:name} :en \"person; human\" :fr \"personne\")
   Tries to obtain the English tag, if not take the first of the list
   Takes all names and capitalizes the first letter of each word.
Arguments:
   name-option: a list containing language tags.
Returns:
   a stringp
Error:
   error if bad syntax"
  (declare (special *language*))
  ;(format t "~%;--- language: ~S" *language*)
  ;(format t "~%; make-concept-id/ *language*: ~S" *language*)
  (unless (or (mln::mln? name-option)(mln::%mln? name-option))
    ;(terror "Bad syntax of name-option ~S." name-option))
    (error "Bad syntax of name-option ~S." name-option))
  (make-concept-string (mln::get-canonical-name name-option *language*)))

#|
? (make-concept-id '(:name :en "person" :fr "personne"))
"Z-Person"

? (let ((*language* :fr))
  (make-concept-id '(:name :en "person; man" :fr "personne; gus")))
"Z-Personne"

? (let ((*language* :en))
  (make-concept-id '(:fr "personne")))
"Z-Personne"

? (make-concept-id '(:en "person"))
"Z-Person"

? (catch :error (make-concept-id '(:en :fr "person")))
Error: Bad syntax of name-option (:EN :FR "person").

? (make-concept-id '(:en "person   of DUTY "))
"Z-PersonOfDuty"

;; new MLN format
? (let ((*language* :fr))
  (make-concept-id '((:en "person" "man") (:fr "personne" "gus"))))
"Z-Personne"
|#
;;;-------------------------------------------------------- MAKE-CONCEPT-STRING

(defun make-concept-string (name-string)
  "makes a concept string from a user provided string.
Argument:
   name-string: string 
Return:
   concept-id string."
  (apply #'concatenate 'string "Z-" 
         (make-text-list (make-value-string name-string))))

#|
? (MAKE-CONCEPT-STRING "Time Unit")
"Z-TimeUnit"
|#
;;;--------------------------------------------------------- MAKE-STANDARD-NAME

(defun make-standard-name (name-string)
  "makes a concept name from a user provided string.
Argument:
   name-string: string 
Return:
   concept-id string."
  (apply #'concatenate 'string  
         (make-text-list (make-value-string name-string))))

#|
(MAKE-standard-name "Time Unit")
"TimeUnit"
|#
;;;------------------------------------------------------------------- MAKE-DOC

(defun make-doc (doc-spec &aux result doc-text)
  "we build doc options from the list of doc strings.
Arguments:
   concept-name: a list like (:en \"...\" :fr \"...\")
Return:
   a list of OWL comment specifications."
  (declare (special *language*))
  (unless (listp doc-spec)
    (terror "bad doc-list list: ~S" doc-spec))
  ;;
  (setq doc-text (mln::get-canonical-name doc-spec *language*))
  (when doc-text
    (cformat "<rdfs:comment xml:lang=~S>~?</rdfs:comment>"
             (string-downcase (symbol-name *language*))
             doc-text ())))

#|
(let ((*language* :FR))
    (make-doc '(:en "English doc" :fr "French doc" :it "Italian doc")))
("<rdfs:comment xml:lang=\"fr\">French doc</rdfs:comment>")
|#
;;;--------------------------------------------------------------- MAKE-DOMAINS

(defun make-domains (domain-id-list &aux result)
  "build a list of OWL specs for domais.
Arguments;
   domain-id-list: a list of OWL domain IDs
Return:
   a list of OWL strings."
  (cond
   ;; when more than one domain we must use the union option
   ((cdr domain-id-list)
    (cformat "<rdfs:domain>")
    (rtab)
    (cformat "<owl:Class>")
    (rtab)
    (cformat "<owl:unionOf rdf:parseType=\"Collection\">")
    (rtab)
    (dolist (id domain-id-list)
      (cformat "<owl:Class rdf:about=\"#~A\"/>" id))
    (ltab)
    (cformat "</owl:unionOf>")
    (ltab)
    (cformat "</owl:Class>")
    (ltab)
    (cformat "</rdfs:domain>")
    result)
   ;; otherwise a single domain
   (t (cformat "<rdfs:domain rdf:resource=\"#~A\"/>" (car domain-id-list)))))

#|
SOL(48): (make-domains '("Z-Person" "Z-Child" "Z-Wife"))
("  </rdfs:domain>" "    </owl:Class>" "      </owl:unionOf>"
 "        <owl:Class rdf:about=\"#Z-Wife\"/>"
 "        <owl:Class rdf:about=\"#Z-Child\"/>"
 "        <owl:Class rdf:about=\"#Z-Person\"/>"
 "      <owl:unionOf rdf:parseType=\"Collection\">" "    <owl:Class>"
 "  <rdfs:domain>")
SOL(49): (rp *)
<rdfs:domain>
<owl:Class>
<owl:unionOf rdf:parseType="Collection">
<owl:Class rdf:about="#Z-Person"/>
<owl:Class rdf:about="#Z-Child"/>
<owl:Class rdf:about="#Z-Wife"/>
</owl:unionOf>
</owl:Class>
</rdfs:domain>
:END
? (make-domains '("Z-Person" ))
("<rdfs:domain rdf:resource=\"#Z-Person\"/>")
? (rp *)

<rdfs:domain rdf:resource="#Z-Person"/>
:END
|#
;;;---------------------------------------------------------------- MAKE-HEADER

(defun make-header ()
  "fake function to printout a header (copied from Protégé output)"
  (format *output* 
      "<?xml version=\"1.0\" ~A?>
<!DOCTYPE owl [
     <!ENTITY owl  \"http://www.w3.org/2002/07/owl#\" >
     <!ENTITY xsd  \"http://www.w3.org/2001/XMLSchema#\" >
     <!ENTITY terregov \"http://www.terregov.eupn.net/ontology/2005/10/terregov.owl/terregov#\">
   ]>

<rdf:RDF
  xmlns:owl = \"http://www.w3.org/2002/07/owl#\"
  xmlns:rdf = \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
  xmlns:rdfs= \"http://www.w3.org/2000/01/rdf-schema#\"
  xmlns:xsd = \"http://www.w3.org/2001/XMLSchema#\"
  xmlns=\"http://www.owl-ontologies.com/TerreGov.owl#\"
  xml:base=\"http://www.owl-ontologies.com/TerreGov.owl\">"
    (if (eql *encoding* :utf8) "encoding=\"UTF-8\"" 
      "encoding=\"LATIN-1\""))
  (format *output* "
  <owl:FunctionalProperty rdf:ID=\"indexing\">
    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#AnnotationProperty\"/>
    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#DatatypeProperty\"/>  
    <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#boolean\"/>
    <rdfs:domain rdf:resource=\"http://www.w3.org/2002/07/owl#Restriction\"/>
    <rdfs:comment xml:lang=\"en\">Default: rdfs:label allows the indexing of any ontology element (Concept, Property, Individual).
Indexing is a boolean annotation property having for domain Datatype Properties. Such a property having
true as the value of indexing, allows each instance of its domain to be indexed by the value of the property.</rdfs:comment>
  </owl:FunctionalProperty>

")
  (rtab))

;;;--------------------------------------------------------- MAKE-INDEX-ENTRIES

(defun make-index-entries (ind-id class-id ind-options)
  "takes the description of the options of an individual and for each value of ~
   an attribute having an entry option, adds an entry to the index table and ~
   to the *index-list*
Arguments:
   ind-id: id of the individual, e.g. \"z-ind-33\"
   class-id: the normalized individual class, e.g. \"Z-Ville\"
   ind-option: e.g. ((\"nom\" (:EN \"France\" :FR \"France; Gaule\"))
                     (:VAR SOL-TEST5::_FR))
Return:
   the list of entries
Side-effects:
   updates index table and *index-list*"
  ;; must normalize class-id and get the right one from the index table
  (let (val pair att-id index-string)
    ;; loop for each option of the list
    (dolist (option ind-options)
      ;(format t "~%; make-index-entries /option: ~S" option)
      ;; get the normalized name of the option: (car option) is a string
      ;; need to normalize for hash function!
      ;(setq att-id (index-get (make-index-string (car option)) class-id :id))
      (setq att-id (index-get (make-index-string (car option)) :att :id))
      ;(format t "~%; make-index-entries /att-id: ~S" att-id)
      ;; check if attribute has entries by looking into the attribute list
      (when att-id
        (dolist (att-desc *attribute-list*)
          ;(format t "~%; make-index-entries /option: ~S" option)
          ;(format t "~%; make-index-entries /att-desc ~S" att-desc)
          ;; att-list is indexed by concepts
          (when (and (is-subclass-of? class-id (car att-desc)) ; same class
                     (equal+ (car option)(cadr att-desc)) ; same attribute
                     (or (assoc :index (cdr att-desc))
                         (assoc :entry (cdr att-desc))))
            ;(format t "~%; make-index-entries /entry, att-id: ~S" att-id)
            ;; make entry from the values, e.g. "MargnyLesCompiègne"
            ;; add entries to the index table
            ;; add entries to *index-list*
            (setq val (if (is-mln? (cadr option)) (cadr option) (cdr option)))
            (make-indices val ind-id :inst)
            ;(make-indices val :att)
            )))
      ;; individual can also be indexed by the :var option
      (setq pair (assoc :var ind-options))
      ;; second arg of make-indices must be a string 
      (when pair
        (setq index-string (make-index-string (cadr pair)))
        (index-add index-string :inst :id ind-id)
        (index-add index-string :idx *language* ind-id)
        )
;;;      (when pair        
;;;        (make-indices (make-index-string (cadr pair)) ind-id :inst))
      ;(list *language* (format nil "~A" (cadr pair))) ind-id :inst))
      )
    :done))
  
#|
(make-index-entries "z-ind-61" "Z-Pays" 
                    '(("nom" (:en "France" :fr "France; Gaule"))(:var _fr)))

:DONE

index
("France" ((:IDX (:FR "z-ind-61") (:EN "z-ind-61")) (:INST (:ID "z-ind-61")))) 
("Gaule" ((:IDX (:FR "z-ind-61")) (:INST (:ID "z-ind-61"))))
("_Fr" ((:IDX (:FR "z-ind-61")) (:INST (:ID "z-ind-61"))))

*index-list*
("Gaule" "France" "Cité" "Ville" "Pays")

(index-get "_Fr" :inst :id)
("z-ind-71")
|#
;;;---------------------------------------------------------- MAKE-INDEX-STRING

(defun make-index-string (name-string)
  "makes a concept string from a user provided string, e.g. \"time unit\".
Argument:
   name-string: string 
Return:
   concept-id string."
  (apply #'concatenate 'string  
         (make-text-list (make-value-string name-string))))

#|
? (make-index-STRING "Time Unit")
"TimeUnit"

(make-index-STRING 'moss::_var)
"_Var"
|#
;;;--------------------------------------------------------------- MAKE-INDICES
;;; must make indexes for all languages, since in the original ontology the
;;; range of a property can be specified with any  language (same for property
;;; names in defindividual)

(defun make-indices (name-option value-string data-type)
  "takes a name option and builds all possible indices from it
   E.g. ({:name} :en \"man of war ; ship\" :fr \"navire\")
   will produce the following entries: ManOfWar, Ship, Navire
   If name-option is a string, then uses the *language* global variable.
Arguments:
   name-option: a multi-lingual list, e.g. ((:en \"man\" \"bloke\")(:fr \"gus\"))
                or a string in which case language is current language
                or a list of strings
   value-string: string representing the concept id or property id, e.g. \"Z-Country\"
   data-type: :class :att :rel or :inst
Return:
   :done
Side-effects:
   updates the index table
Error:
   throws to an :error tag."
  (declare (special *index-list* *language*))
  (let ((language *LANGUAGE*)
        index-list index-string all-languages)
    
    ;; turn name-option into an mln
    (unless (is-mln? name-option)
      (setq name-option 
            (cond
             ((stringp name-option) (list language name-option))
             ((listp name-option) (list (cons language name-option)))
             (t (terror "Can't make index entries with ~S" name-option)))))
             
    ;; get all languages
    (setq all-languages  (mln::get-languages name-option))

    ;; for each language build an entry in the index table
    (dolist (tag all-languages)
      ;; extract all synonyms for all languages
      (setq index-list (mln::filter-language name-option tag))
      ;; for each name build an index object
      (dolist (index index-list)
        ;; cook up index string, e.g. "SoupeDuJour"
        (setq index-string (make-index-string index))
        ;; insert entry into *index* hash table
        (index-add index-string data-type :id value-string)
        (index-add index-string :idx tag value-string)
        ;; what is index list for anyway?
        (if (eql tag language)
            (pushnew index-string *index-list* :test #'string-equal))
        )
      )
    ;; return (normally nil)
    nil
    ))

#|
(let ((*language* :fr))
  (index-clear)
  (make-indices 
   '((:en "first name" "given name") (:fr "prénom")) "hasPrénom" :att)
  (index))
("FirstName" ((:IDX (:EN "hasPrénom")) (:ATT (:ID "hasPrénom")))) 
("GivenName" ((:IDX (:EN "hasPrénom")) (:ATT (:ID "hasPrénom")))) 
("Prénom" ((:IDX (:FR "hasPrénom")) (:ATT (:ID "hasPrénom")))) 
:END

(let ((*language* :fr))
  (index-clear)
  (make-indices 
   "Albert" "z-ind-23" :inst)
  (index))
("Albert" ((:IDX (:FR "z-ind-23")) (:INST (:ID "z-ind-23")))) 
:END

(let ((*language* :fr))
  (index-clear)
  (make-indices 
   '("France" "Italie") "z-ind-33" :inst)
  (index))
("France" ((:IDX (:FR "z-ind-33")) (:INST (:ID "z-ind-33")))) 
("Italie" ((:IDX (:FR "z-ind-33")) (:INST (:ID "z-ind-33"))))
|#
;;;------------------------------------------------------------ MAKE-INDIVIDUAL
#|
Many languages have a so-called "unique names" assumption: different names refer
to different things in the world. On the web, such an assumption is not possible.
For example, the same person could be referred to in many different ways 
(i.e. with different URI references). For this reason OWL does not make this 
assumption. Unless an explicit statement is being made that two URI references
refer to the same or to different individuals, OWL tools should in principle 
assume either situation is possible.

OWL provides three constructs for stating facts about the identity of individuals:

owl:sameAs is used to state that two URI references refer to the same individual.
owl:differentFrom is used to state that two URI references refer to different 
individuals
owl:AllDifferent provides an idiom for stating that a list of individuals are 
all different

Inside the same site, names of individuals should be unique. I.e. "Martin" must
not refer to two different objects. However, utc:#Martin and puc:#Martin could
be different objects.

Moss objects have no meaningful ids. Hence an object id like $E-PERSON.23 will
be translated into something like Ind-47, the latter being unique for this site.

MOSS object may have a name attribute. The name can have multiple values. If the
name attribute is an index then entry point will be produced to refer to this
object. However, indexes are not unique, i.e. can point to different objects.
This is not true for the :var option for which the value must be locally unique.

PROBLEM
Individuals are not created before make-attributes and make-relations are called
Because MOSS uses entry points to refer to objects, such entry points do not exist
at that time.
Now, a relation can have a :one-of option with a reference to an object, say by
using the :new construct, or by an entry point, or by a variable. Thus, 
make-individual should create the ad hoc entries in the index table for letting
the system make a reference to objects to be created later...
SO(89): must add  ("France" ((:IDX (:INST "z-ind-34"))))
SO(89):           ("Gaule"  ((:IDX (:INST "z-ind-34"))))
SO(89): but also take into account variables from :var option
SO(89):           (_fr ((IDX (:INST "z-ind-34"))))
SO(89): so that :one-of relation can link the objects
|#
;;; In MOSS ontologies individuals have no name option (:name <ML>), therefore 
;;; we make a synthetic name IND-nnn to use the old SOL code

;;; Q. because normal MOSS objects have no names, a name can be that of its
;;; internal id, e.g. z-ind-34. It can also be the value attached to the option
;;; :var (not language dependent, used to make links). It can be any value
;;; appearing associated with an attribute having the option :index or :entry
;;; in which case OWL looses the reference to the property.
;;; Q. when an attribute has a multiple value, how does OWL take that into account
;;; Q. for MLN, we keep the value(s) corresponding to the language.

(defun make-individual (concept-entry &rest option-list)
  "produces the OWL string for representing the individual.
  This function is called twice: once during the second pass, another time by ~
   process-individual.
Arguments:
   concept-entry: an entry string that should be the name of a concept
   option-list: a possible sequence of options
Return:
   a list of strings corresponding to the OWL format."
  (declare (special *language* *compiler-pass* *individual-list*))
  ;; only works during the second compiler pass (needs concept names)

  (when (eql *compiler-pass* 2)
    
    ;(break "make-individual")
    
    ;; first compiler pass, we collect classes and build index entries
    (let* ((individual-name (symbol-name (gentemp "ind-")))
           (id (concatenate 'string "z-" individual-name))
           class-id message result)
      ;(vformat "~&===== ~S ~S" id option-list)
      
      (setq 
       message
        (catch 
         :error    
         ;; concept entry should be a string
         (unless (stringp concept-entry)
           (terror "~&;***** While creating individual ~S: ~&; concept-entry ~
                       should be a string rather than: ~S" 
                   individual-name concept-entry))
         
         ;; get the class-id from the concept-entry
         (setq class-id (car (index-get (make-index-string concept-entry) 
                                        :class :id)))
         ;; check first existence of corresponding concept/class
         (unless class-id (terror "non existing-class ~S" concept-entry))
         
         ;; otherwise make a new entry in the individual list
         ;; e.g. ("z-ind-23" "Z-City" (:fr "ind-23") (<option>*))
         ;; for upward compatibility individual-name should be MLN
         (push (list* id class-id (list *language* individual-name) option-list)
               *individual-list*)
         ;; save entries; make-indices requires an MLN, cook up one
         
         ;(+result (make-indices (list *language* individual-name) id :inst))
         (index-add id :inst :class-id class-id)
         result))
      
      ;; build entries in the index table from the attributes of the individual
      ;; class-id is the normalized id taken from the index table
      
      ;(break "make-individual 1")
      
      (make-index-entries id class-id option-list)
      
      ;(break "make-individual 2")
      
      ;; if message is a string then we have an error, otherwise todo bem
      ;(print message)
      (when (stringp message)
        (twarn "~&;***** While creating individual ~S: ~&~A" 
               individual-name message)
        (return-from make-individual nil)
        )
      (unless *errors-only* (rp result))
      result)))

#|
? (let ((*compiler-pass* 2))
    (make-individual "city" '(:fr "Florence" :it "Firenze")))
NIL
? (let ((*compiler-pass* 2))
    (make-individual "city" '(:fr "Florence" :it "Firenze")))
NIL
? (errors)

;***** While creating individual (:FR \"Florence\" :IT \"Firenze\"): 
Another individual with same key "z-florence" already exists.
;***** While creating individual (:NAME :FR "pyrénées atlantiques"): 
non existing-class "département"
:END
|#
;;;--------------------------------------------------------- MAKE-INDIVIDUAL-ID

(defun make-individual-id (name-option)
  "Parses the input, e.g. ({:name} :en \"person; human\" :fr \"personne\")
Tries to obtain the English tag, if not take the first of the list
Takes all names and capitalizes the first letter of each word.
Arguments:
   name-option: a list containing language tags.
Returns:
   a string
Error:
   throws to :error if bad syntax"
  (when (or (mln::%mln? name-option)(mln::mln? name-option))
    (setq name-option (mln::get-canonical-name name-option *language*)))
  (concatenate 'string "z-" name-option))

#|
? (make-individual-id '(:en "male une fois  ; gentleman" :fr "mec"))
"z-maleUneFois"
? (make-individual-id '(:name :it "uomo" :en "male une fois  ; gentleman" :fr "mec"))
"z-maleUneFois"
? (make-individual-id '(:it "uomo" :fr "mec"))
"z-uomo"
|#
;;;----------------------------------------------------- MAKE-INDIVIDUAL-STRING
;;; still used by sol2html

(defun make-individual-string (name-string)
  "makes an individual string from a user provided string.
Argument:
   name-string: string 
Return:
   concept-id string."
  (let* ((word-list (make-text-list (make-value-string name-string)))
         (first-word (string-downcase (car word-list))))
    (apply #'concatenate 'string "z-" first-word (cdr word-list))))

#|
? (make-individual-string  "male")
"z-male"
? (make-individual-string  "MALE")
"z-male"
? (make-individual-string  "MALE  de jour   ")
"z-maleDeJour"
|#
;;;+-------------------------------------------------- make-inverse-relation-id

;;;(defun make-inverse-relation-id (name-list &aux name-string)
;;;  "get a multilingual list and build an inverse relation id, e.g. \"$isBrotherOf\".
;;;   To do so, uses the first name of the :en option, or if there is no :en option ~
;;;      use the first available name, building a hybrid name!
;;;Arguments:
;;;   name-list: a multi-lingual name list
;;;Return:
;;;   a string that is the attribute ID."
;;;  (declare (special *language*))
;;;  (unless name-list 
;;;    (terror "bad name-list for making relation-id"))
;;;  
;;;  (setq name-string (mln::get-canonical-name name-list *language*))
;;;  (concatenate 'string "is" (make-index-string name-string) "Of"))

#|
(let ((*language* :fr))
  (make-inverse-relation-id 
   '(:fr "propriété inverse" :en "inverse property; inv prop")))
"isPropriétéInverseOf"
|#
;;;---------------------------------------------------------------- MAKE-LABELS

(defun make-labels (concept-name &aux result)
  "we build label options from the list of concept names for *language*.
Arguments:
   concept-name: a list like ((:en \"person\") (:fr \"personne\"))
Return:
   a list of OWL label specifications."
  (declare (special *language*))
  ;; take care of old format
  (if (mln::%mln? concept-name)
      (setq concept-name (mln::make-mln concept-name)))
  (unless (mln::mln? concept-name)
    (terror "bad concept-name format: ~S" concept-name))
  
  (dolist (label (mln::filter-language concept-name *language*))
    ;; build OWL spec
    (cformat  "<rdfs:label xml:lang=~S>~A</rdfs:label>" 
             (string-downcase (symbol-name *language*))
             (string-trim '(#\space) label)))
  result)

#|
(let ((*language* :fr))
  (MAKE-LABELS '((:en "person" "human") (:fr "personne" "mec"))))
("<rdfs:label xml:lang=\"fr\">mec</rdfs:label>"
 "<rdfs:label xml:lang=\"fr\">personne</rdfs:label>")

(let ((*language* :en))
  (so::make-labels '(:NAME :EN "Territory" :FR "Territoire" :IT "territorio")))
("<rdfs:label xml:lang=\"en\">Territory</rdfs:label>")
|#
;;;-------------------------------------------------------------- MAKE-ONTOLOGY

(defun make-ontology (&rest options &aux result) 
  "builds the ontology header. Options are:
   :name, :version, :xmlns, :base, :imports
Arguments:
   options: list of options use Protégé header 
Return:
   noting interesting."
  (declare (special *rule-output*))
  (when (eql *compiler-pass* 1)  ; do that only once
    ;; first print that stuff (push into result)
    (cformat
     "<?xml version=\"1.0\" ~A?>"
     "encoding=\"UTF-8\"")
    
    ;; extract from the defontology options the name space to be used in the 
    ;; rule file
    (when *rule-output*
      (set-rule-name-space options)
      ;; get user-defined operators associated with the option :user-operators
      (apply #'make-user-defined-operator-list options)
      ;; produces the necessary header for the rules file
      (apply #'make-rule-header options))
    
    ;; now take care of the DOCTYPE section if any
    (dolist (option options)
      (case (pop option)
        (:doctype
         ;; for each option build entry
         (cformat "~%<!DOCTYPE owl [")
         (loop
           (unless option (return nil))
           (cformat "~&  <!ENTITY ~A~20T~S>" 
                    (pop option) (pop option)))
         (cformat "~&]>"))))
    ;;;     <!ENTITY owl  \"http://www.w3.org/2002/07/owl#\" >
    ;;;     <!ENTITY xsd  \"http://www.w3.org/2001/XMLSchema#\" >
    ;;;     <!ENTITY terregov \"http://www.terregov.eupn.net/ontology/2005/10/terregov.owl\" >
    ;;;   ]>
    
    ;; now introduce real body
    (cformat "~2&<rdf:RDF")
    
    (let (tag)
      (cformat "  xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"")
      (cformat "  xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"")
      (cformat "  xmlns:owl=\"http://www.w3.org/2002/07/owl#\"")
      (cformat "  xmlns:xsd=\"&xsd;\"")
 ; xmlns="&terregov;"
 ; xmlns:terregov="&terregov;"

      ;; then process each option
      (dolist (option options)
        (case (setq tag (pop option))
          (:title
           (cformat "  xmlns=\"&~A;\"" (car option))
           (cformat "  xmlns=~A=\"&~A\"" (car option)(car option))
           )
          (:xmlns
           ;; for each name space construct the right code
           (loop
             (unless option (return nil))
             (if (string-equal (car option) "self")
               (progn
                 (pop option)
                 (cformat "~&  xmlns=~S" (pop option)))
               (cformat "~&  xmlns:~A=~S" (pop option)(pop option)))))
          (:base
           (cformat "~&  xml:base=~S" (pop option)))))
      ;; close case before addressing imports
      (cformat " >~2%")
      (dolist (option options)
        (case (setq tag (pop option))
          (:imports
           ;; build import list
           (cformat "~&  <owl:Ontology rdf:about=\"\">")
           (cformat "~%<!--")
           (loop
             (unless option (return nil))
             (cformat "~&    <owl:imports rdf:resource=~S/>" (pop option)))
           (cformat "~%-->")
           (cformat "~&  </owl:Ontology>~2%"))
          ((:xmlns :base :doctype)
           ;; already processed
           )
          ;; simply options used elsewhere
          ((:name :version :operator-location :user-operators)) 
          ((:language :title)) ; ignore title yet
          (otherwise
           (error "bad options in defontology: ~S" tag))))
      ;; introduce index classes
      ;;+++++++++++++++++++++++++++ Declaration for Indexing ++++++++++++++++++++++++
      (rtab)
      (mark-section "Declaration for Indexing" *separation-width*)
      (cformat 
       "<owl:FunctionalProperty rdf:ID=\"indexing\">
       <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#AnnotationProperty\"/>
       <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#DatatypeProperty\"/>  
       <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#boolean\"/>
       <rdfs:domain rdf:resource=\"http://www.w3.org/2002/07/owl#Restriction\"/>
       <rdfs:comment xml:lang=\"en\">Default: rdfs:label allows the indexing of any ontolog element (Concept, Property, Individual).
Indexing is a boolean annotation property having for domain Datatype Properties. Such a property having
true as the value of indexing, allows each instance of its domain to be indexed by the value of the property.</rdfs:comment>
</owl:FunctionalProperty>

")
      (mark-section "End Declaration for Indexing" *separation-width*)
      ;;+++++++++++++++++++++++ End Declaration for Indexing ++++++++++++++++++++++++
      ;; Declarations for virtual categories (we introduce them even if there are no 
      ;; rules
      ;;++++++++++++++++++++ Declaration for Virtual Categories +++++++++++++++++++++
      (mark-section "Declaration for Virtual Categories" *separation-width*)
      (ltab)
      (cformat
       "
  <rdfs:Class rdf:ID=\"VirtualClass\">
     <rdfs:comment xml:lang=\"en\">Class of all the virtual classes</rdfs:comment>
     <rdfs:label xml:lang=\"fr\">Classe Virtuelle</rdfs:label>
     <rdfs:subClassOf rdf:resource=\"http://www.w3.org/2002/07/owl#Class\"/>
     <rdfs:label xml:lang=\"en\">Virtual Class</rdfs:label>
  </rdfs:Class>

  <rdfs:Class rdf:ID=\"VirtualObjectProperty\">
     <rdfs:label xml:lang=\"fr\">Propriété Objet Virtuelle</rdfs:label>
     <rdfs:comment xml:lang=\"en\">The class of the virtual object properties</rdfs:comment>
     <rdfs:subClassOf rdf:resource=\"http://www.w3.org/2002/07/owl#ObjectProperty\"/>
     <rdfs:label xml:lang=\"en\">Virtual Object Property</rdfs:label>
  </rdfs:Class>

  <rdfs:Class rdf:ID=\"VirtualDatatypeProperty\">
     <rdfs:subClassOf rdf:resource=\"http://www.w3.org/2002/07/owl#DatatypeProperty\"/>
     <rdfs:comment xml:lang=\"en\">The class of virtual datatype properties</rdfs:comment>
     <rdfs:label xml:lang=\"fr\">Propriété datatype virtuelle</rdfs:label>
     <rdfs:label xml:lang=\"en\">Datatype Virtual Property</rdfs:label>
  </rdfs:Class>

")
      ;;++++++++++++++++++ End Declaration for Virtual Categories +++++++++++++++++++
      (rtab)
      (mark-section "End Declaration for Virtual Categories" *separation-width*)
      (mark-section "Ontology" *separation-width*)
      ))
  ;; reverse print into *output*
  (rp result))

#|
? (make-ontology
  '(:version 1.0)
  '(:doctype "owl" "http://www.w3.org/2002/07/owl#"
            "test" "http://www.terregov.eupn.net/ontology/2005/10/test.owl/test#"
            "xsd"       "http://www.w3.org/2001/XMLSchema#" )
  '(:xmlns "rdf" "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
          "rdfs" "http://www.w3.org/2000/01/rdf-schema#"
          "owl" "http://www.w3.org/2002/07/owl#"
          "xsd" "&xsd;"
          "self" "&test;"
          "test" "&test;")
  '(:base "http://www.terregov.eupn.net/ontology/2005/10/test.owl/test")
  '(:imports "http://www.w3.org/2000/01/rdf-schema"
            "http://www.w3.org/1999/02/22-rdf-syntax-ns"
            "http://www.w3.org/2002/07/owl"))
|#
;;;------------------------------------------------- make-owl-virtual-attribute

(defun make-owl-virtual-attribute (id att-name domain-id-list value-type doc)
  "produces a list of strings corresponding to the OWL format for defining a ~
   datatype property locally. Options are translated into the required ~
   equivalents. When id is already the id of something else, then error.
Arguments:
   id: the OWL attribute internal name, e.g. \"hasAge\"
   att-name: multilingual name
   domain-id-list: list of possible domains
   value-type: an xsd type for the value
   doc: eventual documentation (multilingual string)
Return:
   a list of strings to be printed."
  (when (or (index-get id :att)(index-get id :rel)(index-get id :vrel)
            (index-get id :class)(index-get id :vclass))
    (terror "internal name ~S of virtual attribute already used." id))
  (let (result)
    ;; build index entries
    (setq result (make-indices att-name id :vatt))
    ;; now build virtual relation
    (cformat "<VirtualDatatypeProperty rdf:ID=~S>" id)
    ;; use the string for label
    (rtab)
    (+result (make-labels att-name))
    ;; process the different domains
    ;; if more than 1 use a collection option
    (+result (make-domains domain-id-list))
    ;; process type
    (+result (make-type value-type))
    ;; add eventual documentation
    (if doc (+result (make-doc doc)))
    ;; close the property definition
    (ltab)
    (cformat "</VirtualDatatypeProperty>")
    (push " " result)
    ;; return result
    result))

#|
? (make-owl-virtual-attribute "hasAge" '(:en "age" :fr "aage") '("Z-Person")
                              :non-negative-integer '(:en "...test doc"))
(" " "  </VirtualDatatypeProperty>"
 "  <rdfs:comment xml:lang=\"en\">...test doc</rdfs:comment>"
 "  <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\"/>"
 "  <rdfs:domain rdf:resource=\"#Z-Person\"/>"
 "  <rdfs:label xml:lang=\"fr\">aage</rdfs:label>"
 "  <rdfs:label xml:lang=\"en\">age</rdfs:label>"
 "<VirtualDatatypeProperty rdf:ID=\"hasAge\">")

? (rp *)
<VirtualDatatypeProperty rdf:ID="hasAge">
  <rdfs:label xml:lang="en">age</rdfs:label>
  <rdfs:label xml:lang="fr">aage</rdfs:label>
  <rdfs:domain rdf:resource="#Z-Person"/>
  <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#nonNegativeInteger"/>
  <rdfs:comment xml:lang="en">...test doc</rdfs:comment>
</VirtualDatatypeProperty>
|#
;;;-------------------------------------------------- make-owl-virtual-relation
;;; unused 

;;;(defun make-owl-virtual-relation (id rel-name domain-id-list range-id-list doc
;;;                                          inv-id)
;;;  "produces a list of strings corresponding to the OWL format for defining an ~
;;;      object property locally. Options are translated into the required ~
;;;      equivalents. When id is already the id of an attribute, then error.
;;;Arguments:
;;;   id: the OWL relation ID
;;;   rel-name: multilingual name
;;;   domain-id-list: list of possible domains
;;;   range-id-list: list of possible ranges
;;;   doc: eventual documentation (multilingual string)
;;;Return:
;;;   a list of strings to be printed."
;;;  (when (or (index-get id :att)(index-get id :vatt)(index-get id :rel)
;;;            (index-get id :class)(index-get id :vclass))
;;;    (terror "internal name ~S of virtual relation already used." id))
;;;  (let (result)
;;;    ;; build index entries
;;;    (setq result (make-indices rel-name id :rel))
;;;    ;; now build virtual relation
;;;    (cformat "<VirtualObjectProperty rdf:ID=~S>" id)
;;;    ;; use the string for label
;;;    (rtab)
;;;    (+result (make-labels rel-name))
;;;    ;; process the different domains
;;;    ;; if more than 1 use a collection option
;;;    (+result (make-domains domain-id-list))
;;;    ;; range is range of last class of compose
;;;    (+result (make-ranges range-id-list))
;;;    ;; add eventual documentation
;;;    (if doc (+result (make-doc doc)))
;;;    ;; add pointer to inverse property
;;; ;;;    (cformat "<owl:inverseOf>")
;;; ;;;    (rtab)
;;; ;;;    (cformat "<VirtualObjectProperty rdf:about=\"#~A\"/>" inv-id)  
;;; ;;;    (ltab)
;;; ;;;    (cformat "</owl:inverseOf>")
;;;    (ltab)
;;;    ;; close the property definition
;;;    (cformat "</VirtualObjectProperty>")
;;;    (push " " result)
;;;    ;; return result
;;;    result))
;;;
;;;#|
;;;(make-owl-virtual-relation "hasUncle" '(:en "Uncle" :fr "Oncle") '("Z-Person")
;;;            '("Z-Person") '(:en "test doc...") "isUncleOf")
;;;(rp *)
;;;<VirtualObjectProperty rdf:ID="hasUncle">
;;;  <rdfs:label xml:lang="en">Uncle</rdfs:label>
;;;  <rdfs:label xml:lang="fr">Oncle</rdfs:label>
;;;  <rdfs:domain rdf:resource="#Z-Person"/>
;;;  <rdfs:range rdf:resource="#Z-Person"/>
;;;  <rdfs:comment xml:lang="en">test doc...</rdfs:comment>
;;;</VirtualObjectProperty>
;;;|#
;;;-------------------------------------------------------- MAKE-PROPERTY-INDEX

(defun make-property-index (prop-mln prop-type)
  "makes an entry in the index table for each synonym in the property mln.
Arguments:
   prop-mln: mln naming the property
   prop-type: :att or :rel
Output:
   :done
Side-effects
   updates index"
  (let ((syn-list (mln::extract-all-synonyms prop-mln)))
    (case prop-type
       (:att
       (dolist (prop-name syn-list)
         (index-add 
          (make-index-string prop-name) prop-type :id (make-attribute-id prop-mln))))
      (:rel
       (dolist (prop-name syn-list)
         (index-add 
          (make-index-string prop-name) prop-type :id (make-relation-id prop-mln)))))))

#|
(make-property-index '(:fr "name" :en "nom") :att)
("Nom"
 ((:ATT (:ID "hasNom")) (:IDX (:FR "hasPaysNom" "hasVilleNom"))
  ("Z-Ville" (:ID "hasVilleNom")) ("Z-Pays" (:ID "hasPaysNom")))) 

(make-property-index '(:fr "pays" :en "country") :rel)

|#
;;;---------------------------------------------------------------- MAKE-RANGES

(defun make-ranges (range-id-list &aux result)
  "build a list of OWL specs for ranges (similar to domains).
Arguments;
   range-id-list: a list of OWL range IDs
Return:
   a list of OWL strings."
  (cond
   ;; when more than one domain we must use the union option
   ((cdr range-id-list)
    (cformat "<rdfs:range>")
    (rtab)
    (cformat "<owl:Class>")
    (rtab)
    (cformat "<owl:unionOf rdf:parseType=\"Collection\">")
    (rtab)
    (dolist (id range-id-list)
      (cformat "<owl:Class rdf:about=\"#~A\"/>" id))
    (ltab)
    (cformat "</owl:unionOf>")
    (ltab)
    (cformat "</owl:Class>")
    (ltab)
    (cformat "</rdfs:range>")
    result)
   ;; otherwise a single range
   (t (cformat "<rdfs:range rdf:resource=\"#~A\"/>" (car range-id-list)))))

#|
SOL(56): (make-ranges '("Z-Person" "Z-Child" "Z-Wife"))
("  </rdfs:range>" "    </owl:Class>" "      </owl:unionOf>"
 "        <owl:Class rdf:about=\"#Z-Wife\"/>"
 "        <owl:Class rdf:about=\"#Z-Child\"/>"
 "        <owl:Class rdf:about=\"#Z-Person\"/>"
 "      <owl:unionOf rdf:parseType=\"Collection\">" "    <owl:Class>"
 "  <rdfs:range>")
SOL(57): (rp *)
<rdfs:range>
<owl:Class>
<owl:unionOf rdf:parseType="Collection">
<owl:Class rdf:about="#Z-Person"/>
<owl:Class rdf:about="#Z-Child"/>
<owl:Class rdf:about="#Z-Wife"/>
</owl:unionOf>
</owl:Class>
</rdfs:range>
:END
? (make-ranges '("Z-Person"))
("<rdfs:range rdf:resource=\"#Z-Person\"/>")
|#
;;;?---------------------------------------------------------- MAKE-RELATION-ID
;;; same function as make-attribute-id (could be different)

(defun make-relation-id (name-list &optional class-name &aux name-string)
  "get a multilingual list and build a relation id, e.g. \"$hasBrother\".
   To do so, uses the first name of the :en option, or if there is no :en option ~
      use the first available name, building a hybrid name!
Arguments:
   name-list: a multi-lingual name list
   class-name (opt): class name extracted from class-id \"Z-Ville\" -> \"Ville\"
Return:
   a string that is the attribute ID."
  (declare (special *language*))
  (unless (is-mln? name-list)
    (terror "argument for making relation-id should be an MLN, not: ~S" name-list))
  ;; extract some string to represent the relation
  (setq name-string (mln::get-canonical-name name-list *language*))
  ;; make OWL relation id
  (concatenate 'string "has" (or class-name "") (make-index-string name-string)))

;;;   ;; otherwise pick up first name
;;;   (t (concatenate 'string "has" 
;;;                   (make-index-string (get-first-name (cadr name-list)))))))

#|
? (MAKE-relation-ID '(:IT "fratello" :EN " brother" :FR "frangin; frère"))
"hasBrother"

(let ((*language* :it))
    (MAKE-relation-ID '(:IT "fratello" :FR "frangin; frère")))
"hasFratello"

(let ((*language* :it))
  (MAKE-relation-ID '(:IT "fratello" :FR "frangin; frère") "Persona"))
"hasPersonaFratello"

(catch :error
       (make-relation-id "brother" "Person"))
"argument for making relation-id should be an MLN, not: \"brother\""
|#
;;;--------------------------------------------------------------- MAKE-SECTION

(defun make-section (&rest ll)
  "used to ignore the corresponding macro in the input text"
  (declare (ignore ll))
  nil)

;;;--------------------------------------------------------- MAKE-SINGLE-STRING
;;; used to fuse documentation from different fragments

(defun make-single-string (ll)
  (cond ((null ll) "") 
        ((null (cdr ll)) (car ll))
        (t (concatenate 'string (car ll) " ; " (make-single-string (cdr ll))))))

#|
? (make-single-string '("a" "b" "c"))
"a ; b ; c"
? (make-single-string '("a" "b" ))
"a ; b"
? (make-single-string '("a" ))
"a"
? (make-single-string '())
""
|#
;;;+------------------------------------------------------------- MAKE-SUBCLASS

(defun make-subclass (super-class-list class-id)
  "builds the list of superclasses for the concept.
Arguments:
   super-class-list: if string, then only one super-class otherwise list
Return:
   list of OWL specs
Error:
   throws to :error tag."
  ;; check syntax
  (cond
   ((listp super-class-list))
   ((stringp super-class-list) (setq super-class-list (list super-class-list)))
   (t (terror "bad format for :is-a option: ~S" super-class-list)))
  (let (index super-id result)
    ;; process all superclasses
    (dolist (item super-class-list)
      ;; build index from entry
      (setq index (make-index-string item))
      ;; check for existence using the *index* table
      (setq super-id (car (index-get index :class :id)))
      ;; if nil error
      (unless super-id
        (terror "unknown super-class: ~S in ~S" item super-class-list))
      ;; otherwise
      (cformat  "<rdfs:subClassOf rdf:resource=\"#~A\"/>" super-id)
      ;; add to model of class
      (index-add class-id :class :is-a super-id)
      ;; save concept into the superclass list (what for?)
      (push super-id *super-class-list*))
    ;; return spec list
    result))

#|
? (index-clear)
T
? (index-add "Z-City" :class :name '(:en "city"))
(:EN "city")
? (index-add "City" :class :id "Z-City")
"Z-City"
? (index)

("City" ((:CLASS (:ID "Z-City")))) 
("Z-City" ((:CLASS (:NAME (:EN "city"))))) 
:END
? (make-subclass '("city ") "Z-District")
("<rdfs:subClassOf rdf:resource=\"Z-City\"/>")
? (index)

("City" ((:CLASS (:ID "Z-City")))) 
("Z-City" ((:CLASS (:NAME (:EN "city"))))) 
("Z-District" ((:CLASS (:IS-A "Z-City")))) 
:END
NIL
|#
;;;------------------------------------------------------------- MAKE-TEXT-LIST

(defun make-text-list (text)
  "Norm a text string by separating each word making it lower case with a leading~
      uppercased letter.
Arguments:
   text: text string
Return:
   a list of normed words."
  (let (pos result word)
    (unless text (return-from make-text-list nil))
    (loop
      ;; remove trailing blanks
      (setq text (string-trim '(#\space) text))
      ;; is there any space left?
      (setq pos (position #\space text))
      (unless pos
        (push (string-capitalize text) result)
        (return-from make-text-list (reverse result)))
      ;; extract first word
      (setq word (subseq text 0 pos)
            text (subseq text (1+ pos)))
      (push (string-capitalize word) result)
      )))

#|
? (make-text-list "the   DAY when I    fell INTO the PIT   ")
("The" "Day" "When" "I" "Fell" "Into" "The" "Pit")
|#
;;;--------------------------------------------------------------- MAKE-TRAILER

(defun make-trailer ()
  "to close the ontology"
  (ltab)
  (format *output* "~&</rdf:RDF>"))

;;;------------------------------------------------------------------ MAKE-TYPE
;;; used for building virtual attributes

(defun make-type (type)
  "builds the xsd-type for a datatype property (attribute). 
   Default is string.
Arguments:
   type: a keyword, e.g. :integer, :float, :date, ... or nil
Return:
   list of OWL specs
Error:
   throws to :error tag."
  (let (xsd-type result)
    (unless type
      (setq xsd-type (or xsd-type (cdr (assoc :string *attribute-types*)))))
    (when type
      
      (setq xsd-type (cdr (assoc type *attribute-types*)))
      ;; error if not in the list
      (unless xsd-type
        (terror "bad attribute type: ~S" type)))
    ;; build OWL spec
    (cformat  "<rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#~A\"/>" 
              xsd-type)
    ;; return spec list
    result))

#|
? (make-type :float)
("    <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#float\"/>")

? (make-type :non-negative-integer)
("<rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\"/>")

? (make-type nil)
("    <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#string\"/>")

? (catch :error (make-type :babar))
"bad attribute type: :BABAR"
|#
;;;---------------------------------------------------------- MAKE-VALUE-STRING

(defun make-value-string (value &optional (interchar '#\space))
  "Takes an input string, removing all leading and trailing blanks, replacing ~
   all substrings of blanks and simple quote with the interchar, and ~
   building a new string capitalizing all letters. The function is used ~
   to normalize values for comparing a value with an external one while ~
   querying the database.  
   French accentuated letter are replaced with unaccentuated capitals."
  (let* ((input-string (if (stringp value) value
                         ;; use ~A to remove package mark
                         (format nil "~A" value)))
         (work-list (map 'list #'(lambda(x) x) 
                         ;; remove leading and trailing blanks
                         (string-trim '(#\space) input-string))))
    (string-capitalize   ; we normalize entry-point to lower case
     (map
      'string
      #'(lambda(x) x) 
      ;; this mapcon removes the blocks of useless spaces and uses
      ;; a translation table to replace French letters with the equivalent
      ;; unaccentuated capitals
      (mapcon 
       #'(lambda(x) 
           (cond 
            ;; if we have 2 successive blanks we remove one
            ((and (cdr x)(eq (car x) '#\space)
                  (eq (cadr x) '#\space)) 
             nil)
            ;; if we have a single blank we replace it with an intermediate char
            ((eq (car x) '#\space) (copy-list (list interchar)))
            ;; if the char is in the translation table we use the table
            ((assoc (car x) *translation-table*)
             (copy-list (cdr (assoc (car x) *translation-table*))))
            ;; otherwise we simply list the char
            (t (list (car x)))))
       work-list)))))

#|
? (make-value-string "aujourd'hui  j'ai faim.    ")
"Aujourd Hui J Ai Faim."

(make-value-string '_var)
"_Var"

(make-value-string 'moss::_var)
"Moss::_Var"
|#
;;;----------------------------------------------------- MAKE-VIRTUAL-ATTRIBUTE
;;; JPB080106
;;; during compiler PASS 1 an entry is created in the INDEX table to track the
;;; existence of the virtual attribute The function simply creates the corresponding
;;; rule
;;;
;;; Syntax:
;;;   (defvirtualattribute <mln>
;;;     (:class <concept-ref>)
;;;     (:def <concept-definition>)
;;;     (:type <xsd-type>)
;;;     (:doc <documentation>))

(defun make-virtual-attribute (attribute-name &rest option-list 
                                                  &aux message)
  "copies the definition of the virtual attribute into the index table.
   It will be processed later.
Arguments:
   attribute-name: a multi-lingual :name list
   option-list: a possible sequence of SOL options
Return:
   a list of strings corresponding to the OWL format."
  (when 
    (eql *compiler-pass* 1)
    ;; first compiler pass, we build index entries
    (let (id)
      (setq 
       message
       (catch :error
         ;;=== check simple syntax errors. if an error, throw to :error
         ;; rough check of the name input format, removing :name tag if present
         ;; e.g. ({:name} :en "age" :fr "‰ge")
         ;(unless (setq attribute-name (multilingual-name? attribute-name))
         (unless (is-mln? attribute-name)
           (terror "Bad attribute name format."))
         (unless (or (assoc :compose option-list) (assoc :def option-list))
           (terror "Missing :def or ;compose option."))
         ;; check for existing id
         (if (or (index-get id :att)(index-get id :vatt)(index-get id :rel)
                 (index-get id :vrel))
           (terror "Internal ID ~S already exists." id))
         ;;=== end error check
         
         ;; we use the :en label whenever possible, otherwise the first of the list
         ;;     here the id should be something like: "hasAge"
         (setq id (make-attribute-id attribute-name))
         ;; save it into the attribute list
         (when (assoc :def option-list)
           (pushnew id *virtual-attribute-list* :test #'string-equal))
         (when (assoc :compose option-list)
           (pushnew id *virtual-composed-attribute-list* :test #'string-equal))         
         ;; set the index table entry to be used by process-virtual-attributes
         (%index-set id :vatt :def (cons (list :name attribute-name) option-list))
         ;; add index entries, used by other entities
         (make-indices attribute-name id :vatt)
         ))
      ;; is message is a string then we have an error otherwise todo bem
      (if (stringp message)
        (twarn "~&;***** Error in virtual attribute: ~S;~&;  ~A" 
               attribute-name message))
      :done
      )))

#|
(let ((*compiler-pass* 1))
  (reset)
  (defvirtualattribute (:name :en "age" :fr "âge")
      (:class "Person")
    (:def
        (?class "birth date" ?y)
        ("getYears" ?y ?self)
      )
    (:type :integer)
    (:min 1)
    (:doc :en "the attribute age is computed from the birthdate of a ~
               person by applying the function getYears to the birthdate."))
  (index))

("Age" ((:IDX (:EN "hasAge")) (:VATT (:ID "hasAge")))) 
("hasAge"
 ((:VATT
   (:DEF (:NAME (:EN "age" :FR "âge")) (:CLASS "Person")
    (:DEF (?CLASS "birth date" ?Y) ("getYears" ?Y ?SELF)) (:TYPE :INTEGER)
    (:MIN 1)
    (:DOC :EN "the attribute age is computed from the birthdate of a ~
               person by applying the function getYears to the birthdate."))))) 
("Âge" ((:IDX (:FR "hasAge")) (:VATT (:ID "hasAge"))))  
:END

(let ((*compiler-pass* 1))
  (reset)
  (defvirtualattribute (:name :en "French national" :fr "personne francaise")
      (:class "person")
    (:def
        (?class "country" ?y)
        (?result "name" :equal "france")
      )
    (:type :boolean)
    (:doc :en "A French national is a person whose country is France."))
  (index))

? (index)
("FrenchNational"
 ((:IDX (:EN "hasFrenchNational")) (:VATT (:ID "hasFrenchNational")))) 
("PersonneFrancaise"
 ((:IDX (:FR "hasFrenchNational")) (:VATT (:ID "hasFrenchNational")))) 
("hasFrenchNational"
 ((:VATT
   (:DEF (:NAME (:EN "French national" :FR "personne francaise")) (:CLASS "person")
     (:DEF (?CLASS "country" ?Y) (?RESULT "name" :EQUAL "france")) (:TYPE :BOOLEAN)
     (:DOC :EN "A French national is a person whose country is France."))))) 
:END
|#
;;;------------------------------------------------------- MAKE-VIRTUAL-CONCEPT
;;; JPB080106
;;; during compiler PASS 1 an entry is created in the INDEX table to track the
;;; existence of the virtual class. The function simply creates the corresponding
;;; rule
;;;
;;; Syntax:
;;;   (defvirtualconcept <mln>
;;;     (:is-a <concept-ref>)
;;;     (:def <concept-definition>)
;;;     (:doc <documentation>))

(defun make-virtual-concept (concept-name &rest option-list &aux message id)
  "saves definition into the index table to be processed later..
Arguments:
   concept-name: a multi-lingual :name list
   option-list: a possible sequence of SOL options
Return:
   a list of strings corresponding to the OWL format."
  ;; we cannot process data before the end of processing properties and attributes
  ;; since we need to know the type of some propeerties
  ;; thus we simply record the data in the index table
  (when 
    (eql *compiler-pass* 1)
    (setq 
     message
     (catch :error
       (let (concept-id )
         ;;=== check for simple syntax errors
         (unless (is-mln? concept-name)
           (terror "Bad concept name format: ~S" concept-name))
         
         (setq concept-id (make-concept-id concept-name))
         ;; check for existing id
         (if (or (index-get id :class)(index-get id :vclass))
           (terror "Internal ID ~S already exists." id))
         ;;=== end error check
         
         ;; make a list of index entries
         (make-indices concept-name concept-id :vclass)
         (pushnew concept-id *virtual-concept-list* :test #'string-equal)
         ;; save concept-name as well
         (%index-set concept-id :vclass 
                     :def (cons (list :name concept-name) option-list))
         :done)))
    ;; is message is a string then we have an error otherwise todo bem
    (if (stringp message)
      (twarn "~&;***** Error in virtual concept: ~S;~&;  ~A" 
             concept-name message))
    :done))

#|
(reset)

(let ((*compiler-pass* 1))
  (defconcept (:en "Person" :fr "Personne")
      (:att (:en "age" :fr "âge") (:type :integer)))
  (defvirtualconcept (:en "Adult" :fr "Adulte")
      (:is-a "person")
    (:def 
        (?self "age" > 18))
    (:doc :en "An ADULT is a person over 18.")))
:DONE

(index)
("Adult" ((:IDX (:EN "Z-Adult")) (:VCLASS (:ID "Z-Adult")))) 
("Adulte" ((:IDX (:FR "Z-Adult")) (:VCLASS (:ID "Z-Adult")))) 
("Z-Adult"
 ((:VCLASS
   (:DEF (:NAME (:EN "Adult" :FR "Adulte")) (:IS-A "person ") (:DEF (?* "age" > 18))
     (:DOC :EN "An ADULT is a person over 18."))))) 
:END
|#
;;;------------------------------------------------------ MAKE-VIRTUAL-RELATION
;;; JPB071231
;;; during compiler PASS 1 an entry is created in the INDEX table to track the
;;; existence of the virtual relation
;;;
;;; Syntax:
;;;   (defvirtualrelation <mln>
;;;     (:class <concept-ref>)
;;;     (:compose <relation-path>)
;;;     (:doc <documentation>))

(defun make-virtual-relation (relation-name &rest option-list &aux message)
  "produces a list of strings corresponding to the OWL format for defining the ~
      virtual relation. Sets an entry for later processing the OWL definition. ~
      Builds the rule according to the current rule formalism.
   This function is called twice: once during the first pass, another time ~
      during the second pass.
Syntax is:
     (defvirtualrelation (:name :en \"uncle\" :fr \"oncle\")
        {(:class \"person\")}
        {(:to \"person\")}
        (:compose  \"father\" \"brother\")
        {(:max 12)}
        (:doc :en \"the property uncle is composed from father and brother.\"))
Arguments:
   relation-name: a multi-lingual :name list
   option-list: a possible sequence of SOL options
Return:
   a list of strings corresponding to the OWL format."
  ;; the idea is to create an entry in the inces table that will be processed later
  (when 
    (eql *compiler-pass* 1)
    ;; first compiler pass, we collect classes and build index entries
    (let (id)
      (setq 
       message
       (catch :error
         ;;=== check simple syntax errors. if an error, throw to :error
         ;; rough check of the name input format, removing :name tag if present
         ;; e.g. ({:name} :en "person" :fr "personne")
         ;(unless (setq relation-name (multilingual-name? relation-name))
         (unless (is-mln? relation-name)
           (terror "Bad relation name format, should be MLN: ~S" relation-name))
         ;; check simple things
         (unless (assoc :compose option-list)
           (terror "Missing :compose option in ~S" relation-name))
         ;; we use the :en label whenever possible, otherwise the first of the list
         ;;     here the id should be "hasUncle"
         (setq id (make-relation-id relation-name))
         ;; check for existing id
         (if (or (index-get id :rel)(index-get id :att)(index-get id :vatt)
                 (index-get id :vrel))
           (terror "Internal ID ~S already exists." id))
         ;;=== end error check
         
         ;; save it into the concept list
         (pushnew id *virtual-relation-list* :test #'string-equal)
         ;; set index entry
         (%index-set id :vrel :def (cons (list :name relation-name) option-list))
         ;; add index entries
         (make-indices relation-name id :vrel)
         ))
      ;; is message is a string then we have an error otherwise todo bem
      (if (stringp message)
        (twarn "~&;***** Error in virtual realtion: ~S~&;  ~A" relation-name message))
      :done
      )))

#|
? (index-clear)
T
? (let ((*compiler-pass* 1))
    (make-virtual-relation '(:name :en "uncle" :fr "oncle")
                           '(:class "person")
                           '(:compose "mother" "brother")
                           '(:to "person")
                           '(:max 12)
                           '(:doc :en "an uncle is a person who is the brother of the mother.")))
:DONE

? (index)
("Oncle" ((:IDX (:FR "hasUncle")) (:VREL (:ID "hasUncle")))) 
("Uncle" ((:IDX (:EN "hasUncle")) (:VREL (:ID "hasUncle")))) 
("hasUncle"
 ((:VREL
   (:DEF (:NAME (:EN "uncle" :FR "oncle")) (:CLASS "person")
     (:COMPOSE "mother" "brother") (:TO "person") (:MAX 12)
     (:DOC :EN "an uncle is a person who is the brother of the mother."))))) 
:END
|#
;;;---------------------------------------------------------------- merge-names
;;; unused

;;;(defun merge-names (name-list)
;;;  "reverse from extract-names. Takes a list of strings and builds a single sring~
;;;      by concatenating the strings separated be \"; \". E.g.
;;;   (\"sexe\" \"genre\") -> \"sexe; genre\"
;;;Arguments;
;;;   a list of strings
;;;Return:
;;;   a composite string, nil if incorrect entry"
;;;  (if (every #'stringp name-list) 
;;;    (apply #'concatenate 'string 
;;;           (cdr (mapcan #'(lambda (xx) (list "; " xx)) name-list)))))
;;;
;;;#|
;;;? (merge-names '("papers " " letters " " la nuit   des temps "))
;;;"papers ;  letters ;  la nuit   des temps "
;;;|#
;;;--------------------------------------------------------- multilingual-name?
;;; deprecated, use is mln?

;;;(defun multilingual-name? (expr)
;;;  "checks whether a list starting with :name followed by a multilingual string ~
;;;      or a multilingual string.
;;;Argument:
;;;   expr: something like ({:name} <multilingual-string>)
;;;Return: 
;;;   nil or expr without :name if it was there"
;;;  (and (listp expr)
;;;       (or (and (mls? expr) expr)
;;;           (and (eql (car expr) :name) (mls? (cdr expr)) (cdr expr)))))
;;;
;;;#|
;;;? (multilingual-name? '(:name))
;;;NIL
;;;? (multilingual-name? '(:en "Albert"))
;;;(:EN "Albert")
;;;? (multilingual-name? '(:name :en "Albert"))
;;;(:EN "Albert")
;;;|#
;;;------------------------------------------------------- multilingual-string?
;;; deprecated, use is-mln?

;;;(defun multilingual-string? (expr)
;;;  "checks whether the expression is a multilanguage string (MLS). It must be a list ~
;;;      with alternated language tags and strings. nil is not a valid MLS.
;;;Arguments:
;;;   expr: expression to be tested
;;;Return:
;;;   expr or nil"
;;;  (when expr (multilingual-string-1? expr)))

;;;(defun mls? (expr)
;;;  "same as multilingual-string?"
;;;  (when expr (multilingual-string-1? expr)))

;;;(defun multilingual-string-1? (expr)
;;;  "auxiliary recursive function checking MLS"
;;;  (or (null expr)
;;;      (and (listp expr)
;;;           (member (car expr) *language-tags*)
;;;           (stringp (cadr expr))
;;;           (multilingual-string-1? (cddr expr)))))
;;;
;;;#|
;;;? (mls? '(:en "sex" :fr "sexe; genre"))
;;;(is-mln? '(:en "sex" :fr "sexe; genre"))
;;;T
;;;? (mls? '(:en "sex" :fr "sexe; genre" :pl))
;;;(is-mln? '(:en "sex" :fr "sexe; genre" :pl))
;;;NIL
;;;? (mls? nil)
;;;(is-mln? nil)
;;;NIL
;;;|#
;;;------------------------------------------------------ ONE-OF-ALL-INSTANCES?
;;; does not seem to be used

(defun one-of-all-instances? (object-list)
  "tests whether all objects are references to existing instances using the ~
   index table.
Argments:
   object-list: a list of mln or strings
Return:
   nil if not the case, list of instance-references if so."
  (let ((id-list 
         (mapcar #'(lambda (xx) 
                     (car (index-get (make-index-string xx) :inst :id)))
                 object-list)))
    ;; if we get (nil nil nil) we replace by nil
    (if (and id-list (every #'null id-list)) (setq id-list nil))
    (if (some #'null id-list)
      (terror 
       "Some objects of the :one-of option are not existing instances: ~% ~S"
       object-list))
    id-list)
  )

#|
? (one-of-all-instances? '(" france " "Italy"))
("z-france" "z-italy")

? (catch :error (one-of-all-instances? '(" france " "Italy" "zimbabwe")))
"Some objects of the :one-of option are not existing instances: 
 (\" france \" \"Italy\" \"zimbabwe\")"

? (one-of-all-instances? '())
NIL

? (catch :error (one-of-all-instances? '((:en "France")(:en "italy"))))
NIL
|#
;;;--------------------------------------------------------- PROCESS-ATTRIBUTES

(defun process-attributes ()
  "takes *attribute-list* and build the attribute structures. A given attribute ~
      is composed of fragments when used in different classes. Such fragments must ~
      be checked for consistency.
   - all names are merged and one is chosen as the OWL ID
   - domains can be multiple
   - ranges must be identical
   - :one-of may be different for each class
   In the last two cases, subproperties must be defined in case of conflict.
   Example of format
      (:att (:en \"name\" :fr \"nom\")(:type :name)(:unique))
      (:att (:en \"documentation\" :fr \"documentation\"))
      (:att (:en \"age range\" :fr \"tranche d'âge\")
             (:type :integer-range) 
             (:one-of (0 4) (5 12) (13 19)))
   Formalisme:
      (:att <MLN> {<value-type>} {<card-constraint>} {<value-one-of>})
Arguments:
   none
Return:
   list of OWL spec to be printed
Error:
   throws to :error tag."
  (let (result att-fragments id att-mln domain-id-list range-type message)
    (mark-section "Datatype Properties" *separation-width*)
    (loop
      (setq 
       message
        (catch 
         :error
         (unless *attribute-list*
           (cformat " ")
           (mark-section "End Datatype Properties" *separation-width*)
           (rp result)
           (return-from process-attributes result))
         
         ;; get fragments corresponding to next attribute, removing entries
         ;; from *attribute-list*
         ;; returns an a-list (<att-mln> ("Person" . <att def>)(...))
         (setq att-fragments (extract-attribute-fragments))
         ;; take the att MLN name first element of the list of fragments
         (setq att-mln (pop att-fragments))
         ;(format t "~%;--- att-mln: ~S" att-mln)
         ;; make the OWL id 
         (setq id (make-attribute-id att-mln))
         ;(format t "~%;--- ids: ~S" id)
         ;;
         (make-indices att-mln id :att)
         ;; get domains JPB0510 (moved from below for error message)
         ;; i.e. all the concepts containing this attribute
         (setq domain-id-list (get-attribute-domain att-fragments))
         
         ;; the attribute name is unique from the way we process the attributes
         
         (vformat "~&===== att: ~S" id)
         ;; record attribute MLN into index
         (index-add id :att :name att-mln)
         ;; if ranges are not all same, error (default type is :string)
         (setq range-type (get-attribute-type att-fragments))
         
         ;; record domain and range type (why?)
         (index-add id :att :domain domain-id-list)
         (index-add id :att :range range-type)
         
         ;(break "process attributes")
         
         ;; keep the fragments on the attribute entry, usefull for individuals
         ;; each fragment corresponds to a class-id, add that as an index
         (dolist (fragment att-fragments)
           ;; skip the mln name
           (index-add id :att (car fragment) (cddr fragment)))
        
         ;(break "process-attributes (index)~%  ~S"(index))
         
         ;;=== build now the OWL code for the global datatype property
         (+result 
          (process-attributes-global-definition
           id att-mln domain-id-list range-type))
 
         ;(format t "~%; process-attributes /id: ~S" id)
         ;(break "process-attributes (att-fragments)~%  ~S" att-fragments)
         
         ;;=== each fragment corresponds to a different class
         ;; Then process each fragment in turn, applying specific restrictions
         (dolist (fragment att-fragments)
           ;; if local options, build OWL about clause
           (+result (process-attributes-class-restrictions fragment))
           )))
      
      ;; print each attribute (global and restrictions) in turn, unless error
      (if (stringp message)
          (twarn "~&;***** Error in:~S; ~A" att-mln message)
        ;; print unless we compile for checking errors
        (unless *errors-only* (rp result)))
      ;; reset result for next candidate in loop
      (setq result nil)
      )))

#|
(progn 
  (index-clear)
  (setq *attribute-list* 
        '(("Z-Territoire" (:EN "name" :FR "nom") (:TYPE :NAME) 
           (:UNIQUE) (:INDEX))))
  (process-attributes)
  (index))
("Name" ((:IDX (:EN "hasName")) (:ATT (:ID "hasName")))) 
("Nom" ((:IDX (:FR "hasName")) (:ATT (:ID "hasName")))) 
("hasName"
 ((:ATT (:RANGE :NAME) (:DOMAIN ("Z-Territoire"))
        (:NAME ((:EN "name") (:FR "nom"))))))
:END
|#
;;;-------------------------------------- PROCESS-ATTRIBUTES-CLASS-RESTRICTIONS

(defun process-attributes-class-restrictions (fragment)
  "for a given attribute process local restrictions, namely cardinality ~
   conditions, doc and one-of.
Arguments:
   fragment: attribute fragment, e.g. 
        (\"Z-Pays\" (:EN \"name\" :FR \"nom\") (:TYPE :NAME) (:ENTRY))
Return:
   a list of OWL specs."
  (let ((att-spec (cddr fragment)) ; list of attribute options
        (class-id (car fragment))
        result spec spec-max index-spec doc one-of range-type id)
    ;; get value type
    (setq range-type (or (cadr (assoc :type att-spec)) :string))
    ;; compute attribute class id
    (setq id (make-attribute-id (cadr fragment) (subseq class-id 2)))
    
    ;; check for cardinality conditions
    (if
      ;; uniquenes
      (assoc :unique att-spec)
      (setq spec
            "<owl:cardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:cardinality>"
            )
      ;; else check min AND max options
      (progn
        ;; min
        (if (assoc :min att-spec)
          (setq spec
                (format
                 nil
                 "<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:minCardinality>"
                 (cadr (assoc :min att-spec)))))
        ;; max
        (if (assoc :max att-spec)
          (setq spec-max
                (format
                 nil
                 "<owl:maxCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:maxCardinality>"
                 (cadr (assoc :max att-spec)))))))
    ;; check indexing
    (if (or (assoc :index att-spec)(assoc :entry att-spec))
      (setq index-spec 
            (format
             nil
                "<indexing rdf:datatype=\"http://www.w3.org/2001/XMLSchema#boolean\">true</indexing>")))
    ;; check doc and one-of
    (setq one-of (cdr (assoc :one-of att-spec)))
    (setq doc (cdr (assoc :doc att-spec)))
    
    ;; if we got something then
    (when (or spec spec-max index-spec one-of doc)
      ;; set up the stage
      (push " " result)
      (cformat "<owl:Class rdf:about=\"#~A\">" class-id)
      (rtab)
      (cformat "<rdfs:subClassOf>")
      (rtab)
      (cformat "<owl:Restriction>")
      (rtab)
      (cformat "<owl:onProperty rdf:resource=\"#~A\"/>" id)
      (if spec (cformat spec))
      (if spec-max (cformat spec-max))
      (if index-spec (cformat index-spec))
      ;; if no restriction but index, introduce a phony restriction
      (unless (or spec spec-max)
	(cformat "<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">0</owl:minCardinality>"))
      ;; check one-of option
      (if one-of
        (+result (process-attributes-one-of-values one-of id range-type)))
      ;; check doc condition
      (if doc (+result (make-doc doc)))
      (ltab)
      (cformat "</owl:Restriction>")
      (ltab)
      (cformat "</rdfs:subClassOf>")
      (ltab)
      (cformat "</owl:Class>")
      (push " " result))
    
    result))

#|
(process-attributes-class-restrictions 
   '("Z-Person" (:FR " Age " :IT "age") (:MIN 1) (:TYPE :non-negative-integer)))
(rp *)
  <owl:Class rdf:about="#Z-Person">
    <rdfs:subClassOf>
      <owl:Restriction>
        <owl:onProperty rdf:resource="#hasPersonAge"/>
        <owl:minCardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:minCardinality>
      </owl:Restriction>
    </rdfs:subClassOf>
  </owl:Class>
:END

(process-attributes-class-restrictions 
   '("Z-Person" (:FR " nom " :IT "nome") (:index) (:TYPE :name)))
(rp *)
  <owl:Class rdf:about="#Z-Person">
    <rdfs:subClassOf>
      <owl:Restriction>
        <owl:onProperty rdf:resource="#hasPersonNom"/>
        <indexing rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">true</indexing>
        <owl:minCardinality rdf:datatype="&xsd;nonNegativeInteger">0</owl:minCardinality>
      </owl:Restriction>
    </rdfs:subClassOf>
  </owl:Class>
:END

(process-attributes-class-restrictions 
 '("Z-Person" (:FR " nom " :IT "nome") (:index) (:TYPE :name)
   (:one-of "Albert" "John") (:doc :en "English person")))
(rp *)
  <owl:Class rdf:about="#Z-Person">
    <rdfs:subClassOf>
      <owl:Restriction>
        <owl:onProperty rdf:resource="#hasPersonNom"/>
        <indexing rdf:datatype="http://www.w3.org/2001/XMLSchema#boolean">true</indexing>
        <owl:minCardinality rdf:datatype="&xsd;nonNegativeInteger">0</owl:minCardinality>
        <owl:oneOf>
          <rdf:List>
            <rdf:first rdf:datatype="&xsd;NAME"/>"Albert"</rdf:first>
            <rdf:rest>
              <rdf:List>
                <rdf:first rdf:datatype="&xsd;NAME"/>"John"</rdf:first>
                <rdf:rest rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#nil"/>
              </rdf:List>
            </rdf:rest>
          </rdf:List>
        </owl:oneOf>
        <rdfs:comment xml:lang="fr">English person</rdfs:comment>
      </owl:Restriction>
    </rdfs:subClassOf>
  </owl:Class>
|#
;;;--------------------------------------- PROCESS-ATTRIBUTES-GLOBAL-DEFINITION

(defun process-attributes-global-definition (id att-name domain-id-list range-type)
  "produces a list of strings corresponding to the OWL format for defining a ~
   global datatype property.
Arguments:
   id: the OWL attribute ID
   att-name: multilingual name
   domain-id-list: list of possible domains
Return:
   a list of strings to be printed."
  (let (attribute-type result) 
    ;; get xsd-type throws to error if illegal
    (setq attribute-type (get-xsd-attribute-type range-type id domain-id-list))
    ;; build index entries for the names of the attribute
    (setq result (make-indices att-name id :att))
    ;; now build global OWL attribute
    (cformat "<owl:DatatypeProperty rdf:ID=~S>" id)
    ;; use the string for label
    (rtab)
    (+result (make-labels att-name))
    ;; process the different domains; if more than 1 use a collection option
    (setq result (append (make-domains domain-id-list) result))
    ;; set the range (a literal by default a string)
    (cformat "<rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#~A\"/>"
             attribute-type)
    (ltab)
    ;; close the property definition
    (cformat "</owl:DatatypeProperty>")
    (push " " result)
    ;; return result
    result))

;;;-------------------------------------------- process-atributes-local-options

(defun process-attributes-local-options (att id)
  "for each attribute process local restrictions, namely cardinality ~
      conditions.
Arguments:
   att: attribute fragment, e.g. 
        (\"Tiger\" (:FR \" Genre \" :IT \"sexo\") (:MIN 1) (:TYPE :STRING))
   id: attribute OWL ID
Return:
   a list of OWL specs."
  ;; get attribute SOL spec, removing the :att tag
  (let ((att-spec (cdr att))
        (class-id (car att))
        result spec spec-max index-spec)
    ;; check for cardinality conditions
    (if
      ;; uniquenes
      (assoc :unique att-spec)
      (setq spec
            "<owl:cardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:cardinality>"
            )
      ;; else chack min AND max options
      (progn
        ;; min
        (if (assoc :min att-spec)
          (setq spec
                (format
                 nil
                 "<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:minCardinality>"
                 (cadr (assoc :min att-spec)))))
        ;; max
        (if (assoc :max att-spec)
          (setq spec-max
                (format
                 nil
                 "<owl:maxCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:maxCardinality>"
                 (cadr (assoc :max att-spec)))))))
    ;; check indexing
    (if (assoc :index att-spec)
      (setq index-spec 
            (format
             nil
             "<indexing rdf:datatype=\"http://www.w3.org/2001/XMLSchema#boolean\">true</indexing>")))
    ;; if we got something then
    (when (or spec spec-max index-spec)
      ;; set up the stage
      (push " " result)
      (cformat "<owl:Class rdf:about=\"#~A\">" class-id)
      (rtab)
      (cformat "<rdfs:subClassOf>")
      (rtab)
      (cformat "<owl:Restriction>")
      (rtab)
      (cformat "<owl:onProperty rdf:resource=\"#~A\"/>" id)
      (if spec (cformat spec))
      (if spec-max (cformat spec-max))
      (if index-spec (cformat index-spec))
      ;; if no restriction but index, introduce a phony restriction
      (unless (or spec spec-max)
	(cformat "<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">0</owl:minCardinality>"))
      (ltab)
      (cformat "</owl:Restriction>")
      (ltab)
      (cformat "</rdfs:subClassOf>")
      (ltab)
      (cformat "</owl:Class>"))
    result))

#|
? (process-attributes-local-options 
   '("Person" (:FR " Age " :IT "age") (:MIN 1) (:TYPE :non-negative-integer))
   "hasAge")

("</owl:Class>" "  </rdfs:subClassOf>" "    </owl:Restriction>"
 "      <owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:minCardinality>"
 "      <owl:onProperty rdf:resource=\"#hasAge\"/>" "    <owl:Restriction>"
 "  <rdfs:subClassOf>" "<owl:Class rdf:about=\"#Person\">" " ")

? (rp *)

<owl:Class rdf:about="#Person">
<rdfs:subClassOf>
<owl:Restriction>
<owl:onProperty rdf:resource="#hasAge"/>
<owl:minCardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:minCardinality>
</owl:Restriction>
</rdfs:subClassOf>
</owl:Class>
:END

? (process-attributes-local-options 
   '("Person" (:FR " Age " :IT "age") (:unique) (:TYPE :non-negative-integer))
   "hasAge")
...
<owl:Class rdf:about="#Person">
<rdfs:subClassOf>
<owl:Restriction>
<owl:onProperty rdf:resource="#hasAge"/>
<owl:cardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:cardinality>
</owl:Restriction>
</rdfs:subClassOf>
</owl:Class>
:END

|#
;;;--------------------------------------------------- PROCESS-ATRIBUTES-ONE-OF
#|
In addition to the RDF datatypes, OWL provides one additional construct for 
defining a range of data values, namely an enumerated datatype. This datatype 
format makes use of the owl:oneOf construct, that is also used for describing 
an enumerated class. In the case of an enumerated datatype, the subject of 
owl:oneOf is a blank node of class owl:DataRange and the object is a list of 
literals. Unfortunately, we cannot use the rdf:parseType="Collection" idiom for 
specifying the literal list, because RDF requires the collection to be a list 
of RDF node elements. Therefore we have to specify the list of data values with 
the basic list constructs rdf:first, rdf:rest and rdf:nil.

NOTE: Enumerated datatypes are not part of OWL Lite.

The following example restricts the class to contain instances for which the
property hasStyle takes values in "aristocratique" "ouvrière" or "bourgeoise"

<owl:Class rdf:about="#Z-ville">
  <rdfs:subClassOf>
    <owl:Restriction>
      <owl:onProperty rdf:resource="#hasStyle" />
      <owl:oneOf>
        <rdf:List>
          <rdf:first rdf:datatype="&xsd;string">aristocratique</rdf:first>
          <rdf:rest>
            <rdf:list>
              <rdf:first rdf:datatype="&xsd;string">ouvrière</rdf:first>
              <rdf:rest>
                <rdf:list>
                  <rdf:first rdf:datatype="&xsd;string">bourgeoise</rdf:first>
                  <rdf:rest rdf:resource="&rdf;nil" />
                </rdf:list>
              </rdf:rest>
            </rdf:list>
          </rdf:rest>
        </rdf:list>
      </owl:oneOf>
    </owl:Restriction>
  </rdfs:subClassOf>
</owl:Class>
|#
;;;------------------------------------------- PROCESS-ATTRIBUTES-ONE-OF-VALUES

(defun process-attributes-one-of-values (value-list att-id xsd-range-type)
  "we build a list of OWL specs, given that each fragment corresponds to a ~
   different class.
Arguments:
   value: list of values, either strings or MLN
   range-type: xsd-type of a value, e.g. \"gYear\"
Return:
   a list of OWL specs."
  (declare (ignore att-id))
  (let (result)
    (setq value-list
          (if (is-mln? (car value-list))
              (reduce #'append 
                      (mapcar #'(lambda (xx) (get-lan-values xx :always))
                        value-list))
            value-list))

    (cformat "<owl:oneOf>")
    (rtab)
    ;; start the rest unless we have no more value to include
    (+result
      (reverse (process-attributes-one-of-list value-list xsd-range-type)))
    ;; return on the rest
    (ltab)    
    (cformat "</owl:oneOf>")
    result))
          
#|             
(process-attributes-one-of-values '("dispersed" "compact") "hasStyle" "String")
(rp *)
<owl:oneOf>
  <rdf:List>
    <rdf:first rdf:datatype="&xsd;String"/>"dispersed"</rdf:first>
    <rdf:rest>
      <rdf:List>
        <rdf:first rdf:datatype="&xsd;String"/>"compact"</rdf:first>
        <rdf:rest rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#nil"/>
      </rdf:List>
    </rdf:rest>
  </rdf:List>
</owl:oneOf>
:END

(process-attributes-one-of-values 
 '((:en "dispersed" :fr "dispersé; étalé")
   (:en "compact; dense" :fr "serré")) "hasStyle" "String")
(rp *)
<owl:oneOf>
  <rdf:List>
    <rdf:first rdf:datatype="&xsd;String"/>"dispersed"</rdf:first>
    <rdf:rest>
      <rdf:List>
        <rdf:first rdf:datatype="&xsd;String"/>"compact"</rdf:first>
        <rdf:rest>
          <rdf:List>
            <rdf:first rdf:datatype="&xsd;String"/>"dense"</rdf:first>
            <rdf:rest rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#nil"/>
          </rdf:List>
        </rdf:rest>
      </rdf:List>
    </rdf:rest>
  </rdf:List>
</owl:oneOf>
:END
|#
;;;--------------------------------------------- PROCESS-ATTRIBUTES-ONE-OF-LIST

(defun process-attributes-one-of-list (one-of-list xsd-range-type)
  "builds a list of codes recursively starting at the <rest> level. When we ~
   reach the end of the list we relace value with the nil special marker
Argument:
   one-of-list: list of values to add
   value-type: type of the value to add (xsd type string)
Return:
   a list of OWL codes to be printed."
  (declare (special *left-margin* *indent-amount*))
  ;; construct list
  `(
    ;; start with List class
    ,(indent *left-margin* "<rdf:List>")
    ,(indent (incf  *left-margin* *indent-amount*)
             (format nil "<rdf:first rdf:datatype=\"&xsd;~A\"/>~S</rdf:first>" 
                     xsd-range-type
                     (car one-of-list)))
    ;; start the rest unless we have no more value to include
    ,@(process-attributes-one-of-list-rest (cdr one-of-list) xsd-range-type)
    ;; return on the rest
    ,(indent (decf *left-margin* *indent-amount*) "</rdf:List>")
    ;; 
    ))

(defun process-attributes-one-of-list-rest (one-of-list xsd-range-type)
  "builds the rest of the OWL list. Calls process-attributes-one-of-list recursively.
Argument:
   one-of-list: list of values to add
   value-type: type of the value to add
Return:
   a list of OWL codes to be printed."
  (if one-of-list
    `(,(indent *left-margin* "<rdf:rest>")
      ,@(prog2  ; used for indenting
         (rtab)
         (process-attributes-one-of-list one-of-list xsd-range-type)
         (ltab))
      ,(indent *left-margin* "</rdf:rest>"))
    ;; otherwise terminal state
    (list
     (indent 
      *left-margin* 
      "<rdf:rest rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#nil\"/>")
     )))

#|
(process-attributes-one-of-list '(1 2 3) "integer")

(rp (reverse *))
<rdf:List>
  <rdf:first rdf:datatype="&xsd;integer"/>1</rdf:first>
  <rdf:rest>
    <rdf:List>
      <rdf:first rdf:datatype="&xsd;integer"/>2</rdf:first>
      <rdf:rest>
        <rdf:List>
          <rdf:first rdf:datatype="&xsd;integer"/>3</rdf:first>
          <rdf:rest rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#nil"/>
        </rdf:List>
      </rdf:rest>
    </rdf:List>
  </rdf:rest>
</rdf:List>
:END
|#
;;;------------------------------------------------------------ PROCESS-INDEXES
;;; JPB051009

(defun process-indexes ()
  "takes *index-list* and processes each entry in turn.
An entry of the individual list is for example:
   (\"z-pyrénéesAtlantiques\" \"Z-Département\" (:FR \"pyrénées atlantiques\") (\"code\" \"64\")
  (\"région\" \"aquitaine\"))
Arguments:
   none
Return:
   list of OWL specs."
  (declare (special *index-list* *errors-only* *separation-width*))
  (let (result entry-list prop-string)
    ;; sort index list
    (setq *index-list* (sort *index-list* #'string-lessp))
    (mark-section "Indexes" *separation-width*)
    ;; for each entry of the index list build an index 
    (dolist (index *index-list* )
      ;; get an a-list like ((:EN "hasFirstName")(:FR "..." "..."))
      (setq entry-list (index-get index :idx))
      ;; print when verbose
      (vformat "~&===== idx: ~S" index)
      ;; if nil error
      (if (null entry-list)
        (twarn "Bad index entry: ~S" index)
        ;; otherwise set up index entry
        (progn
          (cformat "<Index rdf:ID=~S>" index)
          (rtab)
          ;; and process each entry in turn
          (dolist (pair entry-list)
            ;; for each pair 
            ;; get the language property
            (setq prop-string (cdr (assoc (car pair) *language-index-property*)))
            ;; for each name build an index object
            (dolist (val (cdr pair))
              (cformat "<~A rdf:resource=\"#~A\"/>" prop-string val)))
          ;; close index entry
          (ltab)
          (cformat "</Index>")
          ;; add blank line
          (cformat " "))))
    (mark-section "End Indexes" *separation-width*)
    (unless *errors-only* (rp result))
    ;; return
    result
    ))

#|
? (index)
("FirstName" ((:IDX (:EN "hasFirstName")) (:ATT (:ID "hasFirstName")))) 
("GivenName" ((:IDX (:EN "hasFirstName")) (:ATT (:ID "hasFirstName")))) 
("Prénom" ((:IDX (:FR "hasFirstName")) (:ATT (:ID "hasFirstName")))) 
:END
? *index-list*
("FirstName" "GivenName" "Prénom")
? (process-indexes)
("   " "  </Index>" "    <isFrIndexOf rdf:resource=\"hasFirstName\"/>"
 "  <Index rdf:ID=\"Prénom\">" "   " "  </Index>"
 "    <isEnIndexOf rdf:resource=\"hasFirstName\"/>" "  <Index rdf:ID=\"GivenName\">"
 "   " "  </Index>" "    <isEnIndexOf rdf:resource=\"hasFirstName\"/>"
 "  <Index rdf:ID=\"FirstName\">")
? (rp *)
  <Index rdf:ID="FirstName">
    <isEnIndexOf rdf:resource="#hasFirstName"/>
  </Index>

  <Index rdf:ID="GivenName">
    <isEnIndexOf rdf:resource="#hasFirstName"/>
  </Index>

  <Index rdf:ID="Prénom">
    <isFrIndexOf rdf:resource="#hasFirstName"/>
  </Index>

:END
|#
;;;-------------------------------------------------------- PROCESS-INDIVIDUALS

(defun process-individuals ()
  "takes *individual-list* and processes each entry checking validity of options.
An entry of the individual list is for example:
   (\"z-Ind-24\" \"Z-City\" (:name \"Ind-24\")
      (\"name\" (:EN \"Paris\" :FR \"Paris; Lutèce\")))
Arguments:
   none
Return:
   list of OWL specs."
  (declare (special *language* *separation-width* *individual-list*))
  ;(break "process-individuals/*individual-list*: ~%  ~S" *individual-list*)
  (let (result ind-fragment ind-names id class-id option-list message index-string)
    (mark-section "Individuals" *separation-width*)
    (setq *individual-list* (reverse *individual-list*))
    
    ;;=== Inverse links are not part of OWL formalism
;;;    ;;=== Establish the inverse links.
;;;    (dolist (individual *individual-list*)
;;;      ;; for each description do some checking
;;;      ;; format: (<class-id> <ind-id> <ind-names> {<option>}* )
;;;      (setq id (car individual)
;;;          option-list (cdddr individual))
;;;      ;; check properties
;;;      (dolist (option option-list)
;;;        ;; consider only relations
;;;        (when (setq rel-id (car (index-get (make-index-string (car option)) 
;;;                                           :rel :id)))
;;;          ;; for each successor, check existence
;;;          ;; get inverse relation id
;;;          (setq inv-id (car (index-get rel-id :rel :inv)))
;;;          (dolist (value (cdr option))
;;;            ;; get successor id from *index*
;;;            (setq suc-id (car (index-get (make-index-string value) :inst :id)))
;;;            ;; if there, record the inverse link, regardless of what suc-id is
;;;            (if suc-id
;;;              (index-add suc-id :inst :inv (cons inv-id id)))
;;;            ))))
    
    ;;=== then build the objects
    (loop
      (setq 
       message
       (catch :error
         (unless *individual-list*
           (mark-section "End Individuals" *separation-width*)
           (rp result)
           (return-from process-individuals result))
         ;; get next individual description
         (setq ind-fragment (pop *individual-list*))
         ;; format: (<class-id> <ind-id> <ind-names> {<option>}* )
         (setq id (car ind-fragment)
               class-id (cadr ind-fragment)
               ind-names (caddr ind-fragment)
               option-list (cdddr ind-fragment))
         ;; print when verbose
         (vformat "~&===== ind: ~S" id)
         ;; build index entries (done in make-individual)
         ;(+result (make-indices ind-names id :inst))
         ;; start building the output
         (cformat "<~A rdf:ID=~S>" class-id id)
         (rtab)
         ;; should make some labels
         (+result (make-labels ind-names))
         ;; process each option
         (dolist (option option-list)
           (cond
            ;; if option is :doc do as usual
            ((eql (car option) :doc)
             (+result (make-doc (cdr option))))
            ;; if option is :var create an index onto the object
            ((eql (car option) :var)
             (setq index-string (make-index-string (cadr option)))
             (index-add  index-string :INST :id id)
             (index-add index-string :idx *language* id)
             ;; do not index variables
             ;(pushnew index-string *index-list* :test #'string-equal)
             ;(format t "~%; process-individuals /:var option: ~S" (cadr option))
             ;(index)
             )
            ;;=== if option is an attribute process-individuals-attribute
            ((index-get (make-index-string (car option)) :att)
             (+result (process-individuals-att id class-id option)))
            ;;=== if relation same approach
            ((index-get (make-index-string (car option)) :rel)
             (+result (process-individuals-rel id class-id option)))
            ;; otherwise ignore option
            (t
             (twarn ";***** Warning: unknown option: ~S" option)
             )))
         ;; add all inverse relations
;;;         (dolist (pair (index-get id :inst :inv))
;;;           (cformat "<~A rdf:resource=\"#~A\"/>" (car pair)(cdr pair))
;;;           )
         ;; end of object
         (ltab)
         (cformat "</~A>" class-id)
         (push " " result)
         ;; then check for possible local property restrictions
         result))
      
      (if (stringp message)
        (twarn ";***** When defining individual ~S: from ~S, result:~&  ~A~%~S" 
               id (car ind-fragment) result message)
        ;; otherwise print result
        (unless *errors-only* (rp result)))
      
      ;; result
      (setq result nil)
      )))

#|
(let ((*individual-list* 
       '(("z-jpb" "Z-Person" (:FR "jpb") ("gender" "masculin")))))
  (process-individuals))

<Z-Person rdf:ID="z-jpb">
<rdfs:label xml:lan="fr">jpb</rdfs:label>
<hasGender rdf:datatype="http://www.w3.org/2001/XMLSchema#STRING">masculin</hasGender>
</Z-Person>
|# 
;;;---------------------------------------------------- PROCESS-INDIVIDUALS-ATT
#|
When using datatypes, please note that even if a property is defined to have a
range of a certain datatype, RDF/XML still requires that the datatype be 
specified each time the property is used. An example could be the declaration 
of a property that we used earlier in the Measurement example:

<owl:DatatypeProperty rdf:about="#timeStamp">
  <rdfs:domain rdf:resource="#Measurement"/>
  <rdf:range rdf:resource="&xsd;dateTime"/>
</owl:DatatypeProperty>

<Measurement>
  <timeStamp rdf:datatype="&xsd;dateTime">2003-01-24T09:00:08+01:00</timeStamp>  
</Measurement>
|#

(defun process-individuals-att (id class-id option)
  "processes an attribute option of an individual. Takes each option and tries ~
   to make the best out of it.
Arguments:
   id: OWL ID of the individual
   class-id: OWL ID of the class of the individual
   option: attribute option e.g. (\"first name\" \"Albert\" \"jules\")
Return:
   a list of OWL spec
Error:
   throws to an :error tag"
  (declare (special *language*))
  (let (result xsd-att-type att-one-of att-id att-type local-restrictions
               entry? index-string val-list)
    ;; get attribute id
    (setq att-id (car (index-get (make-index-string (car option)) :att :id)))
    (unless att-id
      (terror "Unknow atribute; ID not in *index* table at entry ~S" 
              (car option)))
    ;; check if attribute OWL ID is in the index
    (unless (index-get att-id)
      (terror "Attribute ~S has no entry in the *index* table." att-id))
    
    ;(break "process-individuals-att")
    
    
    ;; check now for local options, i.e. cardinality
    (setq local-restrictions (car (index-get att-id :att class-id)))
    ;(format t "~%; process-individuals-att /local-restrictions: ~S" local-restrictions)
    (dolist (restriction local-restrictions)
      (case (car restriction)
        ;; min
        (:min
         (if (< (length (cdr option)) (cadr restriction))
             (terror "not enough values for ~S in ~S" (car option) id)))
        ;; max
        (:max
         (if (> (length (cdr option)) (cadr restriction))
             (terror "too many values (~S) for ~S in ~S" 
                     (length (cdr option)) (car option) id)))
        ;; unique
        (:unique
         (if (not (eql (length (cdr option)) 1))
             (terror "~S in ~S should have a unique value." (car option) id)))
        
        ;; index/entry if there, attribute values are indexes
        ((:index :entry)
         (setq entry? t))
        ))
    
    ;; get attribute type (default is string)
    (setq att-type (or (car (index-get att-id :att :type)) :string))
    ;(format t "~%; process-individuals-att /att-type: ~S" att-type)
    
    ;; if type is :mln set xsd to :string
    (setq xsd-att-type
          (if (eql att-type :mln)
              :string
            (get-xsd-attribute-type att-type att-id id)))
    
    ;; test if option is associated with :one-of for this class from e.g.
    ;; ("hasStyle"
    ;;  ((:ATT
    ;;    ("Z-Hamlet"
    ;;      ((:ONE-OF "dispersed" "compact") (:DOC :FR "Style de petits villages")))
    ;;    ("Z-City"
    ;;      ((:ONE-OF "aristocratique" "ouvrière" "bourgeoise")
    ;;       (:DOC :FR "Style d'agglomérations")))
    ;;    (:RANGE :STRING) (:DOMAIN ("Z-City" "Z-Hamlet"))
    ;;    (:NAME ((:EN "style") (:FR "sorte"))))))
    ;(index)
    ;(format t "~%; process-individuals-att / att-one-of: ~S"
    ;  (index-get att-id :att class-id))
    
    (when
        ;; get description for this class (((:ONE-OF ...)(:dOC ...)))
        (setq att-one-of (index-get att-id :att class-id))
      ;; extract one-of clause => (:ONE-OF ...)
      (setq att-one-of (cdr (assoc :one-of (car att-one-of)))))
    
    ;(format t "~%; process-individuals-att / value: ~S" (cdr option))
    ;(format t "~%; process-individuals-att / att-one-of: ~S" att-one-of) 
    ;(break "att-one-of / att-id: ~S" att-id)
    
    ;; when option is there check values
    (cond
     ((and att-one-of
           (every #'(lambda(xx) (member (if (stringp xx) (make-value-string xx) xx)
                                        att-one-of :test #'moss::equal+))
                  (cdr option)))
      ;; build each value (cdr option) is either a list of values or an MLN
      (setq val-list
            (if (is-mln? (cadr option))
                (mln::filter-language (cadr option) *language*)
              (cdr option)))
      
      ;(format t "~%; process-individuals-att / val-list: ~S" val-list)
      (break "att-one-of / att-id: ~S" att-id)
      
      (dolist (value val-list)
        (cformat "<~A rdf:datatype=\"http://www.w3.org/2001/XMLSchema#~A\">~A</~A>"
                 att-id xsd-att-type value att-id)))
     ;; otherwise, if (cdr option) is not one of the values, complain
     (att-one-of
      (terror "Some value in ~S is not one of ~&~S" option att-one-of))
     
     ;; when values are not prespecified do brute-force spec
     (t
      ;; if value is an MLN set option to the list of values associated with
      ;; *language*
      ;(format t "~%; process-individuals-att /option 1: ~S" (cadr option))
      (if (is-mln? (cadr option))
          (setq option (cons *language* 
                             (mln::filter-language (cadr option) *language*))))
      ;(format t "~%; process-individuals-att /option 2: ~S" option)
      
      (dolist (value (cdr option))
        ;; whenever the attribute is an entry point, build the index references
        (when entry?
          (setq index-string (make-index-string value))
          (index-add  index-string :INST :id id)
          (index-add index-string :idx *language* id)
          (pushnew index-string *index-list* :test #'string-equal)
          ;(format t "~%; process-individuals-att / att: ~S entry ~S" 
          ;  att-id value)
          ;(index)
          )
        (cformat "<~A rdf:datatype=\"http://www.w3.org/2001/XMLSchema#~A\">~A</~A>"
                 att-id xsd-att-type value att-id))))
    ;; return spec list
    result))

#|
? (index)
("Clothes" ((:CLASS (:NAME (:EN "clothes"))))) 
("Size" ((:ATT (:ID "hasSize")))) 
("hasSize"
 ((:ATT ("Clothes" (:MAX 1) (:MIN 1)) (:ONE-OF "Small" "Medium" "Large")))) 
NIL

? (catch :error (process-individuals-att "Chemises" "Z-Clothes" '("Size" " medium" "large ")))
"too many values (2) for \"Size\" in \"Chemises\""

? (catch :error (process-individuals-att "Chemises" "Clothes" '("Size" " medium" )))
("<hasSize rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">\" medium\"</hasSize>")

? (catch :error (process-individuals-att "z-pyrénéesAtlantiques" "Département" 
                                         '("code" "64")))
("  <hasCode rdf:datatype=\"http://www.w3.org/2001/XMLSchema#STRING\">64</hasCode>")

(progn
  (index-clear)
  (defconcept (:en "city" :fr "ville")
      (:att (:en "name" :fr "nom"))
    )
  (index)
  (defindividual "city" ("name" "Paris" "Lutèce"))
  )
|#
;;;---------------------------------------------------- PROCESS-INDIVIDUALS-REL
;;; from the option, ome must check:
;;;  - that the values are instances of the successor class
;;;  - that they belong to the :one-of option
;;;  - that the values exist and are instances of the right class
;;;  - that the number of values respects the cardinamity constraints
;;; the necessary information is in the index table
;;;  - a value is a reference to an object (entry point or variable)
;;;  - card conditions are associated with the rel-id in the index table
;;;  - :one-of restriction is in the list of options

(defun process-individuals-rel (ind-id class-id option)
  "processes a relation option. Takes each option and tries to make the best out ~
   of it. Throws to :error if relation not available for object class.
Arguments:
   ind-id: OWL ID of the individual e.g. \"z-ind-234\"
   class-id: OWL ID of the class of the individual, e.g. \"Z-Ville\"
   option: rel option e.g. (\"département\" \"Gironde\" \"Landes\")
Return:
   a list of OWL spec to add to instance specs
Error:
   throws to an :error tag"
  (let (result rel-id option-list range suc-list)
    
    ;(break "process-individuals-rel")
    
    ;; get relation id from name ("brother") and class ("Z-Person")
    ;; should work for "brother" and "Z-Student" subclass of "Z-Person"
    ;; throws to :error if it cannot find it
    ;(setq rel-id (car (index-get (make-index-string (car option)) class-id :id)))
    (setq rel-id 
          (get-relation-id-for-class (make-index-string (car option)) class-id))
    
    ;; get the class id of the successors
    (setq range (car (index-get rel-id :rel :range)))
    
    ;; check if successors are of the right class, or obey the :one-of option
    (dolist (suc-id (cdr option))
      (pushnew (check-rel-class suc-id range) suc-list))
    
    ;; must now check the various possibilities for the :one-of option
    (check-rel-one-of suc-list option-list rel-id ind-id)
    
    ;; check now that property values check property restrictions, i.e. cardinality
    (setq option-list (car (index-get rel-id :rel :options)))
    ;; check cardinality restrictions, if not OK throws to :error
    (check-rel-cardinality (cdr option) option-list rel-id ind-id)
    
    ;; here, all constraints verified, we add property  and values
    (dolist (suc-id suc-list)
      (cformat "<~A rdf:resource=\"#~A\"/>" rel-id suc-id))
    result))

#|
? (process-individuals-rel "pyrénéesAtlantiques" "Département" '("région" "aquitaine"))
("<hasRegion rdf:resource=\"#z-aquitaine\"/>")
? (catch :error (process-individuals-rel "pyrénéesAtlantiques" "Département" 
                                         '("ville" "aquitaine")))
NIL
? (errors)

***** Error: individual "aquitaine" is not of type ("Z-City")
when populating "hasTown" relation for "pyrénéesAtlantiques"
:END
|#
;;;---------------------------------------------------------- PROCESS-RELATIONS
;;; adding the inverse relations. Assumes that an owl:inverse exists (OWL extension)

(defun process-relations ()
  "takes *relation-list* and build the relation structures. A given relation ~
      is composed of fragments when used in different classes. Such fragments must ~
      be checked for consistency.
   - all names are merged and one is chosen as the OWL ID
   - domains are collected
   - ranges are collected
   - if both domain and range are multiple an error is declared
  - inverse properties are created automatically
   - :one-of must indicates instances
   In the last two cases, subproperties must be defined in case of conflict.
   Examples of format
      (:rel (:en \"brother\" :fr \"frère\")(:type \"person\"))
      (:rel (:en \"employer\" :fr \"employeur\") (:type \"organisation\"))
      (:rel (:en \"employer\" :fr \"employeur\") (:type \"person\"))
      (:rel (:en \"sex; gender\" :fr \"sexe\") (:unique)
              (:one-of (:en \"male\"  :fr \"masculin\") (:en \"female\" :fr \"féminin\"))))
   Formalisme:
      (:rel <MLS> {<suc-type>} {<card-constraint>} {<suc-one-of>})
Arguments:
   none
Return:
   list of OWL spec to be printed
Error:
   throws to :error tag."
  (declare (special *relation-list* *separation-width*))
  (let (result rel-fragments rel-mln message rel-id)
    (mark-section "Object Properties" *separation-width* )
    
    (loop
      (setq 
       message
        (catch 
         :error
         
         (unless *relation-list*
           (cformat "~%")
           (mark-section "End Object Properties" *separation-width*)
           (rp result)
           (return-from process-relations nil))
         
         ;; get fragments corresponding to next relation
         ;; returns an a-list (<name-list> ("Z-Person" . <att def>)(...))
         (setq rel-fragments (extract-relation-fragments))
         ;; gather all names
         (setq rel-mln (pop rel-fragments))
         
         ;; make entries into the index table
         (setq rel-id (make-relation-id rel-mln))
         (index-add rel-id :rel :name rel-mln)
         
         (vformat "~&===== rel: ~S" rel-id)
         ;(print rel-fragments)
         ;(break "process-relations 1")
         
         ;; Then process each fragment in turn, applying local restrictions
         (dolist (fragment rel-fragments)
           ;; skip the mln name
           (index-add rel-id :rel (car fragment) (cddr fragment))
           ;; if local options, build OWL about clause
           (+result (process-relations-class-restrictions fragment rel-mln))
           )
         result))
      ;; print each relation in turn
      (if (stringp message)
        (twarn "~&;***** Error in:~S; ~A" rel-mln message)
        ;; otherwise print result
        (unless *errors-only* (rp result)))
      ;; reset result
      (setq result nil)
      ) ; end of loop on fragments
    ))

#|
(setq *relation-list* 
        '(("Z-Conurbation" (:NAME :EN "town" :FR "ville") (:TO "city"))))

(process-relations)

<!-- ++++++++++++++++++++++++++++++ Object Properties +++++++++++++++++++++++++++++++ -->

<owl:ObjectProperty rdf:ID="hasConurbationVille">
  <rdfs:label xml:lang="FR">ville</rdfs:label>
  <rdfs:domain rdf:resource="#Z-Conurbation"/>
  <rdfs:range rdf:resource="#Z-Ville"/>
</owl:ObjectProperty>
 
<owl:Class rdf:about="#Z-Conurbation">
  <rdfs:subClassOf>
    <owl:Restriction>
      <owl:onProperty rdf:resource="#hasConurbationVille"/>
    </owl:Restriction>
  </rdfs:subClassOf>
</owl:Class>

<!-- ++++++++++++++++++++++++++++ End Object Properties +++++++++++++++++++++++++++++ -->
|#
;;;---------------------------------------- PROCESS-RELATIONS-CLASS-RESTRICTIONS
;;; restrictions are only on the direct property

(defun process-relations-class-restrictions (fragment rel-mln)
  "for each relation process local restrictions, namely cardinality conditions.
Arguments:
   fragment: relation fragment, e.g. 
        (\"Z-Tiger\" (:FR \" frère \" :IT \"fratello\") \"person\" (:MIN 1) )
   rel-mln: 
Return:
   a list of OWL specs.
Side-effects:
   a specific entry on the local relation id is created to allow create instances
   e.g. (\"hasVillePays\" (:rel (:domain \"Z-Ville\")(:range \"Z-Pays\")
                                (:options ((:one-of \"Paris\" \"Rome\")(:unique)
                                           (:forall)))))"
  (declare (special *language*))
  ;; get relations SOL spec, removing the :rel tag
  (let ((rel-spec (cddr fragment))
        (class-id (car fragment))
        (language *LANGUAGE*)
        result spec spec-max doc one-of id range-spec range)
    
    ;;===== define local property
    (setq id (make-relation-id rel-mln (subseq class-id 2)))
    ;; domain is class-id
    ;; get range spec
    (setq range-spec (cadr (assoc :to rel-spec)))
    (setq range (car (index-get (make-index-string range-spec) :class :id)))
    
    (unless range
      (terror "Bad range ~S for relation ~S" range-spec id))
    ;; record domain and range to be used when making instances
    (index-add id :rel :domain class-id)
    (index-add id :rel :range range)
    (index-add id :rel :options rel-spec)
    
    ;;===== OK build OWL output
    (cformat "<owl:ObjectProperty rdf:ID=\"~A\">" id)
    (rtab)
    ;; get all the labels for the current language
    (dolist (item (mln::filter-language rel-mln language))
      (cformat "<rdfs:label xml:lang=\"~A\">~A</rdfs:label>" language item))
    (cformat "<rdfs:domain rdf:resource=\"#~A\"/>" class-id)
    (cformat "<rdfs:range rdf:resource=\"#~A\"/>" range)
    (ltab)
    (cformat "</owl:ObjectProperty>")
    (push " " result)
    
    ;(break "process-relations-class-restrictions")
    
    ;;===== add restriction for the specific class
    ;; check for cardinality conditions
    (if
        ;; uniquenes
        (assoc :unique rel-spec)
        (setq spec
              "<owl:cardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:cardinality>"
            )
      ;; else chack min AND max options
      (progn
        ;; min
        (if (assoc :min rel-spec)
            (setq spec
                  (format
                      nil
                      "<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:minCardinality>"
                    (cadr (assoc :min rel-spec)))))
        ;; max
        (if (assoc :max rel-spec)
            (setq spec-max
                  (format
                      nil
                      "<owl:maxCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:maxCardinality>"
                    (cadr (assoc :max rel-spec)))))))
    
    ;; doc?
    (setq doc (cdr (assoc :doc rel-spec)))
    ;; one-of?
    (setq one-of (cdr (assoc :one-of rel-spec)))
    
    ;; if we got something then
    ;; set up the stage
    ;(push " " result)
    (cformat "<owl:Class rdf:about=\"#~A\">" class-id)
    (rtab)
    (cformat "<rdfs:subClassOf>")
    (rtab)
    ;; must get domain
    
    ;; and range
    (cformat "<owl:Restriction>")
    (rtab)
    (cformat "<owl:onProperty rdf:resource=\"#~A\"/>" id)
    (if spec (cformat spec))
    (if spec-max (cformat spec-max))
    ;; check doc option
    (if doc (+result (make-doc doc)))
    ;; check one-of option
    (if one-of
        (+result (process-relations-one-of-values one-of id)))
    (ltab)
    (cformat "</owl:Restriction>")
    (ltab)
    (cformat "</rdfs:subClassOf>")
    (ltab)
    (cformat "</owl:Class>" class-id)
    result))

#|
(process-relations-class-restrictions 
 '("Z-Ville" (:EN "country" :FR "pays") (:TO "Country"))
 '(:EN "country" :FR "pays"))
(rp *)

<owl:ObjectProperty rdf:ID="hasVillePays">
  <rdfs:label xml:lang="FR">pays</rdfs:label>
  <rdfs:domain rdf:resource="Z-Ville"/>
  <rdfs:range rdf:resource="#Z-Pays"/>
</owl:ObjectProperty>
 
<owl:Class rdf:about="#Z-Ville">
  <rdfs:subClassOf>
    <owl:Restriction>
      <owl:onProperty rdf:resource="#hasVillePays"/>
    </owl:Restriction>
  </rdfs:subClassOf>
</owl:Class>

(process-relations-class-restrictions 
 '("Z-Ville" (:EN "country" :FR "pays") (:TO "Country")(:one-of "france" "italie")
   (:max 1))
 '(:EN "country" :FR "pays"))
(rp *)

<owl:ObjectProperty rdf:ID="hasVillePays">
  <rdfs:label xml:lang="FR">pays</rdfs:label>
  <rdfs:domain rdf:resource="Z-Ville"/>
  <rdfs:range rdf:resource="#Z-Pays"/>
</owl:ObjectProperty>
 
<owl:Class rdf:about="#Z-Ville">
  <rdfs:subClassOf>
    <owl:Restriction>
      <owl:onProperty rdf:resource="#hasVillePays"/>
      <owl:maxCardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:maxCardinality>
      <owl:allValuesFrom>
        <owl:Class>
          <owl:oneOf rdf:parseType="Collection">
            <owl:Thing rdf:about="#z-ind-32"/>
            <owl:Thing rdf:about="#z-ind-33"/>
          </owl:oneOf>
        </owl:Class>
      </owl:allValuesFrom>
    </owl:Restriction>
  </rdfs:subClassOf>
</owl:Class>
:END
|#
;;;-------------------------------------------- PROCESS-RELATIONS-ONE-OF-VALUES
#|
    <owl:Restriction>
      <owl:onProperty rdf:resource="#hasContinent"/>
      <owl:maxCardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:maxCardinality>
      <owl:allValuesFrom>
        <owl:Class>
          <owl:oneOf rdf:parseType="Collection">
            <owl:Thing rdf:about="#Eurasia"/>
            <owl:Thing rdf:about="#Africa"/>
            ...
|#

(defun process-relations-one-of-values (value-list rel-id)
  "we build a list of OWL specs, given that each fragment corresponds to a ~
   different class.
Arguments:
   value-list: list of object references
   rel-id: e.g. \"hasPays\"
Return:
   a list of OWL specs."
  (declare (ignore rel-id))
  (let (result suc-id)
    (cformat "<owl:allValuesFrom>")
    (rtab)
    (cformat "<owl:Class>")
    (rtab)
    (cformat "<owl:oneOf rdf:parseType=\"Collection\">")
    (rtab)
    
    ;(break "process-relations-one-of-values")
    
    ;; start the rest unless we have no more value to include
    (dolist (suc value-list)
      (setq suc-id (car (index-get (make-value-string suc) :inst :id)))
      (when suc-id
        (cformat "<owl:Thing rdf:about=\"#~A\"/>" suc-id)))
    ;; return on the rest
    (ltab)    
    (cformat "</owl:oneOf>")
    (ltab)
    (cformat "</owl:Class>")
    (ltab)
    (cformat "</owl:allValuesFrom>")
    result))

#|
(process-relations-one-of-values '("Gaule" "Italy") "hasPays")
(rp *)
<owl:allValuesFrom>
  <owl:Class>
    <owl:oneOf rdf:parseType="Collection">
      <owl:Thing rdf:about="#z-ind-29"/>
      <owl:Thing rdf:about="#z-ind-30"/>
    </owl:oneOf>
  </owl:Class>
</owl:allValuesFrom>
|#
;;;------------------------------------------------- PROCESS-VIRTUAL-ATTRIBUTES
;;; JPB080111

(defun process-virtual-attributes ()
  "takes *virtual-attribute-list* and processes each entry in turn.
An entry of the individual list is for example:
;   (\"hasAge\"
;     ((:VATT 
;        (:TYPE :INTEGER)
;        (:DOC
;          (:EN \"the attribute age is computed from the birthdate of a ~
;               person by applying the function getYears to the birthdate.\"))
;        (:CLASS (\"person\")) 
;        (:NAME :EN \"age\" :FR \"âge\"))))
Arguments:
   none
Return:
   list of OWL specs."
  (let (result attribute-name domain domain-id-list type doc definition)
    ;; sort index list and print header
    (setq *virtual-attribute-list* (sort *virtual-attribute-list* #'string-lessp))
    (mark-section "Virtual Datatype Properties" *separation-width*)
    
    (unless *errors-only* (rp result))
    (rtab)
    
    ;; for each entry of the index list build an index 
    (dolist (vatt *virtual-attribute-list* )
      (with-warning (attribute-name)
        ;; get the a-list defining the attribute, e.g. 
        ;;   ((:NAME (:EN "hasFirstName" :FR "...")) (:DEF ...) ...)
        (setq definition (index-get vatt :vatt :def))
        ;(print `(++++++++++ definition of vatt ,definition))
        (setq attribute-name (cadr (assoc :name definition))
              domain (cdr (assoc :class definition))
              doc (cdr (assoc :doc definition))
              type (cadr (assoc :type definition))
              )
        ;; check errors
        (cond
         ((null attribute-name)
          (terror "Bad virtual attribute entry: ~S" vatt))
         ((null domain)
          (terror "Bad or missing virtual :class option."))
         )
        
        ;; build the list of domain ids
        (setq domain-id-list (mapcar #'(lambda (xx) 
                                         (make-concept-id 
                                          (list *language* xx)))
                                     domain))
        ;; set up index entry
        ;; print when verbose
        (vformat "~&===== att: ~S" vatt)
        ;; cook up the OWL description
        (+result
         (make-owl-virtual-attribute vatt attribute-name domain-id-list type doc))
        
        ;; process local cardinality options (restrictions)
        (dolist (class-id domain-id-list)
          (+result 
           (process-attributes-class-restrictions  
            ;; build a fragment with the right format
            (list* class-id attribute-name 
                   (remove :name definition :key #'car)))))
        
        ;;=== produce attribute rule
        ;; call the function building the rule (in the sol2rule file)
        (apply #'make-virtual-attribute-rule attribute-name definition)
        ;; add blank line
        (cformat " ")
        )
      ;; reset result for each attribute
      (setq result nil))
    
    ;; before closing the section process virtual composed attributes
    (setq result nil)
    (process-virtual-composed-attributes)    
    
    (ltab)
    ;; add blank line
    (mark-section "End Virtual Datatype Properties" *separation-width*)
    (unless *errors-only* (rp result))
    
    ;; return
    :done
    ))

#| (index-get "hasAge" :vatt :def)
(let ((*compiler-pass* 1))
  (reset)
  (setq *cverbose* t)
  (setq *user-defined-rule-operators* '((:getyears . "getYears")))
  
  (defconcept (:en "Person" :fr "Personne")
      (:att (:en "birth date" :fr "date de naissance") (:type :integer)))
  
  (defvirtualattribute (:name :en "age" :fr "âge")
      (:class "Person")
    (:def
        (?class "birth date" ?y)
        (:fcn "getYears" ?y)
      )
    (:type :integer)
    (:min 1)
    (:doc :en "the attribute age is computed from the birthdate of a ~
               person by applying the function getYears to the birthdate."))
  (index))

("Age" ((:IDX (:EN "hasAge")) (:VATT (:ID "hasAge")))) 
("Person" ((:IDX (:EN "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Personne" ((:IDX (:FR "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Z-Person" ((:CLASS (:NAME :EN "person" :FR "personne")))) 
("hasAge"
 ((:VATT
   (:DEF (:NAME (:EN "age" :FR "âge")) (:CLASS "person")
    (:DEF (?CLASS "birth date" ?Y) ("getYears" ?Y ?*)) (:TYPE :INTEGER) (:MIN 1)
    (:DOC :EN "the attribute age is computed from the birthdate of a ~
                 person by applying the function getYears to the birthdate."))))) 
("åge" ((:IDX (:FR "hasAge")) (:VATT (:ID "hasAge")))) 
:END

(let ((*compiler-pass* 2))
  (defconcept (:name :en "person" :fr "personne")
      (:att (:en "birth date" :fr "date de naissance")))
  (defvirtualattribute 
   (:name :en "age" :fr "âge")
   (:class "person")
   (:def
        (?class "birth date" ?y)
        (:fcn :getYears ?y)
      )
   (:type :integer)
   (:min 1)
   (:doc :en "the attribute age is computed from the birthdate of a ~
                 person by applying the function getYears to the birthdate.")))

(process-virtual-attributes)

<!-- +++++++++++++++++++++++++ Virtual Datatype Properties ++++++++++++++++++++++++++ -->


### the attribute age is computed from the birthdate of a person by applying the function getYears to the birthdate.

[Role_Age:
   (?SELF test4:hasAge ?RESULT)
<-
   (?SELF rdf:type test4:Z-Person)
   (?CLASS birth date ?Y)
   (getYears ?Y ?SELF)
 ]
  <VirtualDatatypeProperty rdf:ID="hasAge">
    <rdfs:label xml:lang="en">age</rdfs:label>
    <rdfs:label xml:lang="fr">âge</rdfs:label>
    <rdfs:domain rdf:resource="#Z-Person"/>
    <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#integer"/>
    <rdfs:comment xml:lang="en">the attribute age is computed from the birthdate of a person by applying the function getYears to the birthdate.</rdfs:comment>
  </VirtualDatatypeProperty>
 
  <owl:Class rdf:about="#Z-Person">
    <rdfs:subClassOf>
      <owl:Restriction>
        <owl:onProperty rdf:resource="#hasAge"/>
        <owl:minCardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:minCardinality>
      </owl:Restriction>
    </rdfs:subClassOf>
  </owl:Class>
   
<!-- +++++++++++++++++++++++ End Virtual Datatype Properties ++++++++++++++++++++++++ -->

:DONE

? (let ((*compiler-pass* 1))
    (make-virtual-attribute '(:name :en "French national" :fr "personne française")
                            '(:class "person")
                            '(:def
                               (?class "country" ?y)
                               (?result "name" :equal "france")
                               )
                            '(:type :boolean)
                            '(:doc :en "A French national is a person whose country is France.")))

? (index)
("FrenchNational"
 ((:IDX (:EN "hasFrenchNational")) (:VATT (:ID "hasFrenchNational")))) 
("PersonneFrancaise"
 ((:IDX (:FR "hasFrenchNational")) (:VATT (:ID "hasFrenchNational")))) 
("hasFrenchNational"
 ((:VATT
   (:DEF (:NAME (:EN "French national" :FR "personne francaise")) (:CLASS "person")
     (:DEF (?CLASS "country" ?Y) (?RESULT "name" :EQUAL "france")) (:TYPE :BOOLEAN)
     (:DOC :EN "A French national is a person whose country is France."))))) 
:END

? (process-virtual-attributes)

### A French national is a person whose country is France.

[Role_FrenchNational:
   (?SELF FrenchNational ?RESULT)
<-
   (?SELF rdf:type test4:Z-Person)
   (?CLASS test4:Z-Country ?Y)
   (?RESULT test4:hasName ?V1323)
   equal(?V1323, "france")
 ]
  <VirtualDatatypeProperty rdf:ID="hasFrenchNational">
    <rdfs:label xml:lang="en">French national</rdfs:label>
    <rdfs:label xml:lang="fr">personne francaise</rdfs:label>
    <rdfs:domain rdf:resource="#Z-Person"/>
    <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#boolean"/>
    <rdfs:comment xml:lang="en">A French national is a person whose country is France.</rdfs:comment>
  </VirtualDatatypeProperty>
   
|#
;;;---------------------------------------- process-virtual-composed-attributes

(defun process-virtual-composed-attributes ()
  "a virtual relation is one obtained by composition. Syntax is:
;   (defvirtualrelation <mln>
;     (:class <concept-ref>)
;     (:compose <relation-path>)
;     {(:to <range>)}
;     {(:min nn) and/or (:max nn) | (:unique)}
;    (:doc <documentation>))
The function processes each entry of the *virtual-relation-list*
Argument:
   none
Return:
   unimportant."
  (declare (special *language*))
  (let (result attribute-name domain domain-id-list doc definition type)
    ;; sort index list
    (setq *virtual-composed-attribute-list* 
          (sort *virtual-composed-attribute-list* #'string-lessp))
    ;; for each entry of the index list build an index 
    (dolist (vatt *virtual-composed-attribute-list*)
      
      (with-warning (attribute-name)
        ;; definition is an a-list like ((:EN "hasFirstName")(:FR "..." "..."))
        (setq definition (index-get vatt :vatt :def))
        ;(print `(++++++++++ definition ,definition))
        (setq attribute-name (cadr (assoc :name definition)))
        (unless attribute-name
          (terror "Bad virtual attribute entry: ~S" vatt))
        
        (setq  domain (cdr (assoc :class definition))
               type (cadr (assoc :type definition))
               doc (cdr (assoc :doc definition))
               )        
        (cond
         ((null domain)
          (terror "Missing :class option (domain)."))
         ((null type)
          (setq type :string)))
        
        ;; build the list of domain ids and range ids
        (setq domain-id-list (mapcar #'(lambda (xx) 
                                         (make-concept-id 
                                          (list *language* xx)))
                                     domain))
        ;; print attribute
        (vformat "~&===== att: ~S" vatt)
        ;; produce OWL output
        (cformat "<VirtualDatatypeProperty rdf:ID=~S>" vatt)
        (rtab)
        ;; process labels
        (+result (make-labels attribute-name))
        ;; domain is class option or domain of first class of compose
        (+result (make-domains domain-id-list))
        ;; process type and doc
        (+result (make-type type))           
        (+result (make-doc doc))           
        (ltab)
        ;; add blank line
        (cformat " ")
        
        (cformat "</VirtualDatatypeProperty>")
        
        ;;=== process local options if any (e.g. cardinality)
        (+result 
         ;;***** next function does not exist...
         (process-attributes-local-options  
          `(,(car domain-id-list) ,attribute-name ,@definition)
          vatt))           
        
        ;;=== make now rule (get the options back from the index :def entry)
        (apply #'make-virtual-relation-rule attribute-name definition)
        )
      ;; reset result for each entry
      (setq result nil))
    ;; return
    :done))

#|
? (let ((*compiler-pass* 1))
     (make-virtual-attribute
      '(:name :en "uncle-age" :fr "âge de l'oncle")
      '(:class "person")
      '(:compose "mother" "brother" "age")
      '(:type :non-negative-integer)
      '(:unique)
      '(:doc :en "test for virtual composed attribute.")))
:DONE

? (index)
(...
("Uncle-Age" ((:IDX (:EN "hasUncle-Age")) (:VATT (:ID "hasUncle-Age")))) 
("hasUncle-Age"
 ((:VATT
   (:DEF (:NAME (:EN "uncle-age" :FR "âge de l'oncle")) (:CLASS "person")
    (:COMPOSE "mother" "brother" "age") (:TYPE :NON-NEGATIVE-INTEGER) (:UNIQUE)
    (:DOC :EN "test for virtual composed attribute.")))))
 ("ågeDeLOncle" ((:IDX (:FR "hasUncle-Age")) (:VATT (:ID "hasUncle-Age")))
 ...)

? (process-virtual-composed-attributes)

### test for virtual composed attribute.

[Role_Uncle-Age:
   (?V1119 test4:hasUncle-Age ?V1122)
<-
   (?V1119 test4:hasMother ?V1120)
   (?V1120 test4:hasBrother ?V1121)
   (?V1121 test4:hasAge ?V1122)
 ]
  <VirtualDatatypeProperty rdf:ID="hasUncle-Age">
    <rdfs:label xml:lang="en">uncle-age</rdfs:label>
    <rdfs:label xml:lang="fr">aage de l'oncle</rdfs:label>
    <rdfs:domain rdf:resource="#Z-Person"/>
    <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#nonNegativeInteger"/>
    <rdfs:comment xml:lang="en">test for virtual composed attribute.</rdfs:comment>
   
  </VirtualDatatypeProperty>
  <owl:Class rdf:about="#Z-Person">
    <rdfs:subClassOf>
      <owl:Restriction>
        <owl:onProperty rdf:resource="#hasUncle-Age"/>
        <owl:cardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:cardinality>
      </owl:Restriction>
    </rdfs:subClassOf>
  </owl:Class>
|#
;;;--------------------------------------------------- PROCESS-VIRTUAL-CONCEPTS
;;; JPB071279
;;; because virtual concepts can make references to virtual properties, they must
;;; be processed after the properties have been processed

(defun process-virtual-concepts ()
  "takes *virtual-concept-list* and processes each entry in turn.
 An entry of the individual list is for example:
  (\"Z-Adult\"
   ((:VCLASS (:DOC (:EN \"An ADULT is a person over 18.\")) (:IS-A (\"person \"))
             (:NAME :EN \"Adult\" :FR \"Adulte\"))))
 Outputs the OWL definitions and calls the function to produce the rule.
Arguments:
   none
Return:
   list of OWL specs."
  (let (result superclass concept-name doc definition)
    ;; sort index list
    (setq *virtual-concept-list* (sort *virtual-concept-list* #'string-lessp))
    (mark-section "Virtual Classes" *separation-width*)
   
    ;; for each entry of the index list build an index 
    (dolist (vclass *virtual-concept-list* )
      (with-warning (concept-name)
        ;; get an a-list like ((:EN "hasFirstName")(:FR "..." "..."))
        (setq definition (index-get vclass :vclass :def)
              concept-name (cadr (assoc :name definition))
              superclass (cadr (assoc :is-a definition))
              doc (cdr (assoc :doc definition))
              )
        ;; print when verbose
        (vformat "~&===== idx: ~S" vclass)
        ;; if nil error
        (if (null concept-name)
          (twarn "Bad virtual class entry: ~S" vclass)
          ;; otherwise set up index entry
          (progn
            (cformat "<VirtualClass rdf:ID=~S>" vclass)
            (rtab)
            ;; now process labels
            (+result (make-labels concept-name))
            ;; if null superclass then issue a warning
            (if superclass
              (+result (make-subclass superclass vclass))
              (twarn "Missing superclass in virtual class entry: ~S" vclass))
            (+result (make-doc doc))
            ;; and process each entry in turn
            (ltab)
            (cformat "</VirtualClass>")
            ;; add blank line
            (cformat " ")
            
            ;;===== build rule 
            (apply #'make-virtual-concept-rule concept-name definition)
            )))
      ;; reset print list
      (setq result nil)
      )
    (mark-section "End Virtual Classes" *separation-width*)
    (unless *errors-only* (rp result))
    ;; return
    :done
    ))

#|
? (index-clear)
T
? (let ((*compiler-pass* 1))
     (make-concept '(:name :en "person" :fr "personne"))
     (make-virtual-concept '(:name :en "Adult" :fr "Adulte")
        '(:is-a  "person ")
        '(:def 
             (?* "age" > 18))
        '(:doc :en "An ADULT is a person over 18.")))
:DONE

? (let ((*compiler-pass* 2))
    (make-concept '(:name :en "person" :fr "personne"))
    )

<owl:Class rdf:ID="Z-Person">
  <rdfs:label xml:lang="en">person</rdfs:label>
  <rdfs:label xml:lang="fr">personne</rdfs:label>
</owl:Class>

? *virtual-concept-list*
("Z-Adult")

? (index)
("Adult" ((:IDX (:EN "Z-Adult")) (:VCLASS (:ID "Z-Adult")))) 
("Adulte" ((:IDX (:FR "Z-Adult")) (:VCLASS (:ID "Z-Adult")))) 
("Person" ((:IDX (:EN "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Personne" ((:IDX (:FR "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Z-Adult"
 ((:VCLASS
   (:DEF (:NAME (:EN "Adult" :FR "Adulte")) (:IS-A "person ") (:DEF (?* "age" > 18))
    (:DOC :EN "An ADULT is a person over 18."))))) 
("Z-Person" ((:CLASS (:NAME :EN "person" :FR "personne")))) 
:END

? (let ((*compiler-pass* 2))
        (process-virtual-concepts))
<!-- +++++++++++++++++++++++++++++++ Virtual Classes ++++++++++++++++++++++++++++++++ --!>

<owl:VirtualClass rdf:ID="Z-Adult">
  <rdfs:label xml:lang="en">Adult</rdfs:label>
  <rdfs:label xml:lang="fr">Adulte</rdfs:label>
  <rdfs:subClassOf rdf:resource="#Z-Person"/>
  <rdfs:comment xml:lang="en">An ADULT is a person over 18.</rdfs:comment>
</owl:VirtualClass>
  in (?SELF "age" > 18)

### An ADULT is a person of 18 years of age or more.

[Role_Adult:
   (?SELF rdf:type test4:Z-Adult)
<-
   (?SELF rdf:type test4:Z-Person)
   (?SELF test4:hasAge ?V1577)
   ge(?V1577, 18)
 ]
<!-- +++++++++++++++++++++++++++++ End Virtual Classes ++++++++++++++++++++++++++++++ --!>

:DONE
|#
;;;-------------------------------------------------- PROCESS-VIRTUAL-RELATIONS
;;; JPB080110
;;; process-virtual-relations actually create the OWL code corresponding to the
;;; virtual relation.
;;; a virtual relation cannot have the same internal name as a relation, attribute,
;;; or virtual attribute (or concept or virtual concept as a matter of fact).
;;; checks are made for domain and range, and local options 

(defun process-virtual-relations ()
  "a virtual relation is one obtained by composition. Syntax is:
   (defvirtualrelation <mln>
     (:class <concept-ref>)
      (:compose <relation-path>)
      {(:to <range>)}
      {(:min nn) and/or (:max nn) | (:unique)}
      (:doc <documentation>)
      )
   The function processes each entry of the *virtual-relation-list*
Arguments:
   none
Return:
   unimportant."
  (let (result relation-name range domain definition domain-id-list)
    ;; sort index list
    (setq *virtual-relation-list* (sort *virtual-relation-list* #'string-lessp))
    (mark-section "Virtual Object Properties" *separation-width*)
    ;; for each entry of the index list build an index 
    (dolist (vrel *virtual-relation-list* ) ; e.g. "hasUncle"
      (with-warning (relation-name)
        ;; definition is an a-list like ((:EN "hasFirstName")(:FR "..." "..."))
        (setq definition (index-get vrel :vrel :def))
        (setq
         relation-name (cadr (assoc :name definition))
          domain (cdr (assoc :class definition))
          range (cdr (assoc :to definition))
          )

        ;; build the list of domain ids and range ids
        (setq domain-id-list 
              (mapcar #'(lambda (xx)(make-concept-id (list *language* xx)))
                domain))
        ;; print when verbose
        (vformat "~&===== rel: ~S" vrel)
        
        (cond
         ;; check now domain and range
         ((null domain)
          (twarn "Missing :class option (domain) in virtual relation: ~S" vrel))
         ((null range)
          (twarn "Missing :to option (range) in virtual relation: ~S" vrel))
         )
        
        ;; if OK, process local cardinality options (restrictions)
        (dolist (class-id domain-id-list)
          (+result 
           (process-relations-class-restrictions    
            ;; build a fragment with the right format
            (list* class-id relation-name 
                   (remove :name definition :key #'car))
            relation-name)))
        (push " " result)
        )
      
      ;;===== make now rule (get the options back from the index :def entry)
      (apply #'make-virtual-relation-rule relation-name 
             (index-get (make-relation-id relation-name) :vrel :def))
    ;; reset result for next relation
    (setq result nil)
    )
  
  (mark-section "End Virtual Object Properties" *separation-width*)
  (unless *errors-only* (rp result))
  
  ;; return
  :done))

#| (index)

(apply #'make-virtual-relation-rule '(:EN "uncle" :FR "oncle") 
       (index-get (make-relation-id '(:EN "uncle" :FR "oncle")) :vrel :def))
(reset)

(let ((*compiler-pass* 1))
  (reset)
  (defconcept 
      (:name :en "Person" :fr "personne" :it "Persona")
      (:att (:en "name" :fr "nom") (:type :name)(:min 1))
    (:rel (:en "mother" :fr "mère") (:to "person") (:unique))  
    (:rel (:en "father" :fr "père") (:to "person") (:unique))
    (:rel (:en "brother" :fr "frère") (:to "person"))  (:rel (:en "country" :fr "pays")(:to "country"))
    (:doc :en "A Person is the generic class for human being. A person may be viewed as ~
             a citizen, employee, referent, etc."))
  (defvirtualrelation (:name :en "uncle" :fr "oncle")
      (:class "person")
    (:compose "mother" "brother")
    (:to "person")
    (:max 12)
    (:doc :en "an uncle is a person who is the brother of the mother."))
  (index))


(let ((*compiler-pass* 2))
    (defconcept 
      (:name :en "Person" :fr "personne" :it "Persona")
      (:att (:en "name" :fr "nom") (:type :name)(:min 1))
    (:rel (:en "mother" :fr "mère") (:to "person") (:unique))  
    (:rel (:en "father" :fr "père") (:to "person") (:unique))
    (:rel (:en "brother" :fr "frère") (:to "person"))  (:rel (:en "country" :fr "pays")(:to "country"))
    (:doc :en "A Person is the generic class for human being. A person may be viewed as ~
             a citizen, employee, referent, etc."))
  (defvirtualrelation (:name :en "uncle" :fr "oncle")
      (:class "person")
    (:compose "mother" "brother")
    (:to "person")
    (:max 12)
    (:doc :en "an uncle is a person who is the brother of the mother."))
  )

[...OWL output for class...]

? (index)
("Oncle" ((:IDX (:FR "hasUncle")) (:VREL (:ID "hasUncle")))) 
("Person" ((:IDX (:EN "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Persona" ((:IDX (:IT "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Personne" ((:IDX (:FR "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Uncle" ((:IDX (:EN "hasUncle")) (:VREL (:ID "hasUncle")))) 
("Z-Person" ((:CLASS (:NAME :EN "Person" :FR "personne" :IT "Persona")))) 
("hasUncle"
 ((:VREL
   (:DEF (:NAME (:EN "uncle" :FR "oncle")) (:CLASS "person")
    (:COMPOSE "mother" "brother") (:TO "person") (:MAX 12)
    (:DOC :EN "an uncle is a person who is the brother of the mother."))))) 
:END

? (progn (process-attributes)
         (process-relations))
[...OWL output for properties...]
  
? (index)
[... Index table contaning entries for properties...]
  
? (process-virtual-relations)

### an uncle is a person who is the brother of the mother.

[Role_Uncle:
   (?V1507 test4:hasUncle ?V1509)
<-
   (?V1507 test4:hasMother ?V1508)
   (?V1508 test4:hasBrother ?V1509)
 ]
<!-- ++++++++++++++++++++++++++ Virtual Object Properties +++++++++++++++++++++++++++ -->

<VirtualObjectProperty rdf:ID="hasUncle">
  <rdfs:label xml:lang="en">uncle</rdfs:label>
  <rdfs:label xml:lang="fr">oncle</rdfs:label>
  <rdfs:domain rdf:resource="#Z-Person"/>
  <rdfs:range rdf:resource="#Z-Person"/>
  <rdfs:comment xml:lang="en">an uncle is a person who is the brother of the mother.</rdfs:comment>
</VirtualObjectProperty>
 
<owl:Class rdf:about="#Z-Person">
  <rdfs:subClassOf>
    <owl:Restriction>
      <owl:onProperty rdf:resource="#hasUncle"/>
      <owl:maxCardinality rdf:datatype="&xsd;nonNegativeInteger">12</owl:maxCardinality>
    </owl:Restriction>
  </rdfs:subClassOf>
</owl:Class>
  
<!-- ++++++++++++++++++++++++ End Virtual Object Properties +++++++++++++++++++++++++ -->

:DONE

? (index-clear  "hasUncle" :vrel :def)
T
? (let ((*compiler-pass* 1))
      (make-virtual-relation 
       '(:name :en "uncle" :fr "oncle")
       '(:class "person")
       '(:compose (:or ("mother" "brother")("father" "brother")))
       '(:to "person")
       '(:max 12)
       '(:doc :en "an uncle is a person who is the brother of the mother or of the ~
                   father.")))
:DONE

? (process-virtual-relations)

### an uncle is a person who is the brother of the mother or of the father.

[Role_Uncle:
   (?V1039 test4:hasUncle ?V1041)
<-
   (?V1039 test4:hasMother ?V1040)
   (?V1040 test4:hasBrother ?V1041)
 ]

### an uncle is a person who is the brother of the mother or of the father.

[Role_Uncle:
   (?V1042 test4:hasUncle ?V1044)
<-
   (?V1042 test4:hasFather ?V1043)
   (?V1043 test4:hasBrother ?V1044)
 ]
[...OWL output...]
|#
;;;------------------------------------------------------- READ-NEXT-VALID-LINE

(defun read-next-valid-line (ss)
  "skip block commented lines.
Arguments:
   ss: stream
   initial-line: first line (that was already read)
return:
   string or :eof"
  (let ((comment-flag 0) line)
    (loop
      (setq line (read-line ss nil :eof))
      (incf *line-number*)
      ;(format t "~&~s" line)
      ;; did we reach end of file? If so return
      (if (eql line :eof)(return-from read-next-valid-line :eof))
      ;; first trim line removing spaces and tabs
      (setq line (string-trim '(#\space #\tab) line))
      ;(format t "~&=> ~s" line)
      ;; otherwise check for comment sign
      (cond
       ;; when empty line, skip it
       ((equal "" line))
       ;; skip simple comment lines
       ((eql (char line 0) #\;))
       ;; if not inside a commented block and line is valid, return it
       ((and (< comment-flag 1)
             (not (search "#|" line :test #'string-equal)))
        (return-from read-next-valid-line line))
       ;; if line is beginning comment
       ;; signal comment and read next line
       ((search "#|" line :test #'string-equal)
        (incf comment-flag)
        ;(format t "~&------- ~S" comment-flag)
        )
       ;; if inside commented-block and line is end of comment, decrease counter
       ((and (> comment-flag 0)
             (search "|#" line :test #'string-equal))
        (decf comment-flag)
        ;(format t "~&------- ~S" comment-flag)
        )
       ;; otherwise, skip line
       ))))

;;;---------------------------------------------------------------------- RESET

(defun reset ()
  "reset all structures preparing for compiling SOL into OWL.
    Arguments:
    none
    Return:
    t"
  (setq *compiler-pass* 1
      *concept-list* nil
      *attribute-list* ()
      *index-list* () ; JPB051009
      *relation-list* ()
      *virtual-attribute-list* ()
      *virtual-composed-attribute-list* ()
      *virtual-concept-list* ()
      *virtual-property-list* ()
      *virtual-relation-list* ()
      *individual-list* ()
      *rule-list* ()
      *super-class-list* ()
      *error-message-list* ()
      *index* (clrhash *index*)
      *left-margin* 0)
  t)

;;;------------------------------------------------------------------ same-name

;;;(defun same-name (first-name-spec second-name-spec)
;;;  "checks whether two multi-lingual name specs share the same name. E.g.
;;;    (:en \"sex\" :fr \"genre\") (:fr \"sexe; Genre\") share  \"genre\"
;;;   comparisons are made on normalized strings.
;;;Arguments:
;;;   first-name-spec: 
;;;   second-name-spec:
;;;Return:
;;;   nil or the list of merged specs"
;;;  (let (result first-name-list second-name-list shared-names tag shared-name? names)
;;;    (loop
;;;      ;; when fist name spec is empty, get out of the loop
;;;      (unless first-name-spec (return))
;;;      ;; norm name-strings
;;;      (setq first-name-spec (multilingual-name? first-name-spec)
;;;            second-name-spec (multilingual-name? second-name-spec))
;;;      ;; get language tag
;;;      (setq tag (pop first-name-spec))
;;;      ;; if second-name-spec has something in this language, then compare values
;;;      (if (setq names (cadr (member tag second-name-spec)))
;;;        (progn
;;;          
;;;          ;; by intersecting normed srings
;;;          (setq first-name-list (mapcar #'make-index-string 
;;;                                        (extract-names (car first-name-spec)))
;;;                second-name-list (mapcar #'make-index-string 
;;;                                         (extract-names names)))
;;;          (setq shared-names 
;;;                (intersection first-name-list second-name-list :test #'string-equal))
;;;          (if shared-names (setq shared-name? t))
;;;          ;; merge the names anyway
;;;          (setq result
;;;                (append 
;;;                 (list tag 
;;;                       (merge-names
;;;                        (union first-name-list second-name-list 
;;;                               :test #'string-equal)))
;;;                 result)))
;;;        ;; otherwise, record first name
;;;        (setq result (append (list tag (car  first-name-spec)) result)))
;;;      (pop first-name-spec) ; remove string part
;;;      )
;;;    ;; check intersection result, if so retruned the merged string
;;;    (when shared-name? 
;;;      ;; must add all language spec from the second-name-spec that were not recorded
;;;      (loop
;;;        (unless second-name-spec (return))
;;;        ;; check if already recorded
;;;        (unless (member (car second-name-spec) result)
;;;          (setq result (append result (list (car second-name-spec)
;;;                                            (cadr second-name-spec)))))
;;;        (pop second-name-spec)
;;;        (pop second-name-spec)
;;;        )
;;;      result)))

#|
? (same-name '(:en "night") '(:en "night"))
(:EN "Night")
(mln::mln-equal '(:en "night") '(:en "night"))
T
? (same-name '(:en "night") '(:en " NIGHT   "))
(:EN "Night")
(mln::mln-equal '(:en "night") '(:en " NIGHT   "))
T
? (same-name '(:en "night") '(:en "nite  ; NIGHT   "))
(:EN "Nite; Night")
(mln::mln-equal '(:en "night") '(:en "nite  ; NIGHT   "))
T
? (same-name '(:en "night" :fr "nuit ") '(:en "nite  ; NIGHT   " :it "notte"))
(:FR "nuit " :EN "Nite; Night")
(mln::mln-equal '(:en "night" :fr "nuit ") '(:en "nite  ; NIGHT   " :it "notte"))
T
? (same-name '(:en "night" :fr "nuit ") '(:en "nite  ; NIGHT   " :it "notte"))
(:FR "nuit " :EN "Nite; Night" :IT "notte")
(mln::mln-equal '(:en "night" :fr "nuit ") '(:en "nite  ; NIGHT   " :it "notte"))
T
? (same-name '(:en "night" :fr "nuit ") '(:en "nite   " :it "notte"))
NIL
(mln::mln-equal '(:en "night" :fr "nuit ") '(:en "nite   " :it "notte"))
NIL
|#
;;;-------------------------------------------------------- SET-RULE-NAME-SPACE
;;; used by make-ontology

(defun set-rule-name-space (options)
  "extract the name space to be used in the rule file from the ontology options."
  (declare (special *rule-prefix* *rule-prefix-ref*)) 
  (let (self ref)
    (setq self (cadr (member "self" (assoc :xmlns options) :test #'string-equal)))
    (setq self (string-trim '(#\space) self))
    (cond
     ((null self)
      (twarn "can't find self in defontology xmlns option, needed for rule file"))
     ;; if first char is & we have a reference
     ((char-equal #\& (char self 0))
      ;; get rid of leading & and trailing ;
      (setq self (subseq self 1 (1- (length self))))
      ;; get the reference in the DOCTYPE option
      (setq ref (cadr (member self (assoc :doctype options) :test #'string-equal)))
      (if (null ref)
        (twarn "can't find self reference in defontology doctype option ~
                (needed for rule file).")
        (setq *rule-prefix* self 
              *rule-prefix-ref* ref)))
     ;; if not reference qe assume that it contaisn the ref
     (t 
      (setq *rule-prefix* self
            *rule-prefix-ref*
            (cadr (member self (assoc :xmlns options) :test #'string-equal)))))))

#|
? (set-rule-name-space
   '((:version 1.0)
     (:doctype "owl" "http://www.w3.org/2002/07/owl#"
               "test4" "http://www.terregov.eupn.net/ontology/2005/10/test.owl/test4#"
               "xsd"       "http://www.w3.org/2001/XMLSchema#" )
     (:xmlns "rdf" "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
             "rdfs" "http://www.w3.org/2000/01/rdf-schema#"
             "owl" "http://www.w3.org/2002/07/owl#"
             "xsd" "&xsd;"
             "self" "&test4;"
             "test" "&test4;")
     (:base "http://www.terregov.eupn.net/ontology/2005/10/test.owl/test4")
     (:imports "http://www.w3.org/2000/01/rdf-schema"
               "http://www.w3.org/1999/02/22-rdf-syntax-ns"
               "http://www.w3.org/2002/07/owl")))
"http://www.terregov.eupn.net/ontology/2005/10/test.owl/test4#"
|#
;;;------------------------------------------------------------------ TRIM-FILE

(defun trim-file (input-file output-file)
  "Cleans a file of the leading UTF-8 mark (3 bytes?) by copying the file into
    new one.
Arguments:
   input-file: pathname of file to clean
   output-file: pathname of cleaned file
Return:
   t"
  (let (line)
    (with-open-file (in input-file :direction :input :external-format :utf8)
      (with-open-file (out output-file :direction :output :if-exists :supersede
                           :if-does-not-exist :create :external-format :utf8)
        
        ;; get rid of the first 3 chars if they look suspicious: ï»¿ (UTF-8 BOM)
        (dotimes (nn 3)
          ;(print (char-code (peek-char t in)))
          (when (member (peek-char t in) '(#\ï #\» #\¿) :test #'char-equal)
            (read-char in)
            ))
        ;; copy the rest of the file
        (loop
          (setq line (read-line in nil :eof))
          ;; if :eof then end of file
          (when (eql line :eof) 
            (close out)
            (return-from trim-file t))
          ;; skip the first line if it starts with ";;;-*-"
          ;; skip the line containing (in-package 
          (unless (or (search ";;;-*-" line)
                      (search "(in-package " line))
            ;; otherwise copy into new file
            ;; remove PC formatted line-feed
            (format out "~&~A" 
              (if (and (> (length line) 0) (char-equal (char line 0) #\Linefeed))
                  (subseq line 1)
                line))
            )
          )
        nil))))

#|
(ext:letf 
  ((custom:*default-file-encoding* 
  (ext:make-encoding :charset charset:utf-8 :line-terminator :dos)))
  (trim-file "c:/sol/ontologies/test0.sol" "c:/sol/ontologies/test0.tmp"))
|#
;;;=================================== INDEX ======================================
#|
The *index* table is a hash-table with complex entries depending on the kind of
information kept at compilation time.
The index will contain entries like
- index onto classes
  "Ville" ((:CLASS (:ID "Z-City")))
  "Prénom" ((:ATT (:ID "Z-hasFirstName")))
- desciption of classes
  "Z-City" ((:PROP (:ATT "Z-hasName" "Z-hasPeople")(:REL "Z-hasRegion")))
- description of properties
  "hasName" ((:ATT (:TYPE :STRING)(:DOMAIN ...)("Z-City" (:max 2)))
           "hasRegion" ((:REL (:RANGE "Z-Region")("Z-Region" (:unique)))))
- description of individuals
  "albert" ((:IND (:id "albert")(:class "Z-Person")))

A key is a string usually produces by make-index-string from any string.
? (make-index-string "le jour  le PLUS long   ")
"LeJourLePlusLong"

Associated with the key is an a-list whose properties are tags 
(:ATT :REL :CLASS or :INST) 
describing properties attached to the entry when it designates an 
attribute, a relation, a concept or an individual.

Each value attached to a tag is itself an a-list whose properties are different
for different tags. For example 

In the case of :ATT properties can be
 :id giving the id of the attribute, e.g. "hasFirstName"
 :class-id giving the domains of the attribute, e.g. ("Z-Person" "Z-Animal")
 :type giving the SOL type of the corresponding values, e.g. :integer
 :one-of giving the list of allowed values, e.g. (:one-of 1 2 3)
 :card giving the cardinality restrictions, e.g. (:card 1 1)

In the case of :REL
 :id giving the id of the relation, e.g. "hasBrother"
 :class-id giving the domain of the relation e.g. ("Z-Person" "Z-Animal")
 :to giving the range, e.g. ("Z-Person" "Z-Animal")
<note that is is an error to have multiple domains AND ranges>
Constraints local to a particular class
<class-id> e.g. "Z-Person" whose value is an a-list
 :card e.g. (:card 1 2)
 :one-of e.g. <is :one-of local?>

In the case of :CLASS
 :id giving the id of the class, e.g. "Z-City"
 :is-a giving the entry of the superclass

In the case of :INST
 :id giving the id of the individual

The table entries will be used in particular for checking the existence of various
objects or for checking constraints for properties of individuals.

Several functions are used to handle the *index* hash table:

INDEX-ADD to add something
INDEX-CLEAR to remove something
INDEX-GET to get some value
INDEX to see what is in the table

Typically something is added as follows:
(index-add "Person" :class :id "Z-Person") ; record the id for the class

|#
;;;---------------------------------------------------------------------- INDEX

(defun index ()
  "dumps the content of the *index* hash-table in alphabetic order (debug function)."
  (let (index-list)
    (maphash #'(lambda (xx yy) (if xx (push (list xx yy) index-list)))
             *index*)
    (mapcar #'print
            (setq index-list (sort index-list #'string< :key #'car)))
    :end))

;;;------------------------------------------------------------------ INDEX-ADD

(defun index-add (key tag prop value)
  "add an entry into the *index* table. Replaces previous entry at the prop level.
Arguments:
   key: the entry string
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
   value: value associated with the entry
Return:
   value"
  (let ((entry (gethash key *index*))
        tag-value prop-value old-pair)
    ;; if no entry, then create one
    (unless entry 
      (setf (gethash key *index*) `((,tag . ((,prop ,value)))))
      (return-from index-add value))
    ;; check if value has tag
    (setq tag-value (cdr (assoc tag entry :test #'equal)))
    ;; if not set up one
    (unless tag-value
      (setf (gethash key *index*) (cons `(,tag . ((,prop ,value))) entry))
      (return-from index-add value))
    ;; check for prop
    (setq prop-value (cdr (assoc prop tag-value :test #'equal)))
    ;; if no, then set up one
    (unless prop-value
      (setq tag-value (cons `(,prop ,value) tag-value))
      ;; replace old tag-value
      (setq entry (cons (cons tag tag-value)
                        (remove (assoc tag entry :test #'equal)
                                entry :test #'equal)))
      (setf (gethash key *index*) entry)
      (return-from index-add value))
    ;; if prop present get previous value, add new one and replace
    (setq old-pair (assoc prop tag-value :test  #'equal)
          ;          value (append (cdr old-pair) (list value)))
          value (if (member value (cdr old-pair) :test #'equal)
                  (cdr old-pair)
                  (append (cdr old-pair) (list value))))
    ;; replace old value with new value
    (setq tag-value (cons `(,prop . ,value)
                          (remove old-pair tag-value :test #'equal)))
    ;; replace old tag-value
    (setq entry (cons (cons tag tag-value) (remove (assoc tag entry :test #'equal)
                                                   entry :test #'equal)))
    ;; replace entry
    (setf (gethash key *index*) entry)
    (return-from index-add value)))

#|
(progn
  (index-clear)
  (index-add "Ville" :CLASS :id "Z-City")
  (index))
("Ville" ((:CLASS (:ID "Z-City"))))
:END

(progn
  (index-add "Ville" :class :one-of "Caen")
  (index))
("Ville" ((:CLASS (:ONE-OF "Caen") (:ID "Z-City")))) 
:END

(progn
  (index-add "Ville" :class :one-of "Nice")
  (index-add "Ville" :class :one-of "Toulouse")
  (index))
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END

(progn
 (index-add "Ville" :inst :att "hasName") ; "hasName"
 (index)
("Ville"
 ((:INST (:ATT "hasName"))
  (:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END

(progn
  (index-add "Ville" :inst :att "hasColor")
  (index))
("Ville"
 ((:INST (:ATT "hasName" "hasColor"))
  (:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END

(progn
  (index-add "Ville" :inst :rel "hasNeighbor")
  (index))
("Ville"
 ((:INST (:REL "hasNeighbor") (:ATT "hasName" "hasColor"))
  (:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END

(progn
  (index-clear)
  (index-add "hasName" :att :id '(:fr "Nom" :en "Name"))
  (index))
("hasName" ((:ATT (:ID (:FR "Nom" :EN "Name")))))
|#
;;;---------------------------------------------------------------- INDEX-CLEAR

(defun index-clear (&optional key tag prop)
  "removes one of the levels of the index depending on the arguments. If none, ~
      then clears the whole table
Arguments:
   key (opt): key
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
Return:
   t"
  (let ((entry (gethash key *index*))
        tag-value)
    ;; if no args, clear table
    (unless key
      (clrhash *index*)
      (return-from index-clear t))
    ;; if key but no tag, then remove entry
    (unless tag 
      (remhash key *index*)
      (return-from index-clear t))
    ;; if no prop, we want to remove tag option
    (setq entry (gethash key *index*))
    (unless prop
      (setf (gethash key *index*) 
            (remove (assoc tag entry :test #'equal) entry :test #'equal))
      (return-from index-clear t))
    ;; check for value, if no value we want to remove prop option
    (setq tag-value (cdr (assoc tag entry :test #'equal)))
    
    (setq tag-value (remove (assoc prop tag-value :test #'equal)
                            tag-value :test #'equal))
    ;; replace old tag-value
    (setq entry 
          (if tag-value
            (cons (cons tag tag-value) (remove (assoc tag entry :test #'equal)
                                               entry :test #'equal))
            (remove (assoc tag entry :test #'equal)
                    entry :test #'equal)))
    (setf (gethash key *index*) entry)
    (return-from index-clear t)))

#|
? (index-clear)
T
? (index)
NIL
? (%index-set "hamlet" nil nil '((:class (:id "Z-Village")(:one-of "Margny"))))
((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))
? (index)
("hamlet" ((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))) 
:END
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice")))) 
("hamlet" ((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))) 
:END
? (index-clear "Ville")
T
? (index)
("hamlet" ((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))) 
:END
? (index-clear "hamlet" :class :one-of)
T
? (index)
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice")))) 
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
? (%index-set "Ville" :class :id '("Z-City"))
("ZCity")
? (%index-set "Ville" :class :id '("Z-City"))
("ZCity")
? (index)
("Ville" ((:CLASS (:ID "Z-City") (:ONE-OF "Caen" "Nice")))) 
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
? (index-clear "Ville" :class )
T
? (index)
("Ville" NIL) 
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
|#
;;;--------------------------------------------------------------- INDEX-REMOVE

(defun index-remove (key tag prop value)
  "remove a value associated with prop. If not there, do nothing.
Arguments:
   key (opt): key
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
   value: value to remove
Return:
   t"
  (let ((old-value (index-get key tag prop))
        new-value)
    (setq new-value (remove value old-value :test #'equal))
    (if new-value
      (%index-set key tag prop new-value)
      (index-clear key tag prop))))

#|
? (index-clear)
T
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice")))) 
:END
? (index-add "Ville" :class :one-of "Paris")
("Caen" "Nice" "Paris")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice" "Paris")))) 
:END
? (index-add "Ville" :class :id "Z-City")
"Z-City"
? (index)
("Ville" ((:CLASS (:ID "Z-City") (:ONE-OF "Caen" "Nice" "Paris")))) 
:END
? 
INDEX-REMOVE
? (index-remove "Ville" :class :one-of "Nice")
("Caen" "Paris")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Paris") (:ID "Z-City")))) 
:END
? (index-remove "Ville" :class :id "Z-City")
T
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Paris")))) 
NIL
|#
;;;----------------------------------------------------------------- %INDEX-SET

(defun %index-set (key tag prop value)
  "set a value associated with key tag or prop. Must have the proper format.
Arguments:
   key: key (error if not present)
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
   value: value to set (must have the proper format)
Return:
   value"
  (let (entry tag-value prop-value)
    (unless key (error "no key arg"))
    ;; if no tag replaces whole entry
    (unless tag
      (setf (gethash key *index*) value)
      (return-from %index-set value))
    ;; get old entry
    (setq entry (gethash key *index*))
    ;; if prop is nil, then replaces the value associated with tag
    (unless prop
      (setf (gethash key *index*) 
            (cons (cons tag value)
                  (remove (assoc tag entry :test #'equal) entry :test #'equal)))
      (return-from %index-set value))
    ;; get old tag-value
    (setq tag-value (assoc tag entry :test #'equal))
    ;; get prop-value part
    (setq prop-value (assoc prop (cdr tag-value) :test #'equal))
    ;; if prop there replace old-value with new value
    ;; e.g. entry ((:CLASS (:id "ZCity")(:one-of ...)) (:INST ...))
    ;;      tag-value (:CLASS (:id "ZCity")(:one-of ...))
    ;;      prop-value (:id "ZCity")
    ;; (%index-set "Ville" :CLASS :id '("ZTown"))
    (setq tag-value 
          (list* tag 
                 (cons prop value) 
                 (remove prop-value (cdr tag-value) :test #'equal)))
    
    (setf (gethash key *index*) 
          (cons tag-value
                (remove (assoc tag entry :test #'equal) entry :test #'equal)))
    value))

#|
? (index-clear)
T
? (%index-set "Ville" nil nil '((:CLASS (id "Z-City"))))
((:CLASS (ID "Z-City")))
? (index)
("Ville" ((:CLASS (ID "Z-City")))) 
NIL
? (%index-set "Ville" nil nil '((:CLASS (id "Z-Town"))))
((:CLASS (ID "Z-Town")))
? (index)
("Ville" ((:CLASS (ID "Z-Town")))) 
NIL
? (%index-set "Ville" :class nil '((:id "Z-Town")))
((:ID "Z-Town"))
? (index)
("Ville" ((:CLASS (:ID "Z-Town")))) 
NIL
? (%index-set "Ville" :class nil '((:id "Z-Town")(:one-of "Bordeaux")))
((:ID "Z-Town") (:ONE-OF "Bordeaux"))
? (index)
("Ville" ((:CLASS (:ID "Z-Town") (:ONE-OF "Bordeaux")))) 
NIL
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice") (:ID "Z-Town")))) 
NIL
|#
;;;------------------------------------------------------------------ INDEX-GET

(defun index-get (key &optional tag prop)
  "get one of the levels of the index depending on the arguments. If none, ~
      then clears the whole table
Arguments:
   key: key
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
Return:
   corresponding value"
  (let ((entry (gethash key *index*)) tag-value)
    (unless tag (return-from index-get entry))
    (setq tag-value (cdr (assoc tag entry :test #'equal)))
    (unless prop (return-from index-get tag-value))
    (cdr (assoc prop tag-value :test #'equal))))

#|
? (index)

("Ville" ((:CLASS (:ID "Z-City")))) 
("Hamlet" ((:CLASS (:ID "Z-Hamlet") (:ONE-OF "margny")))) 
NIL
? (index-get "Hamlet" :class :id)
("Z-Hamlet")

|#

(format t "~%;*** MOSS v~A - Sol2owl loaded ***" moss::*moss-version-number*)

;;; :EOF
