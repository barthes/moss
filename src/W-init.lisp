﻿;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;20/01/30		
;;;		     I N I T - (File W-init.LISP)
;;;	 
;;;==========================================================================
;;; This file contains the code for showing initial windows when loading MOSS
;;; alone independently from OMAS. It also by passes the MOSS project.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
-------
2012
 1018 copying files from the MOSS project into the MOSS package
2014 -> UTF8 encoding
|#
 
(in-package :moss)
 
;;;---------------------------------------------------- MAKE-MOSS-INIT-WINDOW
; (moss::make-moss-init-window)

(defun make-moss-init-window
    (&key parent (owner (or parent (screen cg:*system*))) exterior
     (interior (cg:make-box 599 233 977 392)) (name :moss-init-window) (title " ")
     (border :black) (child-p t) form-p)
  (let ((owner
         (make-window name :owner owner
           :class 'cg:dialog
           :child-p child-p
           :exterior exterior
           :interior interior
           :background-color (cg:make-rgb :red 224 :green 255 :blue 206)
           :border border
           :close-button nil
           :cursor-name :arrow-cursor
           :font (cg:make-font-ex :swiss "MS Sans Serif / ANSI" 16 '(:bold))
           :form-package-name :common-graphics-user
           :form-state :normal
           :maximize-button nil
           :minimize-button nil
           :name :moss-init-window
           :pop-up nil
           :resizable nil
           :scrollbars nil
           :state :normal
           :system-menu t
           :title title
           :title-bar t
           :dialog-items (make-moss-init-window-widgets)
           :form-p form-p
           :form-package-name :moss)))
    owner))

;;;-------------------------------------------- MAKE-MOSS-INIT-WINDOW-WIDGETS

(defun make-moss-init-window-widgets ()
  (list (make-instance 'cg:static-text 
          :font (cg:make-font-ex :swiss "Arial / ANSI" 16 '(:bold)) 
          :height 72
          :left 69 
          :name :title 
          :top 40
          :value  (format nil "      MOSS version ~A alpha

 Copyright Barthès@UTC, 2016"  *moss-version-number*)
          :width 248)
        (make-instance 'cg:button 
          :font (cg:make-font-ex nil "Tahoma / Default" 11) 
          :left 144
          :name :start 
          :on-click 'moss-start-on-click 
          :title "START" 
          :top 120)
        ))

;;;------------------------------------------------------- MOSS-START-ON-CLICK

(defun moss-start-on-click (dialog widget)
  "calls MOSS and close the window"
  (declare (ignore widget))
  
  ;;; create a stub in the user environment so that we can work and create items
  ;;; in it directly 
  ;(moss::%create-new-package-environment :cg-user "CG-USER")
  
  (cg::set-foreground-window (make-moss-import-window))
  (sleep 2)
  (setf (cg:state dialog) :shrunk)
  t)

(format t "~%;*** MOSS v~A - MOSS init window loaded ***" *moss-version-number*)

;;; :EOF