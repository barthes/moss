;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;18/12/18
;;;		M A C R O S - (File macros.lisp)
;;;	 
;;;==========================================================================

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#


#|
(create-user-manual (choose-file-dialog))
(CREATE-USER-MANUAL (cg:ASK-USER-FOR-DIRECTORY :BROWSE-INCLUDE-FILES T))

History
=======
2002
 1222 rebuilding MOSS for a single user
 1230 adding export clause
2003
 0514 setting mgf-load to noop to avoid loading message in %defclass $SYSTEM
 0518 adding *moss-engine-loaded* to avoid compiler complaints
2004
 0628 introducing *application-name*, default: "MOSS-TEST.LISP"
 0712 P: removing *moss-version* defined in the load file
      P: wrapping export in an eval-when
      P: removing LISP  from exported symbols because of conflicts with 
         cg-graphics, prefixing CONFIGURATION SYSTEM PROPERTY *SYSTEM*
         with MOSS- for the same reason
 0715 adding the *moss-standalone* variable. If T, then we display the MOSS
      init window, otherwise, we load MOSS silently.
 1004 introducing transalation table for accentuated PC characters
      adding moss-symbol?
 1025 introducing mformat to be able to write into window panes
      *moss-output* is the current output (stream or pane)
      NO, put into the moss-window file
2005
 0306 adding mformat for MCL defined in the initial window in ACL
      id. for the *moss-output* global variable
      telling the system that compiler is part of the features
 0331 adding verbose-throw and verbose-warn macros
 0418 adding *allowed-restriction-operators*
 0504 changing make-entry-point to accommodate string OR list as input arg
 0508 introducing *language*
 0511 adding *transition-verbose* flag
 0514 make-ideal-id: making the symbol package of ideal that of is class radix
 0618 adding the option *allow-forward-class-references* and *deferred-actions*
 0926 replacing t by *debug-io* in mformat and trformat
 1126 version 6.0
      ===========
 1212 including macros from various files so that we do not have loading order
      problems
2006
 0213 adding lambdap
 0304 adding *deferred-instance-creations*, *allow-forward-instance-references*
 0307 exporting *language*
 0621 improving MCL mformat
 0514 adding %make-id-for-virtual-concept (macro) and ...-class (function)
      defvirtualconcept (exported) %defvirtualconcept
 1017 adding global *export-entry-symbols* to control export of ontology symbols
      when nil allows each agent to define symbol in its own package
 1107 adding package-argument to %make-id-for-instance, %make-id-for-class
      %make-id-for-orphan
 1206 adding with-package
 1227 adding dformat
2007
 0118 adding *delimiters* used by %get-words-from-text
 0223 correcting mformat for ACL adding moss::*moss-output*
 0305 cleaning exported macro symbols
 0720 updating mformat for MCL (mformat bypasses conversations)
 0720 introducing a new global variable *moss-conversation*
 1107 addinf debugging macros ipm ipcl ipcg
2008
 0131 introducing *deferred-default*, modifying mformat to print into fred windows
 0215 adding catch-error and catch-system-error
 0219 changed catch-error
 0602 exporting =make-entry
 0604 === v5.11.2
 0611 small changes #+:MCL -> #+MCL, ...
 0612 adding :unknown to the language tags 
2009
 0124 removing *translation-table* since we use UTF-8 -> JPB0901     
 0211 adding :es to languages
 0728 adding :cn :lu :ja languages  
 1130 adding with-environment  
 1230 adding deffunction    
2010
 0113 adding *database* to handle persistency     
 0117 adding with-language, making the with-xx macro exported
 0124 removing context and package options everywhere       
 0214 adding *ontolog as a synonym for *moss-system*
 0513 mergin v7.16
 0518 updating ref
 0725 adding D+ and D- debugging functions, and V+ V- for verbose
 0731 adding a few punctuation marks to *delimiters*
 0814 removing ? from delimiter list (needed for identifying request performative) 
 0928 adding *editing-box*   
 1110 correcting ref 
 1121 defining display-text as a generic function
2011
 1219 adding alist-set as an non destructive nalist-replace-pv
 1229 adding copy-fact get-fact set-fact for use in defstate 
2012
 1028 changing vformat macro to a function for implementing dialog trace window
 1126 adding pp
2013
 0417 adding conditional OMAS directive to vformat
 1118 adding get-field
 1125 adding equal-gen and member-gen to handle strings in values
 1127 importing string+
2014
 0113 adding equal+ and exporting it
 0307 changed to UTF8 encoding
 0315 changed equal+ to take mlns into account
 0501 adding :pt to allowed languages replacing :lu
2015
 0402 adding dmformat for improved debugging
 1111 adding list-difference (like set-difference, but preserve order)
 1115 correct bug in macro with-language
      adding *attribute-restrictions*
2016
 0209 adding firstn
 0531 adding lists of options for defining attributes, concets and relations
 0620 adding dwformat (like dformat but waits after each message)
2018                            
 1218 adding *crash-allowed* to trigger an error when arg of send is NIL 
2020
 0129 modified to fit the asdf description of the MOSS system
      special variables moved to globals.lisp
|#

(in-package :moss)

;;;========================== debugging macro ===============================

(defMacro terror (cstring &rest args)
  "throws an error message back to an :error catch. Prints message if *verbose*"
  `(progn
     (if *verbose* (format *debug-io* 
                           ,(concatenate 'string "~&;***Error " cstring) ,@args))
     (throw :error 
            (format nil ,(concatenate 'string "~&;***Error " cstring) ,@args))))

(defMacro twarn (cstring &rest args)
  "inserts a message into the *error-message-list* making sure to return nil"
  `(progn (push (format nil ,cstring ,@args) *error-message-list*) nil))
					   
#|
Because macros are expanded at compile time they must precede all functions
using them
(defMacro vformat (cstring &rest args)
  `(if *verbose* (format *debug-io* ,(concatenate 'string "~&;***** " cstring)
                   ,@args))) ;jpb0510

;;; we redefine the macro as a function
;;; since it is for tracing, efficiency is not required
(defun vformat (cstring &rest args)
  (if *verbose* 
      (apply #'format *debug-io* (concatenate 'string "~%;***** " cstring)
             args)))
|#

;;;------------------------------------------------------------------ TFORMAT
;;; trace printing function using *trace-output* *trace-level* to indent

(defMacro tformat (format-string &rest arg-list)
  "used to simplify global trace"
  `(format *trace-output* "~&~VT~S ~?" 
           *trace-level* *trace-level* ,format-string (list ,@arg-list)))

(defun moss::vformat (cstring &rest args)
  (declare (special omas::*trace-dialog-pane* moss::*verbose* omas::*omas*))
  (cond
   #+OMAS
   ((and moss::*verbose* (omas::text-window omas::*omas*))
    (apply #'format omas::*trace-dialog-pane* 
           (concatenate 'string "~%;***** " cstring)
           args))
   (moss:*verbose*
    (apply #'format *debug-io* (concatenate 'string "~%;***** " cstring)
             args))))
			 
(defMacro trformat (fstring &rest args)
  "debugging macro used locally explicitely temporarily"
  `(format *debug-io* ,(concatenate 'string "~&;***** " fstring) 
           ,@args))

(defun D+ ()
  (declare (special moss::*debug*))
  (setq moss::*debug* t)
  "moss::*debug* set to T (debugging dialogs?)"
  )

(defun D- ()
  (declare (special moss::*debug*))
  (setq moss::*debug* nil)
  "moss::*debug* reset to NIL (end debugging)"
  )

(defun V+ ()
  (declare (special moss::*verbose*))
  (setq moss::*verbose* t)
  "moss::*verbose* set to T"
  )

(defun V- ()
  (declare (special moss::*verbose*))
  (setq moss::*verbose* nil)
  "moss::*verbose* reset to NIL"
  )

;;;==========================================================================
;;;                     service functions and macros
;;;==========================================================================

;;;--------------------------------------------------------------- ASSREMPROP

(defMacro assremprop (prop ll)
  `(remove-if #'(lambda(xx)(eq (car xx) ,prop)) ,ll))

;;;-------------------------------------------------------------- CATCH-ERROR

(defMacro catch-error (msg &rest body)
  "intended to catch errors due to poor MOSS syntax. Prints in active window ~
   using mformat unless msg is empty
   Note: body should not return a string (considered as an error message)
Arguments:
   msg: something like \"while executing xxx\"
   body (rest): exprs to execute
Return:
   the value of executing body if no error, nil and prints a message when error"
  (if (equal msg "")
    `(catch :error ,@body nil)
    `(let* ((err (catch :error ,@body nil)))
       ;; if non nil, then string to be printed unless message is empty
       (cond 
        ((stringp err)
         (moss::mformat "~&;*** MOSS error ~A: ~%~A" ,msg err)
         nil)
        ;; otherwise return value returned by catch
        (err)))))

;;;------------------------------------------------------- CATCH-SYSTEM-ERROR

(defMacro catch-system-error (msg &rest body)
  `(let (test errno)
     (multiple-value-setq (test errno) 
       (ignore-errors ,@body))
     (print (list test errno))
     ;; if test is nil and errno is not: error
     (if (and (null test) errno)
       (mformat "~%... system error ~A: ~%~S" ,msg errno))
     test))
#|
(catch-system-error "" (+ 2 3 4))
(catch-system-error "while doing things" (+ 2 3 4)(/ 4 0))
|#

;;;---------------------------------------------------------------- COPY-FACT
;;; used by conversation states to update the FACTS field

(defmacro copy-fact (item1 item2)
  `(move-fact moss::conversation :from ,item1 :to ,item2))

;;;-------------------------------------------------- DEFGENERIC DISPLAY-TEXT
;;; defined to keep the compiler quiet

(defUn display-text (&rest ll) ll)

;;;------------------------------------------------------------------- FIRSTN

(defun firstn (ll nn)
         (cond ((or (null ll)(<= nn 0)) nil)
               (t (cons (car ll) (firstn (cdr ll) (1- nn))))))

;;;-------------------------------------------------------------------- GETAV

(defMacro getav (key list ) `(cadr (member ,key ,list)))

#|
? (getav :b '(:a (1 2 3) :b (4 5 6) :c (7 8 9)))
(4 5 6)
|#
;;;----------------------------------------------------------------- GET-FACT
;;; used by the conversation states to read the FACTS field

(defmacro get-fact (item)
  `(read-fact moss::conversation ,item))

;;;--------------------------------------------------------------------- get-field

(defun get-field (field-name alist)
   (cdr (assoc field-name alist :test #'string-equal)))

#|
(get-field "titre" 
           '(("name" . "sais pas")("titre" . "Le bon choix")("reste" . "junk")))
"Le bon choix"
|#

;;;--------------------------------------------------------------------- GETV

(defMacro getv (prop ll) 
  "Returns
   1. the value associated to prop
   2. t if the value was specified to be NIL"
  `(values (cdr (assoc ,prop ,ll)) (if (car (assoc ,prop ,ll)) t)))

#|
(getv 1 (setq ll '((0 a a)(1 b b) (2))))
(B B)
T

(getv 2 ll)
NIL
T

(getv 3 ll)
NIL
NIL
|#
;;;--------------------------------------------------------------- IN-VERSION
;;; same as with-context no check for valid context
;;; note that we use the *context* variable of the current package

(defMacro in-version (%_context &rest ll)
  (append `(let ((,(intern "*CONTEXT*") ,%_context))) ll ))

;;;-------------------------------------------------------------------- INTER
;;; intersecting any number of lists

(defMacro inter (&rest ll)
  (if (cdr ll)
    `(intersection ,(car ll)(inter ,@(cdr ll)))
    (car ll)))

;;;------------------------------------------------------------------ %MKNAME

(defMacro %mkname (ref type)
  "if ref is a symbol return symbol, otherwise call~
   %make-name-for-<type>.
Arguments:
   ref: a symbol or (multilingual) string
   type: type of name to recover (keyword)
   package (key): default current
Expansion:
   (%make-name-for-<type> ref . more-args)"
  `(if (symbolp ,ref)
     ;; if symbol in current package do nothing
     ;; (*package* is checked at run time)
     (if (eql (symbol-package ,ref) *package*)
       ,ref 
       ;;...otherwise change package
       (intern (symbol-name ,ref)))
     ;; if not a symbol, then create a symbol in the specified package
     (intern (%%make-name-string ,ref ,type))))


#|%%make-name-string (ref type-key

(%mkname "NAME" :property)
HAS-NAME
:INTERNAL
;---
(%%make-name "name" :property)
HAS-NAME
:INTERNAL

;; if symbol keep it as it is
(%mkname 'FIRST-NAME :property)
FIRST-NAME
;---
(%%make-name 'name :property)
HAS-NAME
:INTERNAL

(%mkname 'HAS-NAME :property)
HAS-NAME
;---
(%%make-name 'has-name :property)
HAS-NAME
:INTERNAL

(%mkname '(:en "NAME" :fr "nom") :property)
HAS-NAME
:INTERNAL
;---
(%%make-name '(:en "NAME" :fr "nom") :property)
HAS-NAME
:INTERNAL
;---
(%%make-name '((:en "NAME") (:fr "nom")) :property)
HAS-NAME
:INTERNAL

;;; %mkname does not take :package as keyword

(%mkname "NAME" :property :package :address)
ADDRESS:HAS-NAME
:EXTERNAL

(%mkname '(:en "NAME" :fr "nom") :property :package :address)
ADDRESS:HAS-NAME
:EXTERNAL
|#
;;;------------------------------------------------------------------- MTHROW

(defMacro mthrow (control-string &rest args)
  "print a message and throws the message string to :error"
  `(throw :error
          (mformat ,control-string ,@args)))
;;;-------------------------------------------------------------- NASSREMPROP

;;; deletes destructively a pair from an a-list. setf is used to make sure
;;; that the list is effectively modified destructively when removing the
;;; first element. If the list is pointed to from some atom, doing a delete
;;; does not change the list, it simply returns a pointer onto the cdr of
;;; the list.
;;; Does not work for versioned MOSS objects since it deletes all versions!

(defMacro nassremprop (prop ll)
  "ll must be a symbol naming an a-list"
  `(setf ,ll (delete-if #'(lambda(xx)(eq (car xx) ,prop)) ,ll)))

#|
(MOSS::nassremprop '$T-PERSON-AGE
 '((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.22))
   ($T-PERSON-AGE (0 32))))
|#

;;; print and recover the source code of a function
;;;(defMacro pf (fn) `(function-lambda-expression (function ,fn)))
;;; print plist of an object
;;;(defMacro pl (obj-id) `(symbol-plist (quote ,obj-id)))

;;;-------------------------------------------------------------------- NCONS

(defMacro ncons (arg) `(cons ,arg nil))

;;;------------------------------------------------------------------ RFORMAT
;;; a macro to limit the length of strings to post using Pascal (birrrkkhh)
;;; used by browser and locator

(defMacro rformat (limit &rest ll)
  "takes a text and limits its length. Because the silly Pascal strings must be ~
   less than some length.
Arguments:
  limit: max length
  ll: regular arguments to a text."
  `(let ((result (format ,@ll)))
     (remove '#\return
             (if (> (length result) ,limit)
               (concatenate 'string (subseq result 0 (- ,limit 3)) "...")
               result))))

;;;----------------------------------------------------------------- SET-FACT
;;; used by conversation states to update the FACTS field

(defmacro set-fact (item value)
  `(replace-fact moss::conversation ,item ,value))

;;;---------------------------------------------------------------------- SV

(defmacro sv (xx) `(symbol-value ,xx))

;;;------------------------------------------------------------ VERBOSE-THROW
;;; different from terror, throws to any tag

(defMacro verbose-throw (label &rest format-args)
  "when *verbose* is T, prints a warning before throwing the error message ~
   string to the label.
Arguments:
   label: a label to which to throw (usually :error)
   format-args: arguments to build a message string using format nil
Return:
   throws to the label the message string."
  `(let ((error-message (format nil ,@format-args)))
     (if *verbose* (warn error-message))
     (throw ,label error-message)))

;;;------------------------------------------------------------- VERBOSE-WARN

(defMacro verbose-warn (&rest format-args)
  `(when *verbose*
     (warn ,@format-args)))

;;;A------------------------------------------------------------------- WHILE
;;; we define while here as in UTC-Lisp - we must be careful since in UTCLisp 
;;; while returns the value of the last expr in the loop body

(defMacro while (condition &rest body)
  (append `(loop (if (not ,condition) (return nil))) body ))

;;;A--------------------------------------------------------------- WITH-CONTEXT
;;; (with-context context &rest instructions) - closure in context
;;; because we define the macro in a MOSS package, %%%allowed-context? is the
;;; one defined in MOSS
;;; Should restore initial context in case of a throw from %%allowed-context?
;;; We use %_xxx variables not to be in conflict with ll variables

(defMacro with-context (%_context &rest %_ll)
  `(let ((%_save (symbol-value (intern "*CONTEXT*")))
         %_result)
     ;(format t "~%;+++ with-context /%_context: ~S" ,%_context)
     ;(format t "~%;+++ with-context /save: ~S" %_save)
     (%%allowed-context? ,%_context)
     (set (intern "*CONTEXT*") ,%_context)
     ;; make sure that the context is restored to its previous value
     (unwind-protect
         ;; when ll returns several values, save them into a list
         (setq %_result (multiple-value-list (progn ,@%_ll)))
       (set (intern "*CONTEXT*") %_save))
     ;; but return whatever was computed in the ll exprs using multiple values if
     ;; neede
     (values-list %_result)))

#|
(with-context 3 (print *context*) (1+ *context*))
expands to
(LET ((SAVE 0))
  (SET (INTERN "*CONTEXT*" *PACKAGE*) 3)
  (PRINT *PACKAGE*)
  (%%ALLOWED-CONTEXT? 3)
  (LET ((#:G1000 (PROGN (PRINT *CONTEXT*) (1+ *CONTEXT*))))
    (SET (INTERN "*CONTEXT*") SAVE)
    #:G1000))
and yields
;*** MOSS-error context 3 is illegal in package: #<The MOSS package>.
Error: Attempt to throw to the non-existent tag :ERROR

(with-package :test
  ;; must prefix the variables, otherwise use MOSS...
  (with-context 3 (print test::*context*) (1+ test::*context*)))
expands to
(LET ((*PACKAGE* (FIND-PACKAGE :TEST)))
  (IF *PACKAGE*
      (PROGN (LET ((SAVE 0))
               (SET (INTERN "*CONTEXT*") 3)
               (PRINT (INTERN "*CONTEXT*"))
               (PRINT (SYMBOL-VALUE (INTERN "*CONTEXT*")))
               (%%ALLOWED-CONTEXT? 3)
               (LET ((#:G1000 (PROGN (PRINT TEST::*CONTEXT*) (1+ TEST::*CONTEXT*))))
                 (SET (INTERN "*CONTEXT*") SAVE)
                 #:G1000)))
    (PROGN (IF *VERBOSE*
               (FORMAT *DEBUG-IO* "~&;***Error there is no package called ~S" :TEST)
             NIL)
           (THROW :ERROR (FORMAT NIL "~&;***Error there is no package called ~S" :TEST)))))
and yields
3
3
(with-package :moss
  (with-context 3 (print *context*) (1+ *context*)))
|#
;;;--------------------------------------------------------------- WITH-DATABASE
;;; (with-context context &rest instructions) - closure in database

;;;(defMacro with-database (%_database &rest ll)
;;;  `(let ((*database* ,%_database)) 
;;;     ,@ll ))

;;;------------------------------------------------------------ WITH-ENVIRONMENT
;;; (with-environment package context language &rest instructions) 
;;;  Set a MOSS environment for processing ontology macros (e.g. deferred)

(defMacro with-environment (%_package %_context %_language &rest ll)
  `(let ((*package* ,%_package))
     (set (intern "*LANGUAGE*") ,%_language)
     (set (intern "*CONTEXT*") ,%_context)
     (%%allowed-context? ,%_context)
     ,@ll ))

;;;--------------------------------------------------------------- WITH-LANGUAGE
;;; first save current global language value, then change language, execute exprs
;;; restore language and return result of executing exprs

(defmacro with-language (%_language &rest ll)
  `(let ((saved-lan (symbol-value (intern "*LANGUAGE*"))) result)
     (set (intern "*LANGUAGE*") ,%_language)
     ;; execute exprs, then restore language, but return result of exprs
     (setq result (multiple-value-list ,@ll))
     ;; restore language
     (set (intern "*LANGUAGE*") saved-lan)
     ;; return result preserving multiple values
     (values-list result)))

#|
*language*
:EN
(with-language :fr
  (mln::make-mln '("a" "b")))
((:FR "a" "b"))
*language*
:FR

(with-package :address
  (with-language :en
    (send 'address::$E-PERSON '=get-name)))
("person")

(with-package :address
  (with-language :fr
    (send 'address::$E-PERSON '=get-name)))
("personne")

(with-language :fr (values 1 2))
1
2
|#
;;;---------------------------------------------------------------- WITH-PACKAGE

(defMacro with-package (%_package &rest ll)
  `(let ((*package* (find-package ,%_package)))
     (if *package*
         (progn ,@ll)
       (terror "there is no package called ~S" ,%_package))))

#|
(catch :error (with-package :address (print *package*)))
";***Error there is no package called :ADDRESS"

? (with-package :address-2 (print *package*))
(LET ((*PACKAGE* (FIND-PACKAGE :ADDRESS-2)))
  (IF *PACKAGE*
      (PROGN (PRINT *PACKAGE*))
      (TERROR "there is no package called ~S" :ADDRESS-2)))
#<Package "ADDRESS-2">
|#

;;;=============================================================================
;;;                         macros for making ids
;;;=============================================================================

;;;=== The following macros are used for defining various types of identifiers
;;; Watch it: an optional argument in the macro is resolved at compiled time
;;; Thus all default values for *package* and *context* will be :moss and 0 !!
;;; If one wants to be specific about a package (e.g. in an environment where
;;; execution interleaves programs using different packages) then one must 
;;; add a package argument, except for macros that use have a default behavior
;;; (e.g. inverse prop id is created in the same package as id).
;;; Most of those macros are called when creating ontologies, when loading the
;;; corresponding file (usually uninterrupted). 

;;; WATCH: the *package* default must be specified AT RUN TIME and not at macro
;;; expansion time...

;;;---------------------------------------------------------- %MAKE-ID-FOR-CLASS

(defMacro %make-id-for-class (ref)
  "function to cook up default entity ids: $E-<name>
   If name is a symbol then the symbol is interned into the symbol's ~
   package, otherwise it is interned into the specified package, or ~
   if not there into the current package.
Argument:
   ref: symbol or (multilingual) to be used for radix
Return:
   a symbol specifying the class, e.g. $E-PERSON"
  `(%make-id '$ENT :name ,ref))

#|
? (%make-id-for-class "station headmaster")
$E-STATION-HEADMASTER
:INTERNAL
? (%make-id-for-class '(:en "Stevens" :fr "Albert")) ; *language* is :fr
$E-ALBERT
:INTERNAL
? (let ((*language* :en))
     (%make-id-for-class '(:en "Stevens" :fr "Albert")))
$E-STEVENS
NIL
? (%make-id-for-class 'Albert)
$E-ALBERT
:INTERNAL
? (with-package :address-proxy (moss::%make-id-for-class 'Albertine))
ADDRESS-PROXY::$E-ALBERTINE
NIL
? (with-package :address (moss::%make-id-for-class "Albert"))
ADDRESS::$E-ALBERT
NIL
? (with-package :address (moss::%make-id-for-class '(:en "Albert")))
ADDRESS::$E-ALBERT
:INTERNAL
|#
;;;-------------------------------------------------- %MAKE-ID-FOR-VIRTUAL-CLASS

(defMacro %make-id-for-virtual-class (name) 
  "function to cook up default entity ids: $E-<name>
   If name is a symbol then the symbol is interned into the symbol's ~
   package, otherwise it is interned into the specified package, or ~
   if not there into the current package.
Argument:
   name: symbol or (multilingual) to be used for radix
   package (key): default current
Return:
   a symbol specifying the class, e.g. $E-PERSON"
  `(%make-id '$VENT :name ,name))

#|
? (%make-id-for-virtual-class "station headmaster")
$V-STATION-HEADMASTER
NIL
? (%make-id-for-virtual-class '(:en "Albert"))
$V-ALBERT
NIL
? (%make-id-for-virtual-class 'Albert)
$E-ALBERT
:INTERNAL
? (moss::%make-id-for-virtual-class '"PERSON" :package (find-package :address))
ADDRESS::$V-PERSON
NIL
|#
;;;------------------------------------------------------- %MAKE-ID-FOR-CLASS-SP

(defMacro %make-id-for-class-sp (name class-ref)
  "function to cook up a default relation id.
   If name is a symbol then the symbol is interned into the symbol's ~
   package, otherwise it is interned into the specified package, or ~
   if not there into the current package.
Argument:
   name: symbol or (multilingual) to be used for radix
   class-ref: symbol, string or mln
Return:
   a symbol specifying the class, e.g. $S-<class-name>-<name>"
  `(%make-id '$EPS :name ,name :prefix 
             (%make-name-string-for-concept ,class-ref)))

#|
(%make-id-for-class-sp '(:EN "tender age ; age" :FR "�ge ; degr� de viellesse ")
                         'PERSON)
|$S-PERSON-(:EN-"TENDER-AGE-;-AGE"-:FR-"�GE-;-DEGR�-DE-VIELLESSE-")|
NIL
(%make-id-for-class-sp 
 '((:EN "tender age" "age") (:FR "�ge" "degr� de viellesse ")) 'PERSON)
$S-PERSON-TENDER-AGE
NIL

? (moss::%make-id-for-class-sp '(:EN "tender age") "PERSON")
ADDRESS::$S-PERSON-TENDER-AGE
:INTERNAL
? (moss::%make-id-for-class-sp 'tender-age 'PERSON)
$S-PERSON-TENDER-AGE
:INTERNAL
? (moss::%make-id-for-class-sp 'tender-age '(:en "PERSON"))
 MOSS::$S-PERSON-TENDER-AGE
:INTERNAL
|#
;;;------------------------------------------------------- %MAKE-ID-FOR-CLASS-TP

(defMacro %make-id-for-class-tp (name class-ref)
  "function to cook up default property ids.
   If name is a symbol then the symbol is interned into the symbol's ~
   package, otherwise it is interned into the specified package, or ~
   if not there into the current package.
Argument:
   name: symbol or (multilingual) to be used for radix
   class-ref: symbol, string or mln
Return:
   a symbol specifying the class, e.g. $T-<class-name>-<name>"
  `(%make-id '$EPT :name ,name :prefix 
             (%make-name-string-for-concept  ,class-ref)
             ))

#|
? (%make-id-for-class-tp '(:EN "tender age ; age" :FR "?ge ; degr? de viellesse ")
                         'PERSON)
$T-PERSON-TENDER-AGE
:INTERNAL

(%make-id-for-class-tp 
 '(:EN "tender age ; age" :FR "?ge ; degr? de viellesse ") "PERSONNE")
MOSS::$T-PERSONNE-�GE
:INTERNAL

? (%make-id-for-class-tp '(:EN "tender age ; age" :FR "?ge ; degr? de viellesse ")
                         '(:fr "PERSONNE"))
MOSS::$T-PERSONNE-�GE
:INTERNAL
? (let ((*language* :en))
     (%make-id-for-class-tp '(:EN "tender age ; age" :FR "?ge ; degr? de viellesse ")
                         "PERSONNE"))
$T-PERSONNE-TENDER-AGE
:INTERNAL
? (%make-id-for-class-tp "age" "person" :package :address)
ADDRESS::$T-PERSON-AGE
NIL
|#
;;;-------------------------------------------------------- %MAKE-ID-FOR-CONCEPT

(defMacro %make-id-for-concept (name)
  "see %make-id-for-class"
  `(%make-id-for-class ,name))

#|
? (%make-id-for-concept "station headmaster")
(MOSS::%MAKE-ID 'MOSS::$ENT :NAME "station headmaster" :PACKAGE (OR NIL *PACKAGE*))
$E-STATION-HEADMASTER
:INTERNAL
|#
;;;------------------------------------------------ %MAKE-ID-FOR-VIRTUAL-CONCEPT

(defMacro %make-id-for-virtual-concept (name)
  "see %make-id-for-virtual-class"
  `(%make-id-for-virtual-class ,name))

#|
? (%make-id-for-virtual-concept "station headmaster")
$V-STATION-HEADMASTER
:INTERNAL
? (%MAKE-ID-FOR-VIRTUAL-CONCEPT "albert" :package :address)
ADDRESS::$V-ALBERT
NIL
|#
;;;-------------------------------------------------------- %MAKE-ID-FOR-COUNTER

(defMacro %make-id-for-counter (class-id)
  "makes an ID for a counter, e.g. $E-PERSON.CTR in class-id package.
Arguments:
   class-id: id of the class to which the counter belongs
Returns:
   the counter id."
  `(%make-id '$CTR :id ,class-id))

#|
? (%make-id-for-counter '$E-PERSON)
$E-PERSON.CTR
NIL
? (moss::%make-id-for-counter 'address::$E-PERSON)
ADDRESS::$E-PERSON.CTR
:INTERNAL
|#
;;;---------------------------------------------------------- %MAKE-ID-FOR-IDEAL

(defMacro %make-id-for-ideal (class-id)
  "makes an ID for the class ideal, e.g. $E-PERSON.0 in class-id package.
Arguments:
   class-id: class ID
Return:
   a symbol that is the ID of the ideal."
  `(%make-id ,class-id :ideal t))

#|
? (catch :error (%make-id-for-ideal '$E-PERSON))
"$E-PERSON is not a valid class in package #<Package \"MOSS\"> context 0"

? (moss::%make-id-for-ideal 'address::$E-PERSON)
ADDRESS::$E-PERSON.0
:INTERNAL
|#
;;;------------------------------------------------------- %MAKE-ID-FOR-INSTANCE
;;; the rationale would be to create instances in the same package as that of
;;; the class. Otherwise we could end-up with instances of a class in a different
;;; package, leading to confusion if the class has been defined separately in the
;;; two packages
;;; However, we need to have instances of methods defined in the local package
;;; and not in the MOSS package...

(defMacro %make-id-for-instance (model-id value)
  "Builds internal keys for instances using the radix of a class; e.g. ~
   $E-PERSON.34 (34 is the value obtained from the class counter).
   The package of the new symbol is that of the class.
Arguments:
   model-id: identifier of the class
   value: value of the counter
Returns:
   an identifier obtained by applying make-name."
  `(%make-id ,model-id :value ,value))

#|
? (catch :error (%MAKE-ID-FOR-INSTANCE '$E-person 222))
$E-PERSON.222
NIL
? (catch :error (%MAKE-ID-FOR-INSTANCE 'address::$E-person 222 :package :address) )
ADDRESS::$E-PERSON.222
NIL
? (catch :error (%MAKE-ID-FOR-INSTANCE 'address::$E-person 222 ) )
$E-PERSON.222
NIL
|#
;;;------------------------------------------------ %MAKE-ID-FOR-INSTANCE-METHOD

(defMacro %make-id-for-instance-method (value)
  "Builds an internal id for an instance-method: e.g. $FN.34
Arguments:
   context: context
   value: counter for methods
   package (key): default current
Return:
   method id."
  `(if (eql *package* (find-package :moss))
     (%make-id '$FN :value ,value)
     ;; more complex in application packages
     (intern (format nil "$E-FN.~S" ,value))))

#|
? (%make-id-for-instance-method 34)
$FN.34
:INTERNAL
? (with-package :address (moss::%make-id-for-instance-method 34))
ADDRESS::$E-METHOD.34
NIL
|#
;;;----------------------------------------------- %MAKE-ID-FOR-INVERSE-PROPERTY

(defMacro %make-id-for-inverse-property (id)
  "Builds an inverse property id from a direct one. The function is useful  ~
   only for MOSS kernel. Indeed, for applications, inverse properties are ~
   instances of the $EIL class, and have $EIL.nn as internal name.
Arguments:
   id: identifier of the direct property
Return:
   symbol for the inverse property id."
  ;; cannot call %make-id because it must work even if $EIL has not been created
  ;; %make-id creates inverse in the same package as id
  `(%make-id '$EIL :id ,id))

#|
? (%MAKE-ID-FOR-INVERSE-PROPERTY '$E-TITLE)
$E-TITLE.OF
NIL
? (%MAKE-ID-FOR-INVERSE-PROPERTY 'address::$T-NAME)
ADDRESS::$T-NAME.OF
:INTERNAL

(with-package :test
  (%make-id-for-inverse-property 'test::$T-PERSON-NAME))
TEST::$T-PERSON-NAME.OF
|#
;;;--------------------------------------------------------- %MAKE-ID-FOR-ORPHAN
;;; Can call %make-id since there are no orphans in kernel

(defMacro %make-id-for-orphan (value)
  "Builds an internal id for an orphan object: e.g. $3-5 where 3 is ~
   the context value, 5 the system's counter value.
Arguments:
   value: counter for orphans
Return:
   orphan id, e.g. $3-5."
  `(%make-id '*none* :value ,value))

#|
(%MAKE-ID-FOR-ORPHAN 23)
$0-23
NIL
|#
;;;----------------------------------------------------- %MAKE-ID-FOR-OWN-METHOD

(defMacro %make-id-for-own-method (value)
  "Builds an internal id for an own-method: e.g. $FN.34
Arguments:
   value: counter for methods
Return:
   method id."
  `(%make-id '$FN :value ,value))

#|
? (%make-id-for-own-method 34)
$FN.34
:INTERNAL
|#
;;;------------------------------------------------------------- %MAKE-ID-FOR-SP

(defMacro %make-id-for-sp (name)
  "function to cook up default property ids: 
   If name is a symbol then the symbol is interned into the symbol's ~
   package, otherwise it is interned into the specified package, or ~
   if not there into the current package.
Arguments:
   name: symbol, string or mln
Return:
   a symbol anem $S-<name>"
  `(%make-id '$EPS :name ,name))

#|
? (%make-id-for-sp '(:EN "tender age ; age" :FR "?ge ; degr? de viellesse "))
$S-TENDER-AGE
NIL
? (moss::%make-id-for-sp 'address::brother)
ADDRESS::$S-BROTHER
NIL
? (moss::%make-id-for-sp 'brother )
$S-BROTHER
NIL
? (moss::%make-id-for-sp 'brother :package :address)
$S-BROTHER
:INTERNAL
? (moss::%make-id-for-sp "brother" )
$S-BROTHER
:INTERNAL
? (moss::%make-id-for-sp "brother" :package :address)
ADDRESS::$S-BROTHER
NIL
|#
;;;------------------------------------------------------------- %MAKE-ID-FOR-TP

(defMacro %make-id-for-tp (name)
  "function to cook up default property ids: $T-<name>
   If name is a symbol then the symbol is interned into the symbol's ~
   package, otherwise it is interned into the specified package, or ~
   if not there into the current package.
Arguments:
   name: symbol or (multilingual) string
Return:
   a symbol anem $T-<name>"
  `(%make-id '$EPT :name ,name))

#|
? (%make-id-for-tp '(:EN "tender age ; age" :FR "?ge ; degr? de viellesse "))
$T-TENDER-AGE
NIL
(moss::%make-id-for-tp 'address::age)
ADDRESS::$T-AGE
NIL
|#
;;;----------------------------------------------- %MAKE-ID-FOR-UNIVERSAL-METHOD

(defMacro %make-id-for-universal-method (value)
  "Builds an internal id for an own-method: e.g. $UNI.34
Arguments:
   value: counter for methods
Return:
   method id."
  `(%make-id '$UNI :value ,value))

#|
? (%make-id-for-universal-method 34)
$FN.34
:INTERNAL
? (%make-id-for-universal-method 0 34 :package :address)
ADDRESS::$UNI.34
|#
;;;=============================================================================
;;;                    macros for making name symbols 
;;;=============================================================================

;;;---------------------------------------------------- %MAKE-NAME-FOR-ATTRIBUTE

(defMacro %make-name-for-attribute (name)
  "build the attribute name, e.g. HAS-SECTION-TITLE unless name is a symbol ~
   starting with HAS-, which is already a propert name.
Argument:
   name: symbol or (multilingual) string
Return:
   interned symbol in the current package."
  `(%%make-name ,name :attribute))

#|
(%make-name-for-attribute 'section-title)
HAS-SECTION-TITLE
:INTERNAL

(%make-name-for-attribute 'has-section-title)
HAS-SECTION-TITLE
:INTERNAL

(%make-name-for-attribute "section title")
HAS-SECTION-TITLE
NIL

(%make-name-for-attribute '((:en "section title")))
HAS-SECTION-TITLE
:INTERNAL
|#
;;;-------------------------------------------------------- %MAKE-NAME-FOR-CLASS

(defMacro %make-name-for-class (name)
  "build the class name, e.g. SECTION-TITLE.
Argument:
   name: symbol or (multilingual) string
Return:
   interned symbol in the current package."
  `(%%make-name ,name :class))

#|
(%make-name-for-class 
   '((:EN "tender age" "age") (:FR "�ge" "degr� de viellesse ")))
TENDER-AGE
NIL

(%make-name-for-class   ; old format
 '(:EN "tender age; age" :FR "�ge; degr� de viellesse "))
TENDER-AGE
NIL

(%make-name-for-class "l'�l�ve  studieux  ")
|L'�L�VE-STUDIEUX|
NIL
|#
;;;----------------------------------------------- %MAKE-NAME-FOR-CLASS-PROPERTY
;;; seems to be unused

(defMacro %make-name-for-class-property (prop-ref class-ref)
  "build the property name symbol, e.g. HAS-PERSON-FIRST-NAME.
Argument:
   prop-ref: symbol or (multilingual) string
   class-ref: specifies the class (cannot be mln)
Return:
   interned symbol in the current package."
  ;`(%make-name '$EPR :name ,prop-ref :prefix ,class-ref))
  `(%%make-name ,prop-ref :prop :prefix ,class-ref))

#|
(%make-name-for-class-property "friendly neighbour " 'person)
HAS-PERSON-FRIENDLY-NEIGHBOUR
NIL

(%make-name-for-class-property 'friendly-neighbour '(:en "person"))
Error: bad prefix (:EN "PERSOn"), should be symbol or string.
NIL

(%make-name-for-class-property '(:en "friendly-neighbour") 'person)
HAS-PERSON-FRIENDLY-NEIGHBOUR
:INTERNAL

(%make-name-for-class-property '(:en "friendly-neighbour") "person")
HAS-PERSON-FRIENDLY-NEIGHBOUR
:INTERNAL
|#
;;;------------------------------------------------------ %MAKE-NAME-FOR-CONCEPT

(defMacro %make-name-for-concept (name)
  "build the concept name, e.g. SECTION-TITLE.
Argument:
   name: symbol or (multilingual) string
Return:
   interned symbol in the current package."
  ;`(%make-name '$ENT :name ,name))
  `(%%make-name ,name :class))

#|
(%make-name-for-concept 
 '((:en "tender age" "age")(:fr "�ge" "degr� de vieillesse")))
TENDER-AGE
:INTERNAL

(%make-name-for-concept 'AGE-TENDRE)
AGE-TENDRE
:INTERNAL

(%make-name-for-concept "�ge tendre ")
�GE-TENDRE
NIL
|#
;;;-------------------------------------------------------- %MAKE-NAME-FOR-ENTRY

(defMacro %make-name-for-entry (name)
  "build an entry name, e.g. SECTION-TITLE.
Argument:
   name: symbol or (multilingual) string
Return:
   interned symbol in the current package."
  ;`(%make-name nil :name ,name))
  `(%%make-name ,name :plain))

#|
(%make-name-for-entry "titre de l'ouvrage")
|TITRE-DE-L'OUVRAGE|
NIL

(%make-name-for-entry '(:fr "titre de l'ouvrage" :en "book title"))
BOOK-TITLE
:INTERNAL
|#
;;;--------------------------------------------- %MAKE-NAME-FOR-INVERSE-PROPERTY

(defMacro %make-name-for-inverse-property (name)
  "build the inverse property name, e.g. IS-FIRST-NAME-OF from FIRST-NAME
Argument:
   name: symbol or (multilingual) string
Return:
   interned symbol in the current package."
  `(%%make-name ,name :inv))

#|
(%make-name-for-inverse-property 
   '(:EN "tender age ; age" :FR "?ge ; degr? de viellesse "))
IS-TENDER-AGE-OF
NIL

(%make-name-for-inverse-property 
   '((:EN "tender age" "age") (:FR "�ge" "degr� de viellesse ")))
IS-TENDER-AGE-OF
:INTERNAL

(%make-name-for-inverse-property "first name")
IS-FIRST-NAME-OF
NIL

(%make-name-for-inverse-property ">first name")
HAS-FIRST-NAME
NIL

(%make-name-for-inverse-property 'has-first-name)
IS-FIRST-NAME-OF 
|#
;;;------------------------------------- %MAKE-NAME-FOR-INSTANCE-METHOD-FUNCTION

(defMacro %make-name-for-instance-method-function (name class-id)
  "build the instance method function name, e.g. $E-PERSON=I=0=PRINT-SELF.
Argument:
   name: symbol or (multilingual) string
   class-id: class id that owns the method
Return:
   interned symbol in the current package."
  ;`(%make-name '$FN :name ,name :method-type :instance :method-class-id ,class-id))
  `(%%make-name ,name :inst :class-ref ,class-id))

#|
(catch :error (%make-name-for-instance-method-function '=print-self '$E-PERSON))
$E-PERSON=I=0=PRINT-SELF
NIL
|#
;;;------------------------------------------------------- %MAKE-NAME-FOR-METHOD

(defmacro %make-name-for-method (name)
  "build the instance method name, e.g. PRINT-SELF.
Argument:
   name: symbol or (multilingual) string
Return:
   interned symbol in the current package."
  `(%%make-name ,name :plain))

#|
(%make-name-for-method "display")
DISPLAY
|#
;;;------------------------------------------ %MAKE-NAME-FOR-OWN-METHOD-FUNCTION

(defMacro %make-name-for-own-method-function (name obj-id)
  "build the own method function name, e.g. $E-PERSON=S=0=PRINT-SELF.
Argument:
   name: symbol or (multilingual) string
   class-id: id of the object that owns the method
Return:
   interned symbol in the current package."
  ;`(%make-name '$FN :name ,name :method-type :own :method-class-id ,obj-id))
  `(%%make-name ,name :own :class-ref ,obj-id))

#|
(catch :error (%make-name-for-own-method-function '=print-self '$E-PERSON))
$E-PERSON=S=0=PRINT-SELF
NIL
|#
;;;----------------------------------------------------- %MAKE-NAME-FOR-PROPERTY

(defMacro %make-name-for-property (name)
  "build the property name, e.g. HAS-FIRST-NAME.
Argument:
   name: symbol or (multilingual) string
Return:
   interned symbol in the current package."
  ;`(%make-name '$EPR :name ,name))
  `(%%make-name ,name :prop))

#|
(%make-name-for-property '(:EN "tender age ; age" :FR "?ge ; degr? de viellesse "))
HAS-TENDER-AGE

(%make-name-for-property 
 '((:EN "tender age" "age") (:FR "�ge" "degr� de viellesse ")))
HAS-TENDER-AGE
:INTERNAL

(%make-name-for-property 'age)
HAS-AGE
NIL
|#
;;;----------------------------------------------------- %MAKE-NAME-FOR-RELATION

(defMacro %make-name-for-relation (name)
  "build the relation name, e.g. HAS-SECTION-TITLE.
Argument:
   name: symbol or (multilingual) string
Return:
   interned symbol in the current package."
  `(%%make-name ,name :relation))

#|
(%make-name-for-relation "foreign owner")
HAS-FOREIGN-OWNER
NIL
|#
;;;------------------------------------ %MAKE-NAME-FOR-UNIVERSAL-METHOD-FUNCTION

(defMacro %make-name-for-universal-method-function (name)
  "build the universal method function name, e.g. *0=PRINT-UNIVERSAL.
Argument:
   name: symbol or (multilingual) string
Return:
   interned symbol in the specified package."
  ;`(%make-name '$UNI :name ,name))
  `(%%make-name ,name :uni))

#|
(%make-name-for-universal-method-function '=print-universal)
*0=PRINT-UNIVERSAL
NIL
|#
;;;----------------------------------------------------- %MAKE-NAME-FOR-VARIABLE

(defMacro %make-name-for-variable (name)
  "Builds an external variable name starting with underscore.
Argument:
   name: variable  name
Returns:
   the name starting with an underscore"
  ;`(%make-name :var :name ,name))
  `(%%make-name ,name :var))

#|
(%make-name-for-variable "jean albrecht")
_JEAN-ALBRECHT
NIL

(%make-name-for-variable 'zorro)
_ZORRO
NIL

(catch :error (%make-name-for-variable nil))
Error: name NIL should be a string or symbol or mln
|#
;;;=============================================================================
;;;                    macros for making name-strings 
;;;=============================================================================
;;; since the macros work with string they do not need packages

;;;----------------------------------------------- %MAKE-NAME-STRING-FOR-CONCEPT

(defMacro %make-name-string-for-concept (name)
  "Cooks up a name string from the name of a concept.
Argument:
   name: a symbol or (multilingual) string naming the concept
Return:
   a string naming the concpet"
  ;`(%make-name-string '$ENT :name ,name))
  `(%%make-name-string ,name :class))

#|
(%make-name-string-for-concept 'person)
"PERSON"

(with-language :fr 
  (%make-name-string-for-concept '(:en "person" :fr "personne")))
"PERSONNE"

(%make-name-string-for-concept "station headmaster")
"STATION-HEADMASTER"

(let ((*language* :en))
  (%make-name-string-for-concept '(:fr "personne")))
"PERSONNE"
|#
;;;-------------------------------------- %MAKE-NAME-STRING-FOR-CONCEPT-PROPERTY

(defMacro %make-name-string-for-concept-property (name-string class-ref)
  "Builds an external property name string, starting with HAS-
Arguments:
   name: a multilingual name naming the property given by the user as an ~
   argument to m-deftp for example (m-deftp (:fr \"titre de l'ouvrage\" :en \"book title\") ...)
Return:
   a string naming the property, e.g. \"HAS-TITLE\" "
  ;`(%make-name-string '$EPR :name ,name-string :prefix ,class-ref))
  `(%%make-name-string ,name-string :prop :prefix ,class-ref))

#|
(%make-name-string-for-concept-property 
 '(:en "size ; height" :fr "taille") 'person)
"HAS-PERSON-SIZE"

(%make-name-string-for-concept-property 
 '(:en "size ; height" :fr "taille") 'test::person)
"HAS-PERSON-SIZE"

(%make-name-string-for-concept-property 
 '(:en "size ; height" :fr "taille") '(:en "person")) ; old MLN format
"HAS-PERSON-SIZE"

(%make-name-string-for-concept-property 
   '((:en "size" "height") (:fr "taille")) '((:en "person")))
"HAS-PERSON-SIZE"
|#
;;;----------------------------------- %MAKE-NAME-STRING-FOR-CONVERSATION-HEADER

(defmacro %make-name-string-for-conversation-header (name)
  "Cooks up an index string from the conversation object by sticking ~
   _ up in front.
Argument:
   name: a symbol or string
Return:
   a string naming the doc object"
  ;`(%make-name-string '$EIL :name ,name))
  `(%%make-name-string ,name :conversation))

#|
(%make-name-string-for-conversation-header 'create-answer)
"_CREATE-ANSWER-CONVERSATION"

(%make-name-string-for-conversation-header "create  answer ")
"_CREATE-ANSWER-CONVERSATION"
|#
;;;----------------------------------------- %MAKE-NAME-STRING-FOR-DOCUMENTATION

(defmacro %make-name-string-for-documentation (name)
  "Cooks up an index string from the documentation object by sticking ~
   >- up in front.
Argument:
   name: a symbol or string
Return:
   a string naming the doc object"
  ;`(%make-name-string '$EIL :name ,name))
  `(%%make-name-string ,name :doc))

#|
(%make-name-string-for-documentation "how to do")
">-HOW-TO-DO"
|#
;;;-------------------------------------------------- %MAKE-NAME-FOR-ENTRY-STATE

(defmacro %make-name-for-entry-state (short-name type)
  "Cooks up an index string for a conversation entry state.
Argument:
   name: a symbol or string
   state-type: :entry-state, :success or :failure
Return:
   a string naming the doc object"
  `(%%make-name-string 
    :dummy :q-state :short-name ,short-name :state-type ,type))

#|
(%make-name-for-entry-state 'what :entry-state)
"_WHAT-ENTRY-STATE"
|#
;;;-------------------------------------- %MAKE-NAME-STRING-FOR-INVERSE-PROPERTY

(defMacro %make-name-string-for-inverse-property (name)
  "Cooks up an inverse property string from the name of the property by sticking ~
   IS- and -OF an each side. Remember the inverse property has no semantic ~
   content. It simply provides the possibility of following the arcs in the ~
   reverse direction.
Argument:
   name: a symbol or string naming the direct property
Return:
   a string naming the inverse property"
  ;`(%make-name-string '$EIL :name ,name))
  `(%%make-name-string ,name :inv))

#|
(%make-name-string-for-inverse-property 'brother)
"IS-BROTHER-OF"

(%make-name-string-for-inverse-property 'test::brother)
"IS-BROTHER-OF"
|#
;;;--------------------------------------- %MAKE-NAME-STRING-FOR-METHOD-FUNCTION

(defMacro %make-name-string-for-method-function (class-id method-type method-name)
  "Builds an internal name to store compiled methods, e.g. $CTR=I=2=summary ~
   or $E-PERSON=S=1=print-self where 1 is the context value.
Arguments:
   class-id: identifier of the class
   method-type: :instance (I) or own (S)
   method-name: name of the method
Returns:
   a symbol with package that of the method name."
  ;`(%make-name-string '$FN :name ,method-name :method-type ,method-type 
  ;                    :method-class-id ,class-id))
  `(%%make-name-string ,method-name '$FN :method-type ,method-type 
                       :class-ref ,class-id))

#|
(catch :error
       (%make-name-string-for-method-function '$E-PERSON :instance '=print-self))
"$E-PERSON=I=0=PRINT-SELF"

(%make-name-string-for-method-function '$E-PERSON :own '=print-self)
"$E-PERSON=S=0=PRINT-SELF"
|#
;;;---------------------------------------------- %MAKE-NAME-STRING-FOR-PROPERTY

(defMacro %make-name-string-for-property (name-string)
  "Builds an external property name string, starting with HAS-
Arguments:
   name: a multilingual name naming the property given by the user as an ~
   argument to m-deftp for example (m-deftp (:fr \"titre de l'ouvrage\" :en \"book title\") ...)
Return:
   a string naming the property, e.g. \"HAS-TITLE\" "
  ;`(%make-name-string '$EPR :name ,name-string))
  `(%%make-name-string ,name-string :prop))

#|
(%make-name-string-for-property "TITRE-DE-L-OUVRAGE")
"HAS-TITRE-DE-L-OUVRAGE"

(%make-name-string-for-property "TITRE de l'OUVRAGE")
"HAS-TITRE-DE-L'OUVRAGE"

(%make-name-string-for-property 
 '(:en "title ; header" :fr "titre;TITRE de l'OUVRAGE"))
"HAS-TITLE"
|#
;;;------------------------------------------------- %MAKE-NAME-STRING-FOR-RADIX
;;; ***** this function could be improved to use a better algorithm to shorten
;;; radices

(defMacro %make-name-string-for-radix (name &optional (type ""))
  "build a radix name string for a property or for a class. Tries to build a unique name ~
   using the first 3 letters of the name. Error if the symbol already exists.
Arguments:
   name: e.g. \"SIGLE\"
   type (opt): e.g. \"T\" (for terminal property), \"E\" for entity, etc.
Return:
   e.g. $T-SIG or error if already exists"
  ;`(%make-name-string :radix :name ,name :type ,type))
  `(%%make-name-string ,name :radix :type ,type))

#|
(%make-name-string-for-radix 'SIGLE "T")
"$T-SIG"

(%make-name-string-for-radix 'SIGLE)
"$-SIG"
|#
;;;----------------------------- %MAKE-NAME-STRING-FOR-UNIVERSAL-METHOD-FUNCTION

(defMacro %make-name-string-for-universal-method-function (name)
  "Builds an internal name to store compiled methods.
Arguments:
   name: name of the universal method
Returns:
   a symbol with package that of the method name."
  ;`(%make-name-string '$UNI :name ,name))
  `(%%make-name-string ,name :uni))

#|
(%make-name-string-for-universal-method-function '=print-object)
"*0=PRINT-OBJECT"
|#
;;;---------------------------------------------- %MAKE-NAME-STRING-FOR-VARIABLE

(defMacro %make-name-string-for-variable (name)
  "Builds an internal name string for a variable.
Arguments:
   name: string, symbol or multilingual name
Returns:
   a string."
  ;`(%make-name-string :var :name ,name))
  `(%%make-name-string ,name :var))

#|
(%make-name-string-for-variable "albert ")
"_ALBERT"
(with-language :en
  (%make-name-string-for-variable '(:en "Stevens" :fr "albert ")))
"_STEVENS"
|#

;;;=============================================================================
;;;                              Defining macros
;;;=============================================================================
;;; defconcept          replaces m-defclass (avoiding CLOS conflict with defclass)
;;; defattribute        replaces m-defattribute or m-deftp
;;; defrelation         replaces m-defrelation or m-defsp
;;; definstmethod       replaces m-defmethod
;;; defownmethod        replaces m-defownmethod
;;; defuniversalmethod  replaces m-defuniversalmethod
;;; defobject           replaces m-defobject
;;; definstance         replaces m-definstance
;;; defindividual       id.

;;; defontology         also applies to SOL ontologies
;;; defchapter          to produce HTML outputs
;;; defsection          id.

;;; We must add specific macros for defining methods the name of which we want to
;;; export:

;;; defmossinstmethod
;;; defmossownmethod
;;; defmossuniversalmethod

;;; deffunction
;;; %defsysvar
;;; defsysvarname

;;; defvirtualconcept

;;; Such macros are static macros and are thus executed within a specific package

;***** need to export that stuff

;;;---------------------------------------------------------------- DEFATTRIBUTE

(defMacro defattribute (name &rest option-list)
  `(apply #'%make-tp ',name ',option-list))

;;;------------------------------------------------------------------- %DEFCLASS
;;; for historical reasons

(defMacro %defclass (multilingual-name &rest option-list)
  `(apply #'%make-concept ',multilingual-name ',option-list))

;;;------------------------------------------------------------------ DEFCHAPTER
;;; fake

(defMacro defchapter (&rest option-list)
  (declare (ignore option-list))
  nil)

;;;------------------------------------------------------------------ DEFCONCEPT

(defMacro defconcept (multilingual-name &rest option-list)
  `(apply #'%make-concept ',multilingual-name ',option-list))

;;;----------------------------------------------------------------- %DEFCONCEPT

(defMacro %defconcept (multilingual-name &rest option-list)
  `(apply #'%make-concept ',multilingual-name ',option-list))

;;;----------------------------------------------------------------- DEFFUNCTION

(defMacro deffunction (name var-list &rest body)
  "define a user function: same syntax as defun. Builds a function name and ~
   save its name on *moss-system* $SFL list. Arg-list and body are associated ~
   with a local variable: F_function-name"
  `(let ((ff (intern ,(concatenate 'string "F_" (symbol-name name)))))
     (set ff (list* ',name 'lambda ',var-list ',body))
     (%%add-value (symbol-value (intern "*MOSS-SYSTEM*")) 
                  '$SFL ff (symbol-value (intern "*CONTEXT*")))
     (defUn ,name ,var-list ,@body)
     ))
#|
(deffunction test (a b c d) 
  "test function"
  (declare (ignore A B))
  (list c d))
|#
;;;--------------------------------------------------------------- %DEFGENERICTP

(defMacro %defgenerictp (name &rest option-list)
  "see %make-generic-tp doc"
  `(apply #'%make-generic-tp ',name ',option-list))

;;;--------------------------------------------------------------- DEFINDIVIDUAL

(defMacro defindividual (name &rest option-list)
  `(apply #'%make-individual ',name ',option-list))

;;;----------------------------------------------------------------- DEFINSTANCE

(defMacro definstance (name &rest option-list)
  `(apply #'%make-instance ',name ',option-list))

;;;---------------------------------------------------------------- %DEFINSTANCE
;;; (%definstance class-name &rest option-list) - 
;;; syntax of option list is
;;;	(<tp-name> value[list])
;;;	(<sp-name> <internal-reference>)
;;; The class name is checked for existing classes 
;;; Properties are checked in the same fashion
;;;	Error messages are send if a non existing entity is mentioned
;;; Values for terminal properties are not checked unless for entry-point
;;; generation
;;; Internal references to MOSS entities must correspond to existing objects
;;; and of the proper type.
;;; If not, then an error message is sent.
;;; Example
;;;	(%definstance PERSON
;;;		(HAS-NAME Barthes)
;;;		(HAS-BROTHER p1)
;;;		)
;;; will work if p1 is the internal reference to an already defined person
;;; or specialization of person.
;;; Returns the internal reference to the new object
;;; Once created objects must be modified directly, using access methods
;;; Integrity constraints are checked to see whether the properties are
;;; correctly filled.
;;; The created instance is put onto the system-variable-list.
;;; This function cannot work without the Message Passing kernel

(defMacro %definstance (name &rest option-list)
  `(apply #'%make-instance ',name ',option-list))

;;;--------------------------------------------------------------- DEFINSTMETHOD

(defMacro definstmethod (name selector arg-list &rest body)
  `(funcall #'%make-method ',name ',selector ',arg-list ',body))


;;;----------------------------------------------------------- DEFMOSSINSTMETHOD

(defMacro defmossinstmethod (name selector arg-list &rest body)
  `(funcall #'%make-method ',name ',selector ',arg-list ',body '(:export t)))

;;;------------------------------------------------------------ DEFMOSSOWNMETHOD

(defMacro defmossownmethod (name selector arg-list &rest body)
  `(funcall #'%make-ownmethod ',name ',selector ',arg-list ',body '(:export t)))

;;;------------------------------------------------------ DEFMOSSUNIVERSALMETHOD

(defMacro defmossuniversalmethod (name arg-list &rest body)
  `(funcall #'%make-universal ',name ',arg-list ',body '(:export t)))

;;;------------------------------------------------------------------- DEFOBJECT

(defMacro defobject (&rest option-list)
  `(apply #'%make-object ',option-list))

;;;------------------------------------------------------------------ %DEFOBJECT

(defMacro %defobject (&rest option-list)
  `(apply #'%make-object ',option-list))

;;;----------------------------------------------------------------- DEFONTOLOGY

(defMacro defontology (&rest option-list)
  `(apply #'%make-ontology ',option-list))

;;;---------------------------------------------------------------- DEFOWNMETHOD

(defMacro defownmethod (name selector arg-list &rest body)
  `(funcall #'%make-ownmethod ',name ',selector ',arg-list ',body))

;;;--------------------------------------------------------------- %DEFOWNMETHOD
;;; (%defownmethod name selector arg-list doc body) - 
;;; creates a new method and attaches it to the object defined by the selector
;;; which must exist. If method already exists then error
;;; When selector is a symbol then it is considered an object-id 
;;; Otherwise syntax is:
;;;	(entry tp-name class [:filter function] )
;;; where function is used when more than one object is reached via the
;;; access path entry-tp-name-class
;;; defownmethod is essentially the same function as defmethod. It is easier
;;; to use than defmethod with the :own-method option at the end of the arg
;;; list. Thus it is defined for convenience.

;;; last argument must be a single list otherwise %make-own-method will have to
;;; quote every single instruction of the code

(defMacro %defownmethod (name selector arg-list &rest body)
  `(funcall #'%make-ownmethod ',name ',selector ',arg-list ',body))

;;;------------------------------------------------------------------ %DEFMETHOD

(defMacro %defmethod (name selector arg-list &rest body)
  `(funcall #'%make-method ',name ',selector ',arg-list ',body))

;;;----------------------------------------------------------------- DEFRELATION

(defMacro defrelation (name class suc &rest option-list)
  `(apply #'%make-sp ',name ',class ',suc ',option-list))

;;;------------------------------------------------------------------ DEFSECTION
;;; fake

(defMacro defsection (&rest option-list)
  (declare (ignore option-list))
  nil)

;;;---------------------------------------------------------------------- %DEFSP

(defMacro %defsp (name class suc &rest option-list)
  `(apply #'%make-sp ',name ',class ',suc ',option-list))
 
;;;------------------------------------------------------------------ %DEFSYSVAR
;;; (%defsysvar name doc value) - creates a new system variable
;;; If variable already exists then reinitialize it
;;; The value argument is evaluated.
;;; Example of use
;;;	(%defsysvar *screen* (Initialize system output stream)
;;;		(-> '$TTY '=new))
;;; Should be modified so that system variables are first class objects
;;; like methods. *****
;;; For the time being, we use this simple minded form.

(defMacro %defsysvar (name doc value)
  `(_%defsysvar ',name ',doc ',value))

;;;--------------------------------------------------------------- DEFSYSVARNAME

(defMacro defsysvarname (sysvar)
  "adds the name of a (global) variable to the list of variables recorded at ~
   system level."
  `(%%add-value *MOSS-SYSTEM* '$SVL ',sysvar (symbol-value (intern "*CONTEXT*")))
  )

;;;-------------------------------------------------------------- %DEFSYSVARNAME
;;; The next function can be used to save the name of a variable onto the 
;;; system list

(defMacro %defsysvarname (sysvar-name)
  `(%%add-value '$MOSSSYS '$SVL ',sysvar-name (symbol-value (intern "*CONTEXT*")))
  )

;;;---------------------------------------------------------------------- %DEFTP

(defMacro %deftp (name &rest option-list)
  `(apply #'%make-tp ',name ',option-list))

;;;--------------------------------------------------------------- %DEFUNIVERSAL
;;; (%defuniversal name arg-list body) - creates a new universal 
;;; method and attaches it to its name which must be unique. If name already exists
;;; then error, i.e. universal methods cannot be redefined?

(defMacro %defuniversal (name arg-list &rest body)
  `(funcall #'%make-universal ',name ',arg-list ',body))

;;;---------------------------------------------------------- DEFUNIVERSALMETHOD

(defMacro defuniversalmethod (name arg-list &rest body)
  `(funcall #'%make-universal ',name ',arg-list ',body))


;;;----------------------------------------------------------- DEFVIRTUALCONCEPT

(defMacro defvirtualconcept (multilingual-name &rest option-list)
  `(apply #'%make-virtual-concept ',multilingual-name ',option-list))

;;;---------------------------------------------------------- %DEFVIRTUALCONCEPT

(defMacro %defvirtualconcept (multilingual-name &rest option-list)
  `(apply #'%make-virtual-concept ',multilingual-name ',option-list))

(format t "~%;*** MOSS v~A - Macros loaded ***" moss::*moss-version-number*)

;;; :EOF
