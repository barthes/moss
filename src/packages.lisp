﻿;;;==========================================================================
;;;20/01/29
;;;		
;;;		     P A C K A G E S - (File packages.LISP)
;;;	 
;;;==========================================================================

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
This file contains the definitions of packages used by the MOSS application

History
2019
 0824 creation
2020
 0104 adding exports to package definitions
|#
 
(defpackage :moss (:use :cl)
  (:nicknames :m)
  (:export 
   ;;=== from globals
   :*answer*
   :*application-package*
   :*language*
   :*language-tags*
   :*moss-output*
   :*moss-window*
   :*self*
   :*transition-verbose* 
   :*verbose* 
   ;;=== from macros
   :=make-entry 
   :catch-error 
   :defattribute 
   :defchapter 
   :defconcept 
   :deffunction
   :defindividual 
   :definstance 
   :definstmethod 
   :defobject 
   :defontology 
   :defownmethod
   :defrelation 
   :defrule 
   :defsection 
   :defsimple-subdialog 
   :defstate 
   :defsubdialog
   :deftask               ; from dialog macros
   :defuniversalmethod 
   :defvirtualattribute 
   :defvirtualconcept 
   :defvirtualrelation
   ;dmformat 
   :with-ap 
   :with-context 
   :with-database 
   :with-environment 
   :with-language
   :with-package
   ;;=== from def
   :make-attribute
   :make-concept
   :=make-entry
   :make-individual
   :make-inst-method
   :make-own-method
   :make-orphan
   :make-own-method
   :make-relation
   :make-universal-method
   :m-load
   ;;=== from dialog
   :process-patterns 
   :defelizarules 
   :defcomplexelizarules
   ;;=== from service
   :access-from-words
   :add-values
   :clear-all-facts
   :delete-values
   :fill-pattern
   :filter
   :get-current-year
   ;get-objects-from-attribute-and-value
   ;move-fact
   :mref
   :package-key
   :pep
   :pop-fact
   :print-sys
   :read-fact
   :replace-fact
   :set-language
   :string+
   :symbol-key
   :web-active?
   :web-add-text
   :web-clear-gate
   :web-clear-tag
   :web-clear-text
   :web-get-gate
   :web-get-tag
   :web-get-text
   :web-set-gate
   :web-set-mark
   :web-set-text
   ;;=== from utils
   :alist-add 
   :alist-add-values 
   :alist-rem
   :alist-rem-val 
   :alist-set 
   :alist-set-values 
   :alistp 
   :copy-fact
   :d+ 
   :d- 
   :equal+ 
   :firstn
   :get-fact 
   :mformat 
   :nth-insert 
   :nth-move 
   :nth-remove 
   :nth-replace 
   :pp 
   :set-fact 
   :v+ 
   :v-
   ;VERBOSE-FORMAT
   :string+
   ;;=== from engine
   :send 
   :send-no-trace 
   :ton 
   :toff 
   :trace-message 
   :untrace-message 
   :trace-object 
   :untrace-object 
   :trace-method 
   :untrace-method 
   :untrace-all
   :broadcast 
   :g==> 
   :g-> 
   :s->
   ;;=== from query
   :access
   :<> 
   :filter-objects
   ;;=== from paths
   :locate-objects
   ;;=== persistency 
   :db-close 
   :db-create 
   :db-erase 
   :db-extend 
   :db-load 
   :db-open 
   :db-store 
   :db-vomit
   )
  (:import-from :util.string :string+)
  )

(defpackage :mln 
  (:use :cl) ; for multilingual names
  (:import-from :util.string :string+)
  (:import-from :moss :equal+ :*language* :*language-tags*))

;; because MOSS is part of the OMAS system, there are some references to OMAS 
;; in the code (should be fixed)

#-OMAS (defpackage :omas (:use :moss :cl)(:nicknames :o))

;; pakages for producing OWL or HTML files

(defpackage :sol (:use :cl)(:nicknames :so)  ; for the sol-init.lisp file
  (:export 
   :*compiling* 
   :*html-output*
   :*encoding*
   :*initial-parameters*
   :*language-list*             ; alist of default languages
   :*log-file*                 ; path to a default error file
   :*filename-display* 
   :*ontology-title* 
   :*ontology-initial-directory* 
   :*owl-output*
   :*rule-format* 
   :*rule-format-list* 
   :*rule-output* 
   :*sol-header*
   :*sol-file*
   :*sol-version* 
   :*trace-header* 
   :*trace-pane* 
   :*trace-window* 
   :*verbose-compile*
   ))

(defpackage :sol-owl (:use :sol :cl)(:nicknames :ow)
  (:import-from :util.string :string+)
  (:import-from :moss :equal+)
  (:export           ; why exporting that, :sol-owl is not used by anybody
   :defconcept 
   :defontology 
   :defindividual
   :definstmethod 
   :defownmethod 
   :defuniversalmethod
   :defvirtualconcept 
   :defvirtualattribute 
   :defvirtualrelation
   :defruleheader 
   :defrule
   :defchapter 
   :defsection
   :make-ontology 
   :make-concept 
   :make-individual))

(defpackage :sol-html (:use :sol :cl)(:nicknames :h))

;; :EOF