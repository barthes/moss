;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=============================================================================
;;;20/01/30
;;;		D I A L O G - C L A S S E S (File dialog-classes.lisp)
;;;
;;;=============================================================================
;;; This file contains classes and attached methods for setting up a dialog.
;;; The dialog occurs in the :moss space.

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
-------
2005
 0409 creation
      We create a conversation class whose instances will contain instances of 
      a particular conversation graph. The current conversation id is kept in
      the *conversation* global variable.
 0511 adding *transition-verbose* flag
 0906 adding an agent slot to a Q-conversation object to link with OMAS
 0907 adding =display-text to Q-CONVERSATION
2006
 0308 Version 6
      =========
      changing doc-string to :en doc-string in classes
 0814 replaced ACTION attribute in  Q-CONTEXT by CONTEXT-ACTION will probably 
      bring havoc
 1007 modifying =DISPLAY-TEXT for conversations to take into account mln in omas
 1024 adding debug functions to trace conversation
2007
 0201 adding performative property to state-context
 0214 giving more flesh to the Q-TASK class
 0728 modifying =display-text
 1107 updating =display-text
 1203 replading defXXX with defmossXXX for methods
2008
 0604 === v5.11.2
 0605 replacing #+MICROSOFT #+OMAS by #+(and MICROSOFT-32 OMAAS)
 0611 replacing #+(and MICROSOFT-32 OMAS) by #+MICROSOFT-32 (when (member...
 0621 === v7.0.0
2009
 0515 adding FACTS to the conversation object to simplify dialogs by getting rid
      of the Q-context objects
 0519 simplifying the dialog process
 0522 changed print-conversation
 0723 patching voice output
2010
 0513 cleaning file introducing defsysXXX
 0917 introducing MOSS- prefix, replacing Q- prefix with MOSS- as the other system
      classes and properties. Introducing a deftask macro
 1003 improved deftask to apply it to applications
 1111 exporting deftask
 1206 fixing display-text
 1211 declaring special omas::*omas* in =display-text
2011
 0317 changed the =display-text method to print lists even if they are not strings
 0319 adding header and no-newline options to MOSS-CONVERSATION =display-text method
 0423 removing default "PA> " header from =display-text
2012
 0909 changing the voice output mechanism
2013
 0108 adding HTML option to the =display-text method
 0120 adding web slots to the conversation class
2014
 0307 -> UTF8 encoding
2020
 0130 restructuring MOSS as an asdf system
|#

(in-package :moss)

;;;=================================== macros ==================================
;;; debugging macros
(defMacro ps (ent) `(send ',ent '=print-self))
(defMacro pe (ent) `(%pep ',ent))

(defUn print-conversation (cvs)
  (when cvs
    (let ((state (car (HAS-MOSS-STATE cvs)))
          (ag (car (HAS-MOSS-AGENT cvs)))
          (facts (has-MOSS-facts cvs))
          )
      (format t "~%----- ~S~%State: ~S - ~S~%Agent: ~A ~%Goal: ~{~A ~}~
                 ~&Facts: ~{~&   ~S ~} ~&-----"
              cvs
              state (car (HAS-MOSS-LABEL state))
              ag
              (if (HAS-MOSS-GOAL cvs) 
                (cdr (send (car (HAS-MOSS-GOAL cvs)) '=summary))
                (list "*no goal specified*"))
              facts)
      )))

;;;(unless (member :omas *features*)
;;;  (defUn pc () (print-conversation moss::*conversation*)))

#+OMAS
(defUn pc () (print-conversation moss::*conversation*))

;;;(when (member :omas *features*)
;;;  (defUn pc ()
;;;    (declare (special *traced-agent*))
;;;    (print-conversation (omas::conversation *traced-agent*)))
;;;  (defUn cl-user::pc ()
;;;    (declare (special *traced-agent*))
;;;    (print-conversation (omas::conversation *traced-agent*)))
;;;  )

#+OMAS
(defUn pc ()
  (declare (special *traced-agent*))
  (print-conversation (omas::conversation *traced-agent*)))
#+OMAS
(defUn cl-user::pc ()
  (declare (special *traced-agent*))
  (print-conversation (omas::conversation *traced-agent*)))

;;;#+MCL (unless (member :omas *features*)
;;;        (defun cl-user::pc ()(print-conversation moss::*conversation*)))

#+(AND MCL (not OMAS)) 
(defUn cl-user::pc ()(print-conversation moss::*conversation*))

#+(AND MICROSOFT-32 (not OMAS))
(defUn pc ()(print-conversation moss::*conversation*))

;;;================================================================================
;;;
;;;             Minimal MOSS Query Conversation Graph
;;;
;;;================================================================================
;;; This is intended to provide minimal capability for the PA.
;;;================================================================================
;;; Here we define the possibility of querying MOSS as a state graph. 

;;; The object state specifies a node of the expected conversation. The conversation
;;; object records the current state and the content of the Fact Base in the FACTS
;;; slot.
;;; At a given state we ask something to the user, try to make sense out of it using 
;;; rules, and do a transition. 
;;;--------------------------------------------------------------------- MOSS-STATE

(defsysclass "MOSS-STATE"
  (:id $QSTE)
  (:doc 
   :en "A MOSS-STATE models a particular state of a state graph which defines ~
         the possible sequences corresponding to a plan and to a dialog. ~
         A state graph implements a complex skill of an agent.")
  (:att MOSS-STATE-NAME (:id $STNAM) (:entry))
  (:att MOSS-LABEL (:entry)(:id $LBLT))
  (:att MOSS-EXPLANATION (:id $XPLT)) ; text to be posted to a human reader
  )

;;;---------------------------------------------------------------- MOSS-TASK-INDEX

(defsysclass "MOSS-TASK-INDEX"
  (:id $TIDXE)
  (:doc "a TASK INDEX contains an expression and a weight.")
  (:att "MOSS INDEX" (:id $IDXT))
  (:att "MOSS WEIGHT" (:id $WGHT))
  )

;;;---------------------------------------------------------------------- MOSS-TASK

(defsysclass "MOSS-TASK"
  (:id $QTSKE)
  (:doc "A TASK is a model of task used to trigger sub-dialogs.")
  (:att "MOSS task name" (:id $TSKNAM)(:unique)(:entry))
  (:att "MOSS target" (:id $TRGT) (:one-of :object :time :cause :subject :owner))
  (:att "MOSS performative" (:id $PRFT)(:one-of :request :assert :command :answer))
  (:att "MOSS dialog" (:id $DLGT))
  (:rel "MOSS index pattern" MOSS-TASK-INDEX (:id $IDXS))
  )

;;;------------------------------------------------------------- MOSS-DIALOG-HEADER

(defsysclass "MOSS-DIALOG-HEADER"
  (:doc :en "An object used to identify a dialog.")
  (:id $QHDE)
  (:att MOSS-LABEL (:id $LBLT))
  (:att MOSS-EXPLANATION (:id $XPLT)) ; text to be posted to a human sender
  (:rel MOSS-ENTRY-STATE MOSS-STATE (:id $ESTS)) ;  link onto the entry-state
  (:rel MOSS-PROCESS-STATE MOSS-STATE (:id $PRSS)) ; process state entry
  )

;;;-------------------------------------------------------------- MOSS-CONVERSATION

;;; the conversation object is the interface between the MOSS dialog system and 
;;; the external world. It contains a number of fields to exchange information.
;;; WEB I/O
;;;  MOSS-WEB if true means that the request came from a web process. Should be set
;;;    by the web process and removed when the answer is provided
;;;  MOSS-WEB-GATE gate to allow the web process to resume when it is opened
;;;  MOSS-WEB-TEXT part of the HTML text to sent back to user in the body of the 
;;;    page.

(defsysclass "MOSS-CONVERSATION"
  (:doc :en "An object whose instances represent a conversation and points to the ~
              stage of this conversation. A new conversation starts with the ~
              creation of an instance of this class, e.g. after a reset.")
  (:id $CVSE)
  (:att MOSS-OUTPUT-WINDOW (:id $OWS)) ; panel in which to write questions
  (:att MOSS-INPUT-WINDOW (:id $IWS)) ; panel from which to read master's data
  (:rel MOSS-STATE MOSS-STATE (:id $STS)) ; current-state of the conversation
  (:rel MOSS-INITIAL-STATE MOSS-STATE (:id $ISTS)) ; initial state
  (:att MOSS-AGENT (:id $AGT)) ; PA ID implied in the conversation
  (:rel MOSS-DIALOG-HEADER MOSS-DIALOG-HEADER (:id $DHDRS)) ; main conversation header
  (:rel MOSS-SUB-DIALOG-HEADER MOSS-DIALOG-HEADER (:id $SDHDRS)) ; current sub-conversation
  (:att MOSS-FRAME-LIST (:id $FLT)) ; list of return addresses of sub-dialogs
  (:att MOSS-TEXT-LOG (:id $TXLT)) ; record conversation texts
  (:rel MOSS-TASK-LIST MOSS-TASK (:id $TSKL)) ; list of potential tasks for dialog
  (:rel MOSS-TASK MOSS-TASK (:id $TSKS)) ; current task
  (:att MOSS-FACTS (:id $FCT)) ; to replace the context object
  (:att MOSS-INTERRUPT (:id $INTT)) ; if T, allows interrupting a dialog and start a 
  ; new one. This is checked by the dialog-state-crawler to decide to interrupt or not
  ; must be reset to avoid looping
  (:att DETAILS (:id $DTLT)) ; Contains info about the interrupting process 
  ; (usually :ask skill) to allow the dialog to post its results into the
  ; ANSWER-TO-TASK-TO-DO slot of the agent structure
  (:att MOSS-SENDER (:id $SNDT)) ; records the request sender (:ASK skill)
  ; dialog will put the answer into the agent ANSWER slot and quit
  (:att MOSS-WEB-TAG (:id $WEBT)) ; if true indicates web I/O
  (:att MOSS-WEB-GATE (:id $WGAT)) ; gate for synchronizing web process
  (:att MOSS-WEB-OUTPUT (:id $WOUT)) ; save HTML web output
  (:att MOSS-CONTACT (:id $CTCT)) ; contact in the conversation
  )

;;;================================================================================
;;;
;;;                             Actions and Patterns
;;;
;;;================================================================================
;;; Here we define classes for modeling actions and patterns

;;;----------------------------------------------------------------------- ARGUMENT
;;; start with model of argument of a pattern corresponding to an action

(defsysclass MOSS-TASK-ARGUMENT
  (:id $TARGE)
  (:doc :en "MOSS-TASK-ARGUMENT represents one of the arguments that must be filled without ~
              which an action or a task cannot be launched. The requirement however ~
              is not strict since the argument may be optional.
   An argument contains a name, one or more questions to ask the user when it is ~
              missing, a validation function, room for a value, it has a name.")
  (:att MOSS-ARG-NAME (:id $ARNAM)(:entry))
  (:att MOSS-ARG-KEY (:id $ARKT))
  (:att MOSS-ARG-VALUE (:id $ARVT))
  (:att MOSS-ARG-QUESTION (:id $ARQT))
  )

;;;------------------------------------------------------------------- OBJ-ARGUMENT

(defsysclass MOSS-OBJ-ARGUMENT
  (:id $OBRGE)
  (:doc :en "MOSS-OBJ-ARGUMENT is a special type of argument whose value must be a MOSS ~
              object. It may have a CLASS specification, which defaults to *any*. ~
              It has a filtering method on the VALUE argument that tests whether ~
              the argument if of the proper class.")
  (:att MOSS-REFERENCE (:id $REFT) (:vr (:all *any*)))
  (:att MOSS-CLASS-REFERENCE (:id $CREFT)(:default *any*))
  )

;;;------------------------------------------------------------------------- ACTION

(defsysclass MOSS-ACTION
  (:id $ACTE)
  (:doc :en "MOSS-ACTION represents all actions that can be executed by MOSS. For example, ~
              explaining a term or detailing a procedure. It contains an operator ~
              reference (function name) and a list of arguments to be given as ~
              keywords when calling the function.")
  (:att MOSS-ACTION-NAME (:id $ANAM)(:entry))
  (:att MOSS-DOCUMENTATION (:id $DOCT))
  (:att MOSS-OPERATOR (:id $OPRT))
  (:rel MOSS-OPERATOR-ARGUMENTS MOSS-TASK-ARGUMENT (:id $ARGS))
  )

;;; Add ACTION to Conversation header 
;;; a conversation header points to a particular sub-class of action
(defsyssp MOSS-ACTION MOSS-DIALOG-HEADER  MOSS-ENTITY (:id $ACTS))
;;; a conversation object points to an instance of the sub-class of action
;;; created by the =create-pattern method
(defsyssp MOSS-GOAL MOSS-CONVERSATION MOSS-ACTION (:id $GLS))

;;;==================================== methods ===================================

;;;------------------------------------------ (MOSS-CONVERSATION) =CLEAN-OUPUT-PANE

(defmossinstmethod =clean-output-pane MOSS-CONVERSATION ()
  "cleans output pane no matter what"
  (send *self* '=display-text "" :clean-pane t))

;;;---------------------------------------------- (MOSS-CONVERSATION) =DISPLAY-TEXT

;;; adding the patch to activate voice output JPB0708, updated JPB0907
;;; handles strings, mln, list of string, or list of mlns. If string is  not HTML and
;;; output is web output, then make it HTML before calling display-text method. If
;;; string is HTML as indicated by the option, transmit it as it is.

(defmossinstmethod 
    =display-text MOSS-CONVERSATION (text-arg &key erase new-line final-new-line
                                              newline clean-pane html
                                              (color *black-color*)
                                              (header ""))
  "displays text using the output conversation channel. When the text is a multi- ~
   lingual string, extract the proper language version.
Arguments:
   text-arg: text or list of texts to display (possibly multilingual strings)
   clean-pane (key): it T, erase output pane no  matter what
   color (key): a standard color e.g. moss::*red-color*
   erase (key): if T, erases the area (prints null string)
   header (key): leading header if any (default is no header)
   html (key): if t indicates that the string is an HTML string
   newline (key): if t print inserting a newline
Return:
   nil."
  
  #+OMAS
  (declare (special omas::*omas*))
  
  (let* ((output-pane (car (HAS-MOSS-OUTPUT-WINDOW)))
         #+OMAS (agent-key (omas::key (car (has-moss-agent *self*))))
         output-text)
    
    ;; get text as a simple string
    (setq output-text 
          (cond
           ;; NB: HTML string is a string
           ((stringp text-arg) text-arg)
           ;; if MLN, get the one associated with current language
           ((mln::mln? text-arg)  ; jpb 1406
            (or (mln::filter-language text-arg *language*) ""))
           ;; list of strings
           ((and (listp text-arg) (every #'stringp text-arg))
            (nth (random (length text-arg)) text-arg))
           ;; list of MLN
           ((and (listp text-arg) (every #'mln::mln? text-arg)) ; jpb 1406
            (or (mln::filter-language
                 (nth (random (length text-arg)) text-arg)
                 *language*)
                ""))
           ;; if a list, but not of strings or mlns, coerce the list (to print smth)
           ((listp text-arg) 
            (format nil "~{~S~% ~}" text-arg))
           ;; otherwise error
           (t (warn "bad text argument: ~S" text-arg) "")))
        
    ;;=== check if we have to send text back to the web server
    #+OMAS
    (when (web-active? *self*) ; *self* is the conversation object
      (vformat "=display-text /adding to web text:~&  +=>  ~S" output-text)
      ;; put the answer into FACT/text adding it to whatever is there
      (web-add-text *self* output-text)
      ;; open the gate (no, this is done by activate-answer-panel)
      ;(omas::web-open-gate index)
      ;; get out of method, skipping the voice output
      (throw :return nil))
    
    ;;=== here send regular text to a window pane
    ;(vformat "=display-text /window: ~S, output-pane: ~S; erase: ~S text:~&=> ~S" 
    ;         (has-MOSS-output-window) output-pane erase text-arg)
    (vformat "=display-text /text:~&  =>	~S" output-text)
    ;; print a trace into the listener
    (if new-line
        (format t (concatenate 'string "~%" output-text))
      (format t output-text)
      )
    
    ;; if the output pane is a list starting with :tell we send a message to the
    ;; destination (mainly used by OMAS agents)
    ;; e.g. (:tell :to :ML)
    #+OMAS
    (when (symbolp output-pane)
      (omas::send-message
       (make-instance 'omas::message :from agent-key :to output-pane
         :type :inform :action :tell :args `(:data ,output-text)))
      (throw :return nil))
    
;;;    #+OMAS
;;;    (when (and (listp output-pane)(eql (car output-pane) :tell))
;;;      ;; get destination
;;;      (setq agent (car (has-moss-agent)))
;;;      (unless agent
;;;        (error "in MOSS/=display-text: missing agent reference in conversation object."))
;;;      ;; otherwise send message
;;;      (omas::send-message
;;;       (make-instance 'omas::message :from agent :to (getf (cdr output-pane) :to)
;;;         :type :inform :action :tell :args `(:data ,output-text)))
;;;      ;(setf (omas::answer-to-return agent) output-text)
;;;      ;; get out
;;;      (throw :return nil))
    
    ;; if there is no window to print info, quit
    (unless output-pane 
      (format t "~%; CONVERSATION/=display-text /no output window and no web mark.")
      (throw :return nil))
    
    (unless (eql output-pane t)
      ;; here we assume that the output-window slot of the conversation object 
      ;; contains the window object and that the window object has a display-text 
      ;; method
      (display-text output-pane output-text :erase erase :newline newline
                    :final-new-line final-new-line :header header :html html
                    :clean-pane clean-pane :color color)) ; JPB1104 color
    
    ;;=== voice output only works on PCs
    #+OMAS
    ;; first get agent id from conversation
    (let ((agent (car (has-MOSS-agent *self*))))
      ;; if agent id is not null and the same as the agent id of the voice-
      ;; interaction then send the output to the voice channel
      ;; however, if we have a web interface, we do not use voice with HTML strings
      ;; presumably PA using web interface do not use voice
      (if (and agent (omas::voice-io agent)(not html)) ; JPB1209
          ;; make sure that the arg is a string
          (omas::net-voice-send agent (format nil output-text))))    
    nil))
              		   
;;;----------------------------------------------------- (MOSS-CONVERSATION) =RESET

(defmossinstmethod =reset MOSS-CONVERSATION ()
  "reset does several things:
     - clean the FACTS area in MOSS-CONVERSATION
     - doe not modify web parameters
     - puts a new goal (action pattern) into the GOAL slot of conversation"
  (let ((action (car (HAS-MOSS-ACTION (car (HAS-MOSS-DIALOG-HEADER)))))
        )
    (setf (HAS-MOSS-FACTS *self*) nil)
    ;; set a new goal specified by the type of action in the dialog header
    (if action
      (send *self* '=replace 'HAS-MOSS-GOAL
            (list (send action '=create-pattern)))
      ;; otherwise should delete everything
      (progn
        (send *self* '=get 'HAS-MOSS-GOAL)
        (if *answer*
            (send *self* '=delete-sp 'HAS-MOSS-GOAL *answer*)))
      )))

;;;--------------------------------------------------------- (MOSS-ACTION) =SUMMARY

(defmossinstmethod =summary MOSS-ACTION ()
  "returns the key, name and value of the argument."
  (append (HAS-MOSS-ACTION-NAME) '(":") (HAS-MOSS-OPERATOR) 
          (broadcast (HAS-MOSS-OPERATOR-ARGUMENTS) '=summary)))

;;;-------------------------------------------------- (MOSS-TASK-ARGUMENT) =SUMMARY

(defmossinstmethod =summary MOSS-TASK-ARGUMENT ()
  "returns the key, name and value of the argument."
  (append (HAS-MOSS-ARG-KEY) '(":") (HAS-MOSS-ARG-NAME) 
          (or (HAS-MOSS-ARG-VALUE) '("*unspecified*"))))

;;;--------------------------------------------------- (MOSS-OBJ-ARGUMENT) =SUMMARY

(defmossinstmethod =summary MOSS-OBJ-ARGUMENT ()
  "returns the key, name and value of the argument."
  (append (HAS-MOSS-ARG-KEY) '(":") (HAS-MOSS-ARG-NAME) 
          (or (HAS-MOSS-ARG-VALUE) '("*unspecified*"))
          '("/ CLASS-REFERENCE :")(or (HAS-MOSS-CLASS-REFERENCE)'("*unspecified*"))
          '("/ FILTER :") (or (HAS-MOSS-REFERENCE)'("*unspecified*"))))

;;;---------------------------------------------------------- (MOSS-STATE) =SUMMARY

(defmossinstmethod =summary MOSS-STATE ()
  "Returns the state label to be printed."
  (HAS-MOSS-LABEL))

;;;================================================================================
;;;
;;;                             Actions and Patterns
;;;
;;;================================================================================
;;; Here we define classes for modeling actions and patterns

;;;------------------------------------------------------------- (ARGUMENT) =FILTER
;;; start with model of argument of a pattern corresponding to an action

(defmossownmethod =FILTER $TARGE (input)
  "takes a user input and tries to recover some value corresponding to the argument. ~
   This method has to be redefined for each type of argument..."
  input)

;;;------------------------------------------------------------- (OBJ-ARGUMENT) =XI

(defmossownmethod =xi $CREFT (value)
  "when the value has the right type we return it, otherwise we return nil."
  (if (%type? value (car (HAS-MOSS-CLASS-REFERENCE))) value))

;;;------------------------------------------------------------- (ACTION) =ACTIVATE

(defmossinstmethod =activate MOSS-ACTION ()
  "takes an action instance builds a message and sends it to self. Thus, action ~
   message should be defined at the action level."
  (apply #'send *self*
         (car (HAS-MOSS-OPERATOR))  ; message name
         (reduce #'append      ; arguments (keys)
                 ;; for each argument we build the pair key value
                 (mapcar #'(lambda (xx) (cons (car (HAS-MOSS-ARG-KEY xx))
                                              (HAS-MOSS-ARG-VALUE xx)))
                         (HAS-MOSS-OPERATOR-ARGUMENTS)))))

;;;------------------------------------------------------- (ACTION) =CREATE-PATTERN

(defmossownmethod =create-pattern $ACTE ()
  "create a pattern ACTION <OPR, ARG*> to be filled as a conversation goal.
Arguments:
   none
Return:
   the id of the action."
  
  (let* ((sp (%%get-id 'HAS-MOSS-OPERATOR-ARGUMENTS :prop :class-ref *self* 
                       :include-moss t))
         (suc-list (HAS-MOSS-SUCCESSOR sp))
         nn lres key)
    ;; for each successor in the list build the required number of argument objects
    (dolist (suc suc-list)
      ;; first get max number of args
      (setq nn (car (HAS-MOSS-MAXIMAL-CARDINALITY suc)))
      ;; if nil; default is one
      (dotimes (jj (if (numberp nn) nn 1))
        ;; build an argument
        (push (%%make-instance-from-class-id suc) lres)))
    ;; build the ACTION object
    (setq key (%%make-instance-from-class-id *self*))
    ;; add list of arguments
    (send key '=add-sp 'HAS-MOSS-OPERATOR-ARGUMENTS (reverse lres))
    ;; return key
    key))

;;;------------------------------------------------ (ACTION) =DISPLAY-DOCUMENTATION

(defmossinstmethod =display-documentation MOSS-ACTION (&key obj-id)
  "prints the documentation associated with the object using the output conversation ~
   channel.
Arguments:
   obj-id (key): object of interest
Return:
   nil."
  (let ((conversation (car (send *self* '=get-id '$gls.of)))
        (text (or (car (send obj-id `=has-value-id '$DOCT))
                  "*No documentation available*")))
    ;; we assume that we can get the conversation object
    ;; doc
    (when (%pdm? conversation)
      (send conversation '=display-text 
            (concatenate 'string
                         (or (car (send obj-id '=summary)) (symbol-name obj-id))
                         "~%")
            :no-new-line t)
      ;; required because text may be multlingual
      (send conversation '=display-text  (format nil "~A" text)))))

;;;------------------------------------------------------- (ACTION) =DISPLAY-OBJECT

(defmossinstmethod =display-object MOSS-ACTION (&key obj-id)
  "prints the object using the output conversation channel.
Arguments:
   obj-id (key): object of interest
Return:
   nil."
  (let ((conversation (car (send *self* '=get-id '$gls.of))))
    ;; we assume that we can get the conversation object
    ;; doc
    (when (%pdm? conversation)
      (send obj-id '=print-self :stream (car (has-MOSS-output-window conversation))))))

;;;--------------------------------------------------------------- (ACTION) =READY?

(defmossinstmethod =ready? MOSS-ACTION ()
  "checks that all args are filled."
  (every #'(lambda(xx) xx) (mapcar #'HAS-MOSS-ARG-VALUE (HAS-MOSS-OPERATOR-ARGUMENTS))))

;;;------------------------------------------------------------------ %MAKE-PATTERN

(defUn %make-pattern ()
  "builds a set of objects representing an action pattern from the action"
  :to-do)

;;;================================================================================
;;;
;;;                                MACROS
;;;
;;;================================================================================

;;;------------------------------------------------------------------------ DEFTASK

;;; tasks are defined in the MOSS package for the help dialog. However, they are
;;; normally defined in the agent's package for standard dialogs

(defMacro deftask (name &key target (performative '(:request :command)) dialog
                        indexes doc where-to-ask action)
  (let (res)
    ;(warn "~%; deftask /*package*: ~S" *package*)
    (if (eql *package* (find-package :moss))
        `(defindividual "MOSS-TASK"
             (:doc ,@(if doc (list doc) '("*no documentation*")))
           ,@ (if target `(("moss target" ,target)))
           ("moss task name" ,name)
           ,@(if (listp performative) `(("moss performative" . ,performative))
               `(("moss performative" ,performative)))
           ;("moss performative" ,performative)
           ("moss dialog" ,@(if dialog (list dialog)
                              (error "missing dialog for ~S" name)))
           ("moss index pattern"
            ,@(loop
                (unless indexes (return (reverse res)))
                (unless (cdr indexes)
                  (error "arg to :indexes should be an even list in ~S" name))
                (push `(:new "MOSS task index" ("MOSS index" ,(pop indexes))
                             ("MOSS weight" ,(pop indexes)))
                      res)))
           )
      ;; otherwise we are in an application package
      `(defindividual "TASK"
           (:doc ,@(if doc (list doc) '("*no documentation*")))
         ,@ (if target `(("target" ,target)))
         ("task name" ,name)
         ;; if performative is a list, adds it as a list
         ,@(if (listp performative) `(("performative" . ,performative))
             `(("performative" ,performative)))
         ;("performative" ,performative)
         ("dialog" ,@(if dialog (list dialog) (error "missing dialog for ~S" name)))
         ("index pattern"
          ,@(loop
              (unless indexes (return (reverse res)))
              (unless (cdr indexes)
                (error "arg to :indexes should be an even list in ~S" name))
              (push `(:new "task index" ("index" ,(pop indexes))
                           ("weight" ,(pop indexes)))
                    res)))
         ,@ (if where-to-ask `(("where to ask" ,where-to-ask)))
         ,@ (if action `(("message action" ,action)))
         ))
    ))
#|
(deftask "What is XXX?"
  :doc "Task to explain the meaning of a concept in the ontology (question)."
  ;:performative :request
  :dialog _what-conversation
  :target _my-target
  :indexes
   ("what is" .4
    "what are" .5
    )
  )
expands to:
(DEFINDIVIDUAL "MOSS-TASK" 
    (:DOC "Task to explain the meaning of a concept in the ontology (question).")
  ("moss target" _MY-TARGET) ("moss task name" "What is XXX?")
  ("performative" :REQUEST :COMMAND) ("moss dialog" _WHAT-CONVERSATION)
  ("moss index pattern"
   (:NEW "MOSS task index" ("MOSS index" "what is") ("MOSS weight" 0.4))
   (:NEW "MOSS task index" ("MOSS index" "what are") ("MOSS weight" 0.5))))
|#

(format t "~%;*** MOSS v~A - dialog classes loaded ***" *moss-version-number*)

;;; :EOF