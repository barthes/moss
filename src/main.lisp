;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=================================================================================
;;;20/01/30		
;;;	               M A I N - (File main.lisp)
;;;	
;;;=================================================================================

#|
2020 
 0130 creation
|#

(in-package :moss)

;;;---------------------------------------------------------------------------------
;;; record the location of the folder containing the files
;;; #P"C:\\Users\\Administrateur\\common-lisp\\moss\\src\\"
;;; cannot use *load-pathname*, because fasl files are loaded from a cache location

(defParameter *moss-directory-pathname*
  (make-pathname
   :device "C"
   :directory '(:ABSOLUTE "Users" "Administrateur" "common-lisp" "moss" "src")
   ))
  

(eval-when (:load-toplevel)
  ;; create a stub in the user environment so that we can work and create items
  ;; in it directly
  (%create-new-package-environment :cg-user "CG-USER")
  ;;; launch MOSS by opening MOSS window
  (unless (member :omas *features*) ; JPB0806
    (cg::set-foreground-window (make-moss-init-window)))
  )

(format t "~%;*** MOSS version ~A loaded ***" moss::*moss-version-number*)

(format t
        "~%;*** Methods are cached: *cache-methods* set to T *** ~
         ~&;*** Fancy inheritance turned off *lexicographic-inheritance* set to T ***")

;;; :EOF