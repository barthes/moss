﻿;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;===============================================================================
;;;20/01/30
;;;		         M O S S - K E R N E L   (file kernel.lisp)
;;;
;;;===============================================================================
;;; Following objects and methods define the MOSS kernel in addition to the
;;; ones located in the bootstrap MOSSBOOT.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#



;;; WARNING: some of the elementary method (e.g. =put, =remove) deal directly
;;; with the object implementd format. Thus they DO NOT respect the integrity
;;; constraints of the PDM model.
;;;
;;; Methods are listed in alphabetical order for easier reference.
;;; All printing functions use *moss-output* as output channel

;(break "========== loading moss-kernel: Break for tests ==========")

#| List of Kernel Methods (- means unavailable or deprecated methods)
-    =ADD (attribute) deprecated use =ADD-VALUES
-    =ADD (relation) deprecated use =ADD-VALUES
  =ADD-ATTRIBUTE-DEFAULT (concept)
-    =ADD-ATTRIBUTE-VALUES (universal) deprecated use =ADD-VALUES
-    =ADD-ATTRIBUTE-VALUES-USING-ID (universal) deprecated kept for compatibility
-    =ADD-RELATED-OBJECTS (universal) deprecated use =ADD-VALUES
-    =ADD-SP (universal) deprecated kept for compatibility
-    =ADD-SP-ID (universal) deprecated kept for compatibility
-    =ADD-TP (universal) deprecated kept for compatibility
-    =ADD-TP-DEFAULT (concept) deprecated use =ADD-ATTRIBUTE-DEFAULT
-    =ADD-TP-ID (universal) deprecated kept for compatibility
  =ADD-VALUES (universal)
  =BASIC-NEW (universal)
  =CHANGE-CLASS (universal)
  =CHANGE-CLASS-ID (universal)
  =CHECK (attribute)
  =CHECK (relation)
-    =CHECK (universal) unavailabl
  =CHECK-CARDINALITY-CONSTRAINTS (universal)
-    =CHECK-REQUIRED-PROPERTIES (universal) unavailable
  =CLONE (universal)
-    =CLONE-FROM-CONTEXT (universal) unavailable
-    =DELETE (attribute) deprecated use =DELETE-VALUES
-    =DELETE (inverse-link)
-    =DELETE (relation) deprecated use =DELETE-VALUES
  =DELETE (universal)
-    =DELETE-ALL (attribute) deprecated use =DELETE-VALUES
-    =DELETE-ALL (inverse-link)
-    =DELETE-ALL (relation) deprecated use =DELETE-VALUES
-    =DELETE-ALL-SUCCESSORS (universal) deprecated use =DELETE-VALUES
-    =DELETE-ATTRIBUTE (universal) deprecated use =DELETE-VALUES
-    =DELETE-ATTRIBUTE-VALUES (universal) deprecated use =DELETE-VALUES
-    =DELETE-RELATED-OBJECTS (universal) deprecated use =DELETE-VALUES
-    =DELETE-SP (universal) deprecated use =DELETE-VALUES
-    =DELETE-TP (universal) deprecated use =DELETE-VALUES
  =DELETE-VALUES (universal)
-    =EDIT (method) unavailable
  =ERASE (universal)
  =FILTER (relation)
  =FIND (MOSS system)
  =FORMAT-TRACE (universal)
  =FORMAT-VALUE (attribute)
  =FORMAT-VALUE (relation)
  =FORMAT-VALUE-LIST (attribute)
  =FORMAT-VALUE-LIST (inverse-link)
  =FORMAT-VALUE-LIST (relation)
  =FORMAT-WARNING (universal)
  =GET (universal)
  =GET+ (universal)
  =GET++ (universal)
-    =GET-ALL-OBJECTS (MOSS system) unavailable
  =GET-CLASSES (system)
  =GET-DEFAULT (universal)
  =GET-DEFINITION-SUMMARY (universal)
  =GET-DOCUMENTATION (entry-point)
  =GET-DOCUMENTATION (mehod)
  =GET-ID (universal)
  =GET-INSTANCES (own/attribute)
  =GET-INSTANCES (own/concept)
  =GET-INSTANCES (concept) shadowed by own method?
  =GET-INSTANCES (own/counter)
  =GET-INSTANCES (own/entry-point)
  =GET-INSTANCES (own/inverse-link)
  =GET-INSTANCES (own/method)
  =GET-INSTANCES (own/relation)
  =GET-INTERNAL-INSTANCE-NUMBER (universal)
  =GET-INVERSE-LINK-INFO (universal)
  =GET-INVERSE-PROPERTIES (universal)
  =GET-LAST-INSTANCE (concept)
  =GET-NEXT-INSTANCE (universal)
  =GET-PREVIOUS-INSTANCE (universal)
  =GET-NAME (attribute)
  =GET-NAME (universal)
  =GET-PROPERTIES (universal)
-    =GET-USER-CLASSES (oan/concept) unavailable
-    =GET-USER-CONCEPTS (own/concept) unavailable
  =HAS-INVERSERSE-PROPERTIES (universal)
  =HAS-PROPERTIES (universal)
  =HAS-TYPE? (universal)
  =HAS-VALUE (universal)
  =HAS-VALUE-ID (universal)
  =ID (entry-point)
  =IF-ADDED (attribute)
  =IF-ADDED (entity)
  =IF-ADDED (relation)
-    =IF-NEEDED (universal) unavailable
  =IF-REMOVED (attribute)
  =IF-REMOVED (entity)
  =IF-REMOVED (inverse-link)
  =IF-REMOVED (relation)
  =INCREMENT (counter)
  =INHERIT-INSTANCE (universal) unused
  =INHERIT-ISA-INSTANCE (universal) unused
  =INHERIT-OWN (universal) unused
  =INPUT-VALUE (attribute) unaailable
  =INSTANCE-NAME (attribute)
  =INSTANCE-NAME (concept)
  =INSTANCE-NAME (inverse-link)
  =INSTANCE-NAME (method)
  =INSTANCE-NAME (relation)
  =INSTANCE-NAME (universal)
  =INSTANCE-SUMMARY (concept)
  =INSTANCE-SUMMARY (relation)
  =INVERSE-ID (attribute)
  =INVERSE-ID (inverse-link)
  =INVERSE-ID (relation)
  =KILL-METHOD (universal)
  =LINK (relation)
-    =LOAD-APPLICATION (MOSS system) unavailable
  =MAKE-ENTRY (own/has-moss-concept-name)
  =MAKE-ENTRY (own/has-moss-property-name)
  =MAKE-OBJECT-DESCRIPTION (universal) used by OMAS
  =MAKE-PRINT-ALIST (universal)
  =MAKE-PRINT-STRING (attribute)
  =MAKE-PRINT-STRING (relation)
  =MAKE-PRINT-STRING (universal)
  =MAKE-STANDARD-ENTRY (attribute)
-    =MERGE (entry-point) unavailable
  =MODIFY-VALUE (attribute) unused
  =NEW-VERSION (MOSS system)
  =NORMALIZE (attribute) unused
  =PARSE-SUMMARY (universal)
  =PRINT-ALL (universal)
  =PRINT-ALL-INSTANCES (concept)
  =PRINT-AS-METHOD-FOR (entry-point)
  =PRINT-CODE (method)
  =PRINT-DOC (method)
  =PRINT-DOCUMENTATION (universal)
  =PRINT-ERROR (universal)
  =PRINT-HISTORY (universal)
  =PRINT-INSTANCE (concept)
  =PRINT-LOCAL-METHODS (universal)
  =PRINT-METHODS (universal)
  =PRINT-NAME (concept)
  =PRINT-OBJECT (universal)
  =PRINT-SELF (entry-point)
  =PRINT-SELF (method)
  =PRINT-SELF (universal)
  =PRINT-TYPICAL-INSTANCE (concept)
  =PRINT-VALUE (attribute)
  =PRINT-VALUE (inverse-link)
  =PRINT-VALUE (relation)
  =REMOVE-ATTRIBUTE (universal)
  =REPLACE (universal)
  =SAVE (attribute)
  =SAVE (concept)
  =SAVE (counter)
  =SAVE (entry-point)
  =SAVE (inverse-relation)
  =SAVE (method)
  =SAVE (relation)
  =SAVE (system)
  =SAVE (universal)
  =SAVE (virtual concept)
-    =SAVE-APPLICATION (MOSS system) unavailable
  =SAVE-INSTANCES (concept)
  =SAVE-ORPHANS (MOSS system)
-    =SAVE-APPLICATION-OBJECTS-IN-PACKAGE (MOSS system) unavailable
  =SET (universal)
  =SET-LIST (universal)
  =SET-ID (universal)
  =SET-ID-LIST (universal)
  =SUMMARY (attribute)
  =SUMMARY (concept)
  =SUMMARY (counter)
  =SUMMARY (inverse-link)
  =SUMMARY (method)
  =SUMMARY (relation)
  =SUMMARY (MOSS system)
  =SUMMARY (universal)
  =UNLINK (inverse-link)
  =UNLINK (relation)
  =WHAT? (universal)
  =XI (attribute)
|#

#|
2001
 0930 putting some color
2003
 0110 cleaning the references to *cw* and to *current-window*
2004
 0610 changing ($VALT) to (g-> $VALT) in =add for TERMINAL-PROPERTY
      ignoring option-list in =delete-sp (universal) and in =replace
      changing ($VALT) to (g=> '$VALT) in =summary for COUNTER
      updating =unlink for $EIL and $EPS
 0622 correcting a mistake in =add STRUCTURAL-PROPERTY apply -> funcall
 0628 exporting method names from the kernel file
 0712 P: wrapping export in an eval-when
      P: replacing (neq... with (not (eql...
 0725 adding the own-method =get-user-classes for $ENT
 1003 changing =save-application to rename old file
 1007 changing print method to print into window
 1025 updating saving and reloading apps functions
2005
 0307 updating =save-application and =load-application for MCL environment
      rebuilding properties accessors in =load-application
 0326 removing =in-system?
 0329 modification de =summary pour $EPT
 0418 introduced %validate-tp-value in =xi
2005
 0622 changing =summary methods for attributes and relations
 1126 changing FORMAT-VALUE for EPT
 1214 version 6.0
      ===========
 1227 methods checked against the 6-moss-tests.lisp file
 1231 adding =check methods for properties
      DESIGN DECISION: all lethods dealing with pieces of objects are not 
      responsible for checking constraints related to properties. Those must
      be chhecked explicitely when the object has been fully created or
      modified, using the =check methods.
      This is implemented by the defindividual macro.
2006
 0217 Version 6
      =========
      Adding =format-value-list methods to the library
 0228 change =get-name and =instance-name for $EPT
 0307 changing =make-entry for properties
 0312 correction on =delete-sp and =delete-tp
 0320 change default output to *moss-output*
 0618 doing extensive clean up in the methods and introducing new names
 1007 make =get print internal id of object in case of error
 1107 modifying =GET-INSTANCES (CONCEPT)
 1203 adding =make-print-string methods
2007
 0127 cleaning the *answer* references in the method code
 0128 adding =delete-all-successors
 0130 modifying =replace to accept null argument
 0208 adding =print-documentation universal method
 0221 correcting =add-attribute-values and =add-related-objects to allow extra
      properties
 0508 adding option :always t to mln filtering functions to get some answer 
      when *language* is :all
 0824 created =get-documentation to otain documentation strings
 1003 modified =get to handle the case of inverse prop refes e.g. ">author"
 1017 adding universal method =get-definition-summary
 1202 adding an export option to =add, =clone, =clone-from-context, =merge
 1203 replacing defXXX with defmossXXX in order to export the method names to other
      packages
2008
 0130 added relations to =get-default
 0531 temporarily create access and display-text
 0604 === v5.11.2
 0611 #+ACL -> #+MICROSOFT-32
 0621 === v7.0.0
 0715 Minor cosmetic changes
2009
 1017 adding =get-documentation for entry-points
2010
 0109 adding =set-list (used in omas::load-agent-initialize-environment)
 0113 fixing the =get-instances owm method for counters
 0330 adding special =save for counters (used for omas agent, skill and goal)
 0517 fixing package in entry-point =save method
 0519 adding =make-print-alist
 0729 adding no-warning key arg to =get universal method
 0820 adding :include-subclasses key in =get-instances for concepts
 0917 prefixing MOSS names with MOSS-
 0923 adding =format-value-list for inverse links that had disappeared
 0929 bug in =clone
 1004 $E-SYS.1 -> $SYS.1 in system =save
 1028 adding =INSTANCE-NAME for universal methods
 1121 reactivating the fake access function
2011
 0729 adding =get-last-instance =get-next-instance =get-previous-instance
 0825 adding =get-internal-instance-number
2012
 0201 adding HAS-TYPE? universal method
2013
 0419 fixed =print-doc
 1113 fixing bug in (attribute) =add (cardinality check)
 1118 adding =make-object-description
 1123 adding default =parse-summary universal method
2014
 0108 normalizing class-ref in =make-object-description
 0111 adding if-added and if-removed as instance methods to classes to apply when
      an instance is added or removed
 0212 modifying =make-object-description to include inverse links
      adding =get-inverse-link-info
 0307 -> UTF8 encoding
 0813 modifying =make-object-description to take into account new MLN format
 1119 adding %mln? tests to allow old MLN format
2015
 0428 changing the definition of =what? and get-documentation
 1121 inserting add-values API function in =add methods
 1225 fixing =get
2016
 0222 adding =get-default method to =get
 0625 adding =erase that removes a property in the current version
 1108 adding =get+ and =get++ for dealing with MLN (like equal+)
|#


(in-package :moss)

;;; kludge: we define 2 functions temporarily to keep the MCL compiler happy JPB0805
;(defUn access (&rest ll) ll)
; this is no longer necessary if we load kernel after query
;(defUn display-text (&rest ll) ll)
;;; we make them unbound at the end of the file

;;;================================================================================
;;;                               =ADD Methods
;;;================================================================================

;;;--------------------------------------------------------------- =ADD (ATTRIBUTE)
;;; Modified to accomodate MLNs
;;; an MLN is considered as a list of values, i.e.a property may not have more 
;;; than 1 MLN value.
;;; - if value-list is an MLN, then applies =xi, which may return nil, a list
;;;   of strings, a string or an MLN
;;; - if the base value is an MLN and value-list is an MLN, then merges the two
;;; - if the base value is an MLN and the values are a list of strings, then
;;;   merges with the language of the language option or with *language*. It 
;;;   is an error if no language can be determined. 
;;; - if the base value is a list of strings and the value-list an MLN, then
;;;   issues a warning and produces an MLN using current language for the list of
;;;   strings, if a mied list then error
;;; - if the base value is a list of strings and the value list a string, a number
;;;   a keyword or a list of such things, then the normal case applies
;;; Options before-nth, before-value, different, language, same, entry, export apply.

(defmossinstmethod =add MOSS-TERMINAL-PROPERTY (obj-id value-list &rest option-list)
  "Add a value or a list of new values. Duplicates are NOT discarded. ~
   Values are normalized using the =xi method, then added at the end of the ~
   current list. The =if-added method is checked. Restrictions are checked ~
   and warning messages are printed unless the :no-warning option is t.
Arguments:
   obj-id: ID of object to modify
   value-list: list of values to add
   option-list (opt):
       (:allow-duplicates) - if t, duplicated values are allowed
       (:before-value data) - to insert list in front of the specific data which ~
         must be in internal normalized format
       (:before-nth nth) - to insert list in the nth position (useful when there ~
         are several identical values - that can happen with TPs)
       context is spcified through with-context
       (:export t/nil) - to export entry-point built in the MOSS package
       (:language <tag>) - in case we deal with MLNs
       (:no-warning t/nil) - if true, no arning are printed (default nil)
Return:
   the internal representation of the modified object."
  (add-values obj-id *self* (if (listp value-list) value-list (list value-list))
              :allow-duplicates (car (getv :allow-duplicates option-list))
              :export (car (getv :export option-list))
              :before-value (or (car (getv :before-value option-list))
                                (car (getv :before option-list))) ; compatibiity
              :before-nth (car (getv :before-nth option-list))
              :language (car (getv :language option-list))
              :no-warning (car (getv :no-warning option-list))))

#|
See tests in z-moss-tests-add-values.lisp
|#
;;;---------------------------------------------------------------- =ADD (RELATION)

(defmossinstmethod =add MOSS-RELATION (obj-id suc-list &rest option-list)
  "Add a successor or a list of new successors. Duplicates are discarded.~ 
    Successors are added at the end of the current list. Each successor's type ~
   is checked and must be allowed by the property. All constraints implemented ~
   by means of the =filter and the =if-added method are checked. Whenever they ~
   fail, the corresponding successor is not added. Cardinality constraints are ~
   checked for maximal value. If too many values are specified, then some of ~
   them are discarded. Minimal cardinality is also checked and a warning is ~
   eventually issued.
Arguments:
   object-id: first object id
   suc-list: successor or list of successors
   option-list (opt): 
       (:before-value successor-id) - to insert list in front of the specific successor.
       (:before-nth nth) - to insert the values starting ot the nth position.
       (:no-warning t/nil) - if true, warnings are not printed
Returns:
   the modified internal object representation."
  (add-values obj-id *self* (if (listp suc-list) suc-list (list suc-list))
              :before-nth (car (getv :before-nth option-list))
              :before-value (car (getv :before-value option-list))
              :no-warning (car (getv :no-warning option-list))))

;;;----------------------------------------------- =ADD-ATTRIBUTE-DEFAULT (CONCEPT)
;;; does not check %validate-tp on the value before adding it. Presumably the user
;;; did it before

(defmossinstmethod =add-attribute-default MOSS-ENTITY (att-ref value)
  "adds a default value for a given attribute of a class. The default is added  ~
   to the ideal, replacing whatever value was present. 
Arguments:
   att-ref: symbol or (multilingual) string specifying the attribute
   value:the default value to be added to the ideal
Return:
   the internal representation of the modified ideal."
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (ideal-id (%make-id-for-ideal  *self*))
         (prop-list (send ideal-id '=get-properties))
         tp-id data)
    
    (setq tp-id
          (catch :error
            ;; if there is an ambiguity over the tp-id, then we declare an error
            (%%get-id att-ref :attribute :class-ref *self*)))
    
    ;; result can be nil a symbol or a string (in case of error)
    (cond 
     ((null tp-id)
      (warn "Attribute ~A cannot be found when processing object ~A in package ~S ~
             with context ~S. Default is ignored." 
        att-ref *self* *package* context))
     
     ((stringp tp-id)
      (warn tp-id)
      (warn "Attribute ~A cannot be found when processing object ~A in package ~S ~
             with context ~S. Default is ignored." 
        att-ref *self* *package* context)
      (format t tp-id))
     
     (t
      ;;--- check that tp-id is a member of list of properties
      (unless (member tp-id prop-list)
        (warn "Attribute ~A does not belong to allowed properties for ~A in ~
               package ~S with context ~S. But we add it with default to the ~
               ideal anyway..."
              att-ref *self* *package* context))
      ;; normalize external value
      (setq data (send tp-id '=xi value))
      ;; quit if it cannot be normalized
      (unless data
        (warn " Value ~A has not the proper format for attribute ~A of object ~A ~
                in package ~S with context ~S - and is ignored" 
              value att-ref *self* *package* context)
        (throw :return nil))
      ;; call now the real stuff
      (%%set-value ideal-id data tp-id context)
      ))))

#|
(with-package :test
  (with-context 0
    (send 'test::$E-PERSON '=add-attribute-default "age" 20)))
(($TYPE (0 TEST::$E-PERSON)) (TEST::$T-PERSON-AGE (0 20)))
|#
;;;---------------------------------------------- =ADD-ATTRIBUTE-VALUES (UNIVERSAL)

(defmossuniversalmethod =add-attribute-values (att-ref value-list &rest option-list)
  "Deprecated. Use =add-values"
  (apply #'-> *self* '=add-values att-ref value-list option-list))
   
;;;------------------------------------- =ADD-ATTRIBUTE-VALUES-USING-ID (UNIVERSAL)

(defmossuniversalmethod =add-attribute-values-using-id 
    (att-id value-list &rest option-list)
  "Deprecated. Use =add-values"
  (when value-list 
    (apply #'-> att-id '=add-values *self* value-list option-list)))

;;;----------------------------------------------- =ADD-RELATED-OBJECTS (UNIVERSAL)

(defmossuniversalmethod =add-related-objects (rel-mln successor-list &rest option-list)
  "Deprecated. Use =add-values"
  (apply #'-> *self* '=add-values rel-mln successor-list option-list))

;;;-------------------------------------- =ADD-RELATED-OBJECTS-USING-ID (UNIVERSAL)

;;;(defmossuniversalmethod =add-related-objects-using-id 
;;;  (rel-id successor-list &rest option-list)
;;;  "Links successors to a given entity. Does not check that successors are of ~
;;;   proper type yet. If something wrong happens for a particular successor then ~
;;;   it is ignored. If too many successors are specified a warning is issued.
;;;   Unless the exact prop id is known it is better to use =add-sp.
;;;Arguments:
;;;   sp-id: identifier of structural property (relation)
;;;   successor-list: list of successors (object-ids)
;;;   option-list:  (:before <successor-id>)  to insert in front of given successor
;;;Return:
;;;   not significant."
;;;  (if successor-list
;;;    (apply #'send rel-id '=add *self* successor-list option-list)))

;;;------------------------------------------------------------ =ADD-SP (UNIVERSAL)

(defmossuniversalmethod =add-sp (rel-mln successor-list &rest option-list)
  "Deprecated. Use =add-related-objects."
  (apply #'-> *self* '=add-values rel-mln successor-list option-list))

;;;--------------------------------------------------------- =ADD-SP-ID (UNIVERSAL)

(defmossuniversalmethod =add-sp-id (sp-id successor-list &rest option-list)
  "Deprecated. Use =add-relations-using-id."
  (apply #'-> *self* '=add-values sp-id successor-list option-list))

;;;------------------------------------------------------------ =ADD-TP (UNIVERSAL)

(defmossuniversalmethod =add-tp (att-ref value-list &rest option-list)
  "Deprecated. Use =add-attribute-values."
  (apply #'-> *self* '=add-values att-ref value-list option-list))

;;A------------------------------------------------------ =ADD-TP-DEFAULT (CONCEPT)

(defmossinstmethod =add-tp-default MOSS-ENTITY (tp-ref value)
  "Deprecated. Use =add-attribute-default."
  (-> *self* '=add-attribute-default tp-ref value))

;;;--------------------------------------------------------- =ADD-TP-ID (UNIVERSAL)

(defmossuniversalmethod =add-tp-id (tp-id value-list &rest option-list)
  "Deprecated. Use =add-attribute-values-using-id." 
  (apply #'-> *self* '=add-values tp-id value-list option-list))

;;;-------------------------------------------------------- =ADD-VALUES (UNIVERSAL)
;;; main function for adding values to an object, e.g.
;;;   (send '$E-TEST.1 '=add-values "aa" '(1 2 3) '(:no-warning t))

(defmossuniversalmethod =add-values (prop-ref value-list &rest option-list) 
  "universal method to replace all methods for adding lists of values to properties.
Arguments:
   prop-ref: string or property name or property id
   value-list: list of raw values (for attributes) or pointers (for relations)
   option-list:
     (:allow-duplicates t/nil) default nil
     (:before-value <raw val>)
     (:before-nth <integer>)
     (:language <language-tag>) - for MLN 
     (:no-warning t/nil) default nil
     for context, use with-context
Return:
   internal list of modified object
   list of warning messages"
  (let ((nwrn (car (getv :no-warning  option-list)))
        result message-list)
    
    ;; call the API function
    (multiple-value-setq (result message-list)
      (add-values *self* prop-ref (if (listp value-list) value-list (list value-list))
                  :allow-duplicates (car (getv :allow-duplicates option-list))
                  :no-warning (car (getv :no-warning option-list))
                  :before-value (car (getv :before-value option-list))
                  :before-nth (car (getv :before-nth option-list))
                  :export (car (getv :export option-list))
                  :language (car (getv :language option-list))))
    
    ;; print warnings eventually
    (unless nwrn
      (mapc #'mformat message-list))
    ;; return value
    result      
    ))

;;;============================= End =add methods =================================

;;;--------------------------------------------------------- =BASIC-NEW (UNIVERSAL)
;;; ***** Could be used to create orphans from instances, i.e. objects without 
;;; type, or with type *none*, linked to the cloning instance by an $IS-A
;;; property.

(defmossuniversalmethod =basic-new (&rest option-list)
  "Create a skeleton of object containing only the $TYPE property in ~
   the current context. If object is a class creates an instance otherwise ~
   creates an orphan.
Arguments:
   option-list (opt):
    :ideal t ; we want to create an ideal instance (with sequence number 0)
Return:
   id of created object."
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    (if (%is-model? *self* context)
        ;; if object is a model, then we create an instance
        (%create-basic-new-object *self* context
                                  :ideal (cadr (assoc :ideal option-list)))
      ;; otherwise we create an orphan
      (%create-basic-orphan context))))

#|
(defconcept "person" (:att "name" (:entry))(:att "age")(:att "first name")
  (:rel "brother" (:to "person")))
_person
$E-PERSON

(send _person '=basic-new)
$E-PERSON.1
(($TYPE (0 $E-PERSON)) ($ID (0 $E-PERSON.1)))

$E-PERSON.CTR
(($TYPE (0 $CTR)) ($VALT (0 2)) ($CTRS.OF (0 $E-PERSON)) ($SVL.OF (0 $MOSSSYS)))

(with-package :test
  (with-context 5
    (send 'test::*none* '=basic-new)))
TEST::$ORPHAN.1
(($TYPE (5 TEST::*NONE*)) ($ID (5 TEST::$ORPHAN.1)))
|#
;;;------------------------------------------------------ =CHANGE-CLASS (UNIVERSAL)

(defmossuniversalmethod =change-class (new-class-ref &key include-inverse-links)
  "Takes an object and sets its class to new-class. Copies all properties and ~
   values, using generic properties to adapt properties to the class. Copies ~
   all values, which may result in constraint violations. The change is done ~
   by creating an instance of the new class and transferrring all data and ~
   relations to the new instance.
Argument;
   new-class-ref: must be the ref of an existing class
   include-inverse-links (key): relinks inverse links
Return:
   the id of the new object"
  (let ((new-class-id (%%get-id new-class-ref :class)))
    (unless new-class-id
      (verbose-throw :error 
                     "bad class reference ~S for changing class in package ~S."
                     new-class-ref *package*))
    (send *self* '=change-class-id new-class-id 
          :include-inverse-links include-inverse-links)))

#|
(with-package :test
  (with-context 5
    (defindividual "student" ("name" "Marcio")("teacher" test::_jpb)
      (:var test::_mf))))
$E-STUDENT.4
((MOSS::$TYPE (5 $E-STUDENT)) (MOSS::$ID (5 $E-STUDENT.4)) ($T-PERSON-NAME (5 "Marcio"))
 ($S-STUDENT-TEACHER (5 $E-PERSON.1)))

(with-package :test
  (with-context 5
    (send test::_mf '=change-class "person")))
Warning: Structural Property HAS-TEACHER does not belong to model of $E-PERSON.8 in package
         #<The TEST package> and context 5. But we link it anyway...
TEST::$E-PERSON.8
((MOSS::$TYPE (5 $E-PERSON)) (MOSS::$ID (5 $E-PERSON.8)) ($T-PERSON-NAME (5 "Marcio"))
 ($S-TEACHER (5 $E-PERSON.1)))
$E-STUDENT.4
((MOSS::$TYPE (5 $E-STUDENT)) (MOSS::$ID (5 $E-STUDENT.4)) ($T-PERSON-NAME (5))
 ($S-STUDENT-TEACHER (5)) (MOSS::$TMBT (5 T)))
(with-package :test
  (%alive? 'test::$E-STUDENT.4 5))
NIL
|#
;;;--------------------------------------------------- =CHANGE-CLASS-ID (UNIVERSAL)
;;; to check...

(defmossuniversalmethod =change-class-id (new-class &key include-inverse-links)
  "Takes an object and sets its class to new-class. Copies all properties and ~
   values, using generic properties to adapt properties to the class. Copies ~
   all values, which may result in constraint violation. The change is done ~
   by creating an instance of the new class and transferrring all data and ~
   relations to the new instance.
Argument;
   new-class: must be the id of an existing class
   include-inverse-links (key): relinks inverse links
Return:
   the id of the new object"
  ;; first check new class
  (unless (and (%is-class? new-class)
               (%alive? new-class (symbol-value (intern "*CONTEXT*"))))
    (error "=change-class /bad or dead class ~S in context ~S." 
           new-class (symbol-value (intern "*CONTEXT*"))))
  (let (new-id prop-list prop-name value-list neighbors inv-id)
    ;; create new instance of class`
    (setq new-id (send new-class '=basic-new))
    ;; get all properties in current context
    (setq prop-list (send *self* '=has-properties))
    ;; then copy values
    (dolist (prop-id prop-list)
      ;; get name
      (setq prop-name (car (send prop-id '=get-name)))
      ;; get value
      (setq value-list (send *self* '=get prop-name))
      (trformat "=change-class /prop-name: ~S, value-list: ~&~S" 
                prop-name value-list)
      ;; add to new object
      (cond
       ((%is-attribute? prop-id)
        (send new-id '=add-tp  prop-name value-list))
       ((%is-relation? prop-id)
        (send new-id '=add-sp prop-name value-list))
       (t (error "=change-class /unknown property type: ~S" prop-id))))
    ;; then get inverse properties if wanted
    (when include-inverse-links
      (setq prop-list (send *self* '=has-inverse-properties))
      ;; for each of then relink it
      (dolist (prop-id prop-list)
        ;; get neighbors
        (setq neighbors (send *self* '=get-id prop-id))
        ;; compute inverse property id
        (setq inv-id (%inverse-property-id prop-id))
        ;; ling to new object
        (dolist (obj-id neighbors)
          (send obj-id '=add-sp-id inv-id (list new-id)))))
    ;; then delete old object in current context
    (send *self* '=delete)
    ;; return new obj id
    new-id
    ))

;;;================================================================================
;;;                              =CHECK Methods
;;;================================================================================

;;;*------------------------------------------------------------ =CHECK (ATTRIBUTE)

(defmossinstmethod =check MOSS-ATTRIBUTE (value-list)
  "Check that the value list attached to the attribute in object obeys the ~
   various restrictions attached to the attribute.
   Prints eventual errors using mformat.
Arguments:
   value-list: list of object identifiers (presumably attached to the attribute)
Return:
   value-list if OK, nil otherwise."
  (multiple-value-bind (result error-list) (%validate-tp *self* value-list)
    (dolist (item error-list)
      (mformat (concatenate 'string "~%;warning: " item)))
    result))

#|
? $T-SELLER1-NAME
(($TYPE (0 $EPT)) ($ID (0 $T-SELLER1-NAME)) ($PNAM (0 (:EN "name")))
 ($ETLS.OF (0 $MOSSSYS)) ($IS-A (0 $T-NAME)) ($INV (0 $T-NAME.OF))
 ($PT.OF (0 $E-SELLER1)) ($ONEOF (0 "Joe" "Judith")) ($SEL (0 :EXISTS)) ($MIN 1)
 ($MAX 2) ($OPR (0 :DIFFERENT)))
? (send '$T-SELLER1-NAME '=check '("JOE" "Joe" 1))

;warning: too many values for attribute $T-SELLER1-NAME, max is 2.
;values: ("JOE" "Joe" 1)
NIL
|#
;;;*------------------------------------------------------------- =CHECK (RELATION)

(defmossinstmethod =check MOSS-RELATION (suc-list)
  "Check that the value list obeys the various restrictions attached to the relation.
   Prints eventual errors using mformat.
Arguments:
   suc-list: list of object identifiers (presumably attached to the relation)
Return:
   suc-list if OK, nil otherwise."
  (multiple-value-bind (result error-list) (%validate-sp *self* suc-list)
    (dolist (item error-list)
      (mformat (concatenate 'string "~%;warning: " item)))
    result))

#|
? $S-PERSON-BROTHER
(($TYPE (0 $EPS)) ($ID (0 $S-PERSON-BROTHER)) ($PNAM (0 (:EN "Brother")))
 ($ESLS.OF (0 $MOSSSYS)) ($IS-A (0 $S-BROTHER)) ($INV (0 $S-BROTHER.OF))
 ($PS.OF (0 $E-PERSON)) ($SUC (0 $E-PERSON))
 ($ONESUCOF (0 $E-TEACHER.1 $E-TEACHER.3)) ($SEL (0 :FORALL)) ($MIN (0 1))
 ($MAX (0 2)) ($OPR (0 :DIFFERENT)))
? (send '$S-PERSON-BROTHER '=check (list _dbb _dbb _dbb))

;warning: too many values for relation $S-PERSON-BROTHER, max is 2 values:
values: ($E-TEACHER.3 $E-TEACHER.3 $E-TEACHER.3)
;warning: values for relation $S-PERSON-BROTHER should be all different 
values: ($E-TEACHER.3 $E-TEACHER.3 $E-TEACHER.3)
NIL
|#
;;;------------------------------------------------------------- =CHECK (UNIVERSAL)
;;; untested...
#|
(defuniversalmethod =check ()
  "Checks an instance for integrity"
  (prog ((entity-l (symbol-value *self*))
         (type-list (h=> '$TYPE)) ; may have several types
         (prop-list (=> *self* '=get-properties)) ; does not contain $TYPE, $ID
         prop-id values)
    (unless (%pdm? *self*)
      (mformat "~%;warning: Object ~S is not a PDL object." *self*)
      (return nil))
    (mformat "~&Checking: ~S, as an individual of concept(s): ~S" *self* type-list)
    (send *self* '=print-self)
    ;; check first all entry point and links
    (dolist (pair entity-l)
      (setq prop-id (car pair)
            values (send *self* '=get-id prop-id))
      (cond
       ;; no check on type
       ((eql prop-id '$TYPE))
       ;; id should be the same as identifier
       ((eql prop-id '$ID)
        (unless (eql (car values) *self*)
          (mformat "~%;warning: Recorded id: ~S is not the object id: ~S" 
                   (car values) *self*)))
       ((%is-terminal-property? prop-id)
        ;;***** should check for entry points
        (send prop-id '=check values))
       ((%is-structural-property? prop-id)
        ;; check that neighbors have the right inverse link
        (dolist (suc-id values)
          (unless (member *self* 
                          (send suc-id '=get-id (%inverse-property-id prop-id)))
            ;; complain
            (mformat "~%;warning: Missing link in ~S for property ~S onto ~S"
                     suc-id (%inverse-property-id prop-id) *self*)))
        ;; more complex checks
        (send prop-id '=check values)
        )
       ((%is-inverse-property? prop-id)
        ;;***** should check for missing direct links
        )
       ;; tell user when not a property
       (t (format t "~%;warning: ~S, not a property" prop-id)))
      )
    ;; check now possible ref object in case of multiclass membership
    (return :done)))


;---
(%defmethod 
 =check STRUCTURAL-PROPERTY (entity-id)
 "Checks if inverse links are there - if not print error
		Arg1: entity-id"
 (let ((successor-list (send entity-id '=get-id *self*))
       (inverse-id (send *self* '=inverse-id))
       )
   (while successor-list
     ;; try each successor in turn
     (if (lob-typep (car successor-list))
       ;; if of PDM type then check inverse link
       (progn
         (send (car successor-list) '=get-id inverse-id)
         (if (not (memq entity-id *answer*))
           (format t "~&Missing link in ~S for property ~S onto ~S" 
                   (car successor-list) inverse-id entity-id)
           ) 
         )
       ;; else tell user we have a bad entity
       (format t "~&Entity  ~S associated with ~S is not a PDM object" 
               (car successor-list) *self*)         
       )
     (pop successor-list))	   ;; return link was built with unknown prop
   "*done*" ) )

;---
(%defmethod 
 =check TERMINAL-PROPERTY (entity-id)
 "Check consistensy of terminal prop
		Arg1: entity-id - In case of entry point looks for 
		backpointer - Tell user if missing"
 (when (get-method *self* '=make-entry)
   (let ((data (send entity-id '=get-id *self*))
         entry)
     ;; check now for each piece of data in turn
     (while data
       (setq entry (send *self* '=make-entry (pop data)))
       (when entry
         ;-we assume it is in core 
         ;			(send *lob* '=load-if entry) ; load entry
         ;+
         ;; must be a lob object ***
         (if (or  (not (lob-typep entry))
                  (not(memq entity-id
                            (send entry 
                                  '=has-value-id
                                  (send *self* '=inverse-id )))))
           (format t "~&Missing entry-point ~S for ~S associated with ~S" 
                   entry entity-id (send *self* '=get-name))
           )))
     "*done*") ))
|#
;;;*------------------------------------ =CHECK-CARDINALITY-CONSTRAINTS (UNIVERSAL)

;;; Here we got a method for checking cardinality constraints of any given
;;; object. It first gets the property list of all possible properties it
;;; can have, including inherited ones, then for each prop in turn checks
;;; for minimal and maximal cardinality. It returns a list of properties
;;; that failed the test in the object
;;; *** Note: if we introduce *none* as a value for type, this should
;;; be modified.

(defmossuniversalmethod =check-cardinality-constraints ()
  "Checks if current entity has the required properties ~
   as stated in its model.
Arguments:
   none
Return:
   nil if OK, otherwise the list of faulty properties."
  (let (prop-list lres min max)
    (setq prop-list (send *self* '=get-properties))
    ;; check $MINT and $MAXT constraints
    (while prop-list
      (if
        (or 
         (and (setq min (car (=> (car prop-list) '=get-id '$MINT)))
              (numberp min)
              ;; must do that to take context into account
              (< (length (g=> (car prop-list))) min)
              )
         (and (setq max (car (=> (car prop-list) '=get-id '$MAXT)))
              (numberp max)
              (> (length (g=> (car prop-list))) max)
              )
         )
        (push (car prop-list) lres)
        )
      (pop prop-list))
    lres))

;;;----------------------------------------- =CHECK-REQUIRED-PROPERTIES (UNIVERSAL)
;;; duplicates the =CHECK-CARDINALITY-CONSTRAINTS 
#|
(defuniversalmethod =check-required-properties ()
  "Check if current entity has required properties ~
   as stated in its model.
Arguments:
   nil
Return:
   list of missing properties."
  (let ((prop-list (send *self* '=get-properties))
        lres
        )
    (dolist (prop-id prop-list)
      (and (=> prop-id '=get-id '$MINT)
           (> (car *answer*) (length (g=> prop-id)))
           (push prop-id lres)))
    (remove-duplicates lres)
    ))
|#
;;;========================= End =check methods ===================================

;;;------------------------------------------------------------- =CLONE (UNIVERSAL)

(defmossuniversalmethod =clone (&key export)
  "Clones any object copying values corresponding to the current context.~
   Shallow copies an object, giving the same type to the copy, limited to the ~
   current context.
   Thus it is not possible to clone an object importing it from another ~
   context. Use for that =clone-from-context. ~
   When the object belongs to multiple classes, then something should be done ~
   Currently the first class is used.
Arguments:
   export (key): if t exports newly created entry points
Return:
   the cloned object."
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (property-list (%%has-properties+ *self* context))
         (new-object-id 
          (%create-basic-new-object (car (%get-value *self* '$TYPE)) context))
        value-list
        entry)
    ;; Initialize new key to nil, wiping out the $TYPE field
    (set new-object-id nil)
    ;; Now look at each prop in turn
    ;; ***** if $type and multivalued should do domething. See %make-instance
    (dolist (prop-id property-list)
      ;; get the value list from old object
      (setq value-list (g=> prop-id))
      ;; copy into the new object
      (cond
       ;; if id we should put the identifier of the new object
       ((eql prop-id '$ID)
        (%%set-value-list new-object-id (list new-object-id) prop-id context))
       ;; no value quit
       ((null value-list)
        (%%set-value-list new-object-id value-list prop-id context))
       ;; attribute with entry point
       ((and (%is-attribute? prop-id)
             (get-method prop-id '=make-entry))
        (%%set-value-list new-object-id value-list prop-id context)
        ;; make new entry-point for corresponding property
        (while value-list ; watch, it kills value list
          (setq entry (=> prop-id '=make-entry (pop value-list)))
          ;; %make-ep handles lists
          (%make-ep entry prop-id new-object-id :export export))
          )
       ((%is-attribute? prop-id)
        ;; attribute but no entry point
        (%%set-value-list new-object-id value-list prop-id context))
       ((%is-relation? prop-id)
        ;; relation
        (=> new-object-id '=add-values prop-id value-list))
       ))
    ;; cloned classes are recorded at system level
    (when (%is-model? new-object-id)
      (send '$ENSL '=link *moss-system* new-object-id)
      )
    ;; return new object-id
    new-object-id
    )) 

;;;------------------------------------------------ =CLONE-FROM-CONTEXT (UNIVERSAL)
;;; Cloning from another context  ***** untested
;;; There is a problem for properties that exist in the previous context and
;;; do not exist in the current (importing) context.

#|
(defmossuniversalmethod =clone-from-context (context &key export)
  "Clone any object importing it from another context. Unless a property ~
   does not exist in the current context (warning message), the property and ~
   values are copied into the new object."
  (let ((property-list (with-context context (=> *self* '=get-properties)))
        (new-object-id (=> (car (%get-value *self* '$TYPE :context context)) '=new))
        prop-id
        value-list
        )
    ;; Now look at each prop in turn
    (while property-list
      (setq prop-id (pop property-list))
      (if (not (or (%is-terminal-property? prop-id)
                   (%is-structural-property? prop-id)))
        (warn "while cloning object ~S, the property ~S does not exist in ~
               current context" *self* prop-id)
        (progn
          ;; very low level, copy value into new object
          (setq value-list (%get-value *self* prop-id :context context))
          (when value-list (%%set-value-list new-object-id value-list
                                             prop-id context))
          (when (and (%is-terminal-property? prop-id :context context)
                     (get-method prop-id '=make-entry :context context)
                     )
            ;; make new entry-point for corresponding property
            (%make-ep 
             (=> prop-id '=make-entry 
                 (with-context context (=> *self* '=get-id prop-id)))
             (%inverse-property-id prop-id :context context)
             new-object-id
             :context context
             :export export)  ; not sure this is OK?
            )
          ) ; end of second if clause, i.e. property exists
        ) ; end of if
      )
    ;; cloned classes are recorded at system level
    (when (%is-model? new-object-id :context context)
      (%link *moss-system* '$ENSL new-object-id)
      )
    ;; return new object-id
    new-object-id
    ))
|#

;;;================================================================================
;;;                              =DELETE Methods
;;;================================================================================

;;;------------------------------------------------------------ =DELETE (ATTRIBUTE)
;;; This method should send a warning if we fall under the minimum cardinality

(defmossinstmethod =delete MOSS-TERMINAL-PROPERTY (entity-id value &rest option-list)
  "Delete a single value in a given object. The value is normalized using the =xi ~
   method if any has been defined. Eventual entry points are removed.
Arguments:
   entity-id: id of object
   value: value to delete
   option-list (opt):
        (:nth position) the position of the value is given in case there are ~
   several identical values for the attribute
Return:
   internal representation of object."
  (let* ((context (symbol-value (intern "*CONTEXT*"))))
    (delete-values entity-id *self* (list value) :context context)))

;;;*-------------------------------------------------------- =DELETE (INVERSE-LINK)
;;; should activate demon on the direct property....

(defmossinstmethod =delete MOSS-INVERSE-LINK (entity-id pred-id)
  "Delete a link between a given object and a predecessor.
Arguments:
   entity-id: id of object
   pred-id: id of predecessor
Return:
   internal representation of object."
  (progn
    (send *self* '=unlink entity-id pred-id)
    (symbol-value entity-id)))

;;;------------------------------------------------------------- =DELETE (RELATION)
;;; This method should send a warning if we fall under the minimum cardinality

(defmossinstmethod =delete MOSS-STRUCTURAL-PROPERTY (entity-id suc-id)
  "Delete a link between a given object and a successor
Arguments:
   entity-id: refers to first object
   successor-id: refers to the successor of the first object
Returns:
   the internal representation of the first modified object."
  (delete-values entity-id *self* (list suc-id)))

;;;------------------------------------------------------------ =DELETE (UNIVERSAL)
;;; never called for properties, since they have a specific =DELETE method

(defmossuniversalmethod =delete  ()
  "Deletes an entity by removing all values from current context and putting ~
   a tombstone. All other objects pointing to such an entity are updated.
Arguments:
   nil
Return:
   internal representation of the object."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (entity-l (symbol-value *self*)))
    (while entity-l
           (cond
            ((%is-terminal-property? (caar entity-l) context)
             ;; we do not erase type nor id
             (if (not (member (caar entity-l) '($TYPE $ID)))
                 (send (caar entity-l) '=delete-all *self*)))
            ((%is-structural-property? (caar entity-l) context)
             (send (caar entity-l) '=delete-all *self*))
            ;; do not do anything on inverse terminal properties
            ((%is-inverse-property? (caar entity-l) context)
             (send (caar entity-l) '=delete-all *self*))
            ;; tell user when not a property
            (t (error "Bad entity internal format ~A in package ~S and context ~S"
                 *self* (package-name *package*) context)))
           (pop entity-l))
    ;; ***we must add a tombstone somewhere here to indicate that the object does
    ;; not exist in this context any longer
    (%%set-value *self* t '$TMBT context)
    (symbol-value *self*)
    ))

;;;-------------------------------------------------------- =DELETE-ALL (ATTRIBUTE)

(defmossinstmethod =delete-all MOSS-ATTRIBUTE (entity-id)
  "Delete current values associated with the property of object. The result is ~
   a null value associated with the property.
Arguments: 
   entity-id: id of object
Return:
   internal representation of object."
  (delete-values entity-id *self* :ALL))

;;;----------------------------------------------------- =DELETE-ALL (INVERSE-LINK)

(defmossinstmethod =delete-all MOSS-INVERSE-LINK (entity-id)
  "Delete all existing links without recording that i.e. used ~
   when deleting complete entity.
Arguments:
   entity-id: id of object
Return:
   internal representation of object."
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (candidate-list (%get-value entity-id *self* context))
         (inv-id (%inverse-property-id *self* context))
         old-entity-id
         old-suc-id)
    (dolist (suc-id candidate-list)
      (setq old-entity-id entity-id old-suc-id suc-id)
      (send *self* '=unlink entity-id suc-id)
      ;; for each object do eventual bookkeeping
      ;; inv-id maybe an attribute...
      (cond
       ((%is-attribute? inv-id context)
        ;; e.g. ($ENAM.OF (0 $FN)) inv-id=$ENAM suc-id=$FN *self*=$ENAM.OF
        ;; entity-id=METHOD
        ;; (send $ENAM =if-removed METHOD $FN)
        (send inv-id '=if-removed entity-id suc-id))
       ((%is-relation? inv-id context)
        (send inv-id '=if-removed old-entity-id old-suc-id entity-id suc-id))
       (t
        (terror "something wrong while deleting inverse property ~S of object ~Sin ~
                 package ~S and context ~S"
                *self* entity-id (package-name *package*) context)))
      )
    (symbol-value entity-id)))

;;;--------------------------------------------------------- =DELETE-ALL (RELATION)

(defmossinstmethod =delete-all MOSS-RELATION (entity-id)
  "Delete all existing links without recording that i.e. used ~
   when deleting complete relation.
Arguments: 
   entity-id: id of object
Return:
   internal representation of object."
  (let* ((context (symbol-value (intern "*CONTEXT*"))) ; not needed
         (value-list (%get-value entity-id *self* context))) ; use :ALL instead
    (delete-values entity-id *self* value-list :context context)))

;;; could be: (delete-values entity-id *self* :all)
#|
should test
|#
;;;--------------------------------------------- =DELETE-ALL-SUCCESSORS (UNIVERSAL)

(defmossuniversalmethod =delete-all-successors (rel-ref)
  "Deletes all successors to a given entity. Issues a warning when the number of ~
   related objects falls below the min cardinality level.
Arguments:
   rel-ref: symbol or string or (multilingual) name
Return:
   not significant."
  (let* ((class-id (car (%type-of *self*)))
         (rel-id (%%get-id rel-ref :relation :class-ref class-id))
         ;(value-list (%get-value *self* rel-id))
         )
    (delete-values *self* rel-id :all)))

#|
should test a case with a minimal constraint
|#
;;;-------------------------------------------------- =DELETE-ATTRIBUTE (UNIVERSAL)
;;; deprecated, use =delete-values with arg :all

(defmossuniversalmethod =delete-attribute (att-ref)
  "Deprecated. Use =delete-values with value-list arg :all"
  ;; class-id is the first class of the object in case of multiple class belonging
  (delete-values *self* att-ref :all))

;;;------------------------------------------- =DELETE-ATTRIBUTE-VALUES (UNIVERSAL)

(defmossuniversalmethod =delete-attribute-values (att-ref value-list)
  "Deprecated, use =delete-values."
  (delete-values *self* att-ref value-list))


;;;-------------------------------------------- =DELETE-RELATED-OBJECTS (UNIVERSAL)
;;; method =delete-related-objects applies to all objects
;;; deprecated, use delete-values

(defmossuniversalmethod =delete-related-objects (rel-ref successor-list)
  "Deprecated, use =delete-values."
  (delete-values *self* rel-ref successor-list))



;;;--------------------------------------------------------- =DELETE-SP (UNIVERSAL)
;;; method =delete-sp applies to all objects

(defmossuniversalmethod =delete-sp (sp-ref successor-list)
  "Deprecated. Use =delete-values."
  (delete-values *self* sp-ref successor-list))

;;;--------------------------------------------------------- =DELETE-TP (UNIVERSAL)
;;; only used by COLOMBO-ONTOLOGY...

(defmossuniversalmethod =delete-tp (tp-ref value-list)
  "Deprecated. Use =delete-values."
  (delete-values *self* tp-ref value-list))

;;;----------------------------------------------------- =DELETE-VALUES (UNIVERSAL)
;;; option nth has not been kept. One can get the same result by extracting the
;;; link at position n, and deleting it.

(defmossuniversalmethod =delete-values (prop-ref value-list &rest option-list)
  "associated to any object deletes values (attribute values or successors).
Arguments:
   prop-ref: id, symbol, string, or mln representing the property
   value-list: list of values to be normed (attributes) or resolved (relations)
   option-list (rest):
     (:no-warning t/nil) - it true no warnings are printed
     (:scope :all/:some) - for mln values, deletes the mln or only pieces of it
     (:language tag) - for mln values, when value-list is a list of strings
     (:already-normed t/nil) - if values to delete have been already normed 
        (attributes) or resolved (relations)
     (:count nn) - number of duplicated values to remove
Return:
   the internal list of the modified object."
  (delete-values *self* prop-ref value-list
                 :no-warning (car (getv :no-warning option-list))
                 :scope (car (getv :scope option-list))
                 :language (car (getv :language option-list))
                 :already-normed (car (getv :already-normed option-list))
                 :count (car (getv :count option-list))
                 ))

;;;=========================== END =delete Methods ================================


;;;----------------------------------------------------------------- =EDIT (METHOD)

#|
(defmossinstmethod =edit MOSS-METHOD ()
  "Edition du code d un objet fonction"
  ;	(prog(body)
  ;		(setq body (send *self* '=get 'HAS-CODE))
  ;		(setq body (edlist body))
  ;		(send *self* '=set 'HAS-CODE body) ))
  (error "=edit method not implemented yet..."))
|#

;;;------------------------------------------------------------- =ERASE (ATTRIBUTE)

(defmossuniversalmethod =erase (prop-id)
  "deletes all values for this property, then erases the attribute from the ~
   current version. The result will be that values will come from previous versions.
   Dangerous!"
  (let* ((obj-l (symbol-value *self*))
         (val (cdr (assoc prop-id obj-l))))
    ;; delete all properties in the current context
    (send prop-id '=delete-all *self*)
    ;; then remove the property from the object
    ;; remove current context from the values
    (setq val (remove *context* val :key #'car))
    ;; remove property from object
    (setq obj-l (remove prop-id obj-l :key #'car))
    ;; if any other context there must key the property otherwise, remove it
    (if val (push (cons prop-id val) obj-l))
    (set *self* obj-l)
    obj-l))
      
#|
(defindividual "person" ("age" 32))
$E-PERSON.22
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.22)) ($T-PERSON-AGE (0 32)))

(send '$e-person.22 '=erase '$T-PERSON-AGE)
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.22)))

;;; now with versions
$E-PERSON.22
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.22)) 
 ($T-PERSON-AGE (0 32) (1 34)))
(send '$e-person.22 '=erase '$T-PERSON-AGE)
(($T-PERSON-AGE (1 34)) (MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.22)))
|#      
;;;------------------------------------------------------------- =FILTER (RELATION)

(defmossinstmethod =filter MOSS-RELATION (suc-id entity-id)
  "Can be defined so as to filter successor of a given entity.
   Default action is Don't filter.
Arguments:
   suc-id: id of successor
   entity-id: id of entity
Return:
   suc-id if filtering predicate is satisfied, nil otherwise."
  ;; progn to avoid message from compiler saying that
  ;; entity-id is unused. returns suc-id
  (progn entity-id suc-id))

;;;------------------------------------------------------------ =FIND (MOSS-SYSTEM)
;;; retreives all possible objects with a given entry point. More efficient than
;;; access

(defmossinstmethod =find MOSS-SYSTEM (entry &optional prop-ref concept-ref)
  "extracts entities from KB knowing entry point prop name and concept name.
Arguments:
   entry: entry-point, symbol, string or mln
   prop-ref: property, e.g. HAS-NAME, \"Name\" or (:en \"name\" ...)
   concept-ref (opt): concept ref, e.g. PERSON, \"person\" or mln
Return:
   a list of objects corresponding to entry"
  (let ((pair-list (%%get-objects-from-entry-point entry))
        ep-list inv-prop-list)
    (format t "~%; =find /pair-list: ~S" pair-list)
    (cond
     ;; nothing return nil
     ((null entry) nil)
     
     ;; if we have no prop-ref nor concept-ref, return everything
     ((and (null prop-ref)(null concept-ref))
      ;; compute all possible entry points in the current package
      (setq ep-list (%%get-id entry :ep))
      (format t "~%; =find /ep-list: ~S" ep-list)
      (mapcar #'cdr 
        (reduce #'append  ; jpb 140820 removing mapcan
                (mapcar #'%%get-objects-from-entry-point ep-list))))
     
     ;; if property but no class name then filter results
     ((null concept-ref)
      ;; compute all possible entry points in the current package
      (setq ep-list (%%get-id entry :ep))
      ;; get a list of possible inverse properties
      (setq inv-prop-list  (catch :error (%%get-id prop-ref :inv))) 
      ;; if it is a string then error
      (if (stringp inv-prop-list) (throw :return nil))
      
      ;; test that this is the property name of the property of each pair
      (mapcar #'(lambda (xx) 
                  (if (member (car xx) inv-prop-list) (cdr xx)))
              pair-list))
     
     ;; if class-ref is there, then we can use extract
     (t
      (catch :error
        (%extract entry prop-ref concept-ref)))
     )))

#|
(catch :error
       (with-package :test
         (with-context 3
           (send test::*moss-system* '=find 'test::barthès))))
($E-PERSON.1 $E-PERSON.2 $E-PERSON.4)

(catch :error
       (with-package :test
         (with-context 3
           (access "barthès"))))
(TEST::$E-PERSON.1 TEST::$E-PERSON.2 TEST::$E-PERSON.4)

(catch :error
       (with-package :test
         (with-context 3
           (send test::*moss-system* '=find 'test::barthès "name"))))
(TEST::$E-PERSON.1 TEST::$E-PERSON.2 TEST::$E-PERSON.4)

(catch :error
       (with-package :test
         (with-context 3
           (access '(*any* ("name" :is "barthès"))))))
Error in clause format (bad syntax): ("property (HAS-NAME) not unique in package 
     #<The TEST package> context 3" :IS "barthès")

(catch :error
       (with-package :test
         (with-context 5
           (send test::*moss-system* '=find 'test::barthès "name" "student"))))
TEST::$E-STUDENT.2

(catch :error
       (with-package :test
         (with-context 5
           (access '("student" ("name" :is "barthès"))))))
(TEST::$E-STUDENT.2)
|#
;;;------------------------------------------------------ =FORMAT-TRACE (UNIVERSAL)

(defmossuniversalmethod =format-trace (stream format-string &rest arg-list)
  "builds a string for a trace of calls in between objects as a message number with ~
   eventually a mark followed by the message itself.
Arguments:
   stream: stream or pane
   format-string: format string
   arg-list (rest): list of arguments to the format string
Return:
   nil"
  (let ((*moss-output* stream))
    (if (stringp format-string)
      ;; if format string is given apply it directly
      (mformat "~&~VT~S ~?" *trace-level* *trace-level* format-string arg-list)
      ;; otherwise call print-list
      (mformat "~&~VT~S ~S" *trace-level* *trace-level* arg-list)
      )
    ;; return nil
    nil))

#|
(send '$ENT '=format-trace t "Bonjour... ~S" "Albert")
 0 Bonjour... "Albert"
NIL
|#
;;;------------------------------------------------------ =FORMAT-VALUE (ATTRIBUTE)
;;; insensitive to versions

(defmossinstmethod =format-value MOSS-ATTRIBUTE (value)
  "Format single value associated to property. A property ~
   can use such a function for a special formatting.
   when value is a multilingual name, we extract national canonical part ~
   otherwise we return the value unchanged.
Argument:
   value: value to format
Return:
   value reformatted if MLN, value otherwise."
  ;; when value is a multilingual name, we extract national part
  (if (or (mln::mln? value)
          (mln::%mln? value) ; JPB 1411 (allow old format)
          )
    (or (mln::get-canonical-name value) "-")
    value))

#|
(let ((*language* :all))
  (send '$enam '=format-value (car (has-moss-concept-name '$ENT))))
"MOSS-CONCEPT"

(with-package :test
  (with-context 0
    (send 'moss::$enam '=format-value 
          (car (has-moss-concept-name 'test::$E-person)))))
"person"

? (with-language :fr
    (send 'moss::$enam '=format-value (car (has-concept-name '$E-person))))
"personne"
|#
;;;------------------------------------------------------- =FORMAT-VALUE (RELATION)
;;; this function is intended to be superseded by a user-defined function when
;;; needed for special formatting

(defmossinstmethod =format-value MOSS-RELATION (value &key prop-ref class-ref)
  "formats single value associated to property. A property ~
   can use such a function for a special formatting. Default is =summary.
Argument:
   value: value to format
Return:
   value reformatted if MLN, value otherwise."
  (declare (ignore prop-ref class-ref))
  ;; default is to use =summary
  (send value '=summary))


;;;------------------------------------------------- =FORMAT-VALUE-LIST (ATTRIBUTE)

(defmossinstmethod =format-value-list MOSS-ATTRIBUTE (value-list &key header)
  "Format a list of values, presumably attached to a property. We limit the length ~
   of the string to 80 characters, to avoid problems with Pascal strings.
Argunments:
   value-list: value associated with the property (list)
   header: a string replacing the name of the property
Return:
   a string less than 80 chars (because of the Pascal interface...)
"
  (rformat 80 nil "~A:     ~{~A~^, ~}"
           (or header (car (send *self* '=get-name)))
           (mapcar #'(lambda (xx) (=> *self* '=format-value xx))
                   value-list)))
#|
(with-package :test
  (with-context 0
    (send 'test::$t-person-name '=format-value-list '("Barthès" "Barthès-Biesel"))))
?"name:     Barthès, Barthès-Biesel"
|#
;;;---------------------------------------------- =FORMAT-VALUE-LIST (INVERSE LINK)

(defmossinstmethod =format-value-list MOSS-INVERSE-LINK (suc-list &key header)
  "Produces a string containing summaries of successors limited to 60 characters. ~
   This is due to the silly Pascal restriction on Mac OS 8.
Arguments:
   suc-list: successors
Return:
   string."   
  (rformat 60 nil "~A:     ~{~<~%     ~1:;~{~A~^ ~}~>~^, ~}"
           (or header (car (send *self* '=get-name)))
           (mapcar #'(lambda (xx) (=> xx '=summary))
             suc-list)))

#|
Should be tested
|#
;;;-------------------------------------------------- =FORMAT-VALUE-LIST (RELATION)

(defmossinstmethod =format-value-list MOSS-RELATION (suc-list &key header)
  "Produces a string containing summaries of successors limited to 60 characters. ~
   This is due to the silly Pascal restriction on Mac OS 8.
Arguments:
   suc-list: successors
Return:
   string."   
  (rformat 60 nil "~A:     ~{~<~%     ~1:;~{~A~^ ~}~>~^, ~}"
           (or header (car (send *self* '=get-name)))
           (mapcar #'(lambda (xx) (=> xx '=summary))
                   suc-list)))

#|
(with-package :test
  (with-context 3
    (send '$PS '=format-value-list '(test::$S-PERSON-BROTHER))))
"MOSS-RELATION:     brother/PERSON"
|#
;;;---------------------------------------------------- =FORMAT-WARNING (UNIVERSAL)
;;; ?

(defmossuniversalmethod =format-warning (stream msg &rest arg-list)
  "Default method for printing warnings.
Arguments:
   msg: if a string considers it as a format string
   stream: stream or pane"
  (let ((*moss-output* stream))
    (if (stringp msg)
      (mformat "~%;***Warning: ~?" msg arg-list)
      (mformat "~%;***Warning:~{ ~A~}" msg))
    ))

#|
(send `$ENT '=format-warning t "Qui est l?? ~A ?" "Albert")
;***Warning: Qui est l?? Albert ?
NIL
|#
;;;================================================================================
;;;                               GET Methods
;;;================================================================================

;;;--------------------------------------------------------------- =GET (UNIVERSAL)
;;; When requesting a value from a property we send a message like
;;; 	(send '$PERS.12 '=get 'HAS-NAME)
;;; HAS-NAME may be the entry-point of several terminal properties (e.g.
;;; from different systems). The function %get-property-id-from-name
;;; resolves the ambiguity. We then use the =get-id method.
;;; If we cannot find the property (and list of values) locally, then
;;; we try to inherit a value directly from ancestors at the same level;
;;; otherwise we return a default value if any.

(defmossuniversalmethod =get (prop-ref &key no-warning)
  "Get value-list from entity, given property name -
    E.g. (send '$PERS.12 '=get 'HAS-NAME) or
         (send '$PERS.12 '=get \"name\") in current context.
   Tries first a local property, then a generic property, then any property with ~
   the same name. If still no value can be obtained, uses =if-needed demon.
Arguments:
   prop-ref: symbol (id or name) or string or multilingual name
   no-warning (key): if t, does not issue warning
Return:
   list of values associated to the property."
  
  ;; check first the case of inverse properties JPB 0710
  (let ((context (symbol-value (intern "*CONTEXT*")))
        prop-id-list)
    (if (setq prop-id-list (%is-inverse-property-ref? prop-ref context))
        ;; when non null, is a list of possible inverse properties
        ;; apply them all and remove duplicate objects
        (delete-duplicates
         (reduce #'append
                 (mapcar #'(lambda (xx) (%get-value *self* xx context))
                   prop-id-list)))
      
      ;; otherwise regular property, do as usual...
      (let* ((prop-name (%%make-name prop-ref :property))
             ;; try to get the right property applying to *self*
             (prop-id (%get-property-id-from-name *self* prop-name context))
             ;; otherwise try to get a generic property
             (gen-id (%%get-id prop-ref :prop :include-moss t))
             ;; otherwise get anything that is recorded with this PNAM
             (prop-list (%get-value prop-name (%inverse-property-id '$PNAM context)
                                    context))
             )        
        ;(format t "~%; =get /prop-name: ~S" prop-name)
        ;(format t "~%; =get /prop-id: ~S" prop-id)
        ;(format t "~%; =get /gen-id: ~S" gen-id)
        ;(format t "~%; =get /prop-list: ~S" prop-list)
        
        ;; try to get property from the different cases
        (cond
         ;;=== if prop there OK local property
         (prop-id
          ;; use more elementary function
          (send *self* '=get-id prop-id))
         
         ;;=== try generic property (for orphans?) works also when prop-ref is id!
         (gen-id
          (send *self* '=get-id gen-id))
         
         ;;=== no local prop, nor generic try other possible properties
         ;; other properties should appear in the generic list
         ;; takes the list of properties that have the same name but are not 
         ;; associated with the classes of the instance. Maybe should be return
         ;; the first value associated with one of the other classes
         ;; ********** the semantic of this clause is unclear...
         ((reduce #'append  ; JPB 140820 removed mapcan
                  (mapcar #'(lambda (xx) (%get-value *self* xx context)) 
                    prop-list)))
         
         ;;=== did not work quit
         ((null no-warning)
          (warn "in =get ~A is not a property of object ~A in package ~S and ~
                 context ~A"
            prop-name *self* (package-name *package*) context)
          nil))
        
        ))))
  
#|
(send '$ENT '=get 'has-moss-concept-name)
(((:EN "MOSS-CONCEPT" "MOSS-ENTITY" "MOSS-CLASS")))
0

MOSS classes have only English names
(send '$ENT '=get " nom du concept")
Warning: in =get HAS-NOM-DU-CONCEPT is not a property of object $ENT in package
         "MOSS" and context 0
NIL

(send '$ENT '=get '(:en "MOSS concept name" :fr" nom du concept"))
(((:EN "MOSS-CONCEPT" "MOSS-ENTITY" "MOSS-CLASS")))

(send '$ENT '=get '((:en "MOSS concept name") (:fr " nom du concept")))
(((:EN "MOSS-CONCEPT" "MOSS-ENTITY" "MOSS-CLASS")))
0

(with-package :test
  (with-context 0
    (send test::_dbb '=get "name")))
("Barthès-Biesel" "Barthès")

(with-package :test
  (with-context 3
    (send test::_cb '=get "name")))
Warning: object with id: $E-PERSON.3 is dead in this context (3) and package (#<The TEST package>)
         when trying to apply method: 
=GET, with arg-list: ("name")
NIL

(with-package :test
  (with-context 5
    (send test::_cb '=get "name")))
("Barthès")

(with-package :test
  (with-context 0
    (send test::_jpb '=add-related-objects "wife" '(test::$E-PERSON.2))
    (send test::_jpb '=get "wife")))
(TEST::$E-PERSON.2)
|#
;;;-------------------------------------------------------------- =GET+ (UNIVERSAL)

(defmossuniversalmethod =get+ (prop-ref &key no-warning)
  "Gets value-list from entity, given property name - When the value is an MLN then ~
   returns the value associated with the current *language*; when the value is not ~
   an MLN, returns it.
Arguments:
   prop-ref: symbol (id or name) or string or multilingual name
   no-warning (key): if t, does not issue warning
Return:
   list of values associated to the property
   version in which the values has been last modified or NIL if it doest not exist"
  (multiple-value-bind (val-list no-value-flag)
      (send *self* '=get prop-ref :no-warning no-warning)
    ;; if the returned value is an MLN, then get the value for *language*
    (if (mln::mln? (car val-list))
        (setq val-list (mln::extract (car val-list))))
    (values val-list no-value-flag)))

#|
(send '$e-country.1 '=get "name")
(((:FR "Angleterre") (:EN "England")))
0

*language*
:EN

(send '$e-country.1 '=get+ "name")
("England")
0

(let ((*language* :pt)) (send '$E-COUNTRY.1 '=get+ "name"))
NIL
0
|#
;;;------------------------------------------------------------- =GET++ (UNIVERSAL)

(defmossuniversalmethod =get++ (prop-ref &key no-warning)
  "Gets value-list from entity, given property name - When the value is an MLN then ~
   returns the value associated with the current *language* if possible, otherwise ~
   with :EN, otherwise with a random language; when the value is not an MLN, ~
   returns it.
Arguments:
   prop-ref: symbol (id or name) or string or multilingual name
   no-warning (key): if t, does not issue warning
Return:
   list of values associated to the property
   version in which the values has been last modified or NIL if it doest not exist"
  (multiple-value-bind (val-list no-value-flag)
      (send *self* '=get prop-ref :no-warning no-warning)
    ;; if the returned value is an MLN, then get the value for *language*
    (if (mln::mln? (car val-list))
        (setq val-list (mln::extract (car val-list) :always t)))
    (values val-list no-value-flag)))

#|
(send '$e-country.1 '=get "name")
(((:FR "Angleterre") (:EN "England")))
0

*language*
:EN

(send '$e-country.1 '=get++ "name")
("England")
0

(let ((*language* :pt)) (send '$E-COUNTRY.1 '=get++ "name"))
("England")
0
|#
;;;------------------------------------------------- =GET-ALL-OBJECTS (MOSS-SYSTEM)
;;; ambiguous method when applied to an application: do we want also moss objects?
#|
(defmossinstmethod =get-all-objects MOSS-SYSTEM ()
  "get all objects present in the system and makes a list of them.
Arguments:
    nil
Return:
   list of objects."
  (let (object-list)
    ;; attributes
    (setq object-list (append object-list (send '$EPT '=get-instances)))
    ;; relations
    (setq object-list (append object-list (send '$EPS '=get-instances)))
    ;; inverse links
    (setq object-list (append object-list (send '$EIL '=get-instances)))
    ;; methods
    (setq object-list (append object-list (send '$FN '=get-instances)))
    ;; counters
    ;(setq object-list (append object-list (send '$CTR '=get-instances)))
    ;; entry-points
    (setq object-list (append object-list (send '$EP '=get-instances)))
    ;; system variables
    (setq object-list (append object-list (%get-value *moss-system* '$SVL)))
    ;; classes
    (setq object-list (append object-list (%get-value *moss-system* '$ENLS)))
    ;;instances of user classes
    (dolist (class-id (set-difference (%get-value *moss-system* '$ENLS)
                                      *system-entities*))
      (setq object-list
            (append object-list
                    (send class-id '=get-instances))))
    ;; orphans
    (setq object-list (append object-list (%get-all-orphans)))
    ;; return the ressult
    object-list))
|#

;;;------------------------------------------------- =GET-CLASSES (MOSS-SYSTEM)

(defmossinstmethod =get-classes MOSS-SYSTEM (&rest option-list)
  "gets all instances of classes by extracting them from the $ENLS property of ~
   *moss-system*, own method of $ENT considered as a metaclass.
Arguments:
   option-list (rest): ?
Return:
   List of allclasses in the system."
  (progn option-list ; to keep the compiler quiet
         (%get-value *self* '$ENLS)))

#|
(send '$SYS.1 '=get-classes)
(MOSS::$CTR MOSS::$FN MOSS::$UNI MOSS::$SYS MOSS::$ENT MOSS::$VENT MOSS::$EPR MOSS::$EPS
 MOSS::$EPT MOSS::$EP MOSS::$EIL MOSS::*NONE* MOSS::*ANY* MOSS::$DOCE MOSS::$ENDCE MOSS::$FRDCE
 MOSS::$CNFG MOSS::$USR MOSS::$QHDR MOSS::$QRY MOSS::$QSTE MOSS::$TIDXE MOSS::$QTSKE MOSS::$QHDE
            MOSS::$CVSE MOSS::$TARGE MOSS::$OBRGE MOSS::$ACTE)

But in test package:
(with-package :test
  (send test::*moss-system* '=get-classes))
($SYS $FN $UNI $CTR *NONE* *ANY* $E-OMAS-SKILL $E-OMAS-GOAL $E-OMAS-AGENT 
      $E-ORGANIZATION $E-PERSON $E-STUDENT) 
:DONE
|#
;;;------------------------------------------------------- =GET-DEFAULT (UNIVERSAL)

(defmossuniversalmethod =get-default (prop-id &optional version)
  "The method is used by =get-id to try to obtain a default value when it ~
   could not be obtained from the object itself or from its prototypes. ~
   The way to do it is to ask all the classes of the object, then ~
   the property itself. Default so far are only attached to terminal ~
   properties (attributes). Defaults cannot be inherited.
Arguments:
   prop-id: id of corresponding property
Return:
   defaul value or nil."
  (let ((context (or version (symbol-value (intern "*CONTEXT*"))))
        value-there?)
    (when (or (%is-terminal-property? prop-id context)
              (%is-relation? prop-id context))
      (catch :result
             ;; get the list of classes in case of multiple belonging
             (dolist (class-id (%get-value *self* '$TYPE context))
               ;; skip the case where the class is *any* or *none*
               (unless (or (eql class-id '*any*)(eql class-id '*none*))
                 ;; get the ideal
                 (let ((ideal-id (%make-id-for-ideal class-id))
                       default)
                   (when (%pdm? ideal-id)
                     (multiple-value-setq (default value-there?)
                       (%get-value ideal-id prop-id context))
                     (if value-there? 
                         (throw :result (values default value-there?)))))))
             ;; otherwise, ask the property itself
             ;; maybe should be tried first before the ideal?
             (%get-value prop-id '$DEFT context)))))

#|
test::$E-PERSON.0
((MOSS::$TYPE (0 $E-PERSON)) ($T-PERSON-AGE (5 20)))

(with-package :test
  (with-context 0
    (send 'test::$e-person.1 '=get-default 'test::$T-person-age)))
NIL

(with-package :test
  (with-context 5
    (send 'test::$e-person.1 '=get-default 'test::$T-person-age)))
(20)
A little strange. Would mean that starting in version 5 all persons have a default
age of 20!
|#
;;;-------------------------------------------- =GET-DEFINITION-SUMMARY (UNIVERSAL)

(defmossuniversalmethod =get-definition-summary ()
  "The method is intended to be overloaded for each class. It returns a summary ~
   of enough information to identify an object.
Arguments:
   none
Return:
   a list like (\"person\")."
  (let ((class-id (car (%get-value *self* '$TYPE))))
    (send class-id '=get-name)))

#|
(with-package :test
  (with-context 2
    (print (send 'test::$E-PERSON '=get-definition-summary))
    (print (send 'test::$E-PERSON.1 '=get-definition-summary))
    (print (send 'test::$E-STUDENT.1 '=get-definition-summary))))
("MOSS-CONCEPT") 
("PERSON") 
("STUDENT") 
|#
;;;----------------------------------------------- =GET-DOCUMENTATION (ENTRY-POINT)

(defmossinstmethod =get-documentation MOSS-ENTRY-POINT 
  (&key no-summary (lead "") final-new-line)
  "gets the documentation of the object(s) corresponding to the entry point by calling ~
   the method on the referenced object.
Arguments:
   no-summary (key): if t doest not print a leading summary of the object
   lead (key): string, if there prints it before printing doc (default null string)
   final-new-line (key): if t add a new line at the end of the string.
Return:
   string"
  (let ((object-list (access *self*)))
    ;; non empty
    (when object-list
      ;; return a string 
      (format nil "~{~A~^~%~}"
              (broadcast object-list '=get-documentation :no-summary no-summary
                         :lead lead :final-new-line final-new-line)))))

#|
Requires moss-online-doc to be loaded
(send 'person '=get-documentation)
"
PERSON : Model of a physical person"
|#
;;;---------------------------------------------------- =GET-DOCUMENTATION (METHOD)

(defmossuniversalmethod =get-documentation (&key no-summary (lead "") final-new-line)
  "Get an object documentation as a string or sorry message.
Arguments:
   no-summary (key): if t doest not print a leading summary of the object
   lead (key): string, if there prints it before printing doc (default null string)
   final-new-line (key): if t add a new line at the end of the string.
Return:
   string"
  (let* ((doc (car (send *self* '=get 'HAS-MOSS-documentation)))
         (summary (car (send *self* '=summary)))
         (sorry-mln (car (send (car (access 'moss::>-sorry)) 
                               '=get 'has-moss-documentation)))
         ;; always return something to print (*language* is the default value)
         (sorry-msg (car (mln::extract sorry-mln :always t)))
         doc-string)
    (setq doc-string
          (cond
           ;; doc, no header
           ((and doc no-summary)
            (format nil "~%~A~A" lead (car (mln::extract doc :always t))))
           ;; doc and header
           (doc
            (format nil "~%~A~A : ~A" lead summary 
                    (car (mln::extract doc :always t))))
           ;; no doc, no header
           (no-summary
            (format nil "~%~A~A" lead sorry-msg))
           ;; no doc, header
           (t
            (format nil "~%~A~A : ~A" lead summary sorry-msg))))
    ;; if we want a final new line, then print it
    (if final-new-line (format nil "~A~%" doc-string) doc-string)
    ))

#|
CG-USER(22): (send 'sa-address::$E-phone '=get-documentation)
"
phone : A TELEPHONE is a means to talk when away. One can have different types ~
             of telephone number: home phone, office phone, cell phone, etc."
CG-USER(23): (setq *language* :fr)
:FR
CG-USER(24): (send 'sa-address::$E-phone '=get-documentation)
"
téléphone : Un TELEPHONE est un moyen qui permet de communiquer à distance par la ~
             voix. il y a plusieurs types de téléphones : le téléphone du domicile, ~
             le téléphone professionnel, le portable, etc."

CG-USER(34): (send 'sa-address::$e-person '=get-documentation)
"
personne : *désolé, pas de documentation sur le sujet*"
|#
;;;*----------------------------------------------------------- =GET-ID (UNIVERSAL)
;;; One should be careful when using =get-id in conjunction with tree-properties
;;; Indeed the argument to =get-id should be the exact property-id applying
;;; to *self*. It is thus safer (but less efficient to use =get).
;;;***** needs to be checked

(defmossuniversalmethod =get-id (prop-id &optional version)
  "Same as =get but uses the internal property id.
Arguments:
   prop-id: id of the specific property applying to *self*
   version (opt): if specified sets the version or context
Return:
   2 values 
   - list of values associated to the property
   - context in which the values wer found, nil if values are not specified."
  (let ((context (or version (symbol-value (intern "*CONTEXT*"))))
        value-there?)
    (cond
     ;; error if prop-id is nil
     ((null prop-id)(error "in =get-id prop argument is null for object ~A" *self*))
     ;; look just in case if we have an argument locally
     ;; check if we have a null value
     ((let (result)
        (multiple-value-setq (result value-there?)
          (%get-value *self* prop-id context))
        ;; if the value was there, we return it with flag in case it is nil
        (if value-there? (throw :return (values result value-there?)))
        ;; otherwise no value found, return nil
        result))
     
     ;; if result was nil because the value was missing, try demon
     ((and (check-method prop-id '=if-needed context)
           (send prop-id '=if-needed *self*)))
     
     ;; if no ancestors, then try to get a default value, unless we are
     ;; actually looking for the $DEFT property, in which case we return NIL
     ;; directly to avoid getting into an infinite loop.
     ((null (%get-value *self* '$IS-A context))  ; cheap test
      (if (eq prop-id '$DEFT)
          nil
        ;; otherwise return default from ideal or from property itself
        (-> *self* '=get-default prop-id)))
     
     ;; when the object has ancestors, then we examine ancestors for a 
     ;; possible value. We do that depth first.
     ((let ((entity-set (%sp-gamma *self* '$IS-A))
            value-list)
        (loop
          ;; if no more ancestors, return nil to get to next test
          (unless entity-set (return nil))
          ;; try to get a value from next ancestor
          (multiple-value-setq (value-list value-there?)
            (%get-value (pop entity-set) prop-id context))
          (if value-there? (return (values value-list value-there?))))
        ))
     
     ;; otherwise try to get a default value from the ideal
     ((-> *self* '=get-default prop-id))
     ;; otherwise return nil and flag telling that we could not find any value
     (t (values nil nil))
     )))

#|
In the test package:
(send '$E-PERSON.1 '=get-id '$T-NAME)
NIL
NIL ; meaning that no value was found in this context or any previous context

(send '$E-PERSON.1 '=get-id '$T-PERSON-NAME)
("Barthès")
0
|#

(eval-when (compile load eval)
  (export '(=if-needed)))

;;;================================================================================
;;;                              =GET-INSTANCES
;;;================================================================================

;;; the =get-instance methods return the list of instances in the package that is
;;; active when they are called. In particular they use the local *moss-system*
;;; environment.

;;;*------------------------------------------------ =GET-INSTANCES (OWN:ATTRIBUTE)

(defmossownmethod =get-instances (MOSS-ATTRIBUTE has-MOSS-concept-name MOSS-ENTITY) 
  (&rest option-list)
  "get all instances of attributes by extracting them from the $ETLS property of ~
   *moss-system*."
  (declare (ignore option-list)) ; to keep the compiler quiet
  (%get-value (symbol-value (intern "*MOSS-SYSTEM*")) '$ETLS))

#|
(send 'moss::$EPT '=get-instances)
($T-SK-NAME $T-OMAS-SKILL-SK-NAME $T-SK-DOC $T-OMAS-SKILL-SK-DOC $T-GL-NAME $T-OMAS-GOAL-GL-NAME
 $T-GL-DOC $T-OMAS-GOAL-GL-DOC $T-AG-NAME $T-OMAS-AGENT-AG-NAME $T-AG-DOC $T-OMAS-AGENT-AG-DOC
 $T-AG-KEY $T-OMAS-AGENT-AG-KEY $T-AG-LANGUAGE $T-OMAS-AGENT-AG-LANGUAGE $T-NAME
 $T-ORGANIZATION-NAME $T-PERSON-NAME $T-MIDDLE-NAME $T-PERSON-MIDDLE-NAME $T-EMAIL
            $T-PERSON-EMAIL $T-FIRST-NAME $T-PERSON-FIRST-NAME $T-AGE $T-PERSON-AGE)

(with-package :moss (send 'moss::$EPT '=get-instances))

(MOSS::$REF MOSS::$ID MOSS::$TYPE MOSS::$DEFT MOSS::$VALT MOSS::$CNAM MOSS::$XNB MOSS::$TMBT
 MOSS::$DOCT MOSS::$ARG MOSS::$REST MOSS::$CODT MOSS::$FNAM MOSS::$MNAM MOSS::$UNAM MOSS::$SNAM
 MOSS::$PRFX MOSS::$PKG MOSS::$SVL MOSS::$SFL MOSS::$SML MOSS::$DFXL MOSS::$CRET MOSS::$DTCT
 MOSS::$VERT MOSS::$ENAM MOSS::$RDX MOSS::$ONEOF MOSS::$RFC MOSS::$DEF MOSS::$PNAM MOSS::$MINT
 MOSS::$MAXT MOSS::$VRT MOSS::$SEL MOSS::$OPR MOSS::$TPRT MOSS::$NTPR MOSS::$INAM MOSS::$TIT
 MOSS::$OBJNAM MOSS::$VNBT MOSS::$CFNM MOSS::$USRNAM MOSS::$ACRT MOSS::$CVRS MOSS::$QNAM
 MOSS::$QXPT MOSS::$QUXT MOSS::$QANT MOSS::$STNAM MOSS::$LBLT MOSS::$XPLT MOSS::$IDXT
 MOSS::$WGHT MOSS::$TSKNAM MOSS::$TRGT MOSS::$PRFT MOSS::$DLGT MOSS::$OWS MOSS::$IWS MOSS::$AGT
 MOSS::$FLT MOSS::$TXLT MOSS::$FCT MOSS::$INTT MOSS::$WEBT MOSS::$WGAT MOSS::$WOUT MOSS::$ARNAM
 MOSS::$ARKT MOSS::$ARVT MOSS::$ARQT MOSS::$REFT MOSS::$CREFT MOSS::$ANAM MOSS::$OPRT)
|#
;;;--------------------------------------------------- =GET-INSTANCES (OWN:CONCEPT)

(defmossownmethod =get-instances (MOSS-ENTITY has-MOSS-concept-name MOSS-CONCEPT) 
  (&rest option-list)
  "get all instances of classes by extracting them from the $ENLS property of ~
   *moss-system*, own method of metaclass.
Arguments:
   option-list (rest): ?
Return:
   List of allclasses in the system."
  (declare (ignore option-list)) ; to keep the compiler quiet
  (%get-value (symbol-value (intern "*MOSS-SYSTEM*")) '$ENLS
              (symbol-value (intern "*CONTEXT*"))))

#|
(send '$ent '=get-instances) ; in package MOSS
(MOSS::$CTR MOSS::$FN MOSS::$UNI MOSS::$SYS MOSS::$ENT MOSS::$VENT MOSS::$EPR MOSS::$EPS
 MOSS::$EPT MOSS::$EP MOSS::$EIL MOSS::*NONE* MOSS::*ANY* MOSS::$DOCE MOSS::$ENDCE MOSS::$FRDCE
 MOSS::$CNFG MOSS::$USR MOSS::$QHDR MOSS::$QRY MOSS::$QSTE MOSS::$TIDXE MOSS::$QTSKE MOSS::$QHDE
            MOSS::$CVSE MOSS::$TARGE MOSS::$OBRGE MOSS::$ACTE)

;; in package test
(with-context 6 (defconcept "concept-in-v6" (:att "name")))
$E-CONCEPT-IN-V6

(with-context 0 (send '$ent '=get-instances)) ; in package test
($SYS $FN $UNI $CTR *NONE* *ANY* $E-OMAS-SKILL $E-OMAS-GOAL $E-OMAS-AGENT $E-ORGANIZATION
      $E-PERSON $E-STUDENT)

(with-context 6 (send 'moss::$ent '=get-instances))
($SYS $FN $UNI $CTR *NONE* *ANY* $E-OMAS-SKILL $E-OMAS-GOAL $E-OMAS-AGENT $E-ORGANIZATION
 $E-PERSON $E-STUDENT $E-CONCEPT-IN-V6) 
|#
;;;------------------------------------------------------- =GET-INSTANCES (CONCEPT)
;;; BUG?: shadowed by own method ?

(defmossinstmethod =get-instances MOSS-ENTITY (&key min max ideal include-subclasses)
  "gets a list of instances of the corresponding class. Uses the counter to build ~
   the instance names (including the ideal)"
  (let*((context (symbol-value (intern "*CONTEXT*")))
        (cmin (or min 1)) ; does not includes the ideal
        (cmax (or max most-positive-fixnum))
        instance-result counter instance-counter )
    (catch 
     :error 
     ;(format nil "while getting instances of ~S" *self*)
     ;; first insert the ideal in the resulting list
     (setq instance-result (when ideal (list (%make-id-for-ideal *self*))))
     ;;then get the value of the counter (remember couters are definedin context 0)
     (setq counter (car (%%get-value *self* '$CTRS 0)))
     (setq instance-counter (car (%get-value counter '$VALT 0)))
     ;(setq instance-counter (car (=> (car (h==> 'HAS-MOSS-COUNTER)) '=has-value 'HAS-MOSS-VALUE)))
     (format nil "while getting instances of ~S" *self*)
     ;(format t "~&;=get-instances for ENTITY; counter: ~S; cmax: ~S" counter cmax)
     ;; watch it: we do not
     (setq cmax (min (1- instance-counter) cmax))
     ;; return the required list
     (setq instance-result 
           (append instance-result
                   (remove 
                    nil
                    (loop for cc from cmin to cmax
                        collect (let ((id (%make-id-for-instance *self* cc)))
                                  (if (%alive? id context) id))))))
     ;; if subclasses are wanted get their instances
     (when include-subclasses
       (dolist (class-id (%get-value *self* '$IS-A.OF context))
         (setq instance-result
               (append instance-result
                       (send class-id '=get-instances :include-subclasses t)))))
     instance-result
     )))

#|
(with-package :test
  (with-context 0
    (send 'test::$E-person '=get-instances)))
(TEST::$E-PERSON.1 TEST::$E-PERSON.2)

(with-package :test
  (with-context 2
    (send 'test::$E-person '=get-instances)))
(TEST::$E-PERSON.1 TEST::$E-PERSON.2 TEST::$E-PERSON.4 TEST::$E-PERSON.5)

(with-package :test
  (with-context 2
    (send 'test::$E-person '=get-instances :include-subclasses t)))
(TEST::$E-PERSON.1 TEST::$E-PERSON.2 TEST::$E-PERSON.4 TEST::$E-PERSON.5 
                   TEST::$E-STUDENT.1)
|#
;;;--------------------------------------------------- =GET-INSTANCES (OWN:COUNTER)

(defmossownmethod =get-instances (MOSS-COUNTER has-MOSS-concept-name MOSS-ENTITY) 
  (&rest option-list)
  "get all instances of counters by extracting them from the $ETLS property of ~
   *moss-system*."
  (declare (ignore option-list))
  (let ((class-list 
         (%get-value (symbol-value (intern "*MOSS-SYSTEM*")) '$ENLS)) )
    (mapcar #'(lambda (xx) (car (%%has-value xx '$CTRS 0))) class-list)))

#|
(send '$ctr '=get-instances) ; in package MOSS
($CTR.CTR $FN.CTR $UNI.CTR NIL $ENT.CTR $VENT.CTR $EPR.CTR $EPS.CTR $EPT.CTR 
          $EP.CTR ...)

(with-package :test
  (with-context 0
    (send 'moss::$ctr '=get-instances))) ; in package address-proxy
($SYS.CTR $FN.CTR $UNI.CTR $CTR.CTR *NONE*.CTR *ANY*.CTR $E-OMAS-SKILL.CTR $E-OMAS-GOAL.CTR
 $E-OMAS-AGENT.CTR $E-ORGANIZATION.CTR $E-PERSON.CTR $E-STUDENT.CTR)
|#
;;;----------------------------------------------- =GET-INSTANCES (OWN:ENTRY-POINT)

(defmossownmethod =get-instances (MOSS-ENTRY-POINT has-MOSS-concept-name MOSS-ENTITY) 
  (&rest option-list)
  "get all instances of entry-points by extracting them from the $EPLS property of ~
   *moss-system*."
  (declare (ignore option-list)) ; to keep the compiler quiet
  (%get-value (symbol-value (intern "*MOSS-SYSTEM*")) '$EPLS))

#|
(send '$EP '=get-instances)
(MOSS HAS-MOSS-REF IS-MOSS-REF-OF HAS-MOSS-IDENTIFIER IS-MOSS-IDENTIFIER-OF HAS-MOSS-TYPE
 IS-MOSS-TYPE-OF HAS-MOSS-DEFAULT IS-MOSS-DEFAULT-OF MOSS-COUNTER ...)

(with-package :test
  (with-context 2
    (send 'moss::$EP '=get-instances))) ; in the address-proxy package
(SA_TEST-MOSS SA_TEST-MOSS-SYSTEM METHOD UNIVERSAL-METHOD COUNTER NULL-CLASS UNIVERSAL-CLASS
 ...
 HAS-WIFE IS-WIFE-OF HAS-TEACHER IS-TEACHER-OF =GET-TEACHER BARTHÈS BARTHÈS-BIESEL STREIT) 
|#
;;;---------------------------------------------- =GET-INSTANCES (OWN:INVERSE-LINK)

(defmossownmethod =get-instances (MOSS-INVERSE-LINK has-MOSS-concept-name MOSS-ENTITY) 
  (&rest option-list)
  "get all instances of inverse relations by extracting them from the $EILS property ~
   of *moss-system*."
  (declare (ignore option-list)) ; to keep the compiler quiet
  (%get-value (symbol-value (intern "*MOSS-SYSTEM*")) '$EILS))

#|
(send '$EIL '=get-instances)
($REF.OF $ID.OF $TYPE.OF $DEFT.OF $VALT.OF $CNAM.OF $XNB.OF $TMBT.OF $DOCT.OF ...)

(with-package :test
  (with-context 0
    (send 'moss::$EIL '=get-instances)))
($T-SK-NAME.OF $T-OMAS-SKILL-SK-NAME.OF $T-SK-DOC.OF $T-OMAS-SKILL-SK-DOC.OF $T-GL-NAME.OF
 $T-OMAS-GOAL-GL-NAME.OF $T-GL-DOC.OF $T-OMAS-GOAL-GL-DOC.OF $T-AG-NAME.OF
 ...
 $S-TEACHER.OF $S-STUDENT-TEACHER.OF)
|#
;;;---------------------------------------------------- =GET-INSTANCES (OWN:METHOD)

(defmossownmethod =get-instances (MOSS-method has-MOSS-concept-name MOSS-ENTITY) 
  (&rest option-list)
  "get all instances of methods by extracting them from the $EILS property ~
   of *moss-system*."
  (declare (ignore  option-list)) ; to keep the compiler quiet
  ;; use the local *moss-system* environment
  (%get-value (symbol-value (intern "*MOSS-SYSTEM*")) '$FNLS))

#|
(send '$FN '=get-instances)
($FN.0 $FN.1 $FN.2 $FN.3 $FN.4 $FN.5 $FN.6 $FN.7 $FN.8 $UNI.0 ...)

(with-package :test
  (with-context 0
    (send 'moss::$FN '=get-instances)))
($E-FN.1 $E-FN.2 $FN.3 $FN.4 $E-FN.5 $FN.6 $FN.7 $FN.8 $E-FN.9 $E-FN.10)
|#
;;;-------------------------------------------------- =GET-INSTANCES (OWN:RELATION)

(defmossownmethod =get-instances (MOSS-STRUCTURAL-PROPERTY has-MOSS-concept-name MOSS-ENTITY) 
  (&rest option-list)
  "get all instances of relations by extracting them from the $ESLS property of ~
   *moss-system*."
  (declare (ignore option-list)) ; to keep the compiler quiet
  (%get-value (symbol-value (intern "*MOSS-SYSTEM*")) '$ESLS))

#|
(send '$EPS '=get-instances)
($DOCS $PT $PS $IS-A $OMS $IMS $CTRS $SYSS $DEFS $SEQS ...)

(with-package :test
  (with-context 0
    (send 'moss::$EPS '=get-instances)))
($S-AG-SKILL $S-OMAS-AGENT-AG-SKILL $S-AG-GOAL $S-OMAS-AGENT-AG-GOAL $S-PRESIDENT
 ...
 $S-STUDENT-TEACHER)
|#
;;;-------------------------------------- =GET-INTERNAL-INSTANCE-NUMBER (UNIVERSAL)

(defmossuniversalmethod =get-internal-instance-number ()
  "returns a string containing a number ranking the instance in its class"
  (%get-internal-instance-number *self*))

#|
(send 'test::$e-person.2 '=get-internal-instance-number)
"2"
|#
;;;--------------------------------------------- =GET-INVERSE-LINK-INFO (UNIVERSAL)

(defmossuniversalmethod =get-inverse-link-info ()
  "returns a list of triples giving information on the inverse links of an object."
  (let (inv-name pred-list class-id class-ref summary result)
    (dolist (item (send *self* '=has-inverse-properties))
      ;; get the name of the inverse link
      (setq inv-name (car (send item '=get-name)))
      ;; get the set of linked objects
      (setq pred-list (%get-value *self* item))
      ;; for each object 
      (dolist (pred-id pred-list)
        ;; get the class of the object
        (setq class-id (car (%get-value pred-id '$TYPE)))
        ;; get the name of the class (the first one in case of multiple classes
        (setq class-ref (car (send class-id '=get-name)))
        ;; get the summary of the object
        (setq summary (car (send pred-id '=summary)))
        ;; build a string with the info and save it
        (push (string+ inv-name "/" class-ref "/" summary) result))
      )
    ;; return a list of strings
    (reverse result)))

#|
(with-package :test
 (with-context 0
   (send 'test::$e-person.2 '=get-inverse-link-info)))
("IS-WIFE-OF/PERSON/Barthès: Jean-Paul")
|#
;;;-------------------------------------------- =GET-INVERSE-PROPERTIES (UNIVERSAL)
;;; The semantic of =get is that each value appearing at a given level shadows
;;; the above level. There is no union inheritance of values. Hence to get
;;; all possible properties one has to compute the transitive closure explicitly

(defmossuniversalmethod =get-inverse-properties ()
  "gets list of all inverse links of a given entity from its model. ~
   If it is a classless object then returns the list in a random order.
Arguments:
   nil
Return:
   list of properties fr a concept."
  (let ((context (symbol-value (intern "*CONTEXT*"))))
  (if
    (%is-classless-object? *self* context)
    ;; returns the list of all props. Object is loaded by send function
    (mapcar #'car (symbol-value *self*)) ; generalized symbol-value
    ;; otherwise uses its model
    ;+93/07/03 changed to improve efficiency (note that we use reduce append mapcar
    ; rather than mapcan which is buggy in this version (builds circular lists)
    (delete-duplicates
     (reduce #'append 
             (mapcar #'(lambda(xx)(%get-value xx '$SUC.OF context))
                     (%sp-gamma-l (%get-value *self* '$TYPE context) '$IS-A)))))))


;;;--------------------------------------------------- =GET-LAST-INSTANCE (CONCEPT)

(defmossinstmethod =get-last-instance MOSS-ENTITY (&key even-if-dead)
  "gets the last instance that was created for this class. Does not include subclasses.
Arguments:
   even-if-dead (key): if true, returns erased instance. If not tries previous instance."
  (%get-last-instance *self* :even-if-dead even-if-dead))

#|
(with-package :test
 (with-context 0
   (send 'test::$e-person '=get-last-instance)))
TEST::$E-PERSON.2

(with-package :test
  (with-context 5
    (send 'test::$e-person '=get-last-instance)))
TEST::$E-PERSON.7
|#
;;;------------------------------------------------- =GET-NEXT-INSTANCE (UNIVERSAL)

(defmossuniversalmethod =get-next-instance (&key even-if-dead)
  "get the instance following this one. Does not include subclasses.
Arguments:
   even-if-dead (key): if true, returns erased instance. If not tries next instance."
  (%get-next-instance *self* :even-if-dead even-if-dead))

#|
(with-package :test
 (with-context 0
   (send 'test::$e-person.1 '=get-next-instance)))
TEST::$E-PERSON.2

(with-package :test
 (with-context 0
   (send 'test::$e-person.2 '=get-next-instance)))
NIL
|#
;;;--------------------------------------------- =GET-PREVIOUS-INSTANCE (UNIVERSAL)

(defmossuniversalmethod =get-previous-instance (&key even-if-dead)
  "get the instance preceding this one. Does not include subclasses.
Arguments:
   even-if-dead (key): if true, returns erased instance. If not tries previous instance."
  (%get-previous-instance *self* :even-if-dead even-if-dead))

#|
(with-package :test
 (with-context 0
   (send 'test::$e-person.2 '=get-previous-instance)))
TEST::$E-PERSON.1

(with-package :test
 (with-context 5
   (send 'test::$e-person.7 '=get-previous-instance)))
TEST::$E-PERSON.6
|#
;;;======================== end =get-instances ====================================

;;;---------------------------------------------------------- =GET-NAME (ATTRIBUTE)
;;; takes the package of the object receiving the message

(defmossinstmethod =get-name MOSS-ATTRIBUTE ()
  "Returns the name of an instance $ENAM or $PNAM"
  (send-no-trace *self* '=instance-name ))

#|
(send `moss::$enam '=get-name)
("MOSS-CONCEPT-NAME")

(with-package :test
 (with-context 0
   (send 'test::$T-PERSON-NAME '=get-name)))
("name")
|#
;;;---------------------------------------------------------- =GET-NAME (UNIVERSAL)

(defmossuniversalmethod =get-name ()
  "Returns the name of an object using =instance-name."
  (send-no-trace *self* '=instance-name ))

#|
(send `$ent '=get-name)
("MOSS-CONCEPT")

(with-package :test
 (with-context 3
   (send test::_person '=get-name)))
("PERSON")
|#
;;;---------------------------------------------------- =GET-PROPERTIES (UNIVERSAL)
;;; The semantic of =get is that each value appearing at a given level shadows
;;; the above level. There is no union inheritance of values. Hence to get
;;; all possible properties one has to compute the transitive closure explicitly

(defmossuniversalmethod =get-properties ()
  "gets list of all attributes and relations if a given entity from its model. ~
   If it is a classless object then returns the list in a random order.
Arguments:
   nil
Return:
   list of properties fr a concept."
  (let ((context (symbol-value (intern "*CONTEXT*"))))
  (if
    (%is-classless-object? *self* context)
    ;; returns the list of all props. Object is loaded by send function
    (mapcar #'car (symbol-value *self*))
    ;; otherwise uses its model
    ;+93/07/03 changed to improve efficiency (note that we use reduce append mapcar
    ; rather than mapcan which is buggy in this version (builds circular lists)
    (delete-duplicates
     (reduce #'append 
             (mapcar #'(lambda(xx)
                         (append
                          (%get-value xx '$PT context)
                          (%get-value xx '$ps context)))
                     (%sp-gamma-l (%get-value *self* '$TYPE context) '$IS-A)))))))

#|
(send *moss-system* '=get-properties)
($SNAM $PRFX $PKG $SVL $SFL $SML $DFXL $CRET $DTCT $VERT ...)

(with-package :test
  (with-context 3
    (send test::_person '=get-properties)))
($ENAM $RDX $DOCT $ONEOF $XNB $TMBT $PT $PS $IS-A $OMS ...)

(with-package :test
  (with-context 3
    (send test::_jpb '=get-properties)))
($T-PERSON-NAME $T-PERSON-MIDDLE-NAME $T-PERSON-EMAIL $T-PERSON-FIRST-NAME $T-PERSON-AGE
 $S-PERSON-EMPLOYER $S-PERSON-COUSIN $S-PERSON-FATHER $S-PERSON-HUSBAND $S-PERSON-MOTHER
 $S-PERSON-BROTHER $S-PERSON-SISTER $S-PERSON-WIFE)
|#
;;;------------------------------------------------ =GET-USER-CLASSES (OWN:CONCEPT)
;;; no longer applies
#|
(defmossownmethod =get-user-classes (MOSS-ENTITY HAS-MOSS-CONCEPT-NAME MOSS-ENTITY)
  (&rest option-list)
  "get all instances of classes by extracting them from the $ENLS property of ~
   *moss-system*. All classes in the moss package are removed from the list."
  (declare (ignore option-list)) ; to keep the compiler quiet
  (remove-if #'%is-system-model? (%get-value *moss-system* '$ENLS)))
|#

#|
? (send 'moss::$ent '=get-user-classes)
($E-ORANGE $E-FRUIT $E-APPLE $E-BANANA $E-PERSON $E-STUDENT $E-TOWN $E-TEACHER)
|#
;;;----------------------------------------------- =GET-USER-CONCEPTS (OWN:CONCEPT)
;;; no longer applies
#|
(defmossownmethod =get-user-concepts (MOSS-ENTITY HAS-MOSS-CONCEPT-NAME MOSS-ENTITY) 
  (&rest option-list)
  "get all user-defined concepts by extracting them from the $ENLS property of ~
   *moss-system*. All classes in the moss package are removed from the list."
  (declare (ignore option-list))
  (remove-if #'%is-system-model? (%get-value *moss-system* '$ENLS)))
|#

;;;============================== End =get methods ================================

;;;-------------------------------------------- =HAS-INVERSE-PROPERTIES (UNIVERSAL)
;;; =has-inverse-properties get all the actual inverse properties of an object 

(defmossuniversalmethod =has-inverse-properties ()
  "get list of all local inverse properties of a given entity"
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    (reduce #'append
            (mapcar #'(lambda(xx)
                        (when (%is-inverse-property? (car xx) context)
                          (ncons (car xx))))
              (symbol-value *self*)
              ))))

#|
(send '$ent '=has-inverse-properties)
($ENLS.OF $SUC.OF)

(with-package :test
  (with-context 3
    (send test::_jpb '=has-inverse-properties)))
($S-PERSON-HUSBAND.OF $S-ORGANIZATION-PRESIDENT.OF)
|#
;;;---------------------------------------------------- =HAS-PROPERTIES (UNIVERSAL)
;;; =has-properties get all the actual properties of an object except for $TYPE
;;; and $ID and inverse links even if no values are associated
#|
=has-properties
What is intended is to get all local tp and sp of a given entity except for $TYPE, ~
   $ID, and inverse links.
Problem:
   in the case of an object belonging to several classes, the invisible pointer ~
   will return the property $REF. Thus, the reference should be resolved prior ~
   to collecting properties.
|#

(defmossuniversalmethod =has-properties ()
  "get list of all local tp and sp of a given entity except for $TYPE, ~
   $ID, and inverse properties whether they have a value or not in current context.
Arguments:
   nil
Return:
   the list of local property ids."
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    (format t "~%; =has-properties /context: ~S" context)
    (reduce #'append
            (mapcar #'(lambda(xx)
                        (unless
                            (or (eq (car xx) '$TYPE)
                                (eq (car xx) '$ID)
                                (%is-inverse-property? (car xx) context))
                          (ncons (car xx))))
              (symbol-value *self*)))))


#|
(send '$ent '=has-properties)
($ENAM $RDX $CTRS $DOCT $PT $PS $IMS $OMS)

(with-package :test
  (with-context 5
    (send test::_dbb '=has-properties)))
(TEST::$T-PERSON-NAME TEST::$T-PERSON-FIRST-NAME TEST::$S-PERSON-HUSBAND)
|#
;;;--------------------------------------------------------- =HAS-TYPE? (UNIVERSAL)
;;; a method that tests the type of an object

(defmossuniversalmethod =has-type? (type-ref &key subtype-allowed)
  "checks the type of an object.
Arguments:
   type-ref: reference of a class
   subtype-allowed (key): if T succeedes if object type is a subtype of type-ref
Return:
   the object-type if success, nil otherwise."
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (class-id (%%get-id type-ref :class))
         (obj-type (car (%%get-value *self* '$type context)))
         )
    (cond
     ;; if subclasses are not allowed, compare type directly
     ((null subtype-allowed)
      (and (eql class-id obj-type)
           class-id))
     ;; if subtypes are allowed use %type-of
     ((%type? *self* class-id context)
      obj-type))))
      
#|
(catch :error
       (with-package :test
         (with-context 5
           (defconcept "child" (:is-a "person"))
           (defindividual "child" ("name" "Zoe")(:var _z))
           (print _z)
           (print (send _z '=has-type? "child"))
           (print (send _z '=has-type? "person"))
           (print (send _z '=has-type? "person" :subtype-allowed t))
           )))
$E-CHILD.1 
$E-CHILD 
NIL 
$E-CHILD 
TEST::$E-CHILD
|#
;;;--------------------------------------------------------- =HAS-VALUE (UNIVERSAL)
;;; Like =get, =has-value may have a problem when prop-name is used for several
;;; properties.
;;; Might be interesting to have =has-value+ and =has-value++ for MLNs

(defmossuniversalmethod =has-value (prop-name)
  "Checks if a given attribute has any locally associated value.
Arguments:
   prop-name: the name of a property, e.g. HAS-FIRST-NAME
Return:
   the associated list of values."
  (let* ((context (symbol-value (intern "*CONTEXT*")))
        (prop-id (%get-property-id-from-name *self* prop-name context)))
    (if prop-id 
      (%%has-value *self* prop-id context)
      (error "in =has-value ~A is not a property of object ~A in package ~S and ~
              context ~A"
             prop-name (-> *self* '=summary) (package-name *package*) context))))

#|
(send '$ent '=has-value 'HAS-MOSS-CONCEPT-NAME)
(((:EN "MOSS-CONCEPT" "MOSS-ENTITY" "MOSS-CLASS")))

(with-package :test
  (with-context 5
    (send test::_jpb '=has-value 'test::HAS-NAME)))
("Barthès")

(with-package :test
  (with-context 5
    (send test::_jpb '=has-value "name")))
("Barthès")
|#
;;;------------------------------------------------------ =HAS-VALUE-ID (UNIVERSAL)
;;; Basic method that is used to obtain the list of values associated with
;;; a given property in a given context. It uses the %%has-value primitive

(defmossuniversalmethod =has-value-id (prop-id)
  "Retrieves the value-list associated with a property in a given context. ~
   The value-list is obtained locally, default values are not considered.
Arguments:
   prop-id: id of property of interest
Return:
  the associated list of values."
  (%%has-value *self* prop-id (symbol-value (intern "*CONTEXT*"))))

#|
(send '$ENT '=has-value-id '$ENAM)
(((:EN "MOSS-CONCEPT" "MOSS-ENTITY" "MOSS-CLASS")))

(with-package :test
  (with-context 5
    (send test::_jpb '=has-value-id 'test::$t-person-name)))
("Barthès")
|#
;;;-------------------------------------------------------------- =ID (ENTRY-POINT)
;;; Method to recover an internal name from the entry point and the description
;;; of what it is
;;; *** there should be a :filter as an option

(defmossinstmethod =id MOSS-ENTRY-POINT (property class)
  "recovers the internal-id of an object from its entry-point, the associated ~
   property, and the class to which it belongs
    E.g. (send 'BARTHES '=id 'HAS-NAME 'PERSON)
    It uses the internal function %extract. ~
   Watch it, the result could be a list of objects
Arguments:
   property-ref: ref of property
   class-ref: refof class
Return:
   single value or list of values."
  (%extract *self* property class))

#|
(send 'MOSS-COUNTER '=id 'HAS-MOSS-CONCEPT-NAME 'MOSS-ENTITY)
$CTR

(with-package :test
  (with-context 3
    (send 'test::BARTHèS '=id 'test::HAS-NAME 'test::PERSON)))
(TEST::$E-PERSON.3 TEST::$E-PERSON.2 TEST::$E-PERSON.1)

(with-package :test
  (with-context 3
    (send 'test::BARTHèS '=id "NAME" "PERSON")))
(TEST::$E-PERSON.3 TEST::$E-PERSON.2 TEST::$E-PERSON.1)
|#
;;;---------------------------------------------------------- =IF-ADDED (ATTRIBUTE)

(defmossinstmethod =if-added MOSS-ATTRIBUTE (value entity-id)
  "daemon for doing book keeping after adding something. ~
   Default is to do nothing returning value. ~
   If error is detected then returning nil aborts data.
Arguments:
   value: new value
   entity-id: id of object (in case it is needed)
Return:
   value if OK, nil otherwise."
  ;; progn to avoid compiler complaints
  (progn entity-id value))

;;;------------------------------------------------------------- =IF-ADDED (ENTITY)

(defmossinstmethod =if-added MOSS-ENTITY ()
  "Daemon to be called after an object is created."
  *self*)

;;;----------------------------------------------------------- =IF-ADDED (RELATION)

(defmossinstmethod =if-added MOSS-RELATION (suc-id entity-id)
  "Daemon for doing book keeping after adding something. ~
   Default is to do nothing returning value. ~
   If error is detected then returning nil aborts data.
Arguments:
   suc-id: id of successor
   entity-id: id of object (in case it is needed)
Return:
   suc-id if OK, nil otherwise."
  ;; progn to avoid compiler complaints
  (progn entity-id suc-id))

;;;--------------------------------------------------------- =IF-NEEDED (UNIVERSAL)
;;; if the method is not overridden by a local one, then returns nil
;;; JPB 1601 removing method since it conflicts with find-subset when accessing
;;; data, might generate a warning message somewhere

;;;(defmossuniversalmethod =if-needed (&rest ll)
;;;  (declare (ignore ll))
;;;  nil)

;;;----------------------------------------------------------- =IF-REMOVED (ENTITY)

(defmossinstmethod =if-removed MOSS-ENTITY ()
  "Daemon to be called after an object is deleted"
  *self*)

;;;-------------------------------------------------------- =IF-REMOVED (ATTRIBUTE)

(defmossinstmethod =if-removed MOSS-ATTRIBUTE (value entity-id) 
  "daemon for doing book keeping after removing something ~
   Default is to do nothing returning nil.
Arguments:
   value: value that was removed
   entity-id: object-id
Return:
   nil"
  ;; progn to avoid compiler complaints
  (progn entity-id value nil))

;;;----------------------------------------------------- =IF-REMOVED (INVERSE-LINK)

(defmossinstmethod =if-removed MOSS-INVERSE-LINK 
  (&optional old-entity-id old-suc-id entity-id suc-id)
  "Daemon for doing book keeping after removing something ~
   Default is to do nothing returning nil - 
Arguments:
   old-entity-id (opt): id of old entity
   old-successor-id (opt): id of old successor
   entity-id (opt): id of new (?) entity
   successor-id (opt): id of new (?) successor
Return:
   nil"
  ;; progn to avoid compiler complaints
  (progn entity-id suc-id old-entity-id old-suc-id nil))

;;;--------------------------------------------------------- =IF-REMOVED (RELATION)

(defmossinstmethod =if-removed MOSS-RELATION 
  (&optional old-entity-id old-suc-id entity-id suc-id)
  "Daemon for doing book keeping after removing something ~
   Default is to do nothing returning nil - Optional arguments:
Arguments:
   old-entity-id (opt): id of old entity
   old-successor-id (opt): id of old successor
   entity-id (opt): id of new (?) entity
   successor-id (opt): id of new (?) successor
Return:
   nil"
  ;; progn to avoid compiler complaints
  (progn entity-id suc-id old-entity-id old-suc-id nil))

;;;*---------------------------------------------------------- =INCREMENT (COUNTER)
;;; unused
;;; method for incrementing counters (when creating new objects)
;;; =increment is not used for creating the kernel but will be used
;;; for creating user models. Its definition could be delayed until then.
;;; It is meaningless to increment counters without creating instances.
;;; Was used in =new methods that are currently disabled.

(defmossinstmethod =increment MOSS-COUNTER ()
  "Increases value of counter by 1. Now does that in a special way since ~
   the counter must always increase regardless of the version in which ~
   it has been created.
Argument:
   nil
Return:
   new value of counter."
  (let* ((pair (%%get-value-and-context *self* '$VALT))
         (value (1+ (cadr pair))))
    (%%set-value *self* value '$VALT (car pair))
    value))

#|
(send 'test::$E-AD.CTR '=increment)
5
((MOSS::$TYPE (0 MOSS::$CTR)) (MOSS::$VALT (0 5)) (MOSS::$CTRS.OF (4 $E-AD)))
|#
;;;*------------------------------------------------- =INHERIT-INSTANCE (UNIVERSAL)
;;; unused

(defmossuniversalmethod =inherit-instance (method-name)
  "Default inherit mechanism - Multiple - depth first
Arguments:
   method-name: e.g. =SUMMARY
Return:
   method if found"
  (let*((context (symbol-value (intern "*CONTEXT*")))
        method object-list)	 ; local variables
    (setq object-list (%parents *self*))
    ;; try each candidate in turn
    (while (and object-list (null method))
      (setq method (%get-instance-method (pop object-list) method-name context))
      )
    ;; if method has been found then return it
    method))

;;;---------------------------------------------- =INHERIT-ISA-INSTANCE (UNIVERSAL)
;;; unused

(defmossuniversalmethod =inherit-isa-instance (method-name)
  "Default inheritance mechanism - Multiple - depth first.
Arguments:
   method-name: e.g. =SUMMARY
Return:
   method if found"
  (let*(method 
        (object-list(%parents *self*))	 ; local variables
        )
    ;; try each candidate in turn, until a method is found
    (while (and object-list (null method))
      (setq method 
            (%get-isa-instance-method (pop object-list) method-name))
      )
    ;; if method has been found then return it
    method))

;;;------------------------------------------------------- =INHERIT-OWN (UNIVERSAL)
;;; unused

(defmossuniversalmethod =inherit-own (method-name)
  "Default inherit mechanism - Multiple - depth first
Arguments:
   method-name: e.g. =SUMMARY
Return:
   method if found"
  (let*(method 
        (object-list (%parents *self*))	 ; local variables
        )
    ;; try each candidate in turn
    (while (and object-list (null method))
      (setq method (%get-own-method (pop object-list) method-name))
      )
    ;; if method has been found then return it
    method 
    ))

;;;--------------------------------------------------------------------------------
;;;(%defmethod 
;;; =install MOSS-SYSTEM (file-name)
;;; "Install all models counters methods entry-points related to a given ~
;;;    application defined by sys-id - Should be done once and for all when ~
;;;    transferring the models of the application into a permanent database ~
;;;    - After this is done the initial sequential file that was used to ~
;;;    test the models may be discarded or archived - All future listings ~
;;;    of the models and properties will be obtained from the .mgf file"
;;; (catch :error
;;;        (let*((sys-id *self*)
;;;              (object-list (append
;;;                            (-> sys-id '=has-value-id '$ENLS)
;;;                            (-> sys-id '=has-value-id '$ESLS)
;;;                            (-> sys-id '=has-value-id '$ETLS)
;;;                            (-> sys-id '=has-value-id '$FNLS)
;;;                            (-> sys-id '=has-value-id '$EILS)
;;;                            (-> sys-id '=has-value-id '$SVL)
;;;                            ))
;;;              (ep-list (-> sys-id '=has-value-id '$EPLS))
;;;              )
;;;          (print (append object-list ep-list))
;;;          ;; First open requested file
;;;          (mgf-open file-name)
;;;          (if (mgf-errorp)(throw :error "Invalid file name"))
;;;          ;; ...then declare a transaction (a file must be opened
;;;          (mgf-start-transaction)
;;;          (if (mgf-errorp) (throw :error "No file opened??"))
;;;          ;; then record an initial transaction number
;;;          (mgf-store '*transaction-number* 1)
;;;          (if (mgf-errorp) (throw :error "***=install Error on first STORE"))
;;;          ;; ... and system name
;;;          (mgf-store '*moss-system* sys-id)
;;;          ;; ... and filename
;;;          (mgf-store '*filename* file-name)
;;;          ;; then record all object
;;;          (while object-list
;;;                 (mgf-store (car object-list)(symbol-value (car object-list)))
;;;                 (if (mgf-errorp)(throw :error `(***=install Error while trying
;;;                                                             to save ,(car object-list))))
;;;                 (pop object-list)
;;;                 )
;;;          ;; Now save entry-points stripped from their references to other systems
;;;          (dolist (ep ep-list)
;;;            (mgf-store ep
;;;                       (-> ep '=system-view sys-id))
;;;            (if (mgf-errorp)
;;;                (throw :error `(***=install Error while trying to save ,ep))))
;;;          ;; then end transaction
;;;          (mgf-end-transaction)
;;;          ;;; close file to save world
;;;          (mgf-close)
;;;          ;;; ... and reopen it
;;;          (mgf-open file-name)
;;;          ;; we are done
;;;          "*Installation completed*"
;;;          )))

;;;----------------------------------------------------- =INSTANCE-NAME (ATTRIBUTE)

(defmossinstmethod =instance-name MOSS-ATTRIBUTE (&key class)
  "returns attribute name with associated class name if class keyword is true, canonical name ~
   if MLN.
Arguments:
   class (key): t or nil
Return:
   list of name and class name eventually, e.g. (\"name\") or (\"name/person\")"
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (attribute-value (car (HAS-MOSS-PROPERTY-NAME)))
         (name (list 
                (if (or (mln::mln? attribute-value) ; jpb 1406
                        (mln::%mln? attribute-value) ; jpb 1411 (allow old format)
                        )
                    (mln::get-canonical-name attribute-value) ; jpb 1406
                  attribute-value)))
         (class-id (%%get-value *self* (%inverse-property-id '$PT context) context))
         )
    ;(format t "~%; =instance-name /class-id: ~S" class-id)
    (if (and class class-id)
      (list (concatenate 'string (car name) "/" 
                         (car (send-no-trace (car class-id) '=instance-name))))
      name)))

#|
(with-package :test
  (with-context 5
    (print (send 'test::$T-person-name '=instance-name))
    (print (send 'test::$T-person-name '=instance-name :class t))
    :done
    ))
("name")
("name/PERSON")
|#
;;;------------------------------------------------------- =INSTANCE-NAME (CONCEPT)

(defmossinstmethod =instance-name MOSS-ENTITY ()
  "Returns entity name, according to current language, unless language tag is :all; canonical ~
   name if MLN.
Arguments:
   none
Return:
   a list of one string element, e.g. (\"Person\")."
  (let ((name (car (HAS-MOSS-CONCEPT-NAME))))
    (list (if (or (mln::mln? name) ; jpb 1406
                  (mln::%mln?  name) ; jpb 1411
                  )
            (mln::get-canonical-name name) ; jpb 1406
            name))))

#|
(with-package :test
  (with-context 2
    (send test::_person '=instance-name)))
("PERSON")

(send '$ENT '=instance-name)
("MOSS-CONCEPT")
|#
;;;-------------------------------------------------- =INSTANCE-NAME (INVERSE-LINK)

(defmossinstmethod =instance-name MOSS-INVERSE-LINK ()
  "Returns entity name, canonical if MLN."
  (let ((inv-name (car (HAS-MOSS-INVERSE-NAME))))
    (list (if (or (mln::mln? inv-name) ; jpb 1406
                  (mln::%mln? inv-name) ; jpb 1411
                  )
            (mln::get-canonical-name inv-name) ; jpb1406
            inv-name))))

#|
(with-package :test
  (with-context 2
    (send 'test::$T-NAME.of '=instance-name)))
("IS-NAME-OF")
|#
;;;-------------------------------------------------------- =INSTANCE-NAME (METHOD)

(defmossinstmethod =instance-name MOSS-METHOD ()
  "Returns the name of the method that received the message, canonical if MLN. ~
   It is used by =print-methods."
  ;; method name is a simple string, e.g. =summary
  (let ((method-name (car (HAS-MOSS-METHOD-NAME))))
    (list (if (or (mln::mln? method-name) ; jpb 1406
                  (mln::%mln? method-name) ; jpb 1411
                  )
            (mln::get-canonical-name method-name)  ; jpb 1406
            method-name))))

#|
(send '$FN.22 '=instance-name)
(=FIND)
(with-package :test
  (with-context 2
    (send 'test::$E-FN.2 '=instance-name)))
(=SUMMARY)
|#
;;;------------------------------------------------------ =INSTANCE-NAME (RELATION)

(defmossinstmethod =instance-name MOSS-RELATION (&key class)
  "Returns relation name, canonical if MLN."
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (property-name (car (HAS-MOSS-PROPERTY-NAME)))
         (name (list (if (or (mln::mln? property-name) ; jpb 1406
                             (mln::%mln? property-name) ; jpb 1411
                             )
                       (mln::get-canonical-name property-name) ; jpb 1406
                       property-name)))
         (class-id (%%get-value *self* (%inverse-property-id '$PS) context))
         )
    (if (and class class-id)
      (list (concatenate 'string (car name) "/" 
                         (car (send-no-trace (car class-id) '=instance-name))))
      name)))

#|
(with-package :test
  (with-context 2
    (send 'test::$S-BROTHER '=instance-name)))
("Brother")

(with-package :test
    (with-context 2
      (send 'test::$S-BROTHER '=instance-name :class t)))
("brother/UNIVERSAL CLASS")

(with-package :test
    (with-context 2
      (send 'test::$S-PERSON-BROTHER '=instance-name :class t)))
("brother/PERSON")
|#
;;;----------------------------------------------------- =INSTANCE-NAME (UNIVERSAL)

(defmossinstmethod =instance-name MOSS-UNIVERSAL-METHOD ()
  "Returns the name of the method that received the message, canonical if MLN. ~
   It is used by =print-methods."
  ;; method name is a simple string, e.g. =summary
  (let ((method-name (car (HAS-MOSS-UNIVERSAL-METHOD-NAME))))
    (list (if (or (mln::mln? method-name)  ; jpb 1406
                  (mln::%mln? method-name) ; jpb 1411
                  )
            (mln::get-canonical-name method-name)  ; jpb 1406
            method-name))))
#|
(send '$UNI.25 '=instance-name)
(=GET)
|#
;;;---------------------------------------------------- =INSTANCE-SUMMARY (CONCEPT)

(defmossinstmethod =instance-summary MOSS-ENTITY (entity-id)
  "Returns a list summarizing entity to print - Default is first ~
   non nil terminal property - Maybe we should consider required ~
   properties ...
Arguments:
   entity-id: id of entity to print
Return:
   A list summarizing entity"
  (if (%is-model? entity-id)
    (send entity-id '=instance-name)
    (let ((prop-list (g==> 'HAS-MOSS-TERMINAL-PROPERTY))
          answer)
      (while (and prop-list 
                  (not (setq answer (send entity-id '=get-id (pop prop-list))))))
      answer)))

#|
(with-package :test
  (with-context 2
    (print (send `$ENT '=instance-summary test::_person))
    (print (send test::_person '=instance-summary test::_jpb))
    :done))
("PERSON") 
("Barthès") 
:DONE
;; this is because NAME is the first attribute in the list...
|#
;;;--------------------------------------------------- =INSTANCE-SUMMARY (RELATION)
;;;***** unclear what this is for?

(defmossinstmethod =instance-summary MOSS-RELATION (instance-id)
  "Returns a summary of a relation, namely its name.
Arguments:
   instance-id: the entity to print."
  (send instance-id '=get-name))

#|
? (send _has-brother '=instance-summary _jpb)
("Barthès")
? (send `$ENT '=instance-summary _person)
((:EN "Person"))
|#
;;;-------------------------------------------------------- =INVERSE-ID (ATTRIBUTE)

(defmossinstmethod =inverse-id MOSS-ATTRIBUTE ()
  "Returns the internal id of the inverse attribute. No args."
  (%inverse-property-id *self*))

#|
(with-package :test
  (with-context 1
    (send 'test::$T-AA-NAME '=inverse-id)))
Warning: object with id: $T-AA-NAME is dead in this context (1) and package
         (#<The TEST package>) when trying to apply method: 
=INVERSE-ID, with arg-list

(with-package :test
  (with-context 5
    (send 'test::$T-AA-NAME '=inverse-id)))
TEST::$T-AA-NAME.OF
|#
;;;----------------------------------------------------- =INVERSE-ID (INVERSE-LINK)

(defmossinstmethod =inverse-id MOSS-INVERSE-LINK ()
  "return the inverse prop id, unless we have a property lattice in which case ~
   it returns the wole lattice as a list. No arg."
  (%inverse-property-id *self*))

#|
(with-package :test
  (with-context 5
     (send  'test::$T-person-name.of '=inverse-id)))
TEST::$T-PERSON-NAME
|#
;;;*-------------------------------------------------------- =INVERSE-ID (RELATION)

(defmossinstmethod =inverse-id MOSS-RELATION ()
  "Returns the internal id of the inverse structural property. No args."
  (%inverse-property-id *self*))

#|
(with-package :test
  (with-context 5
     (send  'test::$S-brother '=inverse-id)))
TEST::$S-BROTHER.OF
|#
;;;------------------------------------------------------- =KILL-METHOD (UNIVERSAL)
;;; In fact does some overkill since it kills all own and instance methods
;;; from the p-list 
;;; **** untested

(defmossuniversalmethod =kill-method (method-name)
  "Reaches all models following an inverse $IS-A link, ~
   removing given method from prop-list. ~
   =kill-method must be used each time a method is suppressed or modified to remove ~
   the code from the various p-list where it was cached.
Arguments:
   method-name: method to be removed
Return:
   not significant."
  (let ((span-list (%get-value *self* (%inverse-property-id '$IS-A))))
    (remprop *self* method-name)
    (while span-list
      (-> (pop span-list) '=kill-method method-name))))

;;;*-------------------------------------------------------------- =LINK (RELATION)

(defmossinstmethod =link MOSS-RELATION (entity successor)
  "Links 2 entities.
Arguments:
   entity: id of first entity
   successor: id of the entity to link
Return:
   nothing significant."
  (progn
    (%link entity *self* successor)
    :done))

#|
(with-package :test
  (with-context 4
    (send 'test::$S-person-wife '=link 'test::$E-PERSON.1 'test::$E-PERSON.2)))
:DONE
test::$E-PERSON.1
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.1))
 (TEST::$T-PERSON-NAME (0 "Barthès")) (TEST::$T-PERSON-FIRST-NAME (0 "Jean-Paul"))
 (TEST::$S-PERSON-HUSBAND.OF (0 TEST::$E-PERSON.2))
 (TEST::$S-ORGANIZATION-PRESIDENT.OF (5 TEST::$E-ORGANIZATION.1))
 (TEST::$S-PERSON-WIFE (4 TEST::$E-PERSON.2)))
test::$E-PERSON.2
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.2))
 (TEST::$T-PERSON-NAME (0 "Barthès-Biesel" "Barthès"))
 (TEST::$T-PERSON-FIRST-NAME (0 "Dominique"))
 (TEST::$S-PERSON-HUSBAND (0 TEST::$E-PERSON.1))
 (TEST::$S-PERSON-WIFE.OF (4 TEST::$E-PERSON.1)))

(with-package :test
  (with-context 5
    (%pep 'test::$E-PERSON.1)))
$E-PERSON.1
----------
MOSS-TYPE:                    t0: $E-PERSON
MOSS-IDENTIFIER:              t0: $E-PERSON.1
----- Attributes
NAME:                         t0: "Barthès"
FIRST-NAME:                   t0: "Jean-Paul"
----- Relations
WIFE:                         t4: $E-PERSON.2
-----Inv-Links
IS-HUSBAND-OF:                t0: $E-PERSON.2
IS-PRESIDENT-OF:              t5: $E-ORGANIZATION.1
----------
:DONE
|#
;;;------------------------------------------------ =LOAD-APPLICATION (MOSS-SYSTEM)
;;; unused 

;;;---------------------------------------- =MAKE-ENTRY (OWN:HAS-MOSS-CONCEPT-NAME)
;;;--- method attached to the property ENTITY-NAME of ENTITY
;;; HAS-CONCEPT-NAME is the PROPERTY-NAME of a TERMINAL PROPERTY.

(defmossownmethod =make-entry (HAS-MOSS-CONCEPT-NAME HAS-MOSS-PROPERTY-NAME MOSS-TERMINAL-PROPERTY)
  (data)
  "Makes entry point for $ENAM.
Arguments:
   data: used to build the entry point
Return:
   a list of symbols, id of the entry point."
  (%make-entry-symbols data))

#|
(send '$enam '=make-entry "albert est parti au boulot")
(ALBERT-EST-PARTI-AU-BOULOT)

(send '$enam '=make-entry nil)
(NO-MOSS-CONCEPT-NAME)

(send '$t-person-name '=make-entry nil)
|#
;;;--------------------------------------- =MAKE-ENTRY (OWN:HAS-MOSS-PROPERTY-NAME)

(defmossownmethod =make-entry (HAS-MOSS-PROPERTY-NAME HAS-MOSS-PROPERTY-NAME MOSS-TERMINAL-PROPERTY)
  (data)
  ;; we add entry-point method to the property-name terminal property
  "Makes entry point for $PNAM. Does not take synonyms into account.
Arguments:
   data: used to build the entry point
Return:
   a list of one symbol, id of the entry point."
  ;(list (%make-name '$EPR :name data)))
  (list (%%make-name data :prop)))

#|
(send '$pnam '=make-entry "age du capitaine ; nombre de printemps")

was previously:
(HAS-AGE-DU-CAPITAINE-\;-NOMBRE-DE-PRINTEMPS)

(send '$pnam '=make-entry '((:en "age")
                            (:fr "âge du capitaine ; nombre de printemps")))
(HAS-AGE)

(with-language :fr 
  (send '$pnam '=make-entry '((:en "age")
                              (:fr "âge du capitaine" "nombre de printemps"))))
(HAS-ÂGE-DU-CAPITAINE)
|#
;;;------------------------------------------- =MAKE-OBJECT-DESCRIPTION (UNIVERSAL)
;;; to check
#|
A page description look like that:
    ((:row :header "Nom du pays, ex: Italie*" :name "pays" :edit t
           :path ("pays" "pays" "nom"))
     ;; here we use a field with several lines, the fact that an attribute does not
     ;; terminate the path means that we are using =summarize on the successor
     (:area :header "Correspondant(s), ex: Sugawara, Ramos: Milton*" 
            :name "correspondant" :rows 1 :cols "60%" 
            :path ("correspondant" "personne"))
     (:area :header "Organisation partenaire" :name "partenaire" :rows 1 
            :cols "60%" :edit t :path ("partenaire" "organisation" "nom"))
     (:row :header "Sigle de l'organisation partenaire" :name "sigle partenaire"
           :edit t :path ("partenaire" "organisation" "sigle"))
     (:row :header "Ville du contact" :name "ville" :edit t
           :path ("ville" "ville" "nom"))
     (:row :header "État, ex. Louisianne, Jalisco, Shandong, ..." :name "état"
           :edit t :path ("état" "état" "nom"))
     (:area :header "Correspondant(s) UTC, ex: JPB, Moukrim*"
            :name "correspondant UTC" :rows 1 :cols "60%"
            :path ("correspondant UTC" "personne"))
     (:row :header "Type de contact R(rencontre)/S(suivi)" :name "type de contact")
     (:row :header "Date de début du contact, ex: 2011*" :name "date de début")
     (:area :header "Commentaire" :name "commentaire" :rows 3 :cols "60%" :max 1)
     (:area :header "Inverse Links" :name "inverse-links" :rows 3 :cols "60%" :read-only t)
     )
|#
;;; we want strings everywhere and not symbols to transfer from one package to another
;;; therefore, it is the responsibility of this method to make sure that the MLN 
;;; values are extracted correctly. In particular one must be careful of the
;;; specification of the path in the page description:
;;;  - a path with 2 elements calls for the =summary method on the object
;;;  - a path with 3 elements extracts the value corresponding to the attribute, if
;;;    the value is an MLN, it is transferred as it is.
;;;*********** this should be fixed in this method
#+OMAS
(defmossuniversalmethod =make-object-description ()
  "prepares a description of an object according to the model of the edit page.
Argument:
   none
Return:
   a tree of strings describing the object."
  (declare (special omas::*omas*))
  (dformat :mkdesc 0 "~%;---------- Entering =make-object-description ----------")
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (class-id (car (send *self* '=get "moss type")))
        page-description prop-ref path value-list result tag val-list suc-ref agent
        class-ref)
    (dformat :mkdesc 1 "~%;--- context: ~S" context)
    (dformat :mkdesc 1 "~%;--- class-id: ~S" class-id)
    (dformat :mkdesc 1 "~%;--- *package*: ~S" (package-name *package*))
    
    (with-context
        context
      
      ;; we can recover a reference to the agent structure by using the name of the
      ;; package which is the same as the name of the agent key
      (setq agent (cdr (assoc (intern (package-name *package*) :keyword)
                              (omas::local-agents omas::*omas*))))
      ;; get class-ref
      (setq class-ref (car (send class-id '=get-name)))
      (dformat :mkdesc 1 "~%;--- class-ref: ~S" class-ref)
      ;; get the edit page description, normalize class-ref to access page description
      (setq page-description 
            (get-field (%string-norm class-ref) (omas::edit-layouts agent)))
      (dformat :mkdesc 1 "~%;--- page-description:~%  ~S" page-description)
      
      ;; for each row of the page-description, compute the values to post
      (dolist (item page-description)
        
        (dformat :mkdesc 0 "~2%;--- item: ~S" item)
        ;; reset val-list
        (setq val-list nil)
        ;; get property name: if there is a path, its first item, otherwise
        ;; the value associated to :name
        (setq prop-ref (or (car (getf (cdr item) :path))
                           (getf (cdr item) :name)))
        (setq tag (getf (cdr item) :name))
        (dformat :mkdesc 1 "~%;--- prop-ref ~S" prop-ref)
        (dformat :mkdesc 1 "~%;--- tag ~S" tag)
        
        (cond 
         ;;=== if prop-ref is "inverse-links", then we build a list of triples:
         ;;  inverse-prop-name, class, instance summary
         ((equal+ prop-ref "inverse-links")
          (dformat :mkdesc 0 "~%;--- ~S is an inverse link" prop-ref)
          (push
           (cons prop-ref (send *self* '=get-inverse-link-info))
           result))
         
         ;;=== if prop-ref is "user action" forget it
         ((equal+ prop-ref "user-action")
          (dformat :mkdesc 0 "~%;--- user action" prop-ref))
         
         ;;=== if attribute, get value
         ;; in case of MLN value get all synonyms for a given language
         ;; substitute also works on a NIL value
         ((%is-attribute? prop-ref)
          (dformat :mkdesc 0 "~%;--- ~S is an attribute" prop-ref)
          ;; get associated values
          (setq val-list (send *self* '=get prop-ref))
          (dformat :mkdesc 0 "~%;--- value-list: ~S" val-list)
          ;; if type of values is mln, get the right value(s)
          (if (eql (getf (cdr item) :type) :mln)
              (setq val-list (mln::extract (car val-list))))
          ;(push (cons tag value-list) result)
          )
         
         ;;=== if relation, get path, e.g. ("ville" "ville" "nom") or
         ;;  ("correspondant UTC" "personne")
         ((%is-relation? prop-ref) 
          (dformat :mkdesc 0 "~%;--- ~S is a relation" prop-ref)
          ;; get the list of successors
          (setq value-list (send *self* '=get prop-ref))
          (dformat :mkdesc 0 "~%;--- value-list: ~S" value-list)
          (setq path (getf (cdr item) :path))
          (setq val-list nil)
          (dolist (suc-id value-list)
            (dformat :mkdesc 1 "~%;--- suc-id: ~S" suc-id)
            ;; make string reference
            (setq suc-ref (format nil "~S" suc-id))
            (dformat :mkdesc 1 "~%;--- path ~S" path)
            ;; if path is (rel class) use =summary
            (cond
             ((null (cddr path))
              (push `(,(car (send suc-id '=summary)) ,prop-ref ,suc-ref) val-list)
              )
             ;; when an attribute is specified (rel class attr)
             (t
              (dformat :mkdesc 1 "~%;--- <attr>: ~S" (caddr path))
              (push 
               ;; get the value(s) associated with the attribute
               `(,(or (car (send suc-id '=get (caddr path))) "") 
                   ,prop-ref ,suc-ref ,@(cddr path))
               val-list ))
             )
            )
          )
         ) ; end cond
        
        ;; if values are supposed to be MLN, get the values for the current language
        (when (eql (getf (cdr item) :type) :mln)
          (dformat :mkdesc 1 "~%;--- fix MLN values: ~S" val-list)
            (setq val-list
                  (mapcar #'(lambda(xx) 
                              (cons (mln::get-canonical-name (car xx)) ; jpb 1406
                                    (cdr xx)))
                    val-list)))
        
        (dformat :mkdesc 0 "~%;--- new entry: ~S" (car result))
        
        (push (cons tag (reverse val-list)) result)
        ) ; end dolist
      
      (append (send class-id '=get-name)
              `(("id" ,(format nil "~S" *self*)))
              (reverse result))
      ) ; with-context
    ))
            
#|
(SEND '$E-CONTACT.21 '=MAKE-OBJECT-DESCRIPTION)
("contact"
 ("id" "$E-CONTACT.21")
 ("correspondant" 
  ("Sugawara: Kenji" "correspondant" "$E-PERSON.39")
  ("Fujita: Shigeru" "correspondant" "$E-PERSON.18"))
 ("partenaire" 
  ("Chiba Institute of Technology" "partenaire" "$E-ORGANIZATION.2" "nom"))
 ("sigle partenaire" ("CIT" "partenaire" "$E-ORGANIZATION.2" "sigle"))
 ("ville" ("Tsudanuma" "ville" "$E-CITY.15" "nom"))
 ("état")
 ("correspondant UTC" 
  ("Barthès: Jean-Paul" "correspondant UTC" "$E-PERSON.3")
  ("Moulin: Claude" "correspondant UTC" "$E-PERSON.27"))
 ("type de contact" "rencontre")
 ("date de début" "2000")
 ("commentaire" "RAS"))

(with-package :test
  (with-context 5
    (send 'test::$E-PERSON.1 '=make-object-description)))
|#
;;;-------------------------------------------------- =MAKE-PRINT-ALIST (UNIVERSAL)

(defmossuniversalmethod =make-print-alist ()
  "produces an a-list containing pairs of strings (<prop> <values>) representing ~
   the object to be printed. Values are obtained through a =get.
Arguments:
   none
Return:
   a-list"
  (let ((object-l (symbol-value (moss::%ldif *self*)))
        prop-id result)
    ;; brute-force loop on properties
    (dolist (item object-l)
      (setq prop-id (car item))
      ;; if object is a class, special processing for attributes and relations
      (cond
       ((and (%is-model? *self*)
             (eql prop-id '$PT))
        ;; gather all attributes
        (push
         (cons (send '$PT '=get-name)
               (broadcast '=format-value 
                          (%remove-redundant-properties 
                           (%%get-all-class-attributes *self*))))
         result))
       
       ((and (%is-model? *self*)
             (eql prop-id '$PS))
        ;; gather all relations
        (push
         (cons (car (send '$PS '=get-name))
               (broadcast
                (%remove-redundant-properties 
                 (%%get-all-class-relations *self*)) '=summary))
         result))
       ;; otherwise make a pair
       ((%is-attribute? prop-id)
        (push
         (cons (car (send prop-id '=get-name))
               (send *self* '=get-id prop-id))
         result))
       ((%is-relation? prop-id)
        (push
         (list (car (send prop-id '=get-name))
               ;; if several values, make a list of them
               (format nil
                   "~{~{~A~^ ~}~^, ~}"
                 (broadcast (send *self* '=get-id prop-id) '=summary)))
         result))
       )
      )
    (reverse result)))
                 
#|
(with-package :test
  (with-context 5
    (send 'test::$E-PERSON.1 '=make-print-alist)))
(("MOSS-TYPE" TEST::$E-PERSON) ("MOSS-IDENTIFIER" TEST::$E-PERSON.1) 
 ("name" "Barthès") ("first name" "Jean-Paul"))
|#
;;;*------------------------------------------------ =MAKE-PRINT-STRING (ATTRIBUTE)

(defmossinstmethod =make-print-string MOSS-ATTRIBUTE (value-list &key header no-value-flag)
  "produces a string with values associated to an attribute.
Arguments:
   value-list: values associated to attribute
   header (key): title to use instead of attribute name
   no-value-flag (key): if t print even if value-list is nil
Returns:
   a string"
  (format t "~%; =make-print-string (att)/ att: ~S value-list: ~S" *self* value-list)
  ;; if no value present, then do not print anything
  (if (or value-list no-value-flag)  
    (append
     (if header (list header) (-> *self* '=get-name))
     (mapcar #'(lambda (xx) (-> *self* '=format-value xx)) value-list))))

#|
? (send _has-person-name '=make-print-string '("Albert" "Gérard"))
"name:     Albert, Gérard"
? (send _has-person-name '=make-print-string '("Albert" "Gérard") :header "Nom ")
"Nom :     Albert, Gérard"
? (send _has-person-name '=make-print-string nil :header "Nom ")
""
|#
;;;*------------------------------------------------- =MAKE-PRINT-STRING (RELATION)
;;;
;;; We have a special case when printing concepts and values are attributes or
;;; relations. In that case we should stop the recursion.

(defmossinstmethod
    =make-print-string MOSS-RELATION (suc-list &key header no-value-flag)
  "prints a summary of all linked entities.
Arguments:
   suc-list: list of successors
   offset (key): the number of leading spaces (default 0)
   header (key): title to use instead of attribute name
Returns:
   a list starting with prop name and lists of summaries (?)"
  (format t "~%; =make-print-string (rel)/ rel: ~S suc-list: ~S" *self* suc-list)
  (if (or suc-list no-value-flag)
    (append 
     (if header (list header) (-> *self* '=get-name))
     (mapcar #'(lambda (xx) 
                 (if (or (%is-attribute? xx)
                         (%is-relation? xx)
                         (%is-inverse-property? xx)) ; not sure this is needed
                   (car (-> xx '=get-name))
                   (-> xx '=summary))) 
       suc-list))))

#|
(with-package :test
  (with-context 6
    (send test::_has-person-father '=make-print-string 
          '(test::$E-PERSON.1 test::$E-PERSON.2))))
("father" ("Barthès: Jean-Paul") ("Barthès-Biesel Barthès: Dominique"))
|#
;;;*------------------------------------------------ =MAKE-PRINT-STRING (UNIVERSAL)
;;;
;;; This function can probably loop in case of circular structure. We should
;;; associate a max depth to stop the recursion.

(defmossuniversalmethod =make-print-string (&key context)
  "produces a list of strings for printing an entity by-passing model.
Arguments:
   context for printing object in the particular context 
   offset (key): used to offset each string
Return:
   list of strings"
  (format t "~%;+++ =make-print-string (uni)/ obj-id: ~S" *self*)
  (unless context (setq context (symbol-value (intern "*CONTEXT*"))))
  (let* ((prop-list (send *self* '=get-properties))
         prop-id value-list string-list)
    ;; first produce the concept name
    (setq string-list (send (car (send *self* '=get-id '$TYPE)) '=get-name))
    ;; then print each prop in turn
    (while prop-list
      (setq prop-id (pop prop-list))
      ;; if we are processeing a class and property is $PT, then we must gather
      ;; all relevant properties
      (cond
       ((and (%is-model? *self*)
             (eql prop-id '$PT))
        ;; gather all attributes
        (setq value-list 
              (%remove-redundant-properties 
               (%%get-all-class-attributes *self*)))
        (push (send prop-id '=make-print-string value-list) 
              string-list))
       
       ((and (%is-model? *self*)
             (eql prop-id '$PS))
        ;; gather all relations
        (setq value-list 
              (%remove-redundant-properties 
               (%%get-all-class-relations *self*)))
        (push (send prop-id '=make-print-string value-list) 
              string-list))
       
       (t (push (send prop-id '=make-print-string
                      ;; value associated with prop-id
                      (send *self* '=get-id prop-id)
                      )
                string-list))))
    ;(mformat "~%-----")
    (remove nil (reverse string-list) :test #'equal)))

#|
? *language*
:FR
? (send 'albert::$E-person '=make-print-string)
("CONCEPT" ("CONCEPT-NAME" "personne") ("RADIX" ALBERT::$E-PERSON)
 ("ATTRIBUTE " "nom" "prénom")
 ("RELATION " "adresse domicile" "email" "page web" "époux" "épouse" "mère")
 ("COUNTER" ("COUNTER" ("VALUE" 1))))
|#
;;;----------------------------------------------- =MAKE-STANDARD-ENTRY (ATTRIBUTE)

(defmossinstmethod =make-standard-entry MOSS-ATTRIBUTE (data)
  "Makes standard entry points using make-entry. Returns a list of symbols ~
   that will be used as ids for the entry points.
Arguments:
   data: used to build the entry point (may be a list)
Return:
   a list of symbols"
  (%make-entry-symbols data) )  ; be make-name

#|
(send '$pnam '=make-standard-entry '((:en "âge du capitaine" "nombre de printemps")))
(ÂGE-DU-CAPITAINE NOMBRE-DE-PRINTEMPS)

(send '$pnam '=make-standard-entry '((:en "name")( :fr "nom")))
(NAME NOM)
|#
;;;----------------------------------------------------------- =MERGE (ENTRY-POINT)
;;;***** untested

#|
(defmossinstmethod =merge ENTRY-POINT (app-ep &key export)
  "when reloading application from disk, we must merge app entry-points with 
  system entry-points (already active). ***** Uses raw data format.
Arguments:
   app-ep: application entry-point
   export (key): if t we export entry points (normally not)
Returns:
   a single PDM object containing all entry points."
  (let ()
    (dolist (property-list app-ep)
      ;; check first for inverse prop
      (when (%is-inverse-property? (car property-list))
        ;; within each context recover info
        (dolist (world (cdr property-list))
          ;; get context and values
          (setq context (car world))
          ;; add new entry points
          (dolist (ens (cdr world))
            (%make-ep *self* (%inverse-property-id (car property-list)) 
                      ens :export export)))))
    (symbol-value *self*)))
|#

;;;*----------------------------------------------------- =MODIFY-VALUE (ATTRIBUTE)
;;; unused

(defmossinstmethod =modify-value MOSS-ATTRIBUTE (entity-id value)
  "Modify current value - default is to ask for new value.
Arguments:
   entity-id: id of entity whose property must be modified
   value: value to be modified"
  (declare (special *moss-input*))
  (progn
    entity-id ; just to prevent the compiler from yelling at us
    value
    (send *self* '=input-value *moss-input* 
          (format nil "\~&Old value: ~S \~%New value: " value))))

#|
? (send '$t-person-age '=modify-value _jpb 22)
Old value: 22 
New value: 65
65
|#
;;;================================================================================
;;;                                 =NEW Methods
;;;================================================================================

;;; Normally such methods are used to create new objects. Hpwever, they should be
;;; seriously revisited JPB 1001

;;;------------------------------------------------------------- =NEW (OWN:COUNTER)
;;; define own method for counter for creating instances - supersedes
;;; universal method. Uses counter counter
;;; same as above method: is not used for kernel

#|
(defmossownmethod =new (COUNTER HAS-CONCEPT-NAME ENTITY) ()
  "Creates a new instance of counter e.g. $CTR.34 with initial value 0.
Arguments:
  nil
Return:
   the id of the new counter."
  (let*((context (symbol-value (intern "*CONTEXT*")))
        (counter (car (h-> '$CTRS)))
        (value (-> counter '=increment))
        (radix (car (h-> '$RDX)))
        (key (intern (make-name radix #\. value)))
        )
    (set key (copy-list 
              `(($TYPE (,context $CTR))
                ($VALT(,context 0))
                ($XNB (,context ,context))) ))
    key))
|#

#|
? (send '$ctr '=new)
$CTR.1
? $CTR.1
(($TYPE (0 $CTR)) ($VALT (0 0)) ($XNB (0 0)))
|#
;;;-------------------------------------------------------- =NEW (OWN:INVERSE-LINK)
;;; deprecated
;;;--------------------------------------------------------------- =NEW (UNIVERSAL)
;;; unclear that this is useful

#|
(defmossuniversalmethod =new (&rest dummy-list)
  "Creates a new instance using the radix contained in the model ~
   e.g. $PERS.34. Uses =basic-new
Arguments:
   dummy-list (rest): whatever options there are are ignored.
Return:
   the id of a newly created object."
  (progn
    dummy-list ; to prevent the compiler from yelling at us
    (-> *self* '=basic-new)
    ))
|#

#|
? (send _person '=new)
$E-PERSON.2
? $E-PERSON.2
(($TYPE (0 $E-PERSON)) ($ID (0 $E-PERSON.2)))
|#
;;;----------------------------------------------- =NEW-CLASSLESS-KEY (MOSS-SYSTEM)
;;; deprecated

;;;----------------------------------------------------------- =NEW-KEY (UNIVERSAL)
;;; deprecated

;;;----------------------------------------------------- =NEW-VERSION (MOSS-SYSTEM)
;;; =new-version must work in the different packages in particular when there are
;;; OMAS agents.

(defmossinstmethod =new-version MOSS-SYSTEM (&rest option-list)
  "Adding a new version to the system. Takes the last version of the version-graph ~
   adds 1, and forks from current context unless there is an option
        :from old-branching-context
     or
        :from list-of-branching-contexts
     in which cases contexts are checked for validity before anything is done.
Arguments:
   option-list (opt): e.g. (:from 3) (:name \"2015\") (:doc ...)
Return:
   version graph"
  (apply #'%%make-new-version option-list))

#|
(send *moss-system* '=new-version)
;***Warning: changing version and context from 0 to 1
1

(send *moss-system* '=new-version)
;***Warning: changing version and context from 0 to 2
2

(send *moss-system* '=new-version '(:from 1))
;***Warning: changing version and context from 1 to 3
3

*version-graph*
((3 1) (2 1) (1 0) (0))

;; this does not work making versions in MOSS package
(send test::*moss-system* '=new-version)
1

(setq *version-graph* '((0)) *context* 0)
0

(with-package :test
  (send test::*moss-system* '=new-version))
7
|#
;;;--------------------------------------------------------- =NORMALIZE (ATTRIBUTE)
;;; unused

(defmossinstmethod =normalize MOSS-ATTRIBUTE (value)
  "Normally normalizes data values - default is to do nothing.
Arguments:
   value: value to normalize
Return:
   normalized value"
  (-> *self* '=xi value))

;;;----------------------------------------------------- =PARSE-SUMMARY (UNIVERSAL)

(defmossuniversalmethod =parse-summary (val)
  "Used to reverse-engineer the effect of the =summary method. Returns input value ~
   by default."
  val)

;;;================================================================================
;;;                             PRINT Methods
;;;================================================================================

;;; The kernel contains a number of printing methods (too many) that address 
;;; several cases. However, the problem of where to print is not solved properly.
;;; We have several cases:
;;;  - MOSS by itself without the MOSS-window prints to the listener window
;;;  - MOSS with the MOSS window prints to one of the panes of the window
;;;  - MOSS with an SA or XA agent prints to the listener window
;;;  - MOSS with a PA prints to the PA assistant pane of the PA window, but the 
;;;    channels are recorded in the CONVERSATION object
;;; Each case must select the output channel.
;;; Error and warn must also print to the right place.

;;; we use a macro: mformat that in turn uses a local version of *moss-output*
;;; In the moss package:
;;;  - if *moss-output* is T, prints to listener
;;;  - if *moss-output* indicates the MOSS-window, sends a display-text CLOS
;;;    message to the window object
;;; In the application (agent) package:
;;;  - if *moss-output* is T, prints to listener or console (ACL)
;;;  - if *moss-output* is a conversation object, sends a =display-text message to
;;;    the conversation object. When it receives the message it send a display-text
;;;    CLOS message to the window

;;;--------------------------------------------------------- =PRINT-ALL (UNIVERSAL)
;; ; was changed to take keywords. So we patch =print-all

(defmossuniversalmethod =print-all ()
  "Print an entity by-passing print function if present - uses model to get ~
   properties and to print them.
Arguments:
   stream (key): print stream (can be a window pane)
Return
   :done"
  (let* ((prop-list (send *self* '=get-properties)))
    (mformat "~%----- ~S" *self*)
    (while prop-list
      (send (car prop-list)'=print-value
            (send *self* '=get-id (pop prop-list))
            ;  no special header, no fancy separators
            :no-value-flag t ; but print even empty props
            ))
    (mformat "~%-----")
    :done
    ))

#|
(send '$ent '=print-all)
----- $ENT
 MOSS-CONCEPT-NAME: MOSS-CONCEPT
 MOSS-RADIX: $ENT
 MOSS-DOCUMENTATION: 
 MOSS-ONE-OF: 
 MOSS-TRANSACTION-NUMBER: 
 MOSS-TOMBSTONE: 
 MOSS-ATTRIBUTE: MOSS-CONCEPT-NAME/MOSS-CONCEPT, MOSS-RADIX/MOSS-CONCEPT, 
     MOSS-DOCUMENTATION/MOSS-METHOD, MOSS-ONE-OF/MOSS-CONCEPT, 
     MOSS-TRANSACTION-NUMBER/MOSS-COUNTER, MOSS-TOMBSTONE/MOSS-COUNTER
 MOSS-RELATION: MOSS-ATTRIBUTE/MOSS-CONCEPT, MOSS-RELATION/MOSS-CONCEPT, 
     MOSS-IS-A/MOSS-CONCEPT, MOSS-OWN-METHOD/MOSS-CONCEPT, MOSS-INSTANCE-METHOD/MOSS-CONCEPT, 
     MOSS-COUNTER/MOSS-CONCEPT, MOSS-SYSTEM/MOSS-CONCEPT, 
     MOSS-DEFAULT-SUCCESSORS/MOSS-CONCEPT, MOSS-SEQUENCE/MOSS-CONCEPT, 
     MOSS-AND-PART/MOSS-CONCEPT, MOSS-OR-PART/MOSS-CONCEPT, MOSS-XOR-PART/MOSS-CONCEPT
 MOSS-IS-A: 
 MOSS-OWN-METHOD: =GET-INSTANCES (OWN/MOSS-CONCEPT), =GET-INSTANCE-LIST (OWN/MOSS-CONCEPT)
 MOSS-INSTANCE-METHOD: =ADD-ATTRIBUTE-DEFAULT (INST/MOSS-CONCEPT), 
     =ADD-TP-DEFAULT (INST/MOSS-CONCEPT), =GET-INSTANCES (INST/MOSS-CONCEPT), 
     =GET-LAST-INSTANCE (INST/MOSS-CONCEPT), =IF-ADDED (INST/MOSS-CONCEPT), 
     =IF-REMOVED (INST/MOSS-CONCEPT), =INSTANCE-NAME (INST/MOSS-CONCEPT), 
     =INSTANCE-SUMMARY (INST/MOSS-CONCEPT), =PRINT-ALL-INSTANCES (INST/MOSS-CONCEPT), 
     =PRINT-INSTANCE (INST/MOSS-CONCEPT), =PRINT-NAME (INST/MOSS-CONCEPT), 
     =PRINT-TYPICAL-INSTANCE (INST/MOSS-CONCEPT), =SAVE (INST/MOSS-CONCEPT), 
     =SAVE-INSTANCES (INST/MOSS-CONCEPT), =SUMMARY (INST/MOSS-CONCEPT), 
     =GET-INSTANCE-LIST (INST/MOSS-CONCEPT)
 MOSS-COUNTER: 1
 MOSS-SYSTEM: 
 MOSS-DEFAULT-SUCCESSORS: 
 MOSS-SEQUENCE: 
 MOSS-AND-PART: 
 MOSS-OR-PART: 
 MOSS-XOR-PART: 
-----
:DONE

(with-package :test
  (with-context 4
    (send 'test::$E-PERSON.1 '=print-all)))
----- $E-PERSON.1
 name: Barthès
 middle name: 
 email: 
 first name: Jean-Paul
 age: 
 employer: 
 cousin: 
 father: 
 husband: 
 mother: 
 brother: 
 sister: 
 wife: 
-----
:DONE
|#
;;;------------------------------------------------- =PRINT-ALL-INSTANCES (CONCEPT)
;;;*** Should be extended to print also instances of subclasses if desired

(defmossinstmethod =print-all-instances MOSS-ENTITY (&rest option-list)
  "Print instances of a class - default is to print them all using ~
   the =summary method - However one can print only a range using the ~
   option (:range min max) - one can also specify that we want to ~
   use the =print-instance function using the option (:full-print)
    Does not work for models ($ent, $ept, $eps, ...)
Arguments:
   option-list (opt): Options are:
      (:range min max)
Return:
   :done"
  (let*((context (symbol-value (intern "*CONTEXT*")))
        (cmin (if (assoc :range option-list)
                  (cadr (assoc :range option-list))
                1))
        (cmax (if (assoc :range option-list)
                  (1+ (caddr (assoc :range option-list)))
                most-positive-fixnum))
        (radix (car (=> *self* '=has-value 'HAS-MOSS-RADIX)))
        (counter (car(=> (car (h==> 'HAS-MOSS-COUNTER)) '=has-value 'HAS-MOSS-VALUE)))
        (*moss-output* (if (or (not (boundp '*moss-output*))
                               (null *moss-output*)) t *moss-output*))
        id)
    (setq cmax (min counter cmax))
    (mformat "~%-----")
    (while (< cmin cmax)
           (setq id (intern (make-name radix '#\. cmin)))
           (when (%alive? id context)
             (if (assoc :full-print option-list)
                 (send *self* '=print-instance id)
               (mformat "~%~{~S ~}" `(,cmin - ,@(send id '=summary)))
               ))
           (incf cmin))
    (mformat "~%-----")
    :done))

#|
(with-package :test
  (send test::_person '=print-all-instances))
-----
1 - "Barthès: Jean-Paul" 
2 - "Barthès-Biesel Barthès: Dominique" 
3 - "Barthès: Camille" 
6 - "Barthès: Nicolas" 
7 - "Zorro: *unknown first name*" 
-----
:DONE

(with-package :test
  (with-context 2
    (send test::_person '=print-all-instances)))
-----
1 - "Barthès: Jean-Paul" 
2 - "Barthès-Biesel Barthès: Dominique" 
4 - "Barthès: Camille" 
5 - "Streit Barthès: Peggy" 
-----
:DONE

(with-package :test
  (with-context 2
    (send test::_person '=print-all-instances '(:range 2 4))))
-----
2 - "Barthès-Biesel Barthès: Dominique" 
4 - "Barthès: Camille" 
-----
:DONE
|#
;;;--------------------------------------------- =PRINT-AS-METHOD-FOR (ENTRY-POINT)
;;; very confusing when there are many methods with the same name like =make-entry

(defmossinstmethod =print-as-method-for MOSS-ENTRY-POINT ()
  "Looks at entry-point (*self*) for possible name of a method ~
   If so - for each object prints instance method and local method ~
   corresponding to the name if any.
Arguments:
   nil
Return:
   :done"
  (let ((object-list 
         (%get-value *self* (%inverse-property-id '$MNAM)))
        )
    ;(format t "~%; =print-as-method-for /object-list: ~%  ~S" object-list)
    
    ;; collect all objects whose self is entry-point
    (dolist (object-id  (delete-duplicates object-list))
      (mformat "~2%")
      (send object-id '=print-self))
    
    (setq object-list (%get-value *self* (%inverse-property-id '$UNAM)))
    (format t "~%; =print-as-method-for /object-list: ~%  ~S" object-list)
    
    (dolist (object-id  (delete-duplicates object-list))
      (mformat "~2%")
      (send object-id '=print-self))
    :done))

#|
(send '=xi '=print-as-method-for)
----- =XI
Function: 
----- $EPT=I=0=XI
(LAMBDA (DATA)
  "Normally normalizes data values. Checks value restriction conditions ~
   associated with the terminal property.
Arguments:
   data: list of values to be checked.
Return:
   data if OK, nil otherwise"
  (CATCH :RETURN
    (LET ((RESTRICTION-LIST (HAS-MOSS-VALUE-RESTRICTION)) ERROR-FLAG)
      (DOLIST (RESTRICTION RESTRICTION-LIST)
        (UNLESS (%VALIDATE-TP-VALUE RESTRICTION DATA) (SETQ ERROR-FLAG T) (RETURN NIL)))
      (UNLESS ERROR-FLAG DATA))))


----- =XI
Function: 
----- $CREFT=S=0=XI
(LAMBDA (VALUE)
  "when the value has the right type we return it, otherwise we return nil."
  (CATCH :RETURN (IF (%TYPE? VALUE (CAR (HAS-MOSS-CLASS-REFERENCE))) VALUE)))
; =print-as-method-for /object-list: 
  NIL
:DONE
|#
;;;----------------------------------------------------------- =PRINT-CODE (METHOD)

(defmossinstmethod =print-code MOSS-METHOD ()
  "Prints Function code in pretty format
Arguments:
   none
Return:
   :done"
  (let* ((name (car (HAS-MOSS-FUNCTION-NAME)))
        (code (symbol-value name)))
    (mformat "~%----- ~S~%~S" name code)
    :done))

#|
(send '$FN.39 '=print-code)
----- $EPT=I=0=GET-NAME
(LAMBDA NIL
  "Returns the name of an instance $ENAM or $PNAM"
  (CATCH :RETURN (SEND-NO-TRACE *SELF* '=INSTANCE-NAME)))
:DONE
|#
;;;------------------------------------------------------------ =PRINT-DOC (METHOD)

(defmossinstmethod =print-doc MOSS-METHOD (&key stream)
  "Prints Function name and associated documentation
Arguments:
   stream (key): printing stream default t
Return:
   :done"
  (declare (ignore stream))
  (let ()
    (mformat "~%~A" (car (g-> '$DOCT)))
    :done
    ))

#|
(send '$FN.39 '=print-doc)
Returns the name of an instance $ENAM or $PNAM
:DONE
|#
;;;---------------------------------------------- =PRINT-DOCUMENTATION (UNIVERSAL)

(defmossuniversalmethod =print-documentation (&key (stream *moss-output*) no-summary
                                                   (lead "") final-new-line erase
                                                   &allow-other-keys)
  "prints object documentation or sorry message.
Arguments:
   stream (key): printing stream (default is *moss-output*)
   erase (key): if t and stream is a pane, then erases the pane
   no-summary (key): if t does not print a summary of the object
   final-new-line (key): if t prints a final new line
   lead (key): if t prints a leading header
Return:
   :done"
  (let ((doc (send *self* '=get-documentation :no-summary no-summary :lead lead
                   :final-new-line final-new-line)))
    ;; print doc, using the CLOS display-text method
    (if (and stream (not (eql stream t)))
      (display-text stream doc :erase erase)
      (format t doc))
    :done
    ))

#|
(send '$FN.39 '=print-documentation)
=GET-NAME (INST/MOSS-ATTRIBUTE) : Returns the name of an instance $ENAM or $PNAM
:DONE
|#
;;;*----------------------------------------------------- =PRINT-ERROR (UNIVERSAL)
;;; not too useful

(defmossuniversalmethod =print-error (stream msg &rest arg-list)
  "Default method for printing errors - uses LISP format primitive directly.
Arguments:
   stream: printing stream
   msg: format string
   arg-list (rest): args to format string"
  (let ((*moss-output* stream))
    (if (stringp msg)
      (mformat "~%;***Error: ~?" msg arg-list)
      (mformat "~%;***Error:~{ ~A~}" msg))
    ))

;;;----------------------------------------------------- =PRINT-HISTORY (UNIVERSAL)

(defmossuniversalmethod =print-history ()
  "Print an entity use a primitive function - For each property print all
    values up to the root of the situation lattice.
Arguments:
   stream (key): printing stream
Return:
   not significant"
  (%pep *self* :stream *moss-output*)
  )

#|
(with-package :test
  (with-context 5
    (send test::_person '=print-history)))
$E-PERSON
----------
MOSS-TYPE:                    t0: MOSS::$ENT
MOSS-IDENTIFIER:              t0: $E-PERSON
----- Attributes
MOSS-CONCEPT-NAME:            t0: ((:EN "PERSON"))
MOSS-RADIX:                   t0: $E-PERSON
MOSS-DOCUMENTATION:           t0: ((:EN "no doc"))
----- Relations
MOSS-COUNTER:                 t0: $E-PERSON.CTR
MOSS-ATTRIBUTE:               t0: $T-PERSON-NAME, $T-PERSON-MIDDLE-NAME, $T-PERSON-EMAIL,                                   $T-PERSON-FIRST-NAME,                                   $T-PERSON-AGE
MOSS-RELATION:                t0: $S-PERSON-EMPLOYER, $S-PERSON-COUSIN, $S-PERSON-FATHER,                                   $S-PERSON-HUSBAND,                                   $S-PERSON-MOTHER,                                   $S-PERSON-BROTHER,                                   $S-PERSON-SISTER,                                   $S-PERSON-WIFE
MOSS-INSTANCE-METHOD:         t0: $E-FN.10
-----Inv-Links
IS-MOSS-ENTITY-LIST-OF:       t0: $SYS.1
IS-MOSS-SUCCESSOR-OF:         t0: $S-ORGANIZATION-PRESIDENT, $S-PERSON-COUSIN,                                   $S-PERSON-FATHER,                                   $S-PERSON-HUSBAND,                                   $S-PERSON-MOTHER,                                   $S-PERSON-BROTHER,                                   $S-PERSON-SISTER,                                   $S-PERSON-WIFE,                                   $S-STUDENT-TEACHER
IS-MOSS-IS-A-OF:              t0: $E-STUDENT
----------
:DONE

(with-package :test
  (with-context 5
    (send test::_jpb '=print-history)))
$E-PERSON.1
----------
MOSS-TYPE:                    t0: $E-PERSON
MOSS-IDENTIFIER:              t0: $E-PERSON.1
----- Attributes
NAME:                         t0: "Barthès"
FIRST-NAME:                   t0: "Jean-Paul"
----- Relations
-----Inv-Links
IS-HUSBAND-OF:                t0: $E-PERSON.2
IS-PRESIDENT-OF:              t5: $E-ORGANIZATION.1
----------
:DONE
|#
;;;------------------------------------------------------ =PRINT-INSTANCE (CONCEPT)

(defmossinstmethod =print-instance MOSS-ENTITY (instance-id)
  "Print an instance using model to get list of properties
Arguments:
   instance-id: id of instance to print
   stream (key): printing channel
Return:
   nil"
  (let ((prop-list (send instance-id '=get-properties)))
    (while prop-list
      (send (car prop-list)'=print-value
            (send instance-id '=get-id (car prop-list)))
      (pop prop-list))
    (mformat "~%-----")
    :done))

#|
(with-package :test
  (with-context 5
    (send test::_person '=print-instance  test::_jpb)))
 name: Barthès
 first name: Jean-Paul
-----
:DONE
|#
;;;----------------------------------------------- =PRINT-LOCAL-METHODS (UNIVERSAL)
;;; Here print methods associated with the object receiving the message
;;; Uses ANSI screen codes
;;; *****untested

(defmossuniversalmethod =print-local-methods (&key (stream *moss-output*))
  "Prints all methods and doc associated with a given entity
Arguments:
   stream (key): printing stream
Return:
   not significant"
  (let*((own-method-list (h-> '$OMS))
        (method-list (set-difference (g-> '$OMS) own-method-list))
        (model-id (car (g-> '$TYPE)))
        (instance-method-list (when (not (%is-orphan? *self*))
                                (-> model-id '=get-id '$IMS)))
        (*moss-output* stream))
    ;; print own methods
    (when own-method-list
      (mformat "~2%LOCAL OWN METHODS~%~
                =================")
      (dolist (method own-method-list)
        (mformat "~2%~A" (car (-> method '=get-name)))
        (-> method '=print-doc :stream stream)))
    
    ;; print now inherited methods
    (when method-list
      (mformat "~2%INHERITED OWN METHODS~%~
                =====================")
      (dolist (method method-list)
        (mformat "~2%~A" (car (-> method '=get-name)))
        (-> method '=print-doc :stream stream)))
    
    ;; print now instance methods
    (when instance-method-list
      (mformat "~2%INSTANCE METHODS OBTAINED FROM CLASS~%~
                ====================================")
      (dolist (method instance-method-list)
        (mformat "~2%~A" (car (-> method '=get-name)))
        (-> method '=print-doc :stream stream)))    
    :done ))

#|
(send '$SYS '=print-local-methods)


INSTANCE METHODS OBTAINED FROM CLASS
====================================

=ADD-ATTRIBUTE-DEFAULT
Adds a default value for a given attribute of a class. The default is added  ~
   to the ideal, replacing whatever value was present. 
Arguments:
   att-ref: symbol or (multilingual) string specifying the attribute
   value:the default value to be added to the ideal
Return:
   the internal representation of the modified ideal.

=ADD-TP-DEFAULT
Deprecated. Use =add-attribute-default.

=GET-INSTANCES
get a list of instances of the corresponding class. Uses the counter to build ~
   the instance names (including the ideal)
...

=SUMMARY
Return in a list the first name of the concept in the current language

=GET-INSTANCE-LIST
Get the explicit list of instances corresponding to a given user class.~
   If the global variable *query-allow-sub-classes* is true adds instances ~
   of the spanned sub-classes.
:DONE


(with-package :test
  (with-context 5
    (send test::_person '=print-local-methods)))

INSTANCE METHODS OBTAINED FROM CLASS
====================================

=ADD-ATTRIBUTE-DEFAULT
Adds a default value for a given attribute of a class. The default is added  ~
   to the ideal, replacing whatever value was present. 
Arguments:
   att-ref: symbol or (multilingual) string specifying the attribute
   value:the default value to be added to the ideal
Return:
   the internal representation of the modified ideal.

=ADD-TP-DEFAULT
Deprecated. Use =add-attribute-default.

=GET-INSTANCES
get a list of instances of the corresponding class. Uses the counter to build ~
   the instance names (including the ideal)

...

=SAVE-INSTANCES
save all instances of a concept into the database including ideal.
Arguments:
   none.
Return:
   relation-id.

=SUMMARY
Return in a list the first name of the concept in the current language

=GET-INSTANCE-LIST
Get the explicit list of instances corresponding to a given user class.~
   If the global variable *query-allow-sub-classes* is true adds instances ~
   of the spanned sub-classes.
:DONE
|#
;;;----------------------------------------------------- =PRINT-METHODS (UNIVERSAL)
;;; here we may have a discrepancy between method-names inherited
;;; depth first through the =get mechanism and the special inheritance
;;; mechanism of inherited methods (=inherit-own and =inherit-instance
;;; this will have to be solved in the future
;;; Currently there is no way to find out the names of all methods that 
;;; could be possibly inherited, other than trying every known method name
;;; (even with universal methods since they could be shadowed by own methods)
;;; on object
;;; 89/12/28
;;; We modify =print-methods so that it prints the instance methods of the 
;;; object receiving the message. The rationale is that the user wants to
;;; know the methods s/he can use for the instances of any given model
;;; and is asking the model for it.

(defmossuniversalmethod =print-methods (&key (stream *moss-output*))
  "When sent to a class, prints all methods and doc used by instances of such
    a class. Ignores methods redefined locally in instances.
Arguments:
   stream (opt): channel on which to print (default t)
Return:
   :done"
  (let* ((instance-method-list (-> *self* '=get-id '$IMS))
         (*moss-output* stream))
    ;; print now instance methods
    (when instance-method-list
      (mformat "~2%LOCAL INSTANCE METHODS~%~
                ======================")
      (while instance-method-list
        (mformat "~2%~A" (car (-> (car instance-method-list) '=get-name)))
        (-> (car instance-method-list) '=print-doc :stream stream)
        (pop instance-method-list))
      )
    ;; Now try to inherit other methods from superclasses
    (setq instance-method-list 
          (reduce 'append 
                  (broadcast (delete *self* (%sp-gamma *self* '$IS-A))
                             '=has-value-id '$IMS)))
    ;; and do it again
    (when instance-method-list
      (mformat "~2%INHERITED INSTANCE METHODS~%~
                ==========================")
      (while instance-method-list
        (mformat "~2&~A" (car (-> (car instance-method-list) '=get-name)))
        (-> (car instance-method-list) '=print-doc :stream stream)
        (pop instance-method-list))
      )
    ;;... the end 
    :done))

#|
(with-package :test
  (with-context 2
    (send test::_person '=print-methods)))

LOCAL INSTANCE METHODS
======================

=SUMMARY
NIL
:DONE

|#
;;;---------------------------------------------------------- =PRINT-NAME (CONCEPT)

(defmossinstmethod =print-name MOSS-ENTITY (&key (stream *moss-output*))
  "Print entity name
Arguments:
   stream (opt): channel on which to print (default t: listener)
Return:
   :done"
  (let ((*moss-output* stream))
    (mformat "~&~{~A ~}" (send *self* '=get-name) )))

#|
(with-package :test
  (with-context 2
    (send test::_person '=print-name)))
PERSON
NIL
|#
;;;------------------------------------------------------ =PRINT-OBJECT (UNIVERSAL)

(defmossuniversalmethod =print-object (&key no-inverse (stream *moss-output*))
  "Print any object by sending a =print-value message to all properties.
Arguments:
   no-inverse (key): if true suppresses the printing of inverse properties
   stream (key): allows to redirect printing towards a specific stream (default t).
Return:
   :done"
  (let((entity-l (symbol-value *self*))
       (*moss-output* stream))
    (mformat "~%----- ~A" *self*)
    (while entity-l
      (if (not (and no-inverse 
                    (%is-inverse-property? (caar entity-l))))
        (send (caar entity-l) '=print-value (g-> (caar entity-l))))
              ;:stream stream))
      (pop entity-l))
    (mformat "~%-----")
    :done
    ))

#|
(with-package :test
  (with-context 5
    (send test::_cb '=print-object)))
----- $E-PERSON.3
 MOSS-TYPE: $E-PERSON
 MOSS-IDENTIFIER: $E-PERSON.3
 name: Barthès
 first name: Camille
-----
:DONE

(with-package :test
  (with-context 5
    (send test::_jpb '=print-object)))
----- $E-PERSON.1
 MOSS-TYPE: $E-PERSON
 MOSS-IDENTIFIER: $E-PERSON.1
 name: Barthès
 first name: Jean-Paul
 IS-HUSBAND-OF: Barthès-Biesel Barthès: Dominique
 IS-PRESIDENT-OF: $E-ORGANIZATION.1
-----
:DONE
|#
;;;------------------------------------------------------ =PRINT-SELF (ENTRY-POINT)

(defmossinstmethod =print-self MOSS-ENTRY-POINT () 
  "Print objects corresponding to an entry point.
Arguments
   stream (key): output stream or pane (default t)
Return:
   :done"
  (let ((prop-list (delete-duplicates (send *self* '=has-inverse-properties))))
    (while prop-list
      (when (and (%is-inverse-property? (car prop-list))
                 (not (eql (car prop-list) (%inverse-property-id '$EPLS))))
        (broadcast (%get-value *self* (car prop-list)) '=print-self))
      (pop prop-list))
    :done
    ))

#|
(with-package :test
  (with-context 5
    (send 'test::barthès '=print-self)))
----- $E-PERSON.1
 name: Barthès
 first name: Jean-Paul
-----
----- $E-PERSON.2
 name: Barthès-Biesel, Barthès
 first name: Dominique
 husband: Barthès: Jean-Paul
-----
----- $E-PERSON.3
 name: Barthès
 first name: Camille
-----
----- $E-STUDENT.2
 name: Barthès
 first name: Lucie
-----
----- $E-PERSON.6
 name: Barthès
 first name: Nicolas
-----
:DONE
|#
;;;*---------------------------------------------------------- =PRINT-SELF (METHOD)

(defmossinstmethod =print-self MOSS-METHOD ()
  "Impression d'une methode universelle
Arguments
   stream (key): output stream or pane (default t)
Return:
   :done"
  (let ()
    (mformat "~%----- ~A" (car (-> *self* '=get-name)))
    (mformat "~%Function: ")
    (send *self* '=print-code)
    :done
    ))

#|
(send '$FN.63 '=print-self)
----- =MAKE-STANDARD-ENTRY
Function: 
----- $EPT=I=0=MAKE-STANDARD-ENTRY
(LAMBDA (DATA)
  "Makes standard entry points using make-entry. Returns a list of symbols ~
   that will be used as ids for the entry points.
Arguments:
   data: used to build the entry point (may be a list)
Return:
   a list of symbols"
  (CATCH :RETURN (%MAKE-ENTRY-SYMBOLS DATA)))
:DONE

(with-package :test
  (with-context 5
    (send 'test::$E-FN.2 '=print-self)))
----- =SUMMARY
Function: 
----- $E-OMAS-GOAL=I=0=SUMMARY
(LAMBDA NIL (CATCH :RETURN (SEND *SELF* '=GET "gl-name")))
:DONE
|#
;;;-------------------------------------------------------- =PRINT-SELF (UNIVERSAL)

(defmossuniversalmethod =print-self (&key context)
  "Print an entity by-passing print function if present - uses ~
   model to get properties and to print them.
Arguments:
   context for printing object in the particular context 
Return:
   :done"
  (with-context (or context (symbol-value (intern "*CONTEXT*")))
    (let* ((prop-list (send *self* '=get-properties))
           ;(type-list (remove '*none* (HAS-MOSS-TYPE)))
           prop-id value-list)
      (mformat "~%----- ~A" *self*)
      (while prop-list
             (setq prop-id (pop prop-list))
             ;; if we are processing a class and property is $PT, then we must gather
             ;; all relevant properties
             (cond
              ((and (%is-model? *self*)
                    (eql prop-id 'moss::$PT))
               ;; gather all attributes
               (setq value-list 
                     (%remove-redundant-properties 
                      (%%get-all-class-attributes *self*)))
               (send prop-id '=print-value value-list))
              ((and (%is-model? *self*)
                    (eql prop-id 'moss::$PS))
               ;; gather all relations
               (setq value-list 
                     (%remove-redundant-properties 
                      (%%get-all-class-relations *self*)))
               (send prop-id '=print-value value-list))
              (t (send prop-id '=print-value
                       ;; list of values associated with prop-id
                       (send *self* '=get-id prop-id)))))
      (mformat "~%-----")
      :done)))

#|
(with-package :test
  (with-context 2
    (send 'test::$e-student.1 '=print-self)))
----- $E-STUDENT.1
 name: Streit
 first name: John, Jean-Paul
-----
:DONE
|#
;;;*--------------------------------------------- =PRINT-TYPICAL-INSTANCE (CONCEPT)

(defmossinstmethod =print-typical-instance MOSS-ENTITY () 
  "Prints a typical instance of a class, corresponding to its ideal, i.e., the ~
   instance bearing all the default attributes."
  (send (%make-id-for-ideal *self*) '=print-self))

#|
? (send _person '=print-typical-instance)
----- $E-PERSON.0
 Age: 20
-----
:DONE
|#
;;;------------------------------------------------------- =PRINT-VALUE (ATTRIBUTE)

(defmossinstmethod =print-value MOSS-ATTRIBUTE 
  (value-list &key header no-value-flag)
  "Print values associated to property - A property ~
   can uses =print-value for a special formatting.
Arguments:
   value-list: values associated to attribute
   stream (key): stream or pane (default t)
   header (key): title to use instead of attribute name
   no-value-flag (key): if t print even if value-list is nil
Returns:
   nil"
  ;; if no value present, then do not print anything
  (let ()
    (if (or value-list no-value-flag)  
      (mformat "~%~VT~A: ~{~S~^, ~}"
               *left-margin*
               (if header header (car (-> *self* '=get-name)))
               (mapcar #'(lambda (xx) (-> *self* '=format-value xx))
                       value-list)))
    nil))

#|
(send test::_has-age '=print-value '(34) :header "L'âge du capitaine ")
 L'âge du capitaine : 34
NIL
|#
;;;---------------------------------------------------- =PRINT-VALUE (INVERSE-LINK)
;;; We add this method to inverse links to be able to print entry points or
;;; objects linked to other object through inverse links. Remember inverse
;;; links have no semantic meaning and are simply used to navigate on the
;;; links in the reverse direction.
;;; E.g. if a person has an inverse link IS-BROTHER-OF onto another person, then
;;; al this means is that there is an object in the KB which has this object
;;; as a brother, but this does not mean that this object is the BROTHER of the
;;; other object. Subtle but important for locking mechanisms.

(defmossinstmethod =print-value MOSS-INVERSE-LINK (suc-list &key header no-value-flag)
  "Prints a summary of all linked entities.
Arguments:
   suc-list: list of successors
   header (key): value replacing inverse property name
   no-value-flag (key): if there print even with no value "
  (let ()
    (if (or suc-list no-value-flag)
      (mformat "~%~VT~A: ~{~<~%     ~1:;~{~S~^ ~}~>~^, ~}"
               *left-margin*
               (if header header (car (send *self* '=get-name)))
               (mapcar #'(lambda (xx) (=> xx '=summary))
                       suc-list)))
    nil))

#|
(send 'test::$S-BROTHER.OF '=print-value (list test::_jpb))
 IS-BROTHER-OF: Barthès: Jean-Paul
NIL
|#
;;;-------------------------------------------------------- =PRINT-VALUE (RELATION)

(defmossinstmethod =print-value MOSS-RELATION (suc-list &key header no-value-flag)
  "Prints a summary of all linked entities.
Arguments:
   successors: list of successors
   stream (key): stream or pane (default t)
   header (key): title to use instead of attribute name
   no-value-flag (key): if t print even if value-list is nil
Returns:
   nil"
  (let ()
    (if (or suc-list no-value-flag)
      (mformat "~%~VT~A: ~{~<~%     ~1:;~{~S~^ ~}~>~^, ~}"
               *left-margin*
               (if header header (car (-> *self* '=get-name)))
               (mapcar #'(lambda (xx) (-> xx '=summary))
                       suc-list)))
    nil))

#|
(send test::_has-brother '=print-value (list test::_jpb) :eader "Les Frères ")
 Les Frères : Barthès: Jean-Paul
NIL
|#

;;;============================== End print methods ===============================

;;;-------------------------------------------------- =REMOVE-ATTRIBUTE (UNIVERSAL)

(defmossuniversalmethod =remove-attribute (att-ref)
  "deletes all values associated with a particular attribute and removes the ~
   attribute from the object, making it locally unknown rather than empty.
Arguments:
   att-ref: reference of the attribute to remove
Return:
   the internal representation of the updated object."
  ;; class-id is the first class of the object in case of multiple class belonging
  (let* ((class-id (car (%type-of *self*)))
         (att-id (%%get-id att-ref :attribute :class-ref class-id))
         )
    (remove att-id (delete-values *self* att-id :all :no-warning t) :key #'car)
    ))

#|
p
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1))
 ($T-PERSON-NAME (0 "Jim" "Joe" "Arthur"))
 ($T-PERSON-SEX (0 "male")))

(SEND P '=REMOVE-ATTRIBUTE "sex")
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1))
 ($T-PERSON-NAME (0 "Jim" "Joe" "Arthur")))
|#
;;;*---------------------------------------------------------- =REPLACE (UNIVERSAL)

(defmossuniversalmethod =replace (prop-name value-list)
  "Replaces the value-list with a new one in the current object. Must find the ~
   right property object from the property name. When the value-list is empty ~
   amounts to a delete-all.
Arguments:
   prop-name: name of property, i.e. HAS-TITLE, \"title\", or (:en \"title\")
   value-list: list of values replacing the old ones
Return:
   not significant"
  (if (or (not (listp value-list))
          (mln::mln? value-list)
          (mln::%mln? value-list))
      (setq value-list (list value-list)))
  (delete-values *self* prop-name :all :no-warning t)
  (add-values *self* prop-name value-list :no-warning t)
  )

#|
(send test::_jpb '=add-attribute-values "age" '(40))
(with-package :test
  (with-context 5
    (send test::_jpb '=print-self)))
 $E-PERSON.1
 name: Barthès
 first name: Jean-Paul
 age: 40
-----

(with-package :test
  (with-context 5
     (send test::_jpb '=replace "age" '(70))))
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.1))
 (TEST::$T-PERSON-NAME (0 "Barthès")) (TEST::$T-PERSON-FIRST-NAME (0 "Jean-Paul"))
 (TEST::$S-PERSON-HUSBAND.OF (0 TEST::$E-PERSON.2))
 (TEST::$S-ORGANIZATION-PRESIDENT.OF (5 TEST::$E-ORGANIZATION.1))
 (TEST::$T-PERSON-AGE (5 70)))
|#
;;;================================================================================
;;;
;;;                               =SAVE methods
;;;
;;;================================================================================

;;; the following methods save objects into persistency store unless thet have a
;;; :no-save marker on their property list (system objects in particular should not
;;; be saved). They use the db-XXX function of the persistency interface.
;;; The database must be opened and its partitions must contain one corresponding
;;; to the package of the object being stored.
;;; Only one database is opened for the whole Lisp environment (normally bearing
;;; the name of the application, e.g. NEWS.ODB)

;;;-------------------------------------------------------------- =SAVE (ATTRIBUTE)

(defmossinstmethod =save MOSS-ATTRIBUTE ()
  "saves an attribute into the database. Must also save the entry function.
Arguments:
   database-pathname: handle to get into the database.
Return:
   attribute-id."
  (unless (get *self* :no-save)
      (moss::db-store *self* (symbol-value *self*) 
                      (intern (package-name *package*) :keyword)))
    *self*)

#|
(send 'address::$T-name '=save)
|#
;;;---------------------------------------------------------------- =SAVE (CONCEPT)

(defmossinstmethod =save MOSS-CONCEPT ()
  "saves a concept into the database.
Arguments:
   database-pathname: handle to get into the database.
Return:
   concept-id."
  (unless (get *self* :no-save)
    (moss::db-store *self* (symbol-value *self*) 
                    (intern (package-name *package*) :keyword)))
  *self*)

#|
? (in-package :address)
? (setq database-pathname)
#<IO WOOD:PHEAP to
#P"Odin:Users:barthes:MCL:OMAS-MOSS 8.0:OMAS:applications:ODIN-HDSRI:ADDRESS-DATABASE.wood">
? (send '$E-PERSON '=save)
|#
;;;---------------------------------------------------------------- =SAVE (COUNTER)
;;; counters should not be versioned and kept in context 0

(defmossinstmethod =save MOSS-COUNTER ()
  "saves a counter into the database. If :reset is t on the plist, resets the value ~
   to 1 (used for agent, skill and goal counters).
Arguments:
   database-pathname: handle to get into the database.
Return:
   concept-id."
  (with-context 0
    (unless (get *self* :no-save)
      (format t "~%; =save (COUNTER)/ counter: ~S, reset: ~S" 
        *self* (get *self* :reset))
      (if (get *self* :reset)
          (moss::db-store *self* 
                          (cons `($VALT (0 1))
                                (remove '$VALT (symbol-value *self*) :key #'car))
                          (intern (package-name *package*) :keyword))
        (moss::db-store *self* (symbol-value *self*) 
                        (intern (package-name *package*) :keyword))))
    *self*))

;;;------------------------------------------------------------ =SAVE (ENTRY POINT)
;;; we may have problems with entry points defined in the MOSS package and shared 
;;; in the application package

(defmossinstmethod =save MOSS-ENTRY-POINT ()
  "save an entry point into the database after stripping it from references to ~
   objects not in the required package or to objects not to be saved.
   In core value of the entry object is not modified.
Arguments:
   none
Return:
   concept-id."
  (unless *application-package*
    (mformat "~%Ontology package not defined (*application-package*) while ~
              attempting to save entry-points.")
    (throw :return :error))
  
  (unless (get *self* :no-save)
    ;(format t "~% =save (ep) 1/ *self*: ~S ~%~S" *self* (symbol-value *self*))
    (let ((new-entry 
           (%%strip-entry-point *self* (find-package *application-package*))) ;JP1005
          ;(old-package (symbol-package *self*))
          ;(good-package (eql (symbol-package *self*) *package*))
          )
      ;(format t "~% =save (ep) 2/ *self*: ~S ~%~S" *self* (symbol-value *self*))
      ;; new entry the stripped value of the ep
      ;; when *self* has a MOSS symbol package and we are saving in a different
      ;; package we must cook up a local key: does not work with intern when the
      ;; symbol is inherited
      (db-store (intern (symbol-name *self*)) new-entry 
                (intern (package-name *package*) :keyword))
      ))
  *self*)

;;;------------------------------------------------------- =SAVE (INVERSE-RELATION)

(defmossinstmethod =save MOSS-INVERSE-LINK ()
  "saves an inverse relation into the database.
Arguments:
   database-pathname: handle to get into the database.
Return:
   inverse-relation-id."
  (unless (get *self* :no-save)
    (moss::db-store *self* (symbol-value *self*)
                    (intern (package-name *package*) :keyword)))
  *self*)

#|
(send 'address::$S-ADDRESS '=save)
|#
;;;----------------------------------------------------------------- =SAVE (METHOD)

(defmossinstmethod =save MOSS-METHOD ()
  "saves a method into the database. Must also save the function.
Arguments:
   database-pathname: handle to get into the database.
Return:
   method-id."
  (unless (get *self* :no-save)
    (let (fname)
      (moss::db-store *self* (symbol-value *self*)
                      (intern (package-name *package*) :keyword))
      ;; get the function name
      (setq fname (car (%get-value *self* '$FNAM)))
      (format t "~%; (method) =save /fname: ~S, *package*: ~S, p(f): ~S"
        fname (symbol-package fname) *package*)
      (cond
       ;; test for bad values
       ((or (null fname) (not (symbolp fname)))
        (warn "bad function name when trying to save ~S" *self*))
       ;; save only the functions defined in the application package
       ((eql (symbol-package fname) *package*)
        (moss::db-store fname (symbol-value fname)
                        (intern (package-name *package*) :keyword)))
       )))
  *self*)

#|
(send 'address::$S-ADDRESS '=save)
|#
;;;--------------------------------------------------------------- =SAVE (RELATION)

(defmossinstmethod =save MOSS-RELATION ()
  "saves a relation into the database.
Arguments:
   database-pathname: handle to get into the database.
Return:
   relation-id."
  (unless (get *self* :no-save)
    (moss::db-store *self* (symbol-value *self*)
                    (intern (package-name *package*) :keyword)))
  *self*)

#|
(send 'address::$S-ADDRESS '=save (omas::base address::SA_address))
|#
;;;----------------------------------------------------------------- =SAVE (SYSTEM)
#|
(db-close *database-pathname*)
(db-print-all-objects *database-pathname*)
(setq family::dp *database-pathname*)
|#

(defmossinstmethod =save MOSS-SYSTEM (&key erase)
  "saves ontology and KB from the current package by expunging objects defined ~
   in other packages including the MOSS package. If the database does not exist ~
   creates it, otherwise if the new option is t issues a warning and quit.
Arguments:
   database-name: the filename containing the object database
   erase (key): if t, we want to clear database. Dangerous.
Return:
   :objects-saved or :error"
  (let (concept-list virtual-concept-list attribute-list relation-list method-list
                     inverse-relation-list variable-list function-list ep-list
                     area)
    ;(break "===> =save (SYSTEM)")
    
    ;; check first if the database is opened
    (unless *database-pathname*
      (mformat "~%Database should be created/opened first...")
      (throw :return :no-database))
    
    ;; the next option should be used carefully...
    (when erase
      ;; determine the keyword specifying the partition to erase
      (setq area (intern (package-name *application-package*) :keyword))
      (mformat "~%*** We clear the ~S partition of the ~S database"
               area (pathname-name *database-pathname*))
      (db-clear-all area))
    
    ;;===== Now save the world...
    
    (let* ((*package* (find-package *application-package*))
           (partition (intern (package-name *package*) :keyword)))
      
      ;; first save ontology package name
      (db-store :ontology-package (package-name *package*) partition)
      (mformat "~%; database saving ontology package: ~S" (package-name *package*))
      
      ;;=== concepts (classes)
      (setq concept-list (%get-value *self* '$ENLS))
      (mformat "~%; database saving ~S concepts..." (length concept-list))
      (dolist (concept-id concept-list)
        (send concept-id '=save))
      
      ;;=== virtual concepts
      (setq virtual-concept-list (%get-value *self* '$VELS))
      (mformat "~%; database saving ~S virtual concepts..." 
               (length virtual-concept-list))
      (dolist (virtual-concept-id virtual-concept-list)
        (send virtual-concept-id '=save))
      
      ;;=== attributes
      (setq attribute-list (%get-value *self* '$ETLS))
      (mformat "~%; database saving ~S attributes..." (length attribute-list))
      (dolist (attribute-id attribute-list)
        (send attribute-id '=save))
      
      ;;=== relations
      (setq relation-list (%get-value *self* '$ESLS))
      (mformat "~%; database saving ~S relations..." (length relation-list))
      (dolist (relation-id relation-list)
        (send relation-id '=save))
      
      ;;=== inverse relations
      (setq inverse-relation-list (%get-value *self* '$EILS))
      (mformat "~%; database saving ~S inverse relations..." 
               (length inverse-relation-list))
      (dolist (inverse-relation-id inverse-relation-list)
        (send inverse-relation-id '=save))   
      
      ;;=== orphans are instances of *none*, thus nothing special
      
      ;;=== instances
      (mformat "~%; database saving individuals...")
      (dolist (concept-id concept-list)
        ;; to avoid looping...
        (unless (%is-a? concept-id '$SYS)
          (send concept-id '=save-instances)))
      
      ;; however, we must save the system description
      (moss::db-store (intern "$SYS.1") (symbol-value (intern "$SYS.1"))
                      partition)
      
      ;;=== methods
      (setq method-list (%get-value *self* '$FNLS))
      (mformat "~%; database saving ~S methods..." (length method-list))
      (dolist (method-id method-list)
        (send method-id '=save))
      
      ;;=== entry points
      ;; they must be stripped of all references not pointing to objects in agent
      ;; package. An entry point may point to objects in various packages as well as
      ;; onto objects tat must not be saved
      (setq ep-list (%get-value *self* '$EPLS))
      (mformat "~%; database saving ~S entry points..." (length ep-list))
      (dolist (entry ep-list)
        (send entry '=save))
      
      ;;=== system variable list
      (setq variable-list (%get-value *self* '$SVL))
      (mformat "~%; database saving ~S variables..." (length variable-list))
      (dolist (variable-id variable-list)
        (moss::db-store variable-id (symbol-value variable-id) partition))
      
      ;;=== function list
      (setq function-list (%get-value *self* '$SFL))
      (mformat "~%; database saving ~S functions..." (length function-list))
      (dolist (ff function-list)
        (moss::db-store ff (symbol-value ff) partition)
        )
      
      ;;=== defsetf definitions
      (setq function-list (%get-value *self* '$DFXL))
      (mformat "~%; database saving ~S function definitions..." (length function-list))
      (dolist (ff function-list)
        (moss::db-store ff (symbol-value ff) partition)
        )  
      
      (mformat "~%; =SAVE (SYSTEM) /objects saved..." (length function-list))
      :objects-saved)))

;;;-------------------------------------------------------------- =SAVE (UNIVERSAL)

(defmossuniversalmethod =save ()
  "Default method that applies to all objects. If object has a :no-save tag, will ~
   not be saved, if it has a :save-fcn tag the function will be called.
Arguments:
   none
Return:
   not significant"
  ;(break "universal =save method ==========> ~S" *self*)
  (if (get *self* :save-fcn)
    (moss::db-store *self* (funcall (get *self* :save-fcn) *self*)
                    (intern (package-name *package*) :keyword))
    (moss::db-store *self* (symbol-value *self*)
                    (intern (package-name *package*) :keyword))))

;;;-------------------------------------------------------- =SAVE (VIRTUAL-CONCEPT)

(defmossinstmethod =save MOSS-VIRTUAL-CONCEPT ()
  "save a virtual concept into the database.
Arguments:
   database-pathname: handle to get into the database.
Return:
   virtual-class-id."
  (unless (get *self* :no-save)
    (moss::db-store *self* (symbol-value *self*)
                    (intern (package-name *package*) :keyword)))
  *self*)

#|
(send 'address::$S-ADDRESS '=save (omas::base address::SA_address))
|#

;;;------------------------------------------------ =SAVE-APPLICATION (MOSS-SYSTEM)
;;;***** untested


;;;(defmossinstmethod  =save-application MOSS-SYSTEM
;;;  (&optional (application-name *application-name*))
;;;  "Save all the objects of an application including system objects into a file ~
;;;   whose name is <application name>.mos
;;;  System objects are saved last."
;;;  (declare (special *moss-directory-pathname*))
;;;  (let (file-pathname system-list object-list disk-list answer)
;;;    (unless application-name
;;;      (throw :error ";*** Error: cannot save application that has no name. 
;;;Please set moss::*application-name* to the desired file name."))
;;;    ;; compute some reasonable file path
;;;    (setq file-pathname 
;;;          (merge-pathnames 
;;;           #+MICROSOFT-32
;;;           (concatenate 'string "applications\\" application-name ".mos")
;;;           #+MCL (concatenate 'string ":applications:" application-name ".mos")
;;;           moss::*moss-directory-pathname*))
;;;    (mformat ";*** saving the world into ~S" file-pathname)
;;;    ;; when a file exists with the same name, then change it to a saved one ".sav"
;;;    ;; and save
;;;    ;; then get the list of all objects
;;;    (setq answer (send *self* '=get-all-objects))
;;;    ;; filter out system objects
;;;    (setq system-list 
;;;          (reduce  ; jpb 140820 removing mapcan
;;;           #'append
;;;           (mapcar
;;;               #'(lambda(xx)(when (eql (symbol-package xx) (find-package :moss))
;;;                              (list xx)))
;;;             answer)))
;;;    ;; build list with system objects at the end
;;;    (setq object-list (append (set-difference answer system-list) system-list))
;;;    ;; build a list of pairs
;;;    (setq disk-list (mapcar #'(lambda (xx) 
;;;                                (cons xx (if (boundp xx)
;;;                                             (eval xx)
;;;                                           (progn
;;;                                             (mformat "~%,Warning: unbound object: ~S"
;;;                                                      xx)
;;;                                             :unbound))))
;;;                      object-list))
;;;    ;; then write everything into the new file
;;;    (with-open-file (ss file-pathname :direction :output :if-exists :rename)
;;;      (write disk-list :stream ss))
;;;    t))


;;;------------------------------------------------------ =SAVE-INSTANCES (CONCEPT)

(defmossinstmethod =save-instances MOSS-CONCEPT ()
  "saves all instances of a concept into the database including ideal.
Arguments:
   none.
Return:
   relation-id."
  (unless (get *self* :dont-save-instances)
    (let ((instance-list (send *SELF* '=get-instances :ideal t)))
      ;(print`(+++ =save-instances ,*self* instance-list ,instance-list))
      (dolist (instance instance-list)
        (when (and (boundp instance) instance (not (get instance :no-save)))
          (format t "~&; =save-instances / saving: ~S" instance)
          (send instance '=save)))
      :done)))

;;; careful in case of an instance belonging to several classes, we must not 
;;; resolve the link but save it into the database...

;;;---------------------------------------------------- =SAVE-ORPHANS (MOSS-SYSTEM)

(defmossinstmethod =save-orphans MOSS-SYSTEM ()
  "saves all orphans into the database.
Arguments:
   database-pathname: handle to get into the database.
Return:
   :done"
  (dolist (object (%get-all-orphans))
    (when (and (boundp object) object (not (get object :no-save)))
      (moss::db-store object (symbol-value object)
                      (intern (package-name *package*) :keyword)))))

;;;----------------------------- =SAVE-APPLICATION-OBJECTS-IN-PACKAGE (MOSS-SYSTEM)
#|
(in-package :address-proxy)
(setq moss::database-pathname (omas::base sa_address-proxy))
(setq *self* *moss-system*)
(setq moss::package *package*)
(omas::print-database sa_address-proxy)
|#
#|
(defmossinstmethod  =save-application-objects-in-package MOSS-SYSTEM 
  (package database-pathname)
  "saves ontology and KB from the current package by expunging objects defined ~
   in other packages including the MOSS package. 
Arguments:
   package: the reference package
   database-pathname: a handle to the object database
Return:
   :done"
  (let (concept-list virtual-concept-list attribute-list relation-list method-list
                     inverse-relation-list variable-list function-list ep-list ff)
    ;(break "===> =save-application-objects-in-package")
    
    ;;=== concepts (classes)
    (with-package package   ; used for saving counters
      (setq concept-list (%filter-against-package
                          (%get-value *self* '$ENLS)
                          package))
      (format t "~&; database saving ~S concepts..." (length concept-list))
      (dolist (concept-id concept-list)
        (send concept-id '=save database-pathname))
      
      
      ;;=== virtual concepts
      (setq virtual-concept-list (%filter-against-package
                                  (%get-value *self* '$VELS)
                                  package))
      (format t "~&; database saving ~S virtual concepts..." (length virtual-concept-list))
      (dolist (virtual-concept-id virtual-concept-list)
        (send virtual-concept-id '=save database-pathname))
      
;;; counters either are instances of $E-COUNTER or are class counters $E-COUNTER.CTR
;;;   an exception is orphan counter $E-COUNTER.1
;;;   Maybe should be the couter of the null-class *none*
;;;    ;;=== counters (not recorded as of now**********)
;;;    (setq counter-list (%filter-against-package
;;;                          (%get-value *self* '$ETLS)
;;;                          package))
;;;    (dolist (counter-id counter-list)
;;;      (send counter-id '=save database-pathname))
    
      ;;=== attributes
      (setq attribute-list (%filter-against-package
                            (%get-value *self* '$ETLS)
                            package))
      (format t "~&; database saving ~S attributes..." (length attribute-list))
      (dolist (attribute-id attribute-list)
        (send attribute-id '=save database-pathname))
      
      ;;=== relations
      (setq relation-list (%filter-against-package
                           (%get-value *self* '$ESLS)
                           package))
      (format t "~&; database saving ~S relations..." (length relation-list))
      (dolist (relation-id relation-list)
        (send relation-id '=save database-pathname))
      
      ;;=== inverse relations
      (setq inverse-relation-list (%filter-against-package
                                   (%get-value *self* '$EILS)
                                   package))
      (format t "~&; database saving ~S inverse relations..." 
              (length inverse-relation-list))
      (dolist (inverse-relation-id inverse-relation-list)
        (send inverse-relation-id '=save database-pathname))   
      
      ;;=== orphans
      ;; should do domething about the orphan counter...
      (format t "~&; database saving orphans...")
      (send *self* '=save-orphans database-pathname)
      
      ;;=== instances
      (format t "~&; database saving individuals...")
      (dolist (concept-id concept-list)
        (send concept-id '=save-instances database-pathname))
      
      ;;=== methods
      (setq method-list (%get-value *self* '$FNLS))
      (format t "~&; database saving ~S methods..." (length method-list))
      (dolist (method-id method-list)
        (print method-id)
        (send method-id '=save database-pathname))
      
      ;;=== entry points
      ;; they must be stripped of all references not pointing to objects in agent
      ;; package. An entry point may point to objects in various packages as well as
      ;; onto objects tat must not be saved
      (setq ep-list (%get-value *self* '$EPLS))
      (dolist (entry ep-list)
        (send entry '=save database-pathname package))
      
      
      ;;=== system variable list
      (setq variable-list (%filter-against-package
                           (%get-value *self* '$SVL)
                           package))
      (format t "~&; database saving ~S variables..." (length variable-list))
      (dolist (variable-id variable-list)
        (moss::db-write-object 
         database-pathname variable-id (symbol-value variable-id)))
      
      ;;=== function list
      (setq function-list (%filter-against-package
                           (%get-value *self* '$SFL)
                           package))
      (format t "~&; database saving ~S functions..." (length function-list))
      (dolist (function-name function-list)
        (setq ff (intern (concatenate 'string "F_" (symbol-name function-name))))
        (moss::db-write-object database-pathname ff (symbol-value ff))
        )
      :objects-saved)))
|#

;;;============================= End =save methods ================================

;;;================================================================================
;;;                                =SET Methods
;;;================================================================================

;;; WARNING: =set methods do not preserve the PDM structure!
;;;--------------------------------------------------------------- =SET (UNIVERSAL)

;;; Again here we do not check that we have the right to assign a value
;;; with corresponding property.
;;; ***** We should do it
;;; we should define =set+ to add mln values (either MLN or value made into a string 
;;; associated with current language or specified language)
;;; We should also test that if prop is a relation, then value is a PDM pbject!

(defmossuniversalmethod =set (prop-name value &optional version)
  "Sets a property value given the property name.
Arguments:
   property name: e.g. HAS-NAME
   associated value: e.g. \"George\""
  (let ((context (or version (symbol-value (intern "*CONTEXT*"))))
        ;; actually gets it from prop-ref
        (prop-id (%get-property-id-from-name *self* prop-name)))
    (if prop-id 
      (%%set-value *self* value prop-id context)
      (warn "in =set ~A is not a property of object ~A in context ~A"
            prop-name (-> *self* '=summary) context)
      )))

#|
;; executed in the test package

(send _jpb '=set "age" '64)
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.2))
 ($T-PERSON-NAME (0 "Barths")) ($T-PERSON-FIRST-NAME (0 "Jean-Paul"))
 ($S-PERSON-BROTHER.OF (0 $E-PERSON.1)) ($T-PERSON-AGE (0 64))
 ($S-FATHER (0 $E-PERSON.3)))

(send _jpb '=set "age" '64 3)
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1)) ($T-PERSON-NAME (0 "Barthès"))
 ($T-PERSON-FIRST-NAME (0 "Jean-Paul")) ($S-PERSON-HUSBAND.OF (0 $E-PERSON.2))
 ($S-ORGANIZATION-PRESIDENT.OF (5 $E-ORGANIZATION.1)) ($T-PERSON-AGE (3 64) (0 70)))

(send _jpb '=set "ageb" '64)
; Warning: in =set ageb is not a property of object ($E-PERSON.2) in context 0
; While executing: MOSS::*0==SET
|#
;;;---------------------------------------------------------- =SET-LIST (UNIVERSAL)
;;; Again here we do not check that we have the right to assign a value
;;; with corresponding property.
;;; ***** We should do it

(defmossuniversalmethod =set-list (prop-name value-list &optional version)
  "Sets a property value (list) given the property name.
Arguments:
   property name: e.g. HAS-NAME, \"skill\"
   associated value: e.g. \"George\""
  (let* ((context (or version (symbol-value (intern "*CONTEXT*"))))
        (prop-id (%get-property-id-from-name *self* prop-name context)))
    (if prop-id 
      (%%set-value-list *self* value-list prop-id context)
      (warn "in =set ~A is not a property of object ~A in package ~S and context ~A"
            prop-name (-> *self* '=summary) (package-name *package*) context)
      )))

#|
(send _jpb '=set-list "middle name" '("A." "Z.B.B."))
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1)) ($T-PERSON-NAME (0 "Barthès"))
 ($T-PERSON-FIRST-NAME (0 "Jean-Paul")) ($S-PERSON-HUSBAND.OF (0 $E-PERSON.2))
 ($S-ORGANIZATION-PRESIDENT.OF (5 $E-ORGANIZATION.1)) ($T-PERSON-AGE (3 64) (0 70))
 ($T-PERSON-MIDDLE-NAME (7 "A." "Z.B.B.")))
|#
;;;------------------------------------------------------------ =SET-ID (UNIVERSAL)

(defmossuniversalmethod =set-id (prop-id value)
  "Sets (replaces) a property value given the property id
Arguments:
   prop-id: property-id
   value: associated value
Return:
   internal representation of the object."
  (%%set-value *self* value prop-id (symbol-value (intern "*CONTEXT*"))))

;;;------------------------------------------------------- =SET-ID-LIST (UNIVERSAL)

(defmossuniversalmethod =set-id-list (prop value-list)
  "Sets (replaces) a property value given the property id.
Arguments:
   prop: property identifier
   value-list: list of values replacing the old ones
Return:
   ?"
  (%%set-value-list *self* value-list prop (symbol-value (intern "*CONTEXT*"))))

;;;============================= End =set methods =================================

;;;================================================================================
;;;                              =SUMMARY Methods
;;;================================================================================

;;; =summary methods are used to print something understandable instead of an object
;;; id
;;; (send 'xxx =summary) returns a list containing a single string or the id of the
;;; object when the =summary method is undefined. Maybe should return the id as a
;;; string...

;;;----------------------------------------------------------- =SUMMARY (ATTRIBUTE)

(defmossinstmethod =summary MOSS-ATTRIBUTE ()
  "Return the property name"
  (let ((class-id (car (g=> (%inverse-property-id '$PT))))
        (value (car (HAS-MOSS-PROPERTY-NAME))))
    (setq value (send *self* '=format-value value))
    (list
     (apply #'make-name
            (cons value
                  (if class-id (append '(/) 
                                       (send class-id '=summary))))))))
#|
(send test::_has-age '=summary)
("age")

(send test::_has-person-age '=summary)
("age/PERSON")
|#
;;;------------------------------------------------------------- =SUMMARY (CONCEPT)

(defmossinstmethod =summary MOSS-CONCEPT ()
  "Return in a list the first name of the concept in the current language"
  (let ((value (car (HAS-MOSS-CONCEPT-NAME))))
    (list (if (or (mln::mln? value) ; jpb 1406
                  (mln::%mln? value) ; jpb 1411
                  )
              (mln::get-canonical-name value) ; jpb 1406
            value))))

#|
(send test::_person '=summary)
("PERSON")
|#
;;;------------------------------------------------------------- =SUMMARY (COUNTER)

(defmossinstmethod =summary MOSS-COUNTER ()
  "return value of counter"
  (HAS-MOSS-VALUE))

#|
(send 'moss::$ENT.CTR '=summary)
(1)

(send 'test::$E-PERSON.CTR '=summary)
(8)

(with-package :test
  (with-context 5
    (send '$E-PERSON.CTR '=summary)))
(8)
|#
;;;-------------------------------------------------------- =SUMMARY (INVERSE-LINK)
;;;BUG: should return a string not an MLN : fixed

(defmossinstmethod =summary MOSS-INVERSE-LINK ()
  "Return the inverse property name"
  (list (mln::get-canonical-name (car (HAS-MOSS-INVERSE-NAME)))))

#|
(send 'test::$S-BROTHER.OF '=summary)
("IS-BROTHER-OF")
|#
;;;-------------------------------------------------------------- =SUMMARY (METHOD)

(defmossinstmethod =summary MOSS-METHOD (&rest ll)
  "Return the method name"
  (declare (ignore ll))
  (let ((own (g=> '$OMS.OF))
        (instance (g=> '$IMS.OF))
        (method-name (HAS-MOSS-METHOD-NAME)))
    (list 
     (cond
      (own
       (format nil "~{~A~^ ~} (OWN/~{~A~^ ~})" 
         method-name (send (car own) '=summary)))
      (instance
       (format nil "~{~A~^ ~} (INST/~{~A~^ ~})" 
         method-name (send (car instance) '=summary)))
      (t "*unknown type of method*")))))

#|
(send 'moss::$FN.22 '=summary)
("=FIND (INST/MOSS-SYSTEM)")
|#
;;;------------------------------------------------------------ =SUMMARY (RELATION)

(defmossinstmethod =summary MOSS-RELATION ()
  "Returns the property name with the associated class"
  (let ((class-id (car (g=> (%inverse-property-id '$PS)))))
    (list 
     (apply #'make-name   
            (append  (-> *self* '=get-name) 
                    (if class-id (append '(/) 
                                         (send class-id '=summary))))))))
#|
(send test::_has-brother '=summary)
("Brother/UNIVERSAL-CLASS")

(send test::_has-person-brother '=summary)
("brother/person")
|#
;;;--------------------------------------------------------- =SUMMARY (MOSS-SYSTEM)

(defmossinstmethod =summary MOSS-SYSTEM ()
  "Returns the system name"
  (list (%%make-name-string (car (g=> '$SNAM)) :plain)))

#|
(send moss::*moss-system* '=summary)
("MOSS")
|#
;;;----------------------------------------------------------- =SUMMARY (UNIVERSAL)

(defmossuniversalmethod =summary (&rest option-list)
  "Default method for printing summary of successor.
Arguments:
   option-list (rest): ?
Return:
   list of the internal name by default"
  (declare (ignore option-list))
  ; to prevent the compiler from complaining
  (ncons *self*))

#|
(with-package :test
  (with-context 5
    (send 'test::$e-organization.1 '=summary)))
(TEST::$E-ORGANIZATION.1)
|#
;;;---------------------------------------------------- =SUMMARY (UNIVERSAL-METHOD)

(defmossinstmethod =summary MOSS-UNIVERSAL-METHOD ()
  "Return the method name"
  (list (format nil "~S" (car (HAS-MOSS-UNIVERSAL-METHOD-NAME) ))))

#|
(send 'moss::$UNI.22 '=summary)
("=DELETE-SP-ID")
|#
;;;============================= End =summary methods =============================

;;;--------------------------------------------------------- =UNLINK (INVERSE-LINK)

(defmossinstmethod =unlink MOSS-INVERSE-LINK (entity-id suc-id)
  "Removes links between 2 entities in current context.
Arguments:
   entity-id: id of first entity
   suc-id: id of successor
Return:
   not significant."
  (%unlink entity-id *self* suc-id)
  )

;;;------------------------------------------------------------- =UNLINK (RELATION)

(defmossinstmethod =unlink MOSS-RELATION (entity-id suc-id)
  "Removes links between 2 entities in current context.
Arguments:
   entity-id: id of first entity
   suc-id: id of successor
Return:
   not significant."
  (%unlink entity-id *self* suc-id)
  )

;;;------------------------------------------------------------- =WHAT? (UNIVERSAL)

(defmossuniversalmethod =what? (&key (stream t))
  "Give a summary of the type of object we are considering - ~
   Prints a type message followed by the content of the $DOCT ~
   field of the given model
Argument:
   stream (key): printing channel.
Return:
   t"
  (let* ((model-id (car (h-> '$TYPE)))
         (*moss-output* stream)
         (ancestor-list (cdr (%sp-gamma-l (%get-value *self* '$TYPE) '$IS-A)))
         ;; for orphans properties and models
         (prototype-list  (cdr (%sp-gamma-l (list *self*) '$IS-A)))
         (*trace-flag* nil)
         answer)
    (cond
     ((eql model-id '*none*)
      
      ;;== here the object has no class, thus it is an orphan
      (mformat "The object is an orphan (i.e. has no class).")
      ;; follow the $IS-A links
      (when prototype-list
        (mformat "~%... has prototypes (depth first)")
        (dolist (prototype prototype-list)
          (mformat "~%~{~A ~}" (send-no-trace prototype '=summary))))
      
      ;;--
      (setq answer (send *self* '=has-value-id '$DOCT))
      (if answer
          (mformat "~%~A" (mln::extract (car answer) :always t)) ; jpb 1406
        (mformat "~%*Sorry, no specific documentation available*")
        )
      )
     
     ;;== here object is an instance of something but is not an entry point
     ((not (eql model-id '$EP))
      (mformat "The object is an instance of ~S " 
               (car (mln::extract (car (send model-id '=get-name)) :always t))) ; jpb 1406
      (when prototype-list
        (mformat "~%... has prototypes or generalizations (depth first)")
        (dolist (prototype prototype-list)
          (mformat "~%~{~A ~}" (send-no-trace prototype '=summary))))
      (when ancestor-list
        (mformat "~%... has ancestors classes (depth first)")
        (while ancestor-list
               (mformat "~%~A " 
                        (mln::extract  ; jpb 1406
                         (car (send-no-trace (pop ancestor-list) '=get-name)) 
                         :always t)
                        )))
      ;;--
      (setq answer (send *self* '=has-value-id '$DOCT))
      (if answer
          (mformat "~%~A" (mln::extract (car answer) :always t)) ; jpb 1406
        (mformat "~%*Sorry, no specific documentation available*")
        )
      )
     
     ;;== here object is an entry point, we print all objects pointed to
     (t
      (mformat "~A is an entry point: " *self*)
      (dolist (pair (symbol-value *self*))
        (unless (member (car pair) '($type $id $epls.of))
          (dolist (item (send *self* '=get-id (car pair)))
            (send item '=what?)))))
     )
    t))

#|
(send test::_jpb '=what?)
The object is an instance of "PERSON" 
*Sorry, no specific documentation available*
T

(send 'test::$e-person '=what?)
The object is an instance of "MOSS-CONCEPT"
*Sorry, no specific documentation available*
T
|#
;;;---------------------------------------------------------------- =XI (ATTRIBUTE)
;;; Must not have a validate-tp in the =xi method. =xi is for norming the value
;;; not for checking value-lists

(defmossinstmethod =xi MOSS-TERMINAL-PROPERTY (data)
  "Normally normalizes data values. By default does nothing.
Arguments:
   data: list of values to be checked.
Return:
   data if OK (default), nil otherwise"
  data)

#|
(send 'test::$T-PERSON-NAME '=xi "Jean-Paul")
"Jean-Paul"
|#
;;;--------------------------------------------------------------------------------

;;; removing the fake access function (pb with the MCL compiler)
;(fmakunbound 'access)

(format t "~%;*** MOSS v~A - Kernel loaded ***" *moss-version-number*)

;;; :EOF 