;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==============================================================================
;;;20/01/30
;;;           M O S S - O V E R V I E W (file moss-W-overview-P.lisp)
;;;
;;;==============================================================================
;;; This file contains functions to create an OVERVIEW window allowing to 
;;; visuallize the content of an ontology and associated knowledge base

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#


#|
History
-------
2010
 0927 introducing agent environment when editing or creating instance
 1025 sorting instances using string-lessp (case ignored)
 1029 modifying ow-outline-on-change (reference problem)
2012
 1017 correcting ow-mkinst-button-on-click: bad argument
 1025 allowing indow to be resized
2014
 0302 modifying the EDIT button callback to allow automatic update of the window
      when we come back from an editing session. Adding a refresh function
 0307 -> UTF8 encoding
 0629 replacing mln::extract by mln::extract-to-string
2015
 0826 removed %make-string-from-ref from ow-outline-on-change
2020
 0130 restructuring MOSS as an asdf system
|#

(in-package :moss)

;;; import symbols and functions from cg-user

(eval-when (compile load eval)
  (import '(cg:beep cg:find-component cg:select-window cg:screen cg:make-box
                    cg:make-rgb cg:make-font-ex cg:make-window cg:find-sibling
                    cg:windowp cg:set-focus-component cg:selected-object 
                    cg:find-window cg:find-pixmap cg:interior-width
                    cg:interior-height cg:add-component
                    cg:static-text cg:editable-text cg:multi-line-editable-text
                    cg:button cg:check-box cg:progress-indicator cg:radio-button
                    )))

;;;================================= globals ====================================
;;; none

;;;===============================================================================
;;;
;;;                              OVERVIEW WINDOW 
;;;
;;;===============================================================================
;;; This is the window in which objects are displayed

;;;------------------------------------------------------- OW-MAKE-OUTLINE-WINDOW
;;; make special OVERVIEW and SIL (Single Item List) to keep index of objects
;;; one problem is to display ontologies when concepts are described in the 
;;; package of a particular agent. 

;;; When working in OMAS, the window is created from the agent window, itself
;;; created in the cg-user package.
;;; Whenever we click on a specific button, we must install the package, context, 
;;; and version graph corresponding to the particular ontology.
;;; All the necessary contextual information is stored inside the assistant.
;;; It can be extracted reinstalled temporarily each time we click onto something

(defClass overview-window (cg:dialog)
  ((agent :accessor agent)
   (database :accessor database)
   (display-instance-with-internal-format 
    :accessor display-instance-with-internal-format :initform nil)
   ;; keep track of MOSS window (MOSS only)
   (moss-window :accessor moss-window :initform nil)
   ;; keep track of owner (OMAS only), duplicates owner in that case
   (owner :accessor owner :initform nil)
   (show-concept :accessor show-concept :initform nil)
   ))

(defClass class-outline (cg:outline)
  ((class-index :accessor class-index :initform nil)))
(defClass instance-sil (cg:single-item-list)
  ((instance-index :accessor instance-index :initform nil)))

;;;===============================================================================
;;;                             OUTLINE-WINDOW
;;;===============================================================================
(defparameter *win* nil)
(defun make-overview-window (tree parent-window &rest ll)
  (apply #'ow-make-outline-window tree parent-window ll))

(defUn ow-make-outline-window (tree parent-window 
                                    &key owner title database moss-window)
  "make a window with an outline pane
Arguments:
   tree: tree of objects to represent
   parent-window: owner of the window (unused)
   owner (key): agent to which the window belongs (the OMAS owner)
   title (key): string specifying header title
   database (key): object database
   moss-window (key): reference to MOSS window (nil for OMAS)
Return:
   the id of the window"
  (declare (ignore parent-window))
  (let* ((win (make-window  :outline
                :owner (screen cg:*system*)
                :class 'overview-window
                :name :outline-window
                :title title
                :height 600
                :width 800
                :BACKGROUND-COLOR (MAKE-RGB :RED 224 :GREEN 255 :BLUE 206)
                :state :shrunk
                :package *package*))
         ;; get context corresponding to active package
         (context (symbol-value (intern "*CONTEXT*")))  ; JPB1601
         
         ;;=== outline is the left pane displaying concepts
         (outline 
          (make-instance 'cg:outline
            :name :outline
            :font (make-font-ex nil :courier\ new 13)
            :range nil
            :opened-pixmap (find-pixmap :default-opened)
            :closed-pixmap (find-pixmap :default-closed)
            :leaf-pixmap (find-pixmap :default-leaf)
            :on-change 'ow-outline-on-change
            :left 0
            :top 0
            :width (floor (/ (interior-width win) 4))
            :height (interior-height win)
            :left-attachment :scale
            :top-attachment :scale
            :right-attachment :scale
            :bottom-attachment :bottom))
         
         ;;=== central pane displaying instance instances
         (instance-list 
          (make-instance 'instance-SIL
            :name :instance-list
            :font (make-font-ex nil :courier\ new 13)
            :left (floor (/ (interior-width win) 4))
            :top 0
            :width (floor (/ (interior-width win) 4))
            :height (interior-height win)
            :on-change 'ow-instance-list-on-change
            :bottom-attachment :bottom
            :left-attachment :scale
            :right-attachment :scale))
         
         ;;=== right pane displaying instance details
         (show-instance 
          (make-instance 'multi-line-editable-text
            :name :show-instance
            :font (make-font-ex nil :courier\ new 11)
            :left (floor (/ (interior-width win) 2))
            :top 40
            :width (floor (/ (interior-width win) 2))
            :height (- (interior-height win) 40)
            :bottom-attachment :bottom
            :right-attachment :right
            :left-attachment :scale
            :read-only t
            :value ""))
         
         ;;=== INTERNAL FORMAT
         (if-check-box 
          (make-instance 'check-box
            :name :if-check-box
            :font (MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
            :left (+ (floor (/ (interior-width win) 2)) 4)
            :top 2
            :title (mln::extract-to-string *WOVR-internal-format*)
            :on-change 'ow-if-check-box-on-change
            :width 110
            :bottom-attachment :top
            :left-attachment :scale
            :right-attachment :scale
            :tab-position 4))
         
         ;;=== SHOW CONCEPT
         (class-check-box 
          (make-instance 'check-box
            :name :if-check-box
            :font (MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
            :left (+ (floor (/ (interior-width win) 2)) 4)
            :top 18
            :title (mln::extract-to-string *WOVR-show-concept*)
            :on-change 'ow-class-check-box-on-change
            :width 105
            :bottom-attachment :top
            :left-attachment :scale
            :right-attachment :scale
            :tab-position 4))
         
         ;;=== CONTEXT label
         (version-text 
          (make-instance 'static-text
            :name :version-text
            :font (MAKE-FONT-EX NIL "Tahoma / ANSI" 11 '(:bold))
            :left 688
            :top 12
            :value (mln::extract-to-string *WOVR-context*)
            :left-attachment :scale
            :right-attachment :scale
            :height 16
            :width 56))
         ;;=== CONTEXT value
         (version-box 
          (make-instance 'editable-text
            :name :version-box
            :left 744 :top 8 :width 35 :height 24
            :font (MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
            :delayed t ; to delay on-change until another widget is selected
            :on-change 'ow-version-box-on-change
            :value (format nil "~A" context)
            :left-attachment :scale
            :right-attachment :scale
            ))
         ;;=== MAKE INSTANCE button
         (mkinst-button 
          (make-instance 'button
            :name :mkinst-button
            :font (MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
            :left 502
            :top 8
            :left-attachment :scale
            :right-attachment :scale
            :top-attachment :top
            :title (mln::extract-to-string *WOVR-make-instance*)
            :on-click 'ow-mkinst-button-on-click
            :height 26
            :width 80))
         
         ;;=== EDIT button
         (edit-button 
          (make-instance 'button
            :name :edit-button
            :font (MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL)
            :left 592
            :top 8
            :left-attachment :scale
            :top-attachment :top
            :right-attachment :scale
            :title (mln::extract-to-string *WOVR-edit*)
            :on-click 'ow-edit-button-on-click
            :height 26
            :width 80))
         )
    
    ;; compute the outline tree (forest of classes)
    (setf (cg:range outline)(ow-build-outline-items tree))
    ;; attach widgets to window
    (mapc #'(lambda(xx) (add-component xx win))
      (list outline instance-list show-instance if-check-box 
            edit-button mkinst-button class-check-box version-text version-box))
    ;; display the window
    (select-window win)
    ;; save environment into the window
    (setf (agent win) owner)
    (setf (owner win) owner)
    (setf (database win) database)
    
    ;; attach pointer to the window ???
    ;#-OMAS
    ;(setf (outline-window parent-window) win) ; might be used by OMAS ?
    
    ;; record moss-window if present (nil when called from OMAS)
    (setf (moss-window win) moss-window)
    ;; does not seem to do much...
    ;(setf (slot-value win 'cl:package) *package*)
    (setq *win* win)))

;;;------------------------------------------------------- OW-BUILD-OUTLINE-ITEMS

(defUn ow-build-outline-items (tree &aux outline-list)
  (loop
    (unless tree (return-from ow-build-outline-items 
                   (reverse outline-list)))
    ;(format *debug-io* "~&tree: ~S" tree)
    ;(setq tree (onto-sort tree))
    (push
     (make-instance 'cg:outline-item
       :value (format nil "~A" (pop tree))
       :state :open
       :range (when (listp (car tree))
                (ow-build-outline-items (pop tree))))
     outline-list)))

;;;------------------------------------------------- OW-CLASS-CHECK-BOX-ON-CHANGE
;;; Callback functions execute in the CG-USER package...

(defUn ow-class-check-box-on-change (widget new-value old-value)
  "set global variable *display-instance-with-internal-format* to print instance"
  (declare (ignore old-value))
  (let ((instance-list (find-sibling :instance-list widget))
        (outline-list (find-sibling :outline widget))
        (dialog (cg:parent widget)))
    
    ;(print `("W-overview/ow-class-check-box-on-change/ *package*" ,*package*))
    
    ;; set or reset show-concept property of overview window
    (setf (show-concept dialog) new-value)
    ;; redisplay objects if needed
    (cond
     ;; showing concept and something selected?
     ((and (show-concept dialog) outline-list)
      (ow-outline-on-change outline-list (cg:value instance-list) :dummy-old-value))
     ;; showing concept but nothing selected
     ((show-concept dialog) nil)
     ;; showing an instance?
     ((cg:value instance-list)
      ;; if an object is selected in the instance list, we should redisplay it
      (ow-instance-list-on-change 
       instance-list (cg:value instance-list) :dummy-old-value))))
  t)

;;;------------------------------------------------------ OW-EDIT-BUTTON-ON-CLICK
;;; We removed the possibility of editing a class JPB1403

(defUn ow-edit-button-on-click (dialog widget)
  "calls the EDIT window on the selected object on click"
  (declare (ignore widget))
  ;; when working with OMAS, we must open the editor in the agent package
  ;; but all functions have been written with MOSS package special variables
  ;; thus we save main parameters before installing the agent environment
  (let ((old-package *package*)
        (old-context *context*) ; this saves moss::*context*!
        (old-language *language*)
        (old-version-graph *version-graph*)
        next-editor agent obj-id)
    
    ;(print `("ow-edit-button-on-click/ old *package* 0" ,*package*))
    
    (setq agent (agent dialog)) ; only non nil in OMAS
    #+OMAS
    (when agent
      (setq *package* (omas::ontology-package agent)
          *language* (omas::language agent)
          *context* (omas::moss-context agent)
          *version-graph* (omas::moss-version-graph agent))
      ;; should indicate that we are editing, since it is the only interactive way
      ;; to do it
      )
    
    ;(print `("ow-edit-button-on-click/ *package*" ,*package*))
    ;(print `("ow-edit-button-on-click/ *context*" ,*context*))
    
    ;;=== here we edit a selected instance if any
    (let* ((instance-pane (find-component :instance-list dialog))
           (index (cg:value instance-pane))
           gate)
      (setq obj-id (cdr (assoc index (instance-index instance-pane) :test #'equal)))
      
      (cond
       ((moss::%pdm? obj-id)
        ;; create a closed gate
        (setq gate (mp:make-gate nil))
        ;(format t "~%;ow-edit-button-on-click /creating closed gate: ~S" gate)
        (unwind-protect
            (progn
              ;; here we must synchronize the editor to be able to update the 
              ;; overview window. Pass a gate structure.
              (setq next-editor 
                    (edit obj-id :owner (agent dialog) :database (database dialog)
                          :gate gate))
              ;; must record the editor in MOSS window
              #-OMAS ; agent window has no moss-window slot
              (setf (next-editor (moss-window dialog)) next-editor)
              ;; wait for end of editing, timed out after 5 minutes
              ;; the gate should be set in the first editor of a chain of editors
              (mp:process-wait-with-timeout 
               "waiting edit end" 600 #'mp:gate-open-p gate)
              )
          ;; unwind-protect: open the gate
          ;(format t "~%;ow-edit-button-on-click /opening gate: ~S" gate)
          (mp:open-gate gate)
          )  
        t)
       
       ;; otherwise, give up
       (t
        (warn "obj-id value ~S not the id of a MOSS object" obj-id)
        (cg::beep)
        nil))
      (cg:invalidate instance-pane)
      )   
    
    ;(print `("ow-edit-button-on-click/ *package* 2" ,*package*))
    
    
    ;; here we must update the show-instance widget and/or the instance-list pane
    ;; depending on what was done to the object
    (ow-edit-button-refresh-overview-window dialog obj-id)
    
    ;(cg:invalidate dialog)
    ;; clean up before exit
    (when agent
      (setq *package* old-package
          *language* old-language
          *context* old-context
          *version-graph* old-version-graph))
    
    t))

;;;--------------------------------------- OW-EDIT-BUTTON-REFRESH-OVERVIEW-WINDOW

(defun ow-edit-button-refresh-overview-window (dialog obj-id)
  "refreshes the overview window when returning from editing a specific object. ~
   If object was modified, we simply refresh the :show-instance widget. If object ~
   was deleted, we update the :instance-list pane and clean the :show-instance ~
   widget.
Arguments:
   dialog: the overview window
   obj-id: the object that was edited
Return:
   obj-id"
  (let ((instance-list-pane (find-component :instance-list dialog))
        (instance-pane (find-component :show-instance dialog))
        (context (cg:value (find-component :version-box dialog)))
        index-list)
    
    ;(print `("ow-edit-button-refresh-overview-window/ *package*" ,*package*))
    (catch
     :exit
     ;(format t "~%;ow-edit-button-refresh-overview-window /alive?: ~S, ~S"
     ;  obj-id (%alive? obj-id *context*))
    ;; object still alive?
    (cond 
     ((%alive? obj-id *context*)
      ;; yes simply update the :show-instance pane
      ;; when context is "ALL" we print all contexts
      (when (string= (string-upcase context) "ALL")
        (let ((moss::*moss-output* instance-pane))
          (setf (cg:value instance-pane) "")
          (moss::%pep obj-id))
        (throw :exit t))
      ;; first clear pane
      (setf (cg:value instance-pane) "")
      
      (if (display-instance-with-internal-format dialog)
          ;; when displaying in internal format simply print object
          (setf (cg:value instance-pane) 
            (format nil "~S" (eval obj-id)))
        ;; otherwise show a civilized view of the object
        (let ((moss::*moss-output* instance-pane))
          (moss::send obj-id 'moss::=print-self)))
      )
     ;; object has been delete in this version
     (t
      ;; get the list of index and instances
      (setq index-list (instance-index instance-list-pane))
      ;(format t "~%;ow-edit-button-refresh-overview-window /index-list 1: ~%  ~S"
      ;  index-list)
      ;; remove the object from the index list
      (setq index-list
            (remove (rassoc obj-id index-list) index-list :test #'equal))
      ;(format t "~%;ow-edit-button-refresh-overview-window /index-list 2: ~%  ~S"
      ;  index-list)
      ;; update the instance list pane
      (setf (instance-index instance-list-pane) index-list)
      ;; display the instances into the :instance-list
      (setf (cg:range instance-list-pane) (mapcar #'car index-list))
      (cg:invalidate instance-list-pane)
      ;; clean up the :show-instance pane
      (setf (cg:value instance-pane) "")
      ))
     ) ; end catch :exit
    (cg:invalidate instance-pane)
    ;; return the object-id
    obj-id))

;;;-------------------------------------------------------- OW-FIND-POSTED-ObJECT

(defun ow-find-posted-object ()
  "finds the object that is currently posted in the right detail pane.
Return:
   the object id, or nil if nothing is selected"
  (declare (special *win*))
  (when *win*
    (let* (object-ref object-id instance-pane)
      (cond
       ;; check whether we are displaying an individual or a concept
       ((show-concept *win*)
        ;; if we are displaying a concept
        ;; get the selected value in the list of concepts
        (setq object-ref (cg:value (cg:find-component :outline *win*)))
        ;; if none quit
        (unless object-ref (return-from ow-find-posted-object))
        ;; get the concept id
        (setq object-id (%%get-id object-ref :class))
        )
       
       ;; otherwise, get the selected object in the list of instances
       (t
        ;; get the instance pan
        (setq instance-pane (cg:find-component :instance-list *win*))
        ;; get the label of the selected object
        (setq object-ref (cg:value instance-pane))
        ;; get the object id from the index
        (setq object-id (cdr (assoc object-ref (instance-index instance-pane) 
                                    :test #'equal)))
        )
       )
      ;; return the object
      object-id)))

#|
? (with-package :family
    (ow-find-posted-object))
Select a person
FAMILY::$E-PERSON.1
Display the class
FAMILY::$E-PERSON
|#
;;;---------------------------------------------------- OW-IF-CHECK-BOX-ON-CHANGE
;;; this function executes in the :CG-USER package, which is not important here

(defUn ow-if-check-box-on-change (widget new-value old-value)
  "set global variable *display-instance-with-internal-format* to print instance"
  (declare (ignore old-value))
  (let ((context-value (cg:value (find-sibling :version-box widget)))
        (dialog (cg:parent widget)))
    (setf (display-instance-with-internal-format dialog) new-value)
    ;; reset context if the value is all
    (if (equal+ (string-trim '(#\space) context-value) "ALL")
        ;; reset to the current context
        (ow-reset-current-context)
      ;; otherwise redraw  object
      (ow-redisplay-object)
      )
  t))

;;;--------------------------------------------------- OW-INSTANCE-LIST-ON-CHANGE
;;; can be used instead of on-click
;;; executes in the :CG-USER package

(defUn ow-instance-list-on-change (widget new-value old-value)
  "display the selected object"
  (declare (ignore old-value))
  
  (unless (show-concept (cg:parent widget))
    ;; set up single exit point (for clean up if posting agent ontologies)
    (let* ((instance-pane (find-sibling :show-instance widget))
           (context (read-from-string 
                     (cg:value (find-sibling :version-box widget)) nil nil))
           (current-context (symbol-value (intern "*CONTEXT*")))
           (dialog (cg:parent widget))
           ;; save environment (used when displaying agent ontologies)
           (old-package *package*)
           (old-context *context*)
           (old-language *language*)
           (old-version-graph *version-graph*)
           (agent (agent dialog)) ; when called from OMAS
           object-id)
      (print (symbol-value (intern "*CONTEXT*")))
      (catch 
       :exit
       #+OMAS
       (when agent
         ;; this will set MOSS values, which is not useful
         ;;... might be if we are editing or creating something (not sure)
         (setq *package* (omas::ontology-package agent)
             *language* (omas::language agent)
             *context* (omas::moss-context agent)
             *version-graph* (omas::moss-version-graph agent)))
       
       ;; get the object id from selected string-value
       ;(print `(===> ow-instance-list-on-change package ,*package*
       ;             instance-list ,(instance-index widget)))
       (setq object-id (cdr (assoc new-value (instance-index widget) 
                                   :test #'equal)))              
       ;; first clear pane
       (setf (cg:value instance-pane) "")
       ;; if object is nil quit
       (unless object-id (throw :exit t))
       
       ;; reset context just in case, which displays details
       (if (eql context current-context)
           ;; do not change value but display object
           (ow-redisplay-object object-id current-context) 
         ;; otherwise change content of context, which will trigger call back
         (ow-reset-current-context))
       )
      ;; clean up before exit
      (when agent
        (setq *package* old-package
            *language* old-language
            *context* old-context
            *version-graph* old-version-graph)))
    t))

;;;---------------------------------------------------- OW-MKINST-BUTTON-ON-CLICK

(defUn ow-mkinst-button-on-click (dialog widget)
  "calls the EDIT window to make an instance of the selected class on click"
  (declare (ignore widget))
  ;; here we look for a selected concept (not instance)
  (let ((concept-string (cg:value (find-component :outline dialog)))
        (old-package *package*)
        (old-context *context*)
        (old-language *language*)
        (old-version-graph *version-graph*)
        concept-id editor agent)
    ;; when nothing is selected, get out
    (unless concept-string
      (beep)
      (return-from OW-mkinst-BUTTON-ON-CLICK nil))
    
    ;; otherwise set up agent environment
    (setq agent (agent dialog))
    #+OMAS
    (when agent
      (setq *package* (omas::ontology-package agent)
          *language* (omas::language agent)
          *context* (omas::moss-context agent)
          *version-graph* (omas::moss-version-graph agent)))
    ;; try to get class-id
    (setq concept-id (moss::%%get-id concept-string :class))
    
    (if concept-id
        (progn
          (setq editor
                (edit concept-id 
                      :owner (owner dialog)
                      :database (database dialog)
                      :action :create))
          ;; update MOSS window (not available in OMAS)
          #-OMAS
          (setf (next-editor (moss-window dialog)) editor)
          )
      (warn "no concept selected or cannot find  ~S" concept-string)
      )
    ;; clean up before exit
    (when agent
      (setq *package* old-package
          *language* old-language
          *context* old-context
          *version-graph* old-version-graph)))
  t)

;;;--------------------------------------------------------- OW-OUTLINE-ON-CHANGE

(defUn ow-outline-on-change (widget new-value old-value)
  "displays the list of instances in the second pane"
  (declare (ignorable old-value)) ; used for traces
  (let ((instance-list-pane (find-sibling :instance-list widget))
        (instance-pane (find-sibling :show-instance widget))
        (dialog (cg:parent widget))
        class-id instance-list index-list agent
        ;; save environment for agent case
        (old-package *package*)
        (old-context *context*)
        (old-language *language*)
        (old-version-graph *version-graph*)
        sorted-index-list)
    
    ;(print `("W-overview/ow-outline-on-change/ *package*" ,*package*))
    ;(print `("W-overview/ow-outline-on-change/ new-value" ,new-value))
           
    (setf (cg:range instance-list-pane)(list new-value))
    ;; if the detail window is showing an agent ontology we must set up the proper
    ;; environment
    #+OMAS
    (when (setq agent (agent dialog))
      (setq *package* (omas::ontology-package agent)
          *context* (omas::moss-context agent)
          *version-graph* (omas::moss-version-graph agent)
          *language* (omas::language agent))
      )
    ;(format t "~%; ow-outline-on-change /1 value widget: ~%  ~S" (cg:value widget))
    ;; get the class from its print name
    (setq class-id 
          (moss::%extract (cg:value widget) 
                          'moss::HAS-MOSS-ENTITY-NAME 'moss::MOSS-ENTITY))
    ;(format *debug-io* "~&old-value: ~S; new-value ~S" old-value new-value)
    ;(format *debug-io* "~&package: ~S; language: ~S" *package* *language*)
    ;(format t "~%; ow-outline-on-change /2 class-id: ~%  ~S" class-id)
    ;; when the class does not exist then we have a problem
    (unless class-id
      (cg:beep)
      (return-from OW-outline-ON-CHANGE nil))
    ;; get all the instances, using the query mechanism
    (setq instance-list (moss::access (list (cg:value widget))))
    ;; build an index list, add the internal sequence number of the object so
    ;; that we can distinguish ojects that have the same summary JPB 1010
        
    (setq index-list   
          (mapcar #'(lambda (xx) 
                      (cons 
                       (string+
                        (car (moss::send xx 'moss::=summary))
                        " ("
                        (or (%get-internal-instance-number xx) "")
                        ")")
                       xx))
            instance-list))
    
    ;; if the list is a set of strings sort them JPB 1010
    ;(format t "~%; ow-outline-on-change /3 index-list: ~%  ~S" index-list)
    (when (every #'(lambda (xx) (stringp (car xx))) index-list)
      (setq sorted-index-list (sort index-list #'string-lessp :key #'car))
      ;(format t "~%; ow-outline-on-change /4 raw sorted-index-list: ~%  ~S" 
      ;  sorted-index-list)
      (setq index-list sorted-index-list))
    ;(format t "~%; ow-outline-on-change /5 sorted index-list: ~%  ~S" index-list)
    
    ;; save the list (<summary> <id>) as an index to recover object id
    (setf (instance-index instance-list-pane) index-list)
    ;; display the instances into the :instance-list
    (setf (cg:range instance-list-pane) (mapcar #'car index-list))
    ;(format t "~%; ow-outline-on-change /5 outline range: ~%  ~S" 
    ;  (cg:range instance-list-pane))
    ;; unselect values
    (setf (cg:value instance-list-pane) "")
    ;; clean detail pane unless we want to display concept
    (setf (cg:value instance-pane) "")
    
    (unless class-id 
      ;; reinstall old environment if needed
      (when agent
        (setq *context* old-context
            *language* old-language
            *package* old-package
            *version-graph* old-version-graph))
      (return-from OW-outline-ON-CHANGE nil))
    
    
    (if (show-concept dialog)
        (if (display-instance-with-internal-format dialog)
            ;; when displaying in internal format simply print object
            (setf (cg:value INSTANCE-PANE) 
              (if class-id 
                  (format nil "~S" (eval class-id)) 
                ""))
          ;; otherwise show a civilized view of the object
          (let ((moss::*moss-output* INSTANCE-PANE))
            (moss::send class-id 'moss::=print-self)))
      (setf (cg:value INSTANCE-PANE) ""))
    
    (when agent
      (setq *context* old-context
          *language* old-language
          *package* old-package
          *version-graph* old-version-graph))
    )
  t)

;;;---------------------------------------------------------- OW-REDISPLAY-OBJECT

(defun ow-redisplay-object (&optional object-id context)
  "redisplay an object in the right pane area. If object-id is not supplied or ~
   nil, then the object is the one selected (either instance or concept). If ~
   context is not supplied, then it is the current context. If supplied it must ~
   be a valid context.
  Redisplay does not change the value of the context slot.
Arguments:
   object-id (opt): id of the object to redisplay
   context (opt): context in which to display the object (default:current context)
Return:
   t"
  (declare (special *win*))
  (when *win*
    (let ((instance-pane (cg:find-component :show-instance *win*))
          (object-id (or object-id (ow-find-posted-object)))
          (context (or context (symbol-value (intern "*CONTEXT*"))))
          )
      ;; erase pane
      (setf (cg:value instance-pane) "")      
      (when object-id
        ;; display object
        (if (display-instance-with-internal-format *win*)
            ;; when displaying in internal format simply print object
            ;; does not depend on the value of context
            (setf (cg:value instance-pane)  
                  (format nil "~S" (eval object-id)))
          ;; otherwise show a view of the object in the specified context
          (with-context context
            (let ((moss::*moss-output* instance-pane))
              (moss::send object-id '=print-self))))
        )))
  t)

;;;----------------------------------------------------- OW-RESET-CURRENT-CONTEXT

(defun ow-reset-current-context ()
  "reset current context in context slot and refresh the instance area even ~
   if we do not change the value of context."
  (declare (special *win*))
  ;; changing the content of the area triggers a refresh of the instance pane
  (let* ((version-box (cg:find-component :version-box *win*))
         (current-context (symbol-value (intern "*CONTEXT*")))
         (context (read-from-string (cg:value version-box) nil nil))
         )
    (if (eql context current-context)
        ;; does not change version box, must refresh area
        (ow-redisplay-object (ow-find-posted-object) context)
      ;; otherwise, changing the content of the box redisplays the area
      (setf (cg:value version-box) (string+ current-context))
      )))

;;;----------------------------------------------------- OW-VERSION-BOX-ON-CHANGE

(defUn ow-version-box-on-change (widget new-value old-value)
  "sets up displaying context in the right detail pane, but does not change current ~
   context. When set to ALL, displays all the context but does not change the ~
   current one. Otherwise, set the current context to the new value."
  (declare (ignore old-value))
  (let ((context-value (read-from-string new-value nil nil))
        (instance-pane (cg:find-sibling :show-instance widget))
        (object-id (ow-find-posted-object))
        (current-context (symbol-value (intern "*CONTEXT*")))
        error-text)
    ;; erase pane
    (setf (cg:value instance-pane) "")
    
    ;;== if value is "ALL" display all versions, do not change context
    (when (equal+ new-value "ALL")
      (when object-id
        ;; print all version value
        (%pep object-id :stream instance-pane))
      ;(setf (cg:value widget) old-value)
      (return-from ow-version-box-on-change t)
      )
    
    ;;== when the new value is the same as the current value, we refresh
    (when (eql context-value current-context) 
      (ow-redisplay-object object-id context-value)
      (return-from ow-version-box-on-change t))
    
    ;;== if not a legal context (including bad inputs), send error
    (unless (catch :error (%%allowed-context? context-value))
      (cg:beep)
      (setq error-text
            (format nil "*** ~S is an illegal context ***~%~
                         legal contexts are: ~{~S~^ ~}~%~
                         current context is ~S" new-value
              (delete-duplicates 
               (mapcar #'car (symbol-value (intern "*VERSION-GRAPH*"))))
              current-context))
      (cg:pop-up-message-dialog nil "Warning" error-text nil "OK?")
      (return-from ow-version-box-on-change t)
      )
    
    ;;== here OK, we display object in the new version (could be dead)
    (setq error-text
          (format nil "*** ~S is simply a displaying context ***~%~
                         It does not change the current context: ~S" new-value
            current-context))
    (cg:pop-up-message-dialog nil "Warning" error-text nil "OK?")
    (ow-redisplay-object object-id context-value)
    (return-from ow-version-box-on-change t)
    )
  )



(format t "~%;*** MOSS v~A - MOSS Overview window loaded ***" *moss-version-number*)

;;; :EOF