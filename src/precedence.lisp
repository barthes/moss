;;; compute precedence list

(defun %clean-path (path)
  (cond ((null path) nil)
        ((member (car path) (cdr path)) (cdr path))
        (t (cons (car path)(%clean-path (cdr path))))))

#|
? (reverse (%clean-path (reverse '(U V D W E X B Y V D Z A))))
(U V D W E X B Y V Z A)
|#
;;;-------------------------------------------------- %COMPUTE-ALL-PATHS-TO-ROOT

(defun %compute-all-paths-to-root (path-set result &aux parent-list)
  (cond
   ;; path-set exhausted, return result
   ((null path-set) (reverse (mapcar #'reverse result)))
   ;; when no parents transfer path to result
   ((null (setq parent-list (moss::%parents (caar path-set))))
    (%compute-all-paths-to-root (cdr path-set) (cons (car path-set) result)))
   ;; when parents extend path eventually splitting it
   (t (%compute-all-paths-to-root 
       (append (mapcar #'(lambda (xx) (cons xx (car path-set))) parent-list)
               (cdr path-set))
       result))))

#|
               AA
              /  \
            YY    \
            /     ZZ
           BB     |
          /  \    |
         /    -XX-+---
        /         |   \
       UU         |    EE
       |          |   /
        \         |  WW
         \        | /
          \       DD
           \     /
            \   VV
             \ /
              CC

(defconcept aa)
(defconcept yy (:is-a aa))
(defconcept zz (:is-a aa))
(defconcept bb (:is-a yy))
(defconcept uu (:is-a bb))
(defconcept xx (:is-a bb))
(defconcept ee (:is-a xx))
(defconcept ww (:is-a ee))
(defconcept dd (:is-a zz ww))
(defconcept vv (:is-a dd))
(defconcept cc (:is-a uu vv))

? (%compute-all-paths-to-root (list (list _cc)) ())
(($E-CC $E-UU $E-BB $E-YY $E-AA) ($E-CC $E-VV $E-DD $E-AA)
 ($E-CC $E-VV $E-DD $E-EE $E-XX $E-BB $E-YY $E-AA))

? (merge-paths '($E-UU $E-BB $E-YY $E-AA) '($E-VV $E-DD $E-AA))
($E-UU $E-BB $E-YY $E-VV $E-DD $E-AA)
|#
;;;---------------------------------------------------- %COMPUTE-PRECEDENCE-LIST

(defun %compute-precedence-list (class-id)
  "compute the list of priorities for classes is multiple inheritance is allowed.
Arguments:
   class-id: identifier of starting class
Return:
   a list of classes giving the priority for inheritance."
  (let* ((all-paths (%compute-all-paths-to-root (list (list class-id)) nil))
         path-list)
    ;; remove starting node from the path list and remove empty paths
    (setq path-list (remove nil (mapcar #'cdr all-paths)))
    ;; return precedence list
    (cons class-id 
          (%compute-precedence-list-1 (car path-list) (cdr path-list)))))

#|
? (compute-precedence-list _cc)
($E-CC $E-UU $E-VV $E-DD $E-WW $E-EE $E-XX $E-BB $E-YY $E-VV $E-ZZ $E-AA)
|#
;;;-------------------------------------------------- %COMPUTE-PRECEDENCE-LIST-1

(defun %compute-precedence-list-1 (merged-path path-list)
  (cond
   ((null path-list) 
    (reverse (%clean-path (reverse merged-path))))

   ((null merged-path)
    (%compute-precedence-list-1 
     (%merge-paths (car path-list)(cadr path-list))
     (cddr path-list)))

   (t
    (%compute-precedence-list-1 
     (%merge-paths merged-path (car path-list)) 
     (cdr path-list)))))

;;;---------------------------------------------------------------- %MERGE-PATHS

(defun %merge-paths (result candidate &optional stem)
    "check if a node of result is found in candidate. If so inserts the part of ~
      candidate preceding the shared node in front of it into result."
  (cond
   ((null result) (error "ee"))
   ((member (car result) candidate)
    (append (reverse stem) 
            (subseq candidate 0 (position (car result) candidate)) 
            result))
   (t (%merge-paths (cdr result) candidate (push (car result) stem)))
   ))

#|
? (merge-paths '(u b y a) '(v d z a))
(U B Y V D Z A)
? (merge-paths '(U B Y V D Z A) '(v d w e x b y a))
(U V D W E X B Y V D Z A)
|#




