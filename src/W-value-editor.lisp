;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;===============================================================================
;;;20/01/30
;;;		
;;;		V A L U E - E D I T O R (File MOSS-W-value-editor.Lisp)
;;;	
;;;===============================================================================
;;; This file contains the functions related to editing non MOSS disk items.

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
2010
 1220 Creation
2014
 0307 -> UTF8 encoding
2020
 0130 restructuring MOSS as an asdf system
|#

(in-package :moss)

;;;============================ MOSS function ===================================

(defUn edit-value (lisp-symbol database-pathname area-key &key called-from-editor)
  "launches the value editor.
Arguments:
   lisp-symbol: symbol whose value is to edit
   database-pathname: pathname of the currently opened database
   area-key: partition of the database (should exist)
Return:
   :done"
  (declare (special *value-editor*))
  
  ;; check first preliminary conditions
  (unless (symbolp lisp-symbol)
    (%beep)
    (format t "~%; edit-value /lisp-symbol arg: ~S should be a symbol" lisp-symbol)
    (return-from edit-value))
  
  (unless (db-opened? database-pathname)
    (%beep)
    (format t "~%; edit-value /database: ~S is not opened." database-pathname)
    (return-from edit-value))
  
  (unless (db-area-exists? area-key)
    (%beep)
    (format t "~%; edit-value /partition: ~S does not exist" area-key)
    (return-from edit-value))

  ;; create editor window
  (setq *value-editor* (make-value-editor-window lisp-symbol called-from-editor))
  
  ;; initialize window slots
  (setf (edited-symbol *value-editor*) lisp-symbol)
  (setf (database-pathname *value-editor*) database-pathname)
  (setf (area *value-editor*) area-key)
  (setf (initial-value *value-editor*) (symbol-value lisp-symbol))
  (setf (called-from-editor *value-editor*) called-from-editor)
  *value-editor*)

;;;=============================== Globals =======================================

(defparameter *value-editor* nil)

(defParameter *value-editor-left* 100)
(defParameter *value-editor-top* 100)
(defParameter *value-editor-height* 500)
(defParameter *value-editor-width* 1000)
(defparameter *value-editor-button-width* 70)
(defparameter *value-editor-nb-of-buttons* 6)
(defparameter *delta* 
  (+ *value-editor-button-width*
     (floor (/ (- *value-editor-width* 
                  (* *value-editor-nb-of-buttons* *value-editor-button-width*)
                  *value-editor-nb-of-buttons*) ; to adjust right margin
               (1- *value-editor-nb-of-buttons*)))))

(defConstant +value-editor-color+ 
  #+MCL 15922611 
  #-MCL (cg:MAKE-RGB :RED 241 :GREEN 243 :BLUE 169))

(defconstant +value-editor-font+
  #+MCL '("Chicago" 11 :SRCOR :PLAIN)
  #-MCL (cg:make-font-ex :swiss "MS Sans Serif / ANSI" 11 nil))

(defConstant +value-editor-button-text-font+
  #+MCL '("Helvetica" 11 :SRCCOPY :BOLD)
  #-MCL (cg:make-font-ex nil "Lucida Sans Unicode / ANSI" 11 nil))

(defConstant +value-editor-object-font+
  #+MCL '("Helvetica" 18 :SRCCOPY :BOLD)
  #-MCL (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 24 NIL))

(defConstant +value-editor-static-text-font+
  (cg:MAKE-FONT-EX NIL "Tahoma / ANSI" 11 NIL))



;;;============================= Macros ==========================================

(defParameter *ve-trace* nil "debugging trace, when T prints everything.")

;;; unused?
(defMacro veformat (&rest ll)
  `(if *ve-trace* (format *debug-io* ,@ll)))

;;;============================= Classes =========================================

(defClass value-editor-button (cg:button) ())

;;; create a class for displaying lists of values
(defClass edit-area (cg:multi-line-lisp-text) 
  ()
  (:documentation
   "pane for editing the value"))

;;;------------------------------------------------------------------ VALUE-EDITOR
;;; We define a default window for displaying entities using the window system
;;; The window shows the name of the class, the various properties

(defClass value-editor (cg:lisp-edit-window) 
  ((edited-symbol :accessor edited-symbol)
   (database-pathname :accessor database-pathname)
   (area :accessor area)
   (initial-value :accessor initial-value)
   (called-from-editor :accessor called-from-editor)
   )
  (:documentation 
   "Defines a window for editing the lisp values of some objects."))

;;;=========================== Service-functions =================================

;;;------------------------------------------------------ MAKE-VALUE-EDITOR-WINDOW

(defUn make-value-editor-window (object &optional called-from-editor)
  "Build a page for editing a non MOSS disk value.
Arguments:
   object: object whose value we want to edit
   called-from-editor: if t was called from an editor
Return:
   editor"
  (declare (special *value-editor*))
    ;; some information is kept inside the current page (after it is created)
    ;; create now a new page to display the object
    (setq *value-editor* 
          (make-window :value-editor :owner (cg:screen cg:*system*)
            :class 'value-editor
            :exterior nil
            :interior (cg:make-box *value-editor-left*
                                   *value-editor-top*
                                   (+ *value-editor-left* *value-editor-width*)
                                   (+ *value-editor-top* *value-editor-height*))
            :border :frame
            :child-p nil
            :close-button nil
            ;:font (make-font-ex :swiss "Arial / ANSI" 11)
            :font (make-font-ex :modern "FixedSys" 13)
            :form-state :normal
            :maximize-button t
            :minimize-button t
            :name :value-editor
            :pop-up nil
            :resizable t
            :scrollbars t
            :state :normal
            :system-menu t
            :title "Value Editor"
            :background-color +value-editor-color+
            :title-bar t
            :dialog-items (make-value-editor-widgets object called-from-editor)
            :form-p nil
            :form-package-name nil))
    ;(setf (current-entity current-page) current-entity)
    *value-editor*)

#|
(setq ll '((a 1)(b 2 2 2 (c 3 3 3 (d 4 4 4)))(e 5)))

(make-value-editor-window 'll)
|#
;;;----------------------------------------------------- MAKE-BROWSER-WIDGETS

(defUn make-value-editor-widgets (object &optional called-from-editor)
  "build the various areas to display object info.
Arguments:
   current-entity: id ob object to be displayed
Return:
   a list of widgets."
  
  ;; we first set up a header with buttons for saving the data, reindenting,
  ;; colorizing, committing, or aborting
  (append
   ;; save buton does not show if called from editor
   ;; rationale: we do not update value until committing the whole transaction
   (unless called-from-editor
     (list
      (cr-button 
      :class 'VALUE-EDITOR-BUTTON
      :left 3 :height 16 :width *value-editor-button-width* :top 8 :title "Save"
      :font +value-editor-button-text-font+
      :on-click VEW-save-on-click)))
   
   (list
    ;; the other areas are common to everybody
    
    ;;=== indenting button
    (cr-button 
     :class 'VALUE-EDITOR-BUTTON
     :left (+ 3 *delta*) :height 16 :width *value-editor-button-width* :top 8 
     :title "Reindent"
     :font +value-editor-button-text-font+
     :on-click VEW-reindent-on-click)
    
    ;;=== colorizing button
    (cr-button 
     :class 'VALUE-EDITOR-BUTTON
     :left (+ 3 (* 2 *delta*)) :height 16 :width *value-editor-button-width* 
     :top 8 :title "Colorize"
     :font +value-editor-button-text-font+
     :on-click VEW-colorize-on-click)
    
    ;;=== commit
    (cr-button 
     :class 'VALUE-EDITOR-BUTTON
     :left (+ 3 (* 4 *delta*)) :height 16 :width *value-editor-button-width*
     :top 8 :title "Commit"
     :font +value-editor-button-text-font+
     :on-click VEW-commit-on-click)
    
    ;;=== abort
    (cr-button 
     :class 'VALUE-EDITOR-BUTTON
     :left (+ 3 (* 5 *delta*)) :height 16 :width *value-editor-button-width*
     :top 8 :title "Abort"
     :font +value-editor-button-text-font+
     :on-click VEW-abort-on-click)
    
    ;;=== edit pane
    (make-instance 'cg:multi-line-lisp-text 
      ;:font (make-font-ex nil "Courier New" 13)
      :font (make-font-ex :modern "FixedSys" 13)
      :left 3 :top 35
      :height (- *value-editor-height* 38) 
      :width (- *value-editor-width* 6)
      :name :edit-item 
      :value (symbol-value object)
      )
    )))

;;;============================= Actions ====================================

;;;------------------------------------------------------- VEW-ABORT-ON-CLICK

(defun VEW-abort-on-click (dialog item)
  "restore the initial value of the symbol"
  (declare (ignore item dialog)(special *value-editor*))
  (set (edited-symbol *value-editor*) (initial-value *value-editor*))
  (%window-close *value-editor*)
  t)

;;;---------------------------------------------------- VEW-COLORIZE-ON-CLICK

(defun VEW-colorize-on-click (dialog item)
  (declare (ignore item))
  ;(print dialog)
  (cg:colorize-source-code dialog :entire-buffer t :skip-invalidation nil)
  )
  

;;;------------------------------------------------------ VEW-COMMIT-ON-CLICK
;;; if called from editor does nothing, closes window
;;; otherwise, must save new value onto disk before closing

(defun VEW-commit-on-click (dialog item)
  "if called-from-editor simply closes the window otherwise save value"
  (let ((val (%get-item-value item)))
    (set (edited-symbol *value-editor*) val)
    ;; save onto disk if not in edit session
    (unless (called-from-editor *value-editor*)
      (if (db-opened? (database-pathname *value-editor*))
          (db-with-transaction 
            (db-store (edited-symbol *value-editor*) val (area *value-editor*))
            )
        ;; otherwise complain
        (progn
          (%beep)
          (format t "~%; VEW-commit-on-click / database ~S not opened."
            (database-pathname *value-editor*)))))
    ;; exit
    (%window-close dialog)
    t))

;;;---------------------------------------------------- VEW-REINDENT-ON-CLICK

(defun VEW-reindent-on-click (dialog item)
  (declare (ignore item))
  (cg:reindent-region dialog)
  t)
  
;;;-------------------------------------------------------- VEW-SAVE-ON-CLICK

(defun VEW-save-on-click (dialog item)
  "only used when not calling from an editor"
  (declare (ignore dialog))
  (let ((val (%get-item-value item)))
    (set (edited-symbol *value-editor*) val)
    (if (db-opened? (database-pathname *value-editor*))
        (db-with-transaction 
         (db-store (edited-symbol *value-editor*) val (area *value-editor*))
         )
      ;; otherwise complain
      (progn
        (%beep)
        (format t "~%; VEW-save-on-click / database ~S not opened."
          (database-pathname *value-editor*)))))
  t)


(moss::format t "~%;*** MOSS v~A - value editor loaded ***" moss::*moss-version-number*)

;;; :EOF
