﻿;;;-*- Mode: Lisp; Package: "SOL-OWL" -*-
;;;===============================================================================
;;;20/01/30		
;;;		S O L 2 R U L E S - (File sol2rules-v3.0.lisp)
;;;	
;;;===============================================================================

#|
Copyright Université de Techonologie de Compiègne
contributor : Jean-Paul Barthès

barthes@utc.fr

This software is a computer program, the purpose of which is to compile 
ontologies written in the SOL (Simplified Ontology Language) format 
into a rule format.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

;;;============================================================================

This file contains a parser for the simplified OWL language that translates SOL
rules into a target rume format. Functions are called from the SOL2OWL compiler.
Rules are produced by virtual classes, virtual properties or SOL rules. They are
translated into different formats. Currently only the jena format and the SPARQL
formats are supported.

The grammar is the following:

(defvirtualconcept <mln>
  (:is-a <concept-ref>)
  (:def <concept-definition>)
  (:doc <documentation>))

<documentation> ::= (:doc <multilingual-string>)
<mln> ::= (:name <multilingual-string>)
<multilingual-string> ::= "{<language-marker> STRING}+"
<language-marker> ::= :en | :fr | :it | :pl

(defvirtualattribute <mln>
  (:class <class reference>)
  (:def <concept-definition>) 
  (:doc <documentation>))

(defvirtualrelation <mln>
  (:compose <property path>)
  (:doc <documentation>))

(defrule <mln>
  (:if {<term>}+)
  (:then {<term>}+)
  (:doc <documentation>))

<term> ::= (<node> <node> <node>) | (<builtin> <node> <node>)

<builtin> ::= builtin or user-defined operators
<arg> ::= literal

             
2006
 0603 Creation version 0
2007
 1022 adding the SPARQL format
 1210 change make-rule to make format argument optional
2008
 0114 cleaning up file. Default format in make-rule is cl-user::*rule-format*
 0228 adding :OR option for virtual classes with proper numbering
 0229 changed the format of record-rule
 0312 adding owl meta properties to arguments of clauses and is-owl-meta-ref? function
      and *owl-meta-concepts*
 0329 changed is-concept-ref? to process synonyms in virtual concept names
      make-rule-jena and make-rule-sparql: names set to first synonym
      improving the SPARQL parser
|#

(in-package :SOL-OWL)

;;;--------------------- macros for compiling the SOL file --------------------
;;; Those macros are used when compiling the input sol file and are defined
;;; for producing an OWL output (defined in owl2owl file)

#|
(defMacro defvirtualconcept (concept-name &rest option-list)
  `(apply #'make-virtual-concept ',concept-name ',option-list))

(defMacro defrule (&rest option-list)
  `(apply #'make-rule ',option-list))

(defMacro defvirtualattribute (property-name &rest option-list)
  `(apply #'make-virtual-attribute ',property-name ',option-list))

(defMacro defvirtualrelation (property-name &rest option-list)
  `(apply #'make-virtual-relation ',property-name ',option-list))

|#
;;;----------------------------------------------------------------------------

(defMacro r-error (rstring &rest args)
  "adds the error to the *error-message-list*"
  `(twarn ,rstring ,@args))

#|
? (macroexpand '(r-error "bad argument ~S" 23))
(PROGN (PUSH (FORMAT NIL "bad argument ~S" 23) *ERROR-MESSAGE-LIST*) NIL)
T
|#

(defMacro rformat (rstring &rest args)
  `(format *r-output* ,rstring ,@args))

;;;================================================================================
;;; Global lists to save definitions while processing data
;;;---------------------------- Globals ---------------------------------------

; already defined in sol2owl
;(defParameter *r-output* t "channel to rule file, default is listener")
;(defParameter *rule-error-list* nil "list of error strings")
;(defParameter *rule-file* nil)
;(defParameter *rule-prefix* "" "no prefix by default")
;(defParameter *sparql-rule-header* nil "to cache rule header")
;(defParameter *sparql-function-prefix* nil "name space of sparql functions")
;(defParameter *owl-meta-concepts*
;  '("owl:Class" "owl:DatatypeProperty" "owl:ObjectProperty" "owl:VirtualClass"
;    "owl:VirtualDatatypeProperty" "owl:VirtualObjectProperty")
;  )
;(defParameter *rule-builtin-operator-list*
;  '((:jena (:gt . "greaterThan")(> . "greaterThan")(:lt . "lessThan")(= . "equal")
;           (:getYears . "getYears"))))

;;;============================= Service Functions ================================

;;;------------------------------------------------------------------------ rrp

(defun rrp (ll) 
  "reverse prints the argument list to the rule file.
Arguments:
   ll: list of strings to print in reverse order
Return:
   :END"
  (progn (mapcar #'(lambda(xx) ;(terpri)(princ xx)) 
                     (format *r-output* "~&~A" xx))
                 ll) nil))

;;;================================= functions ====================================
;;;
;;; A SOL rule is defined with the following syntax:
;;;
;;; <rule> ::= <if-clause> <then-clause>
;;; <if-clause> ::= (:if <clause>+)
;;; <then-clause> ::= (:then <clause>+)
;;; <clause> ::= <attribute-clause> | <relation-clause> | <type-clause>
;;; <attribute-clause> ::= (<variable> <attribute-ref> <operator> <literal>*)
;;;                            | (<variable> <attribute-ref> <literal>)
;;; <relation-clause> ::= (<variable> <relation-ref> <variable>)
;;;                            | (<variable> <relation-ref> <object-ref>)
;;; <type-clause> ::= (<variable> :TYPE <class-ref>)
;;; <operator> ::= <predefined operators> | <user-defined-operators>
;;; <predefined-operators> ::= :LT | :LE | :EQ | :GE | :GT | :ALL 
;;; <object-ref> ::= string refering to an object, excluding literals
;;;
;;; Examples
;;;   (?V23 "brother" ?V24)
;;;   (?V25 :type "Adult")
;;;   (?V26 :age :gt 18)
;;;
;;; Since in an object reference the same string may reference a property or a
;;; concept, a convention to use in case of ambiguity is to start strings
;;; referring to properties with a + sign, e.g. "+ marital status"
;;; Globally, any string that cannot be referenced as something else is considered
;;; a literal

#|
;;;-------------------------------------------------------- is-attribute-clause?

(defun is-attribute-clause? (clause)
  "any list starting with a variable and an attribute reference is a clause"
  (if
    (and (listp clause)
         (rule-variable? (car clause))
         (is-attribute-ref? (cadr clause)))
    clause
    (terror "Bad attribute clause: ~S" clause)))
|#

#|
? (is-attribute-clause? '(?V23 "first name" "George"))
(?V23 "first name" "George")

? (catch :error (is-attribute-clause? '("name" :is "George")))
"Bad attribute clause: (\"name\" :IS \"George\")"
|#
;;;----------------------------------------------------------- IS-ATTRIBUTE-REF?
;;; modified to return attribute id JPB0802, synonyms 080329

(defun is-attribute-ref? (ref)
  "check if reference to an attribute or a virtual attribute. 
   Attributes may start with a + sign."
  (when (stringp ref)
    (let (attribute-name name-list result)
      ;; remove trailing spaces just in case
      (setq ref (string-trim '(#\space) ref))
      ;; some property strings start with + to distinguish from concept refs
      (if (eql #\+ (char ref 0))
        (setq ref (subseq ref 1)))
      ;; do it again in case there are spaces between "+" and name
      (setq ref (string-trim '(#\space) ref))
      ;; extract possible synonyms
      (setq name-list (extract-names ref))
      ;; check for each name in turn
      (dolist (name name-list)
        ;; cook up name
        (setq attribute-name (make-standard-name name))
        ;; check index entries
        (if
          (setq
           result
           (cond
            ((car (index-get name :att :id)))  ; takes care of "PersonnesAgees"
            ((car (index-get name :vatt :id)))
            ((car (index-get attribute-name :att :id))) ; "personnes agees"
            ((car (index-get attribute-name :vatt :id)))
            ))
          (return-from is-attribute-ref? result))))))

#| (index)
? (step (is-attribute-ref? "mother")) ; mother is a relation
NIL

? (is-attribute-ref? " birth date ")
"hasBirthDate"

? (is-attribute-ref? " +birth date ")
"hasBirthDate"

? (is-attribute-ref? "age ")
"hasAge"

? (is-attribute-ref? "+ age ")
"hasAge"

? (is-attribute-ref? " jour de naissance  ; age")
"hasAge"
|#
#|
(defun is-attribute-ref? (ref)
  "check if reference to an attribute or a virtual attribute. 
   Attributes may start with a + sign."
  (when (stringp ref)
    (let (attribute-name)
      ;; remove trailing spaces just in case
      (setq ref (string-trim '(#\space) ref))
      (if (eql #\+ (char ref 0))
        (setq ref (subseq ref 1)))
      ;; do it again in case there are spaces between "+" and name
      (setq ref (string-trim '(#\space) ref))
      ;; cook up name
      (setq attribute-name (make-standard-name ref))
      ;; check index entries
      (cond
       ((car (index-get ref :att :id)))  ; takes care of "PersonnesAgees"
       ((car (index-get ref :vatt :id)))
       ((car (index-get attribute-name :att :id))) ; "personnes agees"
       ((car (index-get attribute-name :vatt :id)))
       ))))
|#
;;;------------------------------------------------------------- IS-CONCEPT-REF?
;;; refs are entries into the index table and can be used directly, independently
;;; from the language... JPB0802
;;; We should take into account strings on meta-objects like "owl:Class" or
;;; "owl:DatatypeProperty". They are not part of the index table.

(defun is-concept-ref? (ref)
  "check if reference to a concept or a virtual concept. Watch the case of the labels...
Arguments:
   ref: a string, e.g. \"Person\" or \"= person\"
Return:
   something like ((:ID \"Z-Person\")) or nil if not in the index table"
  (let (name-list result)
    (when (stringp ref)
      ;; remove trailing spaces just in case
      (setq ref (string-trim '(#\space) ref))
      (if (eql #\= (char ref 0))
        (setq ref (subseq ref 1)))
      ;; check the case qhere we have a string containing synonyms
      (setq name-list (extract-names ref))
      ;; do it again in case there are spaces between "=" and name
      (dolist (name name-list)
        ;; when the language is not :en, (list cl-user::*language* ref) cooks up
        ;; something that is not in the index table. However, the string entry is in
        ;; it, so have a first try at it...
        (if
          (setq 
           result
           (car 
            (cond
             ((index-get name :class :id))
             ((index-get name :vclass :id))
             ((index-get (make-standard-name name) :class :id))
             ((index-get (make-standard-name name) :vclass :id))
             ((let ((concept-id (make-concept-id (list *language* name))))
                (cond 
                 ((index-get concept-id :class) concept-id)
                 ((index-get concept-id :vclass) concept-id)))))))
          (return-from  is-concept-ref? result))))))

#|
? (is-concept-ref? "brother")
NIL

? (is-concept-ref? "person")
"Z-Person"

? (is-concept-ref? "Adult")
"Z-Adult"

? (is-concept-ref? "Adulte")
"Z-Adult"

? (is-concept-ref? "personnes agees")
"Z-ElderlyPeople"

? (is-concept-ref? "PersonnesAgees")
"Z-ElderlyPeople"

? (is-concept-ref? "  =  personnes agees")
"Z-ElderlyPeople"

? (is-concept-ref? "personnes agees; vieillards")
"Z-ElderlyPeople"
|#
;;;-------------------------------------------------------- is-object-reference?

(defun is-individual-ref? (ref)
  "any string that makes a reference to an instance of some class."
  (when (stringp ref)
    (setq ref (string-trim '(#\space) ref))
    (if (eql #\* (char ref 0))
      (setq ref (subseq ref 1)))
    ;; do it again in case there are spaces between "*" and name
    (setq ref (string-trim '(#\space) ref))
    (car
     (cond
      ((index-get ref :inst :id))
      ((let ((individual-name (make-standard-name  ref)))
         (index-get individual-name :inst :id)))))))

#|
? (is-individual-ref? "brother")
nil

? (is-individual-ref? "france")
"z-france"

? (is-individual-ref? "italia")
"z-italy"

? (is-individual-ref? "* italie")
"z-italy"
|#
;;;------------------------------------------------------------ is-owl-meta-ref?

(defun is-owl-meta-ref? (ref)
  "checks if the reference is an owl metaclass including virtual ones.
   Ref must include the owl namespace prefix."
  (declare (special *owl-meta-concepts*))
  (when (stringp ref)
    (car (member ref *owl-meta-concepts* :test #'string-equal))))

#|
? (is-owl-meta-ref? "owl:virtualclass")
"owl:VirtualClass"

? (is-owl-meta-ref? "VirtualClass")
NIL
|#
;;;------------------------------------------------------------ IS-RELATION-REF?
;;; adding synonyms JPB080329

(defun is-relation-ref? (ref)
  "check if reference to a relation or a virtual relation.
   Relations may start with a + sign."
  (when (stringp ref)
    (let (relation-name name-list result)
      ;; remove traiming spaces just in case
      (setq ref (string-trim '(#\space) ref))
      ;; check leading + sign
      (if (eql #\+ (char ref 0))
        (setq ref (subseq ref 1)))
      ;; do it again
      (setq ref (string-trim '(#\space) ref))
      ;; check for synonyms
      (setq name-list (extract-names ref))
      ;; process each name in turn
      (dolist (name name-list)
        ;; cook up name
        (setq relation-name (make-standard-name name))
        (if
          (setq
           result
           ;; check index entries
           (cond
            ((car (index-get ref :rel :id)))  ; takes care of "PersonnesAgees"
            ((car (index-get ref :vrel :id)))
            ((car (index-get relation-name :rel :id))) ; "personnes agees"
            ((car (index-get relation-name :vrel :id)))))
        (return-from is-relation-ref? result))))))


#|
? (is-relation-ref? "brother")
"hasBrother"

? (is-relation-ref? "name")
NIL

? (is-relation-ref? "+name")
NIL

? (is-relation-ref? "uncle")
"hasUncle"

? (is-relation-ref? " + uncle")
"hasUncle"

? (is-relation-ref? "  tonton ;  oncle ")
"hasUncle"
|#
;;;--------------------------------------------------- IS-RULE-BUILTIN-OPERATOR?

(defun is-rule-builtin-operator? (expr)
  "checks if the expression id the name of a builtin operator.
Argument:
   expr: expression to check
Return:
   nil on fail, operator on success."
  (declare (special *rule-format* *rule-builtin-operator-list*))
  (cdr (assoc expr 
              (cdr (assoc *rule-format* *rule-builtin-operator-list*)))))

#|
? (let ((*rule-format* :jena))
     (declare (special *rule-format*))
     (is-rule-builtin-operator? ':gt))
"greaterThan"

? (let ((*rule-format* :sparql))
     (declare (special *rule-format*))
     (is-rule-builtin-operator? ':gt))
">"

? (let ((*rule-format* :sparql))
     (declare (special *rule-format*))
     (is-rule-builtin-operator? ':between))
NIL

? (let ((*rule-format* :sparql))
     (declare (special *rule-format*))
     (is-rule-builtin-operator? :equal))
"="
|#
;;;---------------------------------------------- IS-RULE-USER-DEFINED-OPERATOR?
;;; those are declared in the defontology macro and gathered onto a special
;;; list

(defun is-rule-user-defined-operator? (expr)
  "checks if the expression belongs to the *user-defined-operators* list.
   If expr is a string, must match operator exactly.
Argument:
   expr: expression to check
Return:
   nil on fail, operator string on success."
  (declare (special *user-defined-rule-operators*))
  (if (keywordp expr)
    (cdr (assoc expr *user-defined-rule-operators*))
    (cdr (rassoc expr *user-defined-rule-operators* :test #'equal))))

#|
? (is-rule-user-defined-operator? :getyears)
"getYears"

? (is-rule-user-defined-operator? "getYears")
"getYears"

? (is-rule-user-defined-operator? "getyears")
NIL
|#
;;;--------------------------------------------------------- IS-RULE-NODE-JENA?
;;; used by SPARQL, not by JENA

(defun is-rule-node? (expr)
  "a rule node is a reference to a class, a virtual class, a relation, ~
      an attribute, a virtual property, a keyword (e.g. :type), a number, ~
      a variable, a builtin, a string that does not refer to one of the previous ~
      types.
Arguments:
   expr: expression to analyse
Return:
   the equivalent jena string."
  (let (index result)
    (setq 
     result
     (cond
      ((numberp expr)(format nil "~S" expr))
      ((rule-variable? expr) (symbol-name expr))
      ;; builtin operator? (keyword)
      ((cdr (assoc expr (cdr (assoc *rule-format* 
                                    *rule-builtin-operator-list*)))))
      ;; user-defined operator (keyword)
      ((member expr *user-defined-rule-operators*))
      ;((keywordp expr)(symbol-name expr)
      ((keywordp expr) ; something like :type
       (concatenate 'string "rdf:" (string-downcase (symbol-name expr))))
      ))
    ;; if result get out
    (if result (return-from is-rule-node? result))
    ;; otherwise must be a string
    (if (stringp expr)
      ;; if a string try to get the corresponding object
      (dolist (name (extract-names expr) (return-from is-rule-node? expr))
        (setq index (make-index-string name))
        (setq 
         result
         (cond
          ((is-concept-ref? index))           ; (virtual) concept
          ((is-attribute-ref? index))         ; (virtual) attribute
          ((is-relation-ref? index))          ; (virtual) relation
          ((is-individual-ref? index))        ; individual
          ))
        (if result
          (return-from is-rule-node? 
            (concatenate 'string *rule-prefix* ":" result))))
      ;; otherwise not a string error
      (r-error "unknown node format" nil))))

#| (index) (step (is-rule-node? "GrandFather" ))
? (in-package :sol-owl)
#<Package "SOL-OWL">
? (is-rule-node? '?x)
"?X"
? (is-rule-node? 23)
"23"
? (is-rule-node? :type)
"rdf:type"
? (is-rule-node? " person  ")
"terregov:Z-Person"
? (is-rule-node? " name ")
"terregov:hasName"
? (is-rule-node? " uncle ")
"test4:hasUncle"
? (is-rule-node? " grand father ")
"terregov:hasGrandFather"
? (is-rule-node? "GrandFather")
"GrandFather"
? (is-rule-node? " italie ")
"terregov:z-italy"
? (is-rule-node? "Jean")
"Jean"
? (is-rule-node? "Medical Member")
"terregov:hasMedicalMember"
? (is-rule-node? " tonton ;  uncle; shushu ")
"tgv:hasUncle"
|#
#|
(defun is-rule-node? (expr)
  "a rule node is a reference to a class, a virtual class, a relation, ~
      an attribute, a virtual property, a keyword (e.g. :type), a number, ~
      a variable, a builtin, a string that does not refer to one of the previous ~
      types.
Arguments:
   expr: expression to analyse
Return:
   the equivalent jena string."
  (let (index temp)
    (cond
     ((numberp expr)(format nil "~S" expr))
     ((rule-variable? expr) (symbol-name expr))
     ;; builtin operator? (keyword)
     ((cdr (assoc expr (cdr (assoc cl-user::*rule-format* 
                                   *rule-builtin-operator-list*)))))
     ;; user-defined operator (keyword)
     ((member expr *user-defined-rule-operators*))
     ;((keywordp expr)(symbol-name expr)
     ((keywordp expr) ; something like :type
      (concatenate 'string "rdf:" (string-downcase (symbol-name expr))))
     ((not (stringp expr))
      (r-error "unknown node format" nil))
     ((and (setq index (make-index-string expr)) nil))
     ;; class
     ((setq temp (index-get index :class :id))
      (concatenate 'string *rule-prefix* ":" (car temp)))   ; concept
     ((setq temp (index-get index :vclass :id))
      (concatenate 'string *rule-prefix* ":" (car temp)))   ; virtual concept
     ((setq temp (index-get index :att :id))
      (concatenate 'string *rule-prefix* ":" (car temp)))   ; attribute
     ((setq temp (index-get index :rel :id))
      (concatenate 'string *rule-prefix* ":" (car temp)))   ; relation
     ((setq temp (index-get index :vatt :id))   
      (concatenate 'string *rule-prefix* ":" (car temp)))   ; virtual attribute
     ((setq temp (index-get index :vrel :id))   
      (concatenate 'string *rule-prefix* ":" (car temp)))   ; virtual relation
     ((setq temp (index-get index :inst :id))
      (concatenate 'string *rule-prefix* ":" (car temp)))   ; individual
     ;; otherwise return original string 
     ;;   (literal: keyword, number, variable, builtin, string)
     (t expr))))
|#
;;;------------------------------------------------------------------ MAKE-RULE
;;; JPB0802 change optional arg to key arg adding count option
(defun make-rule (rule-name if-part then-part doc 
                              &key (rule-output-format *rule-format*)
                              count)
  "produces a rule with the specified format to be printed into the sol.rules ~
      file. Checks for the existence of classes, properties or filter operators.
Arguments:
   rule-name: multilingual name
   if-part: list of premisses
   then-part: list of conclusions
   doc: eventual documentation (multilingual string)
   rule-output-format (key): currently :jena or :moss  (default :jena)
Return:
   a list of strings to be printed."
  ;; we process rules after everything else
  (case rule-output-format
    (:jena
     (make-rule-jena rule-name if-part then-part doc :count count))
    (:sparql
     (make-rule-sparql rule-name if-part then-part doc :count count))
    (:moss
     (make-rule-moss rule-name if-part then-part doc :count count))
    (t
     (terror "Unknown rule output format: ~S" rule-output-format)))  
  )

#|
? (make-rule '(:en "adult" :fr "adulte")
       '(:if (?* "age" ?x)
            (:ge ?x 18))
       '(then (?* :type "adult"))
       '(:doc :en "an adult is a person over 18.")
       :count 2)
### an adult is a person over 18.

[Role_Adulte-2:
   (?* rdf:type tgv:Z-Adult)
<-
   (?* tgv:hasAge ?X)
   ge(?X, 18)
 ]
NIL
|#
;;;--------------------------------------------------------- make-rule-arg-jena

(defun make-rule-arg-jena (expr)
  "takes an input and output some object usable by jena.
   If a number, output the number as a string
   if a keyword, error (null output)
   if a symbol, must be a variable
   if a string, then is considered a literal unless the string starts with
     + in which case it refers to a property
     = in which case it refers to a concept
     * in which case it refers to an individual"
  (let (temp )
    ;; remove trailing spaces
    (if (stringp expr) (setq temp (string-trim '(#\space) expr)))
    (cond
     ((numberp expr)(format nil "~S" expr))
     ((rule-variable? expr)  expr)
     ((keywordp expr) nil) ; not allowed
     ((not (stringp expr)) nil)
     ;; forced concept
     ((and (eql #\= (char expr 0))
           (setq temp (is-concept-ref? (subseq expr 1))))
      (concatenate 'string *rule-prefix* ":" temp))
     ;; forced property
     ((and (eql #\+ (char expr 0))
           (setq temp (is-attribute-ref? (subseq expr 1))))
      (concatenate 'string *rule-prefix* ":" temp))
     ((and (eql #\+ (char expr 0))
           (setq temp (is-relation-ref? (subseq expr 1))))
      (concatenate 'string *rule-prefix* ":" temp))
     ;; forced individual
     ((and (eql #\* (char expr 0))
           (setq temp (is-individual-ref? (subseq expr 1))))
      (concatenate 'string *rule-prefix* ":" temp))
     ;; otherwise literal
     (t (format nil "~S" expr)))))

#|
? (make-rule-arg-jena 21)
"21"

? (make-rule-arg-jena '?V34)
?V34

? (make-rule-arg-jena "person")
"\"person\""

? (make-rule-arg-jena "=person")
"test4:Z-Person"

? (make-rule-arg-jena "+person")
"\"+person\""

? (make-rule-arg-jena "+mother")
"test4:hasMother"

? (make-rule-arg-jena "+age")
"test4:hasAge"

? (make-rule-arg-jena "*france")
"test4:z-france"

? (make-rule-arg-jena "*mother")
"\"*mother\""

? (make-rule-arg-jena "=mother")
"\"=mother\""
|#
;;;-------------------------------------------- MAKE-RULE-ATTRIBUTE-CLAUSE-JENA

;;; we cannot use make-attribute-id or make-concept-id with a language different
;;; from English.
;;; Since qe only have a simple string in the clauses, we mus recover the concept
;;; id from the index table. 

(defun make-rule-attribute-clause-jena (clause)
  "format is
 attribute-clause
   (<subject> <attribute-ref> <object>)
   where
    <subject> ::= <variable> | <object-ref>
    <object> ::= <variable> | <literal> | <functor> <arg>*"
  (when (listp clause)
    (let (predicate subject object var temp)
      (unless 
        (and
         (is-attribute-ref? (cadr clause))
         ;; we must be careful: we can have the name of an attribute or of a 
         ;; virtual attribute
         (setq predicate 
               (concatenate 
                   'string *rule-prefix* ":" 
                 (car (or (index-get (make-index-string (cadr clause)) :att :id)
                          (index-get (make-index-string (cadr clause)) :vatt :id)))))
         (setq subject
               (cond
                ((rule-variable? (car clause)) (car clause))
                ((setq temp (is-concept-ref? (car clause)))
                 (concatenate 'string *rule-prefix* ":" temp))
                ((setq temp (is-attribute-ref? (car clause)))
                 (concatenate 'string *rule-prefix* ":" temp))
                ((setq temp (is-relation-ref? (car clause)))
                 (concatenate 'string *rule-prefix* ":" temp))
                ((setq temp (is-individual-ref? (car clause)))
                 (concatenate 'string *rule-prefix* ":" temp))
                )))
        (return-from make-rule-attribute-clause-jena nil))
      
      ;; now if subject and predicate are OK, check object
      (cond
       ((setq object
              (or  (is-rule-builtin-operator? (caddr clause))
                   (is-rule-user-defined-operator? (caddr clause))))
        (return-from make-rule-attribute-clause-jena
          (list (format nil "   (~A ~A ~A)" subject predicate (setq var (gensym "?V")))
                (format nil "   ~A(~A, ~{~A~^, ~})" object var
                        (mapcar #'make-rule-arg-jena (cdddr clause))))))
       ((setq object
              (cond
               ((rule-variable? (caddr clause)) (caddr clause))
               ;; test for literal
               ((not (or (is-concept-ref? (caddr clause))
                         (is-attribute-ref? (caddr clause))
                         (is-relation-ref? (caddr clause))
                         (is-individual-ref? (caddr clause))))
                (caddr clause))))
        (return-from make-rule-attribute-clause-jena
          (list (format nil "   (~A ~A ~A)" subject predicate object))))
       ))))

#| 
(make-rule-attribute-clause-jena '(?v23 "age" :gt 18))
("   (?V23 test4:hasAge ?V1686)" "   greaterThan(?V1686, 18)")

? (make-rule-attribute-clause-jena '(?v23 "mother" "france"))
NIL
? (make-rule-attribute-clause-jena '(?v23 "birth date" ?V78))
("   (?V23 test4:hasBirthDate ?V78)")

? (make-rule-attribute-clause-jena '(?v23 "birth date" "person"))
NIL
|#
;;;--------------------------------------------- MAKE-RULE-FUNCTION-CLAUSE-JENA

(defun make-rule-function-clause-jena (clause)
  "format is (:fcn <operator> <anything>)"
  (when (listp clause)
    (let (predicate)
      (if
        (and (eql :fcn (car clause))
             (setq predicate (or (is-rule-user-defined-operator? (cadr clause))
                                 (is-rule-builtin-operator? (cadr clause)))))
        (list (format nil "   ~A(?RESULT, ~{~A~^, ~})" 
                      predicate
                      (cddr clause)))))))

#|
? (make-rule-function-clause-jena '(:fcn :ge ?V44))
("   ge(?RESULT, ?V44)")

? (make-rule-function-clause-jena '(:fcn :getYears ?V44))
("   ge(?RESULT, ?V44)")

? (make-rule-function-clause-jena '(:fcn :type ?V44))
NIL
|#

;;;----------------------------------------------------------- make-rule-header

(defun make-rule-header (&rest option-list &aux op-location)
  "produces the necessary header for the rule file.
Arguments:
   option-list (rest): list of keyword arguments depending on the output format
         jena options are :prefix :include :comment
Return:
   :done"
  (declare (special *rule-prefix* *rule-prefix-ref* *sparql-rule-header*
                    *sparql-function-prefix*))
  (when (eql *compiler-pass* 1)
    (case *rule-format*
      (:jena
       (setq op-location 
             (cadr (assoc :jena (cdr (assoc :operator-location option-list)))))
       (rformat "~%###### Rules expressed in Jena" )
       ;; intialize rule prefix
       (rformat "~2%###### Operator List" )
       (when op-location
         (dolist (opr *user-defined-rule-operators*)
           (rformat "~%# operator=~A~A" op-location (cdr opr))))
       (rformat "~2%###### Prefix List" )
       (rformat "~%@prefix ~A: <~A>." *rule-prefix* *rule-prefix-ref*)
       (rformat "~%@include <RDFS>." )
       (rformat "~2%###### Include List" )
       (rformat "~%@include <OWLMicro>." )
       (rformat "~2%###### Virtual Entities" )
       )
      (:sparql
       ;; 
       (rformat "~%###--- SPARQL Rules ---###~%")
       ;; record sparql function prefix
       (setq *sparql-function-prefix* 
             (cadr (assoc :sparql (cdr (assoc :operator-location option-list)))))
       ;; initialize rule header
       (setq *sparql-rule-header*
             (apply #'make-rule-header-for-single-sparql-rule option-list))
       )
      )))

#|
? (let ((*compiler-pass* 1))
    (apply #'make-rule-header
         '((:name "Virtual Elements Ontology")
           (:version 1.0)
           (:doctype "owl" "http://www.w3.org/2002/07/owl#"
                     "tgv" "http://www.terregov.eupm.net/virtual/tg-virtual.owl#"
                     "xsd"       "http://www.w3.org/2001/XMLSchema#" )
           (:xmlns "rdf" "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                   "rdfs" "http://www.w3.org/2000/01/rdf-schema#"
                   "owl" "http://www.w3.org/2002/07/owl#"
                   "xsd" "&xsd;"
                   "self" "&tgv;")
           (:base "http://www.terregov.eupm.net/virtual/tg-virtual.owl")
           (:imports "http://www.w3.org/2000/01/rdf-schema"
                     "http://www.w3.org/1999/02/22-rdf-syntax-ns"
                     "http://www.w3.org/2002/07/owl")
           (:user-operators (:getyears (:jena "getYears")(:sparql "getYears")))
           (:operator-location 
            (:jena "net.eupm.terregov.categorization.jena.")
            (:sparql "tgfn:" "<java:net.eupm.terregov.categorization.fn.>"))
           )))

###### Rules expressed in Jena

###### Operator List
# operator=net.eupm.terregov.categorization.jena.getYears

###### Prefix List
@prefix tgv: <http://www.terregov.eupm.net/virtual/tg-virtual.owl#>.
@include <RDFS>.

###### Include List
@include <OWLMicro>.

###### Virtual Entities
|#
;;;------------------------------------ make-rule-header-for-single-sparql-rule

(defun make-rule-header-for-single-sparql-rule (&rest option-list)
  "produces the necessary header for each sparql rule. Caches the result on ~
      *sparql-rule-header*.
Arguments:
   option-list (rest): list of keyword arguments depending on the output format
         sparql options are :prefix :include :comment
Return:
   :done"
  (declare (special *rule-prefix* *rule-prefix-ref*))
  (let ((rdf-option (cdr (assoc :xmlns option-list)))
        (op-location 
         (cdr (assoc :sparql (cdr (assoc :operator-location option-list)))))
        rdf-location)
    (setq rdf-location (cadr (member "rdf" rdf-option :test #'string-equal)))
    (list
     (format nil "PREFIX ref: <~A>" rdf-location)
     (format nil "PREFIX ~A: <~A>." *rule-prefix* *rule-prefix-ref*)
     (if op-location
       (format nil "PREFIX ~A <java:~A>" (car op-location)(cadr op-location))
       ""))))

#|
###---Rule: hasAge---### 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX tgv: <http://www.terregov.eupm.net/ontology/2007/02/terregov.owl#>
PREFIX tgfn: <java:net.eupm.terregov.categorization.fn.>
? (setq *sparql-rule-header* nil)
NIL
? (apply #'make-rule-header-for-single-sparql-rule
         '((:name "Virtual Elements Ontology")
           (:version 1.0)
           (:doctype "owl" "http://www.w3.org/2002/07/owl#"
                     "tgv" "http://www.terregov.eupm.net/virtual/tg-virtual.owl#"
                     "xsd"       "http://www.w3.org/2001/XMLSchema#" )
           (:xmlns "rdf" "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                   "rdfs" "http://www.w3.org/2000/01/rdf-schema#"
                   "owl" "http://www.w3.org/2002/07/owl#"
                   "xsd" "&xsd;"
                   "self" "&tgv;")
           (:base "http://www.terregov.eupm.net/virtual/tg-virtual.owl")
           (:imports "http://www.w3.org/2000/01/rdf-schema"
                     "http://www.w3.org/1999/02/22-rdf-syntax-ns"
                     "http://www.w3.org/2002/07/owl")
           (:user-operators (:getyears (:jena "getYears")(:sparql "getYears")))
           (:operator-location 
            (:jena "net.eupm.terregov.categorization.jena.")
            (:sparql "tgfn:" "net.eupm.terregov.categorization.fn."))
           ))
("PREFIX ref: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
 "PREFIX tgv: <http://www.terregov.eupm.net/virtual/tg-virtual.owl#>."
 "PREFIX tgfn: <java:net.eupm.terregov.categorization.fn.>")
|#
;;;--------------------------------------------------- MAKE-RULE-IF-CLAUSE-JENA

(defun make-rule-if-clause-jena (clause)
  "parses an if-clause of a rule:
   {<term>}+
Argument:
   clause: list of terms to parse
Return:
   nil if parse fails or list of strings if parse OK."
  (let (result temp)
    ;; we should collects lines and return them
    ;; first take care of subject: variable, string
    (dolist (item clause)
      (unless (setq temp (make-rule-term-jena item))
        (format t " in ~S" item)
        (return-from make-rule-if-clause-jena nil))
      (result+ temp))
    result))

#|
? (make-rule-if-clause-jena '((?x :type "person")(?x "birth date" ?y)
    (:getYears ?y > 18)))
("   (?X rdf:type person)" "   (?X birth date ?Y)" "   getYears(?Y, greaterThan)")
|#
;;;------------------------------------------------- make-rule-if-clause-sparql

(defun make-rule-if-clause-sparql (clause)
  "parses an if-clause of a rule:
   {<term>}+
Argument:
   clause: list of terms to parse
Return:
   nil if parse fails or list of strings if parse OK."
  (let (result temp)
    ;; we should collects lines and return them
    ;; first take care of subject: variable, string
    (dolist (item clause)
      (unless (setq temp (make-rule-term-sparql item))
        (format t " in ~S" item)
        (return-from make-rule-if-clause-sparql nil))
      (result+ temp))
    result))

#|
? (make-rule-if-clause-sparql '((?x :type "person")(?x "birth date" ?y)
    (:getYears ?y > 18)))
("   ?X rdf:type tg:Z-Person ." "   ?X tg:hasBirthDate ?Y ."
 "   getYears ?Y ?V904 ." "   FILTER (> ?V904 18)")
|#
;;;------------------------------------------------------------- MAKE-RULE-JENA

(defun make-rule-jena
       (rule-name if-part then-part doc &key count)
  "produces a rule with the jena format to be printed into the sol.rules ~
      file. Checks for the existence of classes, properties or filter operators.
Arguments:
   rule-name: multilingual name
   if-part: list of premisses
   then-part: list of conclusions
   doc: eventual documentation (multilingual string)
   count (key): count numbering the rule (:OR option)
Return:
   a list of strings to be printed."
  (declare (special *language*))
  (let (result)
    ;; print doc line as a single line comment (~@? directive)
    (result+ 
     (list (format nil "~%### ~@?" 
                   (mln::get-canonical-name (cdr doc) *language*))))
    ;; start rule by printing rule name (first synonyms)
    (result+ (list (format nil "~%[Role_~A~A:"
                             (make-index-string
                              (mln::get-canonical-name rule-name *language*))
                           (if (numberp count) (format nil "-~A" count) ""))))
    ;; process first conclusions
    (result+
     (make-rule-then-clause-jena (cdr then-part)))
    (result+ '("<-"))
    ;; check and print each condition
    (result+
     (make-rule-if-clause-jena (cdr if-part)))
    ;; close the rule
    (result+ (list " ]"))
    (rrp result)))

#|
? (make-rule-jena '(:en "adult" :fr "adulte")
       '(:if (:* "age" ?x)
            (:ge ?x 18))
       '(then (:* :type "adult"))
       '(:doc :en "an adult is a person over 18."))
### an adult is a person over 18.

[Role_Adulte:
   (?* rdf:type tgv:Z-Adult)
<-
   (?* tgv:hasAge ?X)
   ge(?X, 18)
 ]

? (make-rule-jena '(:en "adult" :fr "adulte")
       '(:if (?* "age" ?x)
            (:ge ?x 18))
       '(then (?* :type "adult"))
       '(:doc :en "an adult is a person over 18.")
       :count 1)
### an adult is a person over 18.

[Role_Adulte-1:
   (?* rdf:type tgv:Z-Adult)
<-
   (?* tgv:hasAge ?X)
   ge(?X, 18)
 ]
|#
;;;------------------------------------------------------------- make-rule-moss

(defun make-rule-moss (&rest dummy)
  "dummy function to avoid compiler messages"
  dummy)

;;;--------------------------------------------- MAKE-RULE-OPERATOR-CLAUSE-JENA
;;; should include multiple n-ary operators...

(defun make-rule-operator-clause-jena (clause &aux arg1 arg2)
  "format is  (<builtin-operator> <anything> <anything>)"
  (when (listp clause)
    (let (predicate)
      (and (setq predicate (is-rule-builtin-operator? (car clause)))
           (setq arg1 (make-rule-arg-jena (cadr clause)))
           (setq arg2 (make-rule-arg-jena (caddr clause)))
           (list (format nil "   ~A(~A, ~A)" predicate arg1 arg2))))))

#|
? (make-rule-operator-clause-jena '(:ge ?V77 ?V99))
("   ge(?V77, ?V99)")

;; this example soed not make sense. Args should be variables or literals
? (make-rule-operator-clause-jena '(:ge "=person" "france"))
("   ge(test4:Z-Person, \"france\")")

? (make-rule-operator-clause-jena '(:ge ?V55 12))
("   ge(?V55, 12)")

? (make-rule-operator-clause-jena '(:ge "+age" 23))
("   ge(tgv:hasAge, 23)") ; nonsensical clause

? (make-rule-operator-clause-jena '(:ge "age" 23))
("   ge(\"age\", 23)") ; nonsensical clause
|#
;;;--------------------------------------------- MAKE-RULE-RELATION-CLAUSE-JENA

;;; we should allow strings that refer to meta-objects like "owl:DatatypeProperty"
;;; or "owl:VirtualClass". Currently those are not object references

(defun make-rule-relation-clause-jena (clause)
  "format is  
;;; relation-clause
;;;   (<subject> <relation-ref> <object>)
;;;   where
;;;    <subject> ::= <variable> | <object-ref>
;;;    <object> ::= <variable> | <object-ref>
"
  (when (listp clause)
    (let (predicate subject object temp)
      (and
       (setq temp (is-relation-ref? (cadr clause)))
       (setq predicate 
             (concatenate 'string *rule-prefix* ":" temp))
       (setq subject
             (cond
              ((rule-variable? (car clause)) (car clause))
              ((setq temp (is-concept-ref? (car clause)))
               (concatenate 'string *rule-prefix* ":" temp))
              ((setq temp (is-attribute-ref? (car clause)))
               (concatenate 'string *rule-prefix* ":" temp))
              ((setq temp (is-relation-ref? (car clause)))
               (concatenate 'string *rule-prefix* ":" temp))
              ((setq temp (is-individual-ref? (car clause)))
               (concatenate 'string *rule-prefix* ":" temp))))
       (setq object
             (cond
              ((rule-variable? (caddr clause)) (caddr clause))
              ((setq temp (is-concept-ref? (caddr clause)))
               (concatenate  'string *rule-prefix* ":" temp))
              ((setq temp (is-attribute-ref? (car clause)))
               (concatenate 'string *rule-prefix* ":" temp))
              ((setq temp (is-relation-ref? (caddr clause)))
               (concatenate 'string *rule-prefix* ":" temp))
              ((setq temp (is-individual-ref? (caddr clause)))
               (concatenate 'string *rule-prefix* ":" temp))))
       (list (format nil "   (~A ~A ~A)" subject predicate object)))
      )))

#| (index)
? (make-rule-relation-clause-jena '(:ge "age" 23))
NIL
? (make-rule-relation-clause-jena '(:ge "mother" 23))
NIL

(make-rule-relation-clause-jena '(?V33 "father" ?V56))
("   (?V33 test4:hasFather ?V56)")

(make-rule-relation-clause-jena '(?V33 "father" "person"))
("   (?V33 test4:hasFather test4:Z-Person)")

? (make-rule-relation-clause-jena '(?V33 "uncle" "person"))
("   (?V33 test4:hasUncle test4:Z-Person)")

? (make-rule-relation-clause-jena '(?V33 "uncle" "personnes agees"))
("   (?V33 tgv:hasUncle tgv:Z-ElderlyPeople)")

(make-rule-relation-clause-jena '(?X "grand father" ?Z))
|#
;;;----------------------------------------------------------- make-rule-sparql

(defun make-rule-sparql
       (rule-name if-part then-part doc &key count)
  "produces a rule with the sparql format to be printed into the sol.rules ~
      file. Checks for the existence of classes, properties or filter operators.
Arguments:
   rule-name: multilingual name
   if-part: list of premisses
   then-part: list of conclusions
   doc: eventual documentation (multilingual string)
  count (key): count when :OR option
Return:
   a list of strings to be printed."
  (declare (ignore doc)(special *sparql-rule-header*))
  (let (result)
    ;; print doc line as a single line comment (~@? directive)
    ;(result+ 
    ; (list (format nil "~%// ~@?" 
    ;               (sol-html::get-language-string (cdr doc) cl-user::*language*))))
    ;; start rule by printing rule name (first-synonym)
    (result+ (list (format nil "~%###---Rule: ~A~A ---###"
                             (make-index-string
                              (mln::get-canonical-name rule-name *language*))
                           (if (numberp count) (format nil "-~A" count) ""))))
;;;    (result+ (list (format nil "~%###---Rule: ~A~A ---###" 
;;;                           (car   ;; JPB080329
;;;                            (extract-names   ;; JPB080329
;;;                             (make-index-string
;;;                              (sol-html::get-language-string 
;;;                               rule-name *language*))))
;;;                           (if (numberp count) (format nil "-~A" count) ""))))
    (result+ *sparql-rule-header*)
    (result+ (list "CONSTRUCT{"))
    ;; process first conclusions
    (result+
     (make-rule-then-clause-sparql (cdr then-part)))
    (result+ '("}
WHERE{"))
    ;; check and print each condition
    (result+
     (make-rule-if-clause-sparql (cdr if-part)))
    ;; close the rule
    (result+ (list "}"))
    (rrp result)))

#|
? (make-rule-sparql
    '(:name :en "Italian Citizenship - Rule")
    '(:if
       (?x :type "Person")
       (?x "Country" ?Y)
       (?y " name"  :equal "italie"))
   '(:then
       ("terregov:citizenship" :type "owl:ObjectProperty")
       (?x "terregov:citizenship" " italie"))
    '(:doc :en "Italian citizens are those with country Italy")
 )

[Rule: ItalianCitizenship-Rule]
CONSTRUCT{
   terregov:citizenship rdf:type owl:ObjectProperty .
   ?X terregov:citizenship tg:z-italy .
}
    WHERE{
   ?X rdf:type tg:Z-Person .
   ?X tg:Z-Country ?Y .
   ?Y tg:hasName ?V905 .
   FILTER (= ?V905 italie)
}
|#
;;;-------------------------------------------------------- MAKE-RULE-TERM-JENA
;;; Jena possible SOL clauses are
;;; function clause
;;;   (:fcn <operator> <anything>)
;;; attribute-clause
;;;   (<subject> <attribute-ref> <object>)
;;;   where
;;;    <subject> ::= <variable> | <object-ref>
;;;    <object> ::= <variable> | <literal> | <functor> <arg>*
;;; operator-clause
;;;   (<builtin-operator> <anything> <anything>)
;;; relation-clause
;;;   (<subject> <relation-ref> <object>)
;;;   where
;;;    <subject> ::= <variable> | <object-ref>
;;;    <object> ::= <object-ref>
;;; type-clause
;;;   (<subject> :type <object>)
;;;   where
;;;    <subject> ::= <variable> | <object-ref>
;;;    <object> ::= <variable> | <concept-ref>
;;;

(defun make-rule-term-jena (expr)
  "checks whether the list represents a SOL rule term:
   (node node node) | (node node functor {arg}*) | (builtin node node)
arguments:
   expr: expression to analyse
Return:
   nil or the equivalent jena string."
  (or
   (make-rule-function-clause-jena expr)
   (make-rule-operator-clause-jena expr)
   (make-rule-attribute-clause-jena expr)
   (make-rule-relation-clause-jena expr)
   (make-rule-type-clause-jena expr)
   (list (format nil "Bad format for rule term: ~S" expr))))

#|
? (make-rule-term-jena '(:fcn  :getYears ?Y))
("   getYears(?RESULT, ?Y)")

? (make-rule-term-jena '(?v23 "age" :gt 18))
("   (?V23 test4:hasAge ?V1695)" "   greaterThan(?V1695, 18)")

? (make-rule-term-jena '(?v23 "mother" "france"))
("   (?V23 test4:hasMother test4:z-france)")

? (make-rule-term-jena '(?v23 "mother ; mater" "france"))
("Bad format for rule term: (?V23 \"mother ; mater\" \"france\")")

? (make-rule-term-jena '(?v23 "father" ?V99))
("   (?V23 test4:hasFather ?V99)")

? (make-rule-term-jena '(?v23 "birth date" ?V78))
("   (?V23 test4:hasBirthDate ?V78)")

? (make-rule-term-jena '(?v23 "birth date" "person"))
("Bad format for rule term: (?V23 \"birth date\" \"person\")")

? (make-rule-term-jena '(:fcn :ge ?V44))
("   ge(?RESULT, ?V44)")

? (make-rule-term-jena '(:fcn :type ?V44))
("Bad format for rule term: (:FCN :TYPE ?V44)")

? (make-rule-term-jena '(:ge ?V77 ?V99))
("   ge(?V77, ?V99)")

? (make-rule-term-jena '(:ge "=person" "france"))
("   ge(test4:Z-Person, \"france\")")

? (make-rule-term-jena '(:ge ?V55 12))
("   ge(?V55, 12)")

? (make-rule-term-jena '(:ge "age" 23)) ;******
("   ge(\"age\", 23)")

? (make-rule-term-jena '(:ge "mother" 23))
("   ge(\"mother\", 23)")

? (make-rule-term-jena '(?V33 "mother" ?V56))
("   ge(\"mother\", 23)")

? (make-rule-term-jena '(?V33 "mother" "person"))
("   (?V33 test4:hasMother test4:Z-Person)")

? (make-rule-term-jena '(?V33 "uncle" "person"))
("   (?V33 test4:hasUncle test4:Z-Person)")

? (make-rule-term-jena '(?v23 :type 23))
("Bad format for rule term: (?V23 :TYPE 23)")

? (make-rule-term-jena '(?v23 :type ?V45))
("   (?V23 rdf:type ?V45)")

? (make-rule-term-jena '(?v23 :type "person"))
("   (?V23 rdf:type test4:Z-Person)")

? (make-rule-term-jena '(?v23 :type "adult"))
("   (?V23 rdf:type test4:Z-Adult)")

? (make-rule-term-jena '("france" :type "adult"))
("   (test4:z-france rdf:type test4:Z-Adult)")
|#

;;;----------------------------------------------------------- make-rule-sparql

(defun make-rule-term-sparql (expr)
  "checks whether the list represents a SOL rule term:
   (node node node) | (node node functor {arg}*) | (builtin node node)
arguments:
   expr: expression to analyse
Return:
   nil or the equivalent jena string."
  (let (temp1 temp2 temp3 var)
    (when (listp expr)
      (cond
       ;; function clause (:fcn "getYears" ?x) => (?X tgfn:getYears ?RESULT)
       ;; where tgfn is the operator namespace, ?RESULT what we look for
       ((and (eql (car expr) :fcn)
             (setq temp2 (is-rule-node? (cadr expr)))
             (setq temp3 (is-rule-node? (caddr expr))))
        (list (format nil "   ~A ~A~A ?RESULT ." 
                      temp3 *sparql-function-prefix* temp2)))
       ;; builtin
       ((and (setq temp1 (is-rule-user-defined-operator? (car expr)))
             (setq temp2 (is-rule-node? (cadr expr)))
             (setq temp3 (is-rule-node? (caddr expr))))
        (list (format nil "   FILTER (~A ~A ~A)" temp1 temp2 temp3)))
       ;; (node predicate builtin {arg}*)
       ((and (setq temp1 (is-rule-node? (car expr)))
             (setq temp2 (is-rule-node? (cadr expr)))
             (setq temp3 (is-rule-builtin-operator? (caddr expr))))
        (list (format nil "   ~A ~A ~A ." temp1 temp2 (setq var (gensym "?V")))
              (format nil "   FILTER (~A ~A ~{~A~^, ~})" temp3 var (cdddr expr))))
       ;; (node predicate node)
       ((and (setq temp1 (is-rule-node? (car expr)))
             (setq temp2 (is-rule-node? (cadr expr)))
             (setq temp3 (is-rule-node? (caddr expr))))
        (list (format nil "   ~A ~A ~A ." temp1 temp2 temp3)))
       ;; otherwise return nil
       ))))

#|
? (make-rule-term-sparql '(:fcn "getYears" ?y))
("   ?Y tgfn:getYears ?RESULT .")

? (make-rule-term-sparql '(?* "age" :ge 65))
("   ?* tgv:hasAge ?V1355 ." "   FILTER (>= ?V1355 65)")

? (make-rule-term-sparql '(?* "birth date place" "france"))
("   ?* tgv:hasBirthDatePlace tgv:z-france .")

? (make-rule-term-sparql '(?x :type "person"))
("   ?X rdf:type tgv:Z-Person .")

? (make-rule-term-sparql '(?x "birth date" ?y))
("   ?X tgv:hasBirthDate ?Y .")

? (make-rule-term-sparql '(:getYears ?y > 18))
NIL
|#
;;;------------------------------------------------- MAKE-RULE-THEN-CLAUSE-JENA

(defun make-rule-then-clause-jena (clause)
  "parses a then clause of a rule.
Argument:
   clause: clause to parse
Return:
   nil if parse fails or list of strings if parse OK."
  (let (result temp)
    ;; we should collects lines and return them
    ;; first take care of subject: variable, string
    (dolist (item clause)
      (unless (setq temp (make-rule-term-jena item))
        (format t ";*** can't make then clause with ~S" item)
        (return-from make-rule-then-clause-jena nil))
      (result+ temp))
    result))

;;;----------------------------------------------- make-rule-then-clause-sparql

(defun make-rule-then-clause-sparql (clause)
  "parses a then clause of a rule.
Argument:
   clause: clause to parse
Return:
   nil if parse fails or list of strings if parse OK."
  (let (result temp)
    ;; we should collects lines and return them
    ;; first take care of subject: variable, string
    (dolist (item clause)
      (unless (setq temp (make-rule-term-sparql item))
        (format t " in ~S" item)
        (return-from make-rule-then-clause-sparql nil))
      (result+ temp))
    result))

;;;------------------------------------------------- MAKE-RULE-TYPE-CLAUSE-JENA

(defun make-rule-type-clause-jena (clause)
  "format is 
;;;   (<subject> :type <object>)
;;;   where
;;;    <subject> ::= <variable> | <object-ref>
;;;    <object> ::= <variable> | <concept-ref>"
  (when (listp clause)
    (let (subject predicate object temp)
      (and 
       (setq predicate (if (eql (cadr clause) :type) "rdf:type"))
       (setq subject
             (cond
              ((rule-variable? (car clause)) (car clause))
              ((setq temp (is-concept-ref? (car clause)))
               (concatenate 'string *rule-prefix* ":" temp))
              ((setq temp (is-attribute-ref? (car clause)))
               (concatenate 'string *rule-prefix* ":" temp))
              ((setq temp (is-relation-ref? (car clause)))
               (concatenate 'string *rule-prefix* ":" temp))
              ((setq temp (is-individual-ref? (car clause)))
               (concatenate 'string *rule-prefix* ":" temp))))
       (setq object
             (cond
              ((rule-variable? (caddr clause)) (caddr clause))
              ((setq temp (is-concept-ref? (caddr clause)))
               (concatenate 'string *rule-prefix* ":" temp))))
       (list (format nil "   (~A ~A ~A)" subject predicate object))))))

#|
? (make-rule-type-clause-jena '(?v23 "age" :gt 23))
NIL

? (make-rule-type-clause-jena '(?v23 :type 23))
NIL

? (make-rule-type-clause-jena '(?v23 :type ?V45))
("   (?V23 rdf:type ?V45)")

? (make-rule-type-clause-jena '(?v23 :type "person"))
("   (?V23 rdf:type test4:Z-Person)")

? (make-rule-type-clause-jena '(?v23 :type "personnes agees"))
("   (?V23 rdf:type tgv:Z-ElderlyPeople)")

? (make-rule-type-clause-jena '(?v23 :type "adult"))
("   (?V23 rdf:type test4:Z-Adult)")

? (make-rule-type-clause-jena '("person" :type "adult"))
("   (test4:Z-Person rdf:type test4:Z-Adult)")

? (make-rule-type-clause-jena '("france" :type "adult"))
("   (test4:z-france rdf:type test4:Z-Adult)")
|#
;;;-------------------------------------------------------------- make-sol-rule

(defun make-sol-rule (rule-name if-part then-part doc 
                                   &optional (rule-output-format :jena) &aux result)
  "produces a rule with the specified format to be printed into the sol.rules ~
      file. Checks for the existence of classes, properties or filter operators.
   Checks the SOL syntax.
Arguments:
   rule-name: multilingual name
   if-part: list of premisses
   then-part: list of conclusions
   doc: eventual documentation (multilingual string)
   rule-output-format: currently :jena or :moss  (default :jena)
Return:
   a list of strings to be printed."
  ;; we process rules after everything else
  (with-warning (rule-name)
    ;(is-sol-rule? (list if-part then-part))
    ;; if OK check output format
    (case rule-output-format
      (:jena
       (make-rule-jena rule-name if-part then-part doc))
      (:sparql
       (make-rule-sparql rule-name if-part then-part doc))
      (:moss
       (make-rule-moss rule-name if-part then-part doc))
      (t
       (terror "Unknown rule output format: ~S" rule-output-format)))
    ))

#|
? (make-sol-rule '(:en "Adult")
                '(:if (?p :type "person")(?p "age" :ge "18")) 
                '(:then (?p :type "adult")) 
                '(:doc :en "test for rules")
      :jena)
### test for rules

[Role_Adult:
   (?P rdf:type test4:Z-Adult)
<-
   (?P rdf:type test4:Z-Person)
   (?P test4:hasAge ?V1616)
   ge(?V1616, "18")
 ]
:END
|#
;;;-------------------------------------------- make-user-defined-operator-list

(defun make-user-defined-operator-list (&rest option-list)
  "from the option list of defontology initializes the list of operators"
  (declare (special *user-defined-rule-operators*))
  (let ((operators (cdr (assoc :user-operators option-list)))
        op-list)
    (dolist (clause operators)
      ;; something like (:GET-YEARS (:JENA "getYears") (:SPARQL "getYears"))
      (if (assoc *rule-format* (cdr clause))
        (pushnew (cons (car clause) (cadr (assoc *rule-format* (cdr clause))))
                 op-list :test #'equal)))
    (setq *user-defined-rule-operators* (reverse op-list))))

;;;------------------------------------------------ make-virtual-attribute-rule

(defun make-virtual-attribute-rule (mln &rest option-list)
  "creates a virtual attribute expressed a rule. Called during the ~
      second compiler pass.
Syntax is:
   (defvirtualattribute (:name :en \"age\" :fr \"âge\")
        (:class \"person\")
        (:def
             (?class \"birth date\" ?y)
             (\"getYears\" ?y ?*)
            )
        (:type :att)
        (:doc :en \Òthe attribute age is computed from the birthdate of a~
      person by applying the function getYears to the birthdate.\"))
Arguments:
   mln: multilingual name naming the virtual concept
   option-list (rest): list of other options (:def, :doc)
Return:
   list of string specifying the rule, nil if parsing fails."
  (declare (special *language* *rule-format*))
  (let ((class-name (cadr (assoc :class option-list)))
        (virtual-prop-ref (make-index-string
                           (mln::get-canonical-name mln *language*))))
    ;; create rule
    (make-rule mln 
               `(:if (?SELF :type ,class-name)
                  ,@(cdr (subst '?SELF :* (assoc :def option-list))))
               `(:then 
                 ;(,virtual-prop-ref :type "owl:DatatypeProperty")
                 (?SELF ,virtual-prop-ref ?result))
               (assoc :doc option-list)
               :rule-output-format *rule-format*)))

;;;-------------------------------------------------- make-virtual-concept-rule
;;; during compiler PASS 1 an entry is created in the INDEX table to track the
;;; existence of the virtual class. The function simply creates the corresponding
;;; rule
;;;
;;; Syntax:
;;;   (defvirtualconcept <mln>
;;;     (:is-a <concept-ref>)
;;;     (:def <concept-definition>)
;;;     (:doc <documentation>))

(defun make-virtual-concept-rule (mln &rest option-list)
  "creates a virtual concept rule from definition.
Syntax is:
   (defvirtualconcept (:name :en \"Adult\" :fr \"Adulte\")
        (:is-a  \"person \")
        (:def 
             (?* \"age\" > 18))
        (:doc :en \"An ADULT is a person over 18.\"))
Arguments:
   mln: multilingual name naming the virtual concept
   omtion-list (rest): list of other options (:def, :doc)
Return:
   list of string specifying the rule, nil if parsing fails."
  (declare (special *language* *rule-format*))
  ;;;  (unless (eql  *compiler-pass* 2)
  ;;;    (error "should be called only after compiler second pass: ~S" mln))
  
  ;(break "make-virtual-concept-rule")
  
  (let (virtual-class-name class-ref count rule-list def)
    
    (setq virtual-class-name (mln::get-canonical-name mln *language*))
    ;; a virtual class must have a reference class with an :is-a option
    (setq class-ref (cadr (assoc :is-a option-list)))
    (unless class-ref (r-error "Missing class reference while defining ~S" 
                               class-ref))
    ;; and a definition
    (setq def (assoc :def option-list))
    (unless def (r-error "Missing definition while defining ~S" 
                         class-ref))
    
    ;; check for an OR clause, if not, make a list of the single rule
    (setq rule-list 
          (if (and (listp (cadr def)) (eql :OR (caadr def)))
              (cdadr def)
            (cdr def)))
    ;; count is used for numbering the rules (nil if single rule)
    (setq count (if (cdr rule-list) 0))
    
    ;; create and print rule(s)
    (dolist (rule rule-list)
      ;(format t "~%;--- ?*: ~S" '?*)
      ;(format t "~&===> make-virtual-concept-rule rule: ~S" rule)
      (rrp 
       (make-rule mln  
                  `(:if 
                    ,(list '?SELF :type class-ref)
                    ,(subst '?SELF :* rule))
                  `(:then 
                    (?SELF :type  ,virtual-class-name)
                    )
                  (assoc :doc option-list)
                  :rule-output-format *rule-format*
                  :count (if (numberp count)(incf count)) ; for :OR option
                  )))))

#|
? (MAKE-VIRTUAL-CONCEPT-RULE
           '(:EN "adult" :FR "Adulte")
           '(:NAME (:EN "adult" :FR "Adulte"))
           '(:IS-A "person")
           '(:DEF (?* "age" :GE 18))
           '(:DOC :EN "Adults are persons over 18 years."))
### Adults are persons over 18 years.

[Role_Adulte:
   (?SELF rdf:type tgv:Z-Adult)
<-
   (?SELF rdf:type tgv:Z-Person)
   (?SELF tgv:hasAge ?V1257)
   ge(?V1257, 18)
 ]
? (MAKE-VIRTUAL-CONCEPT-RULE
           '(:en "People with Contract")
           '(:name :en "People with Contract")
           '(:IS-A "person")
           '(:DEF (:or (?* "marital status" "pacsÃ©")
                       (?* "marital status" "married")))
           '(:doc :en "People with contract are people whose marital status is ~
                       based on a contract."))

### People with contract are people whose marital status is based on a contract.

[Role_PeopleWithContract-1:
   (?SELF rdf:type tgv:Z-PeopleWithContract)
<-
   (?SELF rdf:type tgv:Z-Person)
   (?SELF tgv:hasMaritalStatus tgv:z-pacsÃ©)
 ]

### People with contract are people whose marital status is based on a contract.

[Role_PeopleWithContract-2:
   (?SELF rdf:type tgv:Z-PeopleWithContract)
<-
   (?SELF rdf:type tgv:Z-Person)
   (?SELF tgv:hasMaritalStatus tgv:z-married)
 ]
NIL
|#
;;;------------------------------------------------- make-virtual-relation-rule

(defun make-virtual-relation-rule (mln &rest option-list &aux compose)
  "creates a virtual relation expressed by a rule.
Syntax is:
     (defvirtualrelation (:name :en \"uncle\" :fr \"oncle\")
        {(:class \"person\")}
        {(:to \"person\")}
        (:compose  \"father\" \"brother\")
        {(:max 12)}
        (:doc :en \"the property uncle is composed from father and brother.\"))
Arguments:
   mln: multilingual name naming the virtual relation
   option-list (rest): list of other options (:def, :doc)
Return:
   list of string specifying the rule, nil if parsing fails."
  (when (setq compose (assoc :compose option-list))
    (let ((virtual-prop-name (mln::get-canonical-name mln *language*))
          count rule-list var init-var result)
      ;; check for an OR clause, if not, make a list of the single rule
      (setq rule-list 
            (if (and (listp (cadr compose)) (eql :OR (caadr compose)))
              (cdadr compose)
              (list (cdr compose))))
      ;; count is used for numbering the rules (nil if single rule)
      (setq count (if (cdr rule-list) 0))
      
      ;; check that the composition chain consists of relations or virtual relations
      ;(unless (valid-composition-path? rule-list :type :relation)
      ;  (terror "invalid composition path: ~S" compose))
      ;(print `(++++++++++ rule-list ,rule-list))
      ;; create composed rule
      (dolist (rule-compose rule-list)
        (setq var (gensym "?V") init-var var result nil)
        (rrp
         (make-rule
          mln
          `(:if
             ,@(dolist (item rule-compose result)
                 (result+ (list (list var item (setq var (gensym "?V")))))
                 ;(print result)
                 ))
          `(:then
            ;; if the last property of the chain is an attribute the 
            ;; virtual property is an attribute otherwise a relation
            ;(,virtual-prop-name :type "owl:ObjectProperty")
            (,init-var ,virtual-prop-name ,var))
          (assoc :doc option-list)
          :rule-output-format *rule-format*
          :count (if (numberp count)(incf count)) ; for :OR option
          ))))))

#|
? (apply #'make-virtual-relation-rule '(:EN "uncle" :FR "oncle") 
             (index-get (make-relation-id '(:EN "uncle" :FR "oncle")) :vrel :def))
### an uncle is a person who is the brother of the mother or the brother of the father.

[Role_Oncle-1:
Bad format for rule term: (#:?V1745 "oncle" #:?V1747)
<-
   (?V1745 tgv:hasMother ?V1746)
   (?V1746 tgv:hasBrother ?V1747)
 ]

### an uncle is a person who is the brother of the mother or the brother of the father.

[Role_Oncle-2:
Bad format for rule term: (#:?V1748 "oncle" #:?V1750)
<-
   (?V1748 tgv:hasFather ?V1749)
   (?V1749 tgv:hasBrother ?V1750)
 ]
NIL
|#
;;;--------------------------------------------------------------- PROCESS-RULES

(defun process-rules ()
  "takes *rule-list* and processes each entry in turn.
An entry of the individual list is for example:
;(\"Z-Adult\"
; ((:VCLASS (:DOC (:EN \"An ADULT is a person over 18.\")) (:IS-A (\"person \"))
;           (:NAME :EN \"Adult\" :FR \"Adulte\"))))
Arguments:
   none
Return:
   nothing interesting."
  (declare (special *rule-list* *rule-format*))
  (let (result definition)
    ;; sort index list
    (setq *rule-list* (sort *rule-list* #'string-lessp))
    ;; for each entry of the index list build an index 
    (dolist (rule *rule-list* )
      (with-warning (rule)
        ;; get an a-list like ((:EN "hasFirstName")(:FR "..." "..."))
        ;; contains mln if then doc rule-format
        (setq definition (car (index-get rule :rule :def)))
        ;; print when verbose
        (vformat "~&===== idx: ~S" rule)
        ;(format t "~%; process-rules /*rule-format*: ~S" *rule-format*)
        ;; process rule
        (apply #'make-rule definition)
        ))
    ;; return
    :done
    ))

#|
(let ((*compiler-pass* 1))
  (reset)
  (defconcept (:en "Person" :fr "Personne")
      (:rel (:en "father" :fr "père")(:to "Person")(:max 1))
    (:rel (:en "grand father" :fr "grand père")(:to "Person")(:max 2))
    )
  (defrule (:en "grand father" :fr "grand père")
      (:if (?x :type "Person")
           (?x "father" ?y)
           (?y "father" ?z))
    (:then
     (?x "grand father" ?z))
    (:doc :en "rule to determine grand-fatherhood.")
    :jena)
  )
(let ((*compiler-pass* 2))
  (defconcept (:en "Person" :fr "Personne")
      (:rel (:en "father" :fr "père")(:to "Person")(:max 1))
    (:rel (:en "grand father" :fr "grand père")(:to "Person")(:max 2))
    )
  (defrule (:en "grand father" :fr "grand père")
      (:if (?x :type "Person")
           (?x "father" ?y)
           (?y "father" ?z))
    (:then
     (?x "grand father" ?z))
    (:doc :en "rule to determine grand-fatherhood.")
    :jena)
  (index)
  )

(process-rules)

### rule to determine grand-fatherhood.

[Role_GrandPère:
   (?X :hasGrandPère ?Z)
<-
   (?X rdf:type :Z-Personne)
   (?X :hasPère ?Y)
   (?Y :hasPère ?Z)
 ]
;; Note that grand father must be a relation of the class person
|#
;;;---------------------------------------------------------------- RECORD-RULE

(defun record-rule (rule-name if-part then-part doc 
                              &optional (rule-output-format :jena))
  "creates an entry into the index tale to save the rule until processing time.
Arguments:
   rule-name: multilingual name
   if-part: list of premisses
   then-part: list of conclusions
   doc: eventual documentation (multilingual string)
   rule-output-format: currently :jena or :moss  (default :jena)
Return:
   nothing special."
  (when (eql *compiler-pass* 1)
    (let ((rule-id (make-index-string 
                    (mln::get-canonical-name rule-name *language*))))
      ;; we process rules after everything else
      (index-add rule-id :rule :def 
                 (list rule-name if-part then-part doc 
                       :rule-output-format rule-output-format))  ;JPB0802
      ;(make-indices concept-name id :class)
      (make-indices rule-name rule-id :rule)
      (pushnew rule-id *rule-list* :test #'string-equal)
      :done)))

#|
(let ((*compiler-pass* 1))
  (reset)
  (defrule (:en "grand father")
      (:if (?x :type "Person")
           (?x "father" ?y)
           (?y "father" ?z))
    (:then
     (?x "grand father" ?z))
    (:doc :en "rule to determine grand-fatherhood.")
    :jena)
  (index))

*rule-list*
("GrandFather")
|# 
;;;------------------------------------------------------------- rule-variable?

(defun rule-variable? (var)
  "Any symbol starting with ? is considered a rule variable.
Arguments:
   var: arg to check
Return:
   nil if false, T otherwise."
  (and (symbolp var)(eql (char (symbol-name var) 0) '#\?)))

;;;---------------------------------------------------- valid-composition-path?

(defun valid-composition-path? (path-list &key property-type &aux result)
  "checks for validity of the composition path.
Argument:
   path-list: list of paths (because of :OR option)
   property-type (key): keyword (:relation or :attribute)
Return:
   nil in case of error, something nonnil otherwise. 
   Throws to :error on bad property-type."
  (when (and (listp path-list) (every #'listp path-list))
    (dolist (path path-list)
      (push
       (case property-type
         (:relation
          (every #'is-relation-ref? path))
         (:attribute
          (and
           (every #'is-relation-ref? (butlast path))
           (is-attribute-ref? (car (last path)))))
         (t (terror "Unknown property-type ~S when checking :compose path: ~S"
                    property-type path-list)))
       result))
    (print result)
    (not (some #'null result))))

#| 
? (valid-composition-path? '(("mother" "brother")) :property-type :relation)
T

? (valid-composition-path? '(("mother" "name")) :property-type :relation)
nil

? (valid-composition-path? '(("mother" "brother")) :property-type :attribute)
NIL

? (valid-composition-path? '(("mother" "name")) :property-type :attribute)
T

? (catch :error (valid-composition-path? '(("mother" "name")) ))
"Unknown property-type NIL when checking :compose path: ((\"mother\" \"name\"))"

? (valid-composition-path? '(("mother" "brother")("mother" "father")) 
                         :property-type :relation)
T

? (valid-composition-path? '(("mother" "brother")("mother" "name")) 
                         :property-type :relation)
NIL

? (valid-composition-path? '(("mother" "name")("mother" "name")) 
                         :property-type :attribute)
T
|#

(format t "~%;*** Sol2rules loaded ***")


:EOF
