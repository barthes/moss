﻿;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;===============================================================================
;;;20/01/30
;;;               M O S S - E X P O R T (file export.lisp)
;;;
;;;===============================================================================

;;; THIS RECONSTRUCTION IS ONLY VALID IN A SINGLE CONTEXT (SINGLE VERSION)

;;; MISSING: orphans and virtual classes

;;; The goal of this file is to reconstruct objects definitions from the content
;;; of an application, so that we can recover a reconstructed definition file.

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;; we do that from the system instance ($SYS.1). The easier objects to reconstruct 
;;; are variables, functions and methods. More difficult are classes. Properties 
;;; could be reconstructed separated from classes, however this would lead to 
;;; code difficult-to-read.
;;; entry-points may be ignored, since they are derived from =make-entry classes.
;;; virtual classes may be a problem.

;;; To produce a reconstructed file:
;;;   - go to the right package, e.g. :MISSION
;;;   - call (moss::make-ontology-file)
;;; This will build the file <package name>-ONTOLOGY.lisp in the ACL directory.
;;;
;;; Options
;;;   - a file pathname can be given with the key :file-pathname
;;;   - an ontology object ($SYS.nn) can be given with the key :ontology. Default
;;;     is:
;;;        *ontology* (i.e. first instance of system $SYS.1)

;;; Of course doing so we loose all the comments and recover only capitalized 
;;; code in methods.

;;; POSSIBLE IMPROVEMENTS
;;; =====================
;;; 1. When linking object.2 to object.1, then if an entry point is available 
;;;    for object.2, then it can be used instead of the variable
;;;    Thhis can be done with moss::%get-entry-point

#|
History
-------
2012
 1124 modified output of reconstruct-instance, adding :is-a and reconstruct-isa
 1128 adding print-ontology-closing-info
2014 -> UTF8 encoding
2020
 0130 restructuring MOSS as an asdf system
|#

(in-package :moss)

;;; Exemple of the MISSION environment:
#|
((MOSS::$TYPE (0 $SYS)) (MOSS::$ID (0 $SYS.1)) (MOSS::$SNAM (0 (:EN "SA_MISSION MOSS")))
 (MOSS::$PRFX (0 SA_MISSION-MOSS))
 (MOSS::$EPLS
  (0 SA_MISSION-MOSS SA_MISSION-MOSS-SYSTEM METHOD UNIVERSAL-METHOD COUNTER NULL-CLASS UNIVERSAL-CLASS
   OMAS-SKILL HAS-SK-NAME IS-SK-NAME-OF HAS-SK-DOC IS-SK-DOC-OF OMAS-GOAL HAS-GL-NAME IS-GL-NAME-OF
   HAS-GL-DOC IS-GL-DOC-OF OMAS-AGENT HAS-AG-NAME IS-AG-NAME-OF =MAKE-ENTRY HAS-AG-DOC IS-AG-DOC-OF
   HAS-AG-KEY IS-AG-KEY-OF HAS-AG-SKILL IS-AG-SKILL-OF HAS-AG-GOAL IS-AG-GOAL-OF MISSION COUNTRY PAYS
   HAS-NAME HAS-NOM IS-NAME-OF IS-NOM-OF HAS-UTC-CODE HAS-CODE-UTC IS-UTC-CODE-OF IS-CODE-UTC-OF
   HAS-ARCHIVED HAS-ARCHIVÉ IS-ARCHIVED-OF IS-ARCHIVÉ-OF HAS-START-DATE HAS-DATE-DE-DÉBUT IS-START-DATE-OF
   IS-DATE-DE-DÉBUT-OF HAS-END-DATE HAS-DATE-DE-FIN IS-END-DATE-OF IS-DATE-DE-FIN-OF HAS-LOCATION HAS-LIEU
   IS-LOCATION-OF IS-LIEU-OF HAS-REASON HAS-RAISON IS-REASON-OF IS-RAISON-OF PERSON PERSONNE HAS-FIRST-NAME
   HAS-PRÉNOM IS-FIRST-NAME-OF IS-PRÉNOM-OF HAS-INITIALS HAS-INITIALES-DU-PRÉNOM IS-INITIALS-OF
   IS-INITIALES-DU-PRÉNOM-OF HAS-EMAIL IS-EMAIL-OF HAS-COUNTRY HAS-PAYS IS-COUNTRY-OF IS-PAYS-OF HAS-PERSON
   HAS-PERSONNE IS-PERSON-OF IS-PERSONNE-OF =GET-MISSION =GET-MISSION-STRING =IF-NEEDED =SUMMARY
   =XML-REFERENCE AFGHANISTAN AFGANISTAN SOUTH-AFRICA AFRIQUE-DU-SUD ALBANIA ALBANIE ALGERIA ALGÉRIE GERMANY
   ALLEMAGNE ANDORRA ANDORRE ANGOLA ANTIGUA-AND-BARBUDA ANTIGUA-ET-BARBUDA SAUDI-ARABIA ARABIE-SAOUDITE
   ARGENTINA ARGENTINE ARMENIA ARMÉNIE AUSTRALIA AUSTRALIE AUSTRIA AUTRICHE AZERBAIJAN AZERBAÏDJAN BAHAMAS
   BAHRAIN BAHREÏN BANGLADESH BARBADOS BARBADE BELAU BELGIUM BELGIQUE BELIZE BENIN BÉNIN BHUTAN BHOUTAN
   BELARUS BIÉLORUSSIE BURMA BIRMANIE BOLIVIA BOLIVIE BOSNIA-AND-HERZEGOVINA BOSNIE-HERZÉGOVINE BOTSWANA
   BRAZIL BRÉSIL BRUNEI BULGARIA BULGARIE BURKINA BURUNDI CAMBODIA CAMBODGE CAMEROON CAMEROUN CANADA
   CAPE-VERDE CAP-VERT CHILE CHILI CHINA CHINE CYPRUS CHYPRE COLOMBIA COLOMBIE COMOROS COMORES CONGO
   THE-DEMOCRATIC-REPUBLIC-OF-CONGO LA-RÉPUBLIQUE-DÉMOCRATIQUE-DU-CONGO COOK-ISLANDS LES-ÎLES-COOK
   NORTHERN-COREA CORÉE-DU-NORD SOUTHERN-COREA CORÉE-DU-SUD COSTA-RICA IVORY-COAST |CÔTE-D'IVOIRE| CROATIA
   CROATIE CUBA DENMARK DANEMARK DJIBOUTI DOMINICA DOMINIQUE EGYPT ÉGYPTE UNITED-ARAB-EMIRATES
   ÉMIRATS-ARABES-UNIS ECUADOR ÉQUATEUR ERITREA ÉRYTHRÉE SPAIN ESPAGNE ESTONIA ESTONIE
   UNITED-STATES-OF-AMERICA USA ÉTATS-UNIS ETHIOPIA ÉTHIOPIE FIJI FIDJI FINLAND FINLANDE FRANCE GABON GAMBIA
   GAMBIE GEORGIA GÉORGIE GHANA GREECE GRÈCE GRENADA GRENADE GUATEMALA GUINEA GUINÉE GUINEA-BISSAU
   GUINÉE-BISSAO EQUATORIAL-GUINEA GUINÉE-ÉQUATORIALE GUYANA HAITI HAÏTI HONDURAS HUNGARY HONGRIE INDIA INDE
   INDONESIA INDONÉSIE IRAN IRAQ IRELAND IRLANDE ICELAND ISLANDE ISRAEL ISRAËL ITALY ITALIE JAMAICA JAMAÏQUE
   JAPAN JAPON JORDAN JORDANIE KAZAKHSTAN KENYA KYRGYZSTAN KIRGHIZISTAN KIRIBATI KUWAIT KOWEÏT LAOS LESOTHO
   LATVIA LETTONIE LEBANON LIBAN LIBERIA LYBIA LIBYE LIECHTENSTEIN LITHUANIA LITUANIE LUXEMBOURG MACEDONIA
   MACÉDOINE MADAGASCAR MALAYSIA MALAISIE MALAWI MALDIVES MALI MALTA MALTE MOROCCO MAROC MARSHALL-ISLANDS
   LES-ÎLES-MARSHALL MAURITIUS MAURICE MAURITANIA MAURITANIE MEXICO MEXIQUE MICRONESIA MICRONÉSIE MOLDOVA
   MOLDAVIE MONACO MONGOLIA MONGOLIE MOZAMBIQUE NAMIBIA NAMIBIE NAURU NEPAL NÉPAL NICARAGUA NIGER NIGERIA
   NIUE NORWAY NORVÈGE NEW-ZEALAND NOUVELLE-ZÉLANDE OMAN UGANDA OUGANDA UZBEKISTAN OUZBÉKISTAN PAKISTAN
   PANAMA PAPUA-NEW-GUINEA PAPOUASIE-NOUVELLE-GUINÉE PARAGUAY NETHERLANDS PAYS-BAS PERU PÉROU PHILIPPINES
   POLAND POLOGNE PORTUGAL QATAR CENTRAL-AFRICAN-REPUBLIC RÉPUBLIQUE-CENTRAFRICAINE DOMINICAN-REPUBLIC
   RÉPUBLIQUE-DOMINICAINE CZECH-REPUBLIC RÉPUBLIQUE-TCHÈQUE ROMANIA ROUMANIE UNITED-KINGDOM ROYAUME-UNI
   RUSSIA RUSSIE RWANDA SAINT-KITTS-AND-NEVIS SAINT-CHRISTOPHE-ET-NIÉVÈS SAINT-LUCIA SAINTE-LUCIE SAN-MARINO
   SAINT-MARIN VATICAN SAINT-VINCENT-AND-THE-GRENADINES SAINT-VINCENT-ET-LES-GRENADINES SOLOMON-ISLANDS
   LES-ÎLES-SALOMON EL-SALVADOR SALVADOR WESTERN-SAMOA SAMOA-OCCIDENTALES SAO-TOME-AND-PRINCIPE
   SAO-TOMÉ-ET-PRINCIPE SENEGAL SÉNÉGAL SEYCHELLES SIERRA-LEONE SINGAPORE SINGAPOUR SLOVAKIA SLOVAQUIE
   SSLOVENIA SLOVÉNIE SOMALIA SOMALIE SUDAN SOUDAN SRI-LANKA SWEDEN SUÈDE SWITZERLAND SUISSE SURINAME
   SWAZILAND SYRIA SYRIE TAJIKISTAN TADJIKISTAN TANZANIA TANZANIE CHAD TCHAD THAILAND THAÏLANDE TOGO TONGA
   TRINIDAD-AND-TOBAGO TRINITÉ-ET-TOBAGO TUNISIA TUNISIE TURKMENISTAN TURKMÉNISTAN TURKEY TURQUIE TUVALU
   UKRAINE URUGUAY VANUATU ENEZUELA VENEZUELA VIETNAM VIÊT-NAM YEMEN YÉMEN YUGOSLAVIA YOUGOSLAVIE ZAIRE
   ZAÏRE ZAMBIA ZAMBIE ZIMBAWE ZIMBABWE ABEL MARIE-HÉLÈNE DE-AZEVEDO HILTON BARTHÈS JEAN-PAUL BARTHÈS-BIESEL
   BARGER PAVOL BETTAHAR HATEM BONNIFAIT PHILIPPE BOUABDALLAH MADJID CARLIER JACQUES CASTILLO PEDRO CHARARA
   ALI CHEN KEJIA DENOEUX THIERRY DUBUISSON BERNARD ENEMBRECK FABRICIO FANTONI ISABELLE FONTAINE GONZALES
   OMAR LOURDEAUX DOMITILE LOZANO ROGELIO MORIZET PIERRE MOULIN CLAUDE MOUKRIM AZIZ NACE DRITAN SCHOEN
   WALTER SHAWKY MOHAMED TRIGANO VIDOLOV BORIS CHALLAL YACINE DUBLIN EDWARD LYDIE |NANJING,-CHINA|
   GEBERS-FREITAS ERIK TRONDHEIM MONTREAL BOUASSIDA MOHAMED-SALAH TRENTO KANTSRUHE BARTHES CURITIBA HOLLAND
   JOHANN PISE BIRMINGHAM ATHÈNES GRANDVALET YVES VANCOUVER DUBAÏ VIENNE FRANCFORT BOUALI HUANG XU MONTRÉAL
   GEORGIOU VASILEIOS CHANIA JONES HAZAËL TUNIS ABDALLAH FAHED XIAN DREVELLE VINCENT NAPLES DOUMIATI
   MOUSTAPHA |SAINT-LOUIS,-MISSOURI| LEMARCHAND OLIVIER BARI COLOGNE/KARLSRUHE YOUNES ZOULFICAR OVIEDO
   HELMOND DHERBOMEZ GERALD ESPOSITO NICOLAS HAMMAMET BEYROUTH NASSREDDINE GHALIA LAUSANNE PRAGUE NORWICH
   CORREA-VICTORINO ALESSANDRO BUDAPEST STOCKHOLM GOVAERT GERARD TAHAN OUSSAMA POOO MAASTRICHT WROCLAW BALEY
   JULIEN PÉKIN CAO XIAOKANG SHANGAI CAUDEVILLE BOSTON SAUNIER AMSTERDAM MILAN GARCIA-SALAZAR OCTAVIO
   LEBLANC ADELINE |CHANIA-(CRÊTE)| SAVANNAH FOUQUE CLEMENT VARSOVIE RONDON-SALLAN EDOUARDO SAINT-LOUIS
   WASHINGTON TÉNÉRIFE LIMASSOL SAO-PAULO MADRID LARNACA LE-CAIRE TURIN CASTILLO-GARCIA VALENCE))
 (MOSS::$CRET (0 "MOSS")) (MOSS::$DTCT (0 "26/10/2010")) (MOSS::$VERT (0 "8.0.7")) (MOSS::$XNB (0 0))
 (MOSS::$SVL (0 *MOSS-SYSTEM* *ONTOLOGY*))
 (MOSS::$ENLS
  (0 $SYS $FN $UNI $CTR *NONE* *ANY* $E-OMAS-SKILL $E-OMAS-GOAL $E-OMAS-AGENT $E-COUNTRY $E-MISSION
   $E-PERSON))
 (MOSS::$ETLS
  (0 $T-SK-NAME $T-OMAS-SKILL-SK-NAME $T-SK-DOC $T-OMAS-SKILL-SK-DOC $T-GL-NAME $T-OMAS-GOAL-GL-NAME
   $T-GL-DOC $T-OMAS-GOAL-GL-DOC $T-AG-NAME $T-OMAS-AGENT-AG-NAME $T-AG-DOC $T-OMAS-AGENT-AG-DOC $T-AG-KEY
   $T-OMAS-AGENT-AG-KEY $T-NAME $T-COUNTRY-NAME $T-UTC-CODE $T-COUNTRY-UTC-CODE $T-ARCHIVED
   $T-COUNTRY-ARCHIVED $T-START-DATE $T-MISSION-START-DATE $T-END-DATE $T-MISSION-END-DATE $T-LOCATION
   $T-MISSION-LOCATION $T-REASON $T-MISSION-REASON $T-PERSON-NAME $T-FIRST-NAME $T-PERSON-FIRST-NAME
   $T-INITIALS $T-PERSON-INITIALS $T-EMAIL $T-PERSON-EMAIL))
 (MOSS::$EILS
  (0 $T-SK-NAME.OF $T-OMAS-SKILL-SK-NAME.OF $T-SK-DOC.OF $T-OMAS-SKILL-SK-DOC.OF $T-GL-NAME.OF
   $T-OMAS-GOAL-GL-NAME.OF $T-GL-DOC.OF $T-OMAS-GOAL-GL-DOC.OF $T-AG-NAME.OF $T-OMAS-AGENT-AG-NAME.OF
   $T-AG-DOC.OF $T-OMAS-AGENT-AG-DOC.OF $T-AG-KEY.OF $T-OMAS-AGENT-AG-KEY.OF $S-AG-SKILL.OF
   $S-OMAS-AGENT-AG-SKILL.OF $S-AG-GOAL.OF $S-OMAS-AGENT-AG-GOAL.OF $T-NAME.OF $T-COUNTRY-NAME.OF
   $T-UTC-CODE.OF $T-COUNTRY-UTC-CODE.OF $T-ARCHIVED.OF $T-COUNTRY-ARCHIVED.OF $T-START-DATE.OF
   $T-MISSION-START-DATE.OF $T-END-DATE.OF $T-MISSION-END-DATE.OF $T-LOCATION.OF $T-MISSION-LOCATION.OF
   $T-REASON.OF $T-MISSION-REASON.OF $T-PERSON-NAME.OF $T-FIRST-NAME.OF $T-PERSON-FIRST-NAME.OF
   $T-INITIALS.OF $T-PERSON-INITIALS.OF $T-EMAIL.OF $T-PERSON-EMAIL.OF $S-COUNTRY.OF $S-MISSION-COUNTRY.OF
   $S-PERSON.OF $S-MISSION-PERSON.OF))
 (MOSS::$DFXL
  (0 _GET-HAS-SK-NAME _SET-HAS-SK-NAME _GET-HAS-SK-DOC _SET-HAS-SK-DOC _GET-HAS-GL-NAME _SET-HAS-GL-NAME
   _GET-HAS-GL-DOC _SET-HAS-GL-DOC _GET-HAS-AG-NAME _SET-HAS-AG-NAME _GET-HAS-AG-DOC _SET-HAS-AG-DOC
   _GET-HAS-AG-KEY _SET-HAS-AG-KEY _GET-HAS-AG-SKILL _SET-HAS-AG-SKILL _GET-HAS-AG-GOAL _SET-HAS-AG-GOAL
   _GET-HAS-NAME _SET-HAS-NAME _GET-HAS-UTC-CODE _SET-HAS-UTC-CODE _GET-HAS-ARCHIVED _SET-HAS-ARCHIVED
   _GET-HAS-START-DATE _SET-HAS-START-DATE _GET-HAS-END-DATE _SET-HAS-END-DATE _GET-HAS-LOCATION
   _SET-HAS-LOCATION _GET-HAS-REASON _SET-HAS-REASON _GET-HAS-FIRST-NAME _SET-HAS-FIRST-NAME
   _GET-HAS-INITIALS _SET-HAS-INITIALS _GET-HAS-EMAIL _SET-HAS-EMAIL _GET-HAS-COUNTRY _SET-HAS-COUNTRY
   _GET-HAS-PERSON _SET-HAS-PERSON))
 (MOSS::$FNLS
  (0 $FN.1 $FN.2 $FN.3 $FN.4 $FN.5 $FN.6 $FN.7 $FN.8 $FN.9 $E-FN.10 $E-FN.11 $FN.12 $E-FN.13 $E-FN.14
   $E-FN.15 $E-FN.16 $E-FN.17))
 (MOSS::$ESLS
  (0 $S-AG-SKILL $S-OMAS-AGENT-AG-SKILL $S-AG-GOAL $S-OMAS-AGENT-AG-GOAL $S-COUNTRY $S-MISSION-COUNTRY
   $S-PERSON $S-MISSION-PERSON))
 (MOSS::$SFL (0 F_EXTIFF F_EXTRACT-INITIALS-FROM-FIRST-NAME)))
|#

;;; if we remove entry points and inverse-links and moss-def-expr (HAS-NAME,..):

#|
((MOSS::$TYPE (0 $SYS)) (MOSS::$ID (0 $SYS.1)) (MOSS::$SNAM (0 (:EN "SA_MISSION MOSS")))
 (MOSS::$PRFX (0 SA_MISSION-MOSS))
 (MOSS::$CRET (0 "MOSS")) (MOSS::$DTCT (0 "26/10/2010")) (MOSS::$VERT (0 "8.0.7")) (MOSS::$XNB (0 0))
 (MOSS::$SVL (0 *MOSS-SYSTEM* *ONTOLOGY*))
 (MOSS::$ENLS
  (0 $SYS $FN $UNI $CTR *NONE* *ANY* $E-OMAS-SKILL $E-OMAS-GOAL $E-OMAS-AGENT $E-COUNTRY $E-MISSION
   $E-PERSON))
 (MOSS::$ETLS
  (0 $T-SK-NAME $T-OMAS-SKILL-SK-NAME $T-SK-DOC $T-OMAS-SKILL-SK-DOC $T-GL-NAME $T-OMAS-GOAL-GL-NAME
   $T-GL-DOC $T-OMAS-GOAL-GL-DOC $T-AG-NAME $T-OMAS-AGENT-AG-NAME $T-AG-DOC $T-OMAS-AGENT-AG-DOC $T-AG-KEY
   $T-OMAS-AGENT-AG-KEY $T-NAME $T-COUNTRY-NAME $T-UTC-CODE $T-COUNTRY-UTC-CODE $T-ARCHIVED
   $T-COUNTRY-ARCHIVED $T-START-DATE $T-MISSION-START-DATE $T-END-DATE $T-MISSION-END-DATE $T-LOCATION
   $T-MISSION-LOCATION $T-REASON $T-MISSION-REASON $T-PERSON-NAME $T-FIRST-NAME $T-PERSON-FIRST-NAME
   $T-INITIALS $T-PERSON-INITIALS $T-EMAIL $T-PERSON-EMAIL))
 (MOSS::$FNLS
  (0 $FN.1 $FN.2 $FN.3 $FN.4 $FN.5 $FN.6 $FN.7 $FN.8 $FN.9 $E-FN.10 $E-FN.11 $FN.12 $E-FN.13 $E-FN.14
   $E-FN.15 $E-FN.16 $E-FN.17))
 (MOSS::$ESLS
  (0 $S-AG-SKILL $S-OMAS-AGENT-AG-SKILL $S-AG-GOAL $S-OMAS-AGENT-AG-GOAL $S-COUNTRY $S-MISSION-COUNTRY
   $S-PERSON $S-MISSION-PERSON))
 (MOSS::$SFL (0 F_EXTIFF F_EXTRACT-INITIALS-FROM-FIRST-NAME)))
|#

;;; Note: we are missing the version graph.

;;; Note: $SYS $FN $UNI $CTR *NONE* *ANY* $E-OMAS-SKILL $E-OMAS-GOAL 
;;; $E-OMAS-AGENT are produced when loading the agent and do not belong to 
;;; the ontology

;;; Note: $FN.1  $FN.9 correspond to =default-make-entry

;;; Note: generic properties $T-EMAIL, $S-COUNTRY are built automatically

;;; Note: instances may be a problem

;;; Overall approach:
;;;   We start from classes, rebuild them and eliminate properties as they are 
;;; linked to the classes

;;;===============================================================================

;;;----------------------------------------------------------- MAKE-ONTOLOGY-FILE

(defun make-ontology-file (&key ontology file-pathname)
  "builds the ontology file from the system data. Currently limited to the ~
   current context.
Arguments:
   sys (key): system object e.g. $SYS.1 (default *ontology*)
   file-pathname (key): pathname of the output file (default ontology-test.lisp)
Return:
   :done"
  (declare (ignore ontology))
  (let (;(ontology (or ontology *ontology*))
        (filename (or file-pathname 
                      (format nil "~A-ONTOLOGY.lisp" (package-name *package*))))
        )
    
    ;;=== initialize various lists from system data
    (reconstruct-application)
    
    ;; open file
    ;; for some mysterious reason if we use :external-format, the system does not
    ;; insert the BOM in fromt of the file. We instruct LISP to load UTF8 files if
    ;; no BOM
    (with-open-file (stream filename :direction :output :if-exists :supersede
                            :if-does-not-exist :create :external-format :utf-8)
      ;;=== print main header
      (print-ontology-main-header stream)
      ;;=== print ontology object
      (print-ontology-object stream)
      ;;=== print variables (e.g. *ontology*, *context*,...
      (print-ontology-variables stream (reverse *variable-list*))
      ;;=== print classes
      (print-ontology-classes stream *class-list*) 
      ;;=== print generic attributes
      (print-ontology-attributes stream *attribute-list*)
      ;;=== print generic relations
      (print-ontology-relations stream *relation-list*)
      ;;=== print functions
      (print-ontology-functions stream (reverse *function-list*))
      ;;=== print methods
      (print-ontology-methods stream *method-list*)
      ;;=== print universal methods
      (print-ontology-universal-methods stream *universal-method-list*)
      ;;=== print individuals
      (print-ontology-individuals stream (reverse *instance-list*)) 
      ;;=== print closing info
      (print-ontology-closing-info stream)
      )
    (format t "~%; New ontology file:~%  ~S" filename)
    ))

#|
? (make-ontology-file)
|#
;;;-------------------------------------------------------------- MAKE-SUB-HEADER

(defun make-line-header (title &key (width *title-width*) (fill-char #\-) center)
  "produce a line with name of function"
  (if center
      (let* ((remainder (- width 5 (length title)))
             (left (floor (/ remainder 2)))
             (right (- remainder left)))
        (format nil ";;;~A ~A ~A"
          (make-string left :initial-element fill-char)
          title
          (make-string right :initial-element fill-char)))
    (format nil ";;;~A ~A" 
      (make-string (- width (if (equal title "") 3 4) (length title)) 
                   :initial-element fill-char) 
      (string-upcase title))))

#|
? (moss::make-line-header "sub-header test" :width 40)
";;;--------------------- SUB-HEADER TEST"

? (moss::make-line-header "test" :width 40 :center t)
";;;--------------- test ----------------"

? (moss::make-line-header "" :width 40 :fill-char #\=)
";;;===================================== "
|#
;;;----------------------------------------------------- PRINT-ONTOLOGY-ATTRIBUTES

(defun print-ontology-attributes (stream attribute-list)
  "prints generic attributes, i.e. attributes that have no class, or no local class."
  (when attribute-list
    (let (ordered-att-list)
      ;; print class header
      (format stream "~2%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%;;;")
      (format stream "~%~A" 
        (make-line-header "ATTRIBUTES" :fill-char #\space :center t))
      (format stream "~%;;;")
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      
      ;; order attributes by name
      (setq ordered-att-list (sort attribute-list #'string-lessp :key #'car))
      (dolist (item ordered-att-list)
        ;; print data - item is ("test" . "(defattribute ...)")
        (format stream "~2%~A" (cdr item)))
      :done)))

;;;-------------------------------------------------------- PRINT-ONTOLOGY-CLASSES

(defun print-ontology-classes (stream class-list)
  (when class-list
    (let (ordered-class-list)
      ;; print class header
      (format stream "~2%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%;;;")
      (format stream "~%~A" 
        (make-line-header "CONCEPTS" :fill-char #\space :center t))
      (format stream "~%;;;")
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      
      ;; order classes by class-name
      (setq ordered-class-list (sort class-list #'string-lessp :key #'car))
      (dolist (item ordered-class-list)
        ;; print single class header
        (format stream "~2%~A" (make-line-header "" :fill-char #\=))
        (format stream "~%~A" 
          (make-line-header (string-upcase (car item)) :fill-char #\space :center t))
        (format stream "~%~A~%" (make-line-header "" :fill-char #\=))
        ;; print data
        (format stream "~%~A" (cdr item)))
      :done)))

#|
? (setq  moss::*class-list* nil)
? (moss::reconstruct-class '$E-PERSON)
? (moss::reconstruct-class '$E-MISSION)
? (moss::print-ontology-classes t moss::*class-list*)
<printed stuff>
|#
;;;--------------------------------------------------- PRINT-ONTOLOGY-CLOSING-INFO

(defun print-ontology-closing-info (stream)
  "simply prints an EOF mark"
  (format stream "~2%:EOF"))

;;;------------------------------------------------------ PRINT-ONTOLOGY-FUNCTIONS

(defun print-ontology-functions (stream function-list)
  (when function-list
    (let (ordered-function-list)
      ;; order functions by function name
      (setq ordered-function-list 
            (sort function-list #'string-lessp :key #'car))
      ;; print function header
      (format stream "~2%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%;;;")
      (format stream "~%~A" 
        (make-line-header "FUNCTIONS" :fill-char #\space :center t))
      (format stream "~%;;;")
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      
      ;; print single function
      (dolist (item ordered-function-list)
        (format stream "~2%~A~%" (make-line-header (car item)))
        ;; print function
        (format stream "~%~A" (cdr item)))
      :done)))

#|
? (moss::print-ontology-functions t *function-list*)
|#
;;;---------------------------------------------------- PRINT-ONTOLOGY-INDIVIDUALS

(defun print-ontology-individuals (stream instance-list)
  "as the title says"
  (when instance-list
    (let ()
      ;; we assume that individual objects are ordered by classes and are followed 
      ;; by orphans
      ;; print individual header
      (format stream "~2%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%;;;")
      (format stream "~%~A" 
        (make-line-header "INDIVIDUALS (Knowledge Base)" :fill-char #\space 
                          :center t))
      (format stream "~%;;;")
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      ;; print individual
      (dolist (item instance-list)
        (format stream "~2%~A" item))
      :done)))

#|
? (moss::print-ontology-individuals t moss::*instance-list*)
|#
;;;---------------------------------------------------- PRINT-ONTOLOGY-MAIN-HEADER

(defun print-ontology-main-header (stream)
  "makes the top-level header of the ontology file"
  (let ((package (package-name *package*)))
    (format stream ";;;-*- Mode: Lisp; Package: ~S -*-" package)
    (format stream "~%~A" (make-line-header "" :fill-char #\=))
    (format stream "~%;;;~A" (get-current-date))
    (format stream "~%~A" 
      (make-line-header (format nil "~A ONTOLOGY" package) :fill-char #\space
                        :center t))
    (format stream "~%;;;")
    (format stream "~%~A" (make-line-header "" :fill-char #\=))
    
    (format stream "~2%(in-package ~S)~%" package)
    :done))

#|
? (moss::print-ontology-main-header t)
;;;-*- Mode: Lisp; Package: "MISSION" -*-
;;;============================================================================= 
;;;29/10/2010
;;;                              MISSION ONTOLOGY                               
;;;
;;;============================================================================= 

(in-package "MISSION")

|#
;;;-------------------------------------------------------- PRINT-ONTOLOGY-METHODS

(defun print-ontology-methods (stream method-list)
  (when method-list
    (let (ordered-method-list)
      ;; order methods by method-name and class name
      (setq ordered-method-list
            (sort method-list #'string-lessp 
                  :key #'(lambda (xx) (concatenate 'string (car xx) "+" (cadr xx)))))  
      ;; print method header
      (format stream "~2%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%;;;")
      (format stream "~%~A" 
        (make-line-header "METHODS" :fill-char #\space :center t))
      (format stream "~%;;;")
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      
      (dolist (item ordered-method-list)
        ;; print single method header
        (format stream "~2%~A" 
          (make-line-header 
           (concatenate 'string
             (if (not (equal (cadr item) " "))
                 (format nil "(~A) " (cadr item))
               "")
             (car item))))
        ;; print method
        (format stream "~2%~A" (caddr item)))
      :done)))

#|
? (moss::print-ontology-methods t moss::*method-list*)
|#
;;;--------------------------------------------------------- PRINT-ONTOLOGY-OBJECT

(defun print-ontology-object (stream)
  "reconstructs defontology from $SYS-1"
  (let* ((ontology (symbol-value (intern "*ONTOLOGY*")))
         (version (car (%%has-value ontology '$VERT *context*)))
         (sname (car (%%has-value ontology '$SNAM *context*)))
         (language (or (car (%%has-value ontology '$LANG *context*)) :ALL))
         )
    (format stream "~%(defontology~%  (:title ~S)~%  (:version ~S)~
                    ~%  (:language ~S)~%  )~%" sname version language)
    ))

#|
? (moss::print-ontology-object t)
(defontology
  (:title (:EN "SA_MISSION MOSS"))
  (:version "8.0.7")
  (:language :ALL)
  )
|#
;;;------------------------------------------------------ PRINT-ONTOLOGY-RELATIONS

(defun print-ontology-relations (stream relation-list)
  "prints generic relations, i.e. relations defined outside a class with domain ~
   :any and range :any."
  (when relation-list
    (let (ordered-rel-list)
      ;; print class header
      (format stream "~2%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%;;;")
      (format stream "~%~A" 
        (make-line-header "RELATIONS" :fill-char #\space :center t))
      (format stream "~%;;;")
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      
      ;; order attributes by name
      (setq ordered-rel-list (sort relation-list #'string-lessp :key #'car))
      (dolist (item ordered-rel-list)
        ;; print data - item is ("test" . "(defrelation ...)")
        (format stream "~2%~A" (cdr item)))
      :done)))

;;;---------------------------------------------- PRINT-ONTOLOGY-UNIVERSAL-METHODS

(defun print-ontology-universal-methods (stream method-list)
  (when method-list
    (let (ordered-method-list)
      ;; order methods by method-name and class name
      (setq ordered-method-list
            (sort method-list #'string-lessp :key #'car))  
      ;; print method header
      (format stream "~2%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%;;;")
      (format stream "~%~A" 
        (make-line-header "UNIVERSAL METHODS" :fill-char #\space :center t))
      (format stream "~%;;;")
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      
      (dolist (item ordered-method-list)
        ;; print single method header
        (format stream "~2%~A" (make-line-header (car item)))
        ;; print method
        (format stream "~2%~A" (cdr item)))
      :done)))

#|
? (setq moss::*universal-method-list* nil)
? 
? (moss::print-ontology-universal-methods t moss::*universal-method-list*)
|#
;;;------------------------------------------------------ PRINT-ONTOLOGY-VARIABLES

(defun print-ontology-variables (stream variable-list)
  (when variable-list
    (let (ordered-variable-list)
      ;; order methods by method-name and class name
      (setq ordered-variable-list
            (sort variable-list #'string-lessp))
      ;; print variabe header
      (format stream "~2%~A" (make-line-header "" :fill-char #\=))
      (format stream "~%;;;")
      (format stream "~%~A" 
        (make-line-header "VARIABLES" :fill-char #\space :center t))
      (format stream "~%;;;")
      (format stream "~%~A" (make-line-header "" :fill-char #\=))
      
      (dolist (item ordered-variable-list)
        (format stream "~%~A" item))
      :done)))

#|
? (moss::print-ontology-variables t moss::*variable-list*)
|#
;;;======================= end printing functions ================================

;;;------------------------------------------------------- RECONSTRUCT-APPLICATION

(defun reconstruct-application ()
  "reconstruct all object using the *ontology* structure ($SYS.1)"
  (let* ((ontology (symbol-value (intern "*ONTOLOGY*")))
         ;; ontology should point to local $SYS.1
         (classes (%%has-value ontology '$ENLS *context*))
         ;(attributes (%%has-value ontology '$ETLS *context*))
         ;(relations (%%has-value ontology '$ESLS *context*))
         (methods (%%has-value ontology '$FNLS *context*))
         (functions (%%has-value ontology '$SFL *context*))
         (variables (%%has-value ontology '$SVL *context*))
         universal-methods ordered-classes)
    ;;=== reset all global lists
    (setq *attribute-list* nil
        *class-list* nil
        *method-list* nil
        *relation-list* nil
        *universal-method-list* nil
        *function-list* nil
        *variable-list* nil
        *instance-list* nil)
    
    ;;=== classes
    ;; keep only application classes, removing all classes that are added when
    ;; creating a new MOSS environment or loading agents
    (setq classes 
          (set-difference 
           classes 
           (mapcar #'(lambda (xx) (intern xx))
             '($SYS $FN $UNI $CTR *none* *ANY*
                    $E-OMAS-SKILL $E-OMAS-GOAL $E-OMAS-AGENT))))
    ;; we keep *none* to deal with orphans
    ;; initialize *class-list*
    ;; order classes by (French) class-names
    (setq ordered-classes 
          (sort classes #'(lambda(xx yy)
                              (string-lessp (car (send xx '=get-name))
                                            (car (send yy '=get-name))))))

    (format t "~%; reconstruct-application /classes:~% ~S" ordered-classes)
    (mapc #'reconstruct-class ordered-classes)
    
    ;;=== attributes (normally attached to classes)
    ;;=== relations (norally attached to classes)
    
    ;;=== functions
    (format t "~%; reconstruct-application /functions:~% ~S" functions)
    (mapc #'reconstruct-function functions)
    
    ;;=== methods
    ;; split universal and non universal
    (setq universal-methods 
          (remove-if-not #'%is-universal-method? methods))
    (setq methods
          (set-difference methods universal-methods))
    (format t "~%; reconstruct-application /methods:~% ~S" methods)
    (mapc #'reconstruct-method methods)
    ;;=== universal-methods
    (format t "~%; reconstruct-application /universal methods:~% ~S" 
      universal-methods)
    (mapc #'reconstruct-universal-method universal-methods)
    
    ;;=== variables
    (format t "~%; reconstruct-application /variables:~% ~S" variables)
    (mapc #'reconstruct-variable variables)
    
    ;;=== instances
    ;; do that for each class
    (dolist (class-id ordered-classes)
      ;(format t "~%; reconstruct-application /instances of class: ~S" class-id)
      ;; get instances
      (mapc #'reconstruct-instance (send class-id '=get-instances)))
    ;;=== orphans
    ;(mapc #'reconstruct-orphan (send *none* '=get-instances))
    
    :done))

#|
? (moss::reconstruct-application)
|#
;;;--------------------------------------------------------- RECONSTRUCT-ATTRIBUTE

;;; Example:
;;; $T-COUNTRY-NAME
;;; ((MOSS::$TYPE (0 MOSS::$EPT)) (MOSS::$ID (0 $T-COUNTRY-NAME)) 
;;;  (MOSS::$PNAM (0 (:EN "name" :FR "nom")))
;;;  (MOSS::$ETLS.OF (0 $SYS.1)) (MOSS::$IS-A (0 $T-NAME)) 
;;;  (MOSS::$INV (0 $T-COUNTRY-NAME.OF))
;;;  (MOSS::$PT.OF (0 $E-COUNTRY)) (MOSS::$OMS (0 $FN.4)))
;;;
;;; The only useful data here are $PNAM, $OMS
;;; $IS-A will be used to mark the generic property. At the end of the day, any
;;; property not marked will be defined separatedly
;;; an attribute can have other properties : $MAXT, $MINT, $ONE-OF, $SEL, $DEFT
;;; default may be recorded in the ideal

(defun reconstruct-attribute (att-id &optional class-id)
  "takes an attribute id and reconstructs its definition for a class."
  (let ((tp-name (%%has-value att-id '$PNAM *context*))
        (tp-isa (%%has-value att-id '$IS-A *context*))
        (tp-doc (%%has-value att-id '$DOCT *context*))
        (tp-min (%%has-value att-id '$MINT *context*))
        (tp-max (%%has-value att-id '$MAXT *context*))
        (tp-one-of (%%has-value att-id '$ONEOF *context*))
        (tp-sel (%%has-value att-id '$SEL *context*))
        att-def default entry entry-flag ideal-id method-list fcn
        fcn-l arg-list comment body)
    
    ;;=== entry
    ;; look for =make-entry own-method
    (setq method-list (%%has-value att-id '$OMS *context*))
    (dolist (fn-id method-list)
      (when (equal (car (%%has-value fn-id '$MNAM *context*)) '=make-entry)
        (setq entry-flag t)
        ;; mark method as processed
        (setf (get fn-id :used) t)
        ;(format t "~%; reconstruct-attribute / marking method: ~S class-id: ~S ~
        ;           attribute: ~S mark: ~S"
        ;  fn-id class-id att-id (get fn-id :used))
        ;; if not default method, reconstruct method parms
        (unless (eql (setq fcn (car (%%has-value fn-id '$FNAM *context*)))
                     'default-make-entry)
          ;; all the following junk is necessary because MOSS introduces a clause
          ;; (catch :return ...) around the body of a method!
          ;; get lambda-expr
          (setq fcn-l (symbol-value fcn))
          ;; -> something like ((args) <body>), must get rid of the (catch :return ...
          ;; get arg-list
          (setq arg-list (cadr fcn-l))
          ;; if next arg is string: comment
          (setq comment (if (stringp (caddr fcn-l)) (list (caddr fcn-l))))
          ;; remove catch :return
          (setq body (cddr (cadddr fcn-l)))
          (setq entry `(,arg-list (,@comment ,@body))))))
    
    ;;=== default
    ;; look for $deft prop or go examine ideal
    (unless (setq default (%%has-value att-id '$DEFT *context*))
      (when class-id
        ;; make ideal
        (setq ideal-id (%make-id class-id :ideal t))
        ;; load if from disk
        (%ldif ideal-id)
        ;; get attribute default value if any
        (if (boundp ideal-id) 
            (setq default (%%has-value ideal-id att-id *context*)))
        ))
    
    ;;=== mark generic property (:used t) Why??
    (dolist (gen-att tp-isa)
      (setq method-list (%%has-value gen-att '$OMS *context*))
      (dolist (fn-id method-list)
        (when (equal (car (%%has-value fn-id '$MNAM *context*)) '=make-entry)
          ;; mark method as processed
          (setf (get fn-id :used) t))))
    
    ;;=== create a selector onto the p-list
    (setf (get att-id :sel) 
      `(,@(send att-id '=get-name) "MOSS property name" "MOSS ATTRIBUTE"
           ,@(if class-id `(:class-ref ,(car (send class-id '=get-name))))))
    
    ;; put things together
    (setq att-def
          `(:att
            ,@tp-name 
            ,@(if tp-min `((:min ,@tp-min)))
            ,@(if tp-max `((:max ,@tp-max)))
            ,@(if tp-one-of `((:one-of ,@tp-one-of)))
            ;; sel not necessary if it is "exists"
            ,@(if (equal tp-sel '(:forall)) tp-sel)
            ,@(if default `((:default ,@default)))
            ,@(if entry-flag `((:entry ,@entry)))
            ,@(if tp-doc `((:doc ,@tp-doc)))
            )
          )
    ;; return def expr
    (setq att-def (format nil "~S" att-def))
    att-def
    ))

#|
? $T-PERSON-NAME
((MOSS::$TYPE (0 MOSS::$EPT)) (MOSS::$ID (0 $T-PERSON-NAME)) (MOSS::$PNAM (0 (:EN "name" :FR "nom")))
 (MOSS::$ETLS.OF (0 $SYS.1)) (MOSS::$IS-A (0 $T-NAME)) (MOSS::$INV (0 $T-PERSON-NAME.OF))
 (MOSS::$PT.OF (0 $E-PERSON)) (MOSS::$OMS (0 $FN.7)))

? (moss::reconstruct-attribute '$T-PERSON-NAME)
(:ATT (:EN "name" :FR "nom") (:ENTRY))
? (get '$T-PERSON-NAME :sel)
"(:ATT (:EN \"name\" :FR \"nom\") (:ENTRY))"

? (moss::reconstruct-attribute '$T-PERSON-NAME '$E-PERSON)
"(:ATT (:EN \"name\" :FR \"nom\") (:ENTRY))"
? (get '$T-PERSON-NAME :sel)
("nom" "MOSS property name" "MOSS ATTRIBUTE" :CLASS-REF "personne")
|#
;;;------------------------------------------------- RECONSTRUCT-ATTRIBUTE-GENERIC

;;; we look at the attributes that have no mark, i.e. that were not part of a 
;;; reconstructed class. Indeed, if an attribute is part of a class, then the
;;; corresponding generic attibute will be created automatically.
;;; there is however a very special case, namely when the attribute is also one
;;; of a class that is not reconstructed, like "OMAS AGENT" or "OMAS SKILL" having
;;; been added in the application. Currently we discard such cases since they refer
;;; to system objects and one should be very careful about adding structure to 
;;; system objects.

(defun reconstruct-attribute-generic (att-id)
  "takes a generic attribute id and reconstructs its definition as a defattribute."
  (declare (special *context* *attribute-list*))
  (let ((tp-name (%%has-value att-id '$PNAM *context*))
        ;(tp-isa (%%has-value att-id '$IS-A *context*))
        (tp-doc (%%has-value att-id '$DOCT *context*))
        (tp-min (%%has-value att-id '$MINT *context*))
        (tp-max (%%has-value att-id '$MAXT *context*))
        (tp-one-of (%%has-value att-id '$ONEOF *context*))
        (tp-sel (%%has-value att-id '$SEL *context*))
        (class-list (%%has-value att-id '$PT.OF *context*))
        att-def default entry entry-flag method-list)
    
    ;; warn if ttribute is not a generic attribute
    (when class-list
      (warn "*** attribute ~S is not a generic attribute, has class(es) ~S. ~
             we ignore such classes."
        tp-name class-list))
    
    ;;=== entry
    ;; look for =make-entry own-method
    (setq method-list (%%has-value att-id '$OMS *context*))
    (dolist (fn-id method-list)
      (when (equal (car (%%has-value fn-id '$MNAM *context*)) '=make-entry)
        (setq entry-flag t)
        ;; mark method as processed
        (setf (get fn-id :used) t)
        ;; if not default method, reconstruct method parms
        (unless (eql (car (%%has-value fn-id '$FNAM *context*)) 'default-make-entry)
          ;;;**********
          (setq entry (list "to do...")))))
    
    ;;=== default
    ;; look for $deft prop or go examine ideal
    (setq default (%%has-value att-id '$DEFT *context*))
    
    ;; put things together
    (setq att-def
          `(,@(if tp-min `((:min ,@tp-min)))              
               ,@(if tp-max `((:max ,@tp-max)))
               ,@(if tp-one-of `((:one-of ,@tp-one-of)))
               ;; sel not necessary if it is "exists"
               ,@(if (equal tp-sel '(:forall)) tp-sel)
               ,@(if default `((:default ,@default)))
               ,@(if entry-flag `((:entry ,@entry)))
               ,@(if tp-doc `((:doc ,@tp-doc)))
               )
          )
    (print att-def)
    ;; return def expr
    (setq att-def (format nil "(defattribute ~{~S~} ~{  ~S~^~%~})" tp-name att-def))
    (push (cons (car (send att-id '=get-name)) att-def) *attribute-list*) 
    att-def
    ))

#|
? (moss::reconstruct-attribute-generic '$T-OMAS-SKILL-SK-NAME)
Warning: *** attribute ((:FR "sk-name")) is not a generic attribute, has class(es)
         ($E-OMAS-SKILL). we ignore such classes.
"(defattribute (:FR \"sk-name\") )"

? (defattribute (:en "try" :fr "essai") (:entry) (:min 1) (:one-of 2 4 6))
$T-ESSAI
;;;NB. :one-of is not allowed for a generic property, which limits the use of such
;;; properties for orphans
 
((MOSS::$TYPE (0 MOSS::$EPT)) (MOSS::$ID (0 $T-ESSAI))
 (MOSS::$PNAM (0 (:EN "try" :FR "essai"))) (MOSS::$ETLS.OF (0 $SYS.1))
 (MOSS::$INV (0 $T-ESSAI.OF)) (MOSS::$OMS (0 $FN.21)) (MOSS::$MINT (0 1)))

? (MOSS::RECONSTRUCT-ATTRIBUTE-GENERIC '$T-ESSAI)
"(defattribute (:EN \"try\" :FR \"essai\") (:MIN 1)
(:ENTRY))"

? (defattribute (:en "try2" :fr "essai2") (:entry) (:unique) (:one-of 2 4 6)
(:default "Albert"))
$T-ESSAI2
((MOSS::$TYPE (0 MOSS::$EPT)) (MOSS::$ID (0 $T-ESSAI2))
 (MOSS::$PNAM (0 (:EN "try2" :FR "essai2"))) (MOSS::$ETLS.OF (0 $SYS.1))
 (MOSS::$INV (0 $T-ESSAI2.OF)) (MOSS::$DEFT (0 "Albert")) (MOSS::$OMS (0 $FN.22))
 (MOSS::$MINT (0 1)) (MOSS::$MAXT (0 1)))
? (MOSS::RECONSTRUCT-ATTRIBUTE-GENERIC '$T-ESSAI2)
"(defattribute (:EN \"try2\" :FR \"essai2\") (:MIN 1)
(:MAX 1)
(:DEFAULT \"Albert\")
(:ENTRY))"

?MOSS::*ATTRIBUTE-LIST*
(("essai2" . "(defattribute (:EN \"try2\" :FR \"essai2\") (:MIN 1)
(:MAX 1)
(:DEFAULT \"Albert\")
(:ENTRY))"))
|#
;;;------------------------------------------------------------- RECONSTRUCT-CLASS

(defun reconstruct-class (class-id)
  "takes a class id and reconstructs its definition (defconcept ...)"
  (let ((class-name (car (%%has-value class-id '$ENAM *context*)))
        (isa-list (%%has-value class-id '$IS-A *context*))
        (att-list (%%has-value class-id '$PT *context*))
        (rel-list (%%has-value class-id '$PS *context*))
        (class-doc (%%has-value class-id '$DOCT *context*))
        rec-isa-list rec-att-list rec-rel-list class-def)
    ;; get is-a classes
    (if isa-list
        (push (reconstruct-isa isa-list) rec-isa-list))
    ;; get attributes
    (dolist (att att-list)
      (push (reconstruct-attribute att class-id) rec-att-list))
    ;; get relations
    (dolist (rel rel-list)
      (push (reconstruct-relation rel class-id) rec-rel-list))
    ;;=== create a selector onto the p-list
    (setf (get class-id :sel) 
      `(,@(send class-id '=get-name) "MOSS concept name" "MOSS CONCEPT"))
    
    ;; put things together
    (setq class-def 
          (concatenate 'string
            (format nil "(defconcept ~S" class-name)
            (if isa-list (format nil "~{~%  ~S~}" rec-isa-list))
            (if rec-att-list (format nil "~{~%  ~A~}" (reverse rec-att-list)) "")
            (if rec-rel-list (format nil "~{~%  ~A~}" (reverse rec-rel-list)) "")
            (if class-doc (format nil "~%  (:doc ~{~S~^ ~})" (car class-doc)) "")
            ")")
        )
    ;; save result into class list
    (push (cons (car (send class-id '=get-name)) class-def) *class-list*)
    class-def))

#|
? (moss::reconstruct-class '$e-person)
"(DEFCONCEPT (:EN \"person\" :FR \"personne\")
            (:ATT (:EN \"email\" :FR \"email\"))
            (:ATT (:EN \"initials\" :FR \"initiales du prénom\"))
            (:ATT (:EN \"first-name\" :FR \"prénom\") (:ENTRY))
            (:ATT (:EN \"name\" :FR \"nom\") (:ENTRY))
            (:DOC :EN \"A PERSON is a human being member of a CONTACT.\"
             :FR
             \"Une PERSONNE est un être humain qui constitue un contact.\"))"
(get '$e-person :sel)
("personne" "MOSS concept name" "MOSS CONCEPT")

? (moss::reconstruct-class '$e-mission)
"(DEFCONCEPT (:EN \"mission\" :FR \"mission\")
            (:ATT (:EN \"reason\" :FR \"raison\"))
            (:ATT (:EN \"location\" :FR \"lieu\") (:ENTRY))
            (:ATT (:EN \"end date\" :FR \"date de fin\"))
            (:ATT (:EN \"start date\" :FR \"date de début\"))
            (:SUC (:EN \"person\" :FR \"personne\") (:TO \"personne\"))
            (:SUC (:EN \"country\" :FR \"pays\") (:TO \"pays\")))"
|#
;;;---------------------------------------------------------- RECONSTRUCT-FUNCTION

(defun reconstruct-function (fn-id)
  "takes a symbol pointing to a function a reconstructs the definition."
  (unless (and (boundp fn-id)
               (listp (symbol-value fn-id))
               (eql (cadr (symbol-value fn-id)) 'lambda))
    (error "Bad function name: ~S" fn-id))
  (let ((body (symbol-value fn-id)) fcn-def)
    (setq fcn-def
          (format nil "~S" `(deffunction ,(car body) ,@(cddr body))))
    (push (cons (symbol-name fn-id) fcn-def) *function-list*)
    fcn-def))

#|
? F_EXTIFF
(EXTIFF LAMBDA (NAME-STRING)
        (LET (RESULT POS)
          (IF (AND NAME-STRING (STRINGP NAME-STRING) (NOT #))
              (PROGN (SETQ NAME-STRING #) (LOOP # # # #))
            "?")))

? (moss::reconstruct-function 'F_EXTIFF)
"(DEFFUNCTION EXTIFF (NAME-STRING)
             (LET (RESULT POS)
               (IF (AND NAME-STRING (STRINGP NAME-STRING)
                        (NOT (EQUAL NAME-STRING \"\")))

 ..."
|#
;;;---------------------------------------------------------- RECONSTRUCT-INSTANCE
;;; if instance has an own method, must put variable as selector on the p-list
;;; if the instance has an attribute with an entry point, puts it on the p-list
;;; It can be used instead of a variable for making links

(defun reconstruct-instance (obj-id)
  "takes an instance id and reconstructs its defining form."
  ;; if obj-id is a REF (multiple classes) ignore it
  (when (%alive? obj-id *context*)
    (unless (%%has-value obj-id '$REF *context*)
      (let ((language (symbol-value (intern "*LANGUAGE*")))
             obj-def val-list var var-list prop-list class-name-list ep result)
        ;; get possible classes
        (setq class-name-list
              (mapcar #'(lambda (xx) (send xx '=get-name))
                (%%has-value obj-id '$TYPE *context*)))
        ;; build a stub list into which we push data (note the reverse order)
        (setq obj-def 
              `(,@(if (cdr class-name-list) class-name-list (car class-name-list))
                   defindividual))
        ;; loop 
        (setq prop-list (%get-properties obj-id))
        ;; rebuild object
        ;; ********** should be rebuilt from the raw a-list representing the object
        ;; reason: properties like $OMS are not part of the prop-list returned by
        ;; %get-properties
        (dolist (prop-id prop-list)
          ;; reset list of successors
          (setq var-list nil)
          (cond
           ((and (%is-attribute? prop-id)
                 (setq val-list (%%has-value obj-id prop-id *context*)))
            (push (append (send prop-id '=get-name) val-list)
                  obj-def))
           ((and (%is-relation? prop-id)
                 (setq val-list (%%has-value obj-id prop-id *context*)))
            ;; check for var on the plist of the suc-id
            (dolist (suc-id val-list)
              ;; test if successor has an entry point
              (cond
               ((setq ep (get suc-id :ep))
                ;; if so use it
                (push ep var-list))
               ;; try to compute an entry point for the successor
               ((setq ep (%get-entry-point-if-unique suc-id :language language))
                ;; save the entry point as a string
                (setf (get suc-id :ep) (format nil "~S" ep))
                ;; and use it
                (push (format nil "~S" ep) var-list))
                ;; otherwise look for var name
               ((setq var (get suc-id :var))
                ;; OK yes it
                (push var var-list))
               ;; otherwise create one
               (t
                (setq var (gentemp "_I-"))
                ;; variable gets set when the defindividual is executed, not when
                ;; building the text file
                ;(set var obj-id)
                (setf (get suc-id :var) var)
                (push var var-list))))
            
            ;; if anything there, add the property
            (if var-list
                (push (append (send prop-id '=get-name)
                              (reverse var-list))
                      obj-def)))
           ;; otherwise ignore property
           ))
        
        ;;=== check if an entry point can be obtained for this object
        (setq ep (%get-entry-point-if-unique obj-id :language language))
        ;(format t "~%; reconstruct-instance / ep: ~S" ep)
        ;; if so
        (if ep
            ;; add it to the p-list, for later reference to this instance
            (setf (get obj-id :ep) (format nil "~S" ep))
          ;; otherwise
          (progn
            ;; check if a variable has been defined for this particular instance
            ;; where would it have been created??
            (unless (setq var (get obj-id :var))
              ;; make one
              (setq var (gentemp "_I-"))
              ;(set var obj-id)
              ;; save it onto the p-list
              (setf (get obj-id :var) var))
            ;; add it to def
            (push (list :var var) obj-def)))
        
        
        (setq result (format nil "~S" (reverse obj-def)))
        (push result *instance-list*)
        ;(format t "~%; reconstruct-instance / object: ~S" result)
        ;; return
        result))))

#|
? $E-mission.5
((MOSS::$TYPE (0 $E-MISSION)) (MOSS::$ID (0 $E-MISSION.5))
 ($S-MISSION-PERSON (0 $E-PERSON.31)) ($S-MISSION-COUNTRY (0 $E-COUNTRY.36))
 ($T-MISSION-LOCATION (0 "MONTREAL")) ($T-MISSION-START-DATE (0 "26/10/08"))
 ($T-MISSION-END-DATE (0 "03/11/08")))

? (moss::reconstruct-instance '$e-mission.5)
"(DEFINDIVIDUAL \"mission\" (\"date de début\" \"26/10/08\")
               (\"date de fin\" \"03/11/08\") (\"lieu\" \"MONTREAL\")
               (\"raison\" \"Participation au DOCAM Summit\")
               (\"pays\" _I-18) (\"personne\" _I-18 _I-19) (:VAR _I-20))"
|#
;;;-------------------------------------------------------------- RECONSTRUCT-IS-A

(defun reconstruct-isa (class-id-list)
  "takes a list of class-ids and returns the IS-A clause.
Argument:
   a list of classes
Return:
   something like \"(:IS-A \"territory\")"
  ;; we use summary in case the class name is multi-lingual
  (cons :is-a (flatten (broadcast class-id-list '=summary))))

#|
(moss::RECONSTRUCT-ISA (list '$e-country))
(:IS-A "pays")
|#
;;;------------------------------------------------------------ RECONSTRUCT-METHOD
;;; should maybe construct a selector for methods... to allow methods on methods
;;; should be done after reconstructing instances because of own-methods
;;; Should not reconstruct method for system objects like =summary for skills
;;; However, this is not very important and it is difficult to sort between methods
;;; that were defined by the middleware and methods added by the user, since they
;;; are all in the agent package. They will be redefined...

(defun reconstruct-method (meth-id)
  "takes a method id and reconstructs the MOSS definition
Arguments:
   meth-id: id of method to reconstruct
Return:
   a triple: <name><class-name or selector> <definition>
   the second item is used for ordering the printout."
  ;(format t "~%; recontruct-method / meth-id: ~S, used mark: ~S" 
  ;  meth-id (get meth-id :used))
  (unless (get meth-id :used)
    (let ((meth-name (%%has-value meth-id '$MNAM *context*))
          (meth-body (eval (car (%%has-value meth-id '$FNAM *context*))))
          meth-type meth-def class-id class-name obj-id sel)
      ;; first find out what type of method we have
      (setq meth-type (if (%%has-value meth-id '$IMS.OF *context*) :ims :oms))      
      
      ;;=== instance methods
      (case meth-type
        (:ims
         (setq class-id (car (%%has-value meth-id '$IMS.OF *context*))
             class-name (send class-id '=get-name))
         (setq meth-def
               `(definstmethod 
                    ,@meth-name
                    ,@class-name
                  ,@(%remove-catch-return meth-body)))
         (push `(,(symbol-name (car meth-name)) ,(car class-name)
                   ,(format nil "~S" meth-def))
               *method-list*)
         )
        ;;=== own methods
        (otherwise
         (setq obj-id (car (%%has-value meth-id '$OMS.OF *context*)))
         ;; own-methods have a selector on their p-list, put when the attribute or
         ;; the object has been reconstructed
         ;; if not, then they may belong to system objects that must not be
         ;; reconstructed and can be ignored
         ;; NB. generic attributes can be separated from attributes belonging to
         ;; system object because their owner is the class *any*
         (unless (setq sel (get obj-id :sel))
           (warn "can't find the selector for this method: ~S object: ~S~%~
                  We ignore it..."
             meth-id obj-id)
           (return-from reconstruct-method nil))
         
         ;; selector is otained from p-list of method id
         (setq meth-def
               `(defownmethod 
                    ,@meth-name
                    ,sel
                  ,@(%remove-catch-return meth-body)))
         ;; put space for the selector
         (push `(,(symbol-name (car meth-name)) " " ,(format nil "~S" meth-def))
               *method-list*))
        )
      ;; not necessary to mark it, return definition
      (format nil "~S" meth-def)
      )))

#|
$E-fn.10
((MOSS::$TYPE (0 $FN)) (MOSS::$ID (0 $E-FN.10)) (MOSS::$MNAM (0 =GET-MISSION))
 (MOSS::$DOCT (0 "returns a list of property/values as strings."))
 (MOSS::$FNLS.OF (0 $SYS.1)) (MOSS::$IMS.OF (0 $E-MISSION))
 (MOSS::$FNAM (0 $E-MISSION=I=0=GET-MISSION)))

? (moss::reconstruct-method '$e-fn.10)
"(DEFINSTMETHOD =GET-MISSION \"mission\" NIL
               \"returns a list of property/values as strings.\"
                 (LET (RESULT)
                   (CASE *LANGUAGE*
                     (:FR
                      (PUSH (CONS \"pays\"
                                  (BROADCAST (HAS-COUNTRY) '=SUMMARY))
  ..."

? (moss::reconstruct-method '$fn.2)
"(DEFOWNMETHOD =MAKE-ENTRY _V-5 (MOSS::VALUE-LIST)
              \"default make-entry fonction\"
                (MOSS::%MAKE-ENTRY-SYMBOLS MOSS::VALUE-LIST))"
|#
;;;---------------------------------------------------------- RECONSTRUCT-RELATION

(defun reconstruct-relation (rel-id &optional class-id)
  "takes a relation id and reconstructs its definition for a class."
  (let ((sp-name (%%has-value rel-id '$PNAM *context*))
        (sp-isa (%%has-value rel-id '$IS-A *context*))
        (sp-doc (%%has-value rel-id '$DOCT *context*))
        (sp-suc (%%has-value rel-id '$SUC *context*))
        (sp-min (%%has-value rel-id '$MINT *context*))
        (sp-max (%%has-value rel-id '$MAXT *context*))
        (sp-one-of (%%has-value rel-id '$ONEOF *context*))
        (sp-sel (%%has-value rel-id '$SEL *context*))
        rel-def suc-list default ideal-id)
    ;;=== get suc identity (OK to use =get-name for suc are classes)
    (setq suc-list  ; JPB 140820 removing mapcan
          (reduce #'append
                  (mapcar #'(lambda(xx) (send xx '=get-name)) sp-suc)))
    
    ;;=== default
    ;; look for $deft prop or go examine ideal
    (unless (setq default (%%has-value rel-id '$DEFT *context*))
      (when class-id
        ;; make ideal
        (setq ideal-id (%make-id class-id :ideal t))
        ;; load if from disk
        (%ldif ideal-id)
        ;; get attribute default value if any
        (if (boundp ideal-id) 
            (setq default (%%has-value ideal-id rel-id *context*)))
        ))
    ;;=== mark generic property (:used t)
    (mapcar #'(lambda (xx) (setf (get xx :used) t)) sp-isa)
    
    ;;=== create a selector onto the p-list
    (setf (get rel-id :sel) 
      `(,@(send rel-id '=get-name) "MOSS property name" "MOSS RELATION"
           ,@(if class-id `(:class-ref ,(car (send class-id '=get-name))))))
    
    ;; put things together
    (setq rel-def
          `(:rel
            ,@sp-name 
            (:to ,@suc-list)
            ,@(if sp-min `((:min ,@sp-min)))
            ,@(if sp-max `((:max ,@sp-max)))
            ,@(if sp-one-of `((:one-of ,@sp-one-of)))
            ;; sel not necessary if it is "exists"
            ,@(if (equal sp-sel '(:forall)) sp-sel)
            ,@(if default `((:default ,@default)))
            ,@(if sp-doc `((:doc ,@sp-doc)))
            )
          )
    ;; return def expr
    (format nil "~S" rel-def)
    ))

#|
? $s-mission-country
((MOSS::$TYPE (0 MOSS::$EPS)) (MOSS::$ID (0 $S-MISSION-COUNTRY))
 (MOSS::$PNAM (0 (:EN "country" :FR "pays"))) (MOSS::$ESLS.OF (0 $SYS.1))
 (MOSS::$IS-A (0 $S-COUNTRY)) (MOSS::$INV (0 $S-MISSION-COUNTRY.OF))
 (MOSS::$PS.OF (0 $E-MISSION)) (MOSS::$SUC (0 $E-COUNTRY)))

? (moss::reconstruct-relation '$s-mission-country '$e-mission)
"(:REL (:EN \"country\" :FR \"pays\") (:TO \"pays\"))"
? (get '$s-mission-country :sel)
("pays" "MOSS property name" "MOSS RELATION" :CLASS-REF "mission")
|#
;;;-------------------------------------------------- RECONSTRUCT-RELATION-GENERIC

(defun reconstruct-relation-generic (rel-id &optional class-id)
  "takes a relation id and reconstructs its definition for a class."
  (declare (ignore class-id))
  (let ((sp-name (%%has-value rel-id '$PNAM *context*))
        ;(sp-isa (%%has-value rel-id '$IS-A *context*))
        (sp-doc (%%has-value rel-id '$DOCT *context*))
        (sp-suc (%%has-value rel-id '$SUC *context*))
        (sp-min (%%has-value rel-id '$MINT *context*))
        (sp-max (%%has-value rel-id '$MAXT *context*))
        (sp-one-of (%%has-value rel-id '$ONEOF *context*))
        (sp-sel (%%has-value rel-id '$SEL *context*))
        rel-def suc-list default)
    ;;=== get suc identity
    (setq suc-list ; JPB 140820 removing mapcan
          (reduce #'append
                  (mapcar #'(lambda(xx) (send xx '=get-name)) sp-suc)))
    
    ;;=== default
    ;; look for $deft prop or go examine ideal
    (setq default (%%has-value rel-id '$DEFT *context*))
    
    ;; put things together
    (setq rel-def
          `((:to ,@suc-list)
            ,@(if sp-min `((:min ,@sp-min)))
            ,@(if sp-max `((:max ,@sp-max)))
            ,@(if sp-one-of `((:one-of ,@sp-one-of)))
            ;; sel not necessary if it is "exists"
            ,@(if (equal sp-sel '(:forall)) sp-sel)
            ,@(if default `((:default ,@default)))
            ,@(if sp-doc `((:doc ,@sp-doc)))
            )
          )
    (setq rel-def (format nil "(defrelation ~{~S~}~% ~{~S~^  ~%~})"
                    sp-name  rel-def))
    ;; return def expr
    (push (cons (car (send rel-id '=get-name)) rel-def) *relation-list*)
    rel-def
    ))

#|
? (catch :error
   (defrelation (:en "en-r7" :fr "fr-r7") (:from *any*)
     (:to *any*)(:unique) 
     (:default '$e-person.1) (:doc :en "syntax trial")))

|#
;;;-------------------------------------------------- RECONSTRUCT-UNIVERSAL-METHOD

;;; strangely enough universal method do not seem to have a (catch :return ...)
;;; wrapping clause

(defun reconstruct-universal-method (meth-id)
  "takes a universal method id and reconstructs the MOSS definition"
  (unless (get meth-id :used) ; should not happen
    (let ((meth-name (%%has-value meth-id '$UNAM *context*))
          (meth-body (eval (car (%%has-value meth-id '$FNAM *context*))))
          meth-def result)
      
      (setq meth-def
            `(defuniversalmethod 
                 ,@meth-name
                 ,(cadr meth-body)
               ,@(cddr meth-body)))
      ;; not necessary to mark it, return definition
      (setq result (format nil "~S" meth-def))
      (push (cons (%make-string-from-ref (car (send meth-id '=get-name))) result)
            *universal-method-list*)
      result)))

#|
? MOSS::$UNI.66
((MOSS::$TYPE (0 MOSS::$UNI)) (MOSS::$ID (0 MOSS::$UNI.66)) (MOSS::$UNAM (0 TEST-10))
 (MOSS::$DOCT (0 "essai")) (MOSS::$FNLS.OF (0 $SYS.1)) (MOSS::$FNAM (0 *0=TEST-10)))

? (setq moss::*universal-method-list*  nil)
? (moss::reconstruct-universal-method 'moss::$UNI.66)
"(DEFUNIVERSALMETHOD TEST-10 (VAR) \"essai\" VAR)"
|#
;;;---------------------------------------------------------- RECONSTRUCT-VARIABLE

(defun reconstruct-variable (var)
  "takes a variable symbol and reconstructs its definition."
  (%ldif var)
  ;; an ontology file does not contain the following variables. They should be
  ;; integrated to the defontology macro
  ;; they are recreated whan loading a particular agent and defining its MOSS
  ;; environment
  (unless (or
           (eql var (intern "*MOSS-SYSTEM*"))
           (eql var (intern "*ONTOLOGY*"))
           (eql var (intern "*CONTEXT*"))
           (eql var (intern "*LANGUAGE*"))
           (eql var (intern "*VERSION-GRAPH*"))
           )
    ;(format t "~%; reconstruct-variable /var: ~S (intern...): ~S eql: ~S"
    ;  var (intern "*VERSION-GRAPH*") (eql var (intern "*VERSION-GRAPH*")))
    (let ((var-def (format nil "(defparameter ~S '~S)"
                     var (symbol-value var))))
      (push var-def *variable-list*)
      var-def)))

#|
? (moss::reconstruct-variable *version-graph*)
"(defparameter *VERSION-GRAPH* '((0)))"
|#
;;;---------------------------------------------------------- %REMOVE-CATCH-RETURN

(defun %remove-catch-return (expr)
  "takes a lambda list in input corresponding to a method and reconstructs a list ~
   without lambda and without the (catch :return ...) clause surronding the body.
   No check on expr."
  (let* ((arg-list (cadr expr))
         (comment (if (stringp (caddr expr)) (list (caddr expr))))
         (body (if comment (cdddr expr) (cddr expr))))
    ;; check whether we have a catch :return clause
    (if (and (listp (car body))(null (cdr body))
             (eql (caar body) 'catch)(eql (cadar body) :return))
        (setq body (cddar body)))
    `(,arg-list ,@comment ,@body)))

#|
? (%remove-catch-return '(lambda (xx) "test" (print xx) (print 33)))
((XX) "test" (PRINT XX))
? (%remove-catch-return '(lambda (xx) (print xx)(print 33)))
((XX) (PRINT XX) (PRINT 33))
|#
		
;;;===============================================================================

(format t "~%;*** MOSS v~A - Export functions loaded ***" *moss-version-number*) 

;;; :EOF