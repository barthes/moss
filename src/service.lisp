;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;================================================================================
;;;20/01/29
;;;		
;;;		S E R V I C E - (File service.lsp)
;;;	
;;;	The file contains a library of service functions for handling the 
;;;     basic structure of MOSS objects and simplifying coding of MOSS kernel. 
;;;     First written in April 1992. Rewritten in december 2002.
;;;     In April 2003 the bootstraping MOSS kernel was moved to the MOSSBOOT file
;;;================================================================================
#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
;;;================================================================================
;;; Objects belonging to several classes
;;;================================================================================

An object may belong to several classes at the same time, e.g. a person can be a
a teacher and a student at the same time. This however is poor modelling because 
a person is a Person and can have several roles (e.g. teacher or student). We can 
model this situation using virtual classes. Virtual classes have no instances.
Hence, we drop the multi-class belonging as of version 8.

This leads to droping the $resolve function.

;;;================================================================================
;;; Object structure
;;;================================================================================

An object is recognized as a MOSS objet if it has the right structure, meaning that
it is an a-list containing the $TYPE property. It does not test versions, i.e. the
test is purely structural and is not consider contexts.
The %PDM? predicate tests for the type of object.

Persistency: when using persistency, objects may not be in core, in which case their
id is unbound (or maybe NIL for some obscure reason).
Thus, when %pdm? finds an undefined or null id, and a database is active, it will
try to test the object by loading it from the database. However, after the test,
the value of the id will be restored to its preload value.The main reason is that 
some objects like methods load anciallary objects like the function implementing
the method. This behavior is semantic and not only structural and depends on the
current context.

Liveness: An object is alive if the value of its $TYPE propoerty is on the path 
from the current context to the root, and there is not a $TMBT flag on the way.

;;;================================================================================
;;; Versions
;;;================================================================================

All MOSS objects can be versioned, meaning that they can exist in different contexts
Being versioned for an object means that values associated with properties have a
context marker corresponding to the context in which they have been added or
modified. Values of an object can be defined on different branches of the version
graph. Legal values for a given context must appear on a path going from the 
newest value to the root of the version graph.
MOSS functions execute in different packages (e.g. for agents), thus the current
context is the context of the agent's package, different from *context* present 
in the MOSS functions. Thus, *context* should be removed from all the MOSS 
functions for all functions executing in the current context.

;;;================================================================================
;;; File Structures and Functions
;;;================================================================================

CLASS
  editing-box

GLOBALS (scope limited to this file)
  *versions-built*
  *disc-opened*

FUNCTIONS (DBG)
  build-versions  dbg
  veb
  vni
  vow

MACROS
  cse

FUNCTIONS
  abort-editing
; %access-entities-from-word-list
; access-from-words
; add-fact
; add-fact-string
  add-trace-tag
  %%add-value
  %add-value
  %add-value-list
  add-values
  %add-values-att-all-MLN
  %add-values-att-list
  %add-values-att-list-MLN
  %add-values-att
  %add-values-att-MLN
  %add-values-rel
  %add-values-rel-resolve-formats
  %%alive?
  %alive?
  %%allowed-context?
  %ancestors
  %build-method
  %clean-word-list
; clear-all-facts
  commit-editing
  %create-basic-new-object
  %create-basic-orphan
  %cr
; %create-entry-point-method
  %create-new-package-environment
  %crnpe-pack
  %crnpe-var
  %crnpe-system
  %crnpe-methods
  %crnpe-environment
  default-make-entry
  %determine-property-id
  delete-values
  %delete-values-att
  %delete-values-att-list
  %delete-values-att-MLN
  %delete-values-att-mln-all
  %delete-values-att-mln-list
  %delete-values-att-single
  %delete-values-rel
  %determine-inverse-property-id-for-class
  %determine-property-id-for-class
  %explode-object
  %extract
  %extract-from-id
; fail-answer?
  fill-pattern
  %fill-pattern
  filter
  %filter-against-class
  %filter-against-package
  %filter-alive-objects
; find-answer
  %get-all-attributes
  %%get-all-class-attributes
  %%get-all-class-inverse-relations
  %%get-all-class-properties
  %%get-all-class-relations
  %get-all-concepts
  %get-all-entry-points
  %get-all-instances
  %get-all-inverse-relations
  %get-all-methods
  %get-all-moss-objects
  %get-all-orphans
  %get-all-relations
  %%get-all-subclass-inverse-relations
  %%get-all-subclass-relations
  %get-all-symbols
  %get-all-versions
  %get-and-increment-counter
  %get-application-classes
  %get-application-languages
  %getc
  %get-class-canonical-name-from-ref
  %get-class-counter
  get-current-date
  get-current-year
  %get-default-from-class
; %get-entry-point
  %get-entry-point-if-unique
  %get-generic-property
  %%get-id
  %%get-id-for-class
  %%get-id-for-counter
  %%get-id-for-ep
  %%get-id-for-ideal
  %%get-id-for-inverse-property
  %%get-id-for-inverse-property-knowing-origin
  %%get-id-for-property
  %%get-id-for-virtual-class
; %get-index-weights
  %get-instance-count
  %get-internal-instance-number
  %%get-instance-order-from-id
  %get-instances-in-range
  %get-last-instance
  %%get-list-of-values-and-context
  %getm
  get-name-key
  %get-next-instance
  %get-object-class-name
; get-objects-from-attribute-and-value
  %%get-objects-from-entry-point
  %get-previous-instance
  %get-properties
  %get-property-id-from-name
  %get-property-id-from-ref
  %get-property-name-from-id
  %get-property-synonyms
  %get-relevant-weights
; %get-sp-value-restrictions
  %get-subclass-names
  %get-subclasses
; %get-tp-value-restrictions
  %get-value
  %%get-value
  %%get-value-and-context
; %get-value-from-class-ref
  %get-value-from-prop-ref
  %get-words-from-japanese-text
  %get-version-path-to-root
  %get-words-from-text
  %has-ancestor?
  %has-id-list
  %%has-inverse-properties
  %%has-properties
  %%has-properties+
  %%has-structural-properties
  %%has-terminal-properties
  %%has-value
  insert-before
  insert-list-before
  insert-list-nth
  insert-nth
  %intersect-symbol-lists
  %inverse-property-id
  %is-a?
  %is-attribute?
  %is-attribute-model?
  %is-class?
  %is-classless-object?
  %is-concept?
  %is-counter-model?
  %is-entity-model?
  %is-entry?
  %is-entry-point-instance?
  %is-generic-property-id?
  %%is-id?
  %is-instance-of?
  %is-inverse-link-model?
  %is-inverse-property?
  %is-inverse-property-ref?
  %is-method?
  %is-method-model?
  %is-model?
  %is-orphan?
  %%is-property-name?
  %is-relation?
  %is-relation-model?
  %is-structural-property?
  %is-structural-property-model?
  %is-system?
  %is-system-model?
  %is-terminal-property?
  %is-terminal-property-model?
  %is-universal-method?
  %is-universal-method-model?
  %is-value-type?
  %is-variable-name?
; %is-virtual-concept?
  %ldif
  %%ldif-raw
  %link
  %link-all
  %load-value-from-database
  %make-entity-subtree
  %make-entity-tree
  %make-entity-tree-names
  %make-entry-symbols
  %make-ep
  %make-ep-from-mln
  %%make-id
  %make-inverse-property
  %make-inverse-property-mln
  %%make-name
  %%make-name-string
  %%make-new-version
  %make-phrase
  make-ref-line
  %make-ref-object
; %make-string-from-pattern
  %make-string-from-ref
  %make-word-combinations
  %make-word-list
; %%merge-objects
  moss-trace
  moss-trace?
  moss-trace-add
  moss-untrace
; move-fact
  %occurs-in?
  package-key
; %pclass?
  %pdm?
  %pep
  %pep-pv
; pformat
; %pformat
; pop-fact
  print-sys
; %putc
  %putm
; %rank-entities-wrt-word-list
  read-fact
  %%relink-isa
  %remove-redundant-properties
  %%remprop
  %%remval
  %%remnthval
  remove-trace-tag
  replace-fact
; %reset
  %resolve
  save-new-id
  save-old-value
  %select-best-entities
  set-language
; %%set-symbol-package
  %%set-value
  %%set-value-list
  %sp-gamma
  %sp-gamma-l
  %sp-gamma1
  start-editing
  %string-norm
; %%strip-entry-point
; %%strip-entry-points
  symbol-key
  %synonym-add
  %synonym-explode
  %synonym-make
  %synonym-member
  %synonym-merge-strings
  %synonym-remove
  %subtype?
  %type?
  %type-of
  %unlink
  %validate-sp
  %validate-sp-default
  %validate-sp-max
  %validate-sp-min
  %validate-sp-nsuc
  %validate-sp-one-of
  %validate-sp-suc
  %validate-tp
  %validate-tp-max
  %validate-tp-min
  %validate-tp-mln
  %validate-tp-not-type
  %validate-tp-one-of
  %validate-tp-opr
  %validate-tp-oprall
  %validate-tp-type
  %validate-tp-valr
; %validate-tp-value
  %vg-gamma
  %vg-gamma-l
; web-active?
; web-add-text
; web-clear-delay
; web-clear-gate
; web-clear-tag
; web-clear-text
; web-dec-delay
; web-get-delay
; web-get-gate
; web-get-tag
; web-get-text
; web-set-delay
; web-set-gate
; web-set-mark
; web-set-text
  %%zap-object
  %zap-plist

History
=======
2003
 0107 To each creation of new symbols (intern), adding the specific package :moss
 0110 introducing %make-radix-from-name to reuse main functions
 0518 adding %get-and-increment-counter
2004
 0614 adding *any* to the %same-type? predicate
 0623 adding the function %get-all-orphans
 0624 adding %is-xxx? functions
 0627 adding attribute? relation? and relationship? predicates
 0628 removing package from %make-ideal-id
 0708 adding %is-class? predicate
 0712 P: removing ccl:: from ccl::set-symbol-plist
      P: adding eval-when to wrap export
      replacing (putprop... with (setf (get...
 0725 adding some classes to the list of meta-models
 4 cleaning the file, introducing mw-format
 1023 adding two functions to build a forest of application classes
      %make-entity-tree %make-entity-subtree %get-application-classes
 1026 replacing mw-format with mformat
      introducing %%merge-objects used when reloading applications
 1127 introducing %get-instance-count
2005
 0322 Introducing generic properties and property trees
      The principle of generic properties is the following:
      Each property is represented by a generic object containing the basic
      information shared by all sub-properties. E.g.
      HAS-NAME is the name of property $T-NAME that has an entry-point
               is the name of $T-PERSON-NAME ($IS-A of $T-NAME)
 0331 adding generic function %make-id to replace the set of old ones
      replacing %same-type? by %type?
      replacing %meta-model? by %system-model?
      introducing %subtype?
 0420 adding a %validate-tp-value in connection with $VRT
 0421 correction of %is-class? (adding %pdm? test)
 0428 modifying %create-basic-new-object
 0430 bug (moss::read-fact-string untel::conversation :test)in %get-properties
 0507 simplifying %make-name lambda list and upgrading %make-name-string
 0618 adding %is-variable-name?
 1126 version 6
      =========
      Introducing multilingual strings (MLN), adding merging of MLN
 1231 adding property restrictions and checks: %validat-sp and %valisate-tp
2006
 0213 adding an error test in %make-ep-from-mln
      adding %make-entries-from-mln to build =make-entry methods for MLN
 0228 adding %link-all
 0306 adding a general reset function
 0307 changing the %but-moss function
 0712 changing %determine-property-id to allow for multiple ancestors
 0718 adding %%has-properties+, %make-ref-object
 0720 replacing %%alive? by %alive in %get-value to avoid throw
 0806 kludge in %extract and %extract-from-id to allow strings as entry-points
 0813 adding get-current-year
 0814 introducing virtual classes
      modifying %is-a?
      adding %is-virtual-concept?
 1010 adding display-text
 1031 adding $DOCE option to make-name-string to create >-DOC-ENTRY names
 1106 introducing a package argument into functions that create symbols with a
      default to current package (*package*). Functions are:
      %make-id
 1107 adding package argument fo %create-basic-new-object, %get-all-instances
      modifying %make-ref-object, %create-entry-point-method
 1111 modifying %extract to allow various types of arguments and to take into 
      account packages
 1205 adding property and class synonyms, %get-value-from-class-ref, 
      %get-value-from-prop-ref, str-equal
 1227 adding %pformat
2007
 0118 adding %get-words-from-text
 0120 adding %get-relevant-weights
 0209 adding %is-generic-property-id?, find-entry-points
 0221 some conflict with ACL whenexporting symbols
 0222 %make-entry-symbols adding prefix option
 0227 modify find-entry-points to function in MOSS package
 0303 adding find-all-entry-points
 0316 modifying  %mln-get-canonical-name so that it returns a second value the 
      language tag, and defaults first to English then to the first entry it finds
      modifying %mln-extract-mln-from-ref adding a :default lannguage tag option
 0317 adding %synonym-member, %mln-member,  %mln-equal
      removing non English prefixes and suffixes for property names
 0319 removing the package option from find-all-entry-points
 0508 removing language names from %make-inverse-property-mln
 0730 adding no-resolve key to %%has-value, to use in the conversation process
 0827 adding make-string-from-pattern, %pclass?
 0831 introduced string-equal into %get-relevant-weights
 0901 adding %clean-word-list, %rank-entities-wrt-word-list, %check-entry-points
      %make-word-combinations, %select-best-entities
 0909 fixed find-entry-points when package is different from current one
 0923 fixed a buf in %make-name-string for inverse properties like ">author"
 0926 removing the caching of classes and properties
 0927 adding %get-object-class-name
 1010 adding %%get-all-subclass-inverse-relations, %%Get-All-Subclass-Relations
 1019 adding %get-property-canonical-name-from-ref 
 1019 removing %check-entry-points, %find-all-entry-points, %find-entry-points
      superseeded by find-best-entries in the path file 
 1029 adding %get-index-weight
 1030 adding %get-empty-words
 1202 adding export key to %make-ep
2008
 0125 adding %explode-object
 0130 adding %is-instance-of?
 0218 adding %GET-DEFAULT-FROM-CLASS5 
 0601 modification de la version MCL de %make-entity-tree pour ne concerver que les
      elements du package souhaite
 0604 === v5.11.2
 0611 #+OMAS -> (when (member :omas ...
 0913 adding %mln-extract
 1126 removing package key arg from %get-prop-id-from-name-and-class-ref
2009
 0515 adding read-fact pop-fact replace-fact for conversations
 0521 adding %occurs-in?
 0804 fixing bug in %mln-extract
 1129 adding fill-pattern (exported) and access-from-word-list (exported)
 1218 adding %filter-against-package %get-package-objects
      adding WOOd storage mechanism: %create-base %close-base %moss-store %moss-load
 1222 modifying %extract to cope with several classes with the same name in different
      packages
 1223 adding make-name-from-ref
2010
 0101 changing processing of $CTR in %make-id, correcting a bug in 
      %get-generic-property
 0104 adding test to %extract
 0107 correcting %get-all-concepts, adding symbol-value
 0111 simplifying the %extract function, %extract-from-id modified to add no-subclass
 0112 adding %%strip-entry-points
 0113 adding %ldif
 0316 adding test to %create-new-package-environment
 0319 modifying %type-of so that it always return a list
 0331 add %ldif to %remove-redundant-properties
 0401 adding package-key and symbol-key (exported)
 0405 moving get-answer from address.lisp to here for sharing among agents, renamed
      find-answer
 0514 merging v7.16...
 0519 adding pformat as a replacement for %pformat
 0524 ajout de l'option no-format dans find-answer
 0601 bug in find-answer arguments
 0622 saving counter in %GET-AND-INCREMENT-COUNTER for OMAS persistency
 0702 updating %ldif so that it checks agent persistency before tryin to load object 
      from an opened OMAS database
 0725 adding filter argument to find-answer function
 0822 introducing editing saving functions save-new-ids and save-old-value
      EDITING-BOX class, start-editing function, abort-editing, commit-editing
 1007 adding filter and %filter-against-class
 1015 adding make-ref-line 
 1018 modified %fill-pattern to introduce =summary method
 1020 adding %get-internal-instance-number (used in ontology window)
 1029 adding *context*, *language*,.. to %create-new-package-environment
 1030 fix up: adding :any to %%get-id-for-class and to %get-id-for-property
 1123 fixing fill-pattern
 1201 adding package key arg to start-editing, removing no-variable key arg
      fixing save-new-id, save-old-value
 1213 8.0.10 modified get-current-date adding :compact key arg
2011
 0226 modifying %get-index-weights to cache results on the task p-list
 0315 fixing get-current-date for a nicer format
 0411 fixing %determine-property-id-for-class for multiple inheritance
 0720 modifying error message in %get-generic-property
 0723 adding time key arg to get-current-year and get-current-date
 0728 adding MOVE-FACT, %%get-instance-order-from-id
 0729 adding %get-last-instance %get-next-instance %get-previous-instance
 0824 adding %get-instances-in-range
 0828 adding clear-all-facts
 1228 adding refs as args to %is-attribute? and %is-relation?
2012 
 0212 correcting %ldif to handle database problems
 0217 fixing a bug in %ldif
2012
 1025 correcting bug in %make-entity-tree
 1124 modified %get-properties to make it work for dead objects
 1125 adding %get-entry-point to find some entry point for an object
 1231 corrected %get-entry-point
2013
 0101 adding %get-entry-point-if-unique
 0120 adding web functions
 0313 replacing #+OMAS by (member :OMAS *features*) may be necessary to create
      an OMAS package in case variables from OMAS are referenced
2014
 0203 exporting string+
 0307 converting to UTF-8 encoding
 0314 modifying the %mln-member to better handle :unknown tags
 0315 modified %mln-equal
 0319 adding %mln-identical?
 0322 adding %mln-included?
 0409 adding set-language, and exporting it
 0410 adding get-objects-from-attribute-and-value (exported)
 0615 modifying %mln-equal to accept multiple synonym strings in input
 0717 adding web-clear-delay web-dec-delay, web-get-delay, web-set-delay to 
      synchronize web I/O
 0818 replacing mapcan in %fill-pattern
 1027 adding only-return and allow-all options to set-language arguments
 1110 adding "" test to set-language
 1121 explode-object => fixing mln
2015
 0403 adding add-trace-tag and remove-trace-tag
 0625 changed %get-generic-property for working with versions
      modified %pdm? to augment efficiency
 1111 adding %add-value-list, insert-list-value, insert-list-nth
 1225 allowing %get-value and %%get-value to return multiple values
 1228 %make-entry-symbols modified to accept integers
2016
 0205 we introduce a new option for %%get-id: :inv-from and defin the function
      %%get-id-for-inverse-property-knowing-origin
2018
 0114 modified %alive? and %pdm? to work for $REF objects (multiple belonging)
 0128 modified %ldif to let *context* and *language* be different in core vs on 
      disc
 0129 modified %%get-id-for-class to avoid considering virtual classes as classes
 0929 adding get-parents
2019
 0108 adding %is-ideal?
|# 

(in-package "MOSS")

;;; we need mecab for Japanese words segmentation
(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :mecab)(make-package :mecab)))

;;;=============================================================================
;;; classes
;;;=============================================================================

(defClass editing-box ()
  ((new-object-ids :accessor new-object-ids :initform nil)
   (old-object-values :accessor old-object-values :initform nil)
   (active :accessor active :initform t)
   (owner :accessor owner :initarg :owner :initform nil)
   )
  (:documentation "used to save changes while editing")
  )

;;;=============================================================================
;;; debugging functions
;;;=============================================================================

;;;-------------------------------------------------------------- build-versions
;;; fonctions for setting up some versions for testing version sensitive functions

(defparameter *versions-built* nil)

(defun build-versions ()
  (declare (special *moss-system* *version-graph* *context*))
  
  (setq *version-graph* '((0)))
  (setq *context* 0)
  (send *moss-system* '=new-version)
  (send *moss-system* '=new-version)
  (send *moss-system* '=new-version)
  (setq *context* 0)
  (send *moss-system* '=new-version)
  (send *moss-system* '=new-version)
  (setq *context* 4)
  (send *moss-system* '=new-version)
  (setq *context* 0)
  ;; build some data if not already done
  (unless *versions-built*
    (%make-concept 'PERSON '(:att NAME (:entry)))
    (%make-individual 'PERSON '(HAS-NAME "Albert")'(:var _albert))
    (with-context 3
      (%make-individual 'PERSON '(HAS-NAME "Zoe")'(:var _zoe)))
    (with-context 5
      (%make-individual 'PERSON '(HAS-NAME "Julie")'(:var _julie)))
    )
  (setq *versions-built* t)
  *version-graph*)
  
#|
(setq *version-graph*'((0)))
(build-versions)
((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0))
*version-graph*
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3
|#
;;;------------------------------------------------------------------- disc-test

(defparameter *disc-opened* nil)

(defun disc-test (&optional create)
  (declare (special *f* *disc-opened*))
  (unless *disc-opened*
    (make-package :test :use '(:cl :db.allegrocache))
    (defParameter *f* (moss::db-compute-pathname :name "TEST"))
    (if create
        (db-create :db-pathname *f* :area :test))
    (moss::db-open :db-pathname *f*)
    (moss::db-store 
     '$E-person.1 
     '((moss::$TYPE (0 $E-PERSON))(moss::$ID (0 $E-PERSON.1))
       ($T-PERSON-NAME (0 "Albert")))
     :test)
    (moss::db-store 
     '$E-person.2 
     '((moss::$TYPE (1 $E-PERSON))(moss::$ID (1 $E-PERSON.1))
       ($T-PERSON-NAME (1 "Zoe")))
     :test)
    (moss::db-store 
     '$E-person.3 
     '((moss::$TYPE (0 $E-PERSON))(moss::$ID (0 $E-PERSON.1))
       ($T-PERSON-NAME (1 "Zoe")(5 "Ursule")))
     :test
     )
    (setq *disc-opened* t)
    ))

#|
(disc-test)
|#
;;;------------------------------------------------------------------------- VEB

(defUn veb (&optional edbox) 
  "view editing box"
  (let ((box (or edbox (symbol-value (intern "*EDITING-BOX*")))))
    (if box
        (format t "~%; EDITING-BOX: ~S~%; new-object-ids:~%  ~S~%~
                 ; old-object-values: ~%  ~S~%; active: ~S~%; owner: ~S"
          box
          (new-object-ids box) 
          (mapcar #'car (old-object-values box))
          (active box)
          (owner box))
      (format t "~%; EDITING-BOX undefined."))
    ))
	
(defUn vni () (progn (print (new-object-ids (symbol-value (intern "*EDITING-BOX*")))) nil))
(defUn vov () (progn (print (old-object-values (symbol-value (intern "*EDITING-BOX*"))))
                nil))

;;;=============================================================================
;;; service functions
;;;=============================================================================
;;; some low-level service functions to render the overall code more legible
;;; No checking is done within such low level functions. The user must ensure
;;; that all arguments are of correct type. Context is not specified since
;;; objects contain all possible contexts.
;;;
;;; Service functions are presented in alphabetical order for convenience
;;;
;;; There are several classes of service functions:
;;; 	-Extractors: given an entry point (a name),
;;; the corresponding terminal property, the corresponding class, the
;;; system prefix eventually, they recover the list of obj-ids that
;;; are associated with the entry points.
;;; %def-extract and %def-get-id-list are version independent
;;; %extract and %get-id-list take versions (or context) into account
;;;	-Printing function
;;;	-Predicates for testing the nature of the various PDM objects.
;;; Tests are version dependent since at the time for checking
;;; the corresponding entity could be still non existing. This is not very
;;; important for the kernel since everything is in version 0, but it will
;;; be important for the user functions. hence the functions take an optional
;;; argument which is a version number.
;;;	-Structural functions: they deal with the internal structure of the 
;;; objects, they are used by the low-level methods.
;;;=============================================================================

;;;=========================== multilingual names ==============================
;;; Version 6 introduces multilingual names for giving names to objects.
;;; A simple name is a string.
;;;   (defconcept "Person" ...)
;;; It ispossible to give synonyms by having a more complex strings:
;;;   (defconcept "Person ; people" ...)
;;; The main name is the first one of the list. Others are synonyms.
;;; Names will be recorded as given by the user
;;; (($TYPE (0 $ENT)) ($ENAM (0 "Person ; people")) ($RDX (0 $E-PERSONNE))
;;;  ($ENLS.OF (0 $MOSSSYS)) ($CTRS (0 $E-PERSONNE.CTR)) ($PT (0 $T-PERSONNE-NOM)))
;;; this initial form will be used for printing (we assume that the user gives
;;; the string as he wants to see it printed.
;;; However, for internal purposes, the initial string will be modified to yield
;;; identifiers like $E-PERSONNE or $T-AGE for an attribute.
;;;
;;; Now assume we have several languages.
;;; A multilingual-name is a list containing language tags associated with
;;; complex strings:
;;;   (defconcept (:en "Person ; Individual" :fr "Personne ; Quidam ; Individu")
;;;         ...)
;;; the list will be handled as a multilingual-name type.
;;; The name chosen for building the identifier is the first English name if it
;;; exists, the first name otherwise.
;;; Any synonymous can be used to specify the corresponding object, whether a
;;; concept or an individual.
;;; Functions are provided to handle multilingual names
;;; %MULTILINGUAL-NAME? or MLN for testing type
;;; %GET-FIRST-NAME
;;; %MAKE-MLN
;;; %EXTRACT-NAMES
;;; %GET-NATIONAL-NAMES
;;;=============================================================================

;;;============================== entry points =================================
;;; Entry points are defined as indexes onto MOSS objects. 
;;; So far they are defined on single attributes and the :entry option triggers
;;; a default make-entry-point function taking as argument a list of atoms or
;;; a simple string.
;;; The name choice is poor since the function does not produce an entry point 
;;; but a normalized string, e.g.
;;;   ? (make-entry-point "au jour d'aujourd'hui")
;;;   AU.JOUR.D-AUJOURD-HUI
;;; The introduction of MLN may produce a list of symbols.
;;; Thus, it is better to replace the previous function by a new one, taking as
;;; arguments a simple string (upward compatibility), or an MLN (list) and
;;; returning a list of entry symbols.
;;;=============================================================================

;;;================================= packages ==================================
;;; Packages have been introduced in order to be able to define representation
;;; systems in different packages. This is necessary for OMAS in which agents 
;;; have their own package and develop ontologies and must query them in their
;;; package. In addition light processes do not keep a package reference, which
;;; can lead to problems on context switch.
;;; Thus, every function dealing with dynamically created or referenced symbols
;;; take a package argument that defaults to *package*.
;;;=============================================================================

;;;--------------------------------------------------------------- ABORT-EDITING

(defUn abort-editing (&optional editing-box)
  "when the system is in editing mode, the function operates a rollback to the ~
   start of the session. All new object-ids are made unbound, old values are ~
   restored. P-lists are updated."
  (let ((box (or editing-box (intern "*EDITING-BOX*" *package*))))
    (unless (boundp box) 
      (format t "~%; abort-editing /*editing-box* unbound in package: ~S" *package*)
      (return-from abort-editing nil))
    (setq box (symbol-value box))
    (format t "~%; abort-editing /box: ~S" box)
    ;; must be bound to an EDITING-BOX instance
    (when (and (typep box 'editing-box)
               (active box))
      ;; get new ids
      (mapc #'(lambda (xx) (remprop xx :new)
                (makunbound xx))
        (new-object-ids box))
      ;; restore old values
      (mapc #'(lambda (xx) (remprop (car xx) :saved)
                (set (car xx) (cdr xx)))
        (old-object-values box))
      ;; kill both lists
      (setf (new-object-ids box) nil)
      (setf (old-object-values box) nil)
      ;; and mark as not editing
      (setf (active box) nil)
      ;; just in case remove owner
      (setf (owner box) nil)
      :done)))

;;;--------------------------------------------- %ACCESS-ENTITIES-FROM-WORD-LIST
;;; used by agents to retrieve entities from their knowledge base

(defUn %access-entities-from-word-list (word-list &rest empty-word-lists)
  "takes a list of words, removes the empty words and tries to find entities ~
   of the KB using entry points from the list of words and filtering the results ~
   to keep only best matches.
Arguments:
   word-list: input list of words
   empty-word-list (key): list of empty words to remove from word list
Return:
   list of object ids or nil."
  (let (input entry-list)
    (format *debug-io* 
        "~&+++ %access-entities-from-word-list /data: ~S package: ~S"
      word-list *package*)
    ;; clean input of parasitic words
    (setq input (apply #'%clean-word-list word-list empty-word-lists))
    (format *debug-io* 
        "~&+++ %access-entities-from-word-list /cleaned input: ~S" input)
    ;; if nothing left, then we can't process anything and return nil
    (unless input
      (return-from %access-entities-from-word-list nil))
    ;; check whether the resulting words correspond to entry points
    ;; if so note the corresponding class, e.g. ("barthes" . $E-PERSON)
    ;(setq entry-list (%check-entry-points input))
    (setq entry-list (find-best-entries input))
    (format *debug-io* 
        "~&+++ %access-entities-from-word-list /entry list:~&  ~S"
      entry-list)
    ;; try to filter the entry-list against the input
    (setq entry-list (%rank-entities-wrt-word-list entry-list input))
    (format *debug-io* 
        "~&+++ %access-entities-from-word-list /ranked entities:~% ~S" 
      entry-list)
    ;; if no result, skip next access
    (when entry-list
      ;; select entities with highest rank
      (setq entry-list (%select-best-entities entry-list)))))

#| test in package :sa-address
? (moss::%access-entities-from-word-list '("jean-paul" "barthes"))
($E-PERSON.2)
? (access-entities-from-word-list '("barthes" "camille"))
($E-PERSON.4)
? (access-entities-from-word-list '("barthes"))
($E-PERSON.2 $E-PERSON.3 $E-PERSON.4)
? (access-entities-from-word-list '("adresse" "de" "barthes") *empty-words* 
                                  *address-words*)
($E-PERSON.2 $E-PERSON.3 $E-PERSON.4)
? (access-entities-from-word-list '("adresse" "de" "camille" "barthes") 
                                  *empty-words* *address-words*)
($E-PERSON.4)
|#
;;;----------------------------------------------------------- ACCESS-FROM-WORDS

(defUn access-from-words (word-list &key list-of-empty-word-lists)
  "takes a list of words, builds a set of formal queries and applies them to the ~
   KB. Removes duplicates.
Arguments:
   word-list: a list of words (strings) or s single string (text)
   list-of-empty-word-lists (key): empty words to be removed from word-list
Return:
   a list of object-ids or nil"
  ;; if word-list is empty, then we can't process anything
  (when word-list
    ;; if word-list is a string, we split them into words
    (if (stringp word-list)
        (setq word-list (%make-word-list word-list)))
    ;; if empty words option is there, remove empty words
    (if list-of-empty-word-lists
        (setq word-list
              (apply #'%clean-word-list word-list list-of-empty-word-lists)))
    ;; then process word list
    (let (query-list entry-list results)
      ;; try to build formal queries with the rest of input
      (setq query-list (path-build-query-list word-list))
      ;; try all queries
      (dolist (query query-list)
        ;; using each query try to access addresses...
        (setq entry-list (access query :already-parsed t))
        ;; we exit on the first resuls we get (why?)
        (setq results (append results entry-list)))
      ;; return the answer
      (delete-duplicates results))))

#|
; (in-package :address)
? (access-from-words '("barth�s" "biesel"))
($E-PERSON.3)
? (access-from-words "barth�s biesel")
($E-PERSON.3)
? (access-from-words '("pr�sident" "de" "l" "utc"))
($E-PERSON.20)
? (access-from-words "dominique")
($E-PERSON.3 $E-PERSON.10)
|#
;;;-------------------------------------------------------------------- ADD-FACT
  
(defUn add-fact (conversation tag value)
  "adds the value associated with tag in the fact area of the conversation ~
   object.
Arguments:
   conversation: a MOSS-CONVERSATION object
   tag: e.g. :data
   value::pa  any lisp expression
Return:
   the content of facts."
  (unless (%type? conversation '$CVSE)
    (terror "add-fact / conversation arg not the right type: ~S" conversation))
  (replace-fact conversation tag
                (cons (read-fact conversation tag) (list value))))

;;;------------------------------------------------------------- ADD-FACT-STRING
;;; could test :web and if there insert <BR> instead of ~%

(defUn add-fact-string (conversation tag value)
  "assumes that the value associated with tag is either nil or a string. If a ~
   string, then value must be either nil, a list or a string. If a string it ~
   gets concatenated with the previous value, if a list of strings, then one ~
   of the strings is randomly added to the previous strings.
Arguments:
   conversation: a MOSS-CONVERSATION object
   tag: e.g. :answer
   value::pa  nil, a string or a list of strings
Return:
   the content of facts."
  (unless (%type? conversation '$CVSE)
    (terror "add-fact-string / conversation arg not the right type: ~S" conversation))
  (let ((old-value (read-fact conversation tag)))
    (cond
     ;; if value is NIL don't do a thing
     ((null value))
     ;; if old value is nil set it
     ((null old-value)
      (replace-fact conversation tag value))
     ;; if old value is a not a string complain
     ((not (stringp old-value))
      (error "old-value should be a string: ~S" old-value))
     ;; if value is a string concatenate
     ((stringp value)
      (replace-fact conversation tag 
                    (format nil "~A~%~A" old-value value)))
     ;; if value is a list of string pick one at random
     ((and (listp value) (every #'stringp value))
      (setq value (nth (random (length value)) value))
      (replace-fact conversation tag 
                    (format nil "~A~%~A" old-value value)))
     ;; otherwise complain
     (t (error "value should be nil, a string or a list of strings: ~S" value))
     )))

#|
(moss::add-fact untel::conversation :test "bonjour.")
((MOSS::$TYPE (0 MOSS::$CVSE)) (MOSS::$ID (0 MOSS::$CVSE.3)) (MOSS::$ISTS (0 MOSS::$QSTE.211))
 (MOSS::$STS (0 MOSS::$QSTE.217))
 ...)

(moss::replace-fact  untel::conversation :test nil)
...
(moss::add-fact-string untel::conversation :test "Ici Albert!")
...
(moss::read-fact untel::conversation :test)
"Ici Albert!"
(moss::add-fact-string untel::conversation :test "OK ?")
...
"Ici Albert!
OK ?"
(moss::add-fact-string untel::conversation :test '("1 fois" "2 fois" "3 fois"))
...
"Ici Albert!
OK ?
2 fois"
|#
;;;A-------------------------------------------------------------- ADD-TRACE-TAG

(defun add-trace-tag (tag)
  "adds a tag to the list of tags, *debug-tags*, that trigger a trace format"
  (declare (special *debug-tags*))
  (pushnew tag *debug-tags*))

;;;A---------------------------------------------------------------- %%ADD-VALUE
;;; (%%add-value obj1-id prop-id value context) - insert a new value for a 
;;; property; no checking done on args. Context must be explicitely specified

(defUn %%add-value (obj-id prop-id value context)
  "Inserts a new value for prop-id, no checking done on arguments. Context must ~
   be explicitly given.
Arguments:
   obj-id: id of object
   prop-id: id of property for which we add the value
   value: value to add
   context: context in which we add the value
Return:
   the list representing the modified object."
  (let ((obj-l (symbol-value (%resolve obj-id))))
    ;; when editing check savings after resolving (forward reference is not changed)
    (save-old-value obj-id)
    (if (assoc prop-id obj-l)
        ;; destructively add value to the prop sublist, considering it as an a-list
        ;; and using context as a property
        (nputv-last context value (getv prop-id obj-l))
      ;; destructively add a prop entry e.g. ($T-NAME (2 "Albert")) to the object
      (nputv-last prop-id (list context value) obj-l))
    ;; return value of object (useful in steping mode)
    obj-l))

#|
test::JUNK5
(($TYPE (0 $EP)) ($ID (0 TEST::JUNK5))
 (TEST::$T-PERSON-NAME.OF (0 TEST::$E-PERSON.1 TEST::$E-PERSON.2) (2 TEST::$E-PERSON.2))
 ($EPLS.OF (0 TEST::$SYS.1)))

(with-package :test
  (%%add-value 'test::JUNK5 'test::$t-person-name.of '$PERSON.1 6))
(($TYPE (0 $EP)) ($ID (0 TEST::JUNK5))
 (TEST::$T-PERSON-NAME.OF (0 TEST::$E-PERSON.1 TEST::$E-PERSON.2) (2 TEST::$E-PERSON.2)
  (6 $PERSON.1))
 ($EPLS.OF (0 TEST::$SYS.1)))
|#
;;;A----------------------------------------------------------------- %ADD-VALUE
;;; %%get-value was modified to behave as %%has-value when there is an explicit
;;; value in the specified context

(defUn %add-value (obj-id prop-id value context 
                          &key before-value before-nth allow-duplicates)
  "Inserts a new value for a given property in the specified context. Copies ~
   the old values from a previous context if necessary. 
   We assume that prop-id is the correct local property, and that value has ~
   been validated and before-value normalized. It may be an attribute or a ~
   relation.
Arguments:
   obj-id: id of the object to be modified
   prop-id: id of the target property
   value: value to be added
   context: context in which the value is added
   before-nth (key): value - specify that we want to insert in front of position n
   before-value (key): value - specifies that we want to insert in front of value
   allow-duplicates (key): t allows to insert duplicate values (default nil)
   No cardinality checking done on args. If value is there does not do anything.
   Throws to an :error tag if the context is not allowed
Return:
   the internal format of the modified object"
  ;(format t "~%;--- %add-value /context: ~S" context)
  
  ;; %alive? loads the object if needed. If dead throws to :error
  (%alive? obj-id context)
  
  ;; in case of editing (checks if we are editing. If so, checks if the object 
  ;; is a new one. If not, put the pair (id . value) onto the old-oject-values
  ;; list, if not there)
  (save-old-value obj-id)
  
  ;; %%get-value checks allowed context, throws to :error on illegal context.
  ;; raw value is the set of values for all context
  (let ((raw-value (getv prop-id (symbol-value obj-id)))
        (value-list (%%get-value obj-id prop-id context))
        data)
    ;; if the value is already part of the value-list and allow-duplicates is 
    ;; nil, then we do nothing, i.e. we do not duplicate it
    (when 
        (or allow-duplicates (not (member value value-list :test #'equal+)))
      ;; update the data for all contexts
      (setq data
            (cons (cons context 
                        (if before-value
                            (insert-before value value-list before-value)
                          ;; if before-nth is nil, will add value at the end
                          (insert-nth value value-list before-nth)))
                  (remove context raw-value :key #'car)))
      ;; now replace all values
      (nalist-replace-pv obj-id prop-id data)
      )
    ;; return value of object (useful in stepping mode)
    (symbol-value obj-id)
    ))
#|
(defconcept "test")

;;===== test in current context
(defindividual "test" (:var _ta))

(%ADD-VALUE _TA '$T-TEST-AA 2 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72)) ($T-TEST-AA (0 2)))

(%ADD-VALUE _TA '$T-TEST-AA "a" 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72)) ($T-TEST-AA (0 2 "a")))

(%ADD-VALUE _TA '$T-TEST-AA 3 0 :before-value "a")
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72)) ($T-TEST-AA (0 2 3 "a")))

(%ADD-VALUE _TA '$T-TEST-AA 4 0 :before-nth "a")
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72)) ($T-TEST-AA (0 2 3 "a" 4)))

(%ADD-VALUE _TA '$T-TEST-AA 1 0 :before-nth 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72)) ($T-TEST-AA (0 1 2 3 "a" 4)))

(setq *version-graph*
      '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))

;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3

;; add something in context 2
(%ADD-VALUE _TA '$T-TEST-AA 1 2)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72)) ($T-TEST-AA (0 1 2 3 "a" 4)))

(%ADD-VALUE _TA '$T-TEST-AA 5 2)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72))
 ($T-TEST-AA (2 1 2 3 "a" 4 5) (0 1 2 3 "a" 4)))

(%ADD-VALUE _TA '$T-TEST-AA 6 2 :before-nth 1)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72))
 ($T-TEST-AA (2 1 6 2 3 "a" 4 5) (0 1 2 3 "a" 4)))

(%ADD-VALUE _TA '$T-TEST-AA 6 5 :before-value "a")
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72))
 ($T-TEST-AA (5 1 2 3 6 "a" 4) (2 1 6 2 3 "a" 4 5) (0 1 2 3 "a" 4)))

(%ADD-VALUE _TA '$T-TEST-AA 6 5 :before-value "a" :allow-duplicates t)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.72))
 ($T-TEST-AA (5 1 2 3 6 6 "a" 4) (2 1 6 2 3 "a" 4 5) (0 1 2 3 "a" 4)))

;;===== other tests in package TEST
(setq test::*version-graph*
      '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))

;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3


;; change the name of a person defined in context 0, adding "Robert" in context 6
(with-package :test
    (%add-value 'test::$e-PERSON.1 'test::$T-PERSON-NAME "Robert" 6)))
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.1))
 (TEST::$T-PERSON-NAME (6 "Albert" "Robert") (0 "Albert")))

;; adding a new value directly in context 6
(with-package :test
  (with-context 6
    (%add-value 'test::$e-PERSON.1 'test::$T-PERSON-NAME "Vincent" 6)))
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.1))
 (TEST::$T-PERSON-NAME (6 "Albert" "Robert" "Vincent") (0 "Albert")))
|#
;;;------------------------------------------------------------- %ADD-VALUE-LIST
;;; similar to %add-value but add a whole list

(defun %add-value-list (obj-id prop-id value-list context &key  
                               before-value before-nth allow-duplicates)
  "Inserts a new value for a given property in the specified context. Copies ~
   the old values from a previous context if necessary. 
   We assume that prop-id is the correct local property, that the values ~
   have been validated, and before-value has-been normalized for attributes. ~
   prop-id may be an attribute or a relation.
Arguments:
   obj-id: id of the object to be modified
   prop-id: id of the target property
   value-list: list of values to be added
   context: context in which the value is added
   before-nth (key): value - specify that we want to insert in front of position n
   before-value (key): value - specifies that we want to insert in front of value
   allow-duplicates (key): t allows to insert duplicate values (default nil)
   No cardinality checking done on args. If value is there does not do anything.
   Throws to an :error tag if the context is not allowed
Return:
   the internal format of the modified object"
  ;(format t "~%;--- %add-value /context: ~S" context)
  
  ;; %alive? loads the object if needed. If dead throws to :error
  (%alive? obj-id context)
  
  ;; in case of editing (checks if we are editing. If so, checks if the object 
  ;; is a new one. If not, put the pair (id . value) onto the old-oject-values
  ;; list, if not there)
  (save-old-value obj-id)
  
  ;; %%get-value checks allowed context, throws to :error on illegal context.
  ;; raw value is the set of values for all context
  (let ((raw-value (getv prop-id (symbol-value obj-id)))
        (old-list (%%get-value obj-id prop-id context))
        data)
    ;; when duplicates are not allowed, we remove them from the list to add
    (unless allow-duplicates
      (setq value-list 
            (delete-duplicates (list-difference value-list old-list))))
    ;;  add values only when there is something left in value-list
    (when value-list
      ;; update the data for all contexts
      (setq data
            (cons (cons context 
                        (if before-value
                            (insert-list-before value-list old-list before-value)
                          ;; if before-nth is nil, will add value at the end
                          (insert-list-nth value-list old-list before-nth)))
                  (remove context raw-value :key #'car)))
      ;; now replace all values
      (nalist-replace-pv obj-id prop-id data)
      )
    ;; return value of object (useful in stepping mode)
    (symbol-value obj-id)
    ))

#|
(defconcept "test")

;;===== test in current context
(defindividual "test" (:var _ta) ("aa" 2 3 "a"))
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.73)) ($T-TEST-AA (0 2 3 "a")))

(%ADD-VALUE-list _TA '$T-TEST-AA '("a" 3 4) 0 :allow-duplicates t)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.5)) ($T-TEST-AA (0 2 3 "a" "a" 3 4)))

(%ADD-VALUE-list _TA '$T-TEST-AA nil 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.73)) ($T-TEST-AA (0 2 3 "a")))

(%ADD-VALUE-list _TA '$T-TEST-AA '(4 "b") 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.74)) ($T-TEST-AA (0 2 3 "a" 4 "b")))

(%ADD-VALUE-list _TA '$T-TEST-AA '(4 5 6 "b") 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.74)) ($T-TEST-AA (0 2 3 "a" 4 "b" 5 6)))

(%ADD-VALUE-list _TA '$T-TEST-AA '(3 4 5 6 "a") 0 :before-value "a")
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.74)) ($T-TEST-AA (0 2 3 "a" 4 "b" 5 6)))

(%ADD-VALUE-list _TA '$T-TEST-AA '(3 "a") 0 :before-value "a" :allow-duplicates t)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.74)) ($T-TEST-AA (0 2 3 3 "a" "a" 4 "b" 5 6)))

(%ADD-VALUE-list _TA '$T-TEST-AA '(7) 0 :before-nth "a")
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.74)) ($T-TEST-AA (0 2 3 3 "a" "a" 4 "b" 5 6 7)))

(%ADD-VALUE-list _TA '$T-TEST-AA '(11 22) 0 :before-nth 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.74))
 ($T-TEST-AA (0 11 22 2 3 3 "a" "a" 4 ...)))

(setq *version-graph*
      '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))

;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3
(defindividual "test" (:var _ta) ("aa" "a" 2))

;; add something in context 2
(%ADD-VALUE-LIST _TA '$T-TEST-AA '(4 5) 2)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.78)) ($T-TEST-AA (2 "a" 2 4 5) (0 "a" 2)))

(%ADD-VALUE-LIST _TA '$T-TEST-AA '(6 7) 2)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.78)) ($T-TEST-AA (2 "a" 2 4 5 6 7) (0 "a" 2)))

(%ADD-VALUE-LIST _TA '$T-TEST-AA '(2 6) 2 :before-nth 1)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.78)) ($T-TEST-AA (2 "a" 2 4 5 6 7) (0 "a" 2)))

(%ADD-VALUE-list _TA '$T-TEST-AA '(6 7) 5 :before-value "b")
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.78))
 ($T-TEST-AA (5 "a" 2 6 7) (2 "a" 2 4 5 6 7) (0 "a" 2)))

(%ADD-VALUE-LIST _TA '$T-TEST-AA '(6 7) 5 :before-value "a" :allow-duplicates t)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.78))
 ($T-TEST-AA (5 6 7 "a" 2 6 7) (2 "a" 2 4 5 6 7) (0 "a" 2)))
|#
;;;=============================================================================
;;;                          ADD PROPERTY VALUES
;;;=============================================================================

;;;------------------------------------------------------------------ ADD-VALUES
;;; API function adding a list of values to a specific object associated to a
;;; specific property 
;;; the property may be an attribute or a relation
;;; the object may be an instance or an orphan

(defun add-values (obj-id prop-ref value-list &rest key-list
                          &key context before-value before-nth no-warning
                          &allow-other-keys)
  "API function adding a list of values to a specific object associated to a ~
   specific property, the property may be an attribute or a relation, the object ~
   may be an instance or an orphan. Prints warning messages if no-warning is false.
Arguments:
   obj-id: concerned oject
   prop-ref: prop-id or prop-name or string or relation formats
   value-list: values to add: simple values or mln or list of ids or :none
   allow-duplicates (key): if true allows duplicating values
   context (key): specified context if different from current
   no-warning (key): if t means that no message will be printed
   before-value (key): value in front of which we want to insert new values
   before-nth (key): integer: position at which we want to insert new-values
   export (key): for exporting entry points from the MOSS package
Return:
   1. the internal list of the modified object.
   2. the list of warnings"
  (let (header prop-id obj-l msg-list message-list)
    
    ;; if context is specified (key), check if legal, if illegal throw to :error
    (if context 
        (%%allowed-context? context)
      ;; if context is not specified, then use local context
      (setq context (symbol-value (intern "*CONTEXT*"))))
    
    (setq header ; must wait for a value of context
          (format nil "Warning: when trying to add values (~{~S~^ ~}) to object ~S ~
                  for property ~S in package ~S and context ~S:"
            (if (listp value-list) value-list (list value-list))
            obj-id prop-ref (package-name *package*) context))
    
    (unless value-list
      (push
       (format nil "No value to add to ~S for property ~S" obj-id prop-ref)
       message-list)
      (unless no-warning (mformat "~{~%~S~}" (cons header message-list)))
      (return-from add-values (values (symbol-value obj-id) message-list)))
    
    ;; check if object is alive in that context, if not throw to :error
    (%%alive? obj-id context)
    
    ;; recover prop-id from prop-ref, including moss properties
    (setq prop-id (%%get-id prop-ref :prop :class-ref 
                            (car (%type-of obj-id context))
                            :include-moss t))
    
    ;; if not found for orphans, use generic property
    (if (and (null prop-id)(%is-orphan? obj-id context))
        (setq prop-id (%%get-id prop-ref :prop :include-moss t)))
    
    ;; if still null and object is not an orphan, prop-id does not belong to class,
    ;; use generic attribute
    (if (and (null prop-id)
             (setq prop-id (%%get-id prop-ref :prop :include-moss t)))
      (push
       (format nil "Property ~A does not belong to the object ~A class(es). ~
               ~%We add it anyway using the generic attribute."
         prop-ref obj-id)
       msg-list)
      )
    
    ;; if still nil, then complain
    (unless prop-id
      (push
       (format nil "Cannot find property ~S." prop-ref)
       message-list)
      (unless no-warning (mformat "~{~%~S~}" (cons header message-list)))
      (return-from add-values (values (symbol-value obj-id) message-list)))
    
    (cond
     ;; if attribute, call ad hoc function
     ((%is-attribute? prop-id context)
      (multiple-value-setq (obj-l message-list)
        (apply #'%add-values-att obj-id prop-id value-list context 
               :no-warning t key-list))
      )
     ;; if relation, call ad hoc function (will link successors)
     ((%is-relation? prop-id context)
      (multiple-value-setq (obj-l message-list)
        (%add-values-rel 
         obj-id prop-id value-list :context context :before-value before-value 
         :before-nth before-nth :no-warning t))
      )
     (t
      (push "Property cannot be found." message-list)
      (return-from add-values (values (symbol-value obj-id) message-list)))
     )
    
    ;; here messages come from called functions
    (unless no-warning
      (if (or msg-list message-list)
          (mformat "~{~%~A~}" `(,header ,@msg-list ,@(reverse message-list)))))
    
    ;; return list of added values and a list of warning messages
    (values obj-l (if message-list (cons header message-list)))))

#|
Tests done in the z-moss-tests-add-values.lisp file
|#
;;;=============================================================================
;;;                         ADD ATTRIBUTE VALUES
;;;=============================================================================
;;; the following functions are meant to be used to add values to attributes

;;;----------------------------------------------------- %ADD-VALUES-ATT-ALL-MLN
;;; adding an MLN value to an existing MLN value
;;; the characteristic of an MLN value is the number of languages. Adding a new
;;; value for any of the languages must be compatible with the restrictions
;;; Hence, restrictions are checked for each language and the values are added
;;; for each language successively

(defun %add-values-att-all-MLN (obj-id tp-id value-list old-data-list context
                                       &rest key-arg-list
                                       &key export
                                       &allow-other-keys)
  "adding an MLN value to an existing MLN value.
Arguments:
   see %add-values-att 
   value-list is a list containing a single MLN
   old-data-list is the list of existing values, it should contain a single MLN
Return:
   modified value of object-id"
  (let ((old-mln (car old-data-list))
        language-list result val-list old-data-val fn new-values new-data
        message-list)
    ;; if value-list is nil do nothing
    ;(format t "~%; %add-values-att-all-MLN /old-mln: ~S" old-mln)
    (when value-list 
      ;; get all languages of the new value
      (setq language-list (mln::get-languages (car value-list)))
      
      ;; for each language, do directly with synonyms
      (dolist (lan language-list)
        ;; get list of synonyms (values in the specified language)
        (setq val-list (mln::extract (car value-list) :language lan))
        ;; extract eventual data associated with the current language
        (when old-mln
          ;; get the list of synonyms
          (setq old-data-val (mln::extract old-mln :language lan)))
        
        ;; call the adding function for non MLN values (obj-id is ignored)
        ;; returns {:failure|<list of values>} and <list of added values>
        ;; no-warning is passed as it is
        (multiple-value-setq (result new-values message-list)
          (apply #'%add-values-att-list-MLN obj-id tp-id val-list
                 old-data-val context key-arg-list))
        
        ;(format t "~%; %add-values-att-all-MLN /result: ~S" result)
        ;; we do something only in case it is not a failure
        (unless (or (eql result :failure)(null result))
          ;(format t "~%; %add-values-att-all-MLN /old-mln 2: ~S" old-mln)
          ;(format t "~%; %add-values-att-all-MLN /lan: ~S" lan)
          
          ;; if non nil, replace old value for specific language
          (if old-mln
              (setq old-mln (mln::set-values old-mln lan result))
            (setq old-mln (mln::make-mln result :language lan)))
          
          ;; save new entries for later creating entry ponts and =if-added
          (push (cons lan new-values) new-data))
        ) ; end dolist language
      
      ;; here old-mln has been modified to include new values
      ;; before adding check if there is a =xi method
      (setq old-mln (-> tp-id '=xi old-mln))
      
      ;; update object with new MLN
      (%%set-value obj-id old-mln tp-id context)
      ;; create all new entry points and invoke =if-added
      (setq fn (get-method tp-id '=make-entry))
      (dolist (pair new-data)
        (with-language (car pair)
          (dolist (val (cdr pair))
            (if fn
                (%make-ep (-> tp-id '=make-entry val) tp-id obj-id 
                          :context context :export export))
            ;; do bookkeeping
            )
          ))
      ;; do bookkeeping
      (-> tp-id '=if-added old-mln obj-id)
      )
    ;; return object internals
    (values (symbol-value obj-id) message-list)))

#|
The function is tested through add-values in z-moss-tests-add-values/lisp

(defconcept "test")
(defindividual "test" (:var _td))

(catch :error
 (defattribute "da" (:class-ref "test")(:type :mln)))
(%add-values-att-all-MLN _td '$T-TEST-DA '((:fr "Albert")) 
                         (%%has-value _td '$T-TEST-DA 0) 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.2)) ($T-TEST-DA (0 ((:FR "Albert")))))

(catch :error
 (defattribute "db" (:class-ref "test")(:type :string)))
(%add-values-att-all-MLN _td '$T-TEST-DB '((:fr "Albert")) 
                              (%%has-value _td '$T-TEST-DB 0) 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.3)) ($T-TEST-DB (0 ((:FR "Albert")))))

(%add-values-att-all-MLN _td '$T-TEST-DB '(((:fr "Zoe" "J�r�me")))
                              (%%has-value _td '$T-TEST-DB 0) 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.3))
 ($T-TEST-DB (0 ((:FR "Albert" "Zoe" "J�r�me")))))

(%add-values-att-all-MLN _td '$T-TEST-DB '(((:fr 12 13)))
                              (%%has-value _td '$T-TEST-DB 0) 0)
Error: illegal mln format: ((:FR 12 13))
;; MLN synonms can only be strings...

(catch :error
       (defattribute "dd" (:class-ref "test")(:not-type :string)))
(%add-values-att-all-MLN _td '$T-TEST-DD '(((:fr "Dan")))
                         (%%has-value _td '$T-TEST-DD 0) 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.3))
 ($T-TEST-DB (0 ((:FR "Albert" "Zoe" "J�r�me"))))
 ($T-TEST-DD (0 ((:FR "Dan")))))
;; not-type does not apply to MLN values

(defattribute "de" (:class-ref "test")(:unique))
(%add-values-att-all-MLN _td '$T-TEST-DE '(((:fr "Ernest")))
                              (%%has-value _td '$T-TEST-DE 0) 0)
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.3)) 
 ($T-TEST-DB (0 ((:FR "Albert" "Zoe" "J�r�me"))))
 ($T-TEST-DD (0 ((:FR "Dan")))) 
 ($T-TEST-DE (0 ((:FR "Ernest")))))

(%add-values-att-all-MLN _td '$T-TEST-DE '(((:fr "Fran�ois" "Georges")))
                         (%%has-value _td '$T-TEST-DE 0) 0)
Warning: too many values ("Ernest" "Fran�ois" "Georges") for attribute $T-TEST-DE of
         object $E-TEST.3 in package "MOSS" and context 0, max is 1.
We add them anyway.
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.3))
 ($T-TEST-DB (0 ((:FR "Albert" "Zoe" "J�r�me"))))
 ($T-TEST-DD (0 ((:FR "Dan")))) 
 ($T-TEST-DE (0 ((:FR "Ernest" "Fran�ois" "Georges")))))

(defattribute "df" (:class-ref "test")(:value "Nicolas"))
(%add-values-att-all-MLN _td '$T-TEST-DF '(((:fr "Fran�ois" "Georges")))
                         (%%has-value _td '$T-TEST-DF 0) 0)
;; no restriction on that value...

Warning: value to add to attribute $T-TEST-DF should be "Nicolas" rather than "Georges"
         in package #<The MOSS package> and context 0
Warning: we ignore that value.
Warning: value to add to attribute $T-TEST-DF should be "Nicolas" rather than "Fran�ois"
         in package #<The MOSS package> and context 0
Warning: we ignore that value.
Warning: value list to add to attribute $T-TEST-DF should be ("Nicolas") rather than NIL
in package #<The MOSS package> and context 0

(defattribute "dg" (:class-ref "test")(:min 2)(:entry))
(%add-values-att-all-MLN _td '$T-TEST-DG '(((:fr "Fran�ois")))
                         (%%has-value _td '$T-TEST-DG 0) 0)
Warning: not enough values ("Fran�ois") for attribute $T-TEST-DG of object $E-TEST.3 in
         package "MOSS" and context 0, min is 2.
We add them anyway.
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.3)) 
 ($T-TEST-DB (0 ((:FR "Albert" "Zoe" "J�r�me"))))
 ($T-TEST-DD (0 ((:FR "Dan"))))
 ($T-TEST-DE (0 ((:FR "Ernest" "Fran�ois" "Georges"))))
 ($T-TEST-DF (0 ((:FR "Fran�ois" "Georges"))))
 ($T-TEST-DG (0 ((:FR "Fran�ois")))))

FRAN�ois
(($TYPE (0 $EP)) ($ID (0 FRAN�OIS)) ($T-TEST-DG.OF (0 $E-TEST.3)) ($EPLS.OF (0 $SYS.1)))

(%add-values-att-all-MLN _td '$T-TEST-DG '(((:fr "Fran�ois")(:en "Andrew")))
                              (%%has-value _td '$T-TEST-DG 0) 0)
Warning: not enough values ("Andrew") for attribute $T-TEST-DG of object $E-TEST.3 in
         package "MOSS" and context 0, min is 2.
We add them anyway.
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.3)) 
 ($T-TEST-DB (0 ((:FR "Albert" "Zoe" "J�r�me"))))
 ($T-TEST-DD (0 ((:FR "Dan")))) 
 ($T-TEST-DE (0 ((:FR "Ernest" "Fran�ois" "Georges"))))
 ($T-TEST-DF (0 ((:FR "Fran�ois" "Georges"))))
 ($T-TEST-DG (0 ((:FR "Fran�ois") (:EN "Andrew")))))

(%add-values-att-all-MLN _td '$T-TEST-DG 
                              '(((:fr "Isidore")(:en "Bill")(:it "Mario")))
                         (%%has-value _td '$T-TEST-DG 0) 0)
Warning: not enough values ("Mario") for attribute $T-TEST-DG of object $E-TEST.3 in
         package "MOSS" and context 0, min is 2.
We add them anyway.
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.3)) 
 ($T-TEST-DB (0 ((:FR "Albert" "Zoe" "J�r�me"))))
 ($T-TEST-DD (0 ((:FR "Dan")))) 
 ($T-TEST-DE (0 ((:FR "Ernest" "Fran�ois" "Georges"))))
 ($T-TEST-DF (0 ((:FR "Fran�ois" "Georges"))))
 ($T-TEST-DG (0 ((:FR "Fran�ois" "Isidore")(:EN "Andrew" "Bill") (:IT "Mario")))))
|#
;;;-------------------------------------------------------- %ADD-VALUES-ATT-LIST
;;; here we assume that the values are not MLN values

(defun %add-values-att-list (obj-id tp-id value-list old-data-list context 
                                    &rest key-arg-list 
                                    &key allow-duplicates export no-warning
                                    before-value before-nth
                                    &allow-other-keys)
  "add a list of NON-MLN values to an attribute that has non-MLN values or no value.
Arguments:
   see %add-values-att
Return:
   1. the internal representation of the updated object
   2. a list of warning messages."
  (let (result-val fn message-list msg-list)
    ;; adjust context
    (setq context (or context (symbol-value (intern "*CONTEXT*"))))
    
    ;; new values must be normalized using the =xi method
    ;; we assume that the old values have been normalized!
    (setq value-list 
          (remove nil (mapcar #'(lambda(xx) (-> tp-id '=xi xx)) value-list)))
    
    ;; remove duplicates if not allowed, preserving order in value-list
    (unless allow-duplicates
      (setq value-list (list-difference value-list old-data-list)))
    ;(format t "~%; %add-values-att-list /value-list: ~S" value-list)
    
    ;; check here the list of values to add against restrictions: value, type,
    ;; not-type, one-of, between, outside
    (multiple-value-setq (result-val message-list)
      (%validate-tp
       tp-id 
       value-list
       :obj-id obj-id
       :context context
       :restrictions `($VALR $TPRT $NTPR $ONEOF $OPR)
       :no-warning t))
    
    ;; must check now global constraints on resulting list (restrictions do not
    ;; depend on the order of the values): same, different, min, max
    (multiple-value-setq (result-val msg-list)
      (%validate-tp
       tp-id 
       (append old-data-list result-val)
       :obj-id obj-id
       :context context
       :restrictions `($MAXT $MINT $OPRALL) ; cannot add $OPR only for same or different
       :no-warning t))
    ;(format t "~%; %add-values-att-list /result-val ~S" result-val)
    (setq message-list (append message-list msg-list))
    
    ;;********** here one must remove old values from the list of adjusted values
    ;; taking care of duplicated values, e.g. do a "strict-list-difference"
    (setq result-val 
          (list-difference result-val old-data-list allow-duplicates))
    ;(format t "~%; %add-values-att-list /result-val ~S" result-val)
    
    ;; if nil cannot add new values, therefore do not add new values
    (when result-val
      (unless (eql result-val :failure)
        
        ;; if before option there, must norm corresponding value
        (if before-value (-> tp-id '=xi before-value))
        
        ;; we use %add-value-list to keep the right order of the added values
        (%add-value-list obj-id tp-id result-val context :allow-duplicates allow-duplicates
                         :before-value before-value :before-nth before-nth)
        
        ;; check if property is an entry point
        (setq fn (get-method tp-id '=make-entry))
        ;; use saved values to produce entry
        ;(format t "~%; %add-values-att-list /val-list ~S" val-list)
        
        ;; however, now we must create entry-points and do some bookkeeping
        (dolist (val result-val)
          (if fn
              (%make-ep (-> tp-id '=make-entry val) tp-id obj-id 
                        :context context :export export))
          ;;
          ;(format t "~%; %add-values-att-list / calling =if-added: ~S" val)
          (-> tp-id '=if-added val obj-id)
          )
        ))
    
    (unless no-warning
      (mformat "~{~%~S~}" message-list))
    
    ;; return 2 values
    (values (symbol-value obj-id) message-list)))
    
#|
see file z-moss-add-values.lisp
|#
;;;----------------------------------------------------- %ADD-VALUES-ATT-LIST-MLN
;;; We call this function for a particular language and want to recover the
;;; new set of synonyms for this language
;;; we should return the list of added values for a particular language so that
;;; we can later call =if-needed with an updated object

(defun %add-values-att-list-MLN (obj-id tp-id value-list old-data-list context 
                                             &rest key-arg-list 
                                             &key allow-duplicates
                                             before-value before-nth no-warning
                                             &allow-other-keys)
  "add a list of NON-MLN values to an attribute that has non-MLN values or no value.
Arguments:
   see %add-values-att
Return:
   2 value:
     1 the list of updated values
     2 the list of added values (for this language)"
  (let (final-value-list result-val val-list message-list)
    ;; cheap test
    (unless value-list
      (return-from %add-values-att-list-MLN old-data-list))
    
    ;; remove duplicates if not allowed; not sure order is preserved in value-list
    (setq val-list
          (if allow-duplicates
              value-list
            (list-difference value-list old-data-list)))
    ;; none of the tests on a single value applies since they would have to apply 
    ;; to all languages
    
    ;; now since we have MLNs, all values were already tested for strings
    ;; check each value in turn; if nothing left will do nothing
    
    ;; check insertion order
    (setq final-value-list
          (cond
           ;; nothing-left
           ((null val-list) old-data-list)
           ;; cheap test
           ((null old-data-list) val-list)
           ((and before-value (stringp before-value))
            (insert-list-before val-list old-data-list before-value))
           ((and before-nth (numberp before-nth))
            (insert-list-nth  val-list old-data-list before-nth))
           (t (append old-data-list val-list))))
    
    ;; the only tests that could apply would be :min :max, however they are soft
    ;; tests, which makes them useless. Maybe we should print a warning message?
    
    ;; we check restrictions on :one-of, :min, :max, :same, :different 
    ;; no check on :value, :type, :not-type since values are strings nor on :
    ;; :between or outside since they are not numbers
    (multiple-value-setq (result-val message-list)
          (%validate-tp   
           tp-id final-value-list :context context :no-warning no-warning :mln t
           :obj-id obj-id))
    
    ;; return the modified list of values
    ;;********** todo
    (values result-val val-list message-list)))

#|
(defconcept "test")
(defindividual "test" (:var _et))

;; restriction on type
(defattribute "ea" (:class-ref "test")(:type :string))
$T-TEST-EA
(%add-values-att-list-MLN _et '$T-TEST-EA '("Albert" "Bernard") nil 0)
("Bernard" "Albert")
("Albert" "Bernard")
(%add-values-att-list-MLN _et '$T-TEST-EA '("Charles") '("Albert" "Bernard") 0)
("Albert" "Bernard" "Charles")
("Charles")
(%add-values-att-list-MLN _et '$T-TEST-EA '("Albert" 12) nil 0)
Warning: Value 12 for attribute $T-TEST-EA is not of type :STRING in package
         #<The MOSS package> and context 0
Warning: we ignore that value.
("Albert")
("Albert")
(%add-values-att-list-MLN _et '$T-TEST-EA '("Albert" 12) '("Zoe") 0)
Warning: Value 12 for attribute $T-TEST-EA is not of type :STRING in package
         #<The MOSS package> and context 0
Warning: we ignore that value.
("Zoe" "Albert")
("Albert")
|#
;;;------------------------------------------------------------- %ADD-VALUES-ATT
;;; Main function to add attribute values whatever their format

(defun %add-values-att (obj-id tp-id value-list context &rest key-arg-list
                               &key no-warning &allow-other-keys)
  "adding a set of values to an attribute of an object id supposed to exist in the
   specific context.
Arguments:
   obj-id: id of the object
   tp-id: id of the attribute
   value-list: list of values to add
   context: current context
   allow-duplicates (key): allows duplicate values
   before (key): must insert in fromt of the specific value
   before-nth (key): insert at the nth position
   export (key): t/nil to export entry points built in the MOSS mackage
   language (key): current language
   no-warning (key): t/nil, if t no warning issued (default nil)
Return:
   1. the internal representation of the modified objet
   2. the list of warning messages"
  (let (message-list)
    ;(format t "~%; %add-values-att / value-list 1: ~S" value-list)
    ;; check first if value-list is NIL which is illegal
    (unless value-list
      (push 
       (format nil "while trying to add attibute values to object ~S for attribute ~S the ~
           list of values to add is empty." obj-id tp-id)
       message-list)
      (unless no-warning
        (mformat "~{~%~S~}" message-list))
      (return-from %add-values-att (values (symbol-value obj-id) message-list))
      )
    
    (let (old-data-list version)
      ;; if we want to add no-value but there are already values in this context
      ;; we loose and return unmodified object
      ;; <to-do>
      (multiple-value-setq (old-data-list version) 
        (%get-value obj-id tp-id context))
      
      ;;=== first see if we do not want to add a null value...
      (when (eql value-list :none)
        ;; check if we have a non null value in the current context
        (when (and old-data-list (eql context version))
          (push 
           (format nil "while trying to set a null value to object ~S for attribute ~
               ~S, the list of existing values is non empty: ~S." 
             obj-id tp-id old-data-list)
           message-list)        
          (unless no-warning
            (mformat "~{~%~S~}" message-list))
          (return-from %add-values-att (values (symbol-value obj-id) message-list))
          )
        
        ;; set value to nil shodowing any values from previous context
        (%%set-value obj-id nil tp-id context)
        (return-from %add-values-att (values (symbol-value obj-id) message-list))
        )
      
      ;; check if we have an MLN value somewhere, in which case we call a special function
      ;(format t "~%; %add-values-att / <list>: ~S" (append value-list old-data-list))
      ;; %mln? is used for backward compatibility
      (when (or
             (eql (car (%get-value tp-id '$TPRT)) :mln)
             (some #'(lambda (xx) (or (mln::%mln? xx) (mln::mln? xx)))
                   (append value-list old-data-list)))
        (return-from %add-values-att ; return 2 values
          (apply #'%add-values-att-mln obj-id tp-id value-list
                 old-data-list context key-arg-list)))
      
      ;(format t "~%; %add-values-att / value-list 2: ~S" value-list)
      
      ;; here we have a list of non MLN values
      (return-from %add-values-att ; return 2 values
        (apply #'%add-values-att-list obj-id tp-id value-list old-data-list 
               context key-arg-list)))
    
    (values (symbol-value obj-id) message-list)))
 
#|
;; test: see Z-moss-tests-add-attribute.list
|#
;;;--------------------------------------------------------- %ADD-VALUES-ATT-MLN
    
(defun %add-values-att-MLN (obj-id tp-id value-list old-data-list context
                                         &rest key-arg-list &key language
                                         &allow-other-keys)
  "adding an attribute value when some value is an MLN. The function tries to ~
   create two MLNs before calling %add-values-att-all-MLN.
Arguments:
   see %add-values-att 
   old-data-list
Return:
   the internal representation of the modified object"
  ;; cheap test: nothing to add
  (unless value-list
    (return-from %add-values-att-MLN (symbol-value obj-id)))
  
  (flet ((mln? (xx) (or (mln::%mln? xx)(mln::mln? xx)))
         (mln-error (xx yy zz uu)
                    (throw :error 
                      (format nil "Can't add value ~S to ~S for attribute ~S of ~
                                   object ~S in context ~S"
                        xx yy zz uu))))
    (let ((lan (or language *language*))
          val old-val)
      (cond
       ;;=== all MLN?
       ((every #'mln? (append value-list old-data-list))
        (apply #'%add-values-att-all-MLN obj-id tp-id value-list old-data-list
               context key-arg-list))
       
       ;;=== only value-list (value to add) contains an MLN, old value was not
       ((mln? (car value-list))
        ;; if old value was not nil, send warning (should not be nil anyway)
        (when old-data-list
          (warn "We are making value of attribute ~S of object ~S in package ~S ~
                 and context ~S an MLN by adding ~S. It was previously ~S"
            tp-id obj-id *package* context value-list old-data-list))
        ;; check language option
        (unless lan
          (mln-error value-list old-data-list tp-id obj-id context))
        (setq old-val 
              (if (cdr old-data-list)
                  (mln::make-mln old-data-list :language lan)
                (mln::make-mln (car old-data-list) :language lan)))
        
        (apply #'%add-values-att-all-MLN obj-id tp-id value-list (list old-val)
               context key-arg-list))
       
       ;;=== value-list (value to add) is not MLN,  old-data is MLN
       (t
        ;; check language option, if no language, then error
        (unless lan
          (mln-error value-list old-data-list tp-id obj-id context))
        
        ;; must norm new data before trying to make it into an MLN
        (setq value-list 
              (loop for xx in value-list 
                    if (stringp (-> tp-id '=xi xx)) collect xx 
                    else do
                   (warn "Value ~S should be a string when trying to make an MLN ~
                          for attribute ~S of object ~S in package ~S and context ~
                          ~S. We ignore it."
                     xx tp-id obj-id *package* context)))
        ;(format t "~%; %add-values-att-MLN /value-list: ~S" value-list)
        
        ;; if nothing left return
        (unless value-list
          (return-from %add-values-att-MLN (symbol-value obj-id)))
                   
        (setq val
              (if (cdr value-list)
                  ;; e.g. ("albert") or "albert" or "albert; j�r�me")
                  (mln::make-mln value-list :language lan)
                (mln::make-mln (car value-list) :language lan)))
        ;(format t "~%; %add-values-att-MLN /*language*: ~S " *language*)
        (apply #'%add-values-att-all-MLN obj-id tp-id (list val) old-data-list
               context key-arg-list))))))
  
  
;;;=============================================================================
;;;                          ADD RELATION SUCCESSORS
;;;=============================================================================
  
;;;------------------------------------------------------------- %ADD-VALUES-REL

(defun %add-values-rel (obj-id sp-id suc-list 
                               &key before-value before-nth context no-warning)
  "Add a list of new successors. Duplicates are discarded. ~ 
   Successors are added at the end of the current list unless there is a
   :before-value or before-nth option. Each successor type ~
   is checked and must be allowed by the property. The constraint implemented ~
   by means of the =filter method is checked and if it fails the corresponding ~
   successor is not added. If added the =if-added method is fired presumably ~
   for bookkeeping Cardinality constraints are checked for minimal and maximal ~
   value. 
Arguments:.
   object-id: first object id
   sp-ref: reference of property: id, name or string
   suc-list: list of successors, in any of the legal relation formats
   before successor-id (key): to insert list in front of the specific successor.
   before-nth nth (key): to insert the values starting ot the nth position.
   context (key): context
   no-warning (key): t/nil) if t does not pring warning messages
Returns:
   the modified internal object representation."
  (let ((context (or context (symbol-value (intern "*CONTEXT*"))))
        message-list new-successor-list header old-data-list version)
    
    ;; for printing message list
    (setq header 
          (format nil "Warning: when adding successors ~{~S~^ ~} to object ~S for ~
             relation ~S in package ~S and context ~S:"
            (if (listp suc-list) suc-list (list suc-list))
            obj-id sp-id (package-name *package*) context))
    
    ;;=== check first if we want to add a null value
    (when (eql suc-list :none)
      ;; check whether there are non null values in this context
      (multiple-value-setq (old-data-list version)
        (send obj-id '=get-id sp-id))
      (when (and old-data-list (eql context version))
        (push 
         (format nil "while trying to set a null value to object ~S for relation ~
               ~S, the list of existing neighbors is non empty: ~S." 
           obj-id sp-id old-data-list)
         message-list)        
        (unless no-warning
          (mformat "~{~%~S~}" message-list))
        (return-from %add-values-rel (values (symbol-value obj-id) message-list))
        )
      
      ;; set value to nil shodowing any values from previous context
      (%%set-value obj-id nil sp-id context)
      (return-from %add-values-rel (values (symbol-value obj-id) message-list))
      )
    
    ;; first resolve relation formats of values, the result is a set of ids
    (multiple-value-setq (suc-list message-list)
      (%add-values-rel-resolve-formats sp-id suc-list context :obj-id obj-id))
    
    (when suc-list  
      ;; if arg is empty, then quit right away
      ;; in case of editing (checks if we are editing. If so, checks if the object 
      ;; is a new one. If not, put the pair (id . value) onto the old-oject-values
      ;; list, if not there)
      (save-old-value obj-id)
      
      (let* (;; single value allowed, but it is a bad idea
             (successor-list (delete-duplicates 
                              (if (listp suc-list) suc-list (list suc-list))))
             (old-successor-list (%get-value obj-id sp-id context))
             answer msg-list target-class
             suc-list-to-add)
        
        ;; process each successor in turn
        (dolist (suc-id successor-list)
          
          ;; cheap test: if already linked we skip it
          (if (member suc-id old-successor-list)(go skip))
          
          ;;--- check that successors have the proper format
          ;; %pdm? is not version sensitive
          (unless (%pdm? suc-id)
            (push
             (format nil "object ~A is not a PDM object. We skip it." suc-id)
             message-list)
            (go skip))
          
          ;; check if successor is alive in current context
          (unless (%alive? suc-id context)
            (push
             (format nil "object ~A is unknown or dead in context ~S. We skip it."
               suc-id context)
             message-list)
            (go skip))
          
          ;; test that the value to add has a type compatible with the class
          ;; specified in $SUC
          (setq target-class (car (%get-value sp-id '$SUC context)))
          (unless 
              (%subtype? (car (%type-of suc-id context)) target-class)
            (push
             (format nil "object ~A has not the right type ~S. We skip it." 
               suc-id target-class obj-id)
             message-list)
            (go skip))
          
          ;;--- check for integrity constraints, returning data if OK, 
          ;; nil otherwise
          (setq answer (send sp-id '=filter suc-id obj-id))
          (unless answer (go skip))
          
          ;; save suc-id to add it later
          (push suc-id suc-list-to-add)
          
          SKIP  ; for skiping a successor in case of error
          ) ; end of dolist
        
        (setq suc-list-to-add (reverse suc-list-to-add))
        
        ;; check each value for $ONE-OF restriction
        (multiple-value-setq (suc-list-to-add msg-list)
          (%validate-sp sp-id suc-list-to-add :restrictions `($ONEOF) 
                        :obj-id obj-id :context context :no-warning t))
        (setq message-list (append (reverse message-list) msg-list))
        
        (cond
         (before-nth
          (setq new-successor-list
                (insert-list-nth suc-list-to-add old-successor-list before-nth)))
         (before-value
          (setq new-successor-list
                (insert-list-before suc-list-to-add old-successor-list before-value)))
         (t
          (setq new-successor-list
                (append old-successor-list suc-list-to-add)))
         )
        
        ;; OK test restrictions on the set of all values
        ;; here :min :max won't modify old-successor-list
        (multiple-value-setq (new-successor-list msg-list)
          (%validate-sp sp-id new-successor-list
                        :restrictions `($MAXT $MINT)
                        :context context :obj-id obj-id :no-warning t))
        (setq message-list (append message-list msg-list))
        
        ;; add the updated list to the relation
        (%%set-value-list obj-id new-successor-list sp-id context)
        
        ;; for each new value added (restrictions may have eliminated some of
        ;; the links to add)
        (dolist (item (list-difference new-successor-list old-successor-list))
          ;; %add-value saves the value of successor in case of editing
          (%add-value item (%inverse-property-id sp-id) obj-id context)
          
          ;; do now any final bookkeeping 
          (send sp-id '=if-added obj-id item)
          ;; =if-added does something when the value is added, thus cannot result
          ;; in not adding the value
          )
        ) ; end of let*
      ) ; end of when
    
    ;; print warnings unless not wanted (here because resolving may have produced
    ;; warnings)
    (unless no-warning
      (if message-list
          (mformat "~{~%~A~}" (cons header message-list))))
    ;; return the object list and the list of warnings
    (values
     (symbol-value obj-id)
     message-list)
    ))

#|
Tests done on add-values in z-moss-tests-add-values.lisp file
|#
;;;--------------------------------------------- %add-values-rel-resolve-formats
;;; an interesting question is that values should be or not in the application
;;; package? However, one checks if they are an instance of the class successor
;;; of the relation, which should eliminate unwanted objects

(defun %add-values-rel-resolve-formats (sp-id value-list context 
                                              &key obj-id class-id)
  "takes a list of possible formats for successors and tries to obtain the ids ~
   of the designated objects. In case of error on some values return a warning ~
   message. In case of execution in deferred mode, builds a list of commands ~
   for unresolved cases. However, the function is not intended for this mode.
   Formats are:
     - the id of a PDM object
       - a list starting with :new, e.g. (:new \"city\" (\"name\" \"Paris\"))
       - a name of a variable, e.g. _albert
       - a string, e.g. \"Paris\"
       - a MOSS query
Arguments:
   sp-id: relation
   value-list: original list of values
   context: context in which all objects have to exist.
   obj-id (key): refers to the instance to which we add values
   class-id (key): id of the class of the instance
Return:
   1. a list of object ids or nil 
   2. a list of warning messages."
  (declare (special *allow-forward-instance-references* *deferred-instance-creations*))
  (let (suc-list access-list message-list new-suc-id)
    (dolist (value value-list)
      
      ;(format *debug-io* "~&+++ %add-values-rel-resolve-formats / value: ~S ~
      ;                   in package: ~S" value (package-name *package*))
      
      ;; value is a list starting with :new, e.g. (:new "address" ("street" "..."))
      (cond
       
       ;;=== first case we have a :new option
       ((and (listp value)(eql (car value) :new))
        ;; we must create the corresponding instance (make-instance "address" ...)
        ;; if it does not work then we get a string
        ;; if it works then we get an object identifier that we can use as a value
        (setq new-suc-id 
              (catch :error
                     (apply #'%make-instance (cdr value))))
        ;(format *debug-io* "~&+++ MOSS:%make-instance-sp-option value: ~S" value)
        ;; if result is a string then it is an error message
        (if (stringp new-suc-id) (push new-suc-id message-list)
          (push new-suc-id suc-list))
        )
      
       ;;=== second case we have a variable name, the variable is unbound and
       ;; we allow forward references (for batch files)
       ((and (%is-variable-name? value)
             (not (boundp value))
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-link ',obj-id ',sp-id ,value ',class-id)))
              *deferred-instance-creations*))
       
       ;;=== third case, no forward references allowed, skip the value
       ;; send a warning
       ((and (%is-variable-name? value)
             (not (boundp value)))
        ;; then we skip the link and send warning
        (push
         (format nil "unbound reference ~S for property ~S. We skip the successor." 
           value sp-id)
         message-list)
        )
       
       ;;=== fourth case, variable name, boundp to some object
       ;; if the value of value is not a PDM object, will be caught by %validate-sp
       ((%is-variable-name? value)
        (push (symbol-value value) suc-list)
        )
             
       ;;=== fifth case we have a string (SOL) representing an object name
       ;; should not happen (done at the %make-instance level)
       ((and (stringp value) 
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-link ',obj-id ',sp-id ,value ',class-id)))
              *deferred-instance-creations*)
        )
       
       ;;=== sixth case we have a string, no deferred linking, but $IS-A prop
       ((and (stringp value)(eql sp-id '$IS-A))
        ;; we cannot filter on the successor class of $IS-A since it is $ENT
        ;; for prototyping successor may be any object
        ;; we may have several classes in the $IS-A clause
        (setq suc-list (append (reverse (access value)) suc-list))
        )
       
       ;;=== seventh case, we have a symbol not entry point
       ;; meaning we cannot have entry points as successors
       ((and (%pdm? value)
             (not (%is-entry? value)))
        (push value suc-list))
       
       ;;=== eighth case, try the possibility that value is a query
       ;; includes the case where value is a string or an entry point symbol
       ((and (not (numberp value))
             (parse-user-query value)
             (setq access-list (access value)))
        ;(format *debug-io* "~&+++ MOSS:%make-instance-sp-option value: ~S result: ~S"
        ;  value (access value))
        ;; values not instances of $SUC classes will be removed by %validate-sp
        (setq suc-list (append (reverse access-list) suc-list))
        )
       
       ;;=== any other case is invalid
       (t 
        (push (format nil "Could not find successor corresponding to ~S" value)
              message-list)
        )
       )
      
      ;(format t "~%; %add-values-rel-resolve-formats /suc-list: ~%  ~S" suc-list)
      )
    ;; return list of successors to be added
    (values (reverse suc-list) message-list)))

#|
for tests see file z-moss-add-values.lisp
|# 
;;;=============================================================================
;;;                          END add attribute/relation
;;;=============================================================================

;;;A------------------------------------------------------------------- %%ALIVE?
;;; used in boot mode...
;;; uses %alive? and if the result is nil, throws to :error

(defUn %%alive? (obj-id context)
  "Checks if object is PDM, and object is alive. Same tests as in %alive? but ~
   takes a specific context argument and throws to :error if context is illegal ~
   or object is not alive.
Arguments:
   obj-id: id
   context (opt): context
Return:
   resolved id, if OK. 
   Throws to :error otherwise (illegal context or dead object)."
  (or (%alive? obj-id context)
      ;; if not, then prints a message using mformat (*moss-output* must not be 
      ;; nil, allowing to print into :moss-window), then throws to :error
      (mthrow "reference to ~S which does not exist in context ~S" obj-id context)
      ))

#|
(build-versions)
((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0))
(%%alive? '$ENT 0)
$ENT

(%%alive? '$EPS 6) ; OK in boot-mode
$EPS

(catch :error (%%alive? '$ENT 7))
;*** MOSS-error context 7 is illegal.
NIL
|#
;;;A-------------------------------------------------------------------- %ALIVE?
;;; %alive is a very primitive function used by many other ones. It depends on
;;; %PDM?, %allowed-context? and %%get-value
;;; What does it mean for an object to exist and be alive in a given context?
;;; - the object is alive in a given context if it does not have a tombstone in
;;;   one of the higher contexts
;;; - and it has been created in one of the higher contexts
;;; Thus, it can be checked by looking for a tombstone

(defUn %alive? (obj-id context)
  "Checks if object exists in given context. Each object has a tombstone which ~
   records if it has been killed by somebody in the given context. 
   If context is illegal, throws to :error.
   If the object is not in core and database is opened $pdm? brings it in.
Arguments:
   obj-id: identifier of the object to be checked
   context (opt): specifict context
Return:
   nil if object is dead, obj-id otherwise
Side-effect:
   if there is an opened database and obj-id is nil or unbound but represents ~
   an object in the database, then the object is loaded."
  (declare (special *boot-mode*))
  ;; if we are in boot mode most objects do not exist yet
  ;; not sure this is still meaningfull...
  (or
   (and *boot-mode* obj-id)
     ;(print context)
     (and
      ;; %pdm? loads object if needed, regardless of context
      (%pdm? obj-id)
      (or
       ;; adding test for references JPB1801
       (and (assoc '$REF (symbol-value obj-id))
            (%alive? (car (%%get-value obj-id '$REF context)) context))
      ;; allowed context is checked in %%get-value, if illegal throws to :error
      (and (not (%%get-value obj-id '$TMBT context))
           (%%get-value obj-id '$TYPE context)
           obj-id)
       )
      )))

#|
(catch :error (build-versions))
*version-graph*
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3

(with-package :test
  (%alive? '$E-person 0))
$E-PERSON

(with-package :test
  (boundp 'test::$E-PERSON.2))
NIL

(with-package :test
  (%alive? 'test::$E-person.2 0))
$E-PERSON.2

$e-person.2
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.2)) ($T-PERSON-NAME (0 "Zoe")))

(with-package :test
  (with-context 3
    (catch :error (%alive? 'test::$E-person 3))))
MOSS::$E-PERSON

(%alive? *moss-system* 3)
$SYS.1

;;; disc tests  can be done when the TEST agent is loaded

(db-load '$e-person.2 :test)
(($TYPE (1 $E-PERSON)) ($ID (1 $E-PERSON.1)) ($T-PERSON-NAME (1 "Zoe")))

(makunbound 'test::$E-person.2)
$E-PERSON.2

*context*
0

(%alive? '$E-PERSON.2 0)
NIL

(with-package :test
  (with-context 1
    (%alive? 'test::$E-PERSON.2 1)))
TEST::$E-PERSON.2

;; with-context is not needed here
(with-package :test
  (%alive? 'test::$e-person.2 1))
TEST::$E-PERSON.2
|#
;;;A--------------------------------------------------------- %%ALLOWED-CONTEXT?

(defUn %%allowed-context? (context) 
  "Checks if the context is allowed. Does this by looking at the global
   variable *version-graph*. If not allowed throws to :error."
  (or (assoc context (symbol-value (intern "*VERSION-GRAPH*")))
      (mthrow "~&;*** MOSS-error context ~A is illegal in package: ~S."
              context *package*)))

#|
(catch :error (%%allowed-context? 0))
(0)

(catch :error (%%allowed-context? 3))
(3 2)

(catch :error (%%allowed-context? 12))
;*** MOSS-error context 12 is illegal in package: #<The MOSS package>.
NIL

(with-package :test
  (catch :error (%%allowed-context? 2)))
(2 1)

(with-package :test
  (catch :error (%%allowed-context? 22)))
;*** MOSS-error context 22 is illegal in package: #<The TEST package>.
NIL
|#
;;;----------------------------------------------------------------- %ATTRIBUTE?
;;; deprecated, see %is-attribute?

;;;A-------------------------------------------------------------- %BUILD-METHOD
;;; this function does not depend on versions

(defUn %build-method (function-name arg-list body)
  "builds a MOSS method from its definition in the various defxxxmethof macros. ~
   Body may have a comment (string) and/or a declare statement. The method ~
   body is enclosed in a catch :return clause to allow exits, since we cannot ~
   use the return-from clause inside the method.
   When the compiler is included in the environment, compiles the code.
   If the body of the method returns multiple value, applying the method does so.
Arguments:
   function-name: the name of the function implementing the method 
                  (e.g. $UNI=I=0=SUMMARY)
   arg-list: method arguments
   body: the body of the function
Return:
   the function name."
  (declare (special *features*))
  (let* ((comments (if (stringp (car body)) (list (pop body))))
         (declaration 
          (if (and (listp (car body)) (eql (caar body) 'declare))
              (list (pop body))))
         lambda-expr)
    ;; build the lambda expr
    (setq lambda-expr `(lambda ,arg-list ,@comments ,@declaration
                         (catch :return ,@body)))
    (set function-name lambda-expr)
    ;; when editing, save it
    (save-new-id function-name)
    ;; ACL built images do not include the compiler
    (when (member :compiler *features*)
      (compile function-name lambda-expr))
    function-name))

#|
(moss::%build-method 'fff '(a b &rest ll) '("test function" (list a b ll)))
FFF
(LAMBDA (A B &REST LL) "test function" (CATCH :RETURN (LIST A B LL)))

(moss::%build-method 'ggg '(a b &rest ll) 
                     '("test function" 
                       (declare (ignore ll))
                       (throw :return (list a b))
                       a))
GGG
(LAMBDA (A B &REST LL)
  "test function"
  (DECLARE (IGNORE LL))
  (CATCH :RETURN (THROW :RETURN (LIST A B)) A))

(moss::%build-method 'fff '(a b &rest ll) '((declare (ignore ll))
                                              (throw :return (list a b))
                                              a))
MOSS::FFF
(LAMBDA (MOSS::A MOSS::B &REST MOSS::LL)
  (DECLARE (IGNORE MOSS::LL))
  (CATCH :RETURN (THROW :RETURN (LIST MOSS::A MOSS::B)) MOSS::A))

(moss::%build-method 'fff '(a b &rest ll) '((print ll)
                                              (throw :return (list a b))
                                              a))
MOSS::FFF
(LAMBDA (MOSS::A MOSS::B &REST MOSS::LL)
  (CATCH :RETURN (PRINT MOSS::LL) (THROW :RETURN (LIST MOSS::A MOSS::B)) MOSS::A))

(%build-method 'hhh '(a b) '((floor a b)))
hhh
(LAMBDA (A B) (CATCH :RETURN (FLOOR A B)))
(apply hhh 10 3 nil)
3
1

(%build-method 'hhh '(a b) '((values a b)))
hhh
(LAMBDA (A B) (CATCH :RETURN (VALUES A B)))
(apply hhh 10 3 nil)
10 
3
|#
;;;A----------------------------------------------------------- %CLEAN-WORD-LIST
;;; used by agents for cleaning list of empty words
;;; does not depend on versions

(defUn %clean-word-list (word-list &rest empty-word-lists)
  "remove empty words from the list using empty-word lists.
Arguments:
   word-list: list of words to be cleaned
   empty-word-lists (rest): lists of empty words
Returns:
   the cleaned list."
  (when word-list
    (if empty-word-lists
        ;; collect only words that do not belong to the different lists
        (reduce ; JPB 140820 removing mapcan
         #'append
         (mapcar #'(lambda (xx) 
                     (if 
                         ;; test if one word of word list belongs to some empty
                         ;; word list by returning something non null
                         (reduce  ; JPB 140820 removing mapcan
                          #'append
                          (mapcar #'(lambda (yy) 
                                      (if (member xx yy :test #'string-equal)
                                          (list t)))
                            empty-word-lists))
                         nil
                       (list xx)))
           word-list))
      ;; if no list of empty words return original word list
      word-list)))

#|
(%clean-word-list '("from" "Mr" " barthes"))
("from" "Mr" " barthes")

(%clean-word-list '("adresse" "de" "Mr" " barthes") sa-address::*empty-words*)
("adresse" "Mr" " barthes")

(%clean-word-list '("adresse" "de" "Mr" " barthes") sa-address::*empty-words* 
                    sa-address::*address-words*)
("Mr" " barthes")
|#
;;;A----------------------------------------------------------- CLEAR-ALL-FACTS

(defUn clear-all-facts (conversation)
  "clears the FACT base of the conversation object in the local current context.
Arguments:
   conversation: a MOSS-CONVERSATION object
Return:
   :done"
  (unless (%type? conversation '$CVSE)
    (terror "move-fact/ conversation arg not the right type: ~S" conversation))
  (%%set-value conversation '$FCT nil (symbol-value (intern "*CONTEXT*")))
  :done)

;;;-------------------------------------------------------------- COMMIT-EDITING
;;; Not clear whether the Editing box depends on the context

(defUn commit-editing ()
  "when the system is in editing mode, validate the changes, and if a database is ~
   opened, saves the new values onto disk. Updates the plists."
  (declare (special *database-pathname*))
  (let ((editing-box-var (intern "*EDITING-BOX*" *package*))
        (area-key (intern (package-name *package*) :keyword))
        box transaction)
    (unless (boundp editing-box-var)(return-from commit-editing nil))
    
    (setq box (symbol-value editing-box-var))
    ;; are we editing
    (when (and (typep box 'EDITING-BOX)
               (active box))
      ;;=== first handle the case when we have no database
      (unless *database-pathname*
        ;; we update plists
        (format t "~%; commit-editing /no database, box ~S" box)
        (mapc #'(lambda (xx) (remprop xx :new))
          (new-object-ids box))
        (mapc #'(lambda (xx) (remprop (car xx) :saved))
          (old-object-values box))
        )
      
      ;;=== here we have an opened database
      (when *database-pathname*
        (setq transaction (db-start-transaction))
        ;; we must save the new objects and update the old ones
        (format t "~%; commit-editing /starting saving stuff from ~S" box)
        ;; we will commit at the end
        (mapc #'(lambda (xx) (remprop xx :new)
                  (db-store xx (symbol-value xx) area-key :no-commit t))
          (new-object-ids box))
        (mapc #'(lambda (xx) (remprop (car xx) :saved)
                  (db-store (car xx) (symbol-value (car xx)) area-key :no-commit t))
          (old-object-values box))
        ;; must commit now
        (db-commit-transaction transaction)
        )
      
      ;;=== update the box
      ;; kill both lists
      (setf (new-object-ids box) nil)
      (setf (old-object-values box) nil)
      ;; disable box
      (setf (active box) nil)
      ;; remove owner
      (setf (owner box) nil)
      )
    ;;return
    :done))

;;;A--------------------------------------------------- %CREATE-BASIC-NEW-OBJECT

(defUn %create-basic-new-object (object-id context &key id ideal)
  "Creates a skeleton of object containing only the $TYPE and $ID ~
   properties in the current context.
Arguments:
   object-id: presumably class of the object to be created
   id (key): if there, id is specified (we do not create it)
   context: context in which to create new object
   ideal (key): if t, we want to create an ideal instance 
                     (with sequence number 0)
Return:
   id of the newly created object."
  (let ()
    ;(format t "~%;---------- %create-basic-new-object")
    ;(format t "~%;--- (sv (intern...)): ~S" (symbol-value (intern "*CONTEXT*")))
    ;(format t "~%;--- context: ~S" context)
    (setq object-id (%%get-id object-id :class))
    ;(format t "~%;---2 (sv (intern...)): ~S" (symbol-value (intern "*CONTEXT*")))
    (if object-id
        (let (value)
          ;; set value to 0 or to counter value
          (setq value 
                (if ideal 
                    0
                  ;; otherwise we must get counter value from class
                  ;; counters are in context 0
                  (%get-and-increment-counter object-id)))
          ;(format t "~%;---3 (sv (intern...)): ~S" (symbol-value (intern "*CONTEXT*")))
          ;; create now instance key, in a specific context and a specific package
          (setq id  (or id (%make-id-for-instance object-id value)))
          ;(format t "~%;---4 (sv (intern...)): ~S" (symbol-value (intern "*CONTEXT*")))
          ;; when editing save id (counter saved in %get-and-increment-counter)
          (save-new-id id)

          (set id `(($TYPE (,context ,object-id))($ID (,context ,id))))
          )
      ;; if not a model, this is an error
      (mthrow "when creating a new basic object ~
             the reference object ~A is not a concept."
              object-id))
    ;(format t "~%;--- (sv (intern...)): ~S" (symbol-value (intern "*CONTEXT*")))
    id))

#|
(defconcept "person" (:att "name" (entry))(:att "first-name"))
$E-PERSON

(catch :error (%create-basic-new-object '$E-person 0))
$E-PERSON.2
(($TYPE (0 $E-PERSON)) ($ID (0 $E-PERSON.2)))

(%create-basic-new-object '$E-person 0 :ideal t)
$E-PERSON.0

(catch :error (%create-basic-new-object 0 '$Z :ideal t))
; Warning: when creating a new basic object the reference object $Z is not a model (class).
; While executing: %CREATE-BASIC-NEW-OBJECT

(catch :error (%create-basic-new-object 'TEST::$E-PERSON 0 :ideal t ))
TEST::$E-PERSON.0

(%create-basic-new-object '$CTR 0 :id '$ORPH.CTR)
$ORPH.CTR

;;=== check versions
test::*version-graph*
((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0))

(with-context 6
  (catch :error (%create-basic-new-object '$E-person 6)))
$E-PERSON.3
(($TYPE (6 $E-PERSON)) ($ID (6 $E-PERSON.3)))
|#
;;;A------------------------------------------------------- %CREATE-BASIC-ORPHAN
;;; should maybe create the id in the MOSS package ?
;;; this is equivalent to: (%create-basic-new-object '*none* context) 

(defUn %create-basic-orphan (context)
  "Creates a skeleton for an orphan object and the associated key.
Arguments:
   context (key): context (default current)
Return:
   the id of the new orphan."
  (let ((id (%make-id-for-orphan 
             (%get-and-increment-counter (intern "*NONE*") '$CTRS))))
    (set id `(($type (,context *none*))($ID (,context ,id))))
    ;; when editing
    (save-new-id id)
    id))

#|
(%create-basic-orphan 0 )
$ORPHAN.1

(with-package :test
  (with-context 4
    (%create-basic-orphan 4)))
TEST::$ORPHAN.1
((MOSS::$TYPE (4 MOSS::*NONE*)) (MOSS::$ID (4 TEST::$ORPHAN.1)))

;; here we do not need "with-context"
(with-package :test
  (%create-basic-orphan 6))
TEST::$ORPHAN.2
((MOSS::$TYPE (6 MOSS::*NONE*)) (MOSS::$ID (6 $ORPHAN.2)))
|#
;;;A------------------------------------------------- %CREATE-ENTRY-POINT-METHOD
;;;********* record on *moss-system* ??

(defUn %create-entry-point-method (object-id object-name &optional args doc body)
  "Creates an entry point method, with default if no arguments. ~
   the =make-entry symbol is defined in the same package as object-id.
   If args is present it must contain an option &key (package *package*).
   Replace the previous method if there is one.
Arguments:
   object-id: id to which the =make-entry method will be attached
   object-name: name of the object (for error message)
   args (opt): arguments of the method (default is (value-list))
   doc (opt): documentation string
   body (opt): body of the method
Return:
   an own-method id."
  (if
      (or (and (null args)(null body))
          (and args body))
      ;; if args is non  nil, then so must be body 
      (let ((entry-args (or args `(value-list)))
            (entry-doc (if doc (format nil "~A" doc)
                         (format nil "Entry-point function for ~A" 
                           (if (mln::mln? object-name) ; jpb 1406
                               (mln::filter-language object-name *language*)
                             object-name))))
            (entry-body (or body `((%make-entry-symbols value-list)))))
        ;; a new entry in the specified package will be created unless the symbol
        ;; is imported from another package (e.g. MOSS)
        (%make-ownmethod (intern "=MAKE-ENTRY" (symbol-package object-id)) 
                         object-id entry-args (cons entry-doc entry-body)))
    ;; otherwise error
    (terror "bad definition for creating =make-entry method for attribute ~S"
            object-name)))

#|
;; in package moss
test::$T-NAME
(($TYPE (0 $EPT)) ($ID (0 TEST::$T-NAME)) ($PNAM (0 ((:EN "name")))) 
 ($ETLS.OF (0 TEST::$SYS.1)) ($INV (0 TEST::$T-NAME.OF)) 
 ($IS-A.OF (0 TEST::$T-PERSON-NAME)) ($OMS (0 TEST::$FN.6)))

(moss::%create-entry-point-method '$T-NAME '(:EN "NAME1"))
$FN.6

$FN.6
((MOSS::$TYPE (0 $FN)) (MOSS::$ID (0 $FN.6)) (MOSS::$MNAM (0 =MAKE-ENTRY))
 (MOSS::$FNLS.OF (0 $SYS.1)) (MOSS::$OMS.OF (0 $T-NAME)) 
 (MOSS::$FNAM (0 $T-NAME=S=0=MAKE-ENTRY))
 (MOSS::$DOCT (0 "Entry-point function for (EN NAME1)")))

$T-NAME=S=0=MAKE-ENTRY
(LAMBDA (MOSS::VALUE-LIST)
  "Entry-point function for (EN NAME1)"
  (CATCH :RETURN (MOSS::%MAKE-ENTRY-SYMBOLS MOSS::VALUE-LIST)))

(catch :error (moss::%create-entry-point-method '$T-NAME 'HAS-NAME '(val) ))
"bad definition for creating =make-entry method for attribute HAS-NAME"

;; does not create a new method but replaces the previous one if any
(catch :error 
       (moss::%create-entry-point-method 
        '$T-NAME 'HAS-NAME 
        '(val &key (package *package*)) 
        nil 
        '((intern (moss::%string-norm val) package))))
? $FN.6
((MOSS::$TYPE (0 $FN)) (MOSS::$ID (0 $FN.6)) (MOSS::$MNAM (0 =MAKE-ENTRY))
 (MOSS::$FNLS.OF (0 $SYS.1)) (MOSS::$OMS.OF (0 $T-NAME)) 
 (MOSS::$FNAM (0 $T-NAME=S=0=MAKE-ENTRY))
 (MOSS::$DOCT (0 "Entry-point function for HAS-NAME")))

$T-NAME=S=0=MAKE-ENTRY
(LAMBDA (VAL &KEY (PACKAGE *PACKAGE*))
  "Entry-point function for HAS-NAME"
  (CATCH :RETURN (INTERN (MOSS::%STRING-NORM VAL) PACKAGE)))

;;===== versions
TEST(46): (with-context 6 (moss::%create-entry-point-method '$E-PERSON.1 "Albert"))
$FN.10
TEST(47): 
((MOSS::$TYPE (6 $FN)) (MOSS::$ID (6 $FN.10)) (MOSS::$MNAM (6 =MAKE-ENTRY))
 (MOSS::$DOCT (6 "Entry-point function for Albert")) (MOSS::$FNLS.OF (6 $SYS.1))
 (MOSS::$OMS.OF (6 $E-PERSON.1)) (MOSS::$FNAM (6 $E-PERSON.1=S=6=MAKE-ENTRY)))
TEST(48): 
(LAMBDA (MOSS::VALUE-LIST)
  "Entry-point function for Albert"
  (CATCH :RETURN (MOSS::%MAKE-ENTRY-SYMBOLS MOSS::VALUE-LIST)))
|#
;;;--------------------------------------------- %CREATE-NEW-PACKAGE-ENVIRONMENT
;;; assuming we define an ADDRESS ontology in package :address
;;; we must create the following objects:
;;;   - address::$SYS, system ID
;;;   - a local system name ADDRESS-MOSS-SYSTEM (entry point)
;;;   - address::$SYS.1 to describe the local environment
;;;   - a local name for the instance: ADDRESS-MOSS
;;;   - address::$FN a local class proxy for methods
;;;   - address::$UNI a local class proxy for universal methods
;;;   - address::$CTR a local class proxy for counters
;;;   - address::*none* and address::*any* classes and counters (for *none*)
;;; some objects are put into the *moss-system* environment
;;;   - *moss-system* and *ontology* (synonym) with value address::$SYS.1
;;; the new environment is created in context 0.
;;; we assume that this function is not called when an agent is editing meaning
;;; that we do not save newly created objects nor changed objects... 
;;;********** might lead to some obscure errors, add check

(defUn %create-new-package-environment (package-key system-ref)
  "when a new application package is defined, we must create various objects like ~
   a version of *moss-system* a model for methods, etc. System language is English, ~
   the new environment is created in context 0. 
Argument:
   package-key: keyword specifying the new package, e.g. :address (can be package)
   system-ref: string for the new system name, e.g. \"Address\"
Return:
   id of the system instance (value of *ontology*)"
  (let* ((target-package 
          (or (find-package package-key)
              (make-package package-key :use '(:moss #+OMAS :omas :cl))))
         (sys1 (intern "$SYS.1" target-package)))
    ;(format t "~2%;---------- Entering %create-new-package-environment: ~S"
    ;  package-key)
    ;(format t "~%;--- *package*: ~S" *package*)
    ;(format t "~%;--- target-package: ~S" target-package)
    ;(format t "~%;--- sys1: ~S" sys1)
    ;(format t "~%;--- <target-package>::$SYS.1: ~S" 
    ;  (if (boundp sys1) (symbol-value sys1) "yet unbound"))
    ;(format t "~%;--- (moss-version-graph agent): ~S" (omas::moss-version-graph agent))
    (unless (stringp system-ref)
      (error "system-ref argument: ~S, should be a string" system-ref))
    
    ;(format t "~%;---package-key (package for creating data): ~S" package-key)
    ;(format t "~%;---system-ref (system name):~S" system-ref)
    
    ;; if $SYS.1 is bound, then the environment was already created
    ;; issue a warning and quit
    (when (boundp sys1)
      (warn "when trying to create new environment in package ~S is has already ~
             been done. $SYS.1 is ~S" target-package
        (symbol-value (intern "$SYS.1" target-package)))
      (return-from %create-new-package-environment :already-exists))
        
    ;; execute the following expressions in the agent package
    (with-package package-key
      (proclaim (list 'special (intern "*MOSS-SYSTEM*") (intern "*ONTOLOGY*")
                      (intern "*MOSS-CONTEXT*")(intern "*VERSION-GRAPH*")))
      (let ((*language* :EN)
            ;; create system-name from system-ref
            (local-moss-sys-name 
             (intern 
              (concatenate 'string (%string-norm (%make-string-from-ref system-ref))
                "-MOSS")))
            (context-symbol (intern "*CONTEXT*"))
            (context 0)
            sys-id)
        
        ;;***** kludge: we initialize local *context* to 0 and *version-graph* to ((0))
        (set context-symbol 0)
        (set (intern "*VERSION-GRAPH*") '((0)))
        
        ;(format t "~%;--- *package*: ~S" *package*)
        ;(format t "~%;--- *context*: ~S" *context*)
        ;(break "%create-new-package-environment 1 local-moss-sys-name: ~S" local-moss-sys-name)
        
        ;;===============================================================================
        ;;                    specific instance of MOSS SYSTEM
        ;;===============================================================================
        
        (proclaim (list 'special local-moss-sys-name))
        
        ;; local system entry point, e.g. ADDRESS-MOSS of :$SYS.1
        (set local-moss-sys-name 
             `((moss::$TYPE (0 moss::$EP))(moss::$ID (0 ,local-moss-sys-name))
               (moss::$SNAM.OF (0 ,(intern "$SYS.1")))(moss::$XNB (0 0))
               (moss::$IS-A (0 moss::$SYS))(moss::$EPLS.OF (0 ,(intern "$SYS.1")))))
        
        ;; Object representing the application system
        (set (intern "$SYS.1")
             `((moss::$TYPE (0 ,(intern "$SYS")))
               (moss::$ID (0 ,(intern "$SYS.1")))
               ;(moss::$SNAM (0 (:en ,(concatenate 'string system-ref " MOSS"))))
               (moss::$SNAM (0 ,(mln::make-mln (concatenate 'string system-ref " MOSS")
                                               :language :en)))
               (moss::$PRFX (0 ,local-moss-sys-name))
               (moss::$EPLS (0 ,local-moss-sys-name)) ; list of all entry-points
               (moss::$CRET (0 "MOSS"))
               (moss::$DTCT (0 ,(get-current-date)))
               (moss::$VERT (0 ,moss::*moss-version-number*))
               (moss::$XNB  (0 0))
               ))
        
        ;; record system description into global variables
        (set (intern "*MOSS-SYSTEM*") (intern "$SYS.1"))
        (set (intern "*ONTOLOGY*") (intern "$SYS.1"))
        
        ;; add *moss-system* (value $SYS.1) to list of variables
        (moss::%add-value (intern "$SYS.1") '$SVL (intern "*MOSS-SYSTEM*") context)
        (moss::%add-value (intern "$SYS.1") '$SVL (intern "*ONTOLOGY*") context)
        
        ;;===============================================================================
        ;;                    specific class for MOSS SYSTEM
        ;;===============================================================================
        ;; we create first the MOSS-SYSTEM local-class and the *moss-system*
        ;; local environment prior to creating the other objects
        ;; when creating LOCAL MOSS-SYSTEM class and instance, local *moss-system* is
        ;; not defined, so tell the system not to save it
        (setq sys-id
              (%make-concept 
               (concatenate 'string 
                 (%string-norm (%make-string-from-ref system-ref))
                 " MOSS SYSTEM")
               `(:id ,(intern "$SYS"))
               '(:is-a moss::$SYS)))
        ;; this creates $E-SYS, XXX-MOSS-SYSTEM ep, $E-SYS.CTR
        ;; increment counter since we already have an instance
        (%get-and-increment-counter sys-id)
        
        ;;===============================================================================
        ;;                    specific class for METHODS
        ;;===============================================================================
        (moss::%make-concept "METHOD" '(:is-a moss::$FN) `(:id ,(intern "$FN")))
        
        (moss::%make-concept "UNIVERSAL METHOD" '(:is-a moss::$UNI) 
                             `(:id ,(intern "$UNI")))
        
        ;;===============================================================================
        ;;                    specific class for COUNTERS
        ;;===============================================================================
        
        ;;allows to create counter for orphans
        (moss::%make-concept "COUNTER" '(:is-a moss::$CTR) `(:id ,(intern "$CTR")))
        
        ;;===============================================================================
        ;;                    specific classes *none* and *any*
        ;;===============================================================================
        
        ;; *none* is used to shelter orphans
        (moss::%make-concept "NULL-CLASS" '(:is-a moss::*none*)
                             `(:id ,(intern "*NONE*"))
                             `(:rdx ,(intern "$ORPHAN")))
        (moss::%make-concept "UNIVERSAL CLASS" 
                             '(:is-a moss::*any*)
                             `(:id ,(intern "*ANY*")))
        
        ;;===============================================================================
        ;;                    version graph, context, language
        ;;===============================================================================
        
        ;; version-graph, context and language are global exported variables. However,
        ;; when we create a new environment, they could be different. Anyway, context
        ;; currently is assumed to be 0. JPB1010
        ;; copy them from the current environment. They will be possibly modified later
        ;;**********
        ;; Normally MOSS functions work with moss::*context*, moss::*language*,...
        ;; the fonction creating the thread containing the new environment should
        ;; shadow the corresponding variables, i.e. include moss::*context*,... in
        ;; a let directive encompassing all processing done in the new thread.
        ;; The values of such variables should nevertheless be saved into the database
        ;; to be reinitialized later when reloading the environment (e.g. agent)
        ;; Currently *context* and *version-graph* are frozen to configuration 0.
        ;;**********
        
        (moss::%add-value (intern "$SYS.1") '$SVL (intern "*CONTEXT*") context)
        (set (intern "*LANGUAGE*") *language*)
        (moss::%add-value (intern "$SYS.1") '$SVL (intern "*LANGUAGE*") context)
        ;(break "%create-new-package-environment 2/ *version-graph*: ~S" *version-graph*)
        (set (intern "*VERSION-GRAPH*") *version-graph*)
        (moss::%add-value (intern "$SYS.1") '$SVL (intern "*VERSION-GRAPH*") context)
        
        :done))))
#|
(defUn cnpe (&rest ll) (apply #'%create-new-package-environment ll))

? (%create-new-package-environment :address "Address")
:DONE
;(in-package :address)
(defconcept (:en "person" :fr "personne; individu")
    (:att (:en "name" :fr "nom")(:entry))
  (:att (:en "first name; given name" :fr "pr?nom"))
  (:rel (:en "brother" :fr "fr�re")(:to "person")))
(defconcept "teacher" (:is-a "person")
  (:rel (:en "father" :fr "p�re")(:to "person")))
(defconcept "student" (:is-a "person")
  (:rel (:en "father" :fr "p�re")(:to "person")))
|#
;;;A----------------------------------------------------- %DETERMINE-PROPERTY-ID
;;; to check...

;;; a question is : can objects have properties defined in other packages?
;;; answer: yes, this is the case for application objects with properties
;;; defined in the MOSS package

(defUn %determine-property-id (obj-id prop-name &optional version)
  "For the object with id obj-id, attempts to determine which of its ~
   properties corresponds to prop-name in the specified context.
   If the property is present at the object level, then return it, otherwise:
      - if the object has a prototype, try the prototype
      - if the object has a class, look at the class level for properties ~
   defined at the class level.
      - if the object belongs to several classes try each one in turn.
Problems are: 
   - tree properties: properties are in a tree (normally each node of the ~
   tree should have the same property name)
   - lattices of prototypes, where a property could be derived from another ~
   one thru an IS-A relation (not implemented).
Arguments:
   obj-id: identifier of the object
   prop-name: name of property to check: e.g. HAS-NAME (must be valid, no check)
   context (opt): context, default is local context
Returns:
   a list of normally a single property-id."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (setq obj-id (%%alive? obj-id context))
    ;; reset prop-name to the package of obj-id
    (setq prop-name (%%set-symbol-package prop-name (symbol-package obj-id)))
    ;(format t "~%;%determine-property-id /prop-name: ~S" prop-name)
    
    (let ((prop-list    ; list of all direct or inverse properties
           (append 
            (%get-value prop-name (%inverse-property-id '$PNAM) context)
            (%get-value prop-name (%inverse-property-id '$INAM) context))) 
          ancestors prop)
      ;(format t "~%;%determine-property-id /prop-list: ~S" prop-list)
      ;(format t "~%;%determine-property-id /obj-id: ~S" obj-id)
      ;(format t "~%;%determine-property-id /obj-id: ~S" context)
      ;(format t "~%;%determine-property-id /local properties: ~S" 
      ;  (%%has-properties obj-id context))
      (cond
       ;; when prop-list is nil, then prop-name is NOT a property name
       ((null prop-list) nil)
       ;; check first local properties
       ((%intersect-symbol-lists prop-list (%%has-properties obj-id context)))
       ;; otherwise check if the object has an ancestor
       ((and (setq ancestors (%%has-value obj-id '$is-a context)) nil))
       ;; if no ancestor and object is an orphan, that's it
       ((and (null ancestors) (%is-orphan? obj-id)) nil)
       ;; if no ancestor and not orphan try properties from class
       ((null ancestors)
        (%intersect-symbol-lists prop-list (%get-properties obj-id)))
       ;; if ancestor and orphan, check ancestor
       ((%is-orphan? obj-id)
        (dolist (ancestor ancestors)
          (setq prop (%determine-property-id ancestor prop-name context))
          (if prop (return prop))))
       ;; otherwise, if ancestor and not orphan, check class properties
       ((%intersect-symbol-lists prop-list (%get-properties obj-id context)))
       ;; otherwise, if ancestor and not orphan and we could not find a class
       ;; property, check ancestor. So here, we check class properties before
       ;; folowing the is-a links
       (t (dolist (ancestor ancestors)
            (setq prop (%determine-property-id ancestor prop-name context))
            (if prop (return prop))))
       ))))

#|
;; in the test package
test::$E-PERSON.1
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1))
 ($T-PERSON-NAME (0 "Barth�s"))
 ($T-PERSON-FIRST-NAME (0 "Jean-Paul"))
 (MOSS::$OMS (6 $FN.10)))

(moss::%determine-property-id '$E-PERSON.1 'has-name 0)
($T-PERSON-NAME)

(with-package :test 
    (%determine-property-id 'test::$E-PERSON.1 'test::HAS-NAME))
(TEST::$T-PERSON-NAME)

$E-PERSON.4
((MOSS::$TYPE (2 $E-PERSON)) (MOSS::$ID (2 $E-PERSON.4)) 
 ($T-PERSON-NAME (2 "Barth�s"))
 ($T-PERSON-FIRST-NAME (2 "Camille")))

(catch :error
       (moss::%determine-property-id '$E-PERSON.4 'has-name 0))
reference to $E-PERSON.4 which does not exist in context 0

;;==== versions
$E-PERSON.4
((MOSS::$TYPE (2 $E-PERSON)) (MOSS::$ID (2 $E-PERSON.4)) 
 ($T-PERSON-NAME (2 "Barth�s")) ($T-PERSON-FIRST-NAME (2 "Camille")))

(catch :error
       (moss::%determine-property-id '$E-PERSON.4 'has-name 2))
reference to $E-PERSON.4 which does not exist in context 2

(catch :error
       (moss::%determine-property-id 'test::$E-PERSON.4 'test::has-name 2))
;*** MOSS-error context 2 is illegal in package: #<The MOSS package>.
NIL

(with-package :test
 (catch :error
       (moss::%determine-property-id 'test::$E-PERSON.4 'test::has-name 2)))
(TEST::$T-PERSON-NAME)

;; this property is not an application property and does not depend on the class
;; of the object. %%has-value does not return system properties
;; NB. The property can be accessed directly
(with-package :test
 (catch :error
        (moss::%determine-property-id 
         'test::$E-PERSON.4 'test::has-moss-identifier 2)))
NIL

;; id for type
(with-package :test
 (catch :error
        (moss::%determine-property-id 
         'test::$E-PERSON.4 'test::has-moss-type 2)))
NIL
|#
;;;=============================================================================
;;;                           DELETE FUNCTIONS
;;;============================================================================= 
;;; When deleting a value:
;;;  - if the values are non MLN and the value is not there, do nothing
;;;  - if the value is there, then remove it
;;;  - if the existing value is an MLN and the value is not, remonve from the
;;;    specified or default language
;;;  - if both values are MLN, subtract MLNs
;;;  - if the existing value is not MLN and the value is, use the dpecified or 
;;;    or default language to remove the value
;;; For each suppressed value run the =if-removed method
;;; Globally apply the %validate-xx function to the list of values that is left
;;; Return the modified object.

;;;--------------------------------------------------------------- DELETE-VALUES
;;; setting default-value of context to nil avoids transmitting it twice when
;;; calling further functions
;;; BUG: should treat the case when an inverse property is ambiguous and leads
;;; to a list of possible inverse pointing to the object, e.g. 
;;; ($S-COMPANY-PRESIDENT.OF $S-NPO-PRESIDENT.OF)
;;; one should delete values from all references, issuing a warning.

(defun delete-values (obj-id prop-ref value-list &rest key-list
                             &key (context nil) (no-warning nil) (scope :some)
                             &allow-other-keys)
  "API function deleting a list of values from a specific object associated to a ~
   specific property, the property may be an attribute or a relation, the object ~
   may be an instance or an orphan. Prints warning messages if no-warning is false.
Arguments:
   obj-id: concerned object
   prop-ref: prop-id or prop-name or string or relation formats
   value-list: values to delete: simple values or mln or list of ids or :ALL
   context (key): specified context if different from current (default NIL)
   nth (key): we want to remove the nth value
   no-warning (key): if t means that no message will be printed
   count (key): if a number counts the number of values to be deleted in case of
     multiple values (does not apply to MLNs)
   scope (key): for MLNs, :all delete the MLN, :some delete synonyms
   already-normed (key): t/nil, t if =xi has already been applied to values to
     delete for attributes or if successors have been resolved
   multiple values
Return:
   1. the internal list of the modified object.
   2. the list of warnings"
  (let ((context (or context (symbol-value (intern "*CONTEXT*"))))
        header prop-id obj-l message-list)
    
    ;; if context is illegal throw to :error
    (%%allowed-context? context)
    
    ;; check if object is alive in that context, if not throw to :error
    (%%alive? obj-id context)
    
    ;; we need that to compute header (kludge for the :all value)
    (if (eql value-list :all) (setq value-list (list :all)))
    (setq header ; must wait for a value of context
          (format nil "Warning: when trying to delete values (~{~S~^ ~}) ~
                  for property ~S of object ~S in package ~S and context ~S:"
            value-list prop-ref obj-id (package-name *package*) context))
    
    ;; quit if there are no values to delete
    (unless value-list
      (push
       (format nil "No value to delete to ~S for property ~S" obj-id prop-ref)
       message-list)
      (unless no-warning (mformat "~{~%~S~}" (cons header message-list)))
      (return-from delete-values (values (symbol-value obj-id) message-list)))
    
    ;; restore value-list to single :all value
    (if (eql (car value-list) :all) (setq value-list :all))
    
    ;;=== recover property id from prop-ref
    (cond
     ;; for orphans, use generic property
     ((%is-orphan? obj-id context)
      (setq prop-id (%%get-id prop-ref :prop :include-moss t)))
     ;; case of generic inverse properties
     ((%is-orphan? obj-id context)
      (setq prop-id (%%get-id prop-ref :inv :include-moss t)))
     ;; otherwise recover prop-id from prop-ref, including moss properties
     ((setq prop-id (%%get-id prop-ref :prop :class-ref 
                              (car (%type-of obj-id context))
                              :include-moss t)))
     ;; case of inverse properties
     ((%is-orphan? obj-id context)
      (setq prop-id (%%get-id prop-ref :inv :include-moss t)))
     ((setq prop-id (%%get-id prop-ref :inv :class-ref 
                              (car (%type-of obj-id context))
                              :include-moss t)))
     ;; if still nil, then complain and quit
     (t
      (push
       (format nil "Cannot find property ~S." prop-ref)
       message-list)
      (unless no-warning (mformat "~{~%~S~}" (cons header message-list)))
      (return-from delete-values (values (symbol-value obj-id) message-list)))
     )
    
    ;; we cannot normalize value-list here because of the case when we have MLNs
    ;; but value-list is a list of strings to be transformed into an MLN
       
    ;(format t "~%; delete-values /prop-id: ~S" prop-id)
    
    (cond
     ;;=== if attribute, call ad hoc function
     ((%is-attribute? prop-id context)
      (multiple-value-setq (obj-l message-list)
        (apply #'%delete-values-att obj-id prop-id value-list :context context 
               :no-warning t :scope scope key-list))
      )
     ;;=== if relation, call ad hoc function (will link successors)
     ((%is-relation? prop-id context)
      (multiple-value-setq (obj-l message-list)
        (%delete-values-rel 
         obj-id prop-id value-list :context context :no-warning t))
      )
     ;;=== if inverse-property, call ad hoc function
     ((%is-inverse-property? prop-id context)
      (multiple-value-setq (obj-l message-list)
        (%delete-values-inv 
         obj-id prop-id value-list :context context :no-warning t))
      )
     ;;=== not attribute nor relation...
     (t
      (push (format nil "Property cannot be found or is ambiguous: ~S." prop-id) 
            message-list)
      (setq obj-l (symbol-value obj-id)))
     )
    
    ;; here messages come from called functions
    (unless no-warning
      (if message-list
          (mformat "~{~%+++++ ~A~}" (cons header (reverse message-list)))))
    
    ;; return list of added values and a list of warning messages
    (values obj-l message-list)))

;;;---------------------------------------------------------- %DELETE-VALUES-ATT

(defun %delete-values-att (obj-id tp-id value-list &rest key-arg-list 
                                  &key context no-warning &allow-other-keys)
  "removing a set of values from an attribute of an object, id supposed to exist
   in the specific context.
Arguments:
   obj-id: id of the object
   tp-id: id of the attribute
   value-list: list of values to delete or :ALL
   context (key): current context
   count (key) : nb of values to remove
   allow-duplicates (key): allows duplicate values
   already-normed (key): if t, =xi was already applied ttoo value-list
   language (key): current language
   no-warning (key): t/nil, if t no warning issued (default nil)
Return:
   1. the internal representation of the modified objet
   2. the list of warning messages"    
  (let* ((context (or context (symbol-value (intern "*CONTEXT*"))))
         (old-data-list (%get-value obj-id tp-id context))
         message-list)
    
    ;; check first the :all case
    (when (eql value-list :all)
      (unless old-data-list
        ;; if we had no value, we simply mark it and return
        (send obj-id '=set-id tp-id nil)
        (return-from %delete-values-att (values (symbol-value obj-id) nil)))
      ;; we do that to actually remove values, activating =if-removed if necessary      
      (setq value-list old-data-list))
    
    ;; if we have no value to delete, then quit
    (unless value-list
      (push
       (format nil "No value to delete for object ~S and property ~S" obj-id tp-id)
       message-list)
      ; complain if allowed
      (unless no-warning (mformat "~{~%~S~}" message-list))
      ;; return standard object-list and message list
      (return-from %delete-values-att (values (symbol-value obj-id) message-list)))
    
    ;; normalize values to remove
    (setq value-list
          (remove nil (mapcar #'(lambda (xx)(-> tp-id '=xi xx)) value-list)))
    
    ;(format t "~%; %delete-values-att / normed value-list: ~S" value-list)
    ;; check if we have an MLN value somewhere, in which case we call a special
    ;; function (here %mln? is used for backward compatibility)
    ;(format t "~%; %add-values-att / <list>: ~S" (append value-list old-data-list))
    
    (when (or (eql (car (%get-value tp-id '$TPRT)) :mln)
              (some #'(lambda (xx) (or (mln::%mln? xx) (mln::mln? xx)))
                    ;; here we can have old-data-list twice, but it does not matter
                    (append value-list old-data-list)))
      (return-from %delete-values-att ; return 2 values
        (apply #'%delete-values-att-mln obj-id tp-id value-list old-data-list
               key-arg-list)))
    
    ;;=== here we have a list of non MLN values
    ;; if anything left call %delete-values-att-list to process values
    (when value-list
      (return-from %delete-values-att ; return 2 values
        (apply #'%delete-values-att-list obj-id tp-id value-list 
               :context context key-arg-list)))
    
    ;;=== otherwise issue a message
    (push
     (format nil "No value left to delete after applying the =xi method ~
                    for object ~S and property ~S" obj-id tp-id)
     message-list)
    
    ;;=== complain if allowed
    (unless no-warning (mformat "~{~%~S~}" message-list))
    
    (values (symbol-value obj-id) message-list)))
    
;;;----------------------------------------------------- %delete-VALUES-ATT-LIST

(defun %delete-values-att-list (obj-id tp-id value-list &key context count
                                       no-warning mln &allow-other-keys)
  "delete a list of NON-MLN values to an attribute that has non-MLN values. The ~
   object is not modified although entry points are updated.
Arguments:
   obj-id: current object
   tp-id: current attribute
   value-list: list of values to remove
   context (key): specified context
   already-normed (key): if t, =xi was already applied to value-list
   count (key): number of occurences of the same value that we remove
   no-warning (key): printing flag
   mln (key): if t means that we process synonyms of MLNs
Return:
   1. the list of remaining values so that it can be used for MLNs
   2. a list of warning messages."
      (let* ((context (or context (symbol-value (intern "*CONTEXT*"))))
             header obj-l val-list msg-list message-list)
        
        (%%allowed-context? context)
        
        ;;=== define header in case we want to print warning messages
        (setq header (format nil "Trying to erase values (~{~A~^ ~}) from object ~A ~
                              for attribute ~A in package ~S and context ~S."
                       value-list obj-id tp-id (package-name *package*) context))
        
        ;;=== delete each value in turn
        (dolist (value value-list)
          (multiple-value-setq (obj-l msg-list)
            (%delete-values-att-single obj-id tp-id value :context context 
                                       :count count :no-warning t :mln mln))
          (if msg-list (push msg-list message-list)))
        
        ;;=== check-validity
        (multiple-value-setq (val-list msg-list)
          (%validate-tp tp-id (%get-value obj-id tp-id) :obj-id obj-id 
                        :context context :no-warning t))
        
        (if msg-list (setq message-list (append msg-list message-list)))
        
        ;; what happens if the list of values has been changed?
        (when (set-difference (%get-value obj-id tp-id context) val-list)
          (push (format nil "Some values are not valid but no change has been done")
                message-list))
        
        ;;=== print warning if required
        (unless no-warning
          (mformat "~{~%~S~^ ~}" (cons header (reverse message-list))))
        
        ;;=== when the function was called when having mlns return the list of modified
        ;; values
        (if mln
            (values val-list message-list)
          (values obj-l message-list))))

;;;------------------------------------------------------- %DELETE-VALUES-ATT-MLN
    
(defun %delete-values-att-MLN (obj-id tp-id value-list old-data-list 
                                      &rest key-arg-list &key language context
                                      already-normed scope &allow-other-keys)
  "deleting an attribute value when some value is an MLN. The function tries to ~
   create two MLNs before calling %delete-values-att-all-MLN.
Arguments:
   see %delete-values-att 
   old-data-list
Return:
   the internal representation of the modified object"
  ;; cheap test: nothing to delete
  (unless (and value-list old-data-list)
    (return-from %delete-values-att-MLN (symbol-value obj-id)))
  
  (flet ((mln? (xx) (or (mln::%mln? xx)(mln::mln? xx))))
    (let ((lan (or language *language*))
          old-mln mln msg-list message-list obj-l mln-normed)
      
      ;;=== make everything MLN e.g. if value-list is '("a" "b" "c")
      (cond
       ((mln? (car old-data-list))
        (setq old-mln (car old-data-list)))
       (t
        ;; should warn here since we had a list of value and we transform it into
        ;; an mln
        (setq old-mln (mln::make-mln old-data-list :language lan))
        (push (format nil "We transform existing values (~{~S~^ ~}) into an mln ~S"
                old-data-list old-mln)
              message-list)
        )
       )
      ;;=== check now list of values to remove
      (if (mln? (car value-list))
          ;; if already MLN use it
          (setq mln (car value-list))
        ;; otherwise make new one (may fail if values are not strings)
        (setq mln (mln::make-mln value-list :language lan))
        )
      
      ;;=== norm value (for MLNs =xi applies to the MLN itself)
      (unless already-normed
        (setq mln-normed (-> tp-id '=xi mln)))
      
      ;;=== now delete value
      (cond
       ((null mln-normed)
        (push (format nil "MLN ~S has an illegal format (=xi method)" mln)
              message-list)
        )
       ;; can do the work only if MLN is non nil
       (t
        (setq mln mln-normed)
        ;; if we want to delete the whole mln, check if mlns are equal, i.e. share
        ;; some value in some language
        (if (and (eql scope :all)(equal+ old-mln mln))
            (return-from %delete-values-att-MLN
              ;; call with twice the old argument: will delete the whole MLN
              (apply #'%delete-values-att-MLN-all obj-id tp-id old-mln old-mln
                     :context context key-arg-list)))
        
        ;; otherwise call %delete-values-att-MLN-all to delete some values
        (multiple-value-setq (obj-l msg-list)
          (apply #'%delete-values-att-MLN-all obj-id tp-id mln old-mln
                 :context context key-arg-list))
        (if msg-list (setq message-list (append msg-list message-list))))
       )
      
      ;;=== return object internals
      (values obj-l message-list))))
    
#|
(defindividual "test" ("aa-mln" ((:en "a" "b")(:fr "c")))(:var _ta))
(%delete-values-att-MLN _ta '$t-test-aa-mln '("a") '(((:en "a" "b")(:fr "c")))
                        :context 0 :language :en)
=xi method fired on ((:EN "a"))
((:EN "a")) is this.
=if-removed method is fired
"a" was a string
"Trying to erase values (a) from object $E-TEST.23 for attribute $T-TEST-AA-MLN in package \"MOSS\" and context 0."
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.23)) ($T-TEST-AA-MLN (0 ((:EN "b") (:FR "c")))))
NIL

(%delete-values-att-MLN _ta '$t-test-aa-mln '(((:en "a" "b")(:fr "c"))) 
                        '(((:en "a" "b")(:fr "c"))) :context 0 :language :en)
=xi method fired on ((:EN "a" "b") (:FR "c"))
((:EN "a" "b") (:FR "c")) is this.
=if-removed method is fired
"a" was a string
=if-removed method is fired
"b" was a string
"Trying to erase values (a b) from object $E-TEST.24 for attribute $T-TEST-AA-MLN in package \"MOSS\" and context 0."
=if-removed method is fired
"c" was a string
"Trying to erase values (c) from object $E-TEST.24 for attribute $T-TEST-AA-MLN in package \"MOSS\" and context 0."
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.24)) ($T-TEST-AA-MLN (0)))
NIL
CG-USER(139): 
|#
;;;--------------------------------------------------- %DELETE-VALUES-AT-MLN-ALL

(defun %delete-values-att-mln-all (obj-id tp-id mln old-mln &rest key-arg-list
                                         &key context &allow-other-keys)
  "here mln and old-mln are both non nil values."
  (let (msg-list message-list val-list old-val-list result)
    ;; for each language, do directly with synonyms
    (dolist (lan (mln::get-languages mln))
      ;; get list of synonyms (values in the specified language)
      (setq val-list (mln::extract mln :language lan))
      (setq old-val-list (mln::extract old-mln :language lan))
      
      ;; do some work only when there are some values
      (when old-val-list
        ;; call the deleting function for non MLN values 
        (multiple-value-setq (result msg-list)
          (apply #'%delete-values-att-MLN-list obj-id tp-id val-list old-val-list 
                 key-arg-list))
        (when msg-list 
          (push (format nil "For language ~S:" lan) message-list)
          (setq message-list (append msg-list message-list))
          )
        ;; update old mln value for this language
        (setq old-mln 
              (if result 
                  (mln::set-values old-mln lan result)
                ;; otherwise remove language tag, otherwise no longer MLN
                (mln::remove-language old-mln lan)))
        )
      )
    ;; update object
    (%%set-value obj-id old-mln tp-id context)
    
    ;; fire =if-removed if there
    (-> tp-id '=if-removed old-mln obj-id)
    
    (values (symbol-value obj-id) message-list)))
  
#|
(defind "test" ("aa" ((:en "" "b")(:fr "c"))) (:var _ta))
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.21)) ($T-TEST-AA (0 ((:EN "" "b") (:FR "c")))))

(%delete-values-at-mln-all _ta '$t-test-aa '((:en "a" "b")(:fr "c"))
                           '((:en "a" "b")(:fr "c")) :context 0)
"Trying to erase values (a b) from object $E-TEST.21 for attribute $T-TEST-AA in package \"MOSS\" and context 0."
"Trying to erase values (c) from object $E-TEST.21 for attribute $T-TEST-AA in package \"MOSS\" and context 0."
(($TYPE (0 $E-TEST)) ($ID (0 $E-TEST.21)) ($T-TEST-AA (0)))
NIL
|#
;;;------------------------------------------------- %DELETE-VALUES-ATT-MLN-LIST

(defun %delete-values-att-mln-list (obj-id tp-id value-list old-value-list  
                                           &key context count no-warning scope
                                           &allow-other-keys)
  "delete a list of synonyms to an attribute that has values. The ~
   object is not modified although entry points are updated.
Arguments:
   obj-id: current object
   tp-id: current attribute
   value-list: list of values to remove
   old-val-list: list of current values to be modified
   context (key): specified context
   count (key): number of occurences of the same value that we remove
   no-warning (key): printing flag
   mln (key): if t means that we process synonyms of MLNs
   see %add-values-att
Return:
   1. the list of remaining values so that it can be used by MLNs
   2. a list of warning messages."
  (declare (ignore scope))
  (let* ((context (or context (symbol-value (intern "*CONTEXT*"))))
         header method val-list msg-list message-list entry-list)
    
    (%%allowed-context? context)
    
    ;; define header in case we want to print warning messages
    (setq header (format nil "Trying to erase values (~{~A~^ ~}) from object ~A ~
                              for attribute ~A in package ~S and context ~S."
                   value-list obj-id tp-id (package-name *package*) context))
    
    (if count (push (format nil "MLN values do not admit a count parameter.")
                    message-list))
    
    (dolist (value value-list)
      ;; check possible entries
      (setq method (get-method tp-id '=make-entry))
      ;; if there, then remove reference to entity from entry-point
      (when method
        (setq entry-list (send tp-id '=make-entry value))
        ;; make-entry returns a list of possible entry points
        (dolist (entry entry-list)
          (%%remval entry obj-id (%inverse-property-id tp-id) context)))
      ;; remove all occurences of the value from the previous values
      (setq old-value-list (remove value old-value-list :test #'equal+))
      )
    
    ;; check-validity only if one-of still holds or we have min values
    (multiple-value-setq (val-list msg-list)
      (%validate-tp tp-id old-value-list :obj-id obj-id :context context
                    :restrictions '($ONEOF $MINT) :no-warning t))
    
    (if msg-list (setq message-list (append msg-list message-list)))
    
    ;; what happens if the list of values has been changed?
    (when (set-difference old-value-list val-list)
      (push (format nil "Some values are not valid but no change has been done")
            message-list))
    
    ;; print warning if required
    (unless no-warning
      (mformat "~{~%~S~^ ~}" (cons header (reverse message-list))))
    
    (values old-value-list message-list)))
    
;;;--------------------------------------------------- %DELETE-VALUES-ATT-SINGLE
;;; called by %delete-values-att-list

(defun %delete-values-att-single (obj-id tp-id value  
                                         &key context count no-warning mln)
  "Delete a single value in a given object. The value is normalized using the =xi ~
   method if any has been defined. Eventual entry points are removed.
Arguments:
   obj-id: id of object
   value: value to delete
   context (key): version number supposed to be valid
   count (key): nb of values to delete in case there are several identical values
   mln (key): means that we are processing values from MLNs
   no-warning (key): printing flag
Return:
   1. internal representation of object
   2. message-list."
  
  (let* ((context (or context (symbol-value (intern "*CONTEXT*"))))
         data method entry-list message-list header)
    
    ;; define header in case we want this function to post messages
    (setq header (format nil "Trying to erase value ~A from object ~A for ~
                              attribute ~A in package ~S and context ~S."
                   value obj-id tp-id (package-name *package*) context))
    
    ;; normalize value to remove
    (setq data (-> tp-id '=xi value))
    
    (cond
     ;; if data could not be normalized, skip action
     ((null data)
      ;; here we could not normalize the proposed value
      (push (format nil "Value has a wrong format") message-list)
      )
     (t
      ;; check for possible entry point trying first to recover method
      (setq method (get-method tp-id '=make-entry))
      ;; if there, then remove reference to entity from entry-point
      (when method
        (setq entry-list (send tp-id '=make-entry data))
        ;; make-entry returns a list of possible entry points
        (dolist (entry entry-list)
          (%%remval entry obj-id (%inverse-property-id tp-id) context)))
      
      ;; update current entity, starting at position 0
      (unless mln
        (%%remnthval obj-id data tp-id context 0 :count count))
      ;; then activate potential demon
      (send tp-id '=if-removed data obj-id)
      ))
    
    ;; print messages if required
    (unless no-warning
      (mformat "~{~%~S~^ ~}" (cons header message-list)))
    
    (values (symbol-value obj-id) message-list)))
	 
;;;---------------------------------------------------------- %DELETE-VALUES-INV

(defun %delete-values-inv (obj-id inv-id pred-list &key context no-warning)
  "Deletes a list of predecessors by inverting the property and applying it to ~
   the elements of pred-list, unless pred-list is :all.  
Arguments:.
   object-id: id of current objbect
   in-id: id of inverse relation
   pred-list: list of successors, in any of the legal relation formats or :ALL
   context (key): context
   no-warning (key): t/nil) if t does not pring warning messages
Returns:
   1. the modified internal object representation.
   2. a list of warning messages"
  (let ((context (or context (symbol-value (intern "*CONTEXT*"))))
        (prop-id (%inverse-property-id inv-id))
        message-list suc-list msg-list)
    
    ;; if suc-list is all, get linked values
    (if (eql pred-list :all)
        ;; suc-list might be empty here
        (setq pred-list (%get-value obj-id inv-id context)))
    
    (unless pred-list  
      ;; if arg is empty, nothing to remove, quit right away
      (return-from %delete-values-inv (values (symbol-value obj-id) nil)))
    
    ;; otherwise save obj-id and unlink all values
    (setq suc-list (list obj-id))
    
    (dolist (id pred-list)
      (cond
       ;; inverse attributes are used in entry-points
       ((%is-attribute? prop-id)
        (multiple-value-setq (obj-id msg-list)
          (%delete-values-att id prop-id suc-list :context context
                              :no-warning no-warning))
        )
       ((%is-relation? prop-id)
        (multiple-value-setq (obj-id msg-list)
          (%delete-values-rel id prop-id suc-list :context context
                              :no-warning no-warning)))
       )
      (if msg-list (append  message-list msg-list)))
    
    (values (symbol-value (car suc-list)) message-list)))

;;;---------------------------------------------------------- %DELETE-VALUES-REL
    
(defun %delete-values-rel (obj-id sp-id suc-list &key already-normed context 
                                  no-warning)
  "Deletes a list of new successors.  
  If deleted the =if-removed method is fired presumably for bookkeeping. ~
  Cardinality constraints are checked for one-of and minimal values. 
Arguments:.
   object-id: id of current object
   sp-id: id of relation
   suc-list: list of successors, in any of the legal relation formats or :ALL
   context (key): context
   already-normed (key): if t, =xi or =filter was already applied to value-list
   no-warning (key): t/nil) if t does not print warning messages
Returns:
   1. the modified internal object representation.
   2. a list of warning messages"
  (let ((context (or context (symbol-value (intern "*CONTEXT*"))))
        message-list header)
    
    (unless suc-list  
      ;; if arg is empty, nothing to remove, quit right away
      (return-from %delete-values-rel (values (symbol-value obj-id) nil)))
    
    (cond
     ;; if suc-list is all, delete existing values
     ((eql suc-list :all)
      ;; suc-list might be empty here
      (setq suc-list (%get-value obj-id sp-id context)))
     ;; first resolve relation formats of values, the result is a set of ids
     ((not already-normed)
      (multiple-value-setq (suc-list message-list)
        (%add-values-rel-resolve-formats sp-id suc-list context :obj-id obj-id))))
    
    ;; for printing message list (here because value-list has to be a list)
    (setq header 
          (format nil "Warning: when deleting successors ~{~S~^ ~} to object ~S for ~
             relation ~S in package ~S and context ~S:"
            suc-list obj-id sp-id (package-name *package*) context))
    
    (cond
     ;; if suc-list is not empty  remove the links
     (suc-list
      (dolist (suc-id suc-list)
        (%unlink obj-id sp-id suc-id context)
        ;; do some clean up
        (with-context context
          (send sp-id '=if-removed obj-id suc-id)
          ;; check ONE-of and min restrictions
          (multiple-value-bind (result msg-list)
              (%validate-sp sp-id (%get-value obj-id sp-id context) :obj-id obj-id
                            :context context :restrictions '($ONEOF $MINT) 
                            :no-warning t)
            (declare (ignore result))
            (if msg-list
                (setq message-list (append msg-list message-list)))
            ))             
        ))
     ;; here could not resolve successors (message-list not empty)
     (message-list
      (push (format nil "Can't resolve the successor list...") message-list)
      )
     ;; otherwise we may not have anything to delete (e.g. with the :all option)
     ;; we should insert (<property> (<context>)) to indicate that the object has
     ;; no value in this context, avoiding to look into upper contexts
     (t
      (send obj-id '=set-id sp-id nil))
     )
    
    (unless (or no-warning (null message-list))
      (mformat "~{~A~^ ~}" (cons header (reverse message-list))))
    
    (values (symbol-value obj-id) message-list)))


;;;=============================================================================
;;;                         END delete functions
;;;=============================================================================


;;;------------------------------------ %DETERMINE-INVERSE-PROPERTY-ID-FOR-CLASS

(defUn %determine-inverse-property-id-for-class (prop-list class-id)
  "Determines the list of inverse prop-id that may point to the class, meaning ~
   it includes inverse properties of ancestors.
Arguments:
   prop-list: list of property identifiers, e.g. $S-PERSON-HUSBAND
   class-id: identifier of the class, e.g. $E-PERSON (*any* is allowed)
   context (opt): context default current
Returns:
   nil or list of property-id"
  ;; if we have a list of inverse properties, we need to make them direct
  (if (every #'%is-inverse-property? prop-list)
      (setq prop-list (mapcar #'%inverse-property-id prop-list)))
  ;(format t "~%; %determine-inverse-property-id-for-class / prop-list: ~S" prop-list)
  ;; first time around class-id is a symbol, next time it will be a list
  (let ((ancestor-list (%ancestors class-id))
        incoming-prop-list)
    ;; get all the properties pointing to classes
    (setq incoming-prop-list
          (reduce 
           #'append 
           (mapcar #'(lambda (xx) 
                       (%get-value xx (%inverse-property-id '$SUC)))
             ancestor-list)))
    ;(format t "~%; %determine-inverse-property-id-for-class / incoming-prop-list: ~S"
    ;  incoming-prop-list)
    (mapcar #'%inverse-property-id
      (intersection prop-list (delete-duplicates incoming-prop-list)))))

#| ; in package :test
(MOSS::%DETERMINE-INVERSE-PROPERTY-ID-FOR-CLASS
 '($S-BROTHER $S-COUSIN $S-PERSON-SISTER)
 '$E-PERSON)
($S-PERSON-SISTER.OF)

;; to be executed in the test package
(with-context 6
  (MOSS::%DETERMINE-INVERSE-PROPERTY-ID-FOR-CLASS
   '($S-BROTHER $S-COUSIN $S-PERSON-SISTER)
   '$E-PERSON))
($S-PERSON-SISTER.OF)
|#
;;;-------------------------------------------- %DETERMINE-PROPERTY-ID-FOR-CLASS
;;; to check...
;;;********** what if class-id is *none*? we should return generic property
;;; Not sure what it does with versions...

(defUn %determine-property-id-for-class (prop-list class-id &key inverse)
  "Determines the right prop-id for the specified class among a list of properties.
   No checks on arguments. If property is inherited, then we return the closest one.
   If class is *any* returns the list. In case of multiple inheritance conflict,  ~
   returns the property of the first class in the precedence list.
Arguments:
  prop-list: list of property identifiers, e.g. $T-NAME, $S-PERSON-HUSBAND.OF
  class-id: identifier of the class, e.g. $E-PERSON (*any* is allowed)
  context (opt): context default current
  inverse (key): specify that we look for inverse property
Returns:
  nil or list of property-id"
  (unless prop-list (return-from %determine-property-id-for-class nil))
  ;(format t "~%;---------- %determine-property-id-for-class")
  ;; special case of inverse links
  (if (every #'%is-inverse-property? prop-list)
      (let ((result (%determine-property-id-for-class
                     (mapcar #'%inverse-property-id prop-list)
                     class-id
                     :inverse t)))
        (return-from  %determine-property-id-for-class   
          (if result (list (%inverse-property-id (car result)))))))
  
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    ;(format t "~%;--- *package*: ~S" *package*)
    ;(format t "~%;--- context: ~S" context)
  ;; there is no need to resolve class-id since there is only a single metaclass
  ;; Several cases here according to the class argument
  (cond 
   ;; recursion test
   ((null class-id) nil)
   ;; if any class is valid, we must return the property tree
   ((eql class-id '*any*) prop-list)
   ;; if we look for inverse property test suc.of
   ((and inverse (inter prop-list (%%get-value class-id '$suc.of context))))
   ;; otherwise we must return the specific property for the class
   ((inter prop-list (%%get-value class-id '$pt context)))
   ;; try relationships
   ((inter prop-list (%%get-value class-id '$ps context)))
   ;; try superclass if any
   ;; not very efficient though since it recomputes tree each time
   ;;***** won't work if $IS-A is a tree property
   ;; careful if class has several ancestors...we take the first one 
   ((let ((prop-list
           (reduce #'append
                   (mapcar #'(lambda (xx)
                               (%determine-property-id-for-class 
                                prop-list xx :inverse inverse))
                     ;(%%has-value class-id '$IS-A context)))))
                     (%%get-value class-id '$IS-A context)))))
      ;; if we have several properties from multiple inheritance, we keep one
      (if (cdr prop-list) (list (car prop-list)) prop-list))))))

#|
? (%determine-property-id-for-class '($T-NAME $T-PERSON-NAME $T-BAKER-NAME 
                                      $T-BUTCHER-TRAINEE-NAME) '$E-BUTCHER)
($T-PERSON-NAME)
? (%determine-property-id-for-class '($T-NAME $T-PERSON-NAME $T-BAKER-NAME 
                                              $T-BUTCHER-TRAINEE-NAME) '$E-BAKER)
($T-BAKER-NAME)
? (%determine-property-id-for-class '(address::$t-name address::$t-person-name 
                                                       address::$t-first-name) 'address::$e-person)
(ADDRESS::$T-PERSON-NAME)
;;; inverse properties
? (moss::%determine-property-id-for-class '($s-student-school.of $s-school.of)
                                          '$E-TEACHING-ORGANIZATION)
($S-STUDENT-SCHOOL.OF)
? (moss::%determine-property-id-for-class '($s-student-school.of $s-school.of) 
                                          '$E-university)
($S-STUDENT-SCHOOL.OF)
|#
;;;--------------------------------------------------------------------- %ENTRY?
;;; deprecated. use %is-entry?

;;;A------------------------------------------------------------ %EXPLODE-OBJECT

;;; this function explodes object in case we want to represent the environment as
;;; a set of triples on which we could apply an inference engine.
;;; In that case we need to explode all instances of interest of the application...

(defUn %explode-object (obj-id &key into include-refs)
  "function that explodes an instance into a set of triples appending it to the ~
   list specified by the into variable.
Arguments:
   obj-id: object identifier
   into (key): list to which to append new triples
   include-refs (key): if true include reference objects, otherwise resolve
Return:
   the list of triples."
  (unless include-refs (setq obj-id (%resolve obj-id)))
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    
    (unless (%alive? obj-id context)
      (return-from %explode-object 
        (warn "object ~S is not available or dead in context ~S in package ~S"
          obj-id context *package*)))
    
    (when (moss::%pdm? obj-id)
      (let (result)
        ;; use the raw format representation to explode the object
        (dolist (property (symbol-value obj-id))
          (cond
           ((eql (car property) 'moss::$type)
            (push `(,obj-id HAS-TYPE ,(car (moss::%get-value obj-id (car property))))
                  result))
           ;; discard ID
           ((eql (car property) 'moss::$ID))
           ;; property         
           ((or (moss::%is-attribute? (car property))
                (moss::%is-relation? (car property)))
            (dolist (item (moss::%get-value obj-id (car property)))
              (push (list obj-id (moss::%make-name-for-property 
                                  (car (moss::%get-value (car property) 'moss::$PNAM)))
                          item)
                    result)))
           ;; inverse-property
           ((moss::%is-inverse-property? (car property))
            (dolist (item (moss::%get-value obj-id (car property)))
              (push (list obj-id 
                          (intern 
                           (MLN::GET-CANONICAL-NAME ; will work with old versions
                            (car (moss::%get-value (car property) 'moss::$INAM))))
                          item)
                    result)))))
        ;; return
        (if into (set into (append into (reverse result))))
        (reverse result)))))

#|
(defconcept "person" (:att "name" (:entry))(:att "first name"))
$E-PERSON

(defindividual "person" ("name" "Einstein")("first name" "Albert"))
$E-PERSON.1

(moss::%explode-object '$e-person.1)
(($E-PERSON.1 MOSS::HAS-TYPE $E-PERSON) 
 ($E-PERSON.1 HAS-NAME "Barth�s")
 ($E-PERSON.1 HAS-FIRST-NAME "Jean-Paul"))

(with-context 3
  (moss::%explode-object '$e-person.4))
(($E-PERSON.4 MOSS::HAS-TYPE $E-PERSON) 
 ($E-PERSON.4 HAS-NAME "Barth�s")
 ($E-PERSON.4 HAS-FIRST-NAME "Camille"))

(with-context 6
  (moss::%explode-object '$e-person.4))
Warning: object $E-PERSON.4 is not available or dead in context 6 in package #<The TEST package>
NIL
|#
;;;A------------------------------------------------------------------- %EXTRACT

;;; (%extract (entry tp class &rest option-list) - extract
;;; an object that is specified by the entry-point, terminal property and is
;;; of type class. When more than one apply a function when specified in the
;;; options
;;;	(:filter function)
;;; E.g. (%extract 'COUNTER 'HAS-CONCEPT-NAME 'ENTITY)
;;; or   (%extract 'COUNTER 'HAS-CONCEPT-NAME 'ENTITY :filter #'car)

;;; 5/5/90 We modify %def-extract to accomodate two new queries
;;;	(%def-extract <entry> <tp> '*any* {...}) which does not filter on class
;;;	(%def-extract <entry> <tp> '*none* {...}) which insists for getting a
;;;			classless object
;;; options like system and filter function remain as they were
;;; 23/12/92 We suppress the system option. I.e. all entities will be extracted
;;;      from the active system
;;; 8/2/94 We add the possibility to extract a value from the system and required
;;;      systems. We add an optional parameter which restricts to specified 
;;;      system, not allowing requested systems. Usually though the values will
;;;      be extracted from the specified (current) system and from any other
;;;      required systems if not found in the specified system.
;;; 24/3/05 modification to take into account property trees. System argument
;;;      is removed
;;; 10/01/11 new simplified extract: remove the filter function and the option
;;;      allow multiple values. The rationae is the following: we look for an object
;;;      or several objects in the current package (*package*). If we can't find 
;;;      anything, we look into the MOSS package. The class argument must refer
;;;      to a unique object in the local package or in the MOSS package if not
;;;      available in the local package.

(defUn %extract (entry tp class &key class-ref no-subclass context  
                       (package *package*))
  "Extracts an object that is specified by its entry-point - attribute - ~
   concept. It is recommended to avoid symbols for tp and class.
Arguments:
   entry: entry point (string, symbol or mln)
   tp: attribute (string, symbol or mln)
   class: class name (string, symbol or mln)
   no-subclass (key): if true does not include instances of subclasses
   context (key): context default current
   class-ref (key): reference of class (used to locate a specific property)
   package (key): default current
Returns
   nil if it cannot find anything
   a symbol for a single object
   a list when it finds several objects"
  ;; if context is not specified, use current *package* context
  (unless context (setq context (symbol-value (intern "*CONTEXT*" package))))
  ;(break "%extract")
  (with-package package
    (with-context context
      (dformat :extract 0 "%extract /package (arg): ~S" package)
      (dformat :extract 0 "%extract /*package*: ~S" *package*)
      (dformat :extract 0 "%extract /context (arg): ~S" context)
      (let (prop-list lres class-id filtered-result fn)    
        ;; take care of special inputs (e.g. mln)
        ;; whenever tp is a symbol it must be HAS-XXX
        ;; when the input is a symbol, %make-name-for-property builds a symbol for
        ;; property in the execution package of the function, regardless of the
        ;; package from which the function was called
        (when (symbolp entry) (setq entry (intern (symbol-name entry))))
        (dformat :extract 0 "%extract /entry: ~S" entry)
        (setq tp (%make-name-for-property tp))
        (dformat :extract 0 "%extract /tp: ~S" tp)
        (setq class (%make-name-for-class class)) ; used in messages
        (dformat :extract 0 "%extract /class: ~S" class)
        
        ;; if class-id is not unique %%get-id throws to :error
        ;; careful: the symbol representing the class-id is created in the 
        ;; execution package of the function
        (setq class-id (%%get-id class :class))
        (dformat :extract 0 "%extract /class-id: ~S" class-id)
        
        ;; if class-id is nil, try :moss package
        (unless class-id
          (with-package :moss
            (setq class-id (%%get-id class :class))))
        (dformat :extract 0 "%extract /class-id 2: ~S" class-id)
        
        ;; if still nil, problem no class found
        (unless class-id
          (warn " in %extract: no class ~A in this package (~S) with context (~A),~
                 while trying to locate object ~A." class package context entry)
          (throw :error t))
        
        ;;=== now determine the property id corresponding to the class
        ;; first gather all properties sharing the same name restricted to class-id
        (setq prop-list 
              (%determine-property-id-for-class 
               (%get-value tp (%inverse-property-id '$PNAM)) class-id))
        (dformat :extract 0 "%extract /prop-list: ~S" prop-list)
        
        ;; we may have a list when class-id is *none* (orphans) or *any*
        ;; however we should have at least get one property back
        (unless prop-list
          (warn " in %extract: class ~A has not the property ~A in this context (~A), ~
                 while trying to locate object ~A."
            class tp context entry)
          (throw :error t))
        
        ;; Several cases here according to the class argument
        (cond
         
         ;; any class is valid, thus we retrieve all objects for all properties
         ((eql class-id (intern "*ANY*"))
          (dformat :extract 0 "%extract /...looping on prop-list")
          (dolist (prop-id prop-list)
            ;; first get the =make-entry function related to the property
            (setq fn (get-method prop-id '=make-entry))
            (when fn
              ;; compute entry point
              (setq entry (car (funcall fn entry)))
              ;; get the pointed objects
              (setq lres 
                    (append (%get-value entry (%inverse-property-id prop-id)) 
                            lres)))))
         
         ;; orphans: retrieve objects using all properties, but keep only orphans
         ((eql class-id (intern "*NONE*"))	
          ;; we are only interested by classless objects
          (dolist (prop-id prop-list)
            (setq fn (get-method prop-id '=make-entry))
            (when fn
              ;; compute entry point
              (setq entry (car (funcall fn entry)))
              (setq lres 
                    (append (%get-value entry (%inverse-property-id prop-id))
                            lres))))
          ;; keep orphans
          (setq lres  ; JPB 140820 removing mapcan
                (reduce
                 #'append
                 (mapcar
                     #'(lambda (xx)(if (%is-classless-object? xx) (list xx)))
                   (delete-duplicates lres)))))
         
         (t		; normal case
          ;; we have a list of possible properties and a single class, we must 
          ;; find objects. For all properties looks for objects whose entry point
          ;; is entry.
          (dformat :extract 0 "%extract /... looping on prop-list")
          (dolist (prop-id prop-list)
            (dformat :extract 0 "%extract /prop-id: ~S" prop-id)
            (setq fn (get-method prop-id '=make-entry))
            (dformat :extract 0 "%extract /fn: ~S" fn)
            (when fn
              ;; compute entry point
              (setq entry (car (funcall fn entry)))
              (dformat :extract 0 "%extract /entry: ~S" entry)
              ;; get all objects including from sub-classes
              (setq lres 
                    (append 
                     (%extract-from-id entry prop-id class-id 
                                       :no-subclass no-subclass) 
                     lres))
              (dformat :extract 0 "%extract /lres: ~S" lres)
              ))
          (dformat :extract 0 "%extract /... end loop")
          )  ; end normal case
         )  ; end case
        
        ;; here, if we have nothing left, we quit
        (unless lres (return-from %extract nil))
        
        ;; delete-duplicates if any
        (setq lres (delete-duplicates lres :test #'equal)) 
        (dformat :extract 0 "%extract /after deleting duplicates lres 2: ~S" lres)
        ;; filter against current package
        (setq filtered-result
              (reduce  ; JPB 140820 removing mapcan
               #'append
               (mapcar
                   #'(lambda (xx) (if (eql (symbol-package xx) *package*)
                                      (list xx)))
                 lres)))
        (dformat :extract 0 "%extract /filtered-result wrt *package*: ~S" filtered-result)
        ;; if nothing left, try to filter against MOSS package instead
        (unless filtered-result
          (setq filtered-result 
                (reduce  ; JPB 140820 removing mapcan
                 #'append
                 (mapcar
                     #'(lambda (xx) (if (eql (symbol-package xx) 
                                             (find-package :moss))
                                        (list xx)))
                   lres))))
        (dformat :extract 0 "%extract /filtered-result wrt MOSS: ~S" filtered-result)
        ;; the class-ref argument indicates that we are looking for a property
        ;; we get the tree of properties and must keep the one corresponding to
        ;; class-ref
        (dformat :extract 0 "%extract /class-ref: ~S" class-ref)
        (when class-ref
          (setq filtered-result
                (%determine-property-id-for-class  
                 filtered-result
                 ;(%get-class-id-from-ref class-ref)
                 (%%get-id class-ref :class)
                 ))
          )
        (dformat :extract 0 "%extract /filtered-result 3: ~S" filtered-result)
        ;; if a single object don't return a list
        (if (cdr filtered-result) filtered-result (car filtered-result))
        ))))

#|
Checking various syntactical styles:
(%extract 'moss-counter 'has-moss-concept-name 'moss-class)
$CTR

(%extract "moss-counter" 'has-moss-concept-name 'moss-class)
$CTR

(%extract "moss counter" "moss-concept-name" 'moss-class)
$CTR

(%extract "moss counter" "moss concept-name" "moss entity")
$CTR

(%extract '(:en "moss counter") "moss-concept-name" "moss entity")
$CTR

(%extract '((:en "moss counter")) '((:en "moss concept-name")) "moss entity")
$CTR

(%extract '(:en "moss counter") '(:en "moss concept-name") '(:en "moss entity"))
$CTR

(%extract '(:en "moss counter") 'has-moss-concept-name "moss entity" 
          :package *package*)
$CTR

(with-package :test
  (%extract '(:en "counter") 'has-moss-concept-name "moss entity" 
            :package *package*))
test::$CTR  ; get answer from test environment

(%extract '(:en "counter") 'has-moss-concept-name "moss entity" 
          :package (find-package :test))
test::$CTR

(with-package :test
    (%extract '(:en "moss concept") 'has-moss-concept-name "moss entity"))
$ENT ; get answer from MOSS

(with-package :test
  (%extract '(:en "counter") "moss concept-name" "moss entity" 
            :package *package*))
test::$CTR

(with-package :test
  (%extract '(:en "counter") 'moss-concept-name "moss entity" 
            :package *package*))
test::$CTR

(setq moss::*verbose* t)
T

(catch :error (%extract 'einstein 'has-name 'person))
Warning:  in %extract: no class PERSON in this package (#<The MOSS package>) with context (0),while
         trying to locate object EINSTEIN.
T

(%extract 'test::barth�s 'test::has-name 'test::person :package :test)
(TEST::$E-PERSON.2 TEST::$E-PERSON.1)

; When calling the function from the MOSS package or from any other package than
; test, the calling arguments if symbols are interned in the calling package.
; However, the %extract function replaces them with symbols interned in the package
; active at execution time, which is more intuitive
; i.e in package test
(moss::%extract 'barth�s 'has-name 'person)
(TEST::$E-PERSON.2 TEST::$E-PERSON.1)

; in the next call, although symbols have been defined in the right package, the
; execution is done in the MOSS package, and symbols are reinterpreted in the MOSS
; package where the class PERSON does not exist
(catch :error (%extract 'test::barth�s 'test::has-name 'test::person))
Warning:  in %extract: no class PERSON in this package (#<The MOSS package>) with context
         (0),while trying to locate object BARTH�S.
T

Versions:
(setq *version-graph*
      '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3

test::$E-PERSON.1
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.1))
 (TEST::$T-PERSON-NAME (0 "Barth�s"))
 (TEST::$T-PERSON-FIRST-NAME (0 "Jean-Paul")) ($OMS (6 TEST::$FN.10)))

(with-package :test
  (%extract 'test::barth�s 'test::has-name 'test::person))
(TEST::$E-PERSON.2 TEST::$E-PERSON.1)

(with-package :test
  (with-context 2
    (%extract 'test::barth�s 'test::has-name 'test::person)))
(TEST::$E-PERSON.4 TEST::$E-PERSON.2 TEST::$E-PERSON.1)

(with-context 5
  (defindividual "person" ("name" "MO5")))
((MOSS::$TYPE (5 $E-PERSON)) (MOSS::$ID (5 $E-PERSON.7)) ($T-PERSON-NAME (5 "MO5")))

MO5
((MOSS::$TYPE (5 MOSS::$EP)) (MOSS::$ID (5 MO5)) ($T-PERSON-NAME.OF (5 $E-PERSON.7))
 (MOSS::$EPLS.OF (5 $SYS.1)))

(with-package :test
  (with-context 5
    (%extract 'test::barth�s 'test::has-name 'test::person)))
(TEST::$E-PERSON.6 TEST::$E-STUDENT.2 TEST::$E-PERSON.3 TEST::$E-PERSON.2 TEST::$E-PERSON.1)
|#
;;;A----------------------------------------------------------- %EXTRACT-FROM-ID

(defUn %extract-from-id (entry prop-id class-id &key no-subclass)
  "Returns the list of all objects  corresponding to entry point entry ~
   with inverse attribute of prop-id, and of class class-id in the current ~
   context.
    E.g. (%extract-from-id 'ATTRIBUTE '$ENAM '$ENT 0) returns ($EPT)
    obj-id are returned if they are in class class-id or in one of the subclasses ~
   (transitive closure along the $IS-A.OF link).
    Also all meta-models $ENT, $EPS, $EPT, $FN, $CTR ($EIL?) are defined ~
   as instances of $ENT.
    If context is illegal throws to an :error tag.
    The function works with the *any* and *none* pseudo classes:
	(%extract-from-id <entry> <prop-id> '*any*) returns everything
	(%extract-from-id <entry> <prop-id> '*none*) returns only classless objects.
    We also allow objects with multiple classes.
   No checks are done to speed execution.
Arguments:
   entry: entry point (symbol)
   prop-id: id of property to be used to make the inverse
                should be the exact property associated with class-id
   class-id: id of the potential class of the object
   no-subclass (key): if t do not include instances from subclasses
Returns:
   a list of objects maybe one"
  ;; the following test is useful at boot-time when prop-id and class-id are not
  ;; yet defined and we are creating a new property
  (unless (%is-entry? entry) (return-from %extract-from-id nil))
  (let (entity-list inv-id lres type-list)
    ;; no check on prop-id
    ;(setq inv-id (%make-id-for-inverse-property prop-id))
    (setq inv-id (%inverse-property-id prop-id))
    
    ;;***** kludge to allow strings. Will not work with user-defined =make-entry
    (if (stringp entry) (setq entry (%make-name-for-entry entry)))
    
    ;; consider entry as an entry point id and try to access objects
    (setq entity-list (if (%pdm? entry)(%get-value entry inv-id)))
    ;; if no entity is in the list we return immediately
    (unless entity-list (return-from %extract-from-id nil))
    
    (case class-id
      (*any*  ; we simply return everything
       entity-list)
      
      (*none* ; we return only classles objects
       (reduce  ; JPB 140820 removing mapcan
        #'append
        (mapcar 
            #'(lambda (xx)(if (%is-classless-object? xx) (list xx)))
          entity-list)))
      
      (t	; normal case
       ;; test if we are looking for a meta model
       (when (member entry '(ENTITY STRUCTURAL-PROPERTY TERMINAL-PROPERTY
                                    COUNTER METHOD INVERSE-LINK))
         ;; If this is the case and if class-id is a sub meta class
         ;; then we reset class-id to $ENT
         (if (or (eq class-id '$ENT) ; to speed up operations sometimes
                 (member '$ENT (%sp-gamma class-id '$IS-A)))
             (setq class-id '$ENT)))
       
       ;; compute all possible sub-types of class-id
       (if no-subclass
           (setq type-list (list class-id))
         (setq type-list 
               (%sp-gamma class-id (%make-id '$EIL :id '$IS-A))))
       ;; then for each object in entity-list checks that one of its types belongs to
       ;; the list of subtypes
       (dolist (ent-id entity-list)
         (dolist (prop-id (%get-value ent-id '$TYPE))
           ;; we check here for anything with right subtype
           (if (member prop-id type-list) (pushnew ent-id lres))))
       lres))))

#|
(%EXTRACT-from-id 'person '$ENAM '$ENT)
($E-PERSON)

(catch :error (%extract 'MO1 'has-name 'person))
Warning:  in %extract: no class PERSON in this package (#<The MOSS package>) with context
(0),while trying to locate object MO1.
T

(with-package :test
  (%extract-from-id 'test::person '$ENAM '$ENT))
(TEST::$E-PERSON)

(with-package :test
  (%extract-from-id 'test::barth�s 'test::$t-person-name 'test::$e-person))
(TEST::$E-PERSON.2 TEST::$E-PERSON.1)

(with-package :test
  (%extract-from-id 'test::barth�s 'test::$t-name 'test::$e-person))
NIL

(with-package :test
  (with-context 4
    (%extract-from-id 'test::barth�s 'test::$t-person-name 'test::$e-person)))
(TEST::$E-PERSON.3 TEST::$E-PERSON.2 TEST::$E-PERSON.1)
|#
;;;---------------------------------------------------------------- FAIL-ANSWER?
;;; Uses %get-value, hence tests in the current version
;;; However not tested with versions

(defUn fail-answer? (conversation)
  "test if the answer is a failure, i.e. either FACTS/input contains a ~
   failure mark, or it contains input-there and FACTS/answer contains a ~
   failure mark.
Arguments:
   conversation: conversation id
Return:
   T or nil"
  (let ((input (read-fact conversation :input))
        (answer (read-fact conversation :answer)))
    ;; input could be :failure or ("failure") or '("*failure*") or else ...
    ;; ("input there") with input in answer
    (if (listp input)(setq input (car input)))
    ;; same for answer
    (if (listp answer)(setq answer (car answer))) ; JPB1003
    ;(format t "~%; moss::fail-answer? /~&   input: ~S~&   answer: ~S"
    ;  input answer)
    (or (eql input :failure)
        (and (stringp input)
             (or (string-equal input "failure")
                 (string-equal input "*failure*")
                 (and (string-equal input "answer-there")
                      (or (eql answer :failure)
                          (and (stringp answer)
                               (or (string-equal answer "failure")
                                   (string-equal answer "*failure*"))))))))))
#|
(fail-answer? 'albert::$cvse.1)
; moss::fail-answer? /
input: "answer-there"
answer: (("universit�" ("nom" "Universit� de Technologie de Compi�gne") 
          ("sigle" "UTC")))
NIL
|#	
;;;A--------------------------------------------------------------- FILL-PATTERN

(defUn fill-pattern (obj-id-or-obj-id-list pattern &key default-method arg-list)
  "takes a list of objects and processes each object extracting info corresponding ~
   to pattern. If no pattern, applies the default method. If no default method ~
   applies =summary.
Arguments:
   obj-id-or-ob-id-list: ID of the object or list of oject IDs
   pattern: e.g. (\"person\" (\"name\") (\"address\"))
   default-method (key): applied when there is no pattern
   arg-list (key): eventual arguments to default method
Return:
   filled pattern (eventually partially)"
  (let (object-list result single-flag)
    ;; normalize symbol to list
    (if (symbolp obj-id-or-obj-id-list)
        (setq object-list (list obj-id-or-obj-id-list) single-flag t)
      (setq object-list obj-id-or-obj-id-list))
    
    (cond
     ;; check for possible strange inputs
     ((not (listp object-list))
      (error "object arg (~S) should be an object ID or a list."
        obj-id-or-obj-id-list))
     
     (pattern
      (setq result
            (mapcar #'(lambda (xx) (%fill-pattern xx pattern)) object-list)))
     
     ;; if no pattern apply default method
     ((and (not pattern) default-method)
      (setq result
            (reduce #'append
                    (apply #'broadcast object-list default-method arg-list))))
     
     ;; if no default method apply =summary
     ((and (not pattern) (not default-method))
      (reduce #'append  (setq result (broadcast object-list '=summary)))))
    
    ;; if we had a symbol return first element of the list
    ;; remove NIL from the list linked with objects outside the specified context
    (if single-flag
        (car result)
      (remove NIL result))
    ))

#|
;;; test 091129 with ODIN HDSRI ADDRESS
(fill-pattern '$E-PERSON.2 '("personne" ("nom")("initiales du pr�nom")))
("personne" ("nom" "Barth�s") ("initiales du pr�nom" "J.-P."))

(fill-pattern '$E-PERSON.2 '("personne" ("nom")("employeur")("email")))
("personne" ("nom" "Barth�s") ("employeur") ("email" "barthes@utc.fr"))

(moss::%fill-pattern '$E-PERSON.23 
                       '("personne" ("employeur" ("organisation" ("nom")))
                         ("nom")("email")))
("personne"
 ("employeur" ("organisation" ("nom" "Universit� de Technologie de Compi�gne")))
 ("nom" "Barth�s") ("email" "barthes@utc.fr"))

(fill-pattern '($E-PERSON.2  $E-PERSON.3 $E-PERSON.4)
                '("personne" ("nom")("employeur")("email")))
(("personne" ("nom" "Barth�s") ("employeur") ("email" "barthes@utc.fr"))
 ("personne" ("nom" "Barth�s Biesel") ("employeur") ("email" "dbb"))
 ("personne" ("nom" "Bonnifait") ("employeur") ("email")))

(fill-pattern '($E-PERSON.2  $E-PERSON.3 $E-PERSON.4)
                nil :default-method '=get :arg-list '("name"))
(("Barth�s") ("Barth�s Biesel") ("Bonnifait"))

(fill-pattern '($E-PERSON.2  $E-PERSON.3 $E-PERSON.4)
                nil :default-method '=get-address-full)
(("personne" ("nom" "Barth�s") ("pr�nom" "Jean-Paul")
  ("t�l�phone professionnel" "+33 (0)3 44 23 44 66")
  ("portable" "+33 (0)6 80 45 32 67") ("email" "barthes@utc.fr")
  ("web site" "http://www.utc.fr/~barthes"))
 ("personne" ("nom" "Barth�s Biesel") ("pr�nom" "Dominique")
  ("t�l�phone professionnel" "+33 (0)3 44 23 43 97") ("email" "dbb"))
 ("personne" ("nom" "Bonnifait") ("pr�nom" "Philippe")))

;;===== versions
(with-package :test
  (with-context 3
    (fill-pattern '(test::$E-PERSON.1 test::$E-PERSON.4)
                  '("person" ("name")("first name")))))
(("person" ("name" "Barth�s") ("first name" "Jean-Paul"))
 ("person" ("name" "Barth�s") ("first name" "Camille")))

;; when introducing objects outside the context, does not take them into account
(with-package :test
  (with-context 3
    (fill-pattern '(test::$E-PERSON.1 test::$E-PERSON.4 'test::$E-PERSON.6)
                  '("person" ("name")("first name")))))
(("person" ("name" "Barth�s") ("first name" "Jean-Paul"))
 ("person" ("name" "Barth�s") ("first name" "Camille")))
|#
;;;--------------------------------------------------------------- %FILL-PATTERN
;;; not lexically sensitive to versions

(defUn %fill-pattern (obj-id pattern)
  "takes an object ID and tries to extract from the object information to fit the ~
   pattern made of ontology references.
Arguments:
   obj-id: ID of the object
   pattern: e.g. (\"person\" (\"name\") (\"address\"))
Return:
   filled pattern (eventually partially)"
  (let (prop-id obj-list class-id)
    (cond
     ;; if pattern is empty, done
     ((null pattern) nil)
     
     ;; if (car pattern) is a string, this is supposed to be the class of obj-id
     ;; currently we do not check
     ((stringp (car pattern))
      ;; here we must check that the object is of type that of the class
      (setq class-id (%%get-id (car pattern) :class))
      (cond
       ;; if the string is not a class, ignore it
       ((null class-id) nil)
       ;; if the object is not of the right type, ignore it
       ((not (%type? obj-id class-id)) nil)
       ;; if we have an isolated class, use =summary
       ((null (cdr pattern)) 
        (cons (car pattern) (send obj-id '=summary)))
       ;; otherwise go fill detailed info
       (t (cons (car pattern)
                (%fill-pattern obj-id (cdr pattern))))))
     
     ;; if pattern is not a list, bad syntax in pattern
     ((not (listp pattern)) nil)
     
     ;; if (car pattern) is not a list, syntax error ignore car (warning?)
     ((not (listp (car pattern)))
      (%fill-pattern obj-id (cdr pattern)))
     
     ;; if (car pattern) is not a property of the object skip item
     ((not (setq prop-id (%get-property-id-from-name obj-id (caar pattern))))
      (%fill-pattern obj-id (cdr pattern)))
     
     ;; OK, prop-is is a property, try attribute
     ((%is-attribute? prop-id)
      (cons (cons (caar pattern) (send obj-id '=get (caar pattern)))
            (%fill-pattern obj-id (cdr pattern))))
     
     ;; if a not a relation, then syntax error, we skip the item
     ((not (%is-relation? prop-id))
      (%fill-pattern obj-id (cdr pattern)))
     
     ;; if prop-id is a relation we get linked objects
     ((null (setq obj-list (send obj-id '=get (caar pattern))))
      ;; if none, note it
      (cons (list (caar pattern)) ; property with no value
            (%fill-pattern obj-id (cdr pattern))))
          
     ;; here we have objects, check pattern for details
     ((null (cdar pattern))
      ;; we do not want details
      (cons (cons (caar pattern) 
                  (reduce
                   #'append
                   (mapcar #'(lambda(xx) (send xx '=summary)) obj-list)))
            (%fill-pattern obj-id (cdr pattern))))
     
     ;; otherwise we want details (recursive call)
     (t 
      (cons (cons (caar pattern)
                  (remove 
                   nil
                   (mapcar #'(lambda (xx)(%fill-pattern xx (cadar pattern))) 
                     obj-list)))
            (%fill-pattern obj-id (cdr pattern)))))))

;;;---------------------------------------------------------------------- FILTER
;;; exported API function

(defUn filter (id-list &key class-ref no-subclass)
  "filter objects using %filter-against-class"
  (%filter-against-class id-list class-ref :no-subclass no-subclass))

;;;A------------------------------------------------------ %FILTER-AGAINST-CLASS
;;; should test with classes defined in different contexts

(defUn %filter-against-class (id-list class-ref &key no-subclass)
  "filter the list of objects keeping the instances of the specified class in ~
   the current context.
Arguments:
   id-list: list of MOSS object ids
   class-ref: reference of the class (can be class-id)
   no-subclass (key): if t exclude instances of subclasses
Return:
   a list of object ids possibly empty"
  (let ((class-id (%%get-id class-ref :class))
        (context (symbol-value (intern "*CONTEXT*"))))
    
    (unless class-id 
      (warn "can't find the class: ~S for filtering in context ~S in package ~S"
        class-ref context *package*)
      (return-from %filter-against-class id-list))
    
    ;(format t "~%; %filter-against-class / class-id: ~S, id-list:~%  ~S"
    ;  class-id id-list)
    
    (format t "~%; %filter-against-class / id list: ~% ~S" id-list)
    
    (format t "~%; %filter-against-class / no subclass result: ~% ~S"
      (mapcar 
          #'(lambda (xx) 
              (if (member class-id 
                          ;; can't use %%has-value here, otherwise will not keep
                          ;; objects from upper contexts!
                          (%%get-value  xx '$type context))
                  xx))
        id-list))
    
    (if no-subclass
        ;; check if the object if a direct instance of the class
        (remove nil 
                (mapcar 
                    #'(lambda (xx) 
                        (if (member class-id 
                                    (%%get-value  xx '$type context))
                            xx))
                  id-list))
      ;; include class subtypes
      (remove nil 
              (mapcar 
                  #'(lambda (xx) 
                      (if (%type? xx class-id) 
                          xx)) 
                id-list)))))

#|
;; tests to do in the test package
(with-context 0
  (moss::%filter-against-class 
   '($e-PERSON.2 $E-PERSON.3 $E-STUDENT.1 $E-ORGANIZATION.2) "person"))
($E-PERSON.2)

(with-context 2
  (moss::%filter-against-class 
   '($e-PERSON.2 $E-PERSON.3 $E-STUDENT.1 $E-ORGANIZATION.2) "person"))
($E-PERSON.2 $E-STUDENT.1)

(with-context 2
  (moss::%filter-against-class 
   '($e-PERSON.2 $E-PERSON.3 $E-STUDENT.1 $E-ORGANIZATION.2) "person" 
   :no-subclass t))
($E-PERSON.2)

(moss::%filter-against-class 
   '($e-PERSON.2 $E-PERSON.3 $E-STUDENT.1 $E-ORGANIZATION.2) '(:br "pessoa"))
Warning: can't find the class: (:BR "pessoa") for filtering in context 0 in package
         #<The TEST package>
($E-PERSON.2 $E-PERSON.3 $E-STUDENT.1 $E-ORGANIZATION.2)
|#
;;;A---------------------------------------------------- %FILTER-AGAINST-PACKAGE
;;; probably not very useful JP1004
;;; context not involved

(defUn %filter-against-package (id-list package)
  "filters the list keeping objects with package package. No check on arguments.
Arguments:
   id-list: list of MOSS object ids
   package: Lisp package or keyword
Return:
   a list"
  (with-package package ; check that package is a package
    (remove NIL (mapcar #'(lambda (xx) (if (eql (symbol-package xx) *package*) xx))
                  id-list))))

#| Find those entry points in the ADDRESS environment that come from MOSS
(moss::%filter-against-package (moss::%get-value address::*moss-system* 'moss::$EPLS)
                               (find-package :moss))
(UNIVERSAL-METHOD COUNTER NULL-CLASS UNIVERSAL-CLASS =SUMMARY =MAKE-ENTRY
                  =IF-NEEDED)
|#
;;;A------------------------------------------------------ %FILTER-ALIVE-OBJECTS

(defUn %filter-alive-objects (object-list &optional version)
  "Takes a list of objects and keeps the ones that are alive in the specified ~
   context. Eventually loads the object from disk.
Arguments:
   object-list: list of objects
   version (opt): context (default current)
Return:
   a (possibly empty) list of objects alive in context."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (remove nil (mapcar #'(lambda (xx) (%alive? xx context)) object-list))))

#|
;;; $E-PERSON.7 is dead in context 0
(%filter-alive-objects '($E-PERSON.1 $E-PERSON.3 $E-PERSON.7 $E-STUDENT.1) 0)
($E-PERSON.1)

(moss::%filter-alive-objects '($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1) 2)
($E-PERSON.1 $E-STUDENT.1)
|#
;;;------------------------------------------------------------------- FIND-ANSWER
;;; find-answer processes a message received by an agent. The message must obey the
;;; OMAS CL format. if the message contains a query, then the query is given to
;;; the access function and executed over the agent knowledge base. If the message
;;; contains a list of words (:data option), then the MOSS access-from-words is
;;; executed after removing possible empty words.
;;; The answer in both cases is a list of object keys. When the answer in not empty
;;; then, if the message contains a pattern then information is extracted from
;;; each object to fill the pattern, using the MOSS function fill-pattern, 
;;; otherwise the argument method is applied to each object.
;;; The output of the find-answer function is either NIL (no answer) or a list of
;;; lists describing each extracted object.
;;; find-answer can be used in most skills related to extracting information from
;;; an agent knowledge base.

(defUn find-answer (args &key empty-words special-words no-format filter
                         (format-method '=summary) format-method-args)
  "generic function that uses the input data to a skill to access info in the ~
   agent KB, and format it, using either pattern, if in the data, or ~
   =format-method.
Arguments:
   args: an alist with properties :data/:query :pattern :language
   empty-words (key): empty-word list depending on the language
   special-words (key): special words depending on the skill
   no-format (key): if true return the list of object ids
   filter (key): a 1-arg function to apply to each object to filter it
   format method (key): method to be applied to retrieved object if no pattern
   format-method-args (key): additional arguments for the formatting method
Return:
   a list of formated results to be sent back to caller."
  (let ((*language* (or (cdr (assoc :language args)) *language*))
        (pattern (cdr (assoc :pattern args)))
        (data (cdr (assoc :data args)))
        (query (cdr (assoc :query args)))
        results)
    (cond
     (data
      ;; first locate individuals using the word list and clean empty words
      (setq results (access-from-words data :list-of-empty-word-lists
                                       (list empty-words special-words))))
     (query
      ;; in that case we locate individuals by executing the query
      (setq results (access query))))
    
    (unless results 
      (return-from find-answer nil))
    
    ;; remove duplicates
    (setq results (delete-duplicates results))
    
    ;; if we have a filter function, apply it
    ;;********** ??
    (when filter
      (setq results 
            (remove nil 
                    (mapcar #'(lambda (xx)(funcall filter xx)) results))))
    
    ;; here we have results, return them formatted unless no-format option
    (when no-format (return-from find-answer results))
    
    (if pattern 
        (fill-pattern results pattern)
      ;; otherwise, put get standard address
     (apply #'broadcast results format-method format-method-args))
    ;(format *debug-io* "~&+++ results: ~S" results)
    ))

#| when used in the :address workspace:

(find-answer '((:data "de" "Barth�s")(:language . :fr)
               (:pattern . ("person" ("titre")("nom")
                            ("initiales du pr�nom"))))
             :empty-words *empty-words*
             :special-words *address-words*
             :format-method '=get-address)
(("person" ("titre" "Prof.") ("nom" "Barth�s") ("initiales du pr�nom" "J.-P.")))

(find-answer '((:data "de" "Barth�s")(:language . :fr)
               (:pattern . ("person" ("titre")("nom")("address") 
                            ("initiales du pr�nom"))))
             :empty-words *empty-words*
             :special-words *address-words*
             :format-method '=get-address)
(("person" ("titre" "Prof.") ("nom" "Barth�s")
  ("address" "Centre de Recherche de Royallieu, 60205 Compi�gne cedex, France")
  ("initiales du pr�nom" "J.-P.")))
|#
;;;=============================================================================
;;;                            GET functions
;;;=============================================================================

;;; GET functions are used to recover objects or list of objects

;;;A-------------------------------------------------------- %GET-ALL-ATTRIBUTES
;;; returns all attributes in a given package

(defUn %get-all-attributes ()
  "Gets the list of all attributes in all contexts for *moss-system* in the ~
   current package. No arguments."
  (delete-duplicates 
   (%get-all-versions (symbol-value (intern "*MOSS-SYSTEM*")) '$ETLS)))

#|
(moss::%get-all-attributes)
($REF $ID $TYPE $DEFT $VALT $CNAM $XNB $TMBT $DOCT $ARG $REST $CODT $FNAM $MNAM $UNAM $SNAM $PRFX
 $PKG $SVL $SFL $SML $DFXL $CRET $DTCT $VERT $ENAM $RDX $ONEOF $RFC $DEF $PNAM $MINT $MAXT $VRT $SEL
 $OPR $TPRT $NTPR $INAM $TIT $OBJNAM $VNBT $CFNM $USRNAM $ACRT $CVRS $QNAM $QXPT $QUXT $QANT $STNAM
 $LBLT $XPLT $IDXT $WGHT $TSKNAM $TRGT $PRFT $DLGT $OWS $IWS $AGT $FLT $TXLT $FCT $INTT $WEBT $WGAT
 $WOUT $ARNAM $ARKT $ARVT $ARQT $REFT $CREFT $ANAM $OPRT) 
:DONE
(let ((*package* (find-package :p2)))
  (moss::%get-all-attributes))
(OMAS-P2::$T-SK-NAME OMAS-P2::$T-OMAS-SKILL-SK-NAME OMAS-P2::$T-SK-DOC OMAS-P2::$T-OMAS-SKILL-SK-DOC
 OMAS-P2::$T-GL-NAME OMAS-P2::$T-OMAS-GOAL-GL-NAME OMAS-P2::$T-GL-DOC OMAS-P2::$T-OMAS-GOAL-GL-DOC
 OMAS-P2::$T-AG-NAME OMAS-P2::$T-OMAS-AGENT-AG-NAME OMAS-P2::$T-AG-DOC OMAS-P2::$T-OMAS-AGENT-AG-DOC
 OMAS-P2::$T-AG-KEY OMAS-P2::$T-OMAS-AGENT-AG-KEY OMAS-P2::$T-AG-LANGUAGE
 OMAS-P2::$T-OMAS-AGENT-AG-LANGUAGE OMAS-P2::$T-NAME OMAS-P2::$T-AGENT-GOAL-NAME OMAS-P2::$T-STATE
                     ...
 OMAS-P2::$T-CONDITION-ARGS OMAS-P2::$T-VALUE OMAS-P2::$T-CONDITION-VALUE OMAS-P2::$T-INFORM
OMAS-P2::$T-CONDITION-INFORM OMAS-P2::$T-CONSEQUENCE OMAS-P2::$T-CONDITION-CONSEQUENCE
 OMAS-P2::$T-PERSON-NAME OMAS-P2::$T-LOCATION OMAS-P2::$T-PERSON-LOCATION OMAS-P2::$T-PERSON-STATE
 OMAS-P2::$T-POWER-SOURCE-NAME OMAS-P2::$T-POWER-SOURCE-STATE) 
:DONE
|#
;;;A------------------------------------------------- %%GET-ALL-CLASS-ATTRIBUTES

(defUn %%get-all-class-attributes (class-id)
  "Collects all attributes starting with class-id through superclasses.
   Attributes of the same property tree can be included in the result.
Arguments:
   class-id: identifier of the class
Return:
   a list of class attributes." 
  (delete-duplicates
   (reduce #'append 
           (mapcar #'(lambda(xx) (%get-value xx '$PT))
             (%sp-gamma class-id '$IS-A)))))

#|
(moss::%%get-all-class-attributes '$E-PERSON)
($T-PERSON-NAME $T-PERSON-FIRST-NAME $T-PERSON-INITIALS $T-PERSON-SEX
                $T-PERSON-TITLE $T-PERSON-TRADE $T-PERSON-SHORT-ADDRESS $T-PERSON-LONG-ADDRESS
                $T-PERSON-PHONE $T-PERSON-HOME-PHONE $T-PERSON-OFFICE-PHONE $T-PERSON-CELL-PHONE
                $T-PERSON-EMAIL $T-PERSON-WEB-SITE $T-PERSON-SPECIALITY)
|#
;;;A------------------------------------------ %%GET-ALL-CLASS-INVERSE-RELATIONS

(defUn %%get-all-class-inverse-relations (class-id)
  "Collects all relations starting with class-id through superclasses.
   Relations of the same property tree can be included in the result.
Arguments:
   class-id: identifier of the class
Return:
   a list of class attributes." 
  (mapcar #'%inverse-property-id
    (delete-duplicates
     (reduce #'append 
             (mapcar #'(lambda(xx) (%get-value xx '$SUC.OF))
               (%sp-gamma class-id '$IS-A))))))

#|
(moss::%%get-all-class-inverse-relations '$E-PERSON)
($S-TEACHING-ORGANIZATION-PRESIDENT.OF $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF
                                       $S-NON-PROFIT-ORGANIZATION-EXECUTIVE-COMMITTEE.OF)
|#
;;;A------------------------------------------------- %%GET-ALL-CLASS-PROPERTIES

(defUn %%get-all-class-properties (class-id)
  "Collects all properties starting with class-id through superclasses.
   Relations of the same property tree can be included in the result.
Arguments:
   class-id: identifier of the class
Return:
   a list of class attributes." 
  (delete-duplicates
   (reduce #'append 
           (mapcar #'(lambda(xx) 
                       (append
                        (%get-value xx '$PT)
                        (%get-value xx '$PS)))
             (%sp-gamma class-id '$IS-A)))))

#|
(moss::%%get-all-class-properties '$E-PERSON)
($T-PERSON-NAME $T-PERSON-FIRST-NAME $T-PERSON-INITIALS $T-PERSON-SEX
                $T-PERSON-TITLE $T-PERSON-TRADE $T-PERSON-SHORT-ADDRESS $T-PERSON-LONG-ADDRESS
                $T-PERSON-PHONE $T-PERSON-HOME-PHONE $T-PERSON-OFFICE-PHONE $T-PERSON-CELL-PHONE
                $T-PERSON-EMAIL $T-PERSON-WEB-SITE $T-PERSON-SPECIALITY $S-PERSON-ADDRESS
                $S-PERSON-WORK-PLACE $S-PERSON-EMPLOYER)
|#
;;;A-------------------------------------------------- %%GET-ALL-CLASS-RELATIONS

(defUn %%get-all-class-relations (class-id)
  "Collects all relations starting with class-id through superclasses.
   Relations of the same property tree can be included in the result.
Arguments:
   class-id: identifier of the class
Return:
   a list of class attributes." 
  (delete-duplicates
   (reduce #'append 
           (mapcar #'(lambda(xx) (%get-value xx '$PS))
             (%sp-gamma class-id '$IS-A)))))

#|
? (moss::%%get-all-class-relations '$E-PERSON)
($S-PERSON-ADDRESS $S-PERSON-WORK-PLACE $S-PERSON-EMPLOYER)
|#
;;;A---------------------------------------------------------- %GET-ALL-CONCEPTS

(defUn %get-all-concepts ()
  "Gets the list of all concepts in all contexts. No arguments."
  (delete-duplicates 
   (%get-all-versions (symbol-value (intern "*MOSS-SYSTEM*")) '$ENLS)))

#| MOSS
(moss::%get-all-concepts)
($CTR $FN $UNI $SYS $ENT $VENT $EPR $EPS $EPT $EP $EIL *NONE* *ANY* $DOCE $ENDCE $FRDCE $CNFG $USR
 $QHDR $QRY $QSTE $TIDXE $QTSKE $QHDE $CVSE $TARGE $OBRGE $ACTE $E-PERSON)

(with-package :test (%get-all-concepts))
(TEST::$SYS TEST::$FN TEST::$UNI TEST::$CTR TEST::*NONE* TEST::*ANY* TEST::$E-OMAS-SKILL
 TEST::$E-OMAS-GOAL TEST::$E-OMAS-AGENT TEST::$E-PERSON TEST::$E-STUDENT)
|#
;;;A------------------------------------------------------ %GET-ALL-ENTRY-POINTS

(defUn %get-all-entry-points ()
  "Gets the list of all entry-points in all contexts. No arguments."
  (delete-duplicates 
   (%get-all-versions (symbol-value (intern "*MOSS-SYSTEM*")) '$EPLS)))

#|
(moss::%get-all-entry-points)
(SA_ADDRESS-MOSS 
 SA_ADDRESS-MOSS-SYSTEM METHOD UNIVERSAL-METHOD COUNTER NULL-CLASS
 UNIVERSAL-CLASS OMAS-SKILL HAS-SK-NAME IS-SK-NAME-OF HAS-SK-DOC IS-SK-DOC-OF
 OMAS-GOAL HAS-GL-NAME IS-GL-NAME-OF HAS-GL-DOC IS-GL-DOC-OF OMAS-AGENT HAS-AG-NAME
 ...
 D4S MRIS MISSION-POUR-LA-RECHERCHE-ET-L\'INNOVATION-SCIENTIFIQUE)
|#
;;;A--------------------------------------------------------- %GET-ALL-INSTANCES
;;; we could insist that the package for the instances be the same as the package
;;; of the class-id

(defUn %get-all-instances (class-id &optional version)
  "Gets the list of all instances of a class in all contexts. We assume that ~
   counter is in context 0.
   The default behavior is that the package for the instances is the same as ~
   the package of the class-id symbol. Otherwise one should know what ~
   one does...
Arguments:
   class-id: id of the class
   version (opt): context (default current)
return:
   list of all bound identifiers of the instances of the class, including ideal.
   if version is :all includes all instances, even dead objects"
  (let*((result (list (%make-id-for-ideal class-id)))
        (counter (car (%%get-value class-id '$CTRS 0)))
        (context (or version (symbol-value (intern "*CONTEXT*"))))
        count 
        )
    ;; if counter is missing no warning return (system classes have no counter)
    (unless counter
      ;(warn "Missing counter for class: ~S" class-id)
      (return-from %get-all-instances nil))
    
    ;; get value of counter
    (setq count (car (%%get-value counter `$VALT 0)))
    (format t "~%; %get-all-instances /count: ~S" count)
    (unless (integerp count)
      (warn "Bad value (~S) for counter for class: ~S in version ~S in package ~S"
        count class-id context *package*)
      (return-from %get-all-instances nil))
    
    ;; return the required list, loading if necessary
    (delete-if  
     #'(lambda (xx) (or (not (%pdm? xx))
                        ;; we keep all objects if context is :all
                        (and (not (eql context :all))
                             (not (%alive? xx context)))))
     (append result
             (loop for cc from 1 to (1- count)
                 collect (%make-id-for-instance class-id cc))))))

#| In the test space
;; in context 0
(moss::%get-all-instances '$E-person 0)
($E-PERSON.0 $E-PERSON.1 $E-PERSON.2)

;; in context 2
(moss::%get-all-instances '$E-person 2)
($E-PERSON.0 $E-PERSON.1 $E-PERSON.2 $E-PERSON.4 $E-PERSON.5)

(moss::%get-all-instances '$E-student 5)
($E-STUDENT.0 $E-STUDENT.2)

(moss::%get-all-instances '$e-person :all)
($E-PERSON.0 $E-PERSON.1 $E-PERSON.2 $E-PERSON.3 $E-PERSON.4 $E-PERSON.5 
             $E-PERSON.6 $E-PERSON.7)
;; $E-PERSON.7 is a dead object
|#
;;;A------------------------------------------------- %GET-ALL-INVERSE-RELATIONS

(defUn %get-all-inverse-relations ()
  "Gets the list of all inverse relations in all contexts. No arguments."
  (delete-duplicates 
   (%get-all-versions 
    (symbol-value (intern "*MOSS-SYSTEM*"))
    '$EILS)))

#| In the address space
? (MOSS::%GET-ALL-INVERSE-RELATIONS)
($T-SK-NAME.OF $T-OMAS-SKILL-SK-NAME.OF $T-SK-DOC.OF $T-OMAS-SKILL-SK-DOC.OF
               $T-GL-NAME.OF $T-OMAS-GOAL-GL-NAME.OF $T-GL-DOC.OF $T-OMAS-GOAL-GL-DOC.OF
               ...
               $S-TOWN-COUNTRY.OF $S-DISTRICT.OF $S-TOWN-DISTRICT.OF)
|#
;;;A----------------------------------------------------------- %GET-ALL-METHODS

(defUn %get-all-methods ()
  "Gets the list of all methods in all contexts. No arguments."
  (delete-duplicates 
   (%get-all-versions 
    (symbol-value (intern "*MOSS-SYSTEM*"))
    '$FNLS)))

#|
? (MOSS::%GET-ALL-METHODS)
($E-FN.1 $E-FN.2 $E-FN.3 $E-FN.4 $E-FN.5 $E-FN.6 $E-FN.7 $E-FN.8 $E-FN.9 $E-FN.10
         $E-FN.11 $E-FN.12 $E-FN.13 $E-FN.14 $E-FN.15 $E-FN.16 $E-FN.17 $E-FN.18 $E-FN.19
         $E-FN.20 $E-FN.21 $E-FN.22 $E-FN.23 $E-FN.24 $E-FN.25 $E-FN.26 $E-FN.27 $E-FN.28
         $E-FN.29 $E-FN.30 $E-FN.31 $E-FN.32 $E-FN.33 $E-FN.34 $E-FN.35 $E-FN.36 $E-FN.37
         $E-FN.38 $E-FN.39 $E-FN.40 $E-FN.41)
|#
;;;A------------------------------------------------------ %GET-ALL-MOSS-OBJECTS

(defUn %get-all-moss-objects ()
  "Gets the list of all MOSS objects in all contexts. No arguments."
  (let ((classes (%get-all-concepts))
        result)
    (delete-duplicates
     (append
      (%get-all-attributes)
      (%get-all-concepts)
      (%get-all-entry-points)
      (%get-all-inverse-relations)
      (%get-all-orphans)
      (%get-all-relations)
      (%get-all-symbols)
      (dolist (class-id classes result)
        (setq result 
              (append (%get-all-instances class-id :all)
                      result)))))))

#|
;; in the test package
(moss::%get-all-moss-objects)
($T-SK-NAME $T-OMAS-SKILL-SK-NAME $T-SK-DOC $T-OMAS-SKILL-SK-DOC $T-GL-NAME
            $T-OMAS-GOAL-GL-NAME $T-GL-DOC $T-OMAS-GOAL-GL-DOC $T-AG-NAME $T-OMAS-AGENT-AG-NAME
            ...
             $FN.4 $FN.6 $FN.7 $FN.8 $SYS.0 $SYS.1)
|#
;;;A----------------------------------------------------------- %GET-ALL-ORPHANS
;;; orphans have an ideal that could be used for God knows what...

(defUn %get-all-orphans (&key (no-ideal t))
  "Gets the list of all orphans in all contexts. No arguments."
  (let ((result (moss::%get-all-instances (intern "*NONE*") :all)))
    (if no-ideal (cdr result) result)))

#|
(defobject ("name" "Zoe"))
$ORPHAN.1
;; ***** BUG: does not increase *NONE*.CTR *****

$ORPHAN.1
((MOSS::$TYPE (0 *NONE*)) (MOSS::$ID (0 $ORPHAN.1)) ($T-NAME (0 "Zoe")))

(moss::%get-all-orphans)
($ORPHAN.1)
|#
;;;A--------------------------------------------------------- %GET-ALL-RELATIONS

(defUn %get-all-relations ()
  "Gets the list of all relations in all contexts. No arguments."
  (delete-duplicates 
   (%get-all-versions 
    (symbol-value (intern "*MOSS-SYSTEM*"))
    '$ESLS)))

#|
(moss::%get-all-relations)
($S-AG-SKILL $S-OMAS-AGENT-AG-SKILL $S-AG-GOAL $S-OMAS-AGENT-AG-GOAL $S-TOWN
             $S-POSTAL-ADDRESS-TOWN $S-COUNTRY $S-POSTAL-ADDRESS-COUNTRY $S-ADDRESS
             ...
             $S-STATE $S-TOWN-STATE $S-TOWN-COUNTRY $S-DISTRICT $S-TOWN-DISTRICT)
|#
;;;A--------------------------------------- %%GET-ALL-SUBCLASS-INVERSE-RELATIONS

(defUn %%get-all-subclass-inverse-relations (class-id)
  "Collects all properties starting with class-id through superclasses.
   Relations of the same property tree can be included in the result.
Arguments:
   class-id: identifier of the class
Return:
   a list of class attributes." 
  (delete-duplicates
   (reduce #'append 
           (mapcar #'(lambda(xx) 
                       (%get-value xx (%inverse-property-id '$SUC)))
             (%sp-gamma class-id (%inverse-property-id '$IS-A))))))

#|
(moss::%%get-all-subclass-inverse-relations '$E-PERSON)
($S-TEACHING-ORGANIZATION-PRESIDENT $S-NON-PROFIT-ORGANIZATION-PRESIDENT
                                    $S-NON-PROFIT-ORGANIZATION-EXECUTIVE-COMMITTEE)
|#
;;;A----------------------------------------------- %%GET-ALL-SUBCLASS-RELATIONS

(defUn %%get-all-subclass-relations (class-id)
  "Collects all relations starting with class-id through subclasses.
   Relations of the same property tree can be included in the result.
Arguments:
   class-id: identifier of the class
Return:
   a list of class attributes." 
  (delete-duplicates
   (reduce #'append 
           (mapcar #'(lambda(xx) (%get-value xx '$PS))
             (%sp-gamma class-id (%inverse-property-id'$IS-A))))))

#|
(moss::%%get-all-subclass-relations '$E-PERSON)
($S-PERSON-ADDRESS $S-PERSON-WORK-PLACE $S-PERSON-EMPLOYER $S-STUDENT-UNIVERSITY)

(moss::%%get-all-subclass-relations '$E-STUDENT)
($S-STUDENT-UNIVERSITY)
|#
;;;A----------------------------------------------------------- %GET-ALL-SYMBOLS
;;; Is it really useful ? JPB 1003 used by %reset

(defUn %get-all-symbols ()
  "Gets the list of all symbols in all contexts (counters, global variables,...~
   No arguments."
  (delete-duplicates 
   (%get-all-versions 
    (symbol-value (intern "*MOSS-SYSTEM*")) 
    '$SVL)))

#|
(MOSS::%GET-ALL-SYMBOLS)
(MOSS::$UNI.CTR MOSS::$SYS.CTR MOSS::$ENT.CTR MOSS::$EPR.CTR MOSS::$EPS.CTR
                MOSS::$EPT.CTR MOSS::$EP.CTR MOSS::$EIL.CTR MOSS::*NONE*.CTR MOSS::*ANY*.CTR
                MOSS::$DOCE.CTR MOSS::$ENDCE.CTR MOSS::$FRDCE.CTR MOSS::$CNFG.CTR MOSS::$USR.CTR
                MOSS::*LISP-FLAG* MOSS::*TRACE-MESSAGE* MOSS::*TRACE-FLAG* *TRACE-LEVEL* *SELF*
                MOSS::$QHDR.CTR MOSS::$QRY.CTR $E-GEOGRAPHICAL-AREA.CTR $E-RURAL-AREA.CTR
                $E-TERRITORY.CTR $E-CITY.CTR $E-CONURBATION.CTR $E-COUNTRY.CTR $E-COUNTY.CTR
                $E-FRENCH-DEPARTMENT.CTR $E-DISTRICT.CTR $E-REGION.CTR $E-SUBURB.CTR
                ...)
|#
;;;A---------------------------------------------------------- %GET-ALL-VERSIONS
;;; used by several %get-all functions

(defUn %get-all-versions (obj-id prop-id)
  "Returns all versions of values or of successors associated with the given ~
   property prop-id. Removes duplicates of the values corresponding to the ~
   various versions.
   The function is used when trying to get all objects of a kind from the system, ~
   e.g. by %get-all-symbols.
Arguments:
   obj-id: object identifier
   prop-id: property id for which we want all values
Return:
   a list of all values regardless of versions."
  (setq obj-id (%resolve obj-id))
  (remove-duplicates
   (reduce #'append 
           (mapcar #'(lambda(xx)(cdr xx))
             (getv prop-id (symbol-value obj-id)))) ; all values flattened
   :test #'equal))

#|
(MOSS::%get-all-versions '$E-PERSON.2 '$S-PERSON-WORK-PLACE)
($E-WORK-PLACE.2)
|#
;;;A------------------------------------------------- %GET-AND-INCREMENT-COUNTER

(defUn %get-and-increment-counter (obj-id &optional (counter-link-id '$CTRS))
  "Obtains the value of a counter associated with obj-id and increases this value ~
   in the counter itself. Throws to :error if obj-id is invalid. Loads counter ~
   if needed. Counters are created in context 0.
Arguments
   obj-id: id of the object that owns the counter (ususally a class)
   counter-link-id (opt): name of the property linking to the counter (default $CTRS)
Return:
   old value of the counter."
  ;(format t "~%;---------- %get-and-increment-counter")
  ;(format t "~%;--- 1 (sv (intern...)): ~S" (symbol-value (intern "*CONTEXT*")))
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    ;(format t "~%;--- context: ~S:" context)
    (setq obj-id (%%alive? obj-id context))
    
    (unless obj-id
      (verbose-throw :error "object ~S is invalid in context ~S in package ~S"
                     obj-id context *package*))
    
    (let ((counter (car (%get-value obj-id counter-link-id context)))
          val)
      
      (unless counter 
        (error "object ~s has no counter link ~S in context ~S in package ~S"
          obj-id counter-link-id context *package*))
      
      ;; counter is defind in the same context as the corresponding class
      (unless (%alive? counter context)
        (error "counter ~S must be defined in context 0 in package ~S" 
          counter *package*))
      
      ;; otherwise get value get it from context 0
      (setq val (car (%get-value counter '$VALT 0)))
      ;(format t "~%;---------- counter: ~S, val: ~S" counter val)
      (unless (numberp val)
        (error "bad value for counter ~S associated with object ~S in package ~S ~
                and context ~S."
          counter obj-id *package* context))
      ;; when editing
      (save-old-value counter)
      ;(format t "~%;--- 1b (sv (intern...)): ~S" (symbol-value (intern "*CONTEXT*")))
      (%%set-value counter (1+ val) '$VALT 0)
      ;(format t "~%;---------- counter: ~S~%~S" counter (symbol-value counter))
      ;(format t "~%;--- 2 (sv (intern...)): ~S" (symbol-value (intern "*CONTEXT*")))
      ;(format t "~%;----------- End %get-and-increment-counter")
      ;; return old value
      val)))

#|
$E-PERSON.CTR
((MOSS::$TYPE (0 MOSS::$CTR)) (MOSS::$VALT (0 23)) (MOSS::$CTRS.OF (0 $E-PERSON)))

(makunbound '$E-PERSON.CTR)
$E-PERSON.CTR

(moss::%get-and-increment-counter '$E-PERSON)
23

$E-PERSON.CTR
((MOSS::$TYPE (0 MOSS::$CTR)) (MOSS::$VALT (0 24)) (MOSS::$CTRS.OF (0 $E-PERSON)))
|#
;;;+--------------------------------------------------- %GET-APPLICATION-CLASSES
;;; the concept of application has been removed from MOSS
;;; now it is the same as %get-all-concepts

(defUn %get-application-classes ()
  "returns the list of classes of a given application, removing *none* and *any*  ~
   $E-FN, $E-SYS, $E-UNI and $E-CTR.
Arguments:
   context (opt): default is *context*
Returns:
   a list of class ids."  
  (set-difference (%get-all-concepts) 
                  (list (intern "*NONE*") (intern "*ANY*") (intern "$E-FN")
                        (intern "$E-CTR")(intern "$E-SYS")(intern "$E-UNI"))))


#| in the test package
(moss::%get-application-classes)
($E-STUDENT $E-PERSON $E-ORGANIZATION $E-OMAS-AGENT $E-OMAS-GOAL $E-OMAS-SKILL $CTR $UNI $FN
 $SYS)
|#
;;;+---------------------------------------------------------------------- %GETC

(defUn %getc (obj-id prop-id context)
  "Retrieves the value associated with prop-id for the specified context from ~
   the p-list of obj-id used as a cache. If the context is illegal, then throws ~
   to an :error tag.
Arguments:
   obj-id: identifier of object
   prop-id: identifier of local property
   context: context
Returns:
   nil or the cached value, Throws to :error when context is illegal or object dead."
  ;; check for illegal context done in %%alive?. If illegal throws to :error
  ;; if object was saved on disk p-list is empty
  (setq obj-id (%%alive? obj-id context))
  (getv context (get obj-id prop-id)))

#|
should be checked
|#
;;;A----------------------------------------- %GET-CLASS-CANONICAL-NAME-FROM-REF
;;; should be changed

(defUn %get-class-canonical-name-from-ref (ref)
  "uses ref as an entry point to the property. If it fails return nil. Otherwise, ~
   returns the canonical name in the current language.
Arguments:
   ref: e.g. \"country\"
Return:
   canonical name string in the current language specified by *language*."
  ;; we first need to get the class-id from the current ref
  (let ((class-id (%%get-id ref :class)))
    (when class-id
      (mln::get-canonical-name (car (%get-value class-id '$ENAM))))))

#|
(with-package :address (%get-class-canonical-name-from-ref "person"))
"personne"
:FR

(with-package :test
    (%get-class-canonical-name-from-ref '(:en "person" :fr "personne")))
"personne"
:FR

(with-package :test (%get-class-canonical-name-from-ref 'PERSON))
"personne"
:FR
|#
;;;------------------------------------------------------ %GET-CLASS-ID-FROM-REF
;;; deprecated. Use (%%get-id class-ref :class)

;;;A--------------------------------------------------------- %GET-CLASS-COUNTER

(defUn %get-class-counter (obj-id &optional (counter-link-id '$CTRS))
  "Obtains the value of a counter associated with obj-id and increases this value ~
   in the counter itself. Throws to :error if obj-id is invalid. Loads counter ~
   if needed.
Arguments
   obj-id: id of the object that owns the counter (usually a class)
   counter-link-id (opt): name of the property linking to the counter (default $CTRS)
Return:
   old value of the counter."
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    (setq obj-id (%%alive? obj-id context))
    (unless obj-id
      (verbose-throw :error "object ~S is invalid in context ~Sin package ~S"
                     obj-id context *package*))
    
    (let ((counter (car (%get-value obj-id counter-link-id)))
          value)
      (unless counter 
        (error "object ~s has no counter link ~S in context ~S in package ~S"
          obj-id counter-link-id context *package*))
      
      ;; otherwise get value
      (setq value (car (%get-value counter '$VALT)))
      (unless (numberp value)
        (error "bad value for counter ~S of object ~S in context ~S in package ~S"
          counter obj-id context *package*))
      ;; return old value
      value)))

#|
(with-package :test
  (moss::%get-class-counter 'test::$E-PERSON))
4
|#
;;;A----------------------------------------------------------- GET-CURRENT-DATE
;;; making the output format nicer

(defUn get-current-date (&key compact time) 
  (multiple-value-bind (xx xx xx day month year)
      (if time
          (decode-universal-time time)
        (get-decoded-time))      
    (declare (ignore XX))
    (if compact
        (format nil "~2,'0D~2,'0D~2,'0D" (mod year 100) month day)
      (format nil "~2,'0D/~2,'0D/~2,'0D" day month year))))

#|
(get-current-date)
"16/2/2010"

(get-current-date :compact t)
"101213"

(moss::get-current-date :time (get-universal-time))
"23/07/2011"
|#
;;;A----------------------------------------------------------- GET-CURRENT-YEAR

(defUn get-current-year (&key time)
  "get the current year from the system clock."
  (multiple-value-bind (hour minutes seconds day month year)
      (if time
          (decode-universal-time time)
        (get-decoded-time))
    (declare (ignore hour minutes seconds day month))
    year))

#|
(get-current-year)
2006
|#
;;;A---------------------------------------------------- %GET-DEFAULT-FROM-CLASS

(defUn %get-default-from-class (prop-id class-id)
  "gets the default value-list associated with a property in a given class. ~
   The way to do it is to ask the property itself; then the ideal of the class.
   Defaults cannot be inherited.
Arguments:
   prop-id: id of corresponding property
   class-id: id of the class
Return:
   defaul value or nil."
  (when (and (or (%is-terminal-property? prop-id)
                 (%is-relation? prop-id))
             (%is-concept? class-id))
    (catch :result
           (unless (or (eql class-id (intern "*ANY*"))
                       (eql class-id (intern "*NONE*")))
             (let ((ideal-id (%make-id-for-ideal class-id))
                   default)
               ;(%ldif ideal-id)
               ;; %get-value loads the object if needed
               (setq default (%get-value ideal-id prop-id))
               (if default (throw :result default))))
           ;; otherwise, ask the property itself
           (%get-value prop-id '$DEFT))))

#|
(defconcept "test-a" (:att "att-a1" (:default "default-att-a1")))
$E-TEST-A
(($TYPE (0 $ENT)) ($ID (0 $E-TEST-A)) ($ENAM (0 ((:EN "test-a")))) 
 ($RDX (0 $E-TEST-A))
 ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $E-TEST-A.CTR)) ($PT (0 $T-TEST-A-ATT-A1)))

(moss::%get-default-from-class '$T-TEST-A-ATT-A1 '$E-TEST-A)
("default-att-a1")

Versions:
(with-context Z
  (defconcept "test-b" (:att "att-b1" (:default "default-att-b1"))))
$E-TEST-B

(with-context 3
  (moss::%get-default-from-class '$T-TEST-B-ATT-B1 '$E-TEST-B))
("default-att-b1")
|#
;;;------------------------------------------------------------ %GET-EMPTY-WORDS
;********** to check... Does not work if empty-word class not defined

;;; maybe should do that differently

(defUn %get-empty-words (language)
  "recover a list of empty words in specified language. This assumes ~
   that a class empty-words exists and has been initialized.
Arguments:
   language: a string or keyword
Return:
   a list of empty words or nil."
  (let ((empty-word-object 
         (access `("empty words" ("language" :is ,(string+ language))))))
    ;; if non nil get list
    (if empty-word-object
        (send (car empty-word-object) '=get "words"))))

#|
(moss::%get-empty-words :FR)
; Warning: Expr is not an entry point nor a query (#<Package "MOSS">):
;            ("empty words" ("language" :IS ":FR")).
; While executing: PARSE-USER-QUERY
NIL
|#
;;;A---------------------------------------------------------- %GET-ENTRY-POINT
;;; try to get an entry point. Anyone will do. When the object has several
;;; values for a property with entry point, takes the first one

(defun %get-entry-point (obj-id &key language)
  "try to find an entry point for the specified object (must be an instance). ~
   If language is specified, checks of a value is an mln, in which case extracts ~
   the value corresponding to the language.
Argument:
   obj-id: id of the specific object
   language (key): e.g. :FR, :BR, :EN, ...
Return:
   list of entry-point symbols, or NIL if one could not be found or if object ~
   is dead."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        type-list attribute-list val fn)
    
    (when (%alive? obj-id context)
      ;; get the list of type classes
      (setq type-list (moss::%%get-value obj-id 'moss::$TYPE context))
      ;; for each class get the list of attributes
      (dolist (class-id type-list)
        (setq attribute-list 
              (append (moss::%%get-all-class-attributes class-id)
                      attribute-list)))
      ;; remove duplicates
      (setq attribute-list (remove-duplicates attribute-list))
      ;; for each attribute
      (dolist (att-id attribute-list)
        ;; check if the object has a value for this attribute
        (when (setq val (moss::%get-value obj-id att-id))
          ;; OK we got a list of values check if the property is an entry point
          (setq  fn (moss::%%lex-get-own-method att-id '=make-entry context))
          (when fn
            ;; If so check if some values are MLN and if language is specified
            ;; extracting the part corresponding to the specified language from MLN
            (when language
              (setq val 
                    (mapcar #'(lambda (xx) 
                                (if (mln::mln? xx)  ; jpb 1406
                                    (mln::extract xx :language language :always t) ; jpb 1406
                                  xx))
                      val)))
            ;; if so apply it to first value and return symbol list
            (return-from %get-entry-point (funcall fn (car val))))))
      ;; when nothing is found return nil
      )))

#|
(moss::%get-entry-point '$E-country.5)
(GERMANY ALLEMAGNE)

(moss::%get-entry-point '$E-country.5 :language :en)
(GERMANY)

(moss::%get-entry-point '$E-country.5 :language :fr)
(ALLEMAGNE)

(moss::%get-entry-point '$E-country.5 :language :br)
(GERMANY) ; :br unspecified, default is English

(moss::%get-entry-point '$E-financing.7)
(FONDS-FRANCE-CANADA-POUR-LA-RECHERCHE-2013)

(with-package :test
  (moss::%get-entry-point 'test::$E-person.1))
(TEST::EINSTEIN)

(with-package :test
  (with-context 2
    (moss::%get-entry-point 'test::$E-person.3)))
NIL

(with-package :test
  (with-context 4
    (moss::%get-entry-point 'test::$E-person.3)))
(TEST::BARTH�S)
|#
;;;------------------------------------------------- %GET-ENTRY-POINT-IF-UNIQUE
;;; try to get a unique entry point. Anyone will do. When the object has several
;;; values for a property with unique entry point, takes the first one

(defun %get-entry-point-if-unique (obj-id &key language)
  "try to find a unique entry point for the specified object (must be an instance). ~
   If language is specified, checks of a value is an mln, in which case extracts ~
   the value corresponding to the language.
Argument:
   obj-id: id of the specific object
   language (key): e.g. :FR, :BR, :EN, ...
Return:
   list of entry-point symbols, or NIL if one could not be found or if object ~
   is dead."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        type-list attribute-list ep val-list fn targets)
    
    (when (%alive? obj-id context)
      ;; get the list of type classes
      (setq type-list (moss::%%get-value obj-id 'moss::$TYPE context))
      ;; for each class get the list of attributes
      (dolist (class-id type-list)
        (setq attribute-list 
              (append (%%get-all-class-attributes class-id)
                      attribute-list)))
      ;; remove duplicates
      (setq attribute-list (remove-duplicates attribute-list))
      ;; for each attribute
      (dolist (att-id attribute-list)
        ;; check if the object has a value for this attribute
        (when (setq val-list (%get-value obj-id att-id))
          ;; OK we got a list of values check if the property is an entry point
          (setq fn (%%lex-get-own-method att-id '=make-entry context))
          (when fn
            ;; If so check if some values are MLN and if language is specified
            ;; extracting the part corresponding to the specified language from MLN
            (when language
              (setq val-list 
                    (mapcar #'(lambda (xx) 
                                (if (mln::mln? xx)  ; jpb 1406
                                    (mln::extract-to-string xx :language language
                                                            :always t) ; jpb 1406
                                  xx))
                      val-list)))
            ;; for each value compute the corresponding entry point
            (dolist (val val-list)
              ;;***** we assume here that the =make-entry function returns a single ep
              (setq ep (car (funcall fn val)))
              ;; check if it exists
              (%ldif ep)
              (when (%pdm? ep)
                ;; check if it uniquely determines instance
                (setq targets (%get-value ep (%inverse-property-id att-id)))
                ;; check if only one value
                (unless (cdr targets)
                  ;; if so apply it to first value and return symbol list
                  (return-from %get-entry-point-if-unique ep)))))))
      ;; when nothing is found return nil
      )))

#|
(moss::%get-entry-point-if-unique '$e-country.2 :language :FR)
AFRIQUE-DU-SUD

;; in test package
(moss::%get-entry-point-if-unique '$e-person.2 :language :EN)
BARTH�S-BIESEL
|#
;;;----------------------------------------------------------- %%GET-EP-FROM-REF
;;; deprecated. use (%%get-id ref :ep)

;;;-------------------------------------------------------- %%GET-EP-FROM-STRING
;;; deprecated. use (%%get-id ref :ep)

;;;A------------------------------------------------------ %GET-GENERIC-PROPERTY

(defUn %get-generic-property (prop-list &key context)
  "We start with a set of properties having the same name and try to obtain the ~
   one that is generic, i.e. that does not have an ancestor (no $IS-A property).
Argument:
   prop-list: list of property identifiers
   context (key): contexte
Return:
   the identifier of the generic property or nil. Warns if there are more than one
   left."
  ;; when no context argument, set it to *context* in current package
  (unless context (setq context (symbol-value (intern "*CONTEXT*"))))
  
  (when prop-list
    (let (gen-list result)
      ;; first we look for possible generic properties of each property in the list
      ;; just in case it is not part of original list (useful in multilingual
      ;; environments) jpb0101
      (dolist (prop-id prop-list)
        (setq gen-list (%%get-value (%ldif prop-id) '$is-a context))
        (setq result (append gen-list result)))
      (setq prop-list (delete-duplicates (append prop-list result)))
      ;(format t "~%;--- prop-list: ~S" prop-list)
      (when prop-list 
        (let (lres)
          (setq lres 
                (remove nil  
                        (mapcar 
                            #'(lambda (xx) (unless (%get-value xx '$IS-A) xx))
                          prop-list)))
          ;(format t "~%;--- lres: ~S" lres)
          (when (cdr lres)
            (warn "%get-generic-property; we found several properties that could be ~
                   generic for ~A:~%;  ~S.~%; We return the first one." 
              (car (%get-value (car lres) '$PNAM)) lres))
          ;; in all cases we return the first property of the list (possibly nil)
          (car lres))))))

#|
(with-package :test
  (moss::%get-generic-property nil))
NIL

(with-package :test
  (moss::%get-generic-property '(test::$T-PERSON-NAME test::$T-NAME)))
TEST::$T-NAME

(with-package :test
  (moss::%get-generic-property '(test::$T-PERSON-NAME)))
TEST::$T-NAME

(with-package :test
  (with-context 3
    (moss::%get-generic-property '(test::$T-PERSON-NAME))))
TEST::$T-NAME
|#
;;;--------------------------------------------- %GET-GENERIC-PROPERTY-FROM-NAME
;;; deprecated. Replaced by (%%get-id ref :prop)

;;;---------------------------------------------- %GET-GENERIC-PROPERTY-FROM-REF
;;; deprecated. Replaced by (%%get-id ref :prop)

;;;=============================================================================
;;;                               %%GET-ID
;;;=============================================================================

;;; %%GET-ID is a major function to access an object id quickly. It could be
;;; replaced by %extract. However, %extract does a much harder work.

;;;A-------------------------------------------------------------------- %GET-ID
;;; one of the problems of get id is that in a multilingual environment the
;;; id of the class may not correspond to its name, e.g. PERSON and PERSONNE
;;; are entry points to $E-PERSON, however $E-PERSONNE does not exist. It could
;;; be reversed if the concept had been created in a French environment.
;;; Thus, to get the actual class id, one must first build the name, e.g. PERSON
;;; or PERSONNE and then access the class from the name using inverse $ENAM,
;;; since both names are entry points.
;;; It is the same for properties
;;;
;;; %%get-id works for classes, properties, virtual classes, counters, ideals, or
;;; inverse properties.
;;; inverse-properties for a class are the set of all properties that may point
;;; to an instance of the class. The set may contain more than one property. For
;;; example if we consider the inverse of "president", a person may be president 
;;; of a company, president of a club, president of a non-profit organization, etc.
;;; Thus, using %%get-id on inverse properties may return a list of several IDs
;;; This is not the case however, when the inverse property is that of an attribute
;;; e.g. "name" when the attribute has an entry point. E.g.
;;;   (%%get-id "name" :inv :class-ref "Student") => $T-PERSON-NAME
;;;
;;; Finally, %%get-id tries to return objects from the current *package*. However,
;;; if there is no result, the :include-moss option allows to extend it to the
;;; MOSS package. Thus, the object in a specific package shadows the same object
;;; from the MOSS package.

;;; we could have a problem if an attribute and a relation share the same name
;;; when using :property as type-key...

(defUn %%get-id (ref type-key &key class-ref from (include-moss t))
  "get the id of the object of type type-key corresponding to ref and class-ref ~
   if it is a property. It is recommended to use this function only for system ~
   objects like concepts, or properties or virtual concepts...
   If the object is unbound or nil, tries to load it from disk. If it does not ~
   exist and include-moss is nil, returns nil. Otherwise, tries to get if from ~
   the MOSS package.
Arguments:
   ref: an object reference (symbol, string, mln)
   type-key: a keyword (:class, :concept, :attribute, :tp, :relation, etc.)
             or an application ref
   class-ref (key): a class reference for the case where type is a property
                    if not there for a property builds generic id
   from (key): used in conjunction with :inv-from to filter the list of possible ~
               inverse properties, indicates the class where the inverse property ~
               originates
   include-moss (key): if present, if no id is found in current package, try moss
Return:
   the symbol representing the object id or nil, if the object does not exist ~
   in the package. Does not check its validity, nor load it from disk.
   in case of problem throws :error to :error"
  ;(format t "~%; %%get-id /*package*: ~S" *package*)
  
  (case type-key
    ((:class :concept)
     (%%get-id-for-class ref :include-moss include-moss))
    
    ((:virtual-class :vc)
     (%%get-id-for-virtual-class ref :include-moss include-moss))
    
    ((:attribute :tp :relation :sp :property :prop)
     (%%get-id-for-property ref type-key :class-ref class-ref
                            :include-moss include-moss))
    (:counter
     (%%get-id-for-counter ref :include-moss include-moss))
    
    (:ep
     ;; entry points are cross-package
     (%%get-id-for-ep ref :include-moss include-moss))
    
    (:ideal
     ;; system concept have no ideals
     (%%get-id-for-ideal ref))
    
    ;; inverse properties corresponding to a name like IS-PRESIDENT-OF may not
    ;; be unique and gather all possibilities ranging over the classes from
    ;; the specifie class to the class at the root of the inheritance hierarchy
    ;; thus, using the function to resolve an inverse property can be tricky
    ((:inverse-property :inv)
     (%%get-id-for-inverse-property ref :class-ref class-ref))
    
    ;; here we try to recover the property that points to the ref class when knowing
    ;; from where it comes from (class-ref and from are compulsory args)
    ;; e.g. the property ">president" for the class "Person" can be filtered down to
    ;; $S-ORGANIZATION-PRESIDENT.OF if we know that it comes from "Organization"
    ;; (moss::%%get-id ">president" :inv-from :class-ref "person" :from "company")
    (:inv-from
     (%%get-id-for-inverse-property-knowing-origin ref class-ref from))
    
    ;; here illegal type-key
    (t 
     (verbose-throw
      :error
      "error in type-ref (~S) while trying to recover id of ~S of class ~S (opt),~
       allowed values are :concept, :class, :virtual-class, ~
       :relation, :attribute, :inverse-property, :ideal
       package is ~S" 
      type-key ref class-ref *package*)
     )))

#|
(with-package :test
     (%%get-id "person" :concept))
TEST::$E-PERSON

(with-package :TEST
     (%%get-id "personne" :concept)) ; testing ref in different language
NIL

(with-package :moss
    (%%get-id "person" :concept))
NIL

(with-package :test
     (%%get-id '(:en "person; guy" :fr "personne; individu") :concept))
TEST::$E-PERSON

(with-package :test
     (%%get-id 'test::$E-person :concept))
TEST::$E-PERSON

(let ((*language* :en))
    (with-package :test
       (%%get-id '(:en "person; guy" :fr "personne; individu") :concept)))
TEST::$E-PERSON

(let ((*language* :en))
    (with-package :moss
      (%%get-id '(:en "person; guy" :fr "personne; individu") :concept)))
NIL

(with-package :test
     (%%get-id 'NAME :attribute :class-ref 'person))
TEST::$T-PERSON-NAME

(with-package :test
     (%%get-id 'NAME :sp :class-ref 'person))
NIL

(with-package :test
     (%%get-id 'NAME :prop :class-ref 'person))
TEST::$T-PERSON-NAME

(with-package :test
     (%%get-id 'NOM :attribute :class-ref 'test::person))
NIL

(with-package :test
     (%%get-id "NAME" :attribute :class-ref "personne"))
Warning: class "personne" not found in package "TEST" context 0, when looking for id of property
         HAS-NAME
NIL

(with-package :test
  ((with-package :test
     (%%get-id 'NAME :sp :class-ref 'person))
   '(:en "NAME" :fr "nom") :attribute 
               :class-ref '(:en "person" :fr "personne")))
TEST::$T-PERSON-NAME

(with-package :test (%%get-id "NAME" :attribute))
TEST::$T-NAME  ; generic property

(%%get-id "NAME" :attribute)
NIL

(with-package :moss
     (%%get-id 'NOM :attribute :class-ref 'personne))
NIL

(with-package :test (%%get-id "wife" :sp))
TEST::$S-WIFE

(with-package :test (%%get-id "employer" :sp))
$S-EMPLOYER

(with-package :test (%%get-id "employer" :tp))
NIL

(with-package :test (%%get-id '(:EN "employer" :FR "employeur") :sp))
TEST::$S-EMPLOYER

(catch :error
       (with-package :test (%%get-id '(:EN "employer" :FR "employeur") :sp
                                     :class-ref "person")))
TEST::$S-PERSON-EMPLOYER

(%%get-id 'name :inv :class-ref 'person)
NIL

(with-package :test
     (%%get-id 'test::name :inv :class-ref 'test::person))
NIL

(with-package :test
     (%%get-id "president" :inv :class-ref 'test::person))
TEST::$S-ORGANIZATION-PRESIDENT.OF

(with-package :test (%%get-id '(:en "person") :counter))
TEST::$E-PERSON.CTR
NIL

(catch :error (%%get-id '(:en "person") :ideal))
Warning: class (:EN "person") not found in package "MOSS" context 0, when looking for id of class
ideal
NIL

(with-package :test
    (%%get-id '(:en "person") :ideal))
TEST::$E-PERSON.0

;; versions
(with-package :test
  (with-context 5
    (%%get-id '(:en "person") :class)))
TEST::$E-PERSON

(with-package :test
  (with-context 5
    (%%get-id '(:en "name") :attribute)))
TEST::$T-NAME

(with-package :test
  (with-context 5
    (%%get-id '(:en "name") :attribute :class-ref "person")))
TEST::$T-PERSON-NAME
|#
;;;A---------------------------------------------------------- %%GET-ID-FOR-CLASS
;;; when ref is :any, then we reutn *ANY* in the current package, which means
;;; that we must check against *ANY of the current package in the other functions

(defUn %%get-id-for-class (ref &key include-moss)
  "ref is the reference of a class (symbol, string or mln). When ref is :any ~
   returns the class id *ANY* in the current package.
Argument:
   ref
Return:
   id of the class, can be NIL. Throws to :error if more than 1."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        name object-list class-list id)
    ;(format t "~%; %%get-id-for-class /agent: ~S" lu::agent)
    ;; if :any, return universal-class in current package JPB1010
    (when (eql ref :any)
      (return-from %%get-id-for-class (intern "*ANY*")))
    
    ;; test when creating orphans JPB1508
    (when (or (eql ref :none)
              (equal+ ref "MOSS-NULL-CLASS")
              (equal+ ref "MOSS NULL CLASS"))
      (return-from %%get-id-for-class (intern "*NONE*")))
    
    ;; if already id of a class in current package and context, return it
    (when (%type? ref '$ENT) (return-from %%get-id-for-class ref))
    
    ;; otherwise cook up class name. Careful: %make-name-for-concept creates
    ;; a concept symbol in the execution package of the function
    (setq name (%make-name-for-concept ref))
    ;(format t "~%; %%get-id-for-class /name: ~S, *package*: ~S" name *package*)
    ;; if not an entry (%pdm eventually loads the object from disk), return
    ;(format t "~%; %%get-id-for-class /(%pdm? name): ~S" (%pdm? name))
    ;(format t "~%; %%get-id-for-class /(eval name): ~S" (eval name))
    (unless (%pdm? name) (return-from %%get-id-for-class nil))
    
    ;; if we have several agents with the same entry make-sure that we fuse entries
    (%ldif name) ; JPB1601
    
    ;; otherwise get the list of classes pointed to by name
    (setq class-list (%get-value  name (%inverse-property-id '$ENAM)))
    ;(format t "~%; %%get-id-for-class /class-list ~S" class-list)
    ;; and keep only the ones in current package
    (setq object-list
          (%filter-against-package class-list *package*))
    
    ;; if more than one, severe error
    (when (cdr object-list)
      (warn " class id (~S) not unique in package ~S context ~S"
        name *package* context)
      (throw :error nil))
    
    ;; if nothing left and moss is allowed, try to filter against moss
    (if (and (null object-list) include-moss)
        (setq object-list (%filter-against-package class-list (find-package :moss))))
    ;; normally kernel classes are unique...
    ;; return id (or nil)
    (setq id (car object-list))
    ;; %pdm? eventually loads the object
    (if (and (%pdm? id)(%type? id '$ENT)) id)))

#|
(moss::%%get-id-for-class "person")
$E-PERSON

(with-package :test (%%get-id-for-class "person"))
TEST::$E-PERSON

(%%get-id-for-class :any)
*ANY*
:INTERNAL

(with-package :test
  (%%get-id-for-class :any))
TEST::*ANY*
:INTERNAL

(with-package :test
 (%%get-id-for-class "MOSS NULL CLASS"))
TEST::*NONE*
:INTERNAL

Versions:
(with-package :test
  (with-context 4
    (%%get-id-for-class :none)))
TEST::*NONE*
|#
;;;A------------------------------------------------------- %%GET-ID-FOR-COUNTER

(defUn %%get-id-for-counter (ref &key include-moss)
  "ref is the reference of a class 
Argument:
   ref: symbol, string or mln
Return:
   id of the class counter, can be NIL. Throws to :error if class does not exist."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        id class-id)
    ;; should throw to :error in a package where class is undefined
    (cond
     ;; if refernce is an id of type counter, return it
     ((%type? ref '$CTR) ref)
     ;; check existence of class
     ((not (setq class-id (%%get-id ref :class :include-moss include-moss)))
      ;; if not there, error but to be handled by the calling function
      (warn
          "class reference ~S not defined in package ~S context ~S, ~
        when looking for its counter"
        ref (package-name *package*) context)
      (return-from %%get-id-for-counter nil))
     ;; if it is a reference and we can find a counter return it
     ((and (setq id (%make-id-for-counter class-id))
           (%pdm? id))
      id)
     ;; if not found and package is not moss try moss
     ((and (not (eql *package* (find-package :moss)))
           include-moss
           (with-package :moss 
             (setq id (%make-id-for-counter (%make-id-for-class ref))))
           (%pdm? id))
      id)
     ;; otherwise give up
     (t (warn 
            "class reference ~S not defined in package ~S context ~S, ~
          when looking for its counter." 
          ref (package-name *package*) context)
        ;; return nil, to be handled by the calling function
        nil))))

#|
(with-package :test
    (%%get-id-for-counter "person"))
TEST::$E-PERSON.CTR

(catch :error (%%get-id-for-counter "person"))
"class reference \"person\" not defined in package \"MOSS\" context 0, when looking for counter"
NIL

(with-package :test
    (%%get-id-for-counter "moss concept" :include-moss t))
$ENT.CTR

;; versions (counter is defined in context 0)
(with-package :test
  (with-context 4
    (%%get-id-for-counter "person")))
TEST::$E-PERSON.CTR
|#
;;;A------------------------------------------------------------ %%GET-ID-FOR-EP

(defUn %%get-id-for-ep (ref &key include-moss (moss-context 0))
  "we do not have any information about property or class, we try to recover~
   all possible objects that could have ref as an entry point. We must compute ~
   possible entries by using all possible =make-entry functions present in the ~
   current environment.
Argument:
   ref: a symbol, string or mln
   include-moss (key): if true looks for MOSS entries
   moss-context (key): context of the moss search (default is 0)
Return:
   a list of possible entry-points"
  (let ((ep-string (%make-string-from-ref ref))
        (context (symbol-value (intern "*CONTEXT*")))
        make-entry-method-list method-list ep-list fn-list )
    ;(format t "~%; %%get-id-for-ep /*package*: ~S" *package*)
    ;(format t "~%; %%get-id-for-ep /context: ~S" context)
    
    ;; we must get =make-enty methods
    (setq make-entry-method-list 
          (%get-value (intern "=MAKE-ENTRY") 
                      (%inverse-property-id '$MNAM context)))
    ;(format t "~%; %%get-id-for-ep /make-entry-method-list: ~S"
    ;  make-entry-method-list)
    ;; filter against current package
    (setq method-list (%filter-against-package make-entry-method-list *package*))
    (unless method-list (return-from %%get-id-for-ep nil))
    (format t "~%; %%get-id-for-ep /method-list: ~S" method-list)
    
    ;; get the functions
    (setq fn-list 
          (delete-duplicates
           (mapcar #'(lambda (xx) (car (%get-value xx '$FNAM context)))
             method-list)))
    ;; get entry point list
    (setq ep-list (mapcar #'(lambda (fn) (car (apply fn ep-string nil))) fn-list))
    
    ;; if package was not MOSS add moss entries. 
    ;; ***** Watch this: We test MOSS in context 0
    (unless (or (eql *package* (find-package :moss)) (null include-moss))
      (with-package :moss
        (with-context moss-context
          (setq method-list (%filter-against-package make-entry-method-list 
                                                     (find-package :moss)))
          (unless method-list (return-from %%get-id-for-ep nil))
          ;; get the functions
          (setq fn-list 
                (mapcar #'(lambda (xx) (car (%get-value xx '$FNAM moss-context)))
                  method-list))
          ;; get entry point list
          (setq ep-list 
                (append ep-list
                        (mapcar #'(lambda (fn) (car (apply fn ep-string nil)))
                          fn-list))))))
    
    ;(delete-duplicates result :test #'equal)
    (setq ep-list (delete-duplicates ep-list))
    ;; keep only those entry points that are PDM objects
    (remove-if-not #'(lambda(xx)(%alive? xx context))
                   (remove-if-not #'%pdm? ep-list))
    ))

#|
(%%get-id-for-ep "method")
(>-METHOD METHOD)

(%%get-id-for-ep "moss concept name")
(HAS-MOSS-CONCEPT-NAME)

(with-package :test
    (%%get-id-for-ep "method"))
(METHOD)

(with-package :test
    (%%get-id-for-ep "person"))
(TEST::PERSON)

(with-package :test
    (%%get-id-for-ep "person" :include-moss t))
(TEST::PERSON)

Versions:
(with-package :test
  (with-context 4
    (%%get-id-for-ep "barth�s")))
(TEST::BARTH�S)

(with-package :test
  (with-context 0
    (%%get-id-for-ep "streit")))
NIL

(with-package :test
  (with-context 3
    (%%get-id-for-ep "streit")))
(TEST::STREIT)
|#
;;;A--------------------------------------------------------- %%GET-ID-FOR-IDEAL

(defUn %%get-id-for-ideal (ref &key include-moss)
  "ref is the reference of a class 
Argument:
   ref: symbol, string or mln
Return:
   id of the ideal id, can be NIL, warning when class does not exist."
  (let (id class-id)
    (setq class-id (%%get-id ref :class))
    (when class-id
      (setq id (%make-id-for-ideal class-id))
      ;; if exists OK, otherwise return nil (%pdm loads object)
      (return-from %%get-id-for-ideal (if (%pdm? id) id)))
    
    ;; here no class
    (warn 
        "class ~S not found in package ~S context ~S, ~
      when looking for id of class ideal" 
      ref (package-name *package*) (symbol-value (intern "*CONTEXT*")))      
    
    ;; try moss
    (when (and (not (eql *package* (find-package :moss))) include-moss)
      (with-package :moss
        (%%get-id-for-ideal ref)
        ))))

#|
(with-package :test
    (%%get-id-for-ideal "person"))
TEST::$E-PERSON.0

(with-package :test
    (%%get-id-for-ideal "ZOE"))
; Warning: class reference "ZOE" not defined in package "TEST" context 0, when looking for id of class ideal
; While executing: MOSS::%%GET-ID-FOR-IDEAL
NIL

(with-package :moss
    (%%get-id-for-ideal "moss attribute"))
$EPT.0

(with-package :test
    (%%get-id-for-ideal "zoe" :include-moss t))
; Warning: class "zoe" not found in package "TEST" context 0, when looking for id of class ideal
; While executing: MOSS::%%GET-ID-FOR-IDEAL
; Warning: class "zoe" not found in package "MOSS" context 0, when looking for id of class ideal
; While executing: %%GET-ID-FOR-IDEAL
NIL

Versions:
(with-package :test
  (with-context 3
    (%%get-id-for-ideal "person")))
TEST::$E-PERSON.0

;; orphans?
(with-package :test
  (with-context 3
    (%%get-id-for-ideal "MOSS NULL CLASS")))
TEST::$ORPHAN.0

TEST::$ORPHAN.0
((MOSS::$TYPE (0 *NONE*)))
|#
;;;A----------------------------------------------- %GET-ID-FOR-INVERSE-PROPERTY
;;; we have a special case for inverse attributes, e.g.
;;;   (%%get-id "name" :inv :class-ref "Student") => $T-PERSON-NAME.OF
;;; does not guaranty however that the attribute is an entry point, e.g.
;;;   (%%get-id "first name" :inv :class-ref "Student") => $T-PERSON-FIRST-NAME.OF

(defUn %%get-id-for-inverse-property (ref &key class-ref)
  "returns an inverse property id from a specific class, e.g. \"president\", and a ~
   class, e.g. \"Person\", looks for a property is-president-of for the class person.
   A problem is that such properties are not unique and depend on the class linked ~
   to person. E.g., if the neighbor starting class is COMPANY, the property will be ~
   IS-COMPANY-PRESIDENT-OF; if the class is UNIVERSITY, IS-UNIVERSITY-PRESIDENT-OF, ~
   etc. Thus, the function will usually return a list.
Arguments:
   ref: a symbol, string or mln denoting a DIRECT property ~
                  (e.g. PRESIDENT, HAS-PRESIDENT, \"president\")
   class-ref (key): the ref of the class supposed to have the inverse property, ~
                  (e.g. PERSON)
Return:
   a list of IDs of possible properties or nil. Throws to :error if the class is ~
   not defined in the active context."
  (dformat :inv 1 "~%;----------- Entering %%get-id-for-inverse-property")
  
  ;; if ref is already the name of an inverse property return it
  (if (%type? ref '$EIL)  (return-from %%get-id-for-inverse-property ref))
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        object-list class-id name att-id)
    
    ;; check if ref is the name of an attribute
    (when (setq att-id (%%get-id ref :attribute :class-ref class-ref))
      ;; if so return the inverse of it
      (return-from %%get-id-for-inverse-property
        (%inverse-property-id att-id)))
    
    ;; check if ref is the inverse name of an attribute
    (when (setq att-id (%%get-id (%make-name-for-inverse-property ref)
                                 :attribute :class-ref class-ref))
      ;; if so return the inverse of it
      (return-from %%get-id-for-inverse-property
        (%inverse-property-id att-id)))
    
    ;; cook up inverse property name e.g. IS-EMPLOYEUR-OF or IS-EMPLOYER-OF
    (setq name (%make-name-for-inverse-property ref))
    (dformat :inv 1 "~%;--- name: ~S" name)
    ;; if not entry point quit
    (unless (%pdm? name) (return-from %%get-id-for-inverse-property nil))
    
    ;; get the list of possible inverse properties from the name e.g.
    ;; ($S-PRESIDENT.OF $S-TEACHING-ORGANIZATION-PRESIDENT.OF
    ;;    $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF)
    (setq object-list
          (%filter-against-package  
           (or (%get-value name (%inverse-property-id '$INAM) context)
               ;; for processing inverses of inverses
               (%get-value name (%inverse-property-id '$PNAM) context))
           *package*))
    (dformat :inv 1 "~%;--- object-list: ~S" object-list)
    
    ;; if a class is indicated, filter against that class
    ;; otherwise, try to filter using class-ref
    (when class-ref
        ;; if class-ref we must find the inverse property for this class
          ;; first get class id
          (setq class-id (%%get-id class-ref :concept))
          (dformat :inv 1 "~%;--- class-id: ~S" class-id)
          (unless class-id
            (verbose-throw 
             :error 
             " class reference ~S not defined in package ~S context ~S, ~
            when looking for id of inverse property ~S"
             class-ref *package* context name)
            )
          (setq object-list
                (%determine-inverse-property-id-for-class object-list class-id)))
    (if (cdr object-list)
        (warn "there are several inverse properties referenced by ~S~
               while using %%get-id; you may want to use :inv-from option" ref))
    ;; otherwise return the list
    (if (cdr object-list) object-list (car object-list))
    ))

#|
(with-package :address 
     (%%get-id-for-inverse-property "president" :class-ref "person"))
($S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF $S-TEACHING-ORGANIZATION-PRESIDENT.OF)

(%make-name-for-inverse-property ">president")
HAS-PRESIDENT
NIL

(%%make-name ">president" :inv)
HAS-PRESIDENT
:INTERNAL

;; case: ORGANIZATION -> HAS-PRESIDENT -> PERSON
;; in the test environment the property is-president-of has several inverses
(with-package :test
  (%%get-id-for-inverse-property 'test::IS-PRESIDENT-OF))
(TEST::$S-PRESIDENT TEST::$S-ORGANIZATION-PRESIDENT)

;; retrieving direct property for organization
(with-package :test
  (%%get-id-for-property "president" :sp :class-ref "organization"))
TEST::$S-ORGANIZATION-PRESIDENT

;; retrieving inverse property from the class person
(with-package :test
  (%%get-id-for-inverse-property "president" :class-ref "person"))
TEST::$S-ORGANIZATION-PRESIDENT.OF

;; versions
(with-package :test
  (with-context 6
    (%%get-id-for-inverse-property "president" :class-ref "person")))
TEST::$S-ORGANIZATION-PRESIDENT.OF
|#
;;;------------------------------- %%GET-ID-FOR-INVERSE-PROPERTY-KNOWING-ORIGIN

(defun %%get-id-for-inverse-property-knowing-origin (ref class-ref from-ref)
  "returns the hopefully unique inverse property linking from-ref to class-ref ~
   knowing the name of te inverse property.
Arguments:
   ref: symbol, string or MLN denoting the inverse property, e.g IS-THESIS-OF ~
        or \">th�se\" (MLN might be rarely encountered)
   class-ref (key): class owning the inverse property, e.g. \"Thesis\"
   from-ref (key): class source of the inverse property, e.g. \"Person\"
Return:
   inverse property id or id of a generic inverse property in case instances ~
   have properties not recorded at the class level (with a woarning)"
  (let ((context (symbol-value (intern "*CONTEXT*")))
        prop-list inv-list from-id class-id prop-name class-prop-list)
    (dformat :inv 1
             "~%;----------- Entering %%get-id-for-inverse-property-knowing-origin")
    ;; if ref is the id of an inverse property return it
    (if (%type? ref '$EIL) 
        (return-from %%get-id-for-inverse-property-knowing-origin ref))
    
    ;; get ids of class-ref and from-ref
    (setq class-id (%%get-id class-ref :class))
    (setq from-id (%%get-id from-ref :class))
    (dformat :inv 1 "~%;--- class-id: ~S" class-id)
    (dformat :inv 1 "~%;--- from-id: ~S" from-id)
    
    ;; otherwise, get name of the direct property (works with symbol, strin and MLN)
    (setq prop-name (%make-name-for-inverse-property ref))
    (dformat :inv 1 "~%;--- prop-name: ~S" prop-name)
    
    ;; get the list of all possible direct properties from the name e.g.
    ;; ($S-PRESIDENT $S-TEACHING-ORGANIZATION-PRESIDENT
    ;;    $S-NON-PROFIT-ORGANIZATION-PRESIDENT)
    ;; we keep only data from the current package (should we include MOSS ?
    (setq prop-list
          (%filter-against-package  
           (%get-value prop-name (%inverse-property-id '$PNAM) context)
           *package*))
    (dformat :inv 1 "~%;--- prop-list: ~S" prop-list)
    
    ;; keep the property attached to the from class
    (setq class-prop-list (%determine-property-id-for-class prop-list from-id))
    (dformat :inv 1 "~%;--- prop-list for the class ~S: ~S" from-id prop-list)
    
    ;; get the list of inverses from class-id
    (setq inv-list (%determine-inverse-property-id-for-class class-prop-list class-id))
    (dformat :inv 1 "~%;--- inv-list for the class ~S: ~S" class-id inv-list)
    
    
    ;; here, prop is not recorded are class level
    ;; return the generic property
    (unless inv-list
      (warn "Inverse property ~S is not an inverse property of class ~S, we take ~
             a generic property." ref class-ref)
      (setq inv-list 
            (list (%inverse-property-id (%get-generic-property prop-list)))))
    
    ;; return
    (if (cdr inv-list) inv-list (car inv-list))
    ))
  
#|
(moss::%%get-id-for-inverse-property-knowing-origin ">th�se" "Th�se" "PhD student")
$S-PHD-STUDENT-THESIS.OF

(moss::%%get-id-for-inverse-property-knowing-origin '$S-PHD-STUDENT-THESIS.OF "Th�se" "PhD student")
$S-PHD-STUDENT-THESIS.OF
|#
;;;A------------------------------------------------------- %GET-ID-FOR-PROPERTY
;;;***** could use more tests

(defUn %%get-id-for-property (ref type-key &key class-ref include-moss)
  "return an existing property id according to type-key.
Arguments:
   ref: a symbol, string or mln (e.g. NAME, HAS-NAME \"name\")
   type-key: a keyword (:sp :tp :attribute :relation :prop :property)
   class-ref (key): the ref of the class supposed to have the property, or :any
   include-moss (key): if present if no id is found in current package, try moss
Return:
   the id of the property or nil if class-ref does not refer to existing class.
   throws to :error if class-ref points to more than one class."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        class-id object-list name result)
    ;(format t "~%;---------- %%get-id-for-property")
    (if (%type? ref '$EPR) (return-from %%get-id-for-property ref)) 
    ;; build the internal property name, e.g. name <- HAS-XXX
    (setq name (%make-name-for-property ref))
    ;(format t "~%;--- name: ~S" name)
    ;; If name does not exist, can't fing property-id, then quit
    (unless (%pdm? name) (return-from %%get-id-for-property nil))
    
    ;;=== if class-ref look for a class property 
    (when class-ref
      ;; try to obtain class-id in moss package if needed. If :any, returns :any
      (setq class-id (%%get-id class-ref :concept :include-moss t))
      ;(format t "~%;--- class-id: ~S" class-id)
      (unless class-id
        (warn
            (format nil "class ~S not found in package ~S context ~S, ~
                      when looking for id of property ~S"
              class-ref (package-name *package*) context name))
        (return-from %%get-id-for-property nil))
      
      ;; if class-id is :any, we want to return generic property, e.g. $T-XXX
      ;; generic property is the one without an $IS-A clause
      (when (eql class-id :any)
        ;; get the list of properties corresponding to ref
        ;; just to make sure eliminating anything not in the current package   
        (setq object-list 
              (%filter-against-package
               (%get-value name (%inverse-property-id '$PNAM) context)
               *package*))
        ;; remove any object with $IS-A
        (setq object-list
              (remove-if #'(lambda (xx) (%%get-value xx '$IS-A context))
                         object-list))
        ;; if more than one, severe error
        (when (cdr object-list)
          (throw
              :error
            (format nil "generic property (~S) not unique in package ~S context ~S"
              name *package* context)))
        ;;********** should check MOSS package here ??
        (return-from %%get-id-for-property (car object-list)))        
      
      ;; otherwise get specific property for the class in current package
      (setq object-list
            (%determine-property-id-for-class 
             (%filter-against-package  
              (%get-value name (%inverse-property-id '$PNAM) context)
              *package*)
             class-id)
          )
      ;; if more than one, severe error
      ;; this is the case when class multiply inherits from classes having both
      ;; the property
      (when (cdr object-list)
        (throw
            :error
          (format nil "property (~S) not unique in package ~S context ~S"
            name *package* context)))
      
      ;; if nothing left try MOSS package, if different from current package
      (unless (or object-list (eql *package* (find-package :moss))(not include-moss))
        (setq object-list
              (%determine-property-id-for-class 
               (%filter-against-package  
                (%get-value name (%inverse-property-id '$PNAM) context)
                (find-package :moss))
               class-id)
            ))
      ;; return id (or nil)
      (setq result (car object-list)))
    
    ;;=== if class-ref not there get generic property id
    (unless class-ref
      ;; otherwise we want generic property
      (setq result
            (%get-generic-property
             (%filter-against-package  
              (%get-value name (%inverse-property-id '$PNAM) context)
              *package*)))
      (unless (or result (eql *package* (find-package :moss))(not include-moss))
        (setq result
              (%get-generic-property
               (%filter-against-package  
                (%get-value name (%inverse-property-id '$PNAM) context)
                (find-package :moss))))
        ))
    
    ;; filter on type of property
    (cond
     ((and (member type-key '(:tp :attribute))
           (%is-attribute? result))
      result)
     ((and (member type-key '(:sp :relation))
           (%is-relation? result))
      result)
     ((member type-key '(:prop :property))
      (if (%pdm? result) result))
     )))

#|
(with-package :test
  (%%get-id-for-property "president" :sp :class-ref "organization"))
TEST::$S-ORGANIZATION-PRESIDENT

;; versions
(with-package :test
  (with-context 6
    (%%get-id-for-property "president" :sp :class-ref "organization")))
TEST::$S-ORGANIZATION-PRESIDENT
|#
;;;?-------------------------------------------------- %GET-ID-FOR-VIRTUAL-CLASS

(defUn %%get-id-for-virtual-class (ref &key include-moss)
  "ref is the reference of a virtual class (symbol, string or mln)
Argument:
   ref: (symbol, string or mln)
   inclule-moss (key): if true and result is nil try MOSS package
Return:
   id of the class, can be NIL. Throws to :error if more than 1."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        name object-list class-list id)
    ;; if already id of a class in current package and context, return it
    (when (%type? ref '$VENT) (return-from %%get-id-for-virtual-class ref)) 
    ;; otherwise cook up class name. Careful: %make-name-for-concept creates
    ;; a concept symbol in the execution package of the function
    (setq name (%make-name-for-concept ref))
    ;; if not an entry (%pdm eventually loads the object from disk), return
    (unless (%pdm? name) (return-from %%get-id-for-virtual-class nil))
    
    ;; otherwise get the list of classes pointed to by name
    (setq class-list (%get-value name (%inverse-property-id '$ENAM)))
    ;; and keep only the ones in current package
    (setq object-list
          (%filter-against-package class-list *package*))
    ;; if more than one, severe error error
    (when (cdr object-list)
      (warn " class id (~S) not unique in package ~S context ~S"
        name *package* context)
      (throw :error nil))
    
    ;; if nothing left and moss is allowed, try to filter against moss
    (if (and (null object-list) include-moss)
        (setq object-list 
              (%filter-against-package class-list (find-package :moss))))
    ;; normally kernel classes are unique...
    
    ;; return id (or nil)
    (setq id (car object-list))
    ;; %pdm eventually loads the object
    (if (%pdm? id) id)))

#|
;; needs some virtual classes in the :test package
|#
;;;========================== end %%get-id functions ===========================

;;;---------------------------------------------------------- %GET-INDEX-WEIGHTS
;;; don't know if this function should be here should be in the dialog file?

(defUn %get-index-weights (task-id)
  "get a list of pairs index (a string) value from the list of index patterns ~
   associated with a task.
Arguments:
   task: a task object
Return:
   a list of pairs."
  ;;; we must differentiate MOSS from applications
  (if (eql *package* (find-package :moss))
      (let ((indexes (send task-id '=get-id '$IDXS))) ; MOSS index pattern
        (dformat :dialog 2 "~%; %get-index-weight /package: ~S indexes:~%  ~S" 
                  *package* indexes)
        (mapcar #'(lambda (xx) (list (car (send xx '=get-id '$IDXT)) ; index
                                     (car (send xx '=get-id '$WGHT)))) ; index weight
          indexes))
    ;; application package, we cache the indexes on the task p-list
    (or
     (get task-id :indexes)
     (let ((indexes (send task-id '=get "index pattern"))) ; list of indexes
       (dformat :dialog 2 "~%; %get-index-weight / package: ~S indexes:~%  ~S" 
                 *package* indexes)
       ;; build an a-list for further processing
       (setq indexes
             (mapcar #'(lambda (xx) (list (car (send xx '=get "index")) ; index
                                          (car (send xx '=get "weight")))) ; index weight
               indexes))
       ;; cache result
       (setf (get task-id :indexes) indexes)
       ;; return a-list
       indexes))
    ))

#|
? (%GET-INDEX-WEIGHTS 'albert::$E-task.3)
(("home address" 0.7) ("private address" 0.7) ("lives" 0.4) ("live" 0.4)
 ("where" 0.2) ("residence" 0.5) ("adresse priv?e" 0.7) ("domicile" 0.7)
 ("habite" 0.7) ("vit" 0.7) ("adresse personnelle" 0.7) ("o�" 0.2))
|#
;;;A-------------------------------------------------------- %GET-INSTANCE-COUNT
;;; Usefulness debatable

(defUn %get-instance-count (class-id &key (allow-subclasses t))
  "Gets an upper-bound on the number of instances for a given class regardless ~
   of context or of dead instances.
Arguments:
   class-id: identifier of class
   allow-subclasses (key): if T (default), counts instances of subclasses
Returns:
   a number"
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (class-list 
         (if allow-subclasses
             (%sp-gamma class-id (%make-id '$EIL :id '$IS-A))
           (list class-id))))
    ;(format t "~%; %get-instance-count /class-list: ~S" class-list)
    ;; remove dead classes, loading all of them
    (setq class-list 
          (remove nil (mapcar #'(lambda(xx)(%alive? xx context)) class-list)))
    ;; add value of each counter for the remaining valid classes
    (reduce #'+
            (mapcar #'(lambda (xx)
                        (car (%get-value xx '$VALT 0)))
              (mapcar #'(lambda (yy) (car (%get-value yy '$CTRS 0)))
                class-list)))))

#|
(with-package :test
  (moss::%get-instance-count 'test::$E-PERSON :allow-subclasses NIL))
8

Versions:
(with-package :test
  (with-context 6
    (moss::%get-instance-count 'test::$E-PERSON)))
11
|#
;;;A---------------------------------------------- %GET-INTERNAL-INSTANCE-NUMBER

(defUn %get-internal-instance-number (obj-id)
  "returns the integer associated with the instance id as a string."
  (when (%pdm? obj-id)
    (let* ((name (symbol-name obj-id))
           (pos (position #\. name :from-end t)))
      ;; if for some reason the internal name has not the proper form, return nil
      (if (integerp pos) (subseq name (1+ pos))))))

#|
(moss::%get-internal-instance-number 'an::$E-CAT�GORIE.10)
"10"
|#
;;;A---------------------------------------------- %%GET-INSTANCE-ORDER-FROM-ID

(defun %%get-instance-order-from-id (id)
  "returns a number from a symbol like xxx.234 regardless whether it is a PDM ~
   object or not, otherwise returns -1. Dangerous function." 
  (let* ((name (symbol-name id))
         (pos (position #\. name :from-end t))
         cc)
    (if (and pos
             (setq cc (read-from-string (subseq name (1+ pos))))
             (integerp cc))
        cc 
      -1)))

#|
(%%GET-INSTANCE-ORDER-FROM-ID '$e-info.234)
234
(moss::%%GET-INSTANCE-ORDER-FROM-ID '$e-info)
-1
(moss::%%GET-INSTANCE-ORDER-FROM-ID '$e-info.ZER)
-1
|#
;;;A---------------------------------------------------- %GET-INSTANCES-IN-RANGE

(defun %get-instances-in-range (class-ref min max &optional version)
  "returns the list of valid application instances with id ranging from min to max.
Arguments:
   class-ref: class reference
   min: low order
   max: high order
   version (opt): context (default current)
Return:
   list of alive instances in that range (min, max included)"
  (let ((context (or version (symbol-value (intern "*CONTEXT*"))))
        class-id result id)
    ;; get the class id
    (setq class-id (%%get-id class-ref :class))
    ;; if nil error
    (unless class-id
      (error "bad class reference: ~S in package ~S in context ~S"
        class-ref *package* context))
    
    ;; otherwise synthesize instances id and load the objects
    (dotimes (nn (1+ (- max min)))
      ;; get object
      (setq id (%ldif (%make-id-for-instance class-id (+ min nn))))
      ;; if active set up structure
      (if (%alive? id context) (push id result)))
    (reverse result)))

#|
(moss::%get-instances-in-range "cat�gorie" 4 8)
($E-CAT�GORIE.4 $E-CAT�GORIE.5 $E-CAT�GORIE.6 $E-CAT�GORIE.7 $E-CAT�GORIE.8)

? (send '$e-cat�gorie.6 '=delete)
((MOSS::$TYPE (0 $E-CAT�GORIE)) (MOSS::$ID (0 $E-CAT�GORIE.6)) ($T-CAT�GORIE-LIBELL� (0)) (MOSS::$TMBT (0 T)))

? (moss::%get-instances-in-range "cat�gorie" 4 8)
($E-CAT�GORIE.4 $E-CAT�GORIE.5 $E-CAT�GORIE.7 $E-CAT�GORIE.8)

(with-package :test
  (with-context 0
    (%get-instances-in-range "person" 3 99)))
NIL

(with-package :test
  (with-context 6
    (%get-instances-in-range "person" 3 99)))
(TEST::$E-PERSON.3)

(with-package :test
  (with-context 3
    (%get-instances-in-range "person" 3 99)))
(TEST::$E-PERSON.4 TEST::$E-PERSON.5)
|#
;;;A--------------------------------------------------------- %GET-LAST-INSTANCE

(defun %get-last-instance (class-id &key even-if-dead)
  "returns the last created instance of the specified class, even if dead when ~
   the :even-if-dead key is true. If :even-if-dead is false, returns the last ~
   alive object in the current context.
arguments:
   class-id: id of a class
   even-if-dead (key): if t, returns object even if dead
returns:
   an object id or nil"
  (when (%is-concept? class-id)
    ;; first dereference object loading it if needed. resolve either returns the
    ;; resolved reference or the original object, e.g. if unbound or nil valued
    (let* ((package (symbol-package class-id))
           (context (symbol-value (intern "*CONTEXT*")))
           (rad (car (%%get-value class-id '$rdx context)))
           cmax last-id)
      ;; get class counter
      (setq cmax (1- (%get-class-counter class-id)))
      ;; if counter is 1, there are no instances yet
      (if (< cmax 1)
          (return-from %get-last-instance nil))
      
      ;; if even-if-dead return next object no matter what
      (when even-if-dead
        (setq last-id (intern (format nil "~a.~s" rad cmax) package))
        (return-from %get-last-instance (%ldif last-id)))
      
      ;; otherwise try to get last alive object
      (loop for count from cmax downto 1 
          thereis (let ((last-id (%make-id-for-instance class-id count)))
                    (if (%alive? last-id context) (%ldif last-id)))))))

#|
MOSS(168): (%GET-LAST-INSTANCE 'AN::$E-INFO)
ALBERT-NEWS::$E-INFO.7

(with-package :test
  (with-context 0
    (%get-last-instance 'test::$E-PERSON)))
TEST::$E-PERSON.2

(with-package :test
  (with-context 0
    (%get-last-instance 'test::$E-PERSON :even-if-dead t)))
TEST::$E-PERSON.7

(with-package :test
  (with-context 6
    (%get-last-instance 'test::$E-PERSON :even-if-dead t)))
TEST::$E-PERSON.7
|#
;;;A------------------------------------------- %%GET-LIST-OF-VALUES-AND-CONTEXT

(defUn %%get-list-of-values-and-context (obj-id prop-id)
  "Done to obtain raw value list of objects.
Arguments:
   obj-id: identifier of object
   prop-id: property local identifier
Returns:
   a list of all values by context (internal format of the set of values)."
  (setq obj-id (%resolve obj-id))
  (cdr (assoc prop-id (symbol-value obj-id))))

#|
? (moss::%%get-list-of-values-and-context '$E-person 'moss::$PT)
((0 $T-PERSON-NAME $T-PERSON-FIRST-NAME $T-PERSON-INITIALS $T-PERSON-SEX
  $T-PERSON-TITLE $T-PERSON-TRADE $T-PERSON-SHORT-ADDRESS $T-PERSON-LONG-ADDRESS
  $T-PERSON-PHONE $T-PERSON-HOME-PHONE $T-PERSON-OFFICE-PHONE $T-PERSON-CELL-PHONE
  $T-PERSON-EMAIL $T-PERSON-WEB-SITE $T-PERSON-SPECIALITY))

(with-package :test
  (with-context 3
    (%%get-list-of-values-and-context 'test::$E-PERSON.2 'test::$T-PERSON-NAME)))
((0 "Barth�s-Biesel" "Barth�s"))
|#
;;;?---------------------------------------------------------------------- %GETM
;;;When methods are compiled on p-list it is done as follows:
;;;	(=print-self (:own (nn <int-function name>)*) (:instance  <...>))
;;; if the method type is not specified then the format is slightly different
;;; from the more general format.
;;; The version must be recorded locally otherwise we shall try to inherit it.
;;; Remember this is a cache, not an object.
;;;
;;; When using versions we can have the method cached onto the p-list from a ~
;;; previous version. If the method has not been modified since, it is a waste ~
;;; of energy to recover it again, to recompile it, and to give it a new name. ~
;;; Rather, we want to check that it was not modified along the same branch 
;;; indeed, then we cache it under the current version explicitely with the old
;;; internal name. In order to do that, we must record every time a method code
;;; or arguments are changed.
;;;    Doing
;;;        (%get-value <method-id> <modif> *current-context*)
;;; will return the last modification or nil (it could have been modified, but ~
;;; then it is not in the same branch.
;;; So for recovering a cached method, we first get the current branch of the ~
;;; path from the current context to the root of the version graph.
;;; We then loop on the successive values of the versions in the branch starting ~
;;; with the current one. At each step, either we get a function, or this is the ~
;;; step at which the version was modified, in which case we loose and we must ~
;;; recompute and recompile the method. Otherwise, when we get a previously ~
;;; recorded method, we must cache it explicitely on the p-list for next time around.
;;;
;;;********** BUG
;;; Now we have a problem with multiple packages, since the name of the method is
;;; global and different packages can have different methods with the same name
;;; e.g. =make-entry, in different or same contexts.
;;; Maybe should be
;;;	(=print-self (:test (:own (nn <int-function name>)*) (:instance  <...>))
;;;                  ...)


(defUn %getm (object-id method-name context &rest own-or-instance)
  "Recover the method eventually cached on the plist of the method name, using ~
   the version graph. If nothing was cached return nil.
Arguments:
   object-id: id of the object
   method-name: name of the target method
   context: context
   own-or-instance (key): flag to specify type of method (:own, :instance)
Return:
   method or nil"
  
  (declare (special cached-method))
  ;; not sure the context business is very useful
  (with-context context
    (setq object-id (%resolve object-id)))
  
  ;; first get list ((:own (nn <int-function-name>)*) (:instance  <...>)
  (let* ((method-type (or (car own-or-instance) :general))
         (version-graph-symbol (intern "*VERSION-GRAPH*"))
         )
    ;; get the plist value associated with method-name
    (setq cached-method (get object-id method-name))
    
    (cond
     ;;if nothing has been cached, then return nil
     ((not cached-method) nil)
     
     ;; first look for local value with specific
     ((car (%%has-value 'cached-method method-type context)))
     
     ;; here method is not cached for the given context. We then are going
     ;; to check if it was cached for a version that was not modified later
     (t
      (let ((context-branch 
             (get version-graph-symbol context)) ; version graph branch
            (modification-version 
             ;; for all methods with name method-name get the versions in which
             ;; they were recorded (it is not possible at this stage to know
             ;; which method exactly we are looking for)
             (reduce #'append 
                     ;;********** $MODT does not seem to exist!
                     (mapcar #'(lambda (xx) (%get-value xx '$MODT))
                       (%get-value method-name (%inverse-property-id '$MNAM)))))
            internal-function)
        
        ;; if branch to the root was not cached, compute it and cache it
        (unless context-branch
          (setq context-branch (%vg-gamma context))
          ;; should be computed only once or cached somewhere
          (setf (get version-graph-symbol context) context-branch)
          )
        
        (if modification-version
            ;; if we have recorded modifications, then we must check that they
            ;; were done previously to the last cached method
            (dolist (version context-branch)
              (setq internal-function 
                    (car (%%has-value 'cached-method method-type version)))
              (if internal-function (return nil)))
          
          ;; otherwise just return the last recorded method
          (setq internal-function 
                (car (%%get-value 'cached-method method-type context))))
        
        ;; if we got a method, then we cache it
        (when internal-function
          ;; add value without checking anything.
          (%%add-value 'cached-method method-type internal-function context)
          ;; update p-list of object
          (setf (get object-id method-name) cached-method))
        ;; then return method internal name
        internal-function)))))

#|
;; modified version including package reference
(defUn %getm (object-id method-name context method-type)
  "Recover the method eventually cached on the plist of the method name, using ~
   the version graph. If nothing was cached return nil.
Arguments:
   object-id: id of the object
   method-name: name of the target method
   context: context
   method-type: flag to specify type of method (:own, :instance)
Return:
   method or nil"
  
  (declare (special *cache-methods*))
  ;; not sure the context business is very useful
  (with-context context
    (setq object-id (%resolve object-id)))
  
  (let* (package-alist value-list internal-function)
    ;; get the plist value associated with method-name
    ;; p-list: ((<package> (:own (nn <int-function-name>)*) (:instance  <...>)...)
    
    (setq package-alist 
          (cdr (assoc (package-name *package*) (get object-id method-name)
                      :test #'equal+)))
    ;; now we have ((nn (:own <int-function-name>)(:instance  <...>))...))
    (setq value-list (getv context package-alist))
    ;; now we have ((:own <fname>)(:instance <fname>))
    
    (cond
     ;; first look for local value with specified method-type
     ((car (getv method-type value-list)))
     
     (t
      ;; here method is not cached for the given context. We then are going
      ;; move up along the context branch from the current context to the root
      ;; until hitting a value recorded in data
      ;; %get-version-path-to-root returns nil if context is illegal
      ;; uses vp-gamma and local *version-graph*
      (dolist (ctxt (cdr (%get-version-path-to-root context)))
        (setq internal-function 
              (car (getv method-type (getv ctxt package-alist))))
        (if internal-function (return)))
      ;; if we got a method from higher up, then we cache it in this context
      (when (and internal-function *cache-methods*)
        (%putm object-id internal-function method-name context method-type))
      ;; then return method internal name
      internal-function))))

(with-package :test
  (%getm 'test::$E-PERSON.1 'test::=F1 0 :own))
TEST::NEW-F1

(with-package :test
  (%getm 'test::$E-PERSON.1 'test::=F1 6 :own))
TEST::F6

(symbol-plist 'test::$E-person.1)
(TEST::=F1 (("TEST" (0 (:OWN TEST::NEW-F1)) (6 (:OWN TEST::F6)))) :PDM T)

(with-package :test
  (%getm 'test::$E-PERSON.1 'test::=F1 4 :own))

;; versions
(with-package :test
  (let ((*cache-methods* t))
    (%getm 'test::$E-PERSON 'test::=F1 6 :instance)))
TEST::CLASS-F1

(symbol-plist 'test::$E-person)
Warning: caching instance method =F1 onto the p-list of class $E-PERSON
(TEST::=F1 (("TEST" (6 (:INSTANCE TEST::CLASS-F1))
             (0 (:OWN TEST::OWN-CLASS-F1) (:INSTANCE TEST::CLASS-F1))))
           =SUMMARY ((:INSTANCE (0 TEST::$E-PERSON=I=0=SUMMARY))) :PDM T)
|#
;;;---------------------------------------------------------- %GET-NAME-FROM-REF
;;; deprecated. Use (%make-name-for-XXX ref)

;;;A--------------------------------------------------------------- GET-NAME-KEY
;;; used by %%make-name-string to let type-key be a class-id

(defun get-name-key (class-id &optional type)
  "takes a class-id in input and outputs a keyword to be used by %%make-name-string.
Arguments:
   class-id: e.g. $ENT, $EILS, $EPR
   type (opt): when class-id is $FN should be :instance or :own
Return:
   a keyword or nil"
  (let ((ll '(($DOCE . :doc)($ENT . :class)($EIL . :inv)($EPR . :prop) 
              ($EPS . :relation)($EPT . :attribute)($FN)($UNI . :uni)
              ($CVSE . :conversation)($QSTE . :q-state)($DOCE . :doc)))
        ;; virtual class?
        result)
    (dolist (item ll)
      (when (%subtype? class-id (car item))
        (setq result (if (eql (car item) '$FN) type (cdr item)))
        (return)))
    result))

#|
(get-name-key '$EPR)
:PROP

(get-name-key '$QSTE)
:Q-STATE

(get-name-key '$FN :own)
:OWN
|#
;;;A--------------------------------------------------------- %GET-NEXT-INSTANCE

(defun %get-next-instance (obj-id &key even-if-dead)
  "get the first alive instance created after the specified instance unless the ~
   even-if-dead keyword is true. If the object is the last one returns nil.
   Does not work with classes.
Arguments: 
   obj-id: a symbol, id of an object
   even-if-dead (key): if t returns the next object even if dead
Returns:
   an object-id or nil."
  ;; execute only if obj-id is a PDM object otherwise return nil
  (when (%pdm? obj-id)
    ;; first dereference object loading it if needed. resolve either returns the
    ;; resolved reference or the original object, e.g. if unbound or nil valued
    (let* ((id (%resolve obj-id)) 
           (name (symbol-name id))
           (pos (position #\. name :from-end t))
           (package (symbol-package id))
           (context (symbol-value (intern "*CONTEXT*")))
           rad cc cmax class-id next-id)
      ;(format t "~%; %get-next-instance /pos: ~S" pos)
      ;; if name does not contain a dot, then quit, it is not an instance
      (when pos
        (setq rad (subseq name 0 pos)
            cc (read-from-string (subseq name (1+ pos))))
        ;(format t "~%; %get-next-instance /cc: ~S" cc)
        ;; cc must be a number, otherwise quit
        (when (integerp cc)
          ;; get instance counter (watch for orphans)
          ;; package problems?
          (setq class-id (car (%%get-value id '$TYPE context))) 
          ;(format t "~%; %get-next-instance /context: ~S" context)
          ;(format t "~%; %get-next-instance /class-id: ~S" class-id)
          ;; get class counter
          (setq cmax (%get-class-counter class-id))
          ;(format t "~%; %get-next-instance /cmax: ~S" cmax)
          ;; if (1+ cc) = cmax, obj-id was the last created object
          (if (>= (1+ cc) cmax)
              (return-from %get-next-instance nil))
          ;; if even-if-dead return next object no matter what
          (when even-if-dead
            (setq next-id (intern (format nil "~A.~S" rad (1+ cc)) package))
            (return-from %get-next-instance (%ldif next-id)))
          
          ;; otherwise try to get next alive object
          (loop for count from (1+ cc) to cmax 
              thereis (let ((next-id (%make-id-for-instance class-id count)))
                        ;(format t "~%; %get-next-instance /next-id: ~S" next-id)
                        (if (%alive? next-id context) (%ldif next-id)))))))))

#|
(MOSS::%GET-NEXT-INSTANCE '$E-INFO.5)
$E-INFO.7

AN(123): (MOSS::%GET-NEXT-INSTANCE '$E-INFO.5 :EVEN-IF-DEAD T)
$E-INFO.6
AN(124): (MOSS::%GET-NEXT-INSTANCE '$E-INFO.7)
NIL

(with-package :test
  (with-context 0
    (%get-next-instance 'test::$E-PERSON.1)))
TEST::$E-PERSON.2

(with-package :test
  (with-context 0
    (%get-next-instance 'test::$E-PERSON.2)))
NIL

(with-package :test
  (with-context 4
    (%get-next-instance 'test::$E-PERSON.2)))
TEST::$E-PERSON.3

(with-package :test
  (with-context 4
    (%get-next-instance 'test::$E-PERSON.3)))
NIL

(with-package :test
  (with-context 5
    (%get-next-instance 'test::$E-PERSON.3)))
TEST::$E-PERSON.6

(with-package :test
  (with-context 4
    (%get-next-instance 'test::$E-PERSON.3 :even-if-dead t)))
TEST::$E-PERSON.4
|#          
;;;A----------------------------------------------------- %GET-OBJECT-CLASS-NAME

(defUn %get-object-class-name (obj-id &optional context)
  "get the class names of an object.
Arguments:
   obj-id: object identifier
Return:
   a list of strings representing the names of the classes to which the object
   belongs."
  (let ((context (or context (symbol-value (intern "*CONTEXT*")))))
    (with-context context
      (reduce #'append 
              (broadcast (%%get-value obj-id '$type context)'=get-name)))))

#|
(moss::%get-object-class-name '$e-person.1)
("person")

(with-package :test
  (%get-object-class-name 'test::$e-person.1))
("PERSON")
|#
;;;A--------------------------------------- GET-OBJECTS-FROM-ATTRIBUTE-AND-VALUE
;;; this is to be used in multilingual environments to retrieve objects knowing
;;; an attribute name and a value, part of the MLN value. Because we are in a 
;;; multilingual environment, the attribute reference can be of any language
;;; and the MLN value to be retrieve should either contain a synonym in the 
;;; current language, or in English if there is no entry in the current language
;;; or in any language if there is no entry in the current language nor in English

(defun get-objects-from-attribute-and-value (class-ref att-ref input-value)
  "takes a query and a language and tries to locate an object. Executes first the ~
   query in the context of the specified language, if the result is nil, then tries ~
   English, if the result is nil, then tries any language (:ALL). To be valid the ~
   last result must be such that the MLN does not contain language or English entries.
   This is to avoid retrieving an object with a synonym in a language that does not ~
   correspond to the original data.
Arguments:
  
Return:
   a list of objects corresponding to the original data."
  (let (obj-id-list value result)
    ;; try current language
    (setq obj-id-list (access `(,class-ref (,att-ref :is ,input-value))))
    ;; if it works, quit
    (when obj-id-list
      (return-from get-objects-from-attribute-and-value obj-id-list))
    
    ;; try English, unless *language* is English
    (unless (eql *language* :EN)
      (let ((*language* :EN))
        (setq obj-id-list (access `(,class-ref (,att-ref :is ,input-value)))))
      ;; if there are objects, we must check that they do not have a value associated 
      ;; with the original language
      (dolist (obj-id obj-id-list)
        ;; get the value associated with the attribute
        (setq value (car (send obj-id '=get att-ref)))
        ;; for the object to pass, the value must be an MLN and there should not
        ;; be an entry for *language*
        (if (and (mln::mln? value)
                 (null (mln::extract value :language *language*)))
            (push obj-id result))
        )
      ;; if anything left return it
      (when result
        (return-from get-objects-from-attribute-and-value (reverse result))))
    
    ;; try any other language
    (let ((*language* :all))
      (setq obj-id-list (access `(,class-ref (,att-ref :is ,input-value)))))
    ;; if there are objects, we must check that they do not have a value associated 
    ;; with the original language, nor English
    (dolist (obj-id obj-id-list)
      ;; get the value associated with the attribute
      (setq value (send obj-id '=get att-ref))
      ;; for the object to pass, the value must be an MLN and there should not
      ;; be an entry for *language*
      (if (and (mln::mln? value) ; jpb 1406
               (null (mln::extract value :language *language*)) ; jpb 1406
               (null (mln::extract value :language :EN))) ; jpb 1406
          (push obj-id result))
      )
    ;; if anything left return it
    (when result
      (return-from get-objects-from-attribute-and-value (reverse result)))
    
    ;; otherwise return nil
    nil))

#|
(with-package :test
  (with-context 2
    (get-objects-from-attribute-and-value "person" "name" "barth�s")))
(TEST::$E-PERSON.1 TEST::$E-PERSON.2 TEST::$E-PERSON.4)
|#
;;;--------------------------------------------------------------------- GET-ONE-OF

(defun get-one-of (prop-ref class-ref)
  "return the content of the default area for a property. Nil means no default ~
   value.
Arguments:
   prop-ref: e.g. \"conscience\"
   class-ref: e.g. \"victim\"
Return:
   list of allowed values or nil."
  (let ((prop-id (%%get-id prop-ref :prop :class-ref class-ref)))
    (unless prop-id
      (error "can't find property ~S of class ~S" prop-ref class-ref))
    (%get-value prop-id '$ONEOF)))

#|
(with-package :victim-1
 (with-context 0
   (get-one-of "conscience" "victim")))
("conscient" "semi-conscient" "inconscient")
0

(with-package :victim-1
 (with-context 0
   (get-one-of "conscience" 'victim-1::$e-victim)))
("conscient" "semi-conscient" "inconscient")
0
|#
;;;A--------------------------------------------- %%GET-OBJECTS-FROM-ENTRY-POINT

(defUn %%get-objects-from-entry-point (entry &key keep-entry)
  "Sets up an a-list (inv-prop obj-id) of all objects for which entry is an ~
   entry point, eliminating the current-system (inv prop:$EPLS.OF).
Arguments:
   entry: entry-point
   context (key): context default current
   keep-entry (key): if t keep the entry point
Return:
   a list of pairs (<inv-prop-id> . <obj-id>)  or nil"
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    (when (%pdm? entry)
      (let ((inverse-property-list 
             (delete-duplicates 
              (delete (%inverse-property-id '$EPLS)
                      (%%has-inverse-properties entry context)))))
        (reduce  ; JPB 140820 removing mapcan
         #'append
         (mapcar
             #'(lambda (xx) 
                 (mapcar #'(lambda (yy) (if keep-entry
                                            (list entry xx yy)
                                          (cons xx yy)))
                   (%get-value entry xx context)))
           inverse-property-list))))))

#|
(moss::%%get-objects-from-entry-point 'HAS-PRESIDENT)
((MOSS::$PNAM.OF . $S-PRESIDENT)
 (MOSS::$PNAM.OF . $S-TEACHING-ORGANIZATION-PRESIDENT)
 (MOSS::$PNAM.OF . $S-NON-PROFIT-ORGANIZATION-PRESIDENT))

(moss::%%get-objects-from-entry-point 'HAS-PRESIDENT :keep-entry t)
((HAS-PRESIDENT MOSS::$PNAM.OF $S-PRESIDENT)
 (HAS-PRESIDENT MOSS::$PNAM.OF $S-TEACHING-ORGANIZATION-PRESIDENT)
 (HAS-PRESIDENT MOSS::$PNAM.OF $S-NON-PROFIT-ORGANIZATION-PRESIDENT))

(with-package :test
  (with-context 4
    (%%get-objects-from-entry-point 'test::barth�s)))
((TEST::$T-PERSON-NAME.OF . TEST::$E-PERSON.1) (TEST::$T-PERSON-NAME.OF . TEST::$E-PERSON.2)
  (TEST::$T-PERSON-NAME.OF . TEST::$E-PERSON.3))

(with-package :test
  (with-context 4
    (%%get-objects-from-entry-point 'test::barth�s :keep-entry t)))
((TEST::BARTH�S TEST::$T-PERSON-NAME.OF TEST::$E-PERSON.1)
 (TEST::BARTH�S TEST::$T-PERSON-NAME.OF TEST::$E-PERSON.2)
 (TEST::BARTH�S TEST::$T-PERSON-NAME.OF TEST::$E-PERSON.3))
|#
;;;----------------------------------------------------------------- GET-PARENTS
;;; exported function to obtain the list of parents related to an object specified
;;; by its id

(defun get-parents (obj-id prop-ref parent-class-ref)
  "get the list of parents of an object knowing the property and the class of the ~
   parents.
Arguments:
   obj-id: the object, e.g. $E-WOUND.1
   prop-ref: the property to follow, e.g. \">wounds\"
   parent-class-ref: e.g. \"body part\"
Return:
   a list of parents."
  (declare (special *context*))
  (let (class-id prop-id)
    (setq class-id (car (moss::%%get-value  obj-id 'moss::$type *context*)))
    (setq prop-id (moss::%%get-id-for-inverse-property-knowing-origin
                   prop-ref class-id parent-class-ref))
    (unless prop-id
      (error "can' find the property ~S linking object ~S to parents of class ~S"
        prop-ref obj-id parent-class-ref))
    (moss::%%get-value obj-id prop-id *context*)
    ))

;;;A----------------------------------------------------- %GET-PREVIOUS-INSTANCE

(defun %get-previous-instance (obj-id &key even-if-dead)
  "get the first alive instance created before the specified instance unless the ~
   even-if-dead keyword is true. If the object is the first one returns nil.
   Does not work with classes.
Arguments: 
   obj-id: a symbol, id of an object
   even-if-dead (key): if t returns the next object even if dead or out of context
Returns:
   an object-id or nil."
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    ;; execute only if obj-id is a PDM object otherwise return nil
    (when (%pdm? obj-id)
      ;; first dereference object loading it if needed. resolve either returns the
      ;; resolved reference or the original object, e.g. if unbound or nil valued
      (let* ((id (%resolve obj-id)) 
             (name (symbol-name id))
             (pos (position #\. name :from-end t))
             (package (symbol-package id))
             rad cc class-id previous-id)
        (print `(+++++ ,package))
        ;; if name does not contain a dot, then quit, it is not an instance
        (when pos
          (setq rad (subseq name 0 pos)
              cc (read-from-string (subseq name (1+ pos))))
          ;; cc must be a number, otherwise quit
          (when (integerp cc)
            ;; if (1+ cc) = cmax, obj-id was the first created object
            (if (< (decf cc) 1)
                (return-from %get-previous-instance nil))
            ;; if even-if-dead return next object no matter what
            (when even-if-dead
              (setq previous-id (intern (format nil "~A.~S" rad cc) package))
              (return-from %get-previous-instance (%ldif previous-id)))
            
            ;; otherwise try to get next alive object
            ;; we need class-id
            (setq class-id (car (%%get-value id '$TYPE context)))
            (loop for count from cc downto 1 
                thereis (let ((previous-id (%make-id-for-instance class-id count)))
                          (if (%alive? previous-id context)
                              ;; %alive? load the object if needed
                              previous-id)))))))))

#|
MOSS(173): (%GET-PREVIOUS-INSTANCE 'AN::$E-INFO.7)
ALBERT-NEWS::$E-INFO.3
MOSS(174): (%GET-PREVIOUS-INSTANCE 'AN::$E-INFO.7 :even-if-dead t)
ALBERT-NEWS::$E-INFO.6
MOSS(175): (%GET-PREVIOUS-INSTANCE 'AN::$E-INFO.1)
NIL

(with-package :test
  (with-context 4
    (%GET-PREVIOUS-INSTANCE 'test::$E-PERSON.3)))
TEST::$E-PERSON.2

(with-package :test
  (with-context 5
    (%GET-PREVIOUS-INSTANCE 'test::$E-PERSON.6)))
TEST::$E-PERSON.3

(with-package :test
  (with-context 5
    (%GET-PREVIOUS-INSTANCE 'test::$E-PERSON.6 :even-if-dead t)))
TEST::$E-PERSON.5

(with-package :test
  (with-context 4
    (%GET-PREVIOUS-INSTANCE 'test::$E-PERSON.1 :even-if-dead t)))
NIL
|#
;;;----------------------------------------- %GET-PROP-ID-FROM-NAME-AND-CLASS-REF
;;; deprecated. Use (moss::%%get-id prop-name :prop :class-ref class-ref)
;;; careful this returns a symbol not a list

;;;A------------------------------------------------------------ %GET-PROPERTIES
;********** to check when a property is added to a class in a particular context

;;; a problem is that we can get the same property as a local property and a 
;;; generic property, i.e., $T-PERSON-NAME and $T-NAME. We should only keep
;;; properties that are local to the object.
;;; Amother problem is that a property may be inherited from an ancestor class;
;;; if it has not been redefined locally. E.g., the class BUTCHER may inherit
;;; the property HAS-NAME from the superclass PERSON, i.e. $T-PERSON-NAME.
;;; However, the lattice of properties is not parallele to the lattice of
;;; classes.
;;; For example, if $T-FRENCH-PERSON-NAME is a property of $E-FRENCH-PERSON
;;; that is a subclass of $E-PERSON, we should only keep the first value.

;;;***** must be tested
;;; not sure it does the right thing. Example shows that we get local properties 
;;; but also same properties from ancestors, which might be unwanted.

(defUn %get-properties (object-id &optional version)
  "Obtain all the possible properties for a given object. If an orphan, then ~
   we only get local properties attached to the orphan. Otherwise, we get ~
   all inherited properties corresponding to the object class and superclasses.
Arguments:
   object-id: identifier of object
   context (opt): context (default current)
Return:
   a list of properties associated with the object."
  ;(format t "~%;--- %get-properties/ version: ~S" version)
  ;(format t "~%;--- %get-properties/ *package*: ~S" *package*)
  
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    ;(format t "~%;--- %get-properties/ (intern \"*CONTEXT*\"): ~S" 
    ;  (intern "*CONTEXT*"))
    ;(format t "~%;--- %get-properties/ (symbol-value (intern \"*CONTEXT*\")) ~S"
    ;  (symbol-value (intern "*CONTEXT*")))
    ;(format t "~%;--- %get-properties/ context: ~S" context)
    
    ;; the function shoud work even if the object is dead (when an object is dead
    ;; it has a tombstone in the current context.
    ;; when object is dead %%alive throws to :error
    ;(setq object-id (%%alive? object-id *context*)) ; JPB 121124
    ;(setq object-id (%alive? object-id))
    (if
        (%is-classless-object? object-id)
        ;; returns the list of all props. Object is loaded by send function
        (mapcar #'car (symbol-value object-id))
      ;; otherwise uses its model
      ;+93/07/03 changed to improve efficiency (note that we use reduce append mapcar
      ; rather than mapcan which is buggy in this version (builds circular lists)
      ;; when an object is dead (tombstone) %get-value always returns nil. We must
      ;; then get the class(es) directly
      (let* ((prop-list 
              (delete-duplicates
               (reduce 
                #'append 
                (mapcar #'(lambda(xx)
                            (append
                             (%get-value xx '$PT)
                             (%get-value xx '$ps)))
                  (%sp-gamma-l (%%get-value object-id '$TYPE context) '$IS-A)))))
             )
        
        ;(format t "~%;--- prop-list: ~S" prop-list)
        ;; we must check if some properties share the same generic value and keep
        ;; the closest to the object 
        (%remove-redundant-properties prop-list) 
        ))))

#|
(with-package :test
  (with-context 3
    (%get-properties 'test::$E-PERSON.2)))
($T-PERSON-NAME $T-PERSON-EMAIL $T-PERSON-FIRST-NAME $T-PERSON-AGE $S-PERSON-EMPLOYER
 $S-PERSON-COUSIN $S-PERSON-FATHER $S-PERSON-HUSBAND $S-PERSON-MOTHER $S-PERSON-BROTHER
                $S-PERSON-SISTER $S-PERSON-WIFE)

(with-package :test
  (with-context 2
    (%get-properties 'test::$E-STUDENT.1)))
($S-STUDENT-TEACHER $T-PERSON-NAME $T-PERSON-EMAIL $T-PERSON-FIRST-NAME $T-PERSON-AGE
 $S-PERSON-EMPLOYER $S-PERSON-COUSIN $S-PERSON-FATHER $S-PERSON-HUSBAND $S-PERSON-MOTHER
 $S-PERSON-BROTHER $S-PERSON-SISTER $S-PERSON-WIFE)
|#
;;;--------------------------------------- %GET-PROPERTY-CANONICAL-NAME-FROM-REF
;;; deprecated. Use (%make-string-from-ref ref)

;;;A------------------------------------------------- %GET-PROPERTY-ID-FROM-NAME
;;; deprecated. Use %get-property-id-from-ref instead.

;;***** should be called %get-local-prop-id-for-instance

(defUn %get-property-id-from-name (obj-id prop-ref &optional version)
  "Function that has the difficult role of finding the right property that ~
   applies to the object (instance), and corresponds to the prop-ref. This is ~
   difficult in case we have tree-properties. 
   It is used by elementary universal methods like =get =put =delete
   Result is cached onto the p-list of the property name (e.g. HAS-NAME).
Arguments:
   obj-id: object for which we try to determine property
   prop-ref: property ref, e.g. HAS-FIRST-NAME, \"first name\", or mln
   context (opt): context default current
Return:
   the identifier of the corresponding property or nil if the class inherits property."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (setq obj-id (%%alive? obj-id context)) ; throws if false
    (let* ((prop-name (if (symbolp prop-ref) 
                          prop-ref 
                        (%make-name-for-property prop-ref)))
           (obj-type (%type-of obj-id context)) ; is a list
           (prop-data (get prop-name (car obj-type)))
           )
      ;; when cached onto the p-list return
      ;;*** this is not such a good idea. When redefining property at class level
      ;; introduces some strange errors
      (if prop-data (return-from %get-property-id-from-name prop-data))
      ;; otherwise, go get it
      (setq prop-data (%determine-property-id obj-id prop-name))
      ;; %determine-property-id always return a list
      (when (and (listp prop-data) (cdr prop-data))
        (verbose-warn "ambiguous property ~S for object ~S in package ~S and ~
                 context ~S. we take the first in the list: ~&~S"
                      prop-name obj-id *package* context prop-data)
        ;;***** actually we should take generic property
        )
      ;; do that for all types (should be checked)...
      ;; may be we should do it for direct properties and not for inverses
      ;; indeed there may be several inverse properties pointing to a specific class
      ;; e.g. ($E-CELL-PHONE $S-PERSON-PHONE.OF $E-OFFICE-PHONE $S-PERSON-PHONE.OF
      ;;       $E-HOME-PHONE $S-PERSON-PHONE.OF)
      ;; $E-OFFICE-PHONE may be the phone of a person but also of an organization...
      (unless (%is-inverse-property? (car prop-data))
        (dolist (type obj-type)
          (setf (get prop-name type) (car prop-data))))
      (car prop-data))))

#|
(with-package :test
  (%get-property-id-from-name 'test::$E-PERSON.1 "name" 1))
TEST::$T-PERSON-NAME

(with-package :test
  (with-context 3
    (%get-property-id-from-name 'test::$E-STUDENT.1 "name")))
TEST::$T-PERSON-NAME

(symbol-plist 'test::has-name)
(TEST::$E-STUDENT TEST::$T-PERSON-NAME 
                  TEST::$E-PERSON TEST::$T-PERSON-NAME 
                  EXCL::.ARGS. (0 . 1)
                  EXCL::SETF-METHOD-EXPANDER 
                  #<Interpreted Function (EXCL::SETF-METHOD-EXPANDER TEST::HAS-NAME)>
                  :PDM T)
|#
;;;A-------------------------------------------------- %GET-PROPERTY-ID-FROM-REF
;;; should replace %get-property-id-from-name

(defUn %get-property-id-from-ref (obj-id prop-ref &optional version)
  "Function that has the difficult role of finding the right property that ~
   applies to the object, and corresponds to the prop-ref. This is ~
   difficult in case we have tree-properties. 
   It is used by elementary universal methods like =get =put =delete
   Result is cached onto the p-list of the property name (e.g. HAS-NAME).
Arguments:
   obj-id: object for which we try to determine property
   prop-ref: property ref, e.g. HAS-FIRST-NAME, \"first name\", or mln
   version (opt): context (default current)
Return:DE
   the identifier of the corresponding property or nil."
  (let ((context (or version (symbol-value (intern "*CONTEXT*"))))
        (none (intern "*NONE*")))
    (setq obj-id (%%alive? obj-id context))
    (let (id type-list)
      ;; get property class-id
      (setq type-list (%get-value obj-id '$TYPE context))
      ;; if orphan, return generic property
      (when (eql (car type-list) none)
        (return-from %get-property-id-from-ref (%%get-id prop-ref :prop)))
      
      ;; otherwise try classes in turn
      (dolist (class-id type-list)
        (setq id (%%get-id prop-ref :prop :class-ref class-id))
        (if id (return-from %get-property-id-from-ref id))))))

#|
(moss::%get-property-id-from-ref 'test::$E-STUDENT.1 'name)
$T-PERSON-NAME

(with-package :test
  (%get-property-id-from-ref 'test::$E-PERSON.1 "name" 2))
TEST::$T-PERSON-NAME

(with-package :test
  (with-context 3
    (%get-property-id-from-ref 'test::$E-STUDENT.1 "name")))
TEST::$T-PERSON-NAME

(with-package :test
  (with-context 3
    (%get-property-id-from-ref 'test::$E-STUDENT.1 'test::HAS-NAME)))
TEST::$T-PERSON-NAME

(with-package :test
  (with-context 3
    (%get-property-id-from-ref 'test::$E-STUDENT.1 'test::NAME)))
TEST::$T-PERSON-NAME

(with-package :test
  (with-context 3
    (%get-property-id-from-ref 'test::$E-STUDENT.1 '((:en "name")(:fr "nom")))))
TEST::$T-PERSON-NAME
|#
;;;-------------------------------------------------- %GET-PROPERTY-NAME-FROM-ID

(defun %get-property-name-from-id (prop-id)
  "extract name from the property object in the current context and with the ~
   current language."
  (unless (%pdm? prop-id)
    (mformat "Unknown property ~S in package ~S and context ~S"
             prop-id (package-name *package*) *context*)
   (return-from %get-property-name-from-id nil))
  (%%make-name (mln::get-canonical-name (car (%get-value prop-id '$pnam))) :prop))

#|
(%get-property-name-from-id '$t-aa)
HAS-AA
:INTERNAL
(%get-property-name-from-id '$t-test-ab)
HAS-AB
:INTERNAL
|#
;;;?----------------------------------------------------- %GET-PROPERTY-SYNONYMS
;;; For backward compatibility should work with olf MLN format
;;; does not seem right when a property is shared by several classes, e.g. name
;;; the name of a person may have a number of synonyms, but the name of an
;;; organization different ones, thus the list of synonyms is meaningless

(defUn %get-property-synonyms (prop-ref &optional version)
  "takes a property reference and return the list of synonyms for this property ~
   in current language (*language*).
Arguments:
   prop-ref: a property reference, e.g. \"street\"
   version (opt): context (default current)
Return;
   nil or the list of all property synonyms."
  (let ((context (or version (symbol-value (intern "*CONTEXT*"))))
        prop-name prop-list mln-list expr-list)
    ;; normalize prop name
    (setq prop-name (%make-name-for-property prop-ref))
    
    ;; get names
    (setq prop-list
          (send prop-name '=get-id (moss::%inverse-property-id 'moss::$PNAM)))
    ;; get mlns
    (setq mln-list 
          (mapcar #'(lambda (xx)(car (%%get-value xx 'moss::$PNAM context)) )
            prop-list))
    ;; filter languages
    (setq expr-list ; jpb 1406
          (mapcar #'(lambda (xx) (mln::filter-language xx *language*)) mln-list))
    ;; clean up
;;;    (delete-duplicates 
;;;     (reduce  ; JPB 140820 removing mapcan
;;;      #'append (mapcar #'moss::%synonym-explode expr-list))
;;;     :test #'string-equal)
    ;; takes into account the old MLN format
    (delete-duplicates 
     (reduce
      #'append 
      (mapcar #'(lambda(xx) (if (stringp xx) (%synonym-explode xx) xx)) expr-list))
     :test #'string-equal)
    ))

#|
(with-package :test
  (let ((*language* :en))(%GET-PROPERTY-SYNONYMS "name")))
("name" "family name" "surname" "last name")
|#
;;;A------------------------------------------------------ %GET-RELEVANT-WEIGHTS

(defUn %get-relevant-weights (weight-list word-patterns &key used-patterns)
  "function to obtain weigths for a list of words.
Arguments:
   weight-list: list of weights e.g. ((<string> .6))
   word-patterns: the list of expressions (strings) to test
   used-patterns (key): expressions that have already been tried
Return:
   a list of pairs string weight."
  (declare (ignore used-patterns))
  (let (tested-patterns result entry)
    ;(vformat "%get-relevant-weights /input patterns:~& ~S" word-patterns)
    ;; first compute the pattern list, returns (("xxx" start end)*)
    ;;;    (if used-patterns 
    ;;;      (setq word-patterns (set-difference word-patterns used-patterns 
    ;;;                                          :test #'string=)))
    ;(trformat "%get-relevant-weights /patterns to be tested:~& ~S" word-patterns)
    
    ;; test each expression of the word-patterns
    (dolist (pattern word-patterns)
      ;(vformat "%get-relevant-weights /testing: ~S against:~&   ~S~&result: ~S"
      ;         pattern weight-list 
      ;         (assoc pattern weight-list :test #'string-equal))
      ;; save pattern
      (push pattern tested-patterns)
      ;; check for pattern in the weight list
      (setq entry (assoc pattern weight-list :test #'string-equal))
      ;; if we have an entry, then we must remove the entry from the initial 
      ;; compute a new list of patterns, remove the ones that have been 
      ;; checked and try again
      (if entry (push entry result)))
    ;(trformat "%get-relevant-weights /result: ~S" result)
    (reverse result)))

#|
(moss::%get-relevant-weights '(("what-is" 0.4) ("what-are" 0.5) 
                                 ("explain" 0.5) ("define" 0.5)("means" 0.4) ("meaning" 0.4))
                               '("define" "what" "an" "address" "means"))
(("define" 0.5) ("means" 0.4))

(moss::%get-relevant-weights   ; same thing with combination of words (patterns)
   '(("what-is" 0.4) ("what-are" 0.5) 
     ("explain" 0.5) ("define" 0.5)("means" 0.4) ("meaning" 0.4))
   (mapcar #'car (moss::generate-access-patterns 
                  '("define" "what" "an" "address" "means"))))
(("define" 0.5) ("means" 0.4))
|#
;;;------------------------------------------- %GET-SP-ID-FROM-NAME-AND-CLASS-REF
;;; deprecated. Use (%%get-id prop-name :sp :class-ref clas-ref)

;;;---------------------------------------------------- %GET-SP-VALUE-RESTRICTION

;;; The concept of restriction is fairly complex and concerns the values 
;;; associated with an attribute.
;;; If nothing is specified there is no restriction on the value: i.e. it may
;;; be of any type represented in Lisp.
;;; The following cases may apply
;;;
;;;    :type       ; values should be of that type ($SUC)
;;;    :not-type   ; no value should be of that type (probably useless)
;;;    :value      ; only this value is allowed (individual)
;;;    :forall     ; all values should be of the specified type
;;;    :exists     ; at least one of the values must be of the specified type
;;;    :one-of     ; the value should be a member of this list (individuals)
;;;       :forall  ; all values should be in the one-of list
;;;       :exists  ; one of the values must be in the one-of list
;;;       :not     ; the value should not be one of this list
;;;    :filter     ; user defined filter (single arg function)
;;;    :same       ; all values should be the same
;;;    :different  ; all values should be different
;;;
;;;    :min        ; min number of values
;;;    :max        ; max number of values
;;;    :unique     ; only one value
;;;    
;;; Note that some of the restrictions apply to a single value and can be checked
;;; when adding the value, but exists can only be checked when all values have 
;;; have been provided.
;;; Examples:
;;;    ((:one-of _red _joe) (:type "butcher") (:exists))
;;;   means that all values should be butchers with at list one from the list
;;;    ((:one-of _jpb _mgbl)) 
;;;   all successors should be in that list (says nothing about redundant values)

;;; PDM representation:
;;;   :type       ($SUC (0 $E-PERSON))
;;;   :not-type   ($NSUC (0 $E-BUTCHER))
;;;   :value      ($SUCV (0 _jpb))
;;;   :one-of     ($ONESUCOF (0 _jpb _mgbl)) ($SEL :exists))
;;;   :filter, :same, :different    ($OPR (0 :xxx 3 5 ...))
;;;   :min, :max, :unique  ($MIN 1) ($MAX 1)     

(defUn %get-sp-value-restrictions (prop-id )
  "Obtains the value restrictions from the attribute representation. No ~
   inheritance is done.
   Restrictions are 
    :exists     ; among the possible values one must be of that type
    :forall     ; all values should be of that type
    :type       ; values should be of that type
    :not-type   ; no value should be of that type
    :value      ; only this value is allowed
    :one-of     ; the value should be a member of this list
    :not        ; the value should not be one of this list
    :min        ; no less than min successors
    :max        ; no more than max successors
Arguments:
   prop-id: attribute id
Returns:
   a list of restrictions: e.g. ((:one-of 1 2 3) (:type :integer) (:one))."
  (declare (special *relation-restrictions*))
  (let (result)
    (when (%is-relation? prop-id)
      (dolist (pair (symbol-value prop-id))
        (if (member (car pair) *relation-restrictions*)
           (push pair result)))
      (reverse result))))

#|
;; No good does not take into account versions
$S-PERSON-BROTHER
(($TYPE (0 $EPS)) ($ID (0 $S-PERSON-BROTHER)) ($PNAM (0 (:EN "Brother")))
 ($ESLS.OF (0 $MOSSSYS)) ($IS-A (0 $S-BROTHER)) ($INV (0 $S-BROTHER.OF))
 ($PS.OF (0 $E-PERSON)) ($SUC (0 $E-PERSON)))

(%get-sp-value-restrictions '$S-PERSON-BROTHER)
(($SUC (0 $E-PERSON)))
|#
;;;A-------------------------------------------------------- %GET-SUBCLASSE-NAMES
;;; should test with a subclass efined in a different package

(defUn %get-subclass-names (class-ref)
  "takes a class reference and return the list of synonyms for all subclasses, ~
   including the referenced class.
Arguments:
   class-ref: a class reference, e.g. \"address\"
Return;
   nil or the list of all subclass names."
  (let ((class-id (%%get-id class-ref :class))
        subclass-list name-list)
    (when class-id
      (setq subclass-list (%get-subclasses class-id))
      (setq name-list (mapcar #'(lambda (xx)
                                  (mln::filter-language  ; jpb 1406
                                   (car (send xx '=get-id '$ENAM))
                                   *language*))
                        subclass-list))
      (reduce  ; JPB 140820 removing mapcan
       #'append
       (mapcar #'%synonym-explode name-list)))))

#|
? (MOSS::%GET-SUBCLASS-NAMES "address")
("address" "postal address" "home address" "office address" "professional address"
 "email address" "email")
? (with-package :address (%get-subclass-names "person"))
("personne" "�tudiant")

(with-package :test
  (%get-subclass-names "person"))
("PERSON" "STUDENT")
|#
;;;+------------------------------------------------------------- %GET-SUBCLASSES
;;;*********** should be sensitive to versions

(defUn %get-subclasses (class-id)
  "returns the list of subclasses of class-id.
Argument:
   class-id: a class identifier
Return:
   nil or the transitive closure along the inverse $IS-A property."
  (setq class-id (%resolve class-id)) ; a class cannot belong to several classes...
  (%sp-gamma class-id (%make-id '$EIL :id '$IS-A)) ; brute force
  )
#|
(with-package :test
  (%GET-SUBCLASSES (%%get-id "person" :concept)))
(TEST::$E-PERSON TEST::$E-STUDENT)
|#
;;;------------------------------------------- %GET-TP-ID-FROM-NAME-AND-CLASS-REF
;;; deprecated. Use (%%get-id prop-name :tp :class-ref class-ref)

;;;A-------------------------------------------------- %GET-TP-VALUE-RESTRICTIONS
;;;***** should be version sensitive *****

;;; The concept of restriction is fairly complex and concerns the values 
;;; associated with an attribute.
;;; If nothing is specified there is no restriction on the value: i.e. it may
;;; be of any type represented in Lisp.
;;; The following cases may apply
;;;
;;;    :type       ; values should be of that type
;;;    :not-type   ; no value should be of that type (probably useless)
;;;    :value      ; only this value is allowed
;;;    :one-of     ; the value should be a member of this list
;;;       :forall  ; all values should be in the one-of list
;;;       :exists  ; one of the values must be in the one-of list
;;;       :not     ; the value should not be one of this list
;;;    :between    ; the numerical value should be in that range
;;;    :outside    ; the numerical value should be outside that range
;;;    :filter     ; user defined filter (single arg function)
;;;    :same       ; all values should be the same
;;;    :different  ; all values should be different
;;;
;;;    :min        ; min number of values
;;;    :max        ; max number of values
;;;    :unique     ; only one value
;;;    
;;; Note that some of the restrictions apply to a single value and can be checked
;;; when adding the value, but exists can only be checked when all values have 
;;; have been provided.
;;; Examples:
;;;    ((:one-of 1 2 3) (:type :integer) (:exists))
;;;   means that all values should be integers with at list one from the list
;;;    ((:one-of 1 2 3)) 
;;;   all values should be in that list (says nothing about redundant values

;;; PDM representation:
;;;   :type       ($TPRT (0 :integer))
;;;   :not-type   ($NTPR (0 :integer))
;;;   :value      ($VALR (0 "Albert"))
;;;   :one-of     ($ONEOF (0 1 2 3)) ($SEL :exists))
;;;   :between, :outside, :filter, :same, :different    ($OPR (0 :xxx 3 5 ...))
;;;   :min, :max, :unique  ($MIN 1) ($MAX 1)     

(defUn %get-tp-value-restrictions (prop-id &optional version)
  "Obtains the value restrictions from the attribute representation. No ~
   inheritance is done.
   Restrictions are 
    :exists     ; among the possible values one must be of that type
    :forall     ; all values should be of that type
    :type       ; values should be of that type
    :not-type   ; no value should be of that type
    :value      ; only this value is allowed
    :one-of     ; the value should be a member of this list
    :not        ; the value should not be one of this list
    :between    ; the numerical value should be in that range
    :outside    ; the numerical value should be outside that range
Arguments:
   prop-id: attribute id
   version (opt): context (default current)
Returns:
   a list of restrictions: e.g. ((:one-of 1 2 3) (:type :integer) (:one))."
  (let ((context (or version (symbol-value (intern "*CONTEXT*"))))
        result)
    ;(format t "~%; %get-tp-value-restrictions/att prop-id?: ~S" 
    ;  (%is-attribute? prop-id))
    
    (when (%is-attribute? prop-id context)
      (dolist (pair (symbol-value prop-id))
        (case (car pair)
          ;; seems that only $ONEOF and $SEL are used
          (($TPRT $NTPR $VALR $ONEOF $SEL $OPR $MAXT $MINT $OPRALL)
           ;(format t "~%; %get-tp-value-restrictions/(car pair): ~S" (car pair))
           (if (%%get-value prop-id (car pair) context) 
               (push pair result)))))
      (reverse result))))

#|
$T-SELLER1-NAME
(($TYPE (0 $EPT)) ($ID (0 $T-SELLER1-NAME)) ($PNAM (0 (:EN "name")))
 ($ETLS.OF (0 $MOSSSYS)) ($IS-A (0 $T-NAME)) ($INV (0 $T-NAME.OF))
 ($PT.OF (0 $E-SELLER1)) ($ONEOF (0 "Joe" "Judith")) ($SEL (0 :ONE-OF)))

(%get-attribute-value-restrictions '$T-SELLER1-NAME)
(($ONEOF (0 "Joe" "Judith")) ($SEL (0 :ONE-OF)))

(with-package :test 
  (%get-tp-value-restrictions 'test::$t-organization-name))
(($ONEOF (0 "UTC" "PUCPR")) ($SEL (0 :EXISTS)))
|#
;;;A----------------------------------------------------------------- %GET-VALUE
;;; should modify it when value is an MLN, adding optional key parameters
;;; standard =get will return canonical value if *language* is bound
;;;   if *language* is not bound will default to :EN
;;; if :all will return all synonyms for a given language
;;; if :raw will return MLN
;;; when %%get-value returns nil, then %get-value should return a default value
;;; currently the difference is just the %alive? test.

(defUn %get-value (obj-id prop-id &optional version)
  "Gets the value corresponding to the current context. To do so uses ~
   the *version-graph* to locate within a branch the latest valid value. ~
   The context graph is organized as an a-list of nodes, each node points to ~
   its predecessors: ((9 6)(8 3 2)(7 6)...(0)) it contains all contexts, ~
   context numbers are not necessarily increasing along a branch, e.g. if we have ~
   fused some contexts. 
    Algorithm: (i) gets the branch from the current context (if legal) to the root. ~
   (the branch is computed only once for a given context and cached onto the ~
   p-list of the global symbol *version-graph*). ~
   (ii) goes up in the branch, returning the first non nil value associated with ~
   prop-id in the object.
   The exact property of the object (prop-id) must be used, not the generic one.
   %get-value does not return defaults.
   If context is illegal throws to an :error tag.
Arguments:
   obj-id: identifier of object
   prop-id: identifier of  property
   version (opt): specifies a context
Return:
   1. the list of values associated with prop-id in current context
   2. a flag, if NIL, the value was not present in any reachable context."
  (declare (special *package*))
  
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    ;(format t "~%;--- ~S" (symbol-value (intern "*CONTEXT*")))
    ;; we must specify local context
    (setq obj-id (%alive? obj-id context)) ; checks context
    
    ;; check binding (not done by %alive? in boot-mode) In boot mode context is 0
    (unless (boundp obj-id)
      (mthrow "~S should be bound in package ~S context ~S."
              obj-id (package-name *package*) context))
    
    ;; next function checks for legal context, which should be OK here, but 
    ;; considers obj-id to be a simple a-list
    (%%get-value obj-id prop-id context)
    ))

#|
;; Test dans le package TEST
(setq *version-graph*
      '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3
(defindividual "Person" (:var _al))

(with-package :test
  (print *package*)
  ;; because we are in a MOSS package here, we must prefix the variables with test
  ;; if we want the values taken in the test package
  (setq test::$E-person.3 
        '((MOSS::$TYPE (0 $E-PERSON)) 
          (MOSS::$ID (0 test::$E-PERSON.3))
          (test::$t-PERSON-NAME (0 "Al")(1 "Bernard")(3 "Charles")(5 "Ernest")
                                (6 "Fran�ois" "Fr�d�ric")))))
(with-package :test
  (with-context 0
    (%get-value 'test::$E-PERSON.3 'test::$t-person-name)))
("Al")
0

(with-package :test
  (with-context 3
    (%get-version-path-to-root test::*context*)))
(3 2 1 0)

(with-package :MOSS
  (with-context 3
    (%get-version-path-to-root *context*)))
(3 0)

(with-package :moss
  (with-context 3
    (%get-value 'test::$E-PERSON.3 'test::$t-person-name)))
("Charles")
3

(with-package :test
  (with-context 3
    (%get-value 'test::$E-PERSON.3 'test::$t-person-name)))
("Charles")
3

(with-package :test
  (with-context 4
    ;; should prefix values
    (%get-value 'test::$E-PERSON.3 'test::$t-person-name)))
("Al")
NIL

(with-package :test
  (with-context 6
    (%get-value 'test::$E-PERSON.3 'test::$t-person-name)))
("Fran�ois" "Fr�d�ric")
6
|#
;;;A---------------------------------------------------------------- %%GET-VALUE
;;; Very crude function working directly on the object structure, no semantics.
;;; For a dead object %get-value always returns nil, %%get-value can return the
;;; value for the class, id or tombstone value
;;; This function works even if the object is dead (useful for recovering $TMBT)
;;; When there is no value, distinguishes between 2 cases:
;;;  - we do not know if there is a value (returns NIL NIL)
;;;  - we know ther is no value  (returns (NIL <nn>)

(defUn %%get-value (obj-id prop-id context)
  "Returns the value associated with a particular context using the *version- ~
   graph*. If context is illegal throws to an :error tag.
   Like %get-value but does not require the object to have a property $TYPE.
Arguments:
   obj-id: must be an object identifier and the object must be in core
   prop-id: identifier of local property (must be a symbol)
   context: context (must be an integer)
Return: 
   1. the value associated with prop-id
   2. a flag: if NIL indicates that the value was NOT specified in this context ~
        otherwise returns the version number to specify there is no value."
  (declare (inline %%allowed-context? %get-version-path-to-root))
  ;; check for illegal context. If illegal throws to :error
  (%%allowed-context? context)
  
  ;; %%get-value works with any a-list, not necessarily a PDM object
  ;; get all versions attached to the property "(getv" is macro "(cdr (assoc"
  ;; data will contain the list of all versions of the object corresponding to
  ;; to prop-id	
  (let* ((data (getv prop-id (symbol-value obj-id)))
         (val (getv context data))
         result context-there?)
    ;; if val is non nil, then a value exists in the specified context
    ;; same if val is nil but prop-id is there, e.g. (4) meaning that there is
    ;; no value associated with this property, a known fact
    (setq context-there? (car (assoc context data)))
    (if (or val (car (assoc context data))) 
        (return-from %%get-value (values val context-there?)))
    
    ;; if data is nil, then there is no value attached to that property
    ;; otherwise, there may be a value in a subsuming context
    (when data      
      ;; move up along the context branch from the current context to the root
      ;; until hitting a value recorded in data
      ;; %get-version-path-to-root returns nil if context is illegal
      ;; uses vp-gamma and local *version-graph*
      (dolist (ctxt (cdr (%get-version-path-to-root context)))
        (if (assoc ctxt data)
            ;(return (setq result (cdr (assoc ctxt data)))))))
            (return-from %%get-value (values (cdr (assoc ctxt data)) ctxt)))))

    ;; we return a result specifying that it was not specified in that context
    (values result nil)
  ))

#|
(setq *version-graph* '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))

;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3

(setq id
      '(($TYPE test)
        (a (0 a0))
        (b (0 b0 bb0)(1 b1))
        (c (4))
        (e (5 e5))
        (f (0 f0)(1 f1)(2 f2)(3 f3)(4 f4)(5 f5)(6 f6))))

(%%get-value 'id 'a 0)
(A0)
0

(%%get-value 'id 'a 1)
(A0)
0

(%%get-value 'id 'b 0)
(B0 BB0)
0

(%%get-value 'id 'b 1)
(B1)
1

(%%get-value 'id 'b 3)
(B1)
1

(%%get-value 'id 'b 5)
(B0 BB0)
0

(%%get-value 'id 'e 5)
(E5)
5

(%%get-value 'id 'e 6)
NIL
NIL
;;- this means that we do not know the value

(%%get-value 'id 'f 3)
(F3)
3

(%%get-value 'id 'c 3)
NIL
NIL

(%%get-value 'id 'c 4)
NIL
4

(%%get-value 'id 'c 6)
NIL
4
;; error since we inherit from 4 where we  know that there is NO value

(catch :error (%%get-value 'id 'f 7))
;*** MOSS-error context 7 is illegal in package: #<The MOSS package>.
NIL

(symbol-plist '*version-graph*)
(6 (6 4 0) 5 (5 4 0) 3 (3 2 1 0) 4 (4 0) 2 (2 1 0) 1 (1 0) 0 (0) 
 EXCL::%VAR-DOCUMENTATION "configuration lattice")
|#
;;;A---------------------------------------------------- %%GET-VALUE-AND-CONTEXT

(defUn %%get-value-and-context (obj-id prop-id)
  "Done to obtain value for objects which must not be versioned, like counters ~
   for entities. 
Arguments:
   obj-id: object identifier
   prop-id: local property identifier.
Retrurn:
   a list whose car is context and cadr is the value."
  (cadr (assoc prop-id (symbol-value obj-id))))

#|
(%%get-value-and-context '$ENT '$ENAM)
(0 ((:EN "MOSS-CONCEPT" "MOSS-ENTITY" "MOSS-CLASS")))

(with-package :test
  (%%get-value-and-context test::_jpb 'test::$T-person-name))
(0 "Barth�s")
|#
;;;A-------------------------------------------------- %GET-VALUE-FROM-CLASS-REF
;;; not version sensitive

(defUn %get-value-from-class-ref (class-ref expr)
  "returns the value associated with class-ref from a MOSS pattern.
Arguments:
   class-ref: e.g. \"address\"
   expr: a MOSS pattern
Returns:
   value or nil."
  ;; tries to find a value corresponding to one of the synonyms
  (some #'(lambda (xx) (cdr (assoc xx expr :test #'equal+)))
        (%get-subclass-names class-ref)))

#|
(let ((*language* :EN))
    (moss::%GET-VALUE-FROM-CLASS-REF 
     "postal address" 
     '(("postal address" ("street" "rue Roger Couttolenc") ("town" "Compi�gne") 
        ("zip" "60200")))))
(("street" "rue Roger Couttolenc") ("town" "Compi�gne") ("zip" "60200"))
|#
;;;A----------------------------------------------- %GET-VALUE-FROM-PROPERTY-REF
;;; not version sensitive

(defUn %get-value-from-prop-ref (prop-ref expr)
  "returns the value associated with prop-ref from a MOSS pattern.
Arguments:
   prop-ref: e.g. \"town\"
   package (key): package in which property has been defined
Returns:
   value or nil."
  ;; tries to find a value corresponding to one of the synonyms
  (some #'(lambda (xx)  (cdr (assoc xx expr :test #'equal+)))
        (moss::%get-property-synonyms prop-ref)))

#|
(moss::%GET-VALUE-FROM-prop-ref "town" '(("street" "rue Roger Couttolenc") ("town" "Compi�gne") ("zip" "60200")))
("Compi�gne")

(moss::%GET-VALUE-FROM-prop-ref "city" '(("street" "rue Roger Couttolenc") ("town" "Compi�gne") ("zip" "60200")))
("Compi�gne")
|#
;;;A---------------------------------------------- %GET-WORDS-FROM-JAPANESE-TEXT
;;; This segmentation is fairly crude. A better one is needed.

;;; we need the mecab package to keep the compiler from complaining

;;;(eval-when (:compile-toplevel :load-toplevel :execute)
;;;  (unless (find-package :mecab)(make-package :mecab)))

(defUn %get-words-from-japanese-text (text delimiters &optional chars-to-keep)
  "takes an input string and a set of delimiters and returns a list of words ~
   separated by spaces.
Arguments:
   text: a string
   delimiters: a set of characters that delimit a word
   chars-to-keep (optional): list of characters to keep in the resulting list of
                             words
Return:
   a list of words."
  (declare (ignore delimiters chars-to-keep text))
  (error "should edit the %get-words-from-japanese-text function and add the ~
          mecab library to handle Japanese dialogs.")
  ;; call the Japanese sparse function
  ;(mapcar #'car (mecab::sparse-tostr text))
  )

#|
(%get-words-from-japanese-text "abc, de." '(#\space #\, #\.))
("a" "b" "c" "d" "e")
|#
;;;A-------------------------------------------------- %GET-VERSION-PATH-TO-ROOT

(defun %get-version-path-to-root (context)
  "returns a list giving the path from current context to context 0 or nil if ~
   the context is illegal. Caches the path on the local *version-graph*."
  (let* ((vg (intern "*VERSION-GRAPH*"))
         (version-graph (symbol-value vg))
         context-path)
    (cond
     ;; in case context is illegal but nevertheless on the plist of *version-graph*
     ((not (assoc context version-graph)) nil)
     ((get vg context))
     (t
      (setq context-path (%vg-gamma context))
      ;; should be computed only once and cached on the plist of *version-graph*
      (setf (get vg context) context-path)))))

#|
(setq *version-graph*
   '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3
(%get-version-path-to-root 0)
(0)
(%get-version-path-to-root 3)
(3 2 1 0)
(%get-version-path-to-root 5)
(5 4 0)
(%get-version-path-to-root 7)
NIL
|#
;;;A------------------------------------------------------- %GET-WORDS-FROM-TEXT
   
(defUn %get-words-from-text (text delimiters &optional chars-to-keep)
  "takes an input string and a set of delimiters and returns a list of words ~
   separated by spaces.
Arguments:
   text: a string
   delimiters: a set of characters that delimit a word
Return:
   a list of words."
  (let (result (start 0))
    ;; sweep text removing delimiters
    (dotimes (jj (length text))
      ;; start is an anchor at the beginning of a word
      ;; check current char
      (when (member (char text jj) delimiters)
        ;; test then if it is the char at the anchor place
        (if (eql jj start)
            ;; move start one position
            (incf start)
          ;; otherwise we are at the end of a word, get word
          (progn
            (push (subseq text start jj) result)
            ;; and move start past the word
            (setq start (1+ jj))))
        ;; if the delimiter char is one to keep insert it into result
        (if (member (char text jj) chars-to-keep)
            (push (format nil "~A" (char text jj)) result))        
        )
      ;; if we are at the end of the string and did not find any delimiter,
      ;; we must return the last word
      ;(trformat "start: ~S, jj: ~S" start jj) 
      (if (and (eql jj (1- (length text)))(<= start jj))
          (push (subseq text start) result)))
    ;; return the sentence in the right order
    (reverse result)))

#|
(%get-words-from-text "I thintk, that i'd like to have dominique's phone number.  " 
                        '(#\space #\, #\' #\.))
("I" "thintk" "that" "i" "d" "like" "to" "have" "dominique" "s" "phone" "number")

(moss::%get-words-from-text "hello" moss::*delimiters*)
("hello")

(moss::%get-words-from-text "define address ?" moss::*delimiters*)
("define" "address" "?")

(moss::%get-words-from-text "Avis: vive l'Europe!" moss::*delimiters*)
("Avis" "vive" "l" "Europe")

(%get-words-from-text "I thintk, that i'd like to have dominique's phone number.  " 
                        '(#\space #\, #\' #\.) '(#\,))
("I" "thintk" "," "that" "i" "d" "like" "to" "have" "dominique" ...)

(%get-words-from-text "contacts avec la chine?" 
                        '(#\space #\, #\' #\. #\?) '(#\?))
("contacts" "avec" "la" "chine" "?")
|#
;;;=============================================================================
;;;                         HAS functions
;;;=============================================================================

;;; normally those functions apply to the object itself, discarding any inherited
;;; values through the use of context graph

;;;A------------------------------------------------------------- %HAS-ANCESTOR?
;;; ***** not clear however that it works well with versions *****

(defUn %has-ancestor? (obj-id ancestor  &key (prop-id '$IS-A))
  "Checks if obj2 is equal to obj1, or belongs to the transitive closure of obj1 ~
   using property prop-id.
Arguments:
   obj-id: first object
   ancestor: second object
   prop-id: property to follow (default is $IS-A)"
  (setq obj-id (%resolve obj-id))
  (setq ancestor (%resolve ancestor))
  ;; %sp-gamma uses %get-value with a current context
  (member ancestor (%sp-gamma obj-id prop-id)))

#|
(%has-ancestor? '$EPT '$EPR)
($EPR)

(with-package :address (%has-ancestor? 'address::$E-STUDENT 'address::$E-PERSON))
(ADDRESS::$E-PERSON)

(with-package :test
  (with-context 3
    (%has-ancestor? 'test::$e-student 'test::$e-person)))
(TEST::$E-PERSON)
|#
;;;A---------------------------------------------------------------- %HAS-ID-LIST
;;; If entry is a property name, then we should get the list of all properties
;;; of the property tree (... 'HAS-NAME '$PNAM.OF '$EPT)

(defUn %has-id-list (entry inv-id class-id)
  "Returns the list of all objects corresponding to entry point entry with inverse ~
   terminal property inv-id and of class class-id.
    E.g. (%has-id-list 'ATTRIBUTE '$PNAM.OF '$ENT) returns ($EPT) ~
   class-id must be specified locally; i.e., will not return objects ~
   that are instances of subclasses of class-id.
    If context is illegal throws to an :error tag
Arguments:
   entry: entry symbol
   inv-id: local inverse attribute
   class-id: identifier of target class
   context: context
Return:
   a list of objects."
  (let (lres 
        (entity-list (if (%pdm? entry)(%get-value entry inv-id)))
        )
    (while entity-list
           (if (and (%pdm? (car entity-list))
                    (member class-id 
                            (%get-value (car entity-list) '$TYPE)))
               (push (car entity-list) lres))
           (pop entity-list))
    (reverse lres)
    ))

#|
(%has-id-list 'HAS-MOSS-TERMINAL-PROPERTY '$PNAM.OF '$EPS)
($PT)

(%has-id-list 'MOSS-TERMINAL-PROPERTY '$ENAM.OF '$ENT)
($EPT)

(with-package :test
  (%has-id-list 'test::PERSON '$ENAM.OF '$ENT))
(TEST::$E-PERSON)

(with-package :test
  (%has-id-list 'test::HAS-NAME '$PNAM.OF '$EPT))
(TEST::$T-NAME TEST::$T-ORGANIZATION-NAME TEST::$T-PERSON-NAME)

(with-package :test
  (with-context 0
    (%has-id-list 'test::barth�s 'test::$T-person-name.OF 'test::$E-person)))
(TEST::$E-PERSON.1 TEST::$E-PERSON.2)

(with-package :test
  (with-context 5
    (%has-id-list 'test::barth�s 'test::$T-person-name.OF 'test::$E-person)))
(TEST::$E-PERSON.1 TEST::$E-PERSON.2 TEST::$E-PERSON.3 TEST::$E-PERSON.6)

(with-package :test
  (%has-id-list 'test::barth�s 'test::$T-name.OF 'test::$E-person))
NIL

(%has-id-list 'barth�s '$T-person-name.OF '$E-person)
($E-PERSON.2)
|#
;;;A--------------------------------------------------- %%HAS-INVERSE-PROPERTIES

(defUn %%has-inverse-properties (obj-id context)
  "Returns the list of inverse properties local to an object and having  ~
   associated values in the specified context
Arguments:
   obj-id: object identifier
   context: context
Return:
   a list of properties or nil."
  (with-context context   
    (delete '$TYPE
            (reduce #'append
                    (mapcar 
                        #'(lambda (xx)
                            (if (and
                                 (%is-inverse-property? (car xx))
                                 (%get-value obj-id (car xx)))
                                (ncons (car xx))))
                      (symbol-value obj-id))
                    ))))

#|
(moss::%%has-inverse-properties 'moss-terminal-property 0)
($ENAM.OF $EPLS.OF)

(moss::%%has-inverse-properties 'barth�s 0)
($T-PERSON-NAME.OF MOSS::$EPLS.OF)
|#
;;;A----------------------------------------------------------- %%HAS-PROPERTIES

(defUn %%has-properties (obj-id context)
  "Returns the list of properties local to an object and having associated ~
   values in the specified context, looking directly at the internal format ~
   of the object. $TYPE is removed.
Arguments:
   obj-id: object identifier
   context: context
Return:
   a list of properties or nil." 
  ;; check for alive object. If not throws to :error
  (setq obj-id (%%alive? obj-id context))
  (delete '$id
          (delete '$TYPE
                  (reduce #'append
                          (mapcar 
                              #'(lambda (xx)
                                  (if (%get-value obj-id (car xx) context) 
                                                (ncons (car xx))))
                            (symbol-value obj-id))
                          ))))

#|
(%%has-properties '$ent 0)
($ENAM $RDX $ENLS.OF $CTRS $DOCT $PT $PS $SUC.OF $IMS $OMS)

(%%has-properties _dbb 0)
($T-PERSON-NAME $T-PERSON-FIRST-NAME)

;;===== Versions:
test::$E-PERSON.4
(($TYPE (2 TEST::$E-PERSON)) ($ID (2 TEST::$E-PERSON.4)) 
 (TEST::$T-PERSON-NAME (2 "Barth�s"))
 (TEST::$T-PERSON-FIRST-NAME (2 "Camille")))

(with-package :test
  (moss::%%has-properties 'test::$E-PERSON.4 2))
(TEST::$T-PERSON-NAME TEST::$T-PERSON-FIRST-NAME)
|#
;;;A---------------------------------------------------------- %%HAS-PROPERTIES+
;;; obscure function... used by =clone only

(defUn %%has-properties+ (obj-id context)
  "Returns the list of ALL properties LOCAL to an object even with no ~
   value, in the specified context, looking directly at the internal format ~
   of the object.
Arguments:
   obj-id: object identifier
   context: context
Return:
   a list of properties or nil."
  ;; check for illegal context. If illegal throws to :error
  (setq obj-id (%%alive? obj-id context))
  (when obj-id
    (mapcar #'car (symbol-value obj-id))))

#|
? $E-PERSON.2
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.2))
 ($T-PERSON-NAME (0 "Barth�s")) ($T-PERSON-FIRST-NAME (0 "Jean-Paul"))
 ($T-PERSON-SEX (0 "M")) ($T-PERSON-TITLE (0 "Prof."))
 ($T-PERSON-EMAIL (0 "barthes@utc.fr"))
 ($T-PERSON-WEB-SITE (0 "http://www.utc.fr/~barthes"))
 ($T-PERSON-HOME-PHONE (0 "+33 (0)3 44 20 31 37"))
 ($T-PERSON-OFFICE-PHONE (0 "+33 (0)3 44 23 44 66"))
 ($T-PERSON-CELL-PHONE (0 "+33 (0)6 80 45 32 67"))
 (MOSS::$DOCT
  (0
   (:EN "Jean-Paul Barth�s is a professor at University of ~
         Technology of Compi�gne."
        :FR "Jean-Paul barth�s est professeur ? l'UTC.")))
 ($S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF (0 $E-NON-PROFIT-ORGANIZATION.1))
 ($S-PERSON-WORK-PLACE (0 $E-WORK-PLACE.2)) ($S-PERSON-EMPLOYER (0 $E-UNIVERSITY.4)))

? (moss::%%has-properties+ '$E-PERSON.2 0)
(MOSS::$TYPE MOSS::$ID $T-PERSON-NAME $T-PERSON-FIRST-NAME $T-PERSON-SEX
             $T-PERSON-TITLE $T-PERSON-EMAIL $T-PERSON-WEB-SITE $T-PERSON-HOME-PHONE
             $T-PERSON-OFFICE-PHONE $T-PERSON-CELL-PHONE MOSS::$DOCT
             $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF $S-PERSON-WORK-PLACE $S-PERSON-EMPLOYER)
|#
;;;A------------------------------------------------ %%HAS-STRUCTURAL-PROPERTIES

(defUn %%has-structural-properties (obj-id context)
  "Returns the list of structural properties local to an object and having  ~
   associated values in the specified context. $TYPE and $ID are removed.
Arguments:
   obj-id: object identifier
   context: context
Return:
   a list of properties or nil."
  ;; check for illegal context. If illegal throws to :error
  (setq obj-id (%%alive? obj-id context))
  (with-context context
    (delete '$ID
            (delete '$TYPE
                    (reduce #'append
                            (mapcar 
                                #'(lambda (xx)
                                    (if (and
                                         (%is-relation? (car xx))
                                         (%get-value obj-id (car xx)))
                                        (ncons (car xx))))
                              (symbol-value obj-id))
                            )))))

#|
(%%has-structural-properties '$ent 0)
($CTRS $PT $PS $IMS $OMS)

(with-package :test
    (moss::%%has-structural-properties 'test::$E-PERSON.2 0))
(TEST::$S-PERSON-HUSBAND)

(with-package :test
  (moss::%%has-structural-properties 'test::$E-PERSON.2 4))
(TEST::$S-PERSON-HUSBAND)
|#
;;;A-------------------------------------------------- %%HAS-TERMINAL-PROPERTIES

(defUn %%has-terminal-properties (obj-id context)
  "Returns the list of terminal properties local to an object and having  ~
   associated values in the specified context.
Arguments:
   obj-id: object identifier
   context: context
Return:
   a list of properties or nil."
  ;; check for illegal context. If illegal throws to :error
  (setq obj-id (%%alive? obj-id context))
  (with-context context
    (delete '$ID
            (delete '$TYPE
                    (reduce #'append
                            (mapcar 
                                #'(lambda (xx)
                                    (if (and
                                         (%is-attribute? (car xx))
                                         (%get-value obj-id (car xx)))
                                        (ncons (car xx))))
                              (symbol-value obj-id))
                            )))))

#|
(%%has-terminal-properties '$ent 0)
($ENAM $RDX $DOCT)

(with-package :test
  (moss::%%has-terminal-properties 'test::$E-PERSON.1 3))
(TEST::$T-PERSON-NAME TEST::$T-PERSON-FIRST-NAME)
|#
;;;A---------------------------------------------------------------- %%HAS-VALUE
;;; low level function to see if the value is recorded locally
;;; Watch: does not work if value is inherited from a previous context...

(defUn %%has-value (obj-id prop-id context)
  "Returns the value list associated to a particular context if recorded locally. ~
   No checks are done, except for legal context. If object is not in core, won't ~
   load it. He object inherits a value from a previous context, won't find it.
Arguments:
   obj-id: object under consideration (can be dead)
   prop-id: property (may be local or inherited)
   context: context
Return:
   the local value if any."
  (%%allowed-context? context)
  (cdr (assoc context (cdr (assoc prop-id (symbol-value obj-id))))))

#|
(%%has-value '$ent '$ENAM 0)
(((:EN "MOSS-CONCEPT" "MOSS-ENTITY" "MOSS-CLASS")))

Versions:
(with-package :test
    (moss::%%has-value 'test::$E-person.2 'test::$T-person-name 0))
("Barth�s-Biesel" "Barth�s")

(with-package :test
  (moss::%%has-value 'test::$E-person.2 'test::$T-person-name 2))
NIL
|#
;;;============================= end HAS functions =============================

;;;A-------------------------------------------------------------- INSERT-BEFORE
;;; changed test to equal+ to deal with inhomogeneous data

(defUn insert-before (val val-l &rest val-test)
  "Should be a primitive. 
      Inserts item into a list either at the end of the list, or in front of the ~
   specified test-item, possibly duplicating it.
Arguments:
   val: value to insert
   val-l: list into which to insert the value
   val-test (&rest): value that will be checked for insertion (equal test)
Return:
    modified list or list of the value when val-l was nil"
  (if val-l 
      (if (equal+ (car val-l) (car val-test))
          (cons val val-l)
        ;; we did not find it, then try on the rest of the list
        (cons (car val-l)
              (apply #'insert-before val (cdr val-l) val-test)))
    ;; it was not in the list thus we insert at the end 
    (ncons val)))

#|
(insert-before 1 nil nil)
(1)

(insert-before 2 '(1) nil)
(1 2)

(insert-before 23 '(1 2 3 4 5 6 7) 0)
(1 2 3 4 5 6 7 23)

(insert-before 23 '(1 2 3 4 5 6 7) 3)
(1 2 23 3 4 5 6 7)

(insert-before 55 '(a b c 1 2 3 "a" "b" "z") "z")
(A B C 1 2 3 "a" "b" 55 "z")
|#
;;;A--------------------------------------------------------- INSERT-LIST-BEFORE

(defun insert-list-before (val-list target-list val-test)
  "Should be a primitive. 
   Inserts a list of items into a list either at the end of the list, or in ~
   front of the specified test-item, possibly duplicating it.
Arguments:
   val-list: list of values to insert
   target-list: list into which to insert the value
   val-test (&rest): value that will be checked for insertion (equal test)
Return:
    modified list or list of the value when val-l was nil"
  (cond
   ((null target-list) val-list)
   ((null val-list) target-list)
   ;; test for any type of value
   ((equal+ (car target-list) val-test)
    (append val-list target-list))
   (t
    ;; we did not find it, then try on the rest of the list
    (cons (car target-list)
          (insert-list-before val-list (cdr target-list) val-test)))))


#|
(insert-list-before nil nil nil)
NIL

(insert-list-before nil '(1 2 3 4) nil)
(1 2 3 4)

(insert-list-before '(5 6) '(1 2 3 4) nil)
(1 2 3 4 5 6)

(insert-list-before '(5 6) '(1 2 3 4) 7)
(1 2 3 4 5 6)

(insert-list-before '(5 6) '(1 2 3 4) 2)
(1 5 6 2 3 4)

(insert-list-before '(5 6) '(1 2 3 4) 1)
(5 6 1 2 3 4)

(insert-list-before '(5 "d") '(1 2 3 "a" 4 "B") "a")
(1 2 3 5 "d" "a" 4 "B")
|#
;;;------------------------------------------------------------- INSERT-LIST-NTH

(defun insert-list-nth (val-list target-list nth)
  "Should be a primitive.
      Inserts an item into a list at the nth position. If the list is too short ~
   then the value is inserted at the end of it.
Arguments:
   val-list: list of values to be inserted
   target-list: list into which to insert the value
   nth: position at which the value must be inserted (0 is in front)
Return:
   modified list"
  (cond
   ((null val-list) target-list)
   ((null target-list) val-list)
   ((not (integerp nth))(append target-list val-list))
   ((< nth 1) (append val-list target-list))
   (t (cons (car target-list) 
            (insert-list-nth val-list (cdr target-list) (1- nth))))))

#|
(insert-list-nth nil nil nil)
NIL

(insert-list-nth nil '(1 2 3) nil)
(1 2 3)

(insert-list-nth '(4 5) '(1 2 3) nil)
(1 2 3 4 5)

(insert-list-nth '(4 5) '(1 2 3) "a")
(1 2 3 4 5)

(insert-list-nth '(4 5) '(1 2 3) 1)
(1 4 5 2 3)

(insert-list-nth '(4 5) '(1 2 3) 0)
(4 5 1 2 3)

(insert-list-nth '(4 5) '(1 2 3) 7)
(1 2 3 4 5)
|#
;;;A----------------------------------------------------------------- INSERT-NTH

(defUn insert-nth (val val-l nth)
  "Should be a primitive.
      Inserts an item into a list at the nth position. If the list is too short ~
   then the value is inserted at the end of it.
Arguments:
   val: value to be inserted NIL is considered a value
   val-l: list into which to insert the value
   nth: position at which the value must be inserted (0 is in front)
Return:
   modified list"
  (cond
   ((null val-l)(list val))
   ((not (numberp nth)) (append val-l (list val)))
   ((< nth 1) (cons val val-l))
   (t (cons (car val-l)(insert-nth val (cdr val-l) (1- nth))))))

;;;(defUn insert-nth (val val-l nth)
;;;  "Should be a primitive.
;;;      Inserts an item into a list at the nth position. If the list is too short ~
;;;   then the value is inserted at the end of it.
;;;Arguments:
;;;   val: value to be inserted
;;;   val-l: list into which to insert the value
;;;   nth: position at which the value must be inserted (0 is in front)
;;;Return:
;;;   modified list"
;;;  (if val-l
;;;      (if (< nth 1) 
;;;          (cons val val-l)
;;;        (cons (car val-l) (insert-nth val (cdr val-l) (1- nth))))
;;;    (ncons val)))
#|
(insert-nth nil nil nil)
(NIL)

(insert-nth 23 nil nil)
(23)

(insert-nth 23 '(1 2 3 4 5 6 7) nil)
(1 2 3 4 5 6 7 23)

(insert-nth 23 nil 2)
(23)

(insert-nth 23 '(1 2 3 4 5 6 7) 0)
(23 1 2 3 4 5 6 7)

(insert-nth 23 '(1 2 3 4 5 6 7) 3)
(1 2 3 23 4 5 6 7)

(insert-nth 23 '(1 2 3 4 5 6 7) 102)
(1 2 3 4 5 6 7 23)
|#
;;;A---------------------------------------------------- %INTERSECT-SYMBOL-LISTS
;;; Only used by determine-property-id, maybe could use intersection

(defUn %intersect-symbol-lists (ll1 ll2)
  "Intersect two lists of symbols by using their p-list. Keeps the order of the ~
   items. Done in linear time."
  (let (result)
    (mapcar #'(lambda (xx)(setf (get xx :intersect-mark) t)) ll1)
    (setq result 
          (mapcar #'(lambda (xx)(if (get xx :intersect-mark) (ncons xx))) ll2))
    (mapcar #'(lambda (xx)(remprop xx :intersect-mark)) ll1)
    (reduce #'append result)
    ))

#|
(%intersect-symbol-lists '(A B C D E F) '(C E F G H I))
(C E F)

(intersection '(A B C D E F) '(C E F G H I))
(F E C)

(reverse (intersection '(A B C D E F) '(C E F G H I)))
(C E F)
|#
;;;A------------------------------------------------------- %INVERSE-PROPERTY-ID

(defUn %inverse-property-id (prop-id &optional version)
  "Computes the inverse property id of a given structural or terminal property 
    Optional arg: version. When the property is still undefined, calls
    %make-id-for-inverse-property.
   For inverse properties of an inverse property return a list if more than one, ~
   e.g. when we have a property lattice.
Arguments:
   prop-id: identifier of property
   context (opt): context default current
Returns:
   an inverse property identifier"
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    ;; loads prop-id if needed
    (unless (%alive? prop-id context)
      (error " unknown prop-id ~S in package ~S and in context ~S" 
        prop-id *package* context))
    
    (let (prop-list)
      (cond
       ;; must be a pdm object, a property.
       ((or (%is-structural-property? prop-id)
            (%is-terminal-property? prop-id)
            )
        ;; If already defined, then just
        ;; read it, otherwise build it in a brute force fashion
        (or
         (car (%get-value prop-id '$INV))
         ;; don't forget inverse properties
         ;; inverse-id created if needed in the same package as the direct one
         (car (%get-value prop-id (%make-id-for-inverse-property '$INV)))
         (%make-id-for-inverse-property prop-id)))
       ;; inverse properties of an inverse property are the property tree 
       ;; if only one returns the property itself, otherwise returns a list of
       ;; of properties
       ((%is-inverse-property? prop-id)
        (setq prop-list (%get-value prop-id '$INV.of))
        (if (cdr prop-list) prop-list (car prop-list)))
       ;; in case the property is being defined and is not yet filled, then
       ;; produce a brute force inverse in the same package as prop-id
       ((%make-id-for-inverse-property prop-id)))
      )))

#|
(%inverse-property-id '$CTRS)
$CTRS.OF

(%inverse-property-id '$CTRS.of)
$CTRS

(%inverse-property-id '$ENAM.of)
$ENAM

(with-package :test
  (moss::%inverse-property-id '$T-name.of))
Error:  unknown prop-id $T-NAME.OF in package #<The MOSS package> and in context 5

(with-package :test
  (moss::%inverse-property-id 'test::$T-name 3))
TEST::$T-NAME.OF

(setq test::*version-graph*
   '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3
(with-package :test
  (with-context 5
    (moss::%inverse-property-id 'test::$T-person-name.of)))
TEST::$T-PERSON-NAME

(with-package :test
  (with-context 5
    (moss::%inverse-property-id 'test::$T-person-home)))
Error:  unknown prop-id $T-PERSON-HOME in package #<The TEST package> and in context 5
|#
;;;=============================================================================
;;;                                PREDICATES
;;;=============================================================================

;;;A--------------------------------------------------------------------- %IS-A?
;;;***** this function implies that $is-a has no subproperties
;;; semantics unclear...

(defUn %is-a? (obj1 obj2)
  "Checks if obj1 is equal to obj2, or obj-2 belongs to the transitive closure of ~
   obj1 using property $IS-A in the current context.
Arguments:
   obj1: first object
   obj2: second object"
  
  (cond
   ;; first cheap test
   ((eql obj1 obj2) t)
   ;; when it does not work must check if obj2 is an ancestor of obj1
   ;(setq obj1 (%resolve obj1))
   ;(setq obj2 (%resolve obj2))
   ((and (%pdm? obj1)(%pdm? obj2)(member obj2 (%sp-gamma obj1 '$IS-A))))))

#|
(with-package :test
  (moss::%is-a? 'test::$E-student 'test::$E-person))
(test::$E-PERSON)
;; but also...
(moss::%is-a? 21 21)
T
(%is-a? 21 22)
nil

;; versions
(with-package :test
  (with-context 2
    (%is-a? 'test::$E-student 'test::$E-person)))
(TEST::$E-PERSON)
|#
;;;=============================================================================
;;; the following predicates test whether objects are subclasses of system classes
;;; this was useful when using different name spaces inside the same package
;;; corresponding to different applications.
;;; This possibility was removed from the last versions of MOSS.
;;;=============================================================================

;;;A------------------------------------------------------------- %IS-ATTRIBUTE?
;;; extending %is-attribute? to take a ref string
;;; maybe we should add mln?

(defUn %is-attribute? (prop-id-or-ref &optional version)
  "Checks if the identifier points towards an attribute in the current context.
Arguments:
   prop-id: object identifier or string ref
Return:
   attribute id or nil"
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (cond
     ((stringp prop-id-or-ref)
      (%%get-id prop-id-or-ref :attribute))
     ((%type? prop-id-or-ref '$EPT context)
      prop-id-or-ref))))

#|
(with-package :test
  (with-context 2
    (moss::%is-attribute? 'test::$T-person-name)))
TEST::$T-PERSON-NAME

(with-package :test
  (with-context 2
    (moss::%is-attribute? "name")))
TEST::$T-NAME
|#
;;;A------------------------------------------------------- %IS-ATTRIBUTE-MODEL?
;;; probably not very useful JPB1004 (does not seem to be used)

(defUn %is-attribute-model? (object-id &optional version)
  "Checks if the object is $EPT or some sub-type in the current context.
Arguments:
   object-id: object identifier
Return:
   T or nil"
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (with-context context
      (%subtype? (%ldif object-id) '$EPT))))

#|
(%is-attribute-model? '$PNAM)
NIL

(%is-attribute-model? '$EPT)
T

(moss::%is-attribute-model? '$T-NAME)
NIL
|#
;;;A----------------------------------------------------------------- %IS-CLASS?

(defUn %is-class? (object-id &optional version)
  "Checks if the object is a class, i.e. if its type is $ENT or a subtype of $ENT.
Arguments:
   object-id: identifier of the object to check
   version (opt): context (default current)
Return
   nil or a list result of an intersection."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (and (%pdm? object-id)
         (intersection 
          (%%get-value object-id '$TYPE context)
          (%sp-gamma '$ENT (%make-id '$EIL :id '$IS-A ))))))
#|
(%is-class? '$CTR)
($ENT)

(%is-class? '$ENT)
($ENT)

(%is-class? '*none*)
($ENT)

(%is-class? '*any*)
($ENT)

(with-package :test
  (%is-class? 'test::$E-PERSON))
($ENT)

Versions: define $E-XXA in context 2
(with-package :test
  (with-context 2
    (defconcept "XXA" (:att "role"))))
TEST::$E-XXA 
((MOSS::$TYPE (2 MOSS::$ENT)) (MOSS::$ID (2 $E-XXA)) (MOSS::$ENAM (2 ((:EN "XXA"))))
 (MOSS::$RDX (2 $E-XXA)) (MOSS::$ENLS.OF (2 $SYS.1)) (MOSS::$CTRS (2 $E-XXA.CTR))
 (MOSS::$PT (2 $T-XXA-ROLE)))

(setq test::*version-graph*
   '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3

(with-package :test
  (with-context 1
    (%is-class? 'test::$E-XXA)))
NIL

;; make context specific: although not defined in 1 returns test in 2
(with-package :test
  (with-context 1
    (%is-class? 'test::$E-XXA 2)))
($ENT)

(with-package :test
  (with-context 2
    (%is-class? 'test::$E-XXA)))
($ENT)

(with-package :test
  (with-context 4
    (%is-class? 'test::$E-XXA)))
NIL
|#
;;;?------------------------------------------------------- %IS-CLASSLESS-OBJECT?
;;; can be called from any package
;;; maybe we should intern *none* in the package of the object...
;;;********** to check seriously

(defUn %is-classless-object? (object-id &optional version)
  "Checks if the entity is a classless object, i.e. its type is *none*.
Arguments:
   object-id: identifier of object
   version (opt): context (default current)
Return:
   T if OK, NIL otherwise."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (and (%alive? object-id context)
         (eql (intern "*NONE*")
              (car (%%get-value object-id '$TYPE context))))))

#|
(with-package :test (defobject ("name" "george")(:var test::_g)))
$ORPHAN.1
((MOSS::$TYPE (2 *NONE*)) (MOSS::$ID (2 $ORPHAN.1)) ($T-NAME (2 "george")))

(with-package :test
  (moss::%is-classless-object? test::_g 2))
T

(with-package :test
  (with-context 3
    (moss::%is-classless-object? test::_g)))
T ; OK, normal since 3 inherits from 2

(with-package :test
  (with-context 0
    (defobject ("name" "hubert")(:var test::_h))))
$ORPHAN.2
((MOSS::$TYPE (0 *NONE*)) (MOSS::$ID (0 $ORPHAN.2)) ($T-NAME (0 "hubert")))

(with-package :test
  (with-context 6
    (moss::%is-classless-object? 'test::$ORPHAN.2)))
T
|#
;;;A--------------------------------------------------------------- %IS-CONCEPT?
;;; The object has been created in a specific version (that of the $TYPE property)
;;; The function answers the question: is this object a class in current version?
;;; Same function as %is-class? Should use %is-class? inline...

(defUn %is-concept? (object-id &optional version)
  "Checks if the object is a concept (same as class), i.e. if its type is $ENT ~
   or a subtype of $ENT.
Arguments:
   object-id: identifier of the object to check
Return
   nil or a list result of an intersection."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (and (%pdm? object-id)
         (intersection 
          (%%get-value object-id '$TYPE context)
          (%sp-gamma '$ENT (%make-id '$EIL :id '$IS-A))))))

#|
(%is-concept? '$EPT)
($ENT)

(moss::%is-concept? '$E-student)
NIL

Versions: define $E-XXA in context 2
(setq test::*version-graph*
   '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))
;         0
;        / \
;       1   4
;      /   / \
;     2   5   6
;    /
;   3
(with-package :test
  (with-context 1
    (%is-concept? 'test::$E-XXA)))
NIL

;; priority of argument over the global context
(with-package :test
  (with-context 1
    (%is-concept? 'test::$E-XXA 2)))
($ENT)

(with-package :test
  (with-context 2
    (%is-concept? 'test::$E-XXA)))
($ENT)

(with-package :test
  (with-context 3
    (%is-concept? 'test::$E-XXA)))
($ENT)
|#
;;;A--------------------------------------------------------- %IS-COUNTER-MODEL?
;;; since counters are defined in context 0 it would not make sense to add a
;;; version argument

(defUn %is-counter-model? (object-id)
  "Checks if the object is $CTR or some sub-type. Counters are defined in ~
   context 0.
Arguments:
   object-id: identifier of the object to check
Return
   nil or T." 
  (%subtype? object-id '$CTR))

#|
(%is-counter-model? '$CTR)
T

(%is-counter-model? '$ENT)
NIL

(with-package :test
  (with-context 6
    (%is-counter-model? 'test::$ctr)))
T
|#
;;;A---------------------------------------------------------- %IS-ENTITY-MODEL?

(defUn %is-entity-model? (object-id)
  "Checks if the object is $ENT or some sub-type."
  (%subtype? object-id '$ENT))

#|
(%is-entity-model? '$ENT)
T

(%is-entity-model? '$SYS)
NIL

;; $ENT is not defined in test package
(with-package :test
  (with-context 6
    (%is-counter-model? 'test::$ent)))
NIL

;;;... nor is imported
(with-package :test
  (with-context 6
    (%is-counter-model? '$ent)))
NIL
|#
;;;A----------------------------------------------------------------- %IS-ENTRY?

(defUn %is-entry? (object-id &optional version)
  "Checks if the id is that of an entry point. Package is that of argument.
Arguments:
   object-id: identifier of object
   version (opt): context (default current)
Returns:
   nil or something not meaningful"
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (and (%type? object-id '$EP context) object-id)))

#|
(with-package :test (%is-entry? 'test::barth�s))
TEST::BARTH�S

(%is-entry? 'test::person)
TEST::PERSON

;; teacher does not exist in version 0
(%is-entry? 'test::TEACHER 0)
NIL

(with-package :test
  (with-context 3 
    (%is-entry? 'test::xxa)))
TEST::XXA
|#
;;;A-------------------------------------------------- %IS-ENTRY-POINT-INSTANCE?
;;; Entry points are interesting objects, since they can point to a number of
;;; objects in different contexts. An entry point should in fact be created in
;;; context 0 at the root of the context tree.


(defUn %is-entry-point-instance? (object-id &optional version)
  "Checks if the object is an instance of entry-point."
  (%type? object-id '$EP (or version (symbol-value (intern "*CONTEXT*")))))

#|
(%is-entry-point-instance? 'MOSS-ENTITY)
T
(($TYPE (0 $EP)) ($ID (0 MOSS-ENTITY)) ($ENAM.OF (0 $ENT)) ($EPLS.OF (0 $SYS.1)))

(with-package :test
  (moss::%is-entry-point-instance? 'test::barth�s))
T

Versions: $E-XXA is defined in context 2
(with-package :test
  (with-context 0
    (moss::%is-entry-point-instance? 'test::XXA)))
NIL

(with-package :test
  (with-context 3
    (moss::%is-entry-point-instance? 'test::XXA)))
T

(with-package :test
  (moss::%is-entry-point-instance? 'test::XXA 3))
T
|#
;;;A--------------------------------------------------- %IS-GENERIC-PROPERTY-ID?
;;; we must use %%get-value instead of %%has-value, because in subsequent versions
;;; of the object, %%has-value returns nil for $IS-A

(defUn %is-generic-property-id? (object-id &optional version)
  "Checks if the object is $EPR and is not a subproperty of some other property."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (and (%type? object-id '$EPR context)
         (not (%%get-value object-id '$IS-A context)))))

#|
(%is-generic-property-id? '$PNAM)
T

(moss::%is-generic-property-id? '$T-NAME)
NIL ; PERSON is not defined in MOSS package

(with-package :test
  (moss::%is-generic-property-id? 'test::$T-NAME))
T

(with-package :test
  (moss::%is-generic-property-id? 'test::$T-PERSON-NAME 3))
NIL
|#
;;;------------------------------------------------------------------ %IS-IDEAL?

(defun %is-ideal? (object-id)
  "Checks if the object is an ideal object of a class"
  ;; an object is an ideal if its id is <class-name>.0
  (let* ((name (symbol-name object-id))
         (pos (position #\. name)))
    (when pos
      (equal+ "0" (subseq name (1+ pos))))))
        
  
;;;A----------------------------------------------------------- %IS-INSTANCE-OF?

(defUn %is-instance-of? (object-id class-id &optional version)
  "Checks if the object is an instance of class id or of one of its children.
Arguments:
   object-id: id of the object too test
   class-id: id of the class
   version (opt): context (default current)
Return:
   t if true nil otherwise."
  ;; get the class(es) of the object
  (let* ((context (or version (symbol-value (intern "*CONTEXT*"))))
         (type-list (%get-value object-id '$type context)))
    ;; check if one of the type class is a sub-type of class-id
    (some #'(lambda (xx) (%subtype? xx class-id)) type-list)))

#|
(defconcept "test")
$E-TEST

(defconcept "subtest" (:is-a "test"))
$E-SUBTEST

(defindividual "subtest" (:var _aa))
$E-SUBTEST.1

(moss::%is-instance-of? _aa _subtest)
T

(moss::%is-instance-of? _aa _test)
T

(moss::%is-instance-of? _aa '$ent)
NIL

Versioning
(with-package :test
  (with-context 3
    (%is-instance-of? 'test::$E-PERSON.1 'test::$E-PERSON)))
T

(with-package :test
    (%is-instance-of? 'test::$E-PERSON.3 'test::$E-PERSON 0))
NIL

(with-package :test
  (%is-instance-of? 'test::$E-PERSON.3 'test::$E-PERSON 5))
T

(with-package :test
  (with-context 3
    (%is-instance-of? 'test::$E-STUDENT.1 'test::$E-PERSON)))
T

(with-package :test
  (with-context 4
    (%is-instance-of? 'test::$E-STUDENT.1 'test::$E-PERSON)))
NIL

(with-package :test
  (with-context 4
    (%is-instance-of? 'test::$E-STUDENT.1 'test::$E-PERSON 3)))
T
|#
;;;A---------------------------------------------------- %IS-INVERSE-LINK-MODEL?

(defUn %is-inverse-link-model? (object-id)
  "Checks if the object is $EIL or some sub-type."
  (%subtype? object-id '$EIL))

#|
(%is-inverse-link-model? '$EIL)
T

(with-package :test
  (with-context 3
    (%is-inverse-link-model? 'test::$EIL)))
NIL ; there is no such local property

(with-package :test
  (with-context 3
    (%is-inverse-link-model? 'moss::$EIL)))
T
|#
;;;+------------------------------------------------------ %IS-INVERSE-PROPERTY?

(defUn %is-inverse-property? (xx &optional version)
  "Checks if the id points towards an inverse property, or to a property containing
$EIL in its transitive closure along $IS-A."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (and (%pdm? xx)
         (member '$EIL (%sp-gamma-l (%get-value xx '$TYPE context) '$IS-A))
         t)))

#|
(%is-inverse-property? nil)
NIL

(%is-inverse-property? '$CTRS)
NIL

(%is-inverse-property? '$CTRS.OF)
T

(moss::%is-inverse-property? '$T-NAME.OF)
NIL

Versioning (********** needs more tests)
(with-package :test
  (with-context 3
    (moss::%is-inverse-property? 'test::$T-NAME.OF)))
T

(with-package :test
  (moss::%is-inverse-property? 'test::$T-PERSON-NAME.OF 5))
T
|#
;;;A-------------------------------------------------- %IS-INVERSE-PROPERTY-REF?

(defUn %is-inverse-property-ref? (xx &optional version)
  "Checks if the string is the reference of an inverse property, i.e. a string ~
   starting with the > symbol whose name is that of a property.
Argument:
   xx: string
   version (opt): context (default current)
Return:
   the inverse property-id or nil."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (when (and (stringp xx) (equal #\> (char xx 0)))
      (let ((name (%make-name-for-property (subseq xx 1))))
        (and (%pdm? name)
             (mapcar #'%inverse-property-id
               (%%get-value name 
                            (%inverse-property-id '$PNAM context)
                            context)))))))

#|
(MOSS::%IS-INVERSE-PROPERTY-REF? ">moss-entity-name")
($ENAM.OF)

Versioning: ********** needs more tests

(with-package :test
  (with-context 3
    (moss::%is-inverse-property-ref? ">name")))
(TEST::$T-NAME.OF TEST::$T-ORGANIZATION-NAME.OF TEST::$T-PERSON-NAME.OF)

(MOSS::%IS-INVERSE-PROPERTY-REF? ">avenue")
NIL
|#
;;;A---------------------------------------------------------------- %IS-METHOD?

(defUn %is-method? (object-id &optional version)
  "Checks if the id points towards a method"
  (%type? object-id '$FN (or version (symbol-value (intern "*CONTEXT*")))))

#|
(%is-method? '$FN.12)
T

(%is-method? '$UNI.3)
NIL

(with-package :test
  (moss::%is-method? 'test::$E-FN.1))
T

(with-package :test
  (with-context 6
    (moss::%is-method? 'test::$E-FN.1)))
T
|#
;;;A---------------------------------------------------------- %IS-METHOD-MODEL?

(defUn %is-method-model? (object-id)
  "Checks if the object is $EFN or some sub-type."
  (or (%subtype? object-id '$FN)
      (%subtype? object-id '$UNI)))

#|
(%is-method-model? '$FN)
T

(%is-method-model? '$UNI) ; should be true...
T
|#
;;;A----------------------------------------------------------------- %IS-MODEL?

(defUn %is-model? (object-id &optional version)
  "Checks whether an object is a model (class) or not - Models ~
   are objects that have properties like $PT or $PS or $RDX or $CTRS ~
   or whose type is in the transitive closure of $ENT along the $IS-A.OF link."
  (%type? object-id '$ENT (or version (symbol-value (intern "*CONTEXT*")))))

#|
(%is-model? '$EPR)
T

(moss::%is-model? '$E-student)
NIL

(with-package :test
  (with-context 6
    (moss::%is-model? 'test::$E-student)))
T

(with-package :test
  (moss::%is-model? 'test::$E-teacher 5))
T

(with-package :test
  (moss::%is-model? 'test::$E-teacher 0)) ; undefined in context 0
NIL
|#
;;;A---------------------------------------------------------------- %IS-ORPHAN?

(defUn %is-orphan? (object-id &optional version)
  "Short synonym for %is-classless-object?"
  (declare (inline %is-classless-object?))
  (%is-classless-object? object-id (or version (symbol-value (intern "*CONTEXT*")))))

#|
(with-package :test
  (with-context 0
    (defobject (:var test::_jj)("name" "Smith")("first name" "Jerome"))))
TEST::$ORPHAN.3

(with-package :test
  (with-context 0
    (moss::%is-orphan? test::_jj)))
T
|#
;;;A-------------------------------------------------------- %%IS-PROPERTY-NAME?
;;; does not imply versions
(defUn %%is-property-name? (object-id)
  "checks if name is a property name by looking if it starts with HAS-
Arguments:
   xx: must be a symbol
Return:
   symbol or nil"
  (and (symbolp object-id)
       (equal+ "HAS-" (subseq (symbol-name object-id) 0 4))
       object-id)
  )

#|
(moss::%%is-property-name? 'HAS-NOM)
HAS-NOM

(moss::%%is-property-name? 'HAS-strumpf)
HAS-STRUMPF

(moss::%%is-property-name? 'IS-NOM-of)
NIL
|#
;;;A-------------------------------------------------------------- %IS-RELATION?

(defUn %is-relation? (prop-id-or-ref &optional version)
  "Checks if the object or ref is $EPS or some sub-type."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (cond
     ((stringp prop-id-or-ref)
      (%%get-id prop-id-or-ref :relation))
     (t
      (%type? prop-id-or-ref '$EPS context)))))

#|
(moss::%is-relation? '$CTRS)
T

(moss::%is-relation? 'test::$S-address3-people)
T

(with-package :test
  (moss::%is-relation? "people"))
NIL

Versioning: ***** Needs more tests
(with-package :test
  (moss::%is-relation? "wife" 3))
TEST::$S-WIFE
|#
;;;A---------------------------------------------------------- %IS-RELATIONSHIP?
;;; apparently unused

(defUn %is-relationship? (prop-id &optional version)
  "Checks if the object is $EPS or some sub-type."
  (%is-relation? prop-id (or version (symbol-value (intern "*CONTEXT*")))))

#|
(moss::%is-relationship? '$CTRS)
T

(moss::%is-relationship? 'test::$S-address3-people)
T

(with-package :test
  (moss::%is-relationship? 'test::$S-address3-people))
T

(with-package :test
  (with-context 3
    (moss::%is-relationship? 'test::$S-address3-people)))
T
|#
;;;A-------------------------------------------------------- %IS-RELATION-MODEL?

(defUn %is-relation-model? (object-id)
  "checks if the object is $EPS or some sub-type"
  (%subtype? object-id '$EPS))

#|
(%is-relation-model? '$EPS)
T

(with-package :test
  (%is-relation-model? '$eps))
T
|#
;;;A--------------------------------------------------- %IS-STRUCTURAL-PROPERTY?
;;; Same as %is-relationship?

(defUn %is-structural-property? (prop-id &optional version)
  "Checks if the identifier points towards a structural property"
  (%is-relation? prop-id (or version (symbol-value (intern "*CONTEXT*")))))

;;;A--------------------------------------------- %IS-STRUCTURAL-PROPERTY-MODEL?

(defUn %is-structural-property-model? (object-id)
  "Checks if the object is $EPS or some sub-type."
  (%subtype? object-id '$EPS))

#|
(%is-structural-property-model? '$EPS)
T
|#
;;;A---------------------------------------------------------------- %IS-SYSTEM?

(defUn %is-system? (object-id &optional version)
  "Checks if the id points towards a system."
  (%type? object-id '$SYS (or version (symbol-value (intern "*CONTEXT*")))))

#|
(moss::%is-system? '$SYS.1)
T

(with-package :test
  (with-context 5
    (%is-system? 'test::$SYS.1)))
T

(with-package :test
  (with-context 5
    (%is-system? test::*ontology*)))
T
|#
;;;A---------------------------------------------------------- %IS-SYSTEM-MODEL?

(defUn %is-system-model? (object-id &optional version)
  "Checks if the entity is a model, and belongs to the MOSS package.
Arguments:
   object-id: identifier of object to test
Return:
   nil or t"
  (and (%type? object-id '$ENT (or version (symbol-value (intern "*CONTEXT*"))))
       (eql (symbol-package object-id)(find-package :moss))
       ))

#|
(%is-system-model? '$SYS)
T

(%is-system-model? '$ENT)
T

(moss::%is-system-model? '$E-SYS)
NIL
|#
;;;A----------------------------------------------------- %IS-TERMINAL-PROPERTY?

(defUn %is-terminal-property? (prop-id &optional version)
  "Checks if the identifier points towards a terminal property."
  (%type? prop-id '$EPT (or version (symbol-value (intern "*CONTEXT*")))))

#|
(moss::%is-terminal-property? '$T-PERSON-NAME)
NIL ; person is undefined in moss

(%is-terminal-property? '$PNAM)
T

(with-package :test
  (with-context 5
    (%is-terminal-property? 'test::$T-PERSON-NAME)))
T

(with-package :test
  (%is-terminal-property? 'test::$T-PERSON-NAME 5))
T
|#
;;;A----------------------------------------------- %IS-TERMINAL-PROPERTY-MODEL?

(defUn %is-terminal-property-model? (object-id)
  "Checks if the object is $EPT or some sub-type."
  (%subtype? object-id '$EPT))

#|
(%is-terminal-property-model? '$EPT)
T
|#
;;;A------------------------------------------------------ %IS-UNIVERSAL-METHOD?

(defUn %is-universal-method? (xx &optional version)
  "Checks if the id points towards a universal method - Optional arg: context"
  (%type? xx '$UNI (or version (symbol-value (intern "*CONTEXT*")))))

#|
(with-package :test
  (%is-universal-method? 'test::$e-uni.1 0))
NIL ; no universal method in :test

(%is-universal-method? '$uni.11 0)
T
|#

;;;A------------------------------------------------ %IS-UNIVERSAL-METHOD-MODEL?

(defUn %is-universal-method-model? (object-id)
  "Checks if the object is $UNI or some sub-type."
  (%subtype? object-id '$UNI))

#|
(%is-universal-method-model? '$UNI)
T

(with-package :test
  (%is-universal-method-model? 'test::$E-UNI))
NIL

(with-package :test
  (%is-universal-method-model? 'moss::$UNI))
T
|#
;;;A------------------------------------------------------------ %IS-VALUE-TYPE?
;;; used in %validate-tp, which means that value type restrictions are limited
;;; to :string or :integer, which can be checked through a =xi method

(defUn %is-value-type? (value value-type)
  "Checks that a value has the right type.
Arguments:
   value: value to check
   value-type: one of the XSD types (birkkkhh!)
Returns:
   value if the right type, nil otherwise."
  (case value-type
    (nil 
     value)
    (:string
     (if (stringp value) value))
    (:integer
     (if (integerp value) value))
    (:number
     (if (numberp value) value))
    ;; :mln, might break %validate-tp
    (:mln 
     (if (or (mln::%mln? value)(mln::mln? value)) value))
    (t 
     nil)))

;;;A--------------------------------------------------------- %IS-VARIABLE-NAME?

(defUn %is-variable-name? (ref)
  "Checks if the refernce is a variable name, i.e. a symbol starting with an ~
   _ (underscore)"
  (and (symbolp ref)(equal (char (symbol-name ref) 0) '#\_)))

#|
(%is-variable-name? '_albert)
T
|#
;;;?------------------------------------------------------- %IS-VIRTUAL-CONCEPT?

(defUn %is-virtual-concept? (object-id &optional version)
  "Checks if the object is a concept (same as class), i.e. if its type is $ENT ~
   or a subtype of $ENT.
Arguments:
   object-id: identifier of the object to check
   version (opt): context default current
Return
   nil or a list result of an intersection."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (and (%pdm? object-id)
         (intersection 
          (%%get-value object-id '$TYPE context)
          (%sp-gamma '$VENT (%make-id '$EIL :id '$IS-A ))))))

#|
(moss::%is-virtual-concept? '$V-ADULT)
(MOSS::$VENT)
|#
;;;============================== End predicates ===============================

;;;+---------------------------------------------------------------------- %LDIF
;;; to rewrite. When an object is actually loaded, it must be modified to link
;;; it to MOSS objects (e.g. $E-COUNTER) or to load additional functions (?)

;;; Algorithm
;;; 1. the argument xx must be a symbol e.g. $E-PERSON.1, otherwise error. However,
;;; NIL is a symbol. If NIL, we return NIL.
;;; 2. When xx is an unbound symbol or has a nil value, and
;;;  (%load-value-from-database xx)
;;;  returns a non NIL value (meaning that we had an opened database and xx was in it
;;;    2.1 we set xx = $E-PERSON.1 to this value
;;;    2.2 now sometimes we know that we are loading a method or a saved
;;;      function.
;;; In that case %pdm? was called with the object-type optional
;;;      argument. If so, and if we have an optional argument of
;;;       - :method, we get the method-name in the local context and load the 
;;;         corresponding code
;;;         - if the function exists we compile or evaluate the function
;;;       - :function, we compile or evaluate the function
;;;  At the end of step 2, either xx= $E-PERSON.1 is still unbound or it was set to
;;; a non nil value.
;;; 3. if the symbol xx = PERSON is bound but does not belong to the current
;;; package, which may be the case when we load data from the database and we want
;;; to merge entry points, e.g. PERSON from the database, with PERSON from a
;;; different environment, then
;;;   3.1 we create an ENTRY symbol and assign it to the value local variable
;;;   3.2 we set the ENTRY to the result of loading xx = $E-PERSON.1 from the
;;;      database
;;;     - if the result of the load is nil, we do not change xx
;;;     - if the value of id is the same as the loaded value, we do not change id
;;;     - if both values are entry points, we fuse entry points
;;;     - if values are different error
;;; 4. In any other case return id
;;;:END

(defUn %ldif (id &optional object-type)
  "takes a symbol, if it is unbound or nil tries to load its value from disk, ~
   provided the database exists and is opened. Set the id to its value. When the ~
   database is partitioned into several sub-databases, each partition is indexed ~
   according to the package symbols, thus, the symbol is loaded according to its ~
   package.
Argument:
   id: a symbol, normally, a moss object-id, but NIL is allowed
   object-type (opt): type of object (used to load extra stuff)
Return:
   the id, even if NIL (no error)"
  (declare (special *package*))
  (let ((context (symbol-value (intern "*CONTEXT*")))
        value val from-disc?)
    (unless (symbolp id)
      (error "argument ~S should be a symbol." id))
    
    ;; if id is nil skip the rest (nil is a symbol)
    (when id
      (when (and (or (not (boundp id)) (null (symbol-value id)))
                 ;; the loading function is independent from versions
                 (setq value (%load-value-from-database id)))
        ;; here id was not bound, or bound and nil, and we accesed database to
        ;; obtain a non nil value 
        (set id value)
        ;; now, according to type we load extra stuff
        ;; the following lines are executed only when the object-type is passed
        ;; in argument, which occurs when we re-initialize a persistent agent 
        ;; environment from disc
        (case object-type
          ;; if method, load corresponding function
          (:method
           ;(format t "~% %ldif / methods:~% ~S" =make-entry)
           ;; quasi brute-force recovery of function name
           (let* ((fnam (car (%%get-value id '$FNAM context)))
                  (fn (db-load fnam (intern (package-name *package*) :keyword))))
             (when fn 
               (set fnam fn)
               #+COMPILER
               (compile fnam fn)
               #-COMPILER
               (eval `(defUn ,fnam ,fn)))))
          (:function
           (let ((expr (symbol-value id)))
             #+COMPILER
             (compile (car expr) (cdr expr))
             #-COMPILER
             (eval `(defUn ,(car expr) ,@(cddr expr)))))
          )
        ;; should we return at this point? Yes, because we either started with an
        ;; unbound or nil value, thus we are not in the case of having to merge
        ;; a disk value with an already existing one
        (return-from %ldif id)
        )
      
      ;; when the symbol has a value, but the value belongs to another package
      ;; this case occurs when we are loading entry points that are already defined
      ;; in another package, say MOSS or OMAS
      ;; several cases:
      ;;  - if there is no recorded value, use the current value
      ;;  - if there is a recorded and both values are PDM entries, fuse them
      ;;  - if there is a recorded object and it is not a PDM entry, shadow it
      ;; we shadow the inherited value with that from the disk if it is different.
      (when (and *database-pathname* 
                 (boundp id) (symbol-value id)
                 ;; symbol inherited from another package?
                 (not (eql (symbol-package id) *package*))
                 ;; we allow *language* and *context* to differ on disc vs in core
                 (not (eql id (intern "*LANGUAGE*")))
                 (not (eql id (intern "*CONTEXT*")))
                 )
        
        ;; make temporary id for the loaded data needed for the is-entry? test
        (setq value (intern "ENTRY"))
        
        ;; try to load data from the database, assigning if to entry
        ;; returns 2 values: the value of the value of id, t if it comes from a
        ;; database, nil otherwise
        (multiple-value-setq (val from-disc?)
          (%load-value-from-database id))
        
        ;; set the value of *package*::ENTRY
        (set value val)
        
        ;; several cases
        (cond
         ;; there is no database or the value is not in it, use the current one
         ((null from-disc?))
         ;; core value and recorded values are equal, use the current one JPB1202
         ((equal (symbol-value id)(symbol-value value)))
         ;; recorded and core value both are entry points
         ;; if objects are entry points, then we must merge them regardless of the
         ;; current context and the context in which they were defined (entry points
         ;; are valid across all contexts. Then, once they are merged, we can return
         ;; the id and let the calling code decide if they are valid in this context
         ((and (%is-entry? id) (%is-entry? value))
          ;; fuse entry points (merges only the inverse relations)
          (set id (db-fuse-ep id (intern (package-name *package*) :keyword))))
         ;; any other case is an error
         (t (error "in core and disk value of ~S are different: 
in core: ~S~%in the database: ~S" id (symbol-value id) (symbol-value value)))))
      )
    ;; we return key in all cases
    id))

;;;(defUn %ldif (id &optional object-type)
;;;  "takes a symbol, if it is unbound or nil tries to load its value from disk, ~
;;;   provided the database exists and is opened. Set the id to its value. When the ~
;;;   database is partitioned into several sub-databases, each partition is indexed ~
;;;   according to the package symbols, thus, the symbol is loaded according to its ~
;;;   package.
;;;Argument:
;;;   id: a symbol, normally, a moss object-id, but NIL is allowed
;;;   object-type (opt): type of object (used to load extra stuff)
;;;Return:
;;;   the id, even if NIL (no error)"
;;;  (declare (special *package*))
;;;  (let ((context (symbol-value (intern "*CONTEXT*")))
;;;        value)
;;;    (unless (symbolp id)
;;;      (error "argument ~S should be a symbol." id))
;;;    
;;;    ;; if id is nil skip the rest (nil is a symbol)
;;;    (when id
;;;      (when (and (or (not (boundp id)) (null (symbol-value id)))
;;;                 ;; the loading function is independent from versions
;;;                 (setq value (%load-value-from-database id)))
;;;        ;; here id was not bound, or bound and nil, and we accesed database to
;;;        ;; obtain a non nil value 
;;;        (set id value)
;;;        ;; now, according to type we load extra stuff
;;;        ;; the following lines are executed only when the object-type is passed
;;;        ;; in argument, which occurs when we re-initialize a persistent agent 
;;;        ;; environment from disc
;;;        (case object-type
;;;          ;; if method, load corresponding function
;;;          (:method
;;;           ;(format t "~% %ldif / methods:~% ~S" =make-entry)
;;;           ;; quasi brute-force recovery of function name
;;;           (let* ((fnam (car (%%get-value id '$FNAM context)))
;;;                  (fn (db-load fnam (intern (package-name *package*) :keyword))))
;;;             (when fn 
;;;               (set fnam fn)
;;;               #+COMPILER
;;;               (compile fnam fn)
;;;               #-COMPILER
;;;               (eval `(defUn ,fnam ,fn)))))
;;;          (:function
;;;           (let ((expr (symbol-value id)))
;;;             #+COMPILER
;;;             (compile (car expr) (cdr expr))
;;;             #-COMPILER
;;;             (eval `(defUn ,(car expr) ,@(cddr expr)))))
;;;          )
;;;        ;; should we return at this point? Yes, because we either started with an
;;;        ;; unbound or nil value, thus we are not in the case of having to merge
;;;        ;; a disk value with an already existing one
;;;        (return-from %ldif id)
;;;        )
;;;      
;;;      ;; when the symbol has a value, but the value belongs to another package
;;;      ;; this case occurs when we are loading entry points that are already defined
;;;      ;; in another package, say MOSS or OMAS
;;;      ;; several cases:
;;;      ;;  - if there is no recorded value, use the current value
;;;      ;;  - if there is a recorded and both values are PDM entries, fuse them
;;;      ;;  - if there is a recorded object and it is not a PDM entry, shadow it
;;;      ;; we shadow the inherited value with that from the disk if it is different.
;;;      (when (and *database-pathname* 
;;;                 (boundp id) (symbol-value id)
;;;                 ;; symbol inherited from another package?
;;;                 (not (eql (symbol-package id) *package*)))
;;;        
;;;        ;; make temporary id for the loaded data needed for the is-entry? test
;;;        (setq value (intern "ENTRY"))
;;;        
;;;        ;; try to load data from the database, assigning if to entry
;;;        ;; returns 2 values: the value of the value of id, t if it comes from a
;;;        ;; database, nil otherwise
;;;        (set value (%load-value-from-database id))
;;;        
;;;        ;; several cases
;;;        (cond
;;;         ;; value is nil or no recorded value, use the current one
;;;         ((null (symbol-value value)))
;;;         ;; core value and recorded values are equal, use the current one JPB1202
;;;         ((equal (symbol-value id)(symbol-value value)))
;;;         ;; recorded and core value both are entry points
;;;         ;; if objects are entry points, then we must merge them regardless of the
;;;         ;; current context and the context in which they were defined (entry points
;;;         ;; are valid across all contexts. Then, once they are merged, we can return
;;;         ;; the id and let the calling code decide if they are valid in this context
;;;         ((and (%is-entry? id) (%is-entry? value))
;;;          ;; fuse entry points (merges only the inverse relations)
;;;          (set id (db-fuse-ep id (intern (package-name *package*) :keyword))))
;;;         ;; any other case is an error
;;;         (t (error "in core and disk value of ~S are different: 
;;;in core: ~S~%in the database: ~S" id (symbol-value id) (symbol-value value)))))
;;;      )
;;;    ;; we return key in all cases
;;;    id))

#|
When the persistent agent :TEST is loaded, then there is a :test partition in the
database
(moss::db-show-structure)
TEST(4): 
===== Structure of the ACL-FIREMEN database:
  :TEST
=====
:DONE

(moss::db-load '$E-PERSON.1 :test)
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.1)) (TEST::$T-PERSON-NAME (0 "Albert")))
T

;;; db-load does not set the symbol
(boundp '$E-PERSON.1)
NIL

(moss::%ldif '$E-PERSON.1)
$E-PERSON.1

;;; %ldif sets the symbol
$E-PERSON.1
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1)) ($T-PERSON-NAME (0 "Albert")))

;;; this person does not exist: %ldif returns the symbol unbound
(moss::%ldif '$E-PERSON.333)
$E-PERSON.333

(boundp '$E-PERSON.333)
NIL
|#
;;;A----------------------------------------------------------------- %%LDIF-RAW
;;; not version sensitive

(defun %%ldif-raw (obj-id)
  "takes a symbol, if it is unbound or its value is nil and a database is ~
   available, then tries to load it from disk. Loading is brutal because the ~
   function is called by %pdm? or %resolve and must not get into complicated ~
   processes that could lead to infinite loops.
   When loading from disk, we save the initial value of obj-id (:unbound or nil) ~
   it could be used to reset the value of the object once the test has been done.
Argument:
   obj-id: must be a symbol
Return:
   obj-id, as it was if we could not get a value from disc or if its value was ~
   NIL, otherwise set to its disc value.
Side-effect:
   set obj-id to the value obtained from an opened database except if"
  (let (value disc)
    (unless (symbolp obj-id)
      (error "%%ldif-raw: argument ~S should be a symbol." obj-id))
    
    ;; if id is nil skip the rest (nil is a symbol)
    (when obj-id
      (when (or (not (boundp obj-id)) (null (symbol-value obj-id)))
        (multiple-value-setq (value disc)
          ;; the loading function is independent from versions
          (%load-value-from-database obj-id))
        ;; if disc is t, then we got a value from an opend database
        (if disc (set obj-id value))
        ))
    obj-id))

#|
(%%ldif-raw "Albert")
Error: %%ldif-raw: argument "Albert" should be a symbol.

(%%ldif-raw NIL)
NIL

(with-package :test
  (%%ldif-raw 'test::$E-PERSON))
TEST::$E-PERSON
((MOSS::$TYPE (0 MOSS::$ENT)) (MOSS::$ID (0 $E-PERSON)) (MOSS::$ENAM (0 ((:EN "PERSON"))))
 (MOSS::$RDX (0 $E-PERSON)) (MOSS::$ENLS.OF (0 $SYS.1)) (MOSS::$CTRS (0 $E-PERSON.CTR))
 (MOSS::$DOCT (0 ((:EN "no doc")))) (MOSS::$PT (0 $T-PERSON-NAME $T-PERSON-FIRST-NAME))
 (MOSS::$IS-A.OF (0 $E-STUDENT)))

;; here symbol is unbound but value exists in the database
(with-package :test
  (%%ldif-raw 'test::$E-PERSON.1))
TEST::$E-PERSON.1
((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.1)) ($T-PERSON-NAME (0 "Albert")))

;; here symbol is unbound and value does not exist in the database
(with-package :test
  (%%ldif-raw 'test::$E-PERSON.111))
TEST::$E-PERSON.111
Error: Attempt to take the value of the unbound variable `TEST::$E-PERSON.111'.
|#
;;;+---------------------------------------------------------------------- %LINK

(defUn %link (obj1-id sp-id obj2-id &optional version)
  "Brute force link two objects using the structural property id - ~
   No checking is done on args - crude link to be able ~
   to bootstrap without the properties really existing. Here we manipulate ~
   the names. We assume that sp-id is not an inverse property ~
   If the link already exists then does nothing. Otherwise, introduces a ~
   new link modifying or creating a value in the specified context. ~
   This might look like a bit complicated a piece of code, however we try ~
   to do simple tests first to avoid long execution times.
Arguments:
   obj1-id: identifier for first object
   sp-id: identifier of the local linking property
   obj2-id: identifier for the second object
   version (opt): context (default current)
Return:
   list representing the first object"
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    ;; checks are done by %add-value
    (%add-value obj1-id sp-id obj2-id context)        
    (%add-value obj2-id (%inverse-property-id sp-id) obj1-id context)
    ;; record if editing
    (save-old-value obj1-id)
    (save-old-value obj2-id)
    (symbol-value obj1-id)
    ))

#|
test::$E-PERSON.1
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.1))
 (TEST::$T-PERSON-NAME (0 "Barth�s"))
 (TEST::$T-PERSON-FIRST-NAME (0 "Jean-Paul")) 
 (TEST::$S-PERSON-HUSBAND.OF (0 TEST::$E-PERSON.2))
 (TEST::$S-ORGANIZATION-PRESIDENT.OF (5 TEST::$E-ORGANIZATION.1)))
test::$E-PERSON.2
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.2))
 (TEST::$T-PERSON-NAME (0 "Barth�s-Biesel" "Barth�s"))
 (TEST::$T-PERSON-FIRST-NAME (0 "Dominique"))
 (TEST::$S-PERSON-HUSBAND (0 TEST::$E-PERSON.1)))

(with-package :test
  (%link 'test::$E-PERSON.1 'test::$S-PERSON-WIFE 'test::$E-PERSON.2 2))
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.1)) (TEST::$T-PERSON-NAME (0 "Barth�s"))
 (TEST::$T-PERSON-FIRST-NAME (0 "Jean-Paul")) (TEST::$S-PERSON-HUSBAND.OF (0 TEST::$E-PERSON.2))
 (TEST::$S-ORGANIZATION-PRESIDENT.OF (5 TEST::$E-ORGANIZATION.1))
 (TEST::$S-PERSON-WIFE (2 TEST::$E-PERSON.2)))
|#
;;;A----------------------------------------------------------------- %LINK-ALL

(defUn %link-all (obj1-id sp-id obj-list &optional version)
  "Brute force link an object to a list of objects using the structural property ~
   id - No cardinality check is done - sp-id must be direct property.
    (%link obj1-id sp-id obj-list context) - links two objects ~
   using sp-id - no checking done on args - crude link to be able ~
   to bootstrap without the properties really existing. Here we manipulate ~
   the names. We assume that sp-id is not an inverse property ~
   If the link already exists then does nothing. Otherwise, introduces a ~
   new link modifying or creating a value in the specified context. ~
   This might look like a bit complicated a piece of code, however we try ~
   to do simple tests first to avoid long execution times.
Arguments:
   obj1-id: identifier for first object
   sp-id: identifier of the local linking property
   obj-list: identifier for the second object
   version (opt): context (default current)
Return:
   list representing the first object"
  ;; checks are done by %add-value
  (let* ((context (or version (symbol-value (intern "*CONTEXT*"))))
        (inv-id (%inverse-property-id sp-id context)))
    (dolist (obj2-id obj-list)
      (%add-value obj1-id sp-id obj2-id context)            
      (%add-value obj2-id inv-id obj1-id context)
      (save-old-value obj1-id)
      (save-old-value obj2-id)
      )
    (symbol-value obj1-id)
    ))

;;;A-------------------------------------------------- %LOAD-VALUE-FROM-DATABASE
;;; this function can be used to load an object from an opened database, whether 
;;; a MOSS knowledge base or an agent database when the agent is persistent
;;; NIL is a symbol but cannot be a key. So there should not be any special
;;; treatment for NIL, i.e. it should trigger an error...
;;; Actually, we can store a value indexed by NIL, but not retrieve it (db-load
;;; checks that the key must not be NIL)
;;; The function is not version sensitive

(defun %load-value-from-database (xx)
  "checks if a database is opened, and if so loads the object. OMAS persistency ~
   is taken care of.
Argument:
   xx: must be a non NIL symbol, otherwise error
Return:
   2 values
    - the value associated to the key
    - t if value comes from database, or nil if there is no database or the key ~
      is not in the database
Error:
    - if there is a database and the key is nil"
  (declare (special *database-pathname* *package*))
  (let (#+omas agent-id val disk)
    
    (unless (symbolp xx)
      (error "moss::%load-value-from-database: argument ~S is not a symbol" xx))
          
    (cond
     ((and *database-pathname* 
           ;; if we are executing inside an agent package, the database 
           ;; can be opened for a given application, but the current agent may
           ;; not be persistent
           #+OMAS
           (and
            ;(member :OMAS *features*)
            (setq agent-id (omas::%agent-from-package *package*))
            (omas::persistency agent-id))
           )
      ;; load value from database, make temporary id for loaded object
      (multiple-value-setq (val disk)
        (db-load xx (intern (package-name *package*) :keyword)))
      ;; needed for the is-entry? test
      (values val disk))
     ;; if there is no database
     (t (values nil nil)))))

#|
;;; maybe should declare an error here. Error only occurs if there is an attempt
;;; to load something
(moss::%load-value-from-database NIL)
NIL
NIL

(with-package :test
  (moss::%load-value-from-database NIL))
Error: db-load(:TEST): key NIL must be a non nil symbol.

(moss::%load-value-from-database "$E-PERSON.1")
Error: moss::%load-value-from-database: argument "$E-PERSON.1" is not a symbol

;;; in the MOSS package, $E-PERSON.1 does not exist
(moss::%load-value-from-database '$E-PERSON.1)
NIL
NIL
(with-package :test
  (%load-value-from-database 'test::$E-PERSON.1)
  )
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.1)) (TEST::$T-PERSON-NAME (0 "Albert")))
T

(with-package :test
  (%load-value-from-database 'test::$E-PERSON.333)
  )
NIL
NIL
|#
;;;=============================================================================
;;;                        MAKE functions
;;;=============================================================================

;;; MAKE functions are used to build structures
#|
;;;--------------------------------------------------------- %MAKE-ACCESSOR-NAME
;;; the accessor name should be exported, but maybe not here
;;; nobody seems to use that.

(defun %make-accessor-name (property-name)
  "Builds a name for an accessor method from the specified property-name ~
   so that for example the method =name can be used to access the name value-list.
   The name in interned into the MOSS package.
Argument:
   property-name: name of the property "
  (intern (make-name '= property-name) (find-package :moss))
  )
|#

;;;A------------------------------------------------------- %MAKE-ENTITY-SUBTREE

(defUn %make-entity-subtree (ens &optional version)
  "takes an entity and builds the subtree of its sub-classes, e.g.
   (A (B C (D E) F)) 
   where (D E) are children of C and (B C F) are children of A.
Arguments:
   ens: entity to process
   version (opt): context (default current)
Return:
   (ens) if no subtree, or (ens <subtree>)."
  (let* ((context (or version (symbol-value (intern "*CONTEXT*"))))
         (successors (%get-value ens (%make-id-for-inverse-property '$IS-a)
                                 context)))
    ;; when function returns nil, we have no child. Thus, remove nil
    (remove nil 
            (list ens 
                  (reduce  ; JPB 140820 removing mapcan
                   #'append
                   (mapcar
                       #'(lambda (xx) (%make-entity-subtree xx context))
                     successors))))))

#|
(with-package :test
  (%make-entity-subtree 'test::$E-person 0))
(TEST::$E-PERSON (TEST::$E-STUDENT))

(with-package :test
  (%make-entity-subtree 'test::$E-person 4))
(TEST::$E-PERSON (TEST::$E-STUDENT TEST::$E-TEACHER))
|#
;;;A---------------------------------------------------------- %MAKE-ENTITY-TREE
;;; Used for building the ontology window

(defUn %make-entity-tree (&optional version)
  "builds the forest corresponding to the application classes.
Arguments:
   version (opt): default is *context*
Return:
   a tree like (A (B C (D E) F) G H)
   where (D E) are children of C and (B C F) are children of A, and G and H are
   parallel trees."
  ;; get the list of application classes
  (let ((context (or version (symbol-value (intern "*CONTEXT*"))))
        (class-list (%get-application-classes))
        forest first-level)
    ;; remove classes that have an $IS-A property
    (setq first-level
          (remove-if #'(lambda(xx)(or (not (%alive? xx context))
                                       (%%get-value xx '$IS-A context)))
                     class-list))
    ;; build forest for the remaining classes
    (dolist (ens first-level)
      (setq forest (append forest (%make-entity-subtree ens context))))
    ;; return result
    forest))

#|
(with-package :test
  (moss::%MAKE-ENTITY-TREE 0))
($E-PERSON ($E-STUDENT) $E-ORGANIZATION $E-OMAS-AGENT $E-OMAS-GOAL $E-OMAS-SKILL)

(with-package :test
  (moss::%MAKE-ENTITY-TREE 4))
($E-PERSON ($E-STUDENT $E-TEACHER) $E-ORGANIZATION $E-OMAS-AGENT $E-OMAS-GOAL
           $E-OMAS-SKILL)
|#
;;;+---------------------------------------------------- %MAKE-ENTITY-TREE-NAMES

(defUn %make-entity-tree-names (forest)
  "takes a forest of entity-ids and returns the same structure with entity names.
Arguments:
  forest: a structure made of entity ids
Returns:
  the same structure with entity names"
  (cond ((null forest) nil)
        ((atom forest)
         (catch :error
                (%string-norm 
                 (mln::get-canonical-name ; jpb 1406
                  (car (%get-value forest '$ENAM))))))
        ((cons (%make-entity-tree-names (car forest))
               (%make-entity-tree-names (cdr forest))))))

#|
(with-package :test
  (test::$E-PERSON (test::$E-STUDENT) test::$E-OMAS-AGENT test::$E-OMAS-GOAL
  test::$E-OMAS-SKILL))

|#
;;;A-------------------------------------------------------- %MAKE-ENTRY-SYMBOLS

(defUn %make-entry-symbols (value-ref &key type prefix)
  "Builds entry symbols. If input is a symbol, leave it as it is. If input is a ~
   number, make a symbol (e.g. |2011|). If input is a multilingual-name builds ~
   entry symbols for each of the synonyms in each specified language.
   Note that a list of symbols is not a valid input argument.
Arguments:
   value-ref: string, symbol or multilingual-name specifying the entry points
   tp-id: local attribute identifier
   type (key): type of MOSS object (default is nil)
   prefix (key): prefix for produced symbols
Return:
   list of the entry point symbols"
  (let (syn-list)
    ;; if already a symbol return it as a list
    (cond
     ((symbolp value-ref)
      (return-from %make-entry-symbols (list value-ref)))
     
     ((integerp value-ref)
      (return-from %make-entry-symbols (list (intern (string+ value-ref)))))
     
     ((stringp value-ref)
      (return-from %make-entry-symbols 
        (list (%%make-name value-ref type :prefix prefix))))
     
     ((mln::%mln? value-ref)
      (setq value-ref (mln::make-mln value-ref)))
     
     ((mln::mln? value-ref))
     
     (t
      (error "~%~S ~%should be a symbol, string, an integer or a multilingual name ~
         when building entry symbols."
        value-ref))
     )
    
    ;; here we have an MLN
    (setq syn-list (mln::extract-all-synonyms value-ref))
    
    ;; then, cook up an entry for each name
    (delete-duplicates
     ;(mapcar #'(lambda (xx) (%make-name type :name xx :prefix prefix)) syn-list))
     (mapcar #'(lambda (xx) (%%make-name xx type :prefix prefix)) syn-list)) ; JPB1507
    ))

;;;(defUn %make-entry-symbols (value-ref &key type prefix)
;;;  "Takes a multilingual-name as input and builds entry symbols for each of the ~
;;;   synonyms in each specified language.
;;;   Note that a list of symbols is not a valid input argument.
;;;Arguments:
;;;   value-ref: string, symbol or multilingual-name specifying the entry points
;;;   tp-id: local attribute identifier
;;;   type (key): type of MOSS object (default is nil)
;;;   prefix (key): prefix for produced symbols
;;;Return:
;;;   list of the entry point symbols"
;;;  ;; if already a symbol return it as a list
;;;  (if (symbolp value-ref)
;;;      (return-from %make-entry-symbols (list value-ref)))
;;;  (let (syn-list)
;;;    ;;========== take care of old MLN format
;;;    (if (mln::%mln? value-ref)(setq value-ref (mln::make-mln value-ref)))
;;;    ;; arg should be a string or an MLN
;;;    (unless (or (stringp value-ref)
;;;                (mln::mln? value-ref)) ; jpb 1406
;;;      (error "~%~S ~%should be a string or a multilingual name when building ~
;;;              entry symbols."
;;;        value-ref))
;;;    
;;;    ;; a simple string could be a multilingual string (?)
;;;    (when (stringp value-ref)
;;;      (return-from %make-entry-symbols 
;;;        (list (%%make-name value-ref type :prefix prefix)))) ; jpb1507
;;;    
;;;    ;; here we have an MLN
;;;    (setq syn-list (mln::extract-all-synonyms value-ref))
;;;    
;;;    ;; then, cook up an entry for each name
;;;    (delete-duplicates
;;;     ;(mapcar #'(lambda (xx) (%make-name type :name xx :prefix prefix)) syn-list))
;;;     (mapcar #'(lambda (xx) (%%make-name xx type :prefix prefix)) syn-list)) ; JPB1507
;;;    ))

#|
(%make-entry-symbols '((:en "relation" "structural property")
                       (:fr "relation" "propri�t� de structure")) )
(STRUCTURAL-PROPERTY RELATION PROPRI�T�-DE-STRUCTURE)

(%make-entry-symbols '(:en "relation; structural property"
                           :fr "relation;propri�t� de structure"))
(STRUCTURAL-PROPERTY RELATION PROPRI�T�-DE-STRUCTURE)

(%make-entry-symbols "albert")
(ALBERT)

(%make-entry-symbols 2011)
(|2011|)

(%make-entry-symbols '(le   jour le plus long))
Error: 
(LE JOUR LE PLUS LONG) 
should be a symbol, string, an integer or a multilingual name when building entry symbols.

(%make-entry-symbols '((:en "name") (:fr "nom")) :type '$EPR)
(HAS-NAME HAS-NOM)
|#
;;;A------------------------------------------------------------------- %MAKE-EP
;;; entry is a symbol hopefully produced in a specific package. tp-id and obj-id
;;; are symbols specifying the attribute and object to be referenced.
;;; E.g. entry = test::BARTHES 
;;;      tp-id = test::$T-PERSON-NAME
;;;      obj-id = test::$E-PERSON.1
;;; It is not necessary to specify a package.
;;; However, it is important to specify a context (usually the current one for
;;; a given agent).

(defUn %make-ep (entry tp-id obj-id &key context export)
  "Adds a new object to an entry-point if it exists - Otherwise creates the ~
   entry point(s) and record it/them in the current system (*moss-system*).
   Exports the entry symbol(s).
Arguments:
   entry: symbol or list of symbols specifying the entry point(s)
   tp-id: local attibute identifier
   obj-id: identifier of object to index
   context (key): context (default current)
   export (key): it t export entry points from the package
Return:
   internal format of the entry point object or a list of them"
  (declare (special *objects-to-be-saved*))
  ;(format t "~%;=== %make-ep /*context*: ~S in package ~S" 
  ;  (symbol-value (intern "*CONTEXT*")) *package*)
  ;; %%alive? throws if obj-id (to be indexed) does not exist
  ;;********** it seems that %%alive? changes the value of *context*
  ;(setq obj-id (%%alive? obj-id (symbol-value (intern "*CONTEXT*"))))
  (let ((moss-system (intern "*MOSS-SYSTEM*"))
        (context (or context (symbol-value (intern "*CONTEXT*")))))
    ;(format t "~%;=== %make-ep /entry: ~S" entry)
    
    ;; entry may be a list in which case we recurse
    (cond
     ;; recursive test
     ((null entry) (return-from %make-ep nil))
	 
     ((listp entry)
      (return-from %make-ep 
        (mapcar #'(lambda (xx) 
                    (%make-ep xx tp-id obj-id :context context :export export))
          entry)))
     
     ((%pdm? entry)
      ;; when entry point exists, then
      ;(format t "~%;=== %make-ep /entry point exists" entry)
      ;; first save current value when OMAS agent is editing
      (save-old-value entry)
      ;; add entry to the existing entry-point, check that current system 
      ;; has been recorded in the EPLT inverse link. Otherwise do it.
      (%add-value 
       entry (%inverse-property-id tp-id) obj-id context)
      ;; we can do that unless *moss-system* has not been created yet
      ;; kludge useful when we are creating an agent MOSS environment
      (if (boundp moss-system)
          (progn
            (unless (member (symbol-value moss-system)
                            (%get-value entry (%inverse-property-id '$EPLS)))
              ;; input into the local MOSS environment JPB 1001
              (%link (symbol-value moss-system) '$EPLS entry)))
        ;; otherwise save info to do it later
        (pushnew (cons entry '$EPLS) *objects-to-be-saved* :test #'equal)))
     
     ;; otherwise, create the entry point and link it to the current system
     (t
      ;; we set the $TYPE  and $ID properties in context 0
      (set entry `(($TYPE (,context $EP))
                   ($ID (,context ,entry))
                   (,(%inverse-property-id tp-id)
                      (,context ,obj-id))))
      (if export (export entry))
      ;; save when editing
      (save-new-id entry)
      
      ;; add entry to the list of system entry points
      (if (boundp moss-system)          
          (%link (symbol-value moss-system) '$EPLS entry context)
        (pushnew (cons entry '$EPLS) *objects-to-be-saved* :test #'equal)
        )))
    (symbol-value entry)))

#|
(%make-ep 'JUNK1 '$PT '$ENAM)
(($TYPE (0 $EP)) ($ID (0 JUNK1)) ($PT.OF (0 $ENAM)) ($EPLS.OF (0 $SYS.1)))

(%make-ep '(JUNK1 JUNK2 JUNK3) '$PT '$ENAM)
((($TYPE (0 $EP)) ($ID (0 JUNK1)) ($PT.OF (0 $ENAM)) ($EPLS.OF (0 $SYS.1)))
 (($TYPE (0 $EP)) ($ID (0 JUNK2)) ($PT.OF (0 $ENAM)) ($EPLS.OF (0 $SYS.1)))
 (($TYPE (0 $EP)) ($ID (0 JUNK3)) ($PT.OF (0 $ENAM)) ($EPLS.OF (0 $SYS.1))))

(%make-ep () '$PT '$ENAM)
NIL

(setq test::*version-graph*
   '((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0)))

(with-package :test
  (with-context 2
    (%make-ep 'test::JUNK5 'test::$T-PERSON-NAME 'test::$E-PERSON.2)))
(($TYPE (2 $EP)) ($ID (2 TEST::JUNK5)) (TEST::$T-PERSON-NAME.OF (2 TEST::$E-PERSON.2))
 ($EPLS.OF (0 TEST::$SYS.1)))
BUG

(with-package :test
  (with-context 6
    (%make-ep 'test::JUNK5 'test::$T-PERSON-NAME 'test::$E-PERSON.2)))
(($TYPE (0 $EP)) ($ID (0 TEST::JUNK5))
 (TEST::$T-PERSON-NAME.OF (6 TEST::$E-PERSON.2) (2 TEST::$E-PERSON.2))
 ($EPLS.OF (6 TEST::$SYS.1) (2 TEST::$SYS.1)))
|#
;;;A---------------------------------------------------------- %MAKE-EP-FROM-MLN
;;; entry point should be in the same package as obj-id
;;; New MLN format 14/06/25

(defUn %make-ep-from-mln (mln tp-id obj-id  &key type export)
  "Takes a multilingual-name as input and builds entries for each of the ~
   synonyms in each specified LEGAL language in package of obj-id.
Arguments:
   mln: legal multilingual-name specifying the entry points
   tp-id: local attibute identifier
   obj-id: identifier of object to index
   context (opt): context (default current)
   type (key): type of object, e.g. $ENT, $EPR, $FN, ... (see %make-name)
   export (key): if t then we should export entry points from their package
Return:
   list of the entry point object (internal format)"
  (let (syn-list entry)
    ;; mln should be well formatted (check for illegal languages)
    (setq mln (mln::make-mln mln))
    ;; obj-id not dead in current context?
    (setq obj-id (%alive? obj-id (symbol-value (intern "*CONTEXT*"))))
    ;; OK proceed...
    
    ;; get all synonyms
    (setq syn-list (mln::extract-all-synonyms mln))
    ;; for each of them create an entry point (record for control purposes)
    ;(mapcar #'(lambda (xx) (list (setq entry (%make-name type :name xx))
    (mapcar #'(lambda (xx) (list (setq entry (%%make-name xx type))
                                 (%make-ep entry tp-id obj-id :export export)))
      syn-list)))

#|
(defattribute "header")
$T-HEADER
(%make-ep-from-mln 
 '((:en "header") (:fr "titre")) '$PNAM '$T-header :type '$EPT)
((HAS-HEADER (($TYPE (0 $EP)) ($ID (0 HAS-HEADER)) ($PNAM.OF (0 $T-HEADER)) 
              ($EPLS.OF (0 $SYS.1))))
 (HAS-TITRE (($TYPE (0 $EP)) ($ID (0 HAS-TITRE)) ($PNAM.OF (0 $T-HEADER))
             ($EPLS.OF (0 $SYS.1)))))

(with-package :test
  (with-context 2
    (moss::%make-ep-from-mln '((:en "header") (:fr "titre")) '$PNAM '$T-header
                             :type '$EPT)))
((TEST::HAS-HEADER
  (($TYPE (0 $EP)) ($ID (0 TEST::HAS-HEADER)) ($PNAM.OF (2 $T-HEADER)) 
   ($EPLS.OF (2 TEST::$SYS.1))))
 (TEST::HAS-TITRE
  (($TYPE (0 $EP)) ($ID (0 TEST::HAS-TITRE)) ($PNAM.OF (2 $T-HEADER)) 
   ($EPLS.OF (2 TEST::$SYS.1)))))
|#
;;;=============================================================================
;;;               A series of function to cook up identifiers
;;;=============================================================================

;;;A------------------------------------------------------------------ %%MAKE-ID
;;; ***** problem with *any* and *none*
;;; semantics of this function is unclear
;;; build a specific id for MOSS objects according to their type
#|
%MAKE-ID-FOR-CLASS 
%MAKE-ID-FOR-VIRTUAL-CLASS 
%MAKE-ID-FOR-CLASS-SP 
%MAKE-ID-FOR-CLASS-TP 
%MAKE-ID-FOR-COUNTER 
%MAKE-ID-FOR-IDEAL 
%MAKE-ID-FOR-INSTANCE 
%MAKE-ID-FOR-INSTANCE-METHOD 
%MAKE-ID-FOR-INVERSE-PROPERTY 
%MAKE-ID-FOR-ORPHAN 
%MAKE-ID-FOR-OWN-METHOD 
%MAKE-ID-FOR-SP 
%MAKE-ID-FOR-TP 
%MAKE-ID-FOR-UNIVERSAL-METHOD 
|#

(defUn %%make-id (type-key &key name class-id prefix value prop-id)
  "Generic function to make object identifiers.
Arguments:
   type-key: type of the requested id, legal keys are
             :attribute :att :tp
             :class :concept
             :counter
             :ideal (require existence of class)
             :individual :instance :indiv :inst
             :instance-method
             :inverse-property :inverse-link :inv (require existence of property)
             :orphan (requires value option ref is ignored)
             :own-method
             :relation :rel :sp
             :universal-method
             :vclass virtual-concept             
   class-id (key): id of class (used by ideal, instance)
   ideal (key): if there indicates we want an ideal, :id option must be there
   name (key): a symbol or string used by class and property options
   prefix (key): additional name (string), e.g. for defining properties 
                 (the class name)
   prop-id (key): id of property (used by inverse-link)
   value (key): value for an instance
Return:
   unique new symbol."
  
  (case type-key
    
    ;; class
    ((:class :concept)
     (let ((class-name (%string-norm name)))
       ;; special cases for *any* and *none*
       (cond
        ((string-equal class-name "*ANY*") (intern "*ANY*"))
        ((string-equal class-name "UNIVERSAL-CLASS") (intern "*ANY*"))
        ((string-equal class-name "*NONE*") (intern "*NONE*"))
        ((string-equal class-name "NULL-CLASS") (intern "*NONE*"))
        (t (intern (make-name "$E-" (%string-norm name)))))))
    
    ((:vclass :virtuel-concept)
     (intern (make-name "$V-" (%string-norm name))))
    
    ;; relation
    ((:sp :rel :relation)
     (intern (make-name "$S-" (or prefix "") (if prefix "-" "") (%string-norm name))))
    
    ;; attribute
    ((:tp :att :attribute)
     (intern (make-name "$T-" (or prefix "") (if prefix "-" "") (%string-norm name))))
    
    ;; inverse links required id of direct property
    ((:inv :inverse-link :inverse-property)
     (unless (or (%is-attribute? prop-id) (%is-relation? prop-id))
       (verbose-throw 
        :error "ref ~S should be a property id for making inv-rel-id." prop-id))
     (intern (make-name prop-id '.OF) (symbol-package prop-id)))
    
    ;; counter
    (:counter
     (cond
      ;; if id is there, OK -> $ENT.CTR
      (class-id
       (intern (make-name class-id ".CTR") (symbol-package class-id)))
      ;; otherwise give warning if in moss package, but build counter id
      ((and (eql *package* (find-package :moss)) value)
       (intern
        (apply #'make-name "$CTR" '#\. value nil)))
      ;; in application package build standard counter, e.g. $CTR.23
      (value
       (%%make-id :inst :class-id (intern "$E-COUNTER") :value value))
      (t (verbose-throw :error "can't make counter id, missing value ?"))
      ))
    
    ;; ideal, intern the ideal id into the package of the class-id
    ;; class must exist 
    (:ideal
     (unless (%is-concept? class-id)
       (verbose-throw 
        :error "can't find the class associated to class-ref: ~S" class-id))
     (intern 
      (apply #'make-name (car (%get-value class-id '$RDX)) '#\. 0 nil)
      (symbol-package class-id)))
    
    ;; instance / individual. Class must exist
    ((:inst :indiv :instance :individual)
     (unless (%is-concept? class-id)
       (verbose-throw 
        :error "can't find the class associated to class-ref: ~S" class-id))
     (unless value 
       (verbose-throw :error "missing :value option for making entity identifier"))
     ;; instances are created in the current package, allowing method to use the
     ;; MOSS class, but to create local instances (e.g. for methods)
     (intern
      (apply #'make-name (car (%get-value class-id '$RDX)) '#\. value nil)
      (symbol-package class-id))
     )
    
    ;; instance and own methods
    ((:instance-method :own-method)
     (unless value 
       (verbose-throw :error "missing :value option for making method identifier"))
     (intern
      (if (eql *package* (find-package :moss))
          (make-name "$FN." value)
        (make-name "$E-METHOD." value))))
    
    ;; universal method are interned into the moss package...
    (:universal-method
     (unless value 
       (verbose-throw 
        :error "missing :value option for making universal method identifier"))   
     (intern (make-name "$UNI." value) (find-package :moss)))
    
    ;; orphans
    (:orphan
     (unless value 
       (verbose-throw :error "missing :value option for making orphan identifier"))
     (intern (make-name "$ORPHAN." value)))
    
    ;; entry-points (id is symbol) ?
    ((:ep :entry :entry-point)
     (verbose-throw :error "$EP id is the corresponding symbol"))   
    
    (otherwise 
     (verbose-throw :error "can't make an identifier. Unknown type: ~S" type-key))
    ))

#|
(%%make-id :ideal :class-id '$ENT)
$ENT.0
:INTERNAL

(%%make-id :vclass :name 'TEST)
MOSS::$V-TEST
NIL

(catch :error (with-package :test
                  (%%make-id :ideal :class-id 'test::$E-PERSONNE)))
"can't find the class associated to class-ref: MOSS::$E-PERSONNE"

(catch :error (with-package :test
                  (%%make-id :ideal :class-id 'test::$E-PERSON)))
TEST::$E-PERSON.0
:INTERNAL

(catch :error (%%make-id :inverse-link :prop-id 'test::$T-NAME))
TEST::$T-NAME.OF
:INTERNAL

(%%make-id :attribute :name "name" :prefix 'person)
$T-PERSON-NAME
:INTERNAL

(%%make-id :attribute :name "name")
MOSS::$T-NAME
:INTERNAL

(%%make-id :relation :name "brother")
$S-BROTHER
NIL

(%%make-id :class :name "person")
$E-PERSON
:INTERNAL

(%%make-id :class :name "*any*")
*ANY*
:INTERNAL

(with-package :test (%%make-id :class :name '*any*))
TEST::*ANY*
:INTERNAL

(%%make-id :orphan :value 44)
MOSS::$ORPHAN.44
NIL

(catch :error (%%make-id :inst :class-id '$E-PERSONNE :value 125))
"can't find the class associated to class-ref: $E-PERSONNE"

(%%make-id :inst :class-id 'test::$E-PERSON :value 125)
TEST::$E-PERSON.125
NIL

(%%make-id :counter :class-id '$EPS)
$EPS.CTR
:INTERNAL

(%%make-id :counter :value 25)
$CTR.25
NIL

(catch :error (with-package :test (%%make-id :counter :value 25)))
"can't find the class associated to class-ref: $E-COUNTER"

(catch :error (%%make-id :universal-method :value 55))
$UNI.55
:INTERNAL
|#
;;;A------------------------------------------------------------------- %MAKE-ID
;;; ***** problem with *any* and *none*. Fixed.
;;; Does about the same thing as %%make-id

(defUn %make-id (class-id  &key name prefix value id ideal)
  "Generic function to make object identifiers. If the symbol exists, then ~
   we throw to :error.
Arguments:
   class-id: identifier of the class of the object (*none* is authorized)
   name (key): useful name, e.g.class  name or property name
   prefix (key): additional name (string), e.g. for defining properties (the class name)
   value (key): value for an instance
   context (key): context default current
   id (key): id of class or property
   ideal (key): if there indicates we want an ideal, :id option must be there
   package (key): package into which new symbol must be inserted (default *package*)
Return:
   unique new symbol."
  (cond
   ((and (%is-model? class-id) ideal)  ; making ideal id into same package as class
    (intern 
     (apply #'make-name (car (%get-value class-id '$RDX)) '#\. 0 nil)
     (symbol-package class-id)))
   
   ((%is-a? class-id '$EIL)  ; inverse link into same package as direct link
    (unless id (error "missing :id option for making inverse-link identifier"))
    (intern (make-name id '.OF) (symbol-package id)))
   
   ((%is-a? class-id '$EP)    ; entry-point (id is symbol)
    (verbose-throw :error "$EP id is the corresponding symbol"))
   
   ((and prefix (%is-a? class-id '$EPS))
    (unless name (error "missing :name option for making relation identifier"))
    (intern (make-name "$S-" prefix "-" (%string-norm name)) 
            (if (symbolp name)(symbol-package name) *package*)))
   
   ((%is-a? class-id '$EPS)  ; relation
    (unless name (error "missing :name option for making relation identifier"))
    (intern (make-name "$S-" (%string-norm name)) 
            (if (symbolp name)(symbol-package name) *package*)))
   
   ((and prefix (%is-a? class-id '$EPT))
    (unless name (error "missing :name option for making attribute identifier"))
    (intern (make-name "$T-" prefix "-" (%string-norm name)) 
            (if (symbolp name)(symbol-package name) *package*)))
   
   ((%is-a? class-id '$EPT)  ; attribute
    (unless name (error "missing :name option for making attribute identifier"))
    (intern (make-name "$T-" (%string-norm name)) 
            (if (symbolp name)(symbol-package name) *package*)))
   
   ((%is-a? class-id '$EPR)  ; property
    (verbose-throw :error "$EPR cannot have instances"))
   
   ((eql class-id '*none*)   ; orphan
    (unless value (error "missing :value option for making orphan identifier"))
    (intern (make-name '$ORPHAN. value)))
   
   ;; kludge for computing SYSTEM ideal (same package as class)
   ((and (%is-a? class-id '$SYS) ideal)
    (intern 
     (apply #'make-name (car (%get-value class-id '$RDX)) '#\. 0 nil)
     (symbol-package class-id)))
   
   ;; check if ideal is asked for
   (ideal
    (verbose-throw :error "~S is not a valid class in package ~S context ~S"
                   class-id *package* (symbol-value (intern "*CONTEXT*"))))
   
   ((%is-a? class-id (intern "*ANY*")) ; any object
    (verbose-throw :error "instances cannot be of type *any*"))
   
   ((%is-a? class-id '$ENT)  ; class-identifier (instance of meta-class)
    (unless name (terror "missing :name option for making entity identifier"))
    ;; take care of special classes *any* and *none*
    (cond
     ((member name `(*none* "*none*" ,(list *language* "*none*")) :test #'equal+)
      '$ORPHAN)
     ((member name `(*any* "*any*" ,(list *language* "*any*")) :test #'equal+)
      '*any*)
     (t (intern (make-name "$E-" (%string-norm name))))))
   
   ((%is-a? class-id '$VENT) ; virtual class, same package as symbol (?)
    (intern (make-name "$V-" (%string-norm name))))
   
   ;; we must put $CTR here otherwise problems for creating $CTR.0
   ((%is-a? class-id '$CTR)
    ;(unless id (error "missing :id option for making counter identifier"))
    ;(intern (make-name id ".CTR") (symbol-package id)))
    ;; we try a different approach JPB1001
    (cond
     ;; if id is there, OK
     (id
      (intern (make-name id ".CTR") (symbol-package id)))
     ;; otherwise give warning if in moss package, but build counter name
     ((eql *package* (find-package :moss))
      (warn "missing :id option for making counter identifier of class: ~S"
        class-id)
      (intern
       (apply #'make-name (car (%get-value class-id '$RDX)) '#\. value nil)))
     ;; in application package build stnadard counter
     (t (intern
         (apply #'make-name (car (%get-value class-id '$RDX)) '#\. value nil)))
     ))
   
   ((%is-model? class-id)       ; regular class
    (unless value (error "missing :value option for making entity identifier"))
    ;; instances are created in the current package, allowing method to use the
    ;; MOSS class, but to create local instances (e.g. for methods)
    
    (intern
     ;(apply #'make-name (car (%get-value class-id '$RDX)) '#\. value nil)))
     (make-name (car (%get-value class-id '$RDX)) '#\. value)
     (symbol-package class-id)))
   
   (t (terror "can't make an identifier with input ~S" class-id))
   ))

#|
(unless (boundp '$e-person)
  (defconcept "person" (:att "name" (:entry))(:att "first name"))
  (defconcept "student" (:is-a "person")(:rel "adviser" (:to "person")))
  )
$E-STUDENT

(%make-id '$ENT :ideal t)
$ENT.0
:INTERNAL

(%make-id '$E-PERSON :ideal t)
$E-PERSON.0
:INTERNAL

(%make-id '*none* :value 33 :context 2)
Error: Illegal keyword given: :CONTEXT

(%make-id '$EIL :id '$T-NAME)
$T-NAME.OF
:INTERNAL

(%make-id '$EPS :name "name" :prefix 'person)
$S-PERSON-NAME 
NIL

(%make-id '$EPS :name "name")
$S-NAME
NIL

(catch :error (%make-id '*any*))
"instances cannot be of type *any*"

(%make-id '$ENT :name "person")
$E-PERSON
:INTERNAL

(%make-id '$ENT :name "*any*")
*ANY*

(%make-id '$FN :value 125)
$FN.125

(%make-id '$E-PERSON :value 125)
$E-PERSON.125
:INTERNAL

(%make-id '*none* :value 125)
$ORPHAN.125
:INTERNAL

(catch :error (%make-id '*any* :value 125))
"instances cannot be of type *any*"
|#
;;;A----------------------------------------------------- %MAKE-INVERSE-PROPERTY
;;; we do not use a language arg since the multilingual name arg provides
;;; the corresponding information

(defUn %make-inverse-property (id multilingual-name &key export)
  "create an inverse property for an attribute or a relation. Links it to the ~
   direct property, creates the name and entry point, adds it to MOSS.
   It is created in the same package as id and in the specified context.
   Uses the default language.
Arguments:
   id: identifier of the property to invert
   multilingual-name: name of the property to invert
Return:
   id of inverse property"
  (unless (mln::mln? multilingual-name) ; jpb 1406
    (terror "bad arg to %make-inverse-property, ~S should be a multilingual name."
            multilingual-name))
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        inv-id inv-mln)
    ;; First create the inverse-id in the same package as id
    (setq inv-id (%make-id-for-inverse-property id))
    ;; inverse name looks like: IS-xxx-OF
    ;; build a multilingual inverse name
    (setq inv-mln (%make-inverse-property-mln multilingual-name))
    ;; then create inverse object
    (set inv-id
         `(($TYPE (,context $EIL))	; object type is inverse link
           ($INAM (,context ,inv-mln))))
    ;; save when editing
    (save-new-id inv-id)

    ;; link id to its inverse (manually for bootstrapping)
    (%link id '$INV inv-id context)
    ;; add new inverse-property-id to system-list
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$EILS inv-id context)
    ;; create now entries for inverse names
    (%make-ep-from-mln inv-mln '$INAM inv-id :export export)
    ;; quit
    inv-id))

#|
(defattribute "son" (:is-a "person"))
$T-SON
(($TYPE (0 $EPT)) ($ID (0 $T-SON)) ($PNAM (0 ((:EN "son")))) ($ETLS.OF (0 $SYS.1))
 ($INV (0 $T-SON.OF)))

(with-package :test
  (with-context 4
    (defattribute "girl")))
TEST::$T-GIRL
(($TYPE (4 $EPT)) ($ID (4 TEST::$T-GIRL)) ($PNAM (4 ((:EN "girl")))) 
 ($ETLS.OF (4 TEST::$SYS.1))($INV (4 TEST::$T-GIRL.OF)))
|#
;;;A------------------------------------------------ %MAKE-INVERSE-PROPERTY-MLN

(defUn %make-inverse-property-mln (mln)
  "build an inverse multilingual name for the inverse property.
Arguments:
   mln: multilingual name
Result:
   a list of strings and an inverse mln"
  (declare (special *language-tags*))
  
  (unless (mln::mln? mln)
    (terror "bad arg to %make-inverse-property-mln, ~
             ~S should be a multilingual name."
            mln))
  
  (let (synonym-list symbol inv-mln)
    ;; try each valid language tag
    (dolist (tag *language-tags*)
      ;; first extract synonym-string
      (setq synonym-list (mln::filter-language mln tag)) ; jpb 1406
      (dolist (expr synonym-list)
        ;; build the inverse property name string disregarding language
        (setq symbol
              (%make-name-string-for-inverse-property (%string-norm expr)))
        (when symbol 
          (setq inv-mln (mln::add-value inv-mln symbol tag))))) ; jpb 1406
    inv-mln))

#|
(%make-inverse-property-mln '((:en "chapter head ")))
((:EN "IS-CHAPTER-HEAD-OF"))

(moss::%make-inverse-property-mln '((:EN "niece")(:fr "ni�ce")(:de "Ente")))
((:DE "IS-ENTE-OF") (:EN "IS-NIECE-OF") (:FR "IS-NI�CE-OF"))

(%make-inverse-property-mln '((:en "chapter head" "title")))
((:EN "IS-CHAPTER-HEAD-OF" "IS-TITLE-OF"))
|#
;;;A--------------------------------------------------------- %MAKE-MLN-FROM-REF
;;; should use mln::make-mln instead

(defUn %make-mln-from-ref (ref)
  "builds an MLN. If ref is a symbol or a string uses *language* (:all -> :unknown).~
   if ref is mln live it as it is. Independent from versions.
Argument:
   ref: symbol, string or mln
Return:
   mln."
  (cond
   ((mln::mln? ref) ref) ; jpb 1406
   ((mln::%mln? ref) (mln::make-mln ref)) ; old MLN format
   ((and (symbolp ref)(eql *language* :all)) 
    (mln::make-mln (symbol-name ref) :language :unknown)) ; jpb 1406
   ((symbolp ref)
    (mln::make-mln (symbol-name ref) :language *language*))
   ((and (stringp ref)(eql *language* :all))
    (mln::make-mln ref :language :unknown))
   ((stringp ref)
    (mln::make-mln ref :language *language*))
   (t (error "bad ref argument: ~S" ref))))

#|
(%make-mln-from-ref 'albert)
((:EN "ALBERT"))

(mln::make-mln 'albert)
((:EN "ALBERT"))

(%make-mln-from-ref "Albert ;Zoe")
((:EN "Albert" "Zoe"))

(mln::make-mln "Albert ;Zoe")
((:EN "Albert" "Zoe"))

(%make-mln-from-ref '((:en "Stevens") (:fr "Albert")))
((:EN "Stevens") (:FR "Albert"))

(mln::make-mln '((:en "Stevens") (:fr "Albert")))
((:EN "Stevens") (:FR "Albert"))

(with-language :all (%make-mln-from-ref 'Albert))
((:UNKNOWN "ALBERT"))

(with-language :all (mln::make-mln 'Albert))
Error: Illegal language: :ALL for ALBERT.
Legal ones are: (:CN :DE :EN :ES :FR :IT :JP :PT :PL :BR :ZH :UNKNOWN)

(%make-mln-from-ref '(:en "Stevens" :fr "Albert"))
((:EN "Stevens") (:FR "Albert"))

(mln::make-mln '(:en "Stevens" :fr "Albert"))
((:EN "Stevens") (:FR "Albert"))
|#
;;;------------------------------------------------------------------ %MAKE-NAME
;;; Deprecated. Replaced by %%make-name

;;;(defUn %make-name (class-id &rest option-list &key name &allow-other-keys)
;;;  "Makes a name with %make-name-string and interns is in the specified package.
;;;Arguments:
;;;   class-id: identifier of the corresponding class
;;;   name (key): string, symbol or multilingual string
;;;  more keys are allowed as specified in the lambda-list of the %make-string function
;;;Return:
;;;   interned symbol."
;;;  (declare (ignore name))
;;;  ;; intern symbol in the current package
;;;  (intern (apply #'%make-name-string class-id option-list)))
;;;
;;;#|
;;;(%make-name '$ENT :name 'person)
;;;(%%make-name 'person :class)
;;;PERSON
;;;:INTERNAL
;;;
;;;(%make-name '$EPR :name '((:en "albert")(:fr "Jules")))
;;;(%%make-name '((:en "albert")(:fr "Jules")) :prop)
;;;HAS-ALBERT
;;;:INTERNAL
;;;
;;;(%make-name '$EPR :name "albert")
;;;(%%make-name "albert" :prop)
;;;HAS-ALBERT
;;;:internal
;;;|#
;;;A---------------------------------------------------------------- %%MAKE-NAME
;;; syntax similar to %%get-id

(defUn %%make-name (ref type-key &rest option-list)
  "Makes a name with %make-name-string and interns is in the current package. ~
   No test done to check if resulting symbol is bounded.
Arguments:
   ref: string, symbol or multilingual string
   type-key: a tag specifying the type of name we want (see %make-string-name)
  more keys are allowed as specified in the lambda-list of the %make-string function
Return:
   symbol interned in the current package."
  ;; use this line to accomodate old MLN format
  (if (mln::%mln? ref)(setq ref (mln::make-mln ref)))
  ;; intern symbol in the current package
  (intern (apply #'%%make-name-string ref type-key option-list)))

#|
(%%make-name "chief executive officer" :class)
(%%make-name "chief executive officer" '$ent)
CHIEF-EXECUTIVE-OFFICER
NIL

(%%make-name "first name" :att)
(%%make-name "first name" '$EPT)
HAS-FIRST-NAME
NIL

(%%make-name "first name" :var)
(%%make-name 'first-name :var)
_FIRST-NAME
NIL

(%%make-name "first name" :conversation)
(%%make-name "first name" '$CVSE)
_FIRST-NAME-CONVERSATION
NIL

(%%make-name '=first-name :inst :class-ref "person")
(%%make-name '=first-name `$FN :method-type :instance :class-ref "person")
(%%make-name '=first-name `$FN :method-type :instance :class-ref 'person)
(%%make-name '=first-name `$FN :method-type :instance :class-ref '((:en "person")))
(%%make-name '=first-name `$FN :method-type :instance :class-ref '(:en "person"))
PERSON=I=0=FIRST-NAME
NIL

(%%make-name '(:en "James" :fr "Albert ; g�rard") :att)
HAS-JAMES
NIL                 

(with-package :test
  (with-context 4
    (%%make-name '=first-name :inst :class-ref "person")))
TEST::PERSON=I=4=FIRST-NAME

(catch :error (%%make-name '=first-name :z :class-ref "person"))
"can't make name string for type :Z"
|#
;;;A--------------------------------------------------------- %%MAKE-NAME-STRING
;;; Swiss knife for making all sorts of names to be used instead of various macros

(defUn %%make-name-string (ref type-key &key class-ref
                               state-type short-name prefix (type "") method-type
                               &allow-other-keys &aux pfx name)
  "Name factory: Builds name strings for various moss entities, e.g. method names, ~
   inverse-property names, internal method function names, etc.
   Examples of resulting strings
     XXX-Y-ZZZ from \" Xxx y'zzz \"
     IS-XXX-OF inverse property
     HAS-XXX  property
     $E-PERSON=S=0=PRINT-SELF own method internal function name
     $E-PERSON=I=0=SUMMARY instance method internal function name
     *0=PRINT-OBJECT universal method internal function name
     $-PER typeless radix
     _HAS-BROTHER  internal variable
   We mix English prefix and suffix with other languages, which is not crucial ~
   since such names are internal to MOSS.
Arguments:
   ref: an mln, string or symbol (if mln takes canonical part)
   type-key: specifies the type of wanted name (:ps :var :own :plain ...)
             can also be a class!
   name (key): string, symbol or MLN, e.g. name of method
   prefix (key): symbol, string or mln specifying a class (for class properties)
   class-ref (key): class-id for a given method
   method-type (key): :instance or :own
   state-type (key): for state objects {:entry-state, :success, :failure}
   short-name (key): for state objects, prefix
   type (key): :var :radix :doc :class :concept :inv :inverse-property :inverse
               :property :prop :att :attribute :rel :relation :own :own-method 
               :inst :instance :instance-method :uni :universal-mthod :conversation
               :q-state :plain :simple
Return:
   a single name string"
  (let (context name-length)
    
    ;; pre-processing of some options
    ;; prefix
    (if prefix
        (setq pfx
              (cond
               ((null prefix) "")
               ((symbolp prefix) (symbol-name prefix))
               ((stringp prefix) (%string-norm prefix))
               ((mln::mln? prefix) (%string-norm prefix))
               ;; for backward compatibility
               ((mln::%mln? prefix) (%string-norm prefix))
               (t (error "bad prefix ~S, should be symbol or string." prefix)))))
    
    ;; check for backward-compatible MLN format
    (if (mln::%mln? ref)(setq ref (mln::make-mln ref)))
    
    ;; backward compatibility with the previous %make-name-string function
    (cond
     ((null type-key) (setq type-key :plain))
     ;; %is-class? uses context!
     ((%is-class? type-key)
      (setq type-key (or (get-name-key type-key method-type) :plain))))
    
    ;; norm name if specified
    (if (or (and ref (symbolp ref)) (stringp ref) (mln::mln? ref)) ; jpb 1406
        (setq name (%string-norm ref #\-))
      (error "name ~S should be a string or symbol or mln" ref))
    
    ;; Here, name is a string!
    
    ;(format t "~%;--- %%make-name-string /type-key: ~S" type-key)
    ;; now do it!
    (case type-key
      
      ;;== variable
      (:var
       (concatenate 'string "_" name))
      
      ;;== radix, takes the first 3 letters of name
      (:radix
       (concatenate 'string "$" (%string-norm type) "-" (subseq name 0 3)))
      
      ;;---- end testing keywords
      ;;== specific index for documentation
      (:doc
       (concatenate 'string ">-" name))
      
      ;;== class names have no prefix nor suffix
      ((:class :concept)
       name)
      
      ;;== inverse property-name returns a list of names
      ;;********** the inverse of IS-XXX-OF should be XXX!
      ((:inv :inverse :inverse-property :inverse-link)
       ;; special case when name is "IS-XXXX-OF"
       (setq name-length (length name))
       (cond
        ((and (> name-length 6)
              (equal+ (subseq name 0 3) "IS-")
              (equal+ (subseq name (- name-length 3)) "-OF"))
         (string+ "HAS-" (subseq name 3 (- name-length 3))))
        
        ((and (> name-length 4)
              (equal+ (subseq name 0 4) "HAS-"))
         (string+ "IS-" (subseq name 4) "-OF"))
        
        ;; special string notation (used in queries): ">title"
        ((and (stringp name) (equal #\> (char name 0)))
         ;; remove leading >
         (setq name (string+ "HAS-" (subseq name 1))))
        ;(format *debug-io* "~&+++ %make-name-string; name: ~S test: ~S"
        ;        name (%is-inverse-property-ref? name))
        (t (concatenate 'string "IS-" name "-OF"))))
      
      ;;== local property name?
      ((:prop :property :att :attribute :rel :relation :tp :sp)
       ;; if property name is  a symbol starting with HAS-, don't add HAS- again
       (if (eql 0 (search "HAS-" name))
           name
         (concatenate 'string "HAS-" pfx (if prefix "-" "") name)))
      
      ;;== own-method (use context)
      ((:own :own-method)
       (setq context (symbol-value (intern "*CONTEXT*")))
       (concatenate 'string (%string-norm class-ref) "=S=" 
         (%string-norm context) name))
      
      ;;== instance-method (use context)
      ((:inst :instance :instance-method)
       (setq context (symbol-value (intern "*CONTEXT*")))
       (concatenate 'string (%string-norm class-ref) "=I=" 
         (%string-norm context) name))
      
      ;;== universal (use context)
      ((:uni :universal-method) 
       (setq context (symbol-value (intern "*CONTEXT*")))
       (concatenate 'string "*" (%string-norm context) "=" name))
      
      ;;== conversation header
      (:conversation
       (concatenate 'string "_" name "-CONVERSATION"))
      
      ;;== entry-state (state-type must be there)
      (:q-state
       (if (and (member state-type '(:entry-state :success :failure))
                short-name)
           (concatenate 'string "_" (%string-norm short-name) "-" 
             (symbol-name state-type))))
      
      ;;== if class id is nil, then cook up a simple name
      ((:simple :plain)
       (if prefix
           (concatenate 'string pfx name)
         name))
      
      ;;== otherwise error
      (otherwise
       (verbose-throw :error "can't make name string for type ~S" type-key))
      )))

#|
(catch :error (%%make-name-string " le  test " :plain))
"LE-TEST"

(catch :error (%%make-name-string " le  test " :class))
"LE-TEST"

(catch :error (%%make-name-string " le  test " '$ent))
"LE-TEST"

(catch :error (%%make-name-string " le  test " :inv))
"IS-LE-TEST-OF"

(catch :error (%%make-name-string " le  test " :relation))
"HAS-LE-TEST"

(catch :error (%%make-name-string " le  test " :rel))
"HAS-LE-TEST"

(catch :error (%%make-name-string " le  test " '$EPS))
"HAS-LE-TEST"

(catch :error (%%make-name-string " le  test " :inv))
"IS-LE-TEST-OF"

(catch :error (%%make-name-string 'is-president-of :inv))
"HAS-PRESIDENT"

(catch :error (%%make-name-string ">PRESIDENT"  :inv))
"HAS-PRESIDENT"

(catch :error (%%make-name-string '=summary :inst :class-ref '$E-PERSON))
"$E-PERSON=I=0=SUMMARY"

(catch :error (%%make-name-string '=summary '$FN :method-type :instance
                                  :class-ref '$E-PERSON))
"$E-PERSON=I=0=SUMMARY"

(catch :error (%%make-name-string " le  test " :uni))
"*0=LE-TEST"

(catch :error (%%make-name-string " le  test " '$uni))
"*0=LE-TEST"

(catch :error (%%make-name-string " le  test " :conversation))
"_LE-TEST-CONVERSATION"

(catch :error (%%make-name-string " le  test " :q-state :state-type :entry-state 
                                   :short-name "what"))
"_WHAT-ENTRY-STATE"

(catch :error (%%make-name-string " le  test " :var))
"_LE-TEST"

(catch :error (%%make-name-string " le  test " :radix)) ; ???
"$-LE-"

(catch :error (%%make-name-string " le  test " :radix :type "E"))
"$E-LE-"

(catch :error (%%make-name-string " le  test " :z))
"can't make name string for type Z"

(catch :error (%%make-name-string " le  test " '$CTR))
"LE-TEST"

(catch :error (%%make-name-string  " le  test " :prop :prefix 'PERSON))
"HAS-PERSON-LE-TEST"

(catch :error (%%make-name-string '((:fr " le  test ")(:en "the test")) :var ))
"_THE-TEST"

(catch :error (%%make-name-string '(:fr " le  test " :en "the test") :var ))
"_THE-TEST"

(catch :error (%%make-name-string '((:fr " le  test ")(:en "the test")) :att))
"HAS-THE-TEST"
|#
;;;----------------------------------------------------------- %MAKE-NAME-STRING
;;; obsolete see %%make-name-from-string

;;;A-------------------------------------------------------- %%MAKE-NEW-VERSION

(defun %%make-new-version (&rest option-list)
  "Adding a new version to the system. Takes the last version of the version-graph ~
   adds 1, and forks from current version unless there is an option
        :from <old-branching-context>
   in which case old-branching-context are checked for validity before ~
   anything is done.
Arguments:
   option-list (opt): e.g. (:from 4)(:name \"2015\")(:doc ...)
Return:
   new context
Side-effect:
   throws to :error if optional :from context is illegal"
  (declare (special *version-graph*))
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (version-graph (symbol-value (intern "*VERSION-GRAPH*")))
        (source (assoc :from option-list))
        new-version)
    
    (format t "~%;--- %%make-new-version /*package*: ~S" *package*)
    (format t "~%;--- %%make-new-version /MOSS *context*: ~S" *context*)
    (format t "~%;--- %%make-new-version /current context: ~S" context)
    
    (cond
     ;; if there are no option, we branch from current one
     ((null source))
     
     ;; if (cadr source) is illegal throws to :error
     ((null (catch :error (%%allowed-context? (cadr source))))
      (warn " in =new-version illegal branching context ~S" source)
      (return-from %%make-new-version context))
     
     ;; if there is a valid option, branch from the specified context
     (t
      (setq context (cadr source)))
     )
    
    ;; new version is 1+ the last one
    (setq new-version (1+ (caar version-graph)))
    (mformat "~&;***Warning: changing version and context from ~A to ~A in package ~S." 
             context new-version (package-name *package*))
    (set (intern "*VERSION-GRAPH*") 
         (cons (list new-version context) version-graph))
    (set (intern "*CONTEXT*") new-version)
        
    ;; return new-version
    (symbol-value (intern "*CONTEXT*")))
  )

#|
(with-package :test
  (setq test::*version-graph* '((0)))
  (setq test::*context* 0)
  (moss::%%make-new-version)
  )
;***Warning: changing version and context from 0 to 1
1
test::*version-graph*
((1 0) (0))
test::*context*
1

(with-package :test
  (moss::%%make-new-version))
;***Warning: changing version and context from 1 to 2
2

(with-package :test
  (moss::%%make-new-version '(:from 1)))
;***Warning: changing version and context from 1 to 3
3
test::*version-graph*
((3 1) (2 1) (1 0) (0))

(with-package :test
  (moss::%%make-new-version '(:from 99)))
;*** MOSS-error context 99 is illegal in package: #<The TEST package>.
Warning:  in =new-version illegal branching context (:FROM 99)
|#
;;;A-------------------------------------------------------------- %MAKE-PHRASE

(defUn %make-phrase (&rest words)
  "Takes a list of words and returns a string with the words separated by a space.
Arguments:
   words (rest): strings
Return:
   a string (does not check for single space between words)."
  (format nil "~{~A~^ ~}" words))

#|
? (%make-phrase "a" "new" " class ")
"a new  class "
|#
;;;A------------------------------------------------------ %MAKE-RADIX-FROM-NAME
;;; ***** this function could be improved to use a better algorithm to shorten
;;; radices. Currently unused... Should handle packages
;;; can use (%%make-name name :radix) does not check existing symbol
;;; Currently unused

(defUn %make-radix-from-name (name &key (type ""))
  "build a radix name for a property or for a class. Tries to build a unique name ~
   using the first 3 letters of the name. Error if the symbol already exists.
Arguments:
   name: e.g. \"SIGLE\"
   type (opt): e.g. \"T \" (for terminal property)
Return:
   e.g. $T-SIG or error if already exists"
  (let ((radix (intern (concatenate 'string "$" type "-" 
                         (subseq (symbol-name name) 0 3)))))
    (if (boundp radix)
       (error "the three first letters of the name ~S do not allow to build a unique ~
              radix: ~S" name radix)
      radix)))

#|
(%make-radix-from-name 'tartine)
$-TAR

(%%make-name 'tartine :radix)
$-TAR
:INTERNAL
|#
;;;A-------------------------------------------------------------- MAKE-REF-LINE

(defUn make-ref-line (length text)
  "used when building external files"
  (let ((filling (- length (+ 4 (length text)))))
    (concatenate 'string 
      ";;;" (make-string filling :initial-element #\-) " " text)))

#|
(make-ref-line 30 "MY-FUNCTION")
";;;--------------- MY-FUNCTION"
|#
;;;------------------------------------------------------------ %MAKE-REF-OBJECT
;;; currently used for letting an instance belong to several classes.

;;; We use the REF facility when we want an object to belong to several classes.
;;; E.g. a student can also be a teacher.
;;; Since the student has the identifier $E-STUDENT.34 it cannot have another
;;; identifier like $E-TEACHER.12. 
;;; We thus create a ref object attached to $E-TEACHER.12 that points to the
;;; first identifier. The idea is the same as the lisp "invisible pointers."
;;; The identifier should be in the same package as the object

;;; Multiple class belonging is currently in a shaky state. JPB1507
;;; Furthermore STUDENT and TEACHER are roles taken by a person and should be
;;; declared as a virtual class, not a bona fide class

(defUn %make-ref-object (class-ref obj-id)
  "Builds a reference object, checking the validity of the class. If invalid ~
   throws to :error tag.
Arguments:
   class-ref: reference of the new class
   obj-id: id of object being referenced
   context (opt): context, default current
Return:
   2 values: id of new ref object; class-id."
  (let* ((class-id (%make-id-for-class class-ref))
         next-id)
    ;; check validity of class
    (unless (%is-concept? class-id)
      (terror "while defining a ref object to ~S, ~S is not a valid class name."
              obj-id class-ref))
    ;; build id in the same package as that of class
    (setq next-id (%make-id-for-instance class-id
                                         (%get-and-increment-counter class-id)))
    ;; build object
    (set next-id `(($REF (,(symbol-value (intern "*CONTEXT*")) ,obj-id))))
    ;; save when editing
    (save-new-id next-id)

    ;; return id and class-id
    (values next-id class-id)))

#|
(catch :error 
       (with-package :test
         (moss::%make-ref-object "student" 'test::$E-PERSON.1)))
TEST::$E-STUDENT.2
(($REF (0 TEST::$E-PERSON.1)))
TEST::$E-STUDENT

(catch :error 
       (with-package :test
         (with-context 4
           (moss::%make-ref-object "student" 'test::$E-PERSON.1))))
TEST::$E-STUDENT.3
;; here person 1 is a student in context 4 and children
(($REF (4 TEST::$E-PERSON.1)))
|#
;;;--------------------------------------------------- %MAKE-STRING-FROM-PATTERN
;;; Not implemented, use pformat instead.

(defUn %make-string-from-pattern (control-string pattern &rest properties)
  "makes a string from a pattern (XML like). The first element of pattern is the ~
   class of the  object. The function ~
   is somewhat equivalent to the =summary method, but the method is applied to ~
   the structured object.
   Ex: (fn \"~{~A~^ ~}, ~{~A~^ ~}\"
           '(\"person\" (\"name\" \"Dupond\") (\"first-name\" \"Jean\"))
           \"name\" \"first-name\")
   returns \"Dupond, Jean\"
Arguments:
   pattern: the pattern
   data: the data
Return:
   string to be printed."
  (declare (ignore control-string pattern properties))
  (warn "Not implemented, use pformat instead."))

;;;A------------------------------------------------------ %MAKE-STRING-FROM-REF
;;; currently unused

(defUn %make-string-from-ref (ref &key all)
  "takes a reference and returns the corresponding string. When nil returns ~
   empty string.
Argument:
   ref: symbol, string or mln
   all (key): if true and ref is mln, return all synonyms
Return:
   string. When ref is mln a second value tells the language."
  (cond
   ((null ref) "")
   ((symbolp ref) (symbol-name ref))
   ((stringp ref) ref)
   ((mln::mln? ref)  ; jpb 1406
    (if all
        (format nil "~{~A~^, ~}" (mln::extract ref)) ; jpb 1406
      (mln::get-canonical-name ref))) ; jpb 1406
   ((mln::%mln? ref)  ; old MLN format
    (setq ref (mln::make-mln ref))
    (if all
        (format nil "~{~A~^, ~}" (mln::extract ref)) ; jpb 1406
      (mln::get-canonical-name ref)))
   (t (mthrow "ref: ~S should be symbol, string or mln" ref))))

#|
(%make-string-from-ref nil)
""
(%make-string-from-ref 'albert)
"ALBERT"

(%make-string-from-ref " voil� voila")
" voil� voila"

(%make-string-from-ref '(:fr "albert" :en "stevens"))
"stevens"
:EN

(with-language :fr (%make-string-from-ref '(:fr "albert;zoe" :en "stevens")))
"albert"
:fr

;; does not seem to be very useful...
(with-language :fr 
  (%make-string-from-ref '(:fr "albert;zoe" :en "stevens") :all t))
"albert; zoe"
|#
;;;A---------------------------------------------------- %MAKE-WORD-COMBINATIONS
;;; seems to be unused

(defUn %make-word-combinations (word-list)
  ;; when empty list or only one word return
  (unless (cdr word-list) (return-from %make-word-combinations word-list))
  ;; otherwise build word combinations
  (let ((input-length (length word-list)) ; e.g. 5
        result)
    (dotimes (nn (1+ input-length)) ; e.g. nn: 0 -> 4
      ;; build cursor to move through the list
      (dotimes (jj nn) ; e.g. jj: 0, then jj: 0->1
        (push
         (apply #'%make-phrase 
                (subseq word-list jj (+ (- input-length nn) jj 1)))
         result)))
    (reverse result)))

#|
(%make-word-combinations '("a" "b" "c" "d"))
("a b c d" "a b c" "b c d" "a b" "b c" "c d" "a" "b" "c" "d")
(%make-name-for-entry "a b c")
A-B-C
(%%make-name "a b c" :plain)
A-B-C
:INTERNAL
|#
;;;A----------------------------------------------------------- %MAKE-WORD-LIST

(defUn %make-word-list (text &key (norm t))
  "Norms a text string by separating each word making it lower case with a leading~
   uppercased letter.
Arguments:
   text: text string
   norm (key): if true (default) capitalize the first letter of each word
Return:
   a list of normed words."
  (let (pos result word)
    (unless text (return-from %make-word-list nil))
    (loop
      ;; remove trailing blanks
      (setq text (string-trim '(#\space) text))
      ;; is there any space left?
      (setq pos (position #\space text))
      (unless pos
        (push (if norm (string-capitalize text) text) result)
        (return-from %make-word-list (reverse result)))
      ;; extract first word
      (setq word (subseq text 0 pos)
          text (subseq text (1+ pos)))
      (push (if norm
                (string-capitalize word)
              word)
            result)
      )))

#|
(%make-word-list "the   DAY when I    fell INTO the PIT   ")
("The" "Day" "When" "I" "Fell" "Into" "The" "Pit")

(%make-word-list "the   DAY when I    fell INTO the PIT   " :norm nil)
("the" "DAY" "when" "I" "fell" "INTO" "the" "Pit")

(moss::%make-word-list "voil�: projets avec l'Europe?" :norm nil)
("voil�:" "projets" "avec" "l'Europe?")
|#
;;;------------------------------------------------------------- %%MERGE-OBJECTS
;;;***** currently unused

(defUn %%merge-objects (obj-id obj-list context)
  "obj-id is the id of an object already existing in memory when we are loading
   a new object with same id from disk. The new format of the loaded object is
   obj-list. We compare properties of the existing (moss) object and the loaded
   object and update all properties that are not system or that contain 
   application references. For a tp, the system object is the reference. The
   result is a merged object including system and application data.
Arguments:
   obj-id: id of the object to be processed
   obj-list: object saved by the application
Returns:
   the list representing the system object merged with application references."
  (setq obj-id (%%alive? obj-id context))
  ;; for each pair (<prop-id> . <values>) test first prop-id
  (dolist (pair obj-list)
    (cond
     ;; if prop-id is from application (not :moss package), then add pair
     ((not (eql (symbol-package (car pair)) (find-package :moss)))
      (set obj-id (cons pair (symbol-value obj-id))))
     ;; here prop-id is a system prop, then we must check values
     ;; if equal on both sides, then do nothing
     ((equal (assoc (car pair) (symbol-value obj-id)) pair))
     ;; if prop-id is a terminal property we give priority to the system
     ;; i.e., we do nothing
     ((%is-attribute? (car pair)))
     ;; if structural or inverse link, we must add application data
     (t (dolist (world (cdr pair))
          ;; (car world) is context, (cdr world) contains successors
          (dolist (ens (cdr world))
            ;; if ens is from application we add it to obj-id
            ;; if ens is from moss package we assume that it is already 
            ;; present in obj-id
            (when (not (eql (symbol-package ens) (find-package :moss)))
              (%%add-value obj-id (car pair) ens (car world))))))))
  ;; return object internal list
  (symbol-value obj-id))

#|
??
|#
;;;==============================================================================
;;;                         TRACING FUNCTIONS
;;;==============================================================================
;;; Those functions are intended for developers

;;;------------------------------------------------------------------- MOSS-TRACE

(defun moss-trace (&optional tag level)
  "adds a trace tag to the set of trace tags or gives the list of trace tags.
Argument:
   tag (opt): if there adds the key to the set of trace keys
   level (opt): level of trace (default is 0)
Return:
   list of tags"
  (declare (special *debug-tags* *debug-tag-list*))
  (cond
   (tag 
    (pushnew tag *debug-tags*)
    (setf (get tag :trace) t)
    (setf (get tag :trace-level) (or level 0))
    )
   (t
    *debug-tags*)))

#|
(moss-trace)
(:DIALOG)
|#
;;;------------------------------------------------------------------ MOSS-TRACE?

(defun moss-trace? ()
  "shows the current active trace tags"
  (declare (special *debug-tags* *debug-tag-list*))
  (let ((tag-list *debug-tag-list*))
  (mapcar #'(lambda (xx) (cons xx (or (cdr (assoc xx tag-list)) "?")))
    *debug-tags*))
  )

#|
(moss-trace?)
((:DIALOG . "?"))
|#
;;;--------------------------------------------------------------- MOSS-TRACE-ADD

(defun moss-trace-add (tag reason)
  "creates a new trace tag and add it to the debugging-tags list.
Argument:
   tag: new tag
   reason: a string explaining what will be traced
Return:
   updated list"
  (declare (special *debug-tag-list*))
  (push (cons tag reason) *debug-tag-list*))

#|
(moss-trace-add :dialog "Tracing the dialog crawler and related functions")
((:DIALOG . "Tracing the dialog crawler and related functions"))
|#
;;;----------------------------------------------------------------- MOSS-UNTRACE

(defun moss-untrace (tag)
  "removes a tag from the set of trace tags. If the arg is :all, removes everything."
  (declare (special *debug-tags*))
  (if (eql tag :all)
      (setf *debug-tags* nil)
    (let ((tags (remove tag *debug-tags*)))
      (setf *debug-tags* tags)
      (setf (get tag :trace) nil)
      tags)))

;;;----------------------- End debbugging functions -------------------------

;;;---------------------------------------------------------------- MOVE-FACT

(defUn move-fact (conversation &key from to)
  "reads the value associated with the :from tag in the fact area of the ~
   conversation object and copy it into the :to slot. Context is current.
Arguments:
   conversation: a MOSS-CONVERSATION object
   from (key): a tag, e.g. :raw-input
   to (key): a tag, usually a keyword
Return:
   associated value."
  (unless (%type? conversation '$CVSE)
    (terror "move-fact/ conversation arg not the right type: ~S" conversation))
  (let ((val (read-fact conversation from)))
    (replace-fact conversation to val)
    val))

;;;A---------------------------------------------------------------- %OCCURS-IN?

(defUn %occurs-in? (item expr)
  "returns T if item occurs somewhere in expr.
Arguments: 
   item: something to check (not a list)
   expr: any lisp expr."
  ;(print (list item expr))
  (cond
   ((equal item expr) t)
   ((null expr) nil)
   ((consp expr) (or (%occurs-in? item (car expr))(%occurs-in? item (cdr expr))))
   ((and (stringp item)(stringp expr))(string-equal item expr))
   (t (equal item expr))))

#|
(%occurs-in? 'aa '((A ( 2 3 4 AA) 4 5)))
T
|#
;;;A---------------------------------------------------------------- PACKAGE-KEY
;;; used by db-load, db-store,...

(defUn package-key (package)
  "takes a package object and returns a key corresponding to its name.
Argument:
   package: must be a package
Return:
   a key"
  (if (packagep package)
      (intern (package-name package) :keyword)
    (error "argument is ~S, should be a package." package)))

#|
(moss::package-key *package*)
:ADDRESS
:EXTERNAL

(moss::package-key :zoe)
> Error: argument, :ZOE, should be a package.
> While executing: MOSS::PACKAGE-KEY
> Type Command-. to abort.
|#
;;;A------------------------------------------------------------------- %PCLASS?

(defUn %pclass? (input class-ref)
  "tests if input belongs to the transitive closure of class-ref. Only works ~
   if the language is that of the input and class ref.
Arguments:
   input: some expression that must be a string
   class-ref: a string naming a class
Return:
   nil if not the case, something not nil otherwise."
  (member input (%get-subclass-names class-ref) :test #'string-equal))

#|
(with-package :test
  (with-language :en 
    (moss::%pclass? 
     (car '("student"
            ("name" "Sato") ("first-name" "Gilson")
            ("address"
             ("postal address" ("street" "Centre de Recherche de Royallieu" "BP 20529")
              ("town" "Compi�gne cedex") ("zip" "60205")))))
     "person")))
("STUDENT" "teacher")
|#
;;;A---------------------------------------------------------------------- %PDM?
#| Old version of %pdm?
Algorithm
we pass for example $E-PERSON.1 as the value of xx
1. Argument (xx) must be a symbol, e.g. $E-PERSON.1
2. either $E-PERSON.1 is bound or we call %ldif xx to load it
3. If $E-PERSON.1 was not bound, we check if it is now bound after %ldif - %ldif 
does not bind $E-PERSON.1 if it does not exist in the database
4. test id $E-PERSON.1 is nil, in which case we call %ldif which must return
$E-PERSON.1
5. the value of $E-PERSON.1 must be an a-list
6. the a-list must contin $TYPE or $REF (in case of multiclass belonging
                                            
(defUn %pdm? (xx)
  "Checks if an object had the right PDM format, i.e. it must be a symbol, bound, ~
   be an alist, and have a local $TYPE property. References are PDM objects. If ~
   the object is unbound or nil, tries to load it from the database if there is one.
Argument:
   xx: should be an object id
Return:
   nil if not a pdm object, something non nil otherwise."
  (and 
   (symbolp xx)
   ;; if key is unbound we try to load object from disk (%ldif returns obj-id)
   (or (boundp xx) (%ldif xx))
   ;; if still unbound quit (return nil)
   (boundp xx)
   ;; if object is nil, we try to load it from disk (%ldif returns key)
   (or (symbol-value xx) (symbol-value (%ldif xx)))
   ;; object must have an a-list strucuture
   (alistp (symbol-value xx))
   ;; object must be a bona fide object or a reference
   (or (assoc '$TYPE (symbol-value xx))(assoc '$REF (symbol-value xx))))
  )


|#
;;; %PDM is a purely structural function, not version sensitive.
;;; the fact to try to find the symbol in the database when its value is nil is
;;; obscure, therefore we issue a warning

(defUn %pdm? (xx)
  "Checks if an object has the right PDM format, i.e. it must be a symbol, bound, ~
   be an alist, and have a local $TYPE property. References are PDM objects. It ~
   does not tamper with versions, but uses %ldif if xx is nil or unbound.
Argument:
   xx: should be an object id (a symbol, e.g. $ENT or $E-PERSON.2)
Return:
   nil if not a pdm object, T otherwise.
Side-effect:
   if xx is unbound or nil, calls %ldif that will bind the id if the value is in ~
   the database and is not nil.
   if the object is found and is a PDM object, we put a mark onto its p-list for ~
   saving time if we must test it again."
  (or
   ;; if xx is a non nil symbol with a :pdm property, and it is bound to a non
   ;; nil value, returns T right away
   (and (symbolp xx) xx (get xx :pdm) (boundp xx) (symbol-value xx) T)
   ;; otherwise do some checking
   (and 
    ;; xx must be a symbol
    (symbolp xx)
    ;; xx is a symbol, if xx is nil or unbound tries to load it from disk
    ;; the call is used for its side effect
    (%%ldif-raw xx)
    ;; test after possible load, whether we have a symbol bound to a non nil value
    (boundp xx)
    ;; object must have an a-list structure
    (alistp (symbol-value xx))
    ;; object must be a bona fide object, i.e. an a-list with a $TYPE property
    (and (or (assoc '$TYPE (symbol-value xx))
             (assoc '$REF (symbol-value xx)))
         ;; if so, mark object on its p-list (returns T)
         (setf (get xx :pdm) t))
    )
   )
   ;; otherwise return NIL
   )

#|
(%pdm? nil)
NIL

(%pdm? 'zz)
NIL

(setq zzz nil)
(%pdm? 'zzz)
NIL

(%pdm? "Albert")
NIL

(%pdm? '((a 1)(b 2)))
NIL

(%pdm? '$ENT)
T

(setq $E-PERSON.12 '(($TYPE (0 $E-PERSON))($T-PERSON-NAME (0 "Albert"))))
(%pdm? '$E-PERSON.12)
T

;; Tests with a database are done when the :TEST agent is loaded.

(with-package :test
  (makunbound '$E-person.2))

(db-load '$E-person.2 :test)
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.2)) (TEST::$T-PERSON-NAME (0 "Zoe")))
T
(boundp '$E-person.2)
NIL

(catch :error
       (with-package :test (%ldif '$e-person.2)))
$E-PERSON.2
(boundp 'test::$E-person.2)
NIL

(catch :error
       (with-package :test (%ldif 'test::$e-person.2)))
test::$E-person.2
(($TYPE (0 TEST::$E-PERSON)) ($ID (0 TEST::$E-PERSON.2)) (TEST::$T-PERSON-NAME (0 "Zoe")))
|#
;;;A----------------------------------------------------------------------- %PEP
;;; context is that of obj-id to print

(defUn %pep (obj-id &key (offset 30)(stream t))
  "Prints versions of values from a given context up to the root.
    (%pep obj-id version &rest offset)
    For each property prints values in all previous context up to the root ~
   does not check for illegal context since it is a printing function."
  (declare (special *moss-output*))
  (setq obj-id (%resolve obj-id))
  (let* ((obj-l (symbol-value obj-id))
         (context (symbol-value (intern "*CONTEXT*")))
         (context-branch (%vg-gamma context))
         (*moss-output* stream))
    ;; print header
    (mformat "~&~S" obj-id)
    (mformat "~%----------~%")
    ;; print type
    (%pep-pv (assoc '$TYPE obj-l) context-branch stream offset)
    (%pep-pv (assoc '$ID obj-l) context-branch stream offset)
    (mformat "~&----- Attributes~%")
    ;; print all terminal properties
    (while obj-l
           (when
               (and (%is-attribute? (caar obj-l))	
                    (not (eq (caar obj-l) '$TYPE))
                    (not (eq (caar obj-l) '$ID))
                    )
             (%pep-pv (car obj-l) context-branch stream offset)
             )
           (pop obj-l))
    (mformat "~&----- Relations~%")
    ;; print all structural properties
    (setq obj-l (symbol-value obj-id))
    (while obj-l
           (when
               (%is-relation? (caar obj-l))	
             (%pep-pv (car obj-l) context-branch stream offset)
             )
           (pop obj-l))
    (mformat "~&-----Inv-Links~%")
    ;; and  properties
    (setq obj-l (symbol-value obj-id))
    (while obj-l
           (when
               (%is-inverse-property? (caar obj-l))	
             (%pep-pv (car obj-l) context-branch stream offset)
             )
           (pop obj-l))
    ;; the end of it
    (mformat "~&----------")
    :done
    ))
#|
(%pep '$FN)
$FN
----------
MOSS-TYPE:                    t0: $ENT
MOSS-IDENTIFIER:              t0: $FN
----- Attributes
MOSS-CONCEPT-NAME:            t0: ((:EN "MOSS-METHOD"))
MOSS-RADIX:                   t0: $FN
----- Relations
MOSS-ATTRIBUTE:               t0: $DOCT, $ARG, $REST, $CODT, $FNAM, $XNB, $TMBT, $MNAM
MOSS-COUNTER:                 t0: $FN.CTR
MOSS-OWN-METHOD:              t0: $FN.36, $FN.109
MOSS-INSTANCE-METHOD:         t0: $FN.53, $FN.70, $FN.71, $FN.75, $FN.85, $FN.95
-----Inv-Links
IS-MOSS-ENTITY-LIST-OF:       t0: $SYS.1
IS-MOSS-SUCCESSOR-OF:         t0: $OMS, $IMS, $FNLS
IS-MOSS-IS-A-OF:              t0: SPY::$FN, P1::$FN, P2::$FN, V::$FN, DN::$FN, EV::$FN, FPS::$FN,                                   FV::$FN,                                   C::$FN,                                   TEST::$FN
----------
:DONE

(with-package :test
  (moss::%pep 'test::$E-PERSON.2))
$E-PERSON.2
----------
MOSS-TYPE:                    t0: $E-PERSON
MOSS-IDENTIFIER:              t0: $E-PERSON.2
----- Attributes
NAME:                         t0: "Pile"
FIRST-NAME:                   t0: "Zoe"
----- Relations
-----Inv-Links
----------
:DONE

(with-package :test
  (%pep 'test::$E-PERSON))
$E-PERSON
----------
MOSS-TYPE:                    t0: MOSS::$ENT
MOSS-IDENTIFIER:              t0: $E-PERSON
----- Attributes
MOSS-CONCEPT-NAME:            t0: ((:EN "PERSON"))
MOSS-RADIX:                   t0: $E-PERSON
MOSS-DOCUMENTATION:           t0: ((:EN "no doc"))
----- Relations
MOSS-COUNTER:                 t0: $E-PERSON.CTR
MOSS-ATTRIBUTE:               t0: $T-PERSON-NAME, $T-PERSON-MIDDLE-NAME, $T-PERSON-EMAIL,                                   $T-PERSON-FIRST-NAME,                                   $T-PERSON-AGE
MOSS-RELATION:                t0: $S-PERSON-EMPLOYER, $S-PERSON-COUSIN, $S-PERSON-FATHER,                                   $S-PERSON-HUSBAND,                                   $S-PERSON-MOTHER,                                   $S-PERSON-BROTHER,                                   $S-PERSON-SISTER,                                   $S-PERSON-WIFE
MOSS-INSTANCE-METHOD:         t0: $E-FN.10
-----Inv-Links
IS-MOSS-ENTITY-LIST-OF:       t0: $SYS.1
IS-MOSS-SUCCESSOR-OF:         t0: $S-ORGANIZATION-PRESIDENT, $S-PERSON-COUSIN, $S-PERSON-FATHER,                                   $S-PERSON-HUSBAND,                                   $S-PERSON-MOTHER,                                   $S-PERSON-BROTHER,                                   $S-PERSON-SISTER,                                   $S-PERSON-WIFE,                                   $S-STUDENT-TEACHER
IS-MOSS-IS-A-OF:              t0: $E-STUDENT
                              t4: $E-STUDENT, $E-TEACHER
----------
:DONE
|#
;;;A-------------------------------------------------------------------- %PEP-PV

(defUn %pep-pv (value context-branch &optional (stream t) offset)
  "Used by %pep, prints ~
   a set of value associated with a property from the root to current context.
    Offset is currently set to 30."
  (declare (ignore offset)(special *moss-output*)) ; not used in this version
  ;; get name of property we are printing, direct or inverse
  (with-context (car context-branch)
    (let ((prop-name (or
                      (car (%get-value (car value) '$PNAM))
                      (car (%get-value (car value) '$INAM))))
          (*moss-output* stream))
      
      ;; control string is pretty sophisticated. Let's explain that stuff
      ;;    ~&~A~^: prints a new line, the property name and a column
      ;;    ~:{ ...~} prints a list of sublist
      ;;    ~30Tt~S~^:  prints the version number starting at column 30 e.g. t12:
      ;;    ~@{~A~^, ~}  prints a list of values separated by a comma, note that
      ;;       the ~^ construct avoids printing a trailing comma
      ;;       the ~@{ constructs uses the remaining arguments as a list
      ;;       the ~< ~> construct is used to skip 30T the first time around
      ;;       this is indicates by the ~2:; construct (2 stands for the last 
      ;;       comma and space at the end of the line
      (mformat "~&~A:~:{~30Tt~S: ~@{~<~&~34T~2:;~S~>~^, ~}~&~}"     
               (%string-norm prop-name)
               (reduce  ; JPB 140820 removing mapcan
                #'append
                (mapcar
                    #'(lambda (xx) (if (assoc xx (cdr value))
                                       (list (assoc xx (cdr value)))))
                  (reverse context-branch))
                )))))

#|
(%pep-pv '($TYPE (0 $ENT)) '(0) )

TYPE:                         t0: $ENT
NIL

(with-package :test
  (with-context 5
    (%pep-pv '($TYPE (0 PERSON)(4 TEACHER)) '(5 4 0))))

MOSS-TYPE:                    t0: MOSS::PERSON
                              t4: MOSS::TEACHER
NI
|#
;;;A-------------------------------------------------------------------- PFORMAT
;;; used in dialogs

(defUn pformat (fstring obj-id &rest prop-list)
  "takes a formatting string, an object and a list of property references, and ~
   builds an output string corresponding to a view on the object in a given ~
   context, i.e. trying to use the right language. Non values are marked by ~
   a question mark.
Arguments:
   fstring: formatting string
   obj-id: a PDM object
   prop-list (rest): a list of property references in any valid language.
Return:
   a string or NIL if there is a problem."
  (let (result prop-id value-list string-list)
    (dolist (prop-ref prop-list)
      ;; get the prop-id from the ref What happens on multiple classes?
      (setq prop-id (moss::%%get-id prop-ref :property 
                                    :class-ref (car (moss::%type-of obj-id))))
      ;; get the value associated with the property
      (setq value-list (send obj-id '=get prop-ref))
      ;; apply a formatting function to each value
      (setq string-list 
            (mapcar #'(lambda (xx) (send prop-id '=format-value xx)) value-list))
      ;; now keep result
      (push string-list result))
    (format t "~%; pformat /result: ~S" result)
    ;; return string
    (apply #'format nil fstring (reverse result))))

#|
(pformat "~5T~{~A~^~&~5T ~}~%~5T~{~A~^ ~} ~{~A~^ ~}~%~5T~{~A~^ ~}"
         '$e-home-address.1
         "street" "zip" "town" "country")
"     14, All�e de la Tilloye
     60200 Compi�gne
     (France)"

(with-package :test
  (with-context 4
    (pformat "~5T~{~A~^~&~5T ~}~%~5T~{~A~^~&~5T ~}"
             'test::$E-PERSON.2
             "name" "first name")))
"     Barth�s-Biesel
      Barth�s
     Dominique"
;; does not work for property values that are lists of strings rather than a list
;; of a single string. In that case, it is necessary to provide a =format-value
;; method. Same probably for MLN values
|#
;;;+------------------------------------------------------------------- %PFORMAT
;;; used in dialogs

(defUn %pformat (fstring input &rest prop-list)
  "produces a string for printing from a list produced typically by the ~
   =make-print-string method. Language must be right.
Arguments:
   input: a-list, e.g. ((\"name\" \"Dupond\")(\"first name\" \"Jean\")...)
   fstring: string control format
   prop-list: a list of properties appearing in the a-list. last entry can be the
                  specification of a particular package 
Return:
   a string: e.g. \"Dupond, Jean\" "
  (let ((item (car (last prop-list))))
    (if (typep item 'package)
        ;; if package is present, then
        (with-package item
          (apply #'format nil fstring 
                 (mapcar #'(lambda (xx) (moss::%get-value-from-prop-ref xx input)) 
                   (butlast prop-list))))
      ;; otherwise
      (apply #'format nil fstring 
             (mapcar #'(lambda (xx) (moss::%get-value-from-prop-ref xx input)) 
               prop-list)))))

#|
? (with-language :en
    (moss::%pformat "~{~A~^ ~} (~{~A~^ ~})"
                    '(("name" "Institut International pour l'Intelligence Artificielle")
                      ("acronym" "IIIA"))
                    "name" "acronym"))
"Institut International pour l'Intelligence Artificielle (IIIA)"
|#
;;;+------------------------------------------------------------------- POP-FACT

(defUn pop-fact (conversation tag)
  "reads the value associated with tag in the fact area of the conversation ~
   object, removing it from the facts in the current context.
Arguments:
   conversation: a MOSS-CONVERSATION object
   tag: e.g. :data
Return:
   associated value."
  (unless (%type? conversation '$CVSE)
    (terror "read-fact/ conversation arg not the right type: ~S" conversation))
  (let ((result (cdr (assoc tag (%get-value conversation '$FCT)))))
    ;; remove fact
    (replace-fact conversation tag nil)
    result))

;;;------------------------------------------------------------------- PRINT-SYS

(defun print-sys ()
  "pretty print the content of the current active system"
  (declare (special $SYS.1))
  (pprint $SYS.1))

;;;----------------------------------------------------------------------- %PUTC
;;; unused

(defUn %putc (obj-id value prop-id context)
  "Stores a versioned value onto the p-list of the object used as a cache."
  (with-context context (setq obj-id (%resolve obj-id)))
  (setf (get obj-id prop-id)
    (cons (cons context value)
          (nassremprop context (get obj-id prop-id))) ))

;;;----------------------------------------------------------------------- %PUTM
;;; unused when *cache-methods* is nil
;;; when developing it is recommended to set *cache-methods* to nil, since if
;;; a method is modified, its cached value will not be changed.

;;; Should also use *package* since different agents can have different methods
;;; with the same name in different or same contexts
;;; entry on the plist was:
;;;   ((:OWN (0 *NONE*) (2 *0=GET-PROPERTIES))(:INSTANCE (0 *NONE)))
;;; *package should be added to that
;;;  - either using =make-entry@test 
;;;  - or having a more complex list
;;;   (("MOSS" (:OWN (0 *NONE*) (2 *2=GET-PROPERTIES))(:INSTANCE (0 *NONE)))
;;;    ("TEST" (:OWN (0 *0=GET-PROPERTIES))))
;;; OWN methods should be cached onto the p-list of any object
;;; INSTANCE methods should be cached only on the p-list of the class object
;;; UNIVERSAL methods need not be cached


(defUn %putm 
    (object-id function-name method-name context &rest own-or-instance)
  "Records the function name corresponding to the method onto the ~
   p-list of the object. Own-or-instance can be either one of the ~
   keywords :own or :instance; If not present, then :general is used."
  (with-context context 
    (setq object-id (%resolve object-id)))
  ;; temp-alist get the alist associated with the function-name, e.g.

  (setf (get object-id method-name) ; get value associated with name
    (let ((temp-alist (get object-id method-name))
          (mark (if own-or-instance (car own-or-instance) :general)))
      (if temp-alist
          ;; if it exists we add or replace the corresponding value
          (let ((value-list (getv mark temp-alist)))
            (cons (cons mark (cons (list context function-name)
                                   (assremprop context value-list)))
                  (assremprop mark temp-alist)))
        ;; otherwise cook-up a special list
        (list (list mark (list context function-name)))))))


#|
;; also record the package
;; p-list: (... <method-name> 
;;              (<package name> (nn (:own <function name>)(:instance <...>)) ...)
;;          ...)
;;; In fact we need not include package in any of the application objects. This is
;;; only neede for a class defined in the MOSS package and shared by other packages

(defUn %putm (object-id function-name method-name context method-type)
  "Records the function name corresponding to the method onto the ~
   p-list of the object. Own-or-instance can be either one of the ~
   keywords :own or :instance. own-methods are cached onto the p-list of any ~
   object, instance methods are revorded onto the p-list of classes only.
Arguments:
   object-id: object that received a message
   function-name: the name of the internal function implementing the method
   method-name: the name of the method
   method-type: :own or :instance
Returns:
   the p-list of object-id eventually modified."

  (cond
   ((eql method-type :instance)
    ;; if method is an instance method deal with the class
    (unless (%is-concept? object-id)
      (setq object-id (car (%get-value object-id '$type context))))
    (warn "caching instance method ~S onto the p-list of class ~S"
      method-name object-id))
   
   ((not (eql method-type :own))
    (warn "Ignored method type (~S) when trying to cache method ~S onto p-list ~
      of ~S in package ~S in context ~S"
      method-type method-name object-id *package* context)
    (return-from %putm  nil)))
  
  (let* ((temp-alist (get object-id method-name))
         ;; (<package name> (nn (:own <fname>)...) ...)
         (package-name (package-name *package*))
         (package-alist 
          (cdr (assoc package-name temp-alist :test #'equal+)))
         ;; ((nn (:own <fname>) (:instance <fname>))...)
         (value-list (getv context package-alist))
         ;; ((:own <fname>)(:instance <fname>))
         (fname (getv method-type value-list))
         ;; <fname>
         )
    ;(format t "~%; %putm /temp-alist: ~S" temp-alist)
    ;(format t "~%; %putm /package-alist: ~S" package-alist)
    ;(format t "~%; %putm /value-list: ~S" value-list)
    ;(format t "~%; %putm /fname: ~S" fname)
    
    ;; if name already there and identical, do nothing
    (if (eql fname function-name)
        (return-from %putm temp-alist))
    
    ;; otherwise replace value
    ;(format t "~%; %putm /value-list before: ~S" value-list)
    (setq value-list 
          (cons `(,method-type ,function-name) 
                (remove method-type value-list :key #'car)))
    ;(format t "~%; %putm /value-list after: ~S" value-list)
    
    (setq package-alist
          (cons (cons context value-list) 
                (remove context package-alist :key #'car)))
    ;(format t "~%; %putm /package-alist: ~S" package-alist)
    
    (setq temp-alist
          (cons (cons package-name package-alist) 
                (remove package-name temp-alist :test #'equal+ :key #'car)))
    
    (setf (get object-id method-name) temp-alist)
    (symbol-plist object-id)))

;; in test package (object-id function-name method-name context method-type)
(with-package :test 
  (%putm 'test::$E-PERSON.1 'test::F1 'test::=f1 0 :own))
(=F1 (("TEST" (0 (:OWN F1)))) :PDM T)

;; change the name
(with-package :test 
  (%putm 'test::$E-PERSON.1 'test::new-F1 'test::=f1 0 :own))
(TEST::=F1 (("TEST" (0 (:OWN TEST::NEW-F1)))) :PDM T)

(with-package :test 
  (%putm 'test::$E-PERSON.1 'test::new-F1 'test::=f1 0 :ownn))
Warning: Bad method type (:OWNN) when trying to cache method =F1 onto p-list of
         $E-PERSON.1 in package #<The TEST package> in context 0
NIL

(symbol-plist 'test::$e-person)
  ;; old format
 (=SUMMARY ((:INSTANCE (0 TEST::$E-PERSON=I=0=SUMMARY))) :PDM T)

;; here we have a mix of two formats new and old
(with-package :test 
  (%putm 'test::$E-PERSON.1 'test::class-f1 'test::=f1 0 :instance))
Warning: caching method =F1 onto the p-list of class $E-PERSON.1
(TEST::=F1 (("TEST" (0 (:INSTANCE TEST::CLASS-F1)))) =SUMMARY
           ((:INSTANCE (0 TEST::$E-PERSON=I=0=SUMMARY))) :PDM T)

;; mixture of :own and :instance with the same name should be rare
(with-package :test 
  (%putm 'test::$E-PERSON 'test::own-class-f1 'test::=f1 0 :own))
(TEST::=F1 (("TEST" (0 (:OWN TEST::OWN-CLASS-F1) (:INSTANCE TEST::CLASS-F1)))) 
           =SUMMARY ((:INSTANCE (0 TEST::$E-PERSON=I=0=SUMMARY))) :PDM T)

;; version
(with-package :test
  (with-context 4 
    (%putm 'test::$E-PERSON.1 'test::F6 'test::=f1 6 :own)))
(TEST::=F1
 (("TEST" (0 (:OWN TEST::NEW-F1)) (6 (:OWN TEST::F6)))) :PDM T)
|#
;;;A----------------------------------------------- %RANK-ENTITIES-WRT-WORD-LIST
;;; this function can be used to rank a list of objects resulting from an entry
;;; point access to check for presence of other information in some properties. 
;;; E.g. we get persons from their name and check for first name.
;;; Used in %access-entities-from-word-list

(defUn %rank-entities-wrt-word-list (entity-list word-list)
  "takes a list of entity ids and a list of words, and returns a score for ~
   each entity that is function of the number of apparitions of the words in ~
   attributes of the entity.
Arguments:
   entity-list: a list of object ids (ei)
   word-list: a list of words (wordj)
Returns:
   a list of pairs (ei wi), e.g. (($e-person.2 0.75)($E-PERSON.3 0.5))."
  ;; if empty entity list or empty word list return nil
  (unless (and entity-list word-list)
    (return-from %rank-entities-wrt-word-list nil))
  ;; build a weight list from the list of words
  (let* ((total (length word-list))
         (weight-list (mapcar #'(lambda(xx) (list xx (/ 1 total))) word-list))
         (score 0)
         entity-score-list attribute-list class-id-list att-value
         result)
    ;; loop for each entity
    (dolist (obj-id entity-list)
      ;; reset score
      (setq score 0)
      ;; get the classes of the object
      (setq class-id-list (%get-value obj-id 'moss::$type))
      ;; get all attributes from all the classes
      (when class-id-list
        ;; reset score
        (setq entity-score-list ())
        (setq attribute-list 
              (delete-duplicates
               (reduce  ; JPB 140820 removing mapcan
                #'append
                (mapcar
                    #'%%get-all-class-attributes class-id-list))))
        ;;loop for each attribute
        (dolist (att attribute-list)
          ;; get the value attached to this attribute
          (setq att-value (%get-value obj-id att))
          ;; if empty or not a list of strings give up
          (when (and att-value (every #'stringp att-value))
            ;; compute score of the attribute data, push into entity-score-list
            (setq entity-score-list
                  (append (%get-relevant-weights weight-list att-value)
                          entity-score-list))))
        ;; compute entity score from the score from each attribute
        (dolist (item entity-score-list)
          (setq score (+ score (cadr item) (- (* score (cadr item))))))
        ;; build a pair and push it into the result list
        (push (list obj-id score) result)))
    (sort  result #'>= :key #'cadr)))

#|
(%rank-entities-wrt-word-list 
   '(sa-address::$E-PERSON.2 sa-address::$E-PERSON.3) 
   '("barthes" "jean-paul"))
((SA-ADDRESS::$E-PERSON.2 3/4) (SA-ADDRESS::$E-PERSON.3 1/2))

(with-package :test
  (with-context 2
    (%rank-entities-wrt-word-list 
     '(test::$E-PERSON.1 test::$E-PERSON.2 test::$E-PERSON.3 test::$E-PERSON.4)
     '("Barth�s" "jean-paul"))))
((TEST::$E-PERSON.1 3/4) (TEST::$E-PERSON.2 1/2) (TEST::$E-PERSON.4 1/2))
|#
;;;A--------------------------------------------------------------- READ-FACT

(defUn read-fact (conversation tag)
  "reads the value associated with tag in the fact area of the conversation ~
   object in current context.
Arguments:
   conversation: a MOSS-CONVERSATION object
   tag: e.g. :data
Return:
   associated value."
  (unless (%type? conversation '$CVSE)
    (terror "read-fact/ conversation arg not the right type: ~S" conversation))
  (cdr (assoc tag (%get-value conversation '$FCT))))

;;;?--------------------------------------------------------------- %%RELINK-ISA
;;; use in moss-persistency. Should check if the *context* variable here is the
;;; MOSS *context variable. This function is very specific and should be in the
;;; persistency file

(defUn %%relink-isa (obj1-id obj2-id)
  "raw link used when reconnecting disk application to MOSS."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        value-list)
    ;; first first object
    (setq value-list (%get-value obj1-id '$IS-A))
    (unless (member obj2-id value-list)
      (setq value-list (append value-list (list obj2-id)))
      ;; update
      (nalist-replace-pv
       obj1-id '$IS-A 
       (cons (cons context value-list)
             (assremprop context
                         (getv '$IS-A (symbol-value obj1-id))))))
    ;; then inverse prop
    (setq value-list (%get-value obj2-id '$IS-A.OF))
    (unless (member obj1-id value-list)
      (setq value-list (append value-list (list obj1-id)))
      ;; update
      (nalist-replace-pv 
       obj2-id '$IS-A.OF 
       (cons (cons context value-list)
             (assremprop context 
                         (getv '$IS-A.OF (symbol-value obj2-id))))))
    :done))

;;;+----------------------------------------------- %REMOVE-REDUNDANT-PROPERTIES

(defUn %remove-redundant-properties (prop-list &optional context)
  "Removes properties that share the same generic property and keep the leftmost ~
   one.
Arguments:
   prop-list: list of property ids
   context (opt): context (default current)
Return:
   a cleaned list of properties."
  ;(format t "~%;---------- %remove-redundant-properties/ context: ~S" context)
  (let (generic-name-list result prop-name)
    ;; when context is not specified set it to current context
    (unless context (setq context (symbol-value (intern "*CONTEXT*"))))
    ;(format t "~%;--- *package*: ~S" *package*)
    ;(format t "~%;--- test::*context*: ~S" test::*context*)
    ;(format t "~%;--- (sv (intern...)) ~S" (symbol-value (intern "*CONTEXT*")))
    ;(format t "~%;--- context: ~S" context)
    
    (dolist (prop-id prop-list)
      ;; make sure that all properties are loaded
      (%ldif prop-id)
      ;(format t "~%;--- prop-id: ~S" prop-id)
      ;; if the generic name of the property is in the list drop it
      (setq prop-name 
            (%make-name-for-property (car (%%get-value prop-id '$PNAM context))))
      ;(format t "~%;--- prop-name: ~S" prop-name)
      (unless (member prop-name generic-name-list)
        (push prop-name generic-name-list)
        (push prop-id result))
      ;(format t "~%;--- generic-name-list: ~S" generic-name-list)
      ;(format t "~%;--- result: ~S" result)
      )
    ;(format t "~%;---------- end %remove-redundant-properties")
    (reverse result)))

#|
? (%remove-redundant-properties '($T-BUTCHER-TRAINEE-NAME $T-BUTCHER-NAME $T-PERSON-NAME $T-PERSON-AGE
                                  $T-PERSON-FIRST-NAME $S-PERSON-BROTHER))
($T-BUTCHER-TRAINEE-NAME $T-PERSON-AGE $T-PERSON-FIRST-NAME $S-PERSON-BROTHER)
|#
;;;A------------------------------------------------------------------ %%REMPROP

(defUn %%remprop (obj-id prop-id context)
  "Removes the values associated with ~
   the specified property in the specified context. The net result is ~
   that the object inherits the values from a higher level.
    So the resulting value in the context will probably not be nil. Hence ~
   it is not equivalent to a set-value with a nil argument."
  (with-context context
    (setq obj-id (%resolve obj-id)))
  ;; when editing
  (save-old-value obj-id)
  (nalist-replace-pv obj-id prop-id 
                     (assremprop context (lob-get obj-id prop-id)))
  ;; return the update object value (useful in step)
  (symbol-value obj-id)
  )

;;;A------------------------------------------------------------------- %%REMVAL

(defUn %%remval (obj-id val prop-id context)
  "Removes the value associated with ~
   a property in a given context. Normally useful for terminal properties ~
   However does not check for entry-points. Also val must be a normalized value n 
   i.e. must be expressed using the internal format (it cannot be an external value)."
  (with-context context
    (setq obj-id (%resolve obj-id))
    (let ((value-list (%get-value obj-id prop-id)))
      (if (member val value-list :test #'equal)
          (%%set-value-list obj-id
                            (delete val value-list :test #'equal :count 1)
                            prop-id
                            context)))))

;;;A---------------------------------------------------------------- %%REMNTHVAL

(defUn %%remnthval (obj-id val prop-id context position &key count)
  "Removes the nth value associated ~
   with a property in a given context. Normally useful for terminal properties ~
   However does not check for entry-points. Does not return any significant value.
   Assumes that context is valid.
Arguments:
   obj-id: id of object under consideration
   prop-id: attribute or value id
   val: value to remove from list
   context: context (should be valid)
   position:: where to start deleting in the list of values
   count (key): number of occurences of value to delete when multiple
Return:
   internal list of modified object or \"*failed*\" if val was not there."
  (with-context context
    (setq obj-id (%resolve obj-id))
    (let ((value-list (%get-value obj-id prop-id context)))
      (if (member val value-list :test #'equal+) ; JPB1401 equal+
          (%%set-value-list 
           obj-id
           (remove val value-list :test #'equal :count count :start position)
           prop-id
           context)
        "*failed*"))))

;;;A----------------------------------------------------------- REMOVE-TRACE-TAG

(defun remove-trace-tag (tag)
  (declare (special *debug-tags*))
  (setq *debug-tags* (remove tag *debug-tags*)))

;;;+--------------------------------------------------------------- REPLACE-FACT
;;; to test

(defUn replace-fact (conversation tag value)
  "replaces the value associated with tag in the fact area of the conversation ~
   object in the current context.
Arguments:
   conversation: a MOSS-CONVERSATION object
   tag: e.g. :data
   value: any lisp expression
Return:
   the content of facts."
  (declare (special $FCT $CVSE))
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    (unless (%type? conversation '$CVSE)
      (terror "replace-fact / conversation arg not the right type: ~S" conversation))
    (%%set-value-list conversation 
                      (cons (cons tag value)
                            (remove tag (%get-value conversation '$FCT) :key #'car)) 
                      '$FCT
                      context)))

#|
(replace-fact 'moss::$CVSE.1 :data '("test data"))
(read-fact 'moss::$cvse.1 :data)
|#
;;;?--------------------------------------------------------------------- %RESET
;;; we should also remove accessor functions in case they will not be redefined
;;; e.g. (HAS-SURNAME)
;;; Currently untested and unused

(defUn %reset (&key (context (symbol-value (intern "*CONTEXT*")) there?))
  "Removes all classes and instances of an application, making the corresponding ~
   symbols unbound. Protects all data defined in the kernel (moss package).
Arguments:
   none
Return:
   t"
  (declare (ignore there?))
  (flet ((%but-moss (obj-list) (%filter-against-package obj-list :moss)))
    (let ((concept-list (%but-moss (%get-all-concepts)))
          (orphan-list (%but-moss (%get-all-orphans)))
          (attribute-list (%but-moss (%get-all-attributes)))
          (relation-list (%but-moss (%get-all-relations)))
          (inverse-relation-list (%but-moss (%get-all-inverse-relations)))
          (method-list (%but-moss (%get-all-methods)))
          (entry-point-list (%but-moss (%get-all-entry-points)))
          (other-object-list (%but-moss (%get-all-symbols)))
          instance-list all-instances)
      (flet ((zap-object (object) (setf (symbol-plist object) nil)
                         (makunbound object)))
        ;; for each concept, get instances and delete them
        ;; this removes the corresponding entry points, which is important if the
        ;; entry-point has been defined in the :moss package
        (dolist (concept concept-list)
          ;; collect instances
          (setq instance-list (%get-all-instances concept))
          (setq all-instances (append instance-list all-instances))
          (broadcast instance-list '=delete-object))
        (vformat "instances: ~&~S" all-instances)
        ;; then remove all orphans
        (vformat "orphans: ~&~S" orphan-list)
        (broadcast orphan-list '=delete-object)
        ;; then remove all application classes
        (vformat "concepts: ~&~S" concept-list)
        (broadcast concept-list '=delete-object)
        ;; miscellaneous objects (includes counters)
        ;(broadcast other-object-list '=delete-object)
        ;; then all attributes
        (vformat "attributes: ~&~S" attribute-list)
        (broadcast attribute-list '=delete-object)
        ;; then all relations
        (vformat "relations: ~&~S" relation-list)
        (broadcast relation-list '=delete-object)
        ;; then inverse relations (an application object is linked to a system
        ;; object) This should not be the case since the system does not know of the
        ;; applications...
        (vformat "inverse-relations: ~&~S" inverse-relation-list)
        (broadcast inverse-relation-list '=delete-object)
        ;; send methods
        (vformat "methods: ~&~S" method-list)
        (broadcast method-list '=delete-object)
        ;; must remove methods from MOSS system
        (vformat "methods: ~&~S" (%but-moss (%get-all-instances '$FN)))
        (dolist (val method-list)
          (%%remval (intern "*MOSS-SYSTEM*") val '$FNLS context))
        ;; must remove application entry-points from system list
        (vformat "entry-points: ~&~S" entry-point-list)
        (dolist (val entry-point-list)
          (%%remval (intern "*MOSS-SYSTEM*") val '$EPLS context))
        ;; then we must make everybody unbound
        (mapc #'zap-object 
          (append all-instances
                  orphan-list
                  concept-list
                  attribute-list
                  relation-list
                  inverse-relation-list
                  other-object-list
                  method-list
                  entry-point-list))
        :done))))

;;;+------------------------------------------------------------------- %RESOLVE
;;; We replace resolve with a call to %pdm? which calls %ldif anyway
;;; In all cases, %resolve will return its argument
;;; amounts to a no-op since we removed the multi-class belonging
;;; Could be useful if multiclass belonging is reinstalled for instances

(defUn %resolve (id)
  "Returns id, and if id is a PDM obj id, will load the object from disc if ~
   necessary.
Arguments:
   id: object id
Return:
   id
Side-effect:
   id will be initialized to object loaded from database if needed."
  (declare (inline %pdm?))
  ;; %pdm? loads id from disk and binds it if it was an unbound symbol or a 
  ;; symbol with null value
  (%pdm? id) 
  ;; always return id
  id)

;;; this function is used when we have an object belonging to several classes. 
;;; One of the manifestations of the object belongs to a certain class, all the
;;; other manifestations simply refer to the main one by using the property $REF
;;; instead of $TYPE. 
;;; Now when we have versions, there is a possibility of entering an infinite loop
;;; thus, we must be very careful not calling function that could call us in turn

;;;(defUn %resolve (id)
;;;  "Replaces id with the actual id when it is a ref object. Otherwise returns id.
;;;   When id is unbound returns id. Eventually loads the object.
;;;Arguments:
;;;   id: object id or REF identifier
;;;Return:
;;;   resolved id."
;;;  ;; pdm test loads from disk and does not bind id if unbound and not saved
;;;  (or (and (%pdm? id) 
;;;           ;; %ldif returns the id
;;;           (%ldif 
;;;            ;;*** assumes that reference was made in the current context!
;;;            (cadr (assoc *context* (cdr (assoc '$REF (symbol-value id)))))))
;;;      id))

#|
(setq $E-PERSON.12 '(($TYPE (0 $E-PERSON))($T-PERSON-NAME (0 "Albert"))))
(($TYPE (0 $E-PERSON)) ($T-PERSON-NAME (0 "Albert")))

(setq $E-STUDENT.34 '(($REF (0 $E-PERSON.12))))
(($REF (0 $E-PERSON.12)))

(%resolve '$E-PERSON.12)
$E-PERSON.12

(%resolve '$E-STUDENT.34)
$E-PERSON.12
|#
;;;----------------------------------------------------------------- SAVE-NEW-ID
;;; we cannot use tags when working with several editors...
;;; one of the problems is to make *EDITING-BOX* point to the current active 
;;; editor

(defUn save-new-id (obj-id)
  "checks if we are editing. If so and if the obj-id has not already been saved, ~
   we save it and mark its p-list"
  (unless *boot-mode*
    ;;=== first we want to see if there is an active editing box
    (let ((widget (cg:get-focus))
          win editing-box)
      (when widget 
        ;(format t "~%; save-new-id /window with focus:~%  ~S" widget)
        ;; get parent window
        (setq win (cg:parent widget))
        (when win
          ;; check that win is an editor
          (when (typep win 'EDITOR)
            ;; get editing box
            (setq editing-box (editing-box win))
            ;; if null error
            (unless editing-box
              (error "EDITING BOX unavailable for current editor"))
            ;; OK ready to save new object id
            ;; if the structure exists, check if active
            (when (active editing-box)
              (format t "~%;save-new-id /saving: ~S, win:~%  ~S" obj-id win)
              (pushnew obj-id (new-object-ids editing-box))
              (return-from save-new-id obj-id)
              ))))
      
      ;; when something failed (either there was no focus window, or the window was
      ;; not an editor), we try to get editing box directly
      (setq editing-box (intern "*EDITING-BOX*" *package*))
      ;; editing box contains a reference to a global variable in the executing
      ;; package. If bound, we could be editing 
      ;(format t "~%;save-new-id /editing-box: ~S" editing-box)
      (when (boundp editing-box) ; if editing box does not exist, not editing
        ;; make it point to the instance of the editing box
        (setq editing-box (symbol-value editing-box))
        ;(format t "~%;save-new-id /editing-box-2: ~S" editing-box)
        (when (typep editing-box 'EDITING-BOX)
          ;; if the structure exists, check if active
          (when (active editing-box)
            (format t "~%;save-new-id /saving: ~S" obj-id)
            (pushnew obj-id (new-object-ids editing-box))
            ;; return something non nil
            (return-from save-new-id obj-id))))
      )
    ;; if everything failed, we return nil
    nil))

;;;-------------------------------------------------------------- SAVE-OLD-VALUE

(defUn save-old-value (obj-id)
  "checks if we are editing. If so, checks if the object is a new one. If not,~
   put the pair (id . value) onto the old-oject-values list, if not there."
  (unless *boot-mode* 
    ;;=== first we want to see if there is an active editing box
    (let ((widget (cg:get-focus))
          win editing-box)
      (when widget
        ;; get parent window
        (setq win (cg:parent widget))
        (when win
          ;; check that win is an editor
          (when (typep win 'EDITOR)
            ;; get editing box
            (setq editing-box (editing-box win))
            ;; if null error
            (unless editing-box
              (error "EDITING BOX unavailable for current editor"))
            ;; OK ready to save new object id
            
            ;; if the structure exists, check if active
            (unless (or (assoc obj-id (old-object-values editing-box))
                        (member obj-id (new-object-ids editing-box)))
              ;(format t "~%; save-old-value /saving: ~S, win:~%  ~S" obj-id win)
              (push (cons obj-id (copy-tree (symbol-value obj-id)))
                    (old-object-values editing-box))
              ;; return something non nil
              (return-from save-old-value obj-id)
              ))))
      
      ;; when something failed (either there was no focus window, or the window was
      ;; not an editor), we try to get editing box directly
      ;; case where we are editing from a program without using editor windows
      (setq editing-box (intern "*EDITING-BOX*" *package*))
      ;; editing box contains a reference to a global variable in the executing
      ;; package. If bound, we could be editing    
      (when (boundp editing-box) ; if editing box does not exist, not editing
        ;; make it point to the instance of the editing box
        (setq editing-box (symbol-value editing-box))
        (when (typep editing-box 'editing-box)
          ;; if the structure exists, check if active
          (when (active editing-box)
            ;(format t "~%;save-old-value /obj-id: ~S ~%; old values: ~%  ~S"
            ;  obj-id (old-object-values editing-box))
            
            (unless (or (assoc obj-id (old-object-values editing-box))
                        ;; don't save if new object
                        (member obj-id (new-object-ids editing-box)))
              ;(format t "~%; save-old-value /saving: ~S" obj-id)
              (push (cons obj-id (copy-tree (symbol-value obj-id)))
                    (old-object-values editing-box))
              ;; mark object as saved
              ;(setf (get obj-id :saved) t)
              ;; return something non nil
              (return-from save-old-value obj-id)))))
      )
    ;; otherwise, failure
    nil))

;;;A------------------------------------------------------ %SELECT-BEST-ENTITIES
;;; used by agents accessing objects from a list of words

(defUn %select-best-entities (entity-score-list)
  "select entities with highest score and return the list.
Arguments:
   entity-score-list: a list like (($E-PERSON.2 0.5)(..)...)
Returns:
   the list of entities with the highest score."
  (let ((ref-score (cadar entity-score-list))
        (result (caar entity-score-list)))
    ;; if score is not a number then error
    (unless (numberp ref-score)
      (error "ref-score: ~S should be a number" ref-score))
    (dolist (pair entity-score-list)
      (if (> (cadr pair) ref-score)
          (setq ref-score (cadr pair)
              result (car pair))))
    (list result)))

#|
(%select-best-entities '(($E-PERSON.3 3/4) ($E-PERSON.2 1/2) ($E-PERSON.4 1/2)))
($E-PERSON.3)

(%select-best-entities '(($E-PERSON.3 1/4) ($E-PERSON.2 1/2) ($E-PERSON.4 1/2)))
($E-PERSON.2)
|#
;;;A--------------------------------------------------------------- SET-LANGUAGE
;;; insensitive to versions
;;; WATCH: the *language* variable that is set by the function is not necessarily
;;; the one of the global variable in the calling environment!
;;; it is better when not sure to call the function as follows:
;;;   (setq *language* (set-language xxx :only-return t))

(defun set-language (language &key only-return allow-all)
  "takes a string like \":FR\" or \"fr\" and sets the *language* variable, killing ~
   the previous value. If the arg is invalid, keeps the current language: *language*
Argument:
   language: some expression denoting a language, e.g. :FR, \"FR\" or \":FR\"
   only-return (key): if t, does not set *language* but return the language value
   allow-all (key): if t allows :ALL as a legal language
Return:
   the language if the input corresponds to a legal language, otherwise does nothing
   and returns the value of *language*, i.e. the current language."
  (declare (special *language-tags*))
  (let (tag)
    (when (stringp language)
      ;; remove any trailing spaces
      (setq language (string-trim '(#\space) language))
      (cond
       ((equal+ language "")
        (setq tag nil))
       (t
        ;; check if first char is ":"
        (setq tag
              (if (char-equal #\: (char language 0))
                  ;; if so read string directly
                  (read-from-string language)
                ;; otherwise build keyword
                (intern (moss::%string-norm language) :keyword)))
        ;; check that it is a legal MOSS language
        (unless (or (member tag moss::*language-tags*)
                    (and allow-all (eql tag :ALL)))
          (warn "warning: language tag ~S is not a valid MOSS language tag." language)
          (setq tag nil)))))
    ;; when input is a keyword set tag
    (when (keywordp language)
      (setq tag language)
      (unless (or (member tag moss::*language-tags*)
                  (and allow-all (eql tag :ALL)))
        (warn "warning: language tag ~S is not a valid MOSS language tag." language)
        (setq tag nil)))
    
    ;(format t "~%; set-language /*package*: ~S" *package*)
    ;(format t "~%; set language /*language*: ~S" *language*)
    ;(format t "~%; set language /(package *language*): ~S"
    ;  (symbol-package '*language*))
    ;(format t "~%; set language /set *language*: ~S" (set (intern "*LANGUAGE*") tag))
    ;(format t "~%; set language /(package (intern *language*)): ~S"
    ;  (symbol-package (intern "*LANGUAGE*")))
    
    ;; we do not set *language is only-return is true
    (if only-return
        tag
      ;; if language is not a valid keyword or string, then we keep current language
      (if tag (set (intern "*LANGUAGE*") tag)))))

#|
(setq *language* :EN)

(set-language "fr")
:FR

*language*
:FR

(set-language "ZW")
Warning: error: language tag "ZW" is not a valid MOSS language tag.
:FR

(set-language nil)
NIL
|#
;;;------------------------------------------------------- %%SET-SYMBOL-PACKAGE?
;;; only used by %determine-property-id! 
;;; ***** not very useful, should be discarded *****

(defUn %%set-symbol-package (symbol package)
  "if package is nil set it to default *package*, and create symbol in the ~
   specified package. No check on input.
Arguments:
   symbol: any symbol
   package: a valid package or keyword
Return:
   a symbol interned in the package."
  (intern (symbol-name symbol) package))

#|
(%%set-symbol-package 'albert :address)
ADDRESS::ALBERT
NIL

(%%set-symbol-package 'albert *package*)
ALBERT
:INTERNAL

(%%set-symbol-package nil *package*)
NIL
:INHERITED

(%%set-symbol-package :zoe *package*)
MOSS::ZOE
NIL
|#
;;;A---------------------------------------------------------------- %%SET-VALUE

(defUn %%set-value (obj-id value prop-id context)
  "Resets the value associated ~
   with a property in a given context. Replaces previous values. Does not ~
   try to inherit using the version-graph, but defines a new context locally ~
   Thus, it is not an %add-value, but an actual %%set-value."
  (with-context context
    (setq obj-id (%resolve obj-id)))
  ;; when editing
  (save-old-value obj-id)
  
  (nalist-replace-pv obj-id prop-id 
                     (cons (cons context (if value (ncons value)))
                           (assremprop context (lob-get obj-id prop-id))))
  ;; return the update object value (useful in step)
  (symbol-value obj-id)
  )

;;;A----------------------------------------------------------- %%SET-VALUE-LIST

(defUn %%set-value-list (obj-id value-list prop-id context)
  "Resets the value associated ~
   with a property in a given context. Replaces previous values. Does not ~
   try to inherit using the version-graph, but defines a new context locally ~
   Thus, it is not an %add-value, but an actual %%set-value - ~
   No entry point is created."
  (with-context context (setq obj-id (%resolve obj-id)))
  ;; when editing
  (save-old-value obj-id)
  (nalist-replace-pv obj-id prop-id 
                     (cons (cons context value-list)
                           (assremprop context (lob-get obj-id prop-id))))
  ;; return the update object value (useful in step)
  (symbol-value obj-id)
  )

;;;A------------------------------------------------------------------ %SP-GAMMA
;;; works in current package

(defUn %sp-gamma (node arc-label)
  "Returns the transitive closure of objects for a given version.
    This is the famous gamma function found in graph theory.
    No check that arc-label is a structural property.
Arguments:
   node: starting node
   arc-label: label to follow on the arcs (e.g. $IS-A)
   context (opt): context, default value is *context* (current)"
  ;; %pdm? loads the object from the database if needed
  (unless  (%pdm? node)
    (error "can't compute transitive closure of ~S under ~S
~S is not a PDM object in package ~S context ~S"
      node arc-label node *package* (symbol-value (intern "*CONTEXT*"))))
  (reverse (%sp-gamma1 (list node) arc-label nil nil)))

#|
(with-package :test
  (%sp-gamma 'test::$E-student '$is-a))
(TEST::$E-STUDENT TEST::$E-PERSON)

(with-package :test
  (%sp-gamma 'test::$E-person '$is-a.of))
(TEST::$E-PERSON TEST::$E-STUDENT)

Tests for versioning: see $IS-A?
(with-package :test
  (with-context 0
    (moss::%make-concept "AA")))
test::$E-AA
(($TYPE (0 $ENT)) ($ID (0 TEST::$E-AA)) ($ENAM (0 ((:EN "AA")))) 
 ($RDX (0 TEST::$E-AA))
 ($ENLS.OF (0 TEST::$SYS.1)) ($CTRS (0 TEST::$E-AA.CTR)))

(with-package :test
  (with-context 2
    (moss::%make-concept "AAB" '(:is-a "AA"))))
test::$E-AAB
(($TYPE (2 $ENT)) ($ID (2 TEST::$E-AAB)) ($ENAM (2 ((:EN "AAB")))) 
 ($RDX (2 TEST::$E-AAB))
 ($ENLS.OF (2 TEST::$SYS.1)) ($CTRS (2 TEST::$E-AAB.CTR)) ($IS-A (2 TEST::$E-AA)))

(with-package :test
  (with-context 6
    (moss::%make-concept "AAD" '(:is-a "AA"))))
TEST::$E-AAD
(($TYPE (6 $ENT)) ($ID (6 TEST::$E-AAD)) ($ENAM (6 ((:EN "AAD")))) ($RDX (6 TEST::$E-AAD))
 ($ENLS.OF (6 TEST::$SYS.1)) ($CTRS (6 TEST::$E-AAD.CTR)) ($IS-A (6 TEST::$E-AA)))

(with-package :test
  (with-context 6
    (moss::%sp-gamma 'test::$E-AAD 'moss::$is-a)))
(TEST::$E-AAD TEST::$E-AA)
test::$e-aa
(($TYPE (0 $ENT)) ($ID (0 TEST::$E-AA)) ($ENAM (0 ((:EN "AA")))) ($RDX (0 TEST::$E-AA))
 ($ENLS.OF (0 TEST::$SYS.1)) ($CTRS (0 TEST::$E-AA.CTR))
 ($IS-A.OF (6 TEST::$E-AAD) (2 TEST::$E-AAB TEST::$E-AAC)))
test::$e-aad
(($TYPE (6 $ENT)) ($ID (6 TEST::$E-AAD)) ($ENAM (6 ((:EN "AAD")))) ($RDX (6 TEST::$E-AAD))
 ($ENLS.OF (6 TEST::$SYS.1)) ($CTRS (6 TEST::$E-AAD.CTR)) ($IS-A (6 TEST::$E-AA)))
|#
;;;A---------------------------------------------------------------- %SP-GAMMA-L

(defUn %sp-gamma-l (node-list arc-label)
  "Computes a transitive closure
Arguments:
   node-list: a list of nodes from which to navigate
   arc-label: the label of the arc we want to use
   context (opt): context default current
Return:
   a list of nodes including those of the node-list."
  (reverse (%sp-gamma1 node-list arc-label nil nil)))

;;;A---------------------------------------------------------------- %SP-GAMMA-1

(defUn %sp-gamma1 (ll prop stack old-set) 
  "Called by %sp-gamma for computing a transitive closure.
    gamma1 does the job.
    computes the whole stuff depth first using a stack of yet unprocessed ~
   candidates.
	ll contains the list of nodes being processed
	stack the ones that are waiting
	old-set contains partial results.
Arguments:
   ll: candidate list of nodes to be examined
   prop: property along which we make the closure
   stack: stack of waiting candidates
   old-set: list containing the result
Return:
   list of nodes making the transitive closure."
  (cond 
   ;; when no more candidates and the stack is empty we are done
   ((and (null ll)(null stack)) old-set)
   ;; take the first node from the stack
   ((null ll)
    (%sp-gamma1 (ncons (car stack)) prop (cdr stack) old-set))
   ;; ll is not empty, check if already examined. If so, then discard it
   ((memq (car ll) old-set)
    (%sp-gamma1 (cdr ll) prop stack old-set))
   ;+90/3 Kludge - if (car ll) is not in core then try to get it from disk
   ;; possibility of looping here since we use this function in %ldif for merging
   ;; entry points
   ((and (or (not (boundp (car ll)))(null (car ll)))
         (%ldif (car ll)) ; JPB 1001
         nil)) ; make sure the clause fails
   ;-
   ;; If we go to meta model then we know we must stop
   ((eq (car (%get-value (car ll) '$ENAM)) '$ENT) nil)
   ;; otherwise get ancestors from the first element of ll, and
   ;; put the rest of ll together with the stack. Put the the element
   ;; into the partial results
   (t (%sp-gamma1 (%get-value (car ll) prop)
                  prop
                  (append (cdr ll) stack)
                  (cons (car ll) old-set)))))

;;;A-------------------------------------------------------------- START-EDITING
;;; insensitive to versions

(defUn start-editing (&key owner (package *package*))
  "start an editing session. Creates an instance of editing-box and asociates ~
   it with the package::*editing-box* variable, supposedly that of the editor.
Argument:
   owner (key): the structure that owns the box (usually an agent)
   package (key): specified package (default current)
Return:
   the box"
  ;; create a global variable in the specified package
  (set (intern "*EDITING-BOX*" package)
       (make-instance 'editing-box :owner owner)))

;;;------------------------------------------------------------------- STR-EQUAL
;;; Deprecated. Use equal+ instead

;;;A--------------------------------------------------------------- %STRING-NORM

(defUn %string-norm (input &optional (interchar #\-))
  "Same as make-value-string but with more explicit name.
   If input is a multilingual string, uses the canonical name.
   If input is NIL throws to :error
   IF MLN, takes the canonical value (old format allowed)
Arguments:
   input: may be a string, a symbol or a multilingual string
   interchar: character to hyphenate the final string (defaul is #\-)
Return:
   a normed string, e.g. \"AU-JOUR-D'AUJOURD'HUI\"."
  (cond
   ;; NIL -> error
   ((null input) (throw :error "%string-norm got an illegal NIL input."))
   ((symbolp input) (symbol-name input))
   ((mln::mln? input)
    (make-value-string (mln::get-canonical-name input) interchar))
   ;; backward compatible with old MLN format
   ((mln::%mln? input) 
    (make-value-string 
     (mln::get-canonical-name (mln::make-mln input))
     interchar))
   ;; anything else, cook up a string
   (t (make-value-string input interchar))))
      
#|
(%string-norm "  au jour d'aujourd'hui")
"AU-JOUR-D'AUJOURD'HUI"

(%string-norm 'albert)
"ALBERT"

(%string-norm 34)
"34"

(catch :error (%string-norm nil))
"%string-norm got an illegal NIL input."

(%string-norm '(:EN "John ; george" :FR "Albert ; Jer?me ; Albert "))
"JOHN"

(%string-norm '((:EN "John" "george") (:FR "Albert" "Jer�me" "Albert ")))
"JOHN"

(moss::%string-norm 'HAS-PR�FECTURE) ; UTF8
"HAS-PR�FECTURE"
|#
;;;+-------------------------------------------------------- %%STRIP-ENTRY-POINT
;;; used when saving ontology and KB for the first time

(defUn %%strip-entry-point (entry package)
  "we remove from an entry point all references that do not belong to specified ~
   package, or to objects that must not be saved (:no-save on p-list).
Arguments:
   entry: entry point
   package: package agaisnt which to test
Return:
   the value of the stripped entry point"
  (let (result result-list)
    (dolist (prop-values (symbol-value entry))
      ;; look at each pair, e.g. ($TYPE (0 $E-PERSON))
      ;; check property
      (if (%is-inverse-property? (car prop-values))
          (progn
            (setq result nil)
            ;; for each value, e.g. (0 $PERSON.3 $PERSON.34)
            (dolist (value (cdr prop-values))
              ;; remove values that are not in the package
              (push
               (cons (car value)
                     (remove-if 
                      #'(lambda (xx) (and (symbolp xx)
                                          (or (not (eql (symbol-package xx)
                                                        package))
                                              (get xx :no-save))))
                      (cdr value)))
               result))
            ;; result contains the value for all contexts
            ;; result-list contains the reconstructed object-list
            (push (cons (car prop-values) (reverse result)) result-list)
            )
        ;; here prop is not an inverse property
        (push prop-values result-list)))
    ;; here result-list contains the reconstructed object list
    (reverse result-list)))

#|
COUNTER
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 COUNTER))
 (MOSS::$ENAM.OF
  (0 MOSS::$CTR SPY::$E-COUNTER ALBERT::$E-COUNTER ADDRESS::$E-COUNTER $E-COUNTER))
 (MOSS::$EPLS.OF
  (0 MOSS::$SYS.1 SPY::$E-MOSS-SYSTEM.1 ALBERT::$E-MOSS-SYSTEM.1
   ADDRESS::$E-MOSS-SYSTEM.1 $E-MOSS-SYSTEM.1)))

(moss::%%strip-entry-point 'COUNTER  (find-package :address-proxy))
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 COUNTER)) (MOSS::$ENAM.OF (0 $E-COUNTER))
 (MOSS::$EPLS.OF (0 $E-MOSS-SYSTEM.1)))

omas-skill   ; $E-OMAS-SKILL is not to be saved
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 OMAS-SKILL))
 (MOSS::$ENAM.OF (0 $E-OMAS-SKILL)) (MOSS::$EPLS.OF (0 $E-MOSS-SYSTEM.1)))

(moss::%%strip-entry-point 'omas-skill  (find-package :address-proxy))
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 OMAS-SKILL)) (MOSS::$ENAM.OF (0))
 (MOSS::$EPLS.OF (0 $E-MOSS-SYSTEM.1)))
|#
;;;+------------------------------------------------------- %%STRIP-ENTRY-POINTS
;;; used when saving ontology and KB for the first time
;;; strips for properties that are inverse properties in the current context
;;; ***** Maybe should strip for all inverse properties, i.e. testing the ".OF"  
;;; part of the property id... *****
;;; unused?

(defUn %%strip-entry-points (ep-list package)
  "we remove from each entry point all references that do not belong to specified ~
   package, or to objects that must not be saved (:no-save on p-list). Works ~
   on the raw structure of the objects. 
Arguments:
   ep-list: entry point list
   package: package agaisnt which to test
Return:
   a list of cleaned entry points ((entry-point value)*)"
  (let (result result-list object-list)
    (dolist (entry ep-list)
      (cond
       ((%is-entry? entry)
        (setq result-list nil)
        (dolist (prop-values (symbol-value entry))
          ;; look at each pair, e.g. ($TYPE (0 $E-PERSON))
          ;; check property in current context
          (if (%is-inverse-property? (car prop-values))
              (progn
                (setq result nil)
                ;; for each value, e.g. (0 $PERSON.3 $PERSON.34)
                (dolist (value (cdr prop-values))
                  ;; remove values that are not in the package
                  (push
                   (cons (car value)
                         (remove-if 
                          #'(lambda (xx) 
                              (and (symbolp xx)
                                   (or (not (eql (symbol-package xx)
                                                 package))
                                       (get xx :no-save))))
                          (cdr value)))
                   result))
                ;; result contains the value for ALL contexts
                ;; result-list contains the reconstructed object-list
                (push (cons (car prop-values) (reverse result)) result-list)
                )
            ;; here prop is not an inverse property
            (push prop-values result-list)))
        ;; here result-list contains the reconstructed object list
        (push (cons entry (reverse result-list)) object-list) 
        )
       (t
        (warn " symbol ~S not recognized as an entry in package ~S" entry package)
        (push (cons entry (symbol-value entry)) object-list)
        )))
    (reverse object-list)))

#|
COUNTER
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 COUNTER))
 (MOSS::$ENAM.OF
  (0 MOSS::$CTR SPY::$E-COUNTER ALBERT::$E-COUNTER ADDRESS::$E-COUNTER $E-COUNTER))
 (MOSS::$EPLS.OF
  (0 MOSS::$SYS.1 SPY::$E-MOSS-SYSTEM.1 ALBERT::$E-MOSS-SYSTEM.1
   ADDRESS::$E-MOSS-SYSTEM.1 $E-MOSS-SYSTEM.1)))

(moss::%%strip-entry-points '(counter) (find-package :address-proxy))
((COUNTER (MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 COUNTER))
          (MOSS::$ENAM.OF (0 $E-COUNTER)) (MOSS::$EPLS.OF (0 $E-MOSS-SYSTEM.1))))

OMAS-SKILL
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 OMAS-SKILL))
 (MOSS::$ENAM.OF (0 $E-OMAS-SKILL)) (MOSS::$EPLS.OF (0 $E-MOSS-SYSTEM.1)))

(moss::%%strip-entry-points '(omas-skill) (find-package :address-proxy))
((OMAS-SKILL (MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 OMAS-SKILL))
             (MOSS::$ENAM.OF (0)) (MOSS::$EPLS.OF (0 $E-MOSS-SYSTEM.1))))

(moss::%%strip-entry-points '(counter omas-skill) (find-package :address-proxy))
((COUNTER (MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 COUNTER))
          (MOSS::$ENAM.OF (0 $E-COUNTER)) (MOSS::$EPLS.OF (0 $E-MOSS-SYSTEM.1)))
 (OMAS-SKILL (MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 OMAS-SKILL))
             (MOSS::$ENAM.OF (0)) (MOSS::$EPLS.OF (0 $E-MOSS-SYSTEM.1))))
|#
;;;A----------------------------------------------------------------- SYMBOL-KEY
;;; somewhat equivalent to omas::keywordize
;;; currently seems unused.

(defUn symbol-key (symbol)
  "takes a symbol and returns a key. If nil, error. If keyword, return a new ~
   keyword starting with :K_
Argument:
   symbol: non nil symbol
Return:
   a keyword."
  (cond
   ((null symbol) (error "argument is ~S, should be a non nil symbol" symbol))
   ((keywordp symbol) 
    (intern (concatenate 'string "K_" (symbol-name symbol)) :keyword))
   ((symbolp symbol)
    (intern (symbol-name symbol) :keyword))
   (t (error "argument is ~S, should be a non nil symbol" symbol))))

#|
(moss::symbol-key 'albert)
:ALBERT
:EXTERNAL

(moss::symbol-key :albert)
:K_ALBERT
NIL

(moss::symbol-key nil)
> Error: argument is NIL, should be a non nil symbol
> While executing: MOSS::SYMBOL-KEY

(moss::symbol-key '(z))
> Error: argument is (Z), should be a non nil symbol
> While executing: MOSS::SYMBOL-KEY
|#
;;;=============================================================================
;;;                                SYNONYMS
;;;=============================================================================
;;; The synonym functions are no longer needed with the new MLN package offering
;;; an a-list format for MLNs. They are kept for compatibility with older 
;;; versions of OMAS
;;;
;;; The synonym string type (MLN) is introduced to take care of international 
;;; ontologies.
;;; An entry has the following format:
;;; <synonym-string> ::= "string {; string}*"
;;; ex:
;;;    "ville ; cit� : Municipalit�"
;;;
;;; The following functions are intended to manipulate such strings
;;;=============================================================================

;;;A--------------------------------------------------------------- %SYNONYM-ADD

(defUn %synonym-add (syn-string value)
  "Adds a value to a string at the end. Used by old MLN format.
   Sends a warning if language tag does not exist and does not add value.
Arguments:
   syn-string: a synonym string or nil
   value: value to be included (coerced to string)
Return:
   modified string when no error, original string otherwise."
  (declare (special *current-language*))
  (cond
   ((null syn-string) (format nil "~A" value))
   ((string-equal (string-trim '(#\space) value) "")
    (terror "bad value: ~S to add to a synonym string: ~S" value syn-string))
   ((stringp syn-string)
    (concatenate 'string syn-string "; " (format nil "~A" value)))
   (t (terror "bad synonym string: ~S" syn-string))))

#|
(%synonym-add nil "Albert")
"Albert"

(%synonym-add "George" "Albert")
"George; Albert"

(%synonym-add "George" "Albert ; Robert")
"George; Albert ; Robert"

(catch :error (%synonym-add "George" ""))
"bad value: \"\" to add to a synonym string: \"George\""

(catch :error (%synonym-add 23 "Albert"))
"bad synonym string: 23"
|#
;;;A----------------------------------------------------------- %SYNONYM-EXPLODE
;;; Fixed for new MLN format

(defUn %synonym-explode (text)
  "Takes a string, consider it as a synonym string and extract items separated ~
   by a semi-column. Returns the list of string items.
Arguments:
   text: a string
Return
   a list of strings."
  ;; in the new MLN format names of properties are lists
  (if (listp text)(return-from %synonym-explode text))
  
  ;; old format
  (let (pos result word)
    (unless text (return-from %synonym-explode nil))
    (loop
      ;; remove trailing blanks
      (setq text (string-trim '(#\space) text))
      ;; is there any space left?
      (setq pos (position #\; text))
      (unless pos
        (push text result)
        (return-from %synonym-explode (reverse result)))
      ;; extract first word
      (setq word (subseq text 0 pos)
          text (subseq text (1+ pos)))
      (push word result)
      )))

#|
(%synonym-explode "la croix saint ouen")
("la croix saint ouen")

(%synonym-explode "la croix saint ouen ; compi�gne ; l'arbal�te")
("la croix saint ouen " "compi�gne " "l'arbal�te")
|#
;;;A-------------------------------------------------------------- %SYNONYM-MAKE

(defUn %synonym-make (&rest item-list)
  "Builds a synonym string with a list of items. Each item is coerced to a string.
   Only works for old MLN format (kept for backward compatibility).
Arguments:
   item-list: a list of items
Return:
   a synonym string for old format, a list of strings for new format."
  (when item-list
    (format nil "~{~A~^ ; ~}" item-list)))

#|
(%synonym-make "Albert" "George" "Robert")
"Albert ; George ; Robert"

(%synonym-make "Albert" 'age 35)
"Albert ; AGE ; 35"

(%synonym-make )
NIL

(%synonym-make nil)
"NIL"
|#
;;;A------------------------------------------------------------ %SYNONYM-MEMBER

(defUn %synonym-member (value text)
  "Checks if a string is part of the synonym list. Uses the %string-norm ~
   function to normalize the strings before comparing.
   Only works for old MLN format (kept for backward compatibility).
Arguments:
   value: a value coerced to a string
   text: a synonym string
Return:
   a list of unnormed remaining synonyms if succcess, NIL if nothing left."
  (when (and (stringp value)(stringp text))
    (member (%string-norm value)
            (%synonym-explode text) 
            :test #'(lambda (xx yy)
                      (string-equal
                       (%string-norm xx)
                       (%string-norm yy))))))

#|
(%synonym-member " albeRT  " " Joseph;Pierre;Albert;g�rard  cinq")
("Albert" "g�rard  cinq")

(%synonym-member " albeRT  " " Joseph;Pierre;Aaalbert;g�rard  cinq")
NIL

(%synonym-member 'albeRT  " Joseph;Pierre;Aaalbert;g�rard  cinq")
NIL
|#
;;;A----------------------------------------------------- %SYNONYM-MERGE-STRINGS

(defUn %synonym-merge-strings (&rest names)
  "Merges several synonym strings, removing duplicates (using a norm-string ~
   comparison) and preserving the order.
   Only works for old MLN format (kept for backward compatibility).
Arguments:
   name: a possible complex string
   more-names (rest): more of that
Return:
   a single complex string
Error:
   when some of the aguments are not strings."
  (unless (every #'stringp names)
    (error "all arguments to this function should be strings: ~&~S" names))
  (let ((name-list (%synonym-explode (car names))))
    ;; merge each string in turn exploding the strings
    (dolist (next-name (cdr names))
      (setq name-list 
            (append name-list 
                    (set-difference (%synonym-explode next-name) name-list    
                                    :test #'string-equal :key #'%string-norm))))
    ;; return the complex name-list
    (format nil "~{~A~^; ~}" name-list)))

#|
(%synonym-merge-strings "a ; b " "A; c CC ccc; d" " b; d; E " "f")
"a; b; d; c CC ccc; E; f"

(%synonym-merge-strings nil)
> Error: all arguments to this function should be strings: 
>         name: NIL, more-names: NIL
> While executing: %MERGE-NAME-STRINGS
> Type Command-. to abort.
See the Restarts� menu item for further choices.
1 > 
Aborted

(%synonym-merge-strings "a ; b " "A; c CC ccc; d" " b; d; E " 'f)
> Error: all arguments to this function should be strings: 
>         name: "a ; b ", more-names: ("A; c CC ccc; d" " b; d; E " F)
> While executing: %MERGE-NAME-STRINGS
> Type Command-. to abort.
See the Restarts� menu item for further choices.
1 > 
Aborted

(%synonym-merge-strings "a ; b ")
"a; b"

(%synonym-merge-strings "banane plantin; banane" "banane; banane plantin")
"banane plantin; banane"

(%synonym-merge-strings "banane plantin, banane" "banane; banane plantin")
"banane plantin, banane; banane plantin; banane"
|#
;;;A------------------------------------------------------------ %SYNONYM-REMOVE

(defUn %synonym-remove (value text)
  "Removes a synonym from the list of synonyms. If nothing is left return ~
   empty string.
   Only works for old MLN format (kept for backward compatibility).
Arguments:
   value: a value coerced to a string
   text: a synonym string
Return:
   the string with the value removed NIL if nothing left."
  (let ((result (remove (format nil "~A" value)
                        (%synonym-explode text) 
                        :test #'(lambda (xx yy)
                                  (string-equal
                                   (%string-norm xx)
                                   (%string-norm yy))))))
    (if result 
        (string-trim '(#\space) 
                     (apply #'%synonym-make 
                            result)))))

#|
(%synonym-remove "L'Arbal�te  " "la croix saint ouen ; compi�gne ; l'arbal�te")
"la croix saint ouen  ; compi�gne"

(%synonym-remove " LA  Croix SAINt  Ouen  "   
                   "la croix saint ouen ; compi�gne ; l'arbal�te")
"compi�gne  ; l'arbal�te"

(%synonym-remove " Paris"   "la croix saint ouen ; compi�gne ; l'arbal�te")
"la croix saint ouen  ; compi�gne  ; l'arbal�te"

(%synonym-remove 'compiegne   "la croix saint ouen ; compi�gne ; l'arbal�te")
"la croix saint ouen  ; l'arbal�te"

(%synonym-remove " Paris"   "Paris")
NIL
|#
;;;========================== End synonyms =====================================

;;;A------------------------------------------------------------------ %SUBTYPE?
;;; *any* must be the class defined in the active package, not the MOSS package!

(defUn %subtype? (object-id model-id)
  "Checks if object-id is a sub-type of model-id including itself in current ~
   context.
Arguments:
   object-id: identifier of object to check for modelhood
   model-id: reference-model
   context (opt): context default current
Return:
   T or nil."
  (declare (special *any* $EIL $IS-A))
  ;; should not test for live objects, because of bootstrap conditions
  (unless (symbolp object-id) (return-from %subtype? nil))
  (setq object-id (%resolve object-id))
  (or (equal object-id model-id)
      (and (%pdm? object-id)
           (%pdm? model-id)
           ;(or (eql model-id '*any*)  ; JPB0406
           (or (eql model-id (intern "*ANY*"))  ; JPB1702
               (and (member object-id 
                            (%sp-gamma model-id 
                                       (%make-id '$EIL :id '$IS-A) ; brute force
                                       )) T)))))

#|
(%subtype? '$ENT '$ENT)
T

(with-package :test
  (moss::%subtype? 'test::$E-STUDENT 'test::$E-PERSON))
T

Versions
(with-package :test
  (with-context 4
    (defconcept "teacher" 
        (:is-a "person")
      (:rel "organization" (:to "organization")))))

(with-package :test
  (with-context 4
    (%subtype? 'test::$E-TEACHER 'test::$E-PERSON)))
T

(with-package :test
  (with-context 2
    (%subtype? 'test::$E-TEACHER 'test::$E-PERSON)))
NIL
|#
;;;A--------------------------------------------------------------------- %TYPE?
;;; an object: object-id is an instance of class class-id if its type is *any*
;;; or if its type has class-id in the transitive closure of its $IS-A property
;;; Versioning: it may be that the object was created in some version, then the
;;; $IS-A property added in some other version

(defUn %type? (object-id class-id &optional version)
  "Checks if the class of object-id is class-id or one of its subclasses.
   If class is *any* then the result is true.
   An object may change type in subsequent versions, although this should be rare.
Arguments:
   object-id: the id of a PDM object
   class-id: the id of a class
   version (opt): context (default current)
Returns 
   nil or T."
  
  ;(break "%type? 1")
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (unless (symbolp object-id) (return-from %type? nil))
    ;; quick and dirty test for early bootstrap when $ENT is still undefined
    
    (or
     ;; eventually loads load object-id from disk (%pdm?)
     ;; test if class-id is explicitly the value of the $TYPE property in this 
     ;; context (quick test)
     (and (%pdm? object-id) 
          (member class-id 
                  (%%get-value object-id '$TYPE context))
          T)
     
     (and 
      (%pdm? object-id) ; takes care of the case when objec-id in unbound
      (%pdm? class-id)
      (or (eql class-id (intern "*ANY*"))  ; every object belongs to the universal class
          ;; check if value associated with type is part of the list of classes
          (and (intersection (%sp-gamma class-id 
                                        '$IS-A.OF ; JPB1506
                                        ;; replacing with '$IS-A.OF to avoid
                                        ;; looping if *context* is not 0
                                        ;(%make-id '$EIL :id '$IS-A) ; brute force
                                        )
                             ;; gets the value, considering context
                             (%get-value  object-id '$TYPE context))
               T))))))

#|
(with-package :test
  (moss::%type? 'TEST::$E-PERSON.1 'test::$E-PERSON))
T

(with-package :test
  (with-context 5
    (moss::%type? 'TEST::$E-PERSON.1 'test::$E-PERSON)))
T

(with-package :test
  (with-context 0
    (moss::%type? 'TEST::$E-PERSON.3 'test::$E-PERSON)))
NIL

(with-package :test
  (with-context 3
    (moss::%type? 'TEST::$ORPHAN.1 'test::*none*)))
T
|#
;;;A------------------------------------------------------------------- %TYPE-OF
;;; should return 2 types in case of $REFed objects

(defUn %type-of (obj-id &optional version)
  "Extracts the type from an object (name of its class(es)).
   Careful: returns \"CONS\" if following a programming error ~
   obj-id pass the value of the identifier...
Argument:
   obj-id: identifier of a particular object
   version (opt): context (default current)
Return:
   a list containing the identifiers of the class(es) of which the object is an instance ~
   or its lisp type if it is not a PDM object."
  ;; when obj-id is not a symbol returns the lisp type
  (unless (symbolp obj-id) (return-from %type-of (list (type-of obj-id))))
  
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (setq obj-id (%resolve obj-id))
    
    (if (%pdm? obj-id) (%%get-value obj-id '$type context) 
      (list (type-of obj-id)))))

#|
(moss::%type-of 0.1)
(SINGLE-FLOAT)

(moss::%type-of '((MOSS::$TYPE (0 ADDRESS::$E-HOME-ADDRESS))
                    (MOSS::$ID (0 ADDRESS::$E-HOME-ADDRESS.1)) 
                    (ADDRESS::$T-POSTAL-ADDRESS-TOWN (0 "Compi�gne"))
                    ))
CONS

(with-package :test
    (%type-of '$E-PERSON.2 0))
(SYMBOL)

(with-package :test
  (%type-of nil 0))
(NULL)

;; here type cannotbe resolved
(with-package :test
  (%type-of 'test::$E-PERSON.3 0))
NIL

(with-package :test
    (%type-of 'test::$E-PERSON.3 4))
(TEST::$E-PERSON)

(with-package :test
  (with-context 6
    (moss::%type-of 'test::$E-PERSON.1)))
(TEST::$E-PERSON)

(with-package :test
  (with-context 3
    (moss::%type-of 'test::$ORPHAN.1)))
(TEST::*NONE*)
|#
;;A--------------------------------------------------------------------- %UNLINK

(defUn %unlink (obj1-id sp-id obj2-id &optional version)
  "Disconnects to objects by removing the structural link.
      in the given context. If the context does not exist then one has to ~
   recover the states of the links in the required context. Then if the ~
   link did not exist, then one exits doing nothing. Otherwise, one removes ~
   the link and stores the result.
Arguments:
   obj1-id: object 1
   obj2-id: object 2
   sp-id: relation
   version (opt): context (default current
Return:
   symbol-value of the first object"
  ;; load objects from disk if needed
  (%ldif obj1-id)
  (%ldif sp-id)
  (%ldif obj2-id)
  ;; when editing, saving is done by %%set function
  ;; checks are made by %get-value
  (let* ((context (or version (symbol-value (intern "*CONTEXT*"))))
         (inv-id (%inverse-property-id sp-id))
         (suc-list (%get-value obj1-id sp-id context))
         (pred-list (%get-value obj2-id inv-id context))
         )
    (when (member obj2-id suc-list)
      (%%set-value-list obj1-id (remove obj2-id suc-list) sp-id context))
    (when (member obj1-id pred-list)
      (%%set-value-list 
       obj2-id (remove obj1-id pred-list) inv-id context))
    (symbol-value obj1-id)))

;;;=============================================================================
;;;                          VALIDATION FUNCTIONS
;;;=============================================================================

;;;---------------------------------------------------------------- %VALIDATE-SP
;;; PDM representation:
;;;   :type       ($SUC (0 $E-PERSON))
;;;   :not-type   ($NSUC (0 $E-BUTCHER))
;;;   :value      ($SUCV (0 _jpb))
;;;   :one-of     ($ONESUCOF (0 _jpb _mgbl)) ($SEL :exists))
;;;   :filter, :same, :different    ($OPR (0 :xxx 3 5 ...))
;;;   :min, :max, :unique  ($MIN 1) ($MAX 1)
;;; to check... 
;;; can be used to check values to add or the whole list of values of the relation

(defUn %validate-sp (sp-id suc-list &key restrictions no-warning
                           context (obj-id :unknown) no-repair)
  "Checks if the property successors obey the restrictions attached to the relation.
   max and min should be the last properties of the restriction.
Arguments:
   sp-id: id of the relation
   suc-list: list of successors to check
   restrictions: list of restrictions to be checked
   context (key): context (default current)
   obj-id (key): object owner of the relation (default :unknown)
   no-warning (key): if t, does not print warnings (default nil)
   no-repair (key): if t, return unchanged suc-list
Return:
   2 values:
       1st value value-list if OK, nil otherwise
       2nd value: list of string error message."
  (declare (special *relation-restrictions*))
  (let* ((context (or context (symbol-value (intern "*CONTEXT*"))))
         (restrictions (or restrictions *relation-restrictions*))
         (repaired-values suc-list)
         one-of-list val msg-list message-list header)
    
    (setq header
          (format nil "Warning: when validating values for relation ~S of object ~
              ~S in package ~S and context ~S:"
            sp-id obj-id (package-name *package*) context))
    
    ;; check each restriction
    (dolist (restriction restrictions)
      ;(format t "~%; %validate-tp /MLN restriction: ~S" restriction)
      
      (cond
       ;;=== single obligatory value: use :one-of/forall
       ;((and (eql restriction '$SUCV)
       ;      (setq val (%%get-value sp-id restriction context)))
       ; (multiple-value-setq (repaired-values msg-list)
       ;   (%validate-sp-sucv obj-id sp-id val repaired-values context)))
       
       ;;=== type of successor is specified already checked
       ((and (eql restriction '$SUC)
             (setq val (car (%%get-value sp-id restriction context))))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-sp-suc obj-id sp-id val repaired-values context)))
       
       ;;=== successor should not be of that type (can be used to eliminate some
       ;; sub-classes
       ((and (eql restriction '$NSUC)
             (setq val (car (%%get-value sp-id restriction context))))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-sp-nsuc obj-id sp-id val repaired-values context)))
       
       ;;=== ONE-OF restriction
       ((and (eql restriction '$ONEOF)
             (setq one-of-list (%%get-value sp-id restriction context)))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-sp-one-of obj-id sp-id one-of-list repaired-values context))
        )
       
       ;;=== special operators: no special operators for relations
       ;((and (eql restriction '$OPR)
       ;      (setq val (%%get-value sp-id restriction context)))
       ; (multiple-value-setq (repaired-values msg-list)
       ;   (%validate-sp-opr obj-id sp-id val repaired-values context)))
       
       ;;=== min
       ((and (eql restriction '$MINT)
             (setq val (car (%%get-value sp-id restriction context))))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-sp-min obj-id sp-id val repaired-values context)))
       
       ;;=== max
       ((and (eql restriction '$MAXT)
             (setq val (car (%%get-value sp-id restriction context))))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-sp-max obj-id sp-id val repaired-values context)))          
       ) ; end cond
      
      (when msg-list
        (setq message-list (append message-list msg-list))
        ;; reset msg-list
        (setq msg-list nil))
      
      ) ; end dolist
    
    (when (and (not no-warning) message-list)
      (mformat "~{~%~A~}" (cons header (reverse message-list))))
    
    (values
     (if no-repair suc-list repaired-values)
     (reverse message-list))))

#|
voir z-moss-tests-validate.lisp
|#
;;;--------------------------------------------------------- %validate-sp TESTS
#|
(defconcept "test")
(defindividual "test" (:var _ta))
(defconcept "target")
(defindividual "target" (:var _tg1))
(defindividual "target" (:var _tg2))
(defindividual "target" (:var _tg3))
(defconcept "subtarget" (:is-a :target))
(defindividual "subtarget" (:var _ts1))
(defconcept "junk")
(defindividual "junk" (:var _tj1))
|#
;;;----------------------------------------------------------- %VALIDATE-SP-MAX

(defun %validate-sp-max (obj-id sp-id val value-list context &aux msg)
  "check if too many values"
  (declare (ignore obj-id sp-id context))
  (when (> (length value-list) val)
    (setq msg
          (format nil "too many values (~{~S~^ ~}), max is ~S.~%We keep them anyway."
            value-list val)))
  (values value-list (if msg (list msg))))

#|
(defrelation "ra-max" (:from "test")(:to "target")(:max 1))

(%validate-sp-max _ta _has-ra-max 1 `(,_tg1 ,_tg2) 0)
($E-TARGET.1 $E-TARGET.2)
("too many values ($E-TARGET.1 $E-TARGET.2) for relation $S-RA-MAX of object 
  $E-TEST.2 in package \"MOSS\" and context 0, max is 1.
We add them anyway.")
|#
;;;----------------------------------------------------------- %VALIDATE-SP-MIN

(defun %validate-sp-min (obj-id sp-id val value-list context &aux msg-list)
  "check if not enough values"
  (declare (ignore obj-id sp-id context))
  (when (< (length value-list) val)
    (push
     (format nil "not enough values (~{~S~^ ~}), min is ~S."
       value-list val)
    msg-list))
  (values value-list msg-list))

#|
(defrelation "ra-min" (:from "test")(:to "target")(:min 1))

(%validate-sp-min _ta _has-ra-min 1 `(,_tg1) 0)
($E-TARGET.1)
NIL
|#
;;;---------------------------------------------------------- %VALIDATE-SP-NSUC

(defun %validate-sp-nsuc (obj-id sp-id val value-list context)
  "check if none of the values in value-list are of type val.
Return:
   1. a list of values excluding values that have not type val
   2. a list of warning messages."
  (declare (ignore obj-id sp-id))
  (let (msg-list result)
    (setq result
          (loop for item in value-list 
              if (not (%subtype? (car (%type-of item context)) val)) collect item
              else do 
                (push
                 (format nil "Successor ~S should not be of type ~S.~
                    ~%We ignore that value." item val)
                 msg-list)))
    (values result (reverse msg-list))))

#|
(defrelation "ra-suc" (:from "test")(:to "target"))

(%validate-sp-suc _ta '_has-ra-suc _target `(,_tg1 ,_tg2) 0)
($E-TARGET.1 $E-TARGET.2)
NIL

(%validate-sp-suc _ta '_has-ra-suc _target `(,_tj1 ,_tg2) 0)
($E-TARGET.2)
("Successor $E-JUNK.1 for relation _HAS-RA-SUC of object $E-TEST.2 is not of type $E-TARGET in package \"MOSS\" and context 0.
We ignore that value.")

(%validate-sp-suc _ta '_has-ra-suc _target `(,_ts1 ,_tg2) 0)
($E-SUBTARGET.1 $E-TARGET.2)
NIL
|#
;;;-------------------------------------------------------- %VALIDATE-SP-ONE-OF

(defun %validate-sp-one-of (obj-id sp-id one-of-list suc-list context)
  "check the one-of option, depending on the value of $SEL.
Return:
   1. the original list or NIL if $SEL is :forall or :not and test fails
   2. a list of warnings"
  (declare (ignore obj-id))
  (let (message-list)
    
    ;(format t "~%; %validate-sp-one-of /<sel>: ~S" 
    ;  (car (%%get-value sp-id '$SEL context)))
    ;; result depends on the $SEL value
    (case (or (car (%%get-value sp-id '$SEL context)) :exists)
      (:exists ; one of the values should be in the one-of list
       (unless 
           (some #'(lambda (xx) (member xx one-of-list :test #'equal+))
                 suc-list)
         (push 
          (format nil "Some successor in (~{~S~^ ~}) should be in (~{~S~^ ~})."
            suc-list one-of-list)
          message-list)))
      (:forall
       ;(break "%validate-sp-one-of one-of-list ~S suc-list ~S" one-of-list suc-list)
       (cond
        ;; if all new successors are members of the one-of list OK, do nothing
        ((every #'(lambda (xx) (member xx one-of-list)) suc-list))
        ;; otherwise, prepare message
        (t 
         (push 
          (format nil "Successors of (~{~S~^ ~}) not in (~{~S~^ ~}) are skipped" 
            suc-list one-of-list)
          message-list)
         ;; and remove offenders
         (setq suc-list 
               (remove-if-not #'(lambda (xx) (member xx one-of-list)) suc-list)))
        ))
      
      (:not-in
       (cond 
        ;; if all successors are not from the one-of list do nothing
        ((every #'(lambda (xx) (not (member xx one-of-list))) suc-list))
        ;; otherwise prepare message and remove offenders
        (t
         (push 
          (format nil "Successors in (~{~S~^ ~}) that are in (~{~S~^ ~}) are skipped" 
            suc-list one-of-list)
          message-list)
         (setq suc-list 
               (remove-if #'(lambda (xx) (member xx one-of-list)) suc-list)))
        ))
       )
    (values suc-list (reverse message-list))))

#|
(defrelation "ra-one-of" (:from "test")(:to "target")(:one-of _tg1 _tg3))

(%validate-sp-one-of _ta _has-test-ra-one-of `(,_tg1 ,_tg3) `(,_tg2) 0)
($E-TARGET.2)
("Some successor in ($E-TARGET.2) of relation $S-TEST-RA-ONE-OF of object $E-TEST.4 
  in package \"MOSS\" with context 0 should be in ($E-TARGET.1 $E-TARGET.3).")

(defrelation "ra-one-of-forall" 
    (:from "test")(:to "target")(:one-of _tg1 _tg3)(:forall))
(($TYPE (0 $EPS)) ($ID (0 $S-TEST-RA-ONE-OF-FORALL))
 ($PNAM (0 ((:EN "ra-one-of-forall")))) ($ESLS.OF (0 $SYS.1))
 ($IS-A (0 $S-RA-ONE-OF-FORALL)) ($INV (0 $S-TEST-RA-ONE-OF-FORALL.OF))
 ($PS.OF (0 $E-TEST)) ($SUC (0 $E-TARGET)) ($ONEOF (0 _TG1 _TG3)) 
 ($SEL (0 :FORALL)))

(%validate-sp-one-of _ta _has-test-ra-one-of-forall `(,_tg1 ,_tg3) `(,_tg1 ,_tg2) 0)
($E-TARGET.1)
("Successors of ($E-TARGET.1 $E-TARGET.2) for relation $S-TEST-RA-ONE-OF-FORALL of
 object $E-TEST.4 in package \"MOSS\" with context 0 not in ($E-TARGET.1 
 $E-TARGET.3) are skipped")

(defrelation "ra-one-of-not" (:from "test")(:to "target")(:one-of _tg1 _tg3)(:not-in))
;; l'option :not-in ne fait pas partie des options de relations
(%validate-sp-one-of _ta _has-test-ra-one-of-not `(,_tg1 ,_tg3) `(,_tg1 ,_tg2) 0)
($E-TARGET.2)
("Successors in ($E-TARGET.1 $E-TARGET.2) of relation $S-TEST-RA-ONE-OF-NOT of
  object $E-TEST.4 in package \"MOSS\" with context 0 in ($E-TARGET.1 $E-TARGET.3)
  are skipped")
|#
;;;----------------------------------------------------------- %VALIDATE-SP-OPR

(defun %validate-sp-opr (obj-id sp-id val value-list context)
  "reserved for future development. :between, :outside, :same or :different ~
   options do not apply."
  (declare (ignore obj-id sp-id val context))
  (values value-list nil))

;;;----------------------------------------------------------- %VALIDATE-SP-SUC

(defun %validate-sp-suc (obj-id sp-id val value-list context)
  "check if all values in value-list are of type val
Return:
   1. a list of values excluding values that have not type val
   2. a list of warning messages."
  (declare (ignore obj-id sp-id))
  (let (msg-list result)
    (setq result
          (loop for item in value-list 
              if (%subtype? (car (%type-of item context)) val) collect item
              else do
                (push
                 (format nil "Successor ~S is not of type ~S.~
                   ~%We ignore that value." item val)
                 msg-list)))
    (values result (reverse msg-list))))

#|
(defrelation "ra-suc" (:from "test")(:to "target"))

(%validate-sp-suc _ta '_has-ra-suc _target `(,_tg1 ,_tg2) 0)
($E-TARGET.1 $E-TARGET.2)
NIL

(%validate-sp-suc _ta '_has-ra-suc _target `(,_tj1 ,_tg2) 0)
($E-TARGET.2)
("Successor $E-JUNK.1 for relation _HAS-RA-SUC of object $E-TEST.2 is not of type $E-TARGET in package \"MOSS\" and context 0.
We ignore that value.")

(%validate-sp-suc _ta '_has-ra-suc _target `(,_ts1 ,_tg2) 0)
($E-SUBTARGET.1 $E-TARGET.2)
NIL
|#
;;;---------------------------------------------------------- %VALIDATE-SP-SUCV
;;; single imposed successor. Use :one-of/forall no-duplicates.

(defun %validate-sp-sucv (obj-id sp-id val value-list context)
  (declare (ignore obj-id context))
  (let (msg-list)
    (unless (equal+ value-list val)
      (push
       (format nil 
           "successor to relation ~S should be ~S rather than (~{~S~^ ~}). ~
            ~%We ignore the value(s)." sp-id val value-list)
       msg-list)
      (setq value-list :failure))
    (values value-list msg-list nil)))

#|
(defrelation "ra-sucv" (:from "test")(:to "target")(:value _tg2))
;; does not work!
|#
;;;=============================================================================
;;;                       Validate Attributes
;;;---------------------------------------------------------------- %VALIDATE-TP
;;;   :type       ($TPRT (0 :integer))
;;;   :not-type   ($NTPR (0 :integer))
;;;   :value      ($VALR (0 "Albert"))
;;;   :one-of     ($ONEOF (0 1 2 3)) ($SEL :exists))
;;;   :between, :outside, :filter, :same, :different  ($OPR (0 :xxx 3 5 ...))
;;;   :min, :max, :unique  ($MIN 1) ($MAX 1)
;;; values are checked against tp restrictions. Each specific restriction returns
;;; the list of repaired values that can be nil (no more values) or :failure
;;; meaning that the test failed and values could not be repaired.
;;; when the test value is :failed no more tests can be run on those values


(defUn %validate-tp (tp-id value-list &key (obj-id :unknown) no-repair mln 
                           restrictions context no-warning)
  "Checks if the property values obey the restrictions attached to the attribute ~
   in the current version. For MLNs tests only cardinality. We keep cardinality ~
   checks to the end, since values may have been modified. Duplicates are ~
   allowed.
Arguments:
   tp-id: id of the attribute
   value-list: list of values to check
   context (key): context if different from current one
   obj-id (key): object being modified (default :unknown)
   no-repair (key): default is nil, i.e. remove offending values
   mln (key): tells the function that the values are MLN values for some language
   no-warning (key): if t, does not print warning messages
   restrictions (key): used to specify requested restrictions
Return:
   2 values:
       1st value: value-list (repaired if no-repair is nil/default) if OK, nil
           if some error occurred
       2nd value: list of string error/warning messages; non nil means an error."
  (declare (special *attribute-restrictions*))
  
  ;; we must keep a NIL list in case of a :min restriction (e.g. when all values
  ;; have been removed)
  ;(unless value-list (return-from %validate-tp nil))
  
  ;; check if value is an MLN (assumed to have been extracted from the right 
  ;; context)
  ;; if so, special case
  (if (or (mln::%mln? (car value-list))
          (mln::mln? (car value-list)))
      ;; if so go organize validationvalidation for each separate language
      (return-from %validate-tp
        (%validate-tp-mln tp-id value-list :no-repair no-repair :obj-id obj-id)))
  
  (let* ((context (or context (symbol-value (intern "*CONTEXT*"))))
         (restriction-list (or restrictions *attribute-restrictions*))
         (type-ref (car (%%get-value tp-id '$TPRT context)))
         (repaired-values value-list)
         one-of-list val message-list msg-list header)
    
    ;; prepare header in case no-warning is false
    (setq header
          (format nil "Warning: when validating values for attibute ~S of object ~
              ~S in package ~S and context ~S:"
            tp-id obj-id (package-name *package*) context))
    
    ;; if values come from MLNs, we do not check everything
    (if (or mln (eql type-ref :mln))
        (setq restriction-list `($ONEOF $MINT $MAXT $OPR $OPRALL)))
    
    ;(format t "%; %validate-tp /MLN restriction-list:~%  ~S" restriction-list)
    
    (dolist (restriction restriction-list)
      ;(format t "~%; %validate-tp /MLN restriction: ~S" restriction)
      (cond
       ;;=== single obligatory value
       ((and (eql restriction '$VALR)
             (setq val (%%get-value tp-id restriction context)))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-tp-valr obj-id tp-id val repaired-values context)))
       
       ;;=== type of value is specified: all values not of the type are removed
       ((and (eql restriction '$TPRT)
             (setq val (car (%%get-value tp-id restriction context))))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-tp-type obj-id tp-id val repaired-values context)))
       
       ;;=== values should not be of that type
       ((and (eql restriction '$NTPR)
             (setq val (car (%%get-value tp-id restriction context))))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-tp-not-type obj-id tp-id val repaired-values context)))
       
       ;;=== ONE-OF restriction
       ((and (eql restriction '$ONEOF)
             (setq one-of-list (%%get-value tp-id restriction context)))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-tp-one-of obj-id tp-id one-of-list repaired-values context)))
       
       ;;=== special operators: between, outside applying to a single value
       ((and (eql restriction '$OPR)
             (setq val (%%get-value tp-id restriction context)))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-tp-opr obj-id tp-id val repaired-values context :mln mln)))
       
       ;;=== special operators: same, different applying to a set of values
       ((and (eql restriction '$OPRALL)
             (setq val (%%get-value tp-id restriction context)))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-tp-oprall obj-id tp-id val repaired-values context :mln mln)))
       
       ;;=== min
       ((and (eql restriction '$MINT)
             (setq val (car (%%get-value tp-id restriction context))))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-tp-min obj-id tp-id val repaired-values context)))
       
       ;;=== max
       ((and (eql restriction '$MAXT)
             (setq val (car (%%get-value tp-id restriction context))))
        (multiple-value-setq (repaired-values msg-list)
          (%validate-tp-max obj-id tp-id val repaired-values context)))
       
       ) ; end case
      (when msg-list
        (setq message-list (append message-list msg-list))
        ;; reset msg-list
        (setq msg-list nil))
      
      ;; when failure get out of function
      (when (eql repaired-values :failure) 
        (unless no-warning (mapc #'warn message-list))
        (return-from %validate-tp
          (values (if no-repair value-list) message-list)))
      ) ; end list of restrictions
    
    ;; process the result
    (unless no-warning 
      (mformat "~{%~A~^ ~}" (cons header (reverse message-list))))
    
    (values (if no-repair value-list repaired-values) message-list)))

#|
Tests are listed in the z-moss-tests-validate.lisp file
|#
;;;----------------------------------------------------------- %VALIDATE-TP-MAX

(defun %validate-tp-max (obj-id tp-id val value-list context &aux msg)
  "check if too many values"
  (declare (ignore context tp-id obj-id))
  (when (> (length value-list) val)
    (setq msg
          (format nil "too many values (~{~S~^ ~}) max is ~S.~%We keep them anyway."
            value-list val)))
  (values value-list (if msg (list msg))))

#|
(defconcept "test")
(defattribute "aa-max" (:class-ref "test")(:max 2))
(defindividual "test" (:var _ta))

(%validate-tp-max _ta '$T-AA-MAX 2 '("A" 1 2 "b" 3) 0)
("A" 1 2 "b" 3)
("too many values (\"A\" 1 2 \"b\" 3) max is 2.
We keep them anyway.")
|#
;;;----------------------------------------------------------- %VALIDATE-TP-MIN

(defun %validate-tp-min (obj-id tp-id val value-list context)
  "check if enough values"
  (declare (ignore context tp-id obj-id))
  (let (msg)
    (when (< (length value-list) val)
      (setq msg
            (format nil "not enough values (~{~S~^ ~}), min is ~S. ~
               ~%We keep them anyway."
              value-list val)))
    (values value-list (if msg (list msg)))))


;;;----------------------------------------------------------- %VALIDATE-TP-MLN

(defun %validate-tp-mln (tp-id value-list &key no-repair no-warning obj-id)
  "check MLN value must validate each language
Arguments:
   tp-id: attribute id
   value-list: list of one MLN
   no-repair (key): if true, then we are only interested in warning messages
   no-warning (key): if t, tell MOSS not to print warning messages
   obj-id (key): id of the object owning the attribute
Return
   1. MLN list eventually modified unless no-repair is true
   2. a list of warning messages"
  (let ((mln (car value-list))
        message-list validated-values msg-list update-list lan)
  
    ;; take care of old MLN format, should not return errors
    (if (mln::%mln? mln) (setq mln (mln::make-mln mln)))
    
    (dolist (pair mln)
      (setq lan (car pair))
      (multiple-value-setq (validated-values msg-list)
        (%validate-tp tp-id (cdr pair) :no-repair no-repair :obj-id obj-id
                          :no-warning no-warning :mln t))
      ;; modify language value if val-list different from (cdr pair)
      (unless (equal+ validated-values (cdr pair))
        (push (list lan validated-values) update-list))
      (setq message-list (append message-list msg-list)))
    
    ;; modify mln if there are some changes to do
    (when update-list
      (dolist (item update-list)
        (setq mln (cons item (mln::remove-language mln (car item))))))
    
    (unless no-warning (mapc #'warn message-list))
    
    ;; return
    (values (if no-repair value-list (list mln)) message-list)))
       
#|

(defconcept "test")
(defattribute "ac" (class-ref "test")(:not-type :number))
(defindividual "test" (:var _ta))

(%validate-tp-not-type _ta '$T-TEST-AC :number '("A" 1 2 "b" 3) 0)

|#
;;;------------------------------------------------------ %VALIDATE-TP-NOT-TYPE
  
(defun %validate-tp-not-type (obj-id tp-id val value-list context)
  "check if none of the value are of that type.
Return:
   1. a list of values excluding values of type val
   2. a list of warning messages."
  (declare (ignore context obj-id tp-id))
  (let (msg-list result)
    (setq result
          (loop for item in value-list 
              if (not (%is-value-type? item val)) collect item
              else do
                (push
                 (format nil "Value ~S should not be of type ~S.~%We ignore  ~
                     that value." item val)
                 msg-list)))
    (values result (reverse msg-list))))

#|
(defconcept "test")
(defattribute "ac" (class-ref "test")(:not-type :number))
(defindividual "test" (:var _ta))

(%validate-tp-not-type _ta '$T-TEST-AC :number '("A" 1 2 "b" 3) 0)
("A" "b")
("Value 1 for attribute $T-TEST-AC of object $E-TEST.67 should not be of type 
  :NUMBER in package \"MOSS\" and context 0.
We ignore that value."
 "Value 2 for attribute $T-TEST-AC of object $E-TEST.67 should not be of type 
  :NUMBER in package \"MOSS\" and context 0.
We ignore that value."
 "Value 3 for attribute $T-TEST-AC of object $E-TEST.67 should not be of type 
   :NUMBER in package \"MOSS\" and context 0.
We ignore that value.")

(%validate-tp-not-type _ta '$T-TEST-AC :number '() 0)
NIL
NIL
|#
;;;-------------------------------------------------------- %VALIDATE-TP-ONE-OF

(defun %validate-tp-one-of (obj-id tp-id one-of-list value-list context)
  "check the one-of option, depending on the value of $SEL.
Return:
   1. the modified list or :failure if $SEL is :forall or :not and test fails
   2. a list of warnings"
  (let (msg)
    ;; result depends on the $SEL value
    (case (or (car (%%get-value tp-id '$SEL context)) :exists)
      (:exists ; one of the values should be in the one-of list
       (unless 
           (some #'(lambda (xx) (member+ xx one-of-list)) value-list)
         (setq msg
               (format nil 
                   "some value in (~{~S~^ ~}) should be one of (~{~S~^ ~}).~% We ~
                    keep the value(s) anyway." value-list one-of-list)))
       )
      (:forall ; all values should be in the one-of list
       (unless 
           (every #'(lambda (xx) (member+ xx one-of-list)) value-list)
         (setq msg
               (format nil 
                   "all values (~{~S~^ ~}) should be in (~{~S~^ ~}).~%We ignore ~
                    the value(s)." value-list one-of-list))
         (setq value-list :failure)
         )
       )
      (:not ; no value should be in the one-of list
       (unless 
           (every #'(lambda (xx) (not (member+ xx one-of-list))) value-list)
         (setq msg
               (format nil 
                   "no value (~{~S~^ ~}) should be in (~{~S~^ ~}).~%We ignore ~
                    the value(s)." value-list one-of-list ))
         (setq value-list :failure))
       )
      (t (setq msg
               (format nil 
                   "illegal selection spec ~S associated with :one-of ~
                     option for attribute ~S of object ~S in package ~S and ~
                     context ~S with values  (~{~S~^ ~})" 
                 (car (%%get-value tp-id '$SEL context))
                 tp-id obj-id (package-name *package*) context value-list))
         ))
    (values value-list (if msg (list msg)))))

#|
(defconcept "test")
(defattribute "ad" (class-ref "test")(:one-of "a" 1 "b" 2))
;;********** when generic property exists uses the generic property and does not
;; create a local one
(defindividual "test" (:var _ta))

(%validate-tp-one-of _ta '$T-TEST-AD '("a" 1 "b" 2) '("A" 1 2 "b" 3) 0)
("A" 1 2 "b" 3)

(defattribute "ae" (:class-ref "test")(:one-of "a" 1 "b" 2)(:forall))
(%validate-tp-one-of _ta '$T-TEST-AE '("a" 1 "b" 2) '(3 "d") 0)
:FAILURE
("all values (3
            \"d\") for attribute $T-TEST-AE of object $E-TEST.2 in package \"MOSS\" 
and context 0 should be in (\"a\" 1 \"b\" 2)
We ignore the value(s).")

(defattribute "af" (:class-ref "test")(:one-of "a" 1 "b" 2)(:not-in))
(%validate-tp-one-of _ta '$T-TEST-AF :number '() 0)
NIL
NIL
|#
;;;----------------------------------------------------------- %VALIDATE-TP-OPR

(defun %validate-tp-opr (obj-id tp-id opr-list value-list context &key mln)
  "checking special operators like between, outside, same, different.
Return:
   1. updated list or :failure
   2. list of warning messages"
  (let (msg-list val-list)
    ;; when the values come from MLNs we do not check between nor outside since
    ;; all values are strings
    (case (car opr-list)
      ;; all values should in the specified range; however we keep them all unless
      ;; unless they are not numbers
      (:between
       (dolist (val value-list)
         (unless mln
           (cond
            ((not (numberp val))
             (push
              (format nil "value ~S for attribute ~S in package ~S and ~
                         context ~S should be a number.~%We ignore that value." 
                val tp-id (package-name *package*) context)
              msg-list))
            ((not (and (>= val (cadr opr-list))
                       (<= val (caddr opr-list))))
             (push 
              (format nil "value ~S for attribute ~S of object ~S is not a number ~
                 between ~S and ~S in package ~S and context ~S~%We keep it anyway." 
                val tp-id obj-id (cadr opr-list)(caddr opr-list)
                (package-name *package*) context)
              msg-list)
             (push val val-list))
            (t (push val val-list))))
         (values (reverse val-list) (reverse msg-list))))
      
      (:outside
       (unless mln
         (dolist (val value-list)
           (cond
            ((not (numberp val))
             (push
              (format nil "value ~S for attribute ~S in package ~S and ~
                         context ~S should be a number.~%We ignore that value." 
                val tp-id (package-name *package*) context)
              msg-list))
            ((or (< val (cadr opr-list))
                 (> val (caddr opr-list)))
             (push 
              (format nil "value ~S for attribute ~S of object ~S is not a number ~
                 outside ~S and ~S in package ~S and context ~S~%We keep it anyway." 
                val tp-id obj-id (cadr opr-list)(caddr opr-list)
                (package-name *package*) context)
              msg-list)
             (push val val-list))
            (t (push val val-list))))
         ))
      )
    (values (reverse val-list) (reverse msg-list))))
    
#|
(defconcept "test")
(defattribute "ag" (:class-ref "test")(:between 5 10))
(defindividual "test" (:var _ta))

(%validate-tp-opr _ta '$T-TEST-AG '(:between 5 10) '("A" 1 6 "b" 7) 0)
(1 6 7)
("value \"A\" for attribute $T-TEST-AG in package \"MOSS\" and context 0 should 
  be a number.
We ignore that value."
 "value 1 for attribute $T-TEST-AG of object $E-TEST.2 is not a number between 5
  and 10 in package \"MOSS\" and context 0
We add it anyway."
 "value \"b\" for attribute $T-TEST-AG in package \"MOSS\" and context 0 should 
  be a number.
We ignore that value.")

(%validate-tp-opr _ta '$T-TEST-AG '(:outside 5 10) '("A" 1 6 "b" 7 11) 0)
(1 6 7 11)
("value \"A\" for attribute $T-TEST-AG in package \"MOSS\" and context 0 should 
  be a number.
We ignore that value."
 "value 6 for attribute $T-TEST-AG of object $E-TEST.2 is not a number outside 5 
  and 10 in package \"MOSS\" and context 0
We add it anyway."
 "value \"b\" for attribute $T-TEST-AG in package \"MOSS\" and context 0 should 
  be a number.
We ignore that value."
 "value 7 for attribute $T-TEST-AG of object $E-TEST.2 is not a number outside 5
  and 10 in package \"MOSS\" and context 0
We add it anyway.")


(defattribute "ah" (:class-ref "test")(:same))
(%validate-tp-opr _ta '$T-TEST-AH '(:same) '(3 "d") 0)
(3 "d")
"values (3
        \"d\") for attribute $T-TEST-AH of object $E-TEST.2 should be all the same 
 in package \"MOSS\" and context 0.
We add them anyway."

(%validate-tp-opr _ta '$T-TEST-AH '(:same) '("d" "d" "d") 0)
("d" "d" "d")
NIL

(defattribute "ai" (:class-ref "test")(:different))
(%validate-tp-opr _ta '$T-TEST-AI '(:different) '("a" 1 "b" 2) 0)
("a" 1 "b" 2)
NIL

(%validate-tp-opr _ta '$T-TEST-AI '(:different) '("a" 1 "b" "a") 0)
("a" 1 "b" "a")
"values (\"a\" 1 \"b\"
        \"a\") for attribute $T-TEST-AI of object $E-TEST.2 should be all different
 in package \"MOSS\" and context 0. 
We add them anyway."
|#
;;;-------------------------------------------------------- %VALIDATE-TP-OPRALL

(defun %validate-tp-oprall (obj-id tp-id opr-list value-list context &key mln)
  "checking special operators same, different.
Return:
   1. updated list or :failure
   2. list of warning messages"
  (declare (ignore mln))
  (let (msg-list)
    ;; when the values come from MLNs we do not check between nor outside since
    ;; all values are strings
    (case (car opr-list)
      ;; all values should in the specified range; however we keep them all unless
      ;; unless they are not numbers
      
      (:same
       (unless (all-alike? value-list)
         (push
          (format nil 
              "values (~{~S~^ ~}) for attribute ~S of object ~S should be all the ~
               same in package ~S and context ~S.~%We keep them anyway." 
            value-list tp-id obj-id (package-name *package*) context)
          msg-list)))
      
      (:different
       (unless (all-different? value-list)
         (push
          (format nil
              "values (~{~S~^ ~}) for attribute ~S of object ~S should be all ~
               different in package ~S and context ~S. ~%We keep them anyway." 
            value-list tp-id obj-id (package-name *package*) context )
          msg-list)))
      )
    (values value-list msg-list)))

;;;---------------------------------------------------------- %VALIDATE-TP-TYPE

(defun %validate-tp-type (obj-id tp-id val value-list context)
  "check if all values in value-list are of type val
Return:
   1. a list of values excluding values that have not type val
   2. a list of warning messages."
  (let (msg-list result)
    (setq result
          (loop for item in value-list 
              if (%is-value-type? item val) collect item
              else do
                (push
                 (format nil "Value ~S for attribute ~S of object ~S is not of ~
                     type ~S in package ~S and context ~S.~%We ignore that value." 
                   item tp-id obj-id val (package-name *package*) context)
                 msg-list)))
    (values result (reverse msg-list))))

#|
(defconcept "test")
(defattribute "ab" (class-ref "test")(:type :number))
(defindividual "test" (:var _ta))

(%validate-tp-type _ta '$T-TEST-AB :number '("A" 1 2 "b" 3) 0)
(1 2 3)
("Value \"A\" for attribute $T-TEST-AB of object $E-TEST.67 is not of type :NUMBER
   in package \"MOSS\" and context 0.
We ignore that value."
 "Value \"b\" for attribute $T-TEST-AB of object $E-TEST.67 is not of type :NUMBER 
   in package \"MOSS\" and context 0.
We ignore that value.")
|#
;;;---------------------------------------------------------- %VALIDATE-TP-VALR

(defun %validate-tp-valr (obj-id tp-id val value-list context)
  (let (msg)
    (unless (equal+ value-list val)
      (setq msg
            (format nil 
                "value list to add to attribute ~S of object ~S should be ~S ~
                 rather than (~{~S~^ ~}) in package ~S and context ~S.~%We ignore the value(s)."
              tp-id obj-id val value-list (package-name *package*) context))
      (setq value-list :failure))
    (values value-list (if msg (list msg)) nil)))

;;;---------------------------------------------------------- %VALIDATE-TP-VALUE
;;; to check...
  
;;;(defUn %validate-tp-value (restriction 
;;;                           data &key prop-id obj-id (context *context*))
;;;  "Takes some data and checks if they agree with a value-restriction condition,~
;;;   associated with a terminal property. Used by the =xi method whenever ~
;;;   we want to put data as the value of the property.
;;;   Processed conditions are those of *allowed-restriction-operators*
;;;Arguments:
;;;   restriction: restriction condition associated with the property, e.g. 
;;;                (:one-of (1 2 3))
;;;   data: a list of values to be checked
;;;   prop-id (key): identifier of the local property (unused)
;;;   obj-id (key): identifier of the object being processed (unued)
;;;   context (key): context default current (unused)
;;;Return:
;;;   data when validated, nil otherwise."
;;;    (declare (ignore prop-id obj-id context))
;;;    (unless (listp data) (error "data ~S should be a list of values." data))
;;;    (let ((operator (car restriction))
;;;          (arg-list (cdr restriction))
;;;          )
;;;      ;; specific treatment according to the condition
;;;      (case operator
;;;        ;; one if the values of the arg should be of the specified lisp type
;;;        ;; not really useful
;;;        (:exists
;;;         (unless (some (car arg-list) data)
;;;           (return-from %validate-tp-value  nil)))
;;;        ;; all of the data should be of the specified type
;;;        (:all
;;;         ;; if (car arg-list) is a MOSS object, then we check the type
;;;         (cond 
;;;          ((and (%pdm? (car arg-list))
;;;                (every #'(lambda(xx) (%type? xx (car arg-list))) data))
;;;           ;; OK.
;;;           )
;;;          ((%pdm? (car arg-list))
;;;           (return-from %validate-tp-value  nil))
;;;          ((not (every (car arg-list) data))
;;;           (return-from %validate-tp-value  nil))))
;;;        ;; none of the data should be of this type
;;;        (:not-type
;;;         (when (some (car arg-list) data)
;;;           (return-from %validate-tp-value  nil)))
;;;        ;; the value should be unique (this is redundant with cardiality conditions)
;;;        (:unique
;;;         (verbose-warn ":unique is useless, use cardinality conditions"))
;;;        ;; each value should be in the specified list
;;;        (:one-of
;;;         (unless (every #'(lambda (xx) (member xx arg-list)) data)
;;;           (return-from %validate-tp-value  nil)))
;;;        ;; no value should be one of this list
;;;        (:not
;;;         (when (some #'(lambda (xx) (member xx arg-list)) data)
;;;           (return-from %validate-tp-value  nil)))
;;;        ;; all values should be numbers in the specified range (including limits)
;;;        (:between
;;;         (unless (and (every #'integerp (append data arg-list))
;;;                      (every #'(lambda (xx) (and (>= xx (car arg-list))
;;;                                                 (<= xx (cadr arg-list))))
;;;                             data))
;;;           (return-from %validate-tp-value  nil)))
;;;        (:outside
;;;         (unless (and (every #'integerp (append data arg-list))
;;;                      (every #'(lambda (xx) (or (< xx (car arg-list))
;;;                                                (> xx (cadr arg-list))))
;;;                             data))
;;;           (return-from %validate-tp-value  nil)))
;;;        )
;;;      ;; success, return data
;;;      data))

#|
Should run some tests with all sorts of restrictions cross referenced by context
|#
;;;=============================================================================
;;;                          End validation functions
;;;=============================================================================

;;;A------------------------------------------------------------------ %VG-GAMMA

(defUn %vg-gamma (context)
  "Computes the transitive closure upwards on the version graph.
    Get the transitive closure of the context i.e. a merge of all the paths ~
   from context to root without duplicates, traversed in a breadth first ~
   fashion, since we assume that the value should not be far from our context ~
   This list should be computed once for all when the context is set ~
   e.g. in a %set-context function
Argument:
   context: starting context
Return:
   a list of contexts."
  (let ((version-graph (symbol-value (intern "*VERSION-GRAPH*"))))
    ;(format t "~%;=== %vp-gamma /version-graph: ~S" version-graph)
    (%vg-gamma-l nil nil (ncons context) nil version-graph)))

#|
(with-package :moss *version-graph*)
((5 1) (1 0) (0))

(with-package :moss (%vg-gamma 5))
(5 1 0)

(with-package :moss (%vg-gamma 0))
(0)

(with-package :moss (%vg-gamma 20))
(20)

;; must prefix the variable with the package because here we are in the MOSS
;; package
(with-package :test test::*version-graph*)
((6 4) (5 4) (4 0) (3 2) (2 1) (1 0) (0))

(with-package :test (%vg-gamma 5))
(5 4 0)

(with-package :test (%vg-gamma 0))
(0)
|#
;;;A---------------------------------------------------------------- %VG-GAMMA-L

(defUn %vg-gamma-l (lres lnext ll pred version-graph)
  "Service function used be %vg-gamma
Arguments:
   lres: resulting list
   lnext: next candidates to examine later
   ll: working list of candidates
   pred: list of future nodes to process
   version-graph: current version graph in this package
Return:
   lres "
  (cond 
   ;; when all working lists are empty, return the result (in the right order)
   ((not (or lnext ll pred)) (reverse lres))
   ;; when ll and pred are nil, use lnext as set of candidates
   ((not (or ll pred)) (%vg-gamma-l lres nil lnext ll version-graph)) 
   ;; when only pred is nil, add first element of ll into result, put 
   ;; predecessors of the element on the block for next time around 
   ((null pred)(%vg-gamma-l (cons (car ll)lres) 
                            lnext 
                            (cdr ll)
                            (cdr (assoc (car ll) version-graph))
                            version-graph))
   ;; if the first element of pred has not already been processed, put it into
   ;; the list of next candidates 
   ((not (or (member (car pred) lres)
             (member (car pred) lnext)
             (member (car pred) ll)))
    (%vg-gamma-l lres (cons (car pred) lnext) ll (cdr pred) version-graph))
   ;; otherwise discard it
   (t (%vg-gamma-l lres lnext ll (cdr pred) version-graph))
   ))

;;;=============================================================================
;;;          Functions related to conversation while using the web
;;;=============================================================================
;;; the following functions cannot be used with versions
;;; Such functions may execute in the Conversation process. The conversation process
;;; runs on the same machine as the PA

;;;----------------------------------------------------------------- WEB-ACTIVE?

(defun web-active? (conversation &optional version)
  "gets the web tag from conversation. Synonymous of web-get-tag."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (unless (%type? conversation '$CVSE context)
      (terror "web-clear-gate/ conversation arg not the right type: ~S ~
               in package ~S context ~S"
              conversation *package* context))
    (%%get-value conversation '$WEBT context)))

#|
;; needs testing
|#
;;;---------------------------------------------------------------- WEB-ADD-TEXT

(defun web-add-text (conversation text &optional version)
  "adds text to already existing text inserting a CRLF. Text should be HTML. ~
   Adds the text after the old text and a <BR> code to the $WOUT slot of the ~
   conversation object. It the text is a list, picks an element randomly.
Arguments:
   conversation: conversation object controlling the conversation process
   text: text to add: string, mln, list of strings, list of mlns.
   version (opt): context (default current)"
  (when text
    (unless (%type? conversation '$CVSE)
      (terror "web-clear-gate/ conversation arg not the right type: ~S" 
              conversation))
    
    (let* ((context (or version (symbol-value (intern "*CONTEXT*"))))
           (old-text (car (%%has-value conversation '$WOUT context))))
      ;; process all types of text input to get a string
      ;; if text is a list, then pick up one element at random
      (setq 
       text
        (cond
         ((stringp text) text)
         ;; check first MLN, if so get the canonical synonym
         ((and (listp text)(mln::mln? text)) ; jpb 1406
          (mln::get-canonical-name text)) ; jpb 1406
         ;; otherwise pick one element at random (maybe a list of strings or mlns
         ((and (listp text)(every #'stringp text))
          (nth (random (length text)) text))
         ;; coerce anything else to a string
         (t (format nil "~S" text))
         ))
      
      ;; add new text
      (cond
       ((or (null old-text)(equal old-text ""))
        ;; brute-force add
        (%%set-value conversation text '$WOUT context))
       ((not (stringp old-text))
        (terror "bad text value: ~S in conversation object: ~S" 
                old-text conversation))
       ;; normal
       (t
        (%%set-value 
         conversation  
         (format nil "~A <BR> ~A" old-text text) '$WOUT  context))))))

;;;------------------------------------------------------------- WEB-CLEAR-DELAY

(defun web-clear-delay (conversation)
  "reset delay reference count to nil (used with web I/O)"
  (replace-fact conversation :delay-ref-count nil))

;;;-------------------------------------------------------------- WEB-CLEAR-GATE

(defun web-clear-gate (conversation &optional version)
  "removes the gate index from conversation."
  (unless (%type? conversation '$CVSE)
    (terror "web-clear-gate/ conversation arg not the right type: ~S" conversation))
  (%%set-value conversation nil '$WGAT 
               (or version (symbol-value (intern "*CONTEXT*")))))

;;;--------------------------------------------------------------- WEB-CLEAR-TAG

(defun web-clear-tag (conversation &optional version)
  "clears the web tag from conversation."
  (unless (%type? conversation '$CVSE)
    (terror "web-clear-gate/ conversation arg not the right type: ~S" conversation))
  (%%set-value conversation nil '$WEBT 
               (or version (symbol-value (intern "*CONTEXT*")))))

;;;-------------------------------------------------------------- WEB-CLEAR-TEXT

(defun web-clear-text (conversation &optional version)
  "clears the area for outputting text to the Web."
  (unless (%type? conversation '$CVSE)
    (terror "web-clear-gate/ conversation arg not the right type: ~S" conversation))
  (%%set-value conversation nil '$WOUT 
               (or version (symbol-value (intern "*CONTEXT*")))))

;;;--------------------------------------------------------------- WEB-DEC-DELAY

(defun web-dec-delay (conversation)
  "decreases the delay reference count of conversation/FACTS by one. If it ~
   becomes 0, make it nil. Used with OMAS web interfaces)."
  (let ((count (read-fact conversation :delay-ref-count)))
    (cond
     ((and (numberp count) (<= (1- count) 0))
      (replace-fact conversation :delay-ref-count nil))
     ((numberp count)
      (replace-fact conversation :delay-ref-count (1- count)))
     (t ; otherwise reset to nil
      (replace-fact conversation :delay-ref-count nil)))))

;;;--------------------------------------------------------------- WEB-GET-DELAY

(defun web-get-delay (conversation)
  "return the value of the :delay-ref-count from conversation/FACTS (used with ~
   web interfaces"
  (read-fact conversation :delay-ref-count))

;;;---------------------------------------------------------------- WEB-GET-GATE

(defun web-get-gate (conversation &optional version)
  "gets the web gate from conversation."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (unless (%type? conversation '$CVSE context)
      (terror "web-clear-gate/ conversation arg not the right type: ~S" conversation))
    ;; straight recovery no inheritance, no defaults
    (car (%%has-value conversation '$WGAT context))))

;;;----------------------------------------------------------------- WEB-GET-TAG

(defun web-get-tag (conversation &optional version)
  "gets the web tag from conversation."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (unless (%type? conversation '$CVSE context)
      (terror "web-clear-gate/ conversation arg not the right type: ~S" conversation))
    (%%has-value conversation '$WEBT context)))

;;;---------------------------------------------------------------- WEB-GET-TEXT

(defun web-get-text (conversation &optional version)
  "gets the text to output to the web"
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (unless (%type? conversation '$CVSE context)
    (terror "web-clear-gate/ conversation arg not the right type: ~S" conversation))
  (car (%%has-value conversation '$WOUT context))))

;;;--------------------------------------------------------------- WEB-SET-DELAY

(defun web-set-delay (conversation)
  "sets a reference count into the :DELAY-REF-COUNT property of FACTS so that ~
   if it is non nil answers from other agents are not posted right away into ~
   web pages (which would open the current gate and break the question sequence."
  (let ((count (read-fact conversation :delay-ref-count)))
    (cond
     ((numberp count)
      (replace-fact conversation :delay-ref-count (incf count))
      )
     (t
      (replace-fact conversation :delay-ref-count 1)))))

;;;---------------------------------------------------------------- WEB-SET-GATE

(defun web-set-gate (conversation gate)
  "sets the gate or gate index from conversation."
  (unless (%type? conversation '$CVSE)
    (terror "web-clear-gate/ conversation arg not the right type: ~S" conversation))
  (%%set-value conversation gate '$WGAT (symbol-value (intern "*CONTEXT*"))))

;;;---------------------------------------------------------------- WEB-SET-MARK

(defun web-set-mark (conversation &optional version)
  "sets the tag in conversation to indicate web exchange."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (unless (%type? conversation '$CVSE context)
      (terror "web-clear-gate/ conversation arg not the right type: ~S" conversation))
    (%%set-value conversation t '$WEBT context)))

;;;---------------------------------------------------------------- WEB-SET-TEXT

(defun web-set-text (conversation text &optional version)
  "sets the text field in conversation, replacing whatever was there."
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    (unless (%type? conversation '$CVSE context)
      (terror "web-set-text/ conversation arg not the right type: ~S" conversation))
    (%%set-value conversation text '$WOUT context)))

;;;=============================================================================
;;;                          End Web functions
;;;=============================================================================


;;;---------------------------------------------------------------- %%ZAP-OBJECT
;;; kills the object removing it from the environment and making the identifier
;;; unbound. Removes all versions of the object (used by reset function)

(defUn %%zap-object (obj-id)
  "Deletes an object by removing all values from all contexts and making its ~
   identifier unbound. This can destroy the database consistency.
Arguments:
   obj-id: identifier of the object
Return:
   not significant."
  (when (boundp obj-id)
    ;; when editing (just in case we use the finttion??)
    (save-old-value obj-id)
    (let ((entity-l (symbol-value obj-id)))
      (while entity-l
             (cond
              ((%is-terminal-property? (caar entity-l))
               (if (not (eql (caar entity-l) '$TYPE))
                   (send (caar entity-l) '=delete-all *self*)))
              ((%is-structural-property? (caar entity-l))
               (send (caar entity-l) '=delete-all *self*))
              ;; do not do anything on inverse terminal properties
              ((%is-inverse-property? (caar entity-l))
               (send (caar entity-l) '=delete-all *self*))
              ;; tell user when not a property
              (t (error "Bad entity format ~A" *self*)))
             (pop entity-l))
      (makunbound obj-id)
      :done
      )))

;;;------------------------------------------------------------------ %ZAP-PLIST

(defUn %zap-plist (obj-id)
  "Clears the plist of a given symbol and proxy."
  (when (symbolp obj-id)
    (setf (symbol-plist obj-id) nil)
    (setq obj-id (%resolve obj-id))
    (setf (symbol-plist obj-id) nil)))

(format t "~%;*** MOSS v~A - Service loaded ***" *moss-version-number*)

:EOF
