;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==============================================================================
;;;20/01/30
;;;           M O S S - W I N D O W (file W-window.lisp)
;;;
;;;==============================================================================
;;; This file contains all the functions to build the MOSS interface window

#|
Copyright: Barth�s@HEUDIASYC, CNRS, Universit� de Technologie de Compi�gne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| The MOSS files are organized as follows:
   MOSS folder  <MOSS directory>
     moss-load.lisp
	 moss-load.fasl
     moss-version-info.lisp
     MOSS source
		FASL
		  moss-XX.fasl <compiled moss files>
        moss-XX.lisp files <moss source files>
     applications
	    ACL-MOSS-ONTOLOGIES <Allegrocache database for application ontologies>
        family.lisp
        <application files>
        fasl
           family.fasl
           <compiled application files>
2006
 Version 6
 =========
 0820 creating the file by putting moss-window.bil and moss-window.cl together
 0833 replacing load by m-load to be able to load ontologies
2007
 0222 modification de OW-OUTLINE-ON-CHANGE
 0224 switching from package :cg-user to :moss
 0323 correcting minor flaws
 0325 adding fake use-bigger-fonts and reset-fonts to keep the compiler quiet
      and minor changes in declares
 0531 adding the environment switch when posting agent ontologies in
      OW-INSTANCE-LIST-ON-CHANGE and OW-OUTLINE-ON-CHANGE
 0728 modifying mw-help-on/off-on-click
2008
 0207 adding catching errors for distribution, so that application does not quit
      on system error but sends a message
 0209 removing calls to compiler when compiler is not present
 0210 changing the style of the MOSS window
 0212 restructuring the logics of the MOSS window
 0215 transferring catch errors to macro file
 0605 make sur that the ontology window created by ow-make-outline-window is
      a child of the system screen.
 0611 #+OMAS -> (when (member :omas ...
 0621 === v7.0.0
2010
 0514 === v8.0 introducing persistency
 0823 adding database key argument to ow-make-outline-window
 0822 reorganizing window
2013
 0414 changing default application to "starter"
2014 -> UTF8 encoding
2016 === v9.0
 0516 mw-process-lisp-expr modified to avoid crash on unmatched parents
2020
 0130 restructuring MOSS as an asdf system
|#

;; Code for the dialog :moss-window

(in-package :moss)

;;; import symbols and functions from cg-user

(eval-when (compile load eval)
  (import '(cg:beep cg:find-component cg:select-window cg:screen cg:make-box
                    cg:make-rgb cg:make-font-ex cg:make-window cg:find-sibling
                    cg:windowp cg:set-focus-component cg:selected-object 
                    cg:find-window cg:find-pixmap cg:interior-width
                    cg:interior-height cg:add-component
                    cg:static-text cg:editable-text cg:multi-line-editable-text
                    cg:button cg:check-box cg:progress-indicator cg:radio-button
                    )))

(fmakunbound 'display-text)

;;;================================= globals ====================================
;;; here we declare variables that are local to this file. Shared global variables
;;; are kept in the globals.lisp file

(eval-when (:load-toplevel :execute)
  ;;  not sure this is needed in an eval-when
  (defVar moss::*distribution-version* nil)
  )

(defParameter *acl-debug-mode* nil)

(defParameter *mw-trace-query* nil)
(defParameter *execute-pressed* nil) ; unused

(defParameter *print-internal-format* nil)

;;;-------------------------------------------------------------------- QTFORMAT

(defUn qtformat (dialog string &rest args)
  "to trace querying process in the MOSS window"
  (if *mw-trace-query* 
      (mw-display-more-info dialog (apply #'format nil string args))))

;;;------------------------------------------------------------------- USER-LOAD
;;; When exec does not include compiler, we cannot use load to load MOSS def 
;;; files. Hence a function that loads each expression one by one and evals them.
;;;********** to be checked...

(defUn user-load (file-pathname &key dialog &aux expr)
  "function allowing to load a lisp file from an application. If compiler ~
   is there, calls the standard load function."
  (if (member :compiler *features*)
      (load file-pathname)
    (with-open-file (ss file-pathname :direction :input)
      (loop
        (setq expr (read ss nil :eof))
        (if (eql expr :eof) (return-from user-load :done))
        (mw-display-more-info dialog (format nil "~%~S" expr))
        (sleep .2)
        (eval expr))))
  )

;;;===============================================================================
;;;                                 MOSS-WINDOW
;;;===============================================================================

;;;=================================== Classes ==================================

;;;------------------------------------------------------------------ MOSS-WINDOW

(defClass MOSS-WINDOW (cg:dialog)
  ((application-package :accessor application-package :initform nil) 
   (clean-pane-before-printing :accessor clean-pane-before-printing :initform t)
   (current-entity :accessor current-entity)
   (conversation :accessor conversation :initform nil) ; pointer to conversation
   (conversation-process :accessor conversation-process :initform nil)
   (database :accessor database :initform nil) ; unused
   (gate :accessor gate :initform nil) ; gate to keep process waiting on nil
   (interaction-mode :accessor interaction-mode :initform :query) ; :lisp :conversation
   (next-editor :accessor next-editor :initform nil) ; points to first editor   
   (outline-window :accessor outline-window :initform nil)
   (owner :accessor owner :initform nil) ; owner is unused in MOSS
   (process :accessor process :initform nil) ; to kill process when closing window
   (query-last-results :accessor query-last-results :initform nil)
   (query-abort-condition :accessor query-abort-condition :initform nil)
   (user-input :accessor user-input :initform nil) ; dialog engine waits on user-input
   )
  (:documentation 
   "Window for interacting with the MOSS environment"))

;;;------------------------------------------------- (MOSS-WINDOW) ACTIVATE-INPUT

(defMethod activate-input ((win moss-window) &key erase)
  "prints a text into the output pane of the window.I
Arguments:
   erase (key): if true erase the content, otherwise select it
Return: unimportant."
  ;; activate the user panel
  ;; the user will enter its data that will be transferred into the to-do
  ;; slot, waking up the resume process
  ;(window-show win)
  ;; erase pane
  (if erase (setf (cg:value (cg:find-component :input-pane win)) ""))
  ;(set-current-key-handler win (view-named :input-pane win))
  )

;;;--------------------------------------------------- (MOSS-WINDOW) DISPLAY-TEXT

(defMethod display-text ((win moss-window) text &key erase final-new-line
										        &allow-other-keys)
  "prints a text into the output pane of the window. Must be a string.
Arguments:
   text: text to display
   erase (key): if t, erase previous text, otherwise append to it
   final-new-line (key): if t add a new line at the end of the text
Return: 
   nil."
  (let ((output-pane (cg:find-component :answer-pane win)))
    (unless (stringp text) (error "~S should be a string." text))
    (if final-new-line
        (setq text (concatenate 'string text "~%")))  
    (if erase
        (setf (cg:value output-pane)(format nil text))
      (setf (cg:value output-pane)
        (concatenate 'string (cg:value output-pane)(format nil text))))
    )
  nil)

;;;------------------------------------------------ (MOSS-WINDOW) INPUT-RECEIVED?

(defMethod input-received? ((win moss-window))
  "checks if something in the user-input slot of the window. If yes, returns it ~
   resetting the slot.
Arguments:
   none
Return: 
   text or nil."
  (let ((text (user-input win)))
    (when text
      (cg::beep)
      (setf (user-input win) nil) 
      text)))

;;;(prog1
;;;    (user-input win)
;;;  (setf (user-input win) nil))

;;;------------------------------------------------ (MOSS-WINDOW) SET-CONVERSATION
;;; not used by ACL ?

(defMethod set-conversation ((win moss-window) conversation-id)
  "initializes the conversation slot of the MOSS window with the id of the current ~
   conversation object.
Arguments:
   conversation-id: conversation id (e.g. cl-user::$CVSE.1)
Return:
   the conversation id."
  (setf (conversation win) conversation-id))

;;;---------------------------------------------------- (MOSS-WINDOW) WINDOW-CLOSE
;;; does not work when running project

;;;(defMethod user-close ((win MOSS-WINDOW))
;;;  "closes the outline window, resetting global variables."
;;;  (format t "~%; user-close 1 /gate: ~S" (gate win))
;;;  
;;;  ;(call-next-method)
;;;  
;;;  ;; kill window process (waiting on gate to exit)
;;;  ;(mp:open-gate (gate win))
;;;  (format t "~%; user-close 2 /gate: ~S" (gate win))
;;;  ;(mp:process-kill process)
;;;  )

;;;==================================== Window ===================================

;;;------------------------------------------------------------- MAKE-MOSS-WINDOW
;; The maker-function, which always creates a new window.
;; Call this function if you need more than one copy,
;; or the single copy should have a parent or owner window.
;; (Pass :owner to this function; :parent is for compatibility.)

#|
(make-moss-window)
|#
(defUn make-moss-window
    (&key parent (owner (or parent (screen cg:*system*)))
          (exterior (make-box 630 0 1280 770)) (name :moss-window)
          ;; default title
          (title (concatenate 'string "MOSS " *moss-version-number*)) 
          (border :frame) (child-p nil) 
          ;; package in which the window is created
          (application-package *package*)
          gate)
  (declare (special *moss-window*))
  ;(format t "make-moss-window /current-process: ~S" mp:*current-process*)
  (let ((win
         (make-window name :owner owner
           :class 'MOSS-WINDOW
           :exterior exterior
           :background-color (make-rgb :red 224 :green 255 :blue 206)
           :border border
           :child-p child-p
           :close-button nil
           :cursor-name :arrow-cursor
           :font (make-font-ex :swiss "MS Sans Serif / ANSI" 11 nil)
           :form-package-name :moss
           :form-state :normal
           :maximize-button nil
           :minimize-button t
           :name :moss-window
           :pop-up nil
           :resizable nil
           :scrollbars nil
           :state :normal
           :status-bar nil
           :system-menu t
           :title title
           :title-bar t
           :toolbar nil
           :dialog-items (make-moss-interface-widgets)
           :help-string nil
           )))
    (setf (gate win) gate)
    (setf (application-package win) application-package)
    (vformat "; make-moss-window / *package*: ~S" *package*)
    (setf (process win) mp:*current-process*)
    ;; *moss-window* scope is this process!
    (setq *moss-window* win)))

;;;-------------------------------------------------- MAKE-MOSS-INTERFACE-WIDGETS

(defUn make-moss-interface-widgets ()
  (list 
   
   ;;=== top row of controls
   ;;--- context
   (make-instance 'static-text 
     :name :context-text :value "Context"
     :left 12 :top 14 :width 60 :height 16  
     :font (make-font-ex :swiss "Tahoma / ANSI" 11 '(:bold)) 
     )
   (make-instance 'editable-text 
     :name :context 
     :left 75 :top 8 :width 40
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :template-string nil
     :up-down-control nil 
     :on-change 'mw-context-on-change 
     :value *context*
     )
   ;;--- language
   (make-instance 'static-text 
     :name :object-id-text :value "Language"
     :left 130  :top 14 :width 70
     :font (make-font-ex :swiss "Tahoma / ANSI" 11 '(:bold))
     )
   (make-instance 'cg::combo-box
     :name :language-combo
     :left 205 :top 8 :width 100 :height 500
     :on-change 'mw-set-language-on-change
     ;; function returning a string to show in the menu
     :on-print #'(lambda (value) (cdr (assoc value *language-tag-names*)))
     :range '(:fr :en)
     :value *language*
     )
   ;;--- package
   (make-instance 'static-text 
     :name :object-id-text :value "Package"
     :left 330  :top 14 :width 65
     :font (make-font-ex :swiss "Tahoma / ANSI" 11 '(:bold))
     )
   (make-instance 'editable-text 
     :name :package 
     :left 400 :top 8 :width 120
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :template-string nil
     :up-down-control nil 
     :on-change 'mw-package-on-change 
     :value (package-name *package*)
     )
   
   ;;=== left goup of buttons
   ;;--- OVERVIEW
   (make-instance 'button 
     :name :overview-button :title "OVERVIEW"
     :left 12 :top 40
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-click 'mw-overview-on-click
     )
   ;;--- DB/LOAD ALL
   (make-instance 'button 
     :title "DB LOAD ALL"
     :left 12 :top 72
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-click 'mw-db-load-all-on-click
     )
   ;;--- DB/STRUCTURE
   (make-instance 'button 
     :title "DB STRUCTURE"
     :left 104 :top 40
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-click 'mw-db-structure-on-click
     )
   ;;--- DB/CONTENT
   (make-instance 'button 
     :name :open-database-button :title "DB CONTENT"
     :left 104 :top 72
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-click 'mw-db-content-on-click
     )
   
   ;;=== check box group
   ;;--- CLEAN PANES
   (make-instance 'check-box 
     :name :clean-box :title "clean panes"
     :left 208 :top 36 :width 100
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-change 'mw-clean-box-on-change
     :value t
     )
   ;;--- TRACE QUERYING
   (make-instance 'check-box 
     :name :trace-box :title "trace queries"
     :left 208 :top 58 :width 100
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-change 'mw-trace-box-on-change
     )
   ;;--- INTERNAL FORMAT
   (make-instance 'check-box
     :name :internal-format :title "(unused)" 
     :left 208 :top 80 :width 100
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     ;:on-change 'mw-internal-format-on-change
     )
   
   ;;=== set of radio buttons (for setting mode)
   ;;--- QUERY mode
   (make-instance 'radio-button 
     :name :query-mode-button :title "Query"
     :left 340 :top 44 :width 60
     :font (make-font-ex nil "Tahoma / Default" 11 nil)
     :on-click 'mw-fix-query-mode-on-click
     :value t
     )
   ;;--- HELP mode
   (make-instance 'radio-button 
     :name :help-mode-button :title "MOSS Help"
     :left 340  :top 62 :width 75
     :font (make-font-ex nil "Tahoma / Default" 11 nil)
     :on-click 'mw-fix-help-mode-on-click
     )
   ;;--- DATA mode
   (make-instance 'radio-button 
     :name :data-mode-button :title "Data"
     :left 340 :top 80 :width 60
     :font (make-font-ex nil "Tahoma / Default" 11 nil)
     :on-click 'mw-fix-data-mode-on-click
     )
   (make-instance 'common-graphics:group-box 
     :name :mode-group :title "Mode" 
     :left 322 :top 32 :width 100 :height 72 
     :contained-widgets '(:query-mode-button :help-mode-button :data-mode-button)
     :font (make-font-ex nil "Tahoma / Default" 11 nil)
     )
   
   ;;=== right group of buttons
   ;;--- EXECUTE
   (make-instance 'button 
     :name :browse-button  :title "EXECUTE"
     :left 435 :top 40 :width 90
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil)
     :on-click 'mw-execute-on-click
     )
   ;;--- ABORT QUERY
   (make-instance 'button
     :name :abort-query-button :title "ABRT QRY"
     :left 435 :top 72 :width 90
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil) 
     :on-click 'mw-abort-query-on-click 
     )
   ;;--- EXIT
   (make-instance 'button 
     :name :exit-button  :title "EXIT"
     :left 536 :top 8 :width 94
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil)
     :on-click 'mw-exit-on-click
     )
   ;;--- BROWSE
   (make-instance 'button 
     :name :browse-button  :title "BROWSE"
     :left 536 :top 40 :width 94
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil)
     :on-click 'mw-browse-on-click
     )
   ;;--- CLEAR ANSWER
   (make-instance 'button
     :name :clear-answer-button :title "CLR-ANSWER"
     :left 536 :top 72 :width 94
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil)
     :on-click 'mw-clear-answer-on-click
     )
   
   ;;=== input pane
   (make-instance 'multi-line-editable-text
     :name :input-pane  
     :left 12 :top 104 :width 618 :height 104 
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil)
     :notify-when-selection-changed t 
     :on-change 'mw-input-on-change 
     :template-string nil 
     :up-down-control nil 
     :delayed nil
     :value "<input>" 
     )
   
   ;;===  thermometer
   (make-instance 'progress-indicator 
     :name :entity-gauge 
     :left 12 :top 208 :width 618 :height 12 
     :font nil 
     :range '(0 100)
     :value 0)
   
   ;;=== answer pane
   (make-instance 'multi-line-editable-text 
     :name :answer-pane
     :left 12 :top 220 :width 618 :height 516  
     :font (make-font-ex nil "Tahoma / ANSI" 11 nil)
     :template-string nil 
     :up-down-control nil 
     :value "<answer pane> *** Now in query mode ***
End input with period or question mark. Return is shift+return.")
   ))

#|
(make-moss-window)
|#

;;;===============================================================================
;;;                      CALLBACKS & SERVICE FUNCTIONS
;;;===============================================================================

;;;-----------------------------------------------------  MW-ABORT-QUERY-ON-CLICK

(defUn mw-abort-query-on-click (dialog widget)
  "set up the *query-abort-condition* variable to t, which will abort the query"
  (declare (ignore widget))
  (setf (query-abort-condition dialog) t)
  t)

;;;--------------------------------------------------------  MW-BALANCED-PARENTS?

(defUn mw-balanced-parents? (text)
  "rough check..."
  (unless (stringp text) 
    (error "arg: ~S should be a string." text))
  (when (equal text "")
    (return-from mw-balanced-parents? nil))
  (let ((right 0)(left 0))
    (dotimes (jj (length text))
      (cond
       ((equal #\( (char text jj)) (incf left))
       ((equal #\) (char text jj)) (incf right))))
    (>= right left)))

#|
CG-USER(151): (moss::mw-balanced-parents? "ljljkh")
T
CG-USER(152): (moss::mw-balanced-parents? "(ljljkh")
NIL
CG-USER(153): (moss::mw-balanced-parents? "(ljljkh)")
T
CG-USER(154): (moss::mw-balanced-parents? "(ljl (jkh))))")
T
CG-USER(155): (moss::mw-balanced-parents? 12)
Error: Funcall of "arg: ~S should be a string." which is a non-function.
[condition type: SIMPLE-ERROR]
CG-USER(156): (moss::mw-balanced-parents? "")                                         
NIL
|#
;;;----------------------------------------------------------  MW-BROWSE-ON-CLICK

(defUn mw-browse-on-click (dialog widget)
  "launches browser"
  (declare (ignore widget))
  ;; reads the selected number of the answer pane, error if not a number
  (let ((input-string (selected-object (find-component :answer-pane dialog)))
        nn test errno)
    ;(print `(MW-BROWSE-ON-CLICK ===> input-string ,input-string))
    ;; protect execution
    (multiple-value-setq (test errno)
      (ignore-errors
       (cond
        ((or (null input-string)
             (equal input-string ""))
         (cg::beep))
        ((numberp input-string)
         (browse (cdr (assoc input-string (query-last-results dialog)))
                 :owner (owner dialog)
                 :database (database dialog)
                 :moss-window dialog))
        ((not (numberp (setq nn (read-from-string input-string))))
         ;; beep do not erase what is posted
         (cg::beep))
        ;; here nn is a number referencing the result of browsing
        ((and (assoc nn (query-last-results dialog)))
         (browse (cdr (assoc nn (query-last-results dialog)))
                 :owner (owner dialog)
                 :database (database dialog)
                 :moss-window dialog))
        )))
    ;; test for errors
    (if (and (null test) errno)
        (cg::beep))
    )
  t)

;;;------------------------------------------------------  MW-CLEAN-BOX-ON-CHANGE

(defUn mw-clean-box-on-change (widget new-value old-value)
  "when flag set, then clean the answer box before printing a new result"
  (declare (ignore old-value))
  (setf (clean-pane-before-printing (cg:parent widget)) new-value)
  (if new-value 
      (setf (cg:value (find-sibling :answer-pane widget)) ""))
  t)

;;;----------------------------------------------------------- MW-CLEAR-ANSWER-IF

(defun mw-clear-answer-if (dialog)
  "clean pane if flag is set"
  (let ((pane (cg:find-component :answer-pane dialog))
        (flag (cg:value (cg:find-component :clean-box dialog)))
        )
    (if flag (setf (cg:value pane) ""))))

;;;----------------------------------------------------  MW-CLEAR-ANSWER-ON-CLICK

(defUn mw-clear-answer-on-click (dialog widget)
  "clear the answer pane"
  (declare (ignore widget))
  (setf (cg:value (find-component :answer-pane dialog))  "")
  ;; reset gauge
  (setf (cg:value (find-component :entity-gauge dialog)) 0)
  t)

;;;--------------------------------------------------------  MW-CONTEXT-ON-CHANGE

(defUn mw-context-on-change (widget new-value old-value)
  "sets up displaying context"
  (declare (ignore old-value))
  (let ((answer-pane (cg:find-component :answer-pane (cg:parent widget)))
        (context-value (read-from-string new-value)))
    (if (catch :error (%%allowed-context? context-value))  
        (progn 
          (setq *context* context-value)
          (setf (cg:value answer-pane)
            (format nil "*** context is now: ~S ***" context-value))
          )
      (progn
        (cg:beep)
        (setf (cg:value answer-pane)
          (format nil "*** ~S is an illegal context ***" new-value))
        (return-from mw-context-on-change nil)
        ))
    t))

;;;------------------------------------------------------- MW-DB-CONTENT-ON-CLICK

(defun mw-db-content-on-click (dialog widget)
  "prints the structure of the database"
  (declare (ignore widget)(special *moss-window*))
  (mw-clear-answer-if dialog)
  (if *database-pathname*
      (let ((*moss-output* (cg:find-component :answer-pane dialog)))
        (db-vomit (intern (package-name *package*) :keyword)))
    ;; otherwise complain
    (let ((answer-pane (cg:find-component :answer-pane dialog)))
      (cg:beep)
      (setf (cg:value answer-pane) "*** No database opened ***")
      )
    ))

;;;------------------------------------------------------ MW-DB-LOAD-ALL-ON-CLICK

(defun mw-db-load-all-on-click (dialog widget)
   "unused"
   (declare (ignore widget))
   (let ((answer-pane (cg:find-component :answer-pane dialog)))
      (cg:beep)
      (setf (cg:value answer-pane) "*** Currently unused ***")
      ))
	  
;;;----------------------------------------------------- MW-DB-STRUCTURE-ON-CLICK

(defun mw-db-structure-on-click (dialog widget)
  "print the content of database"
  (declare (ignore widget)(special *moss-window*))
  (mw-clear-answer-if dialog)
  (if *database-pathname*
      (let ((*moss-output* (cg:find-component :answer-pane dialog)))
        (db-show-structure))
    ;; otherwise complain
    (let ((answer-pane (cg:find-component :answer-pane dialog)))
      (cg:beep)
      (setf (cg:value answer-pane) "*** No database opened ***")
      )
    ))

;;;--------------------------------------------------------  MW-DISPLAY-MORE-INFO

(defUn mw-display-more-info (dialog text)
  "adds some text to the end of the dialog :answer-pane.
Arguments:
   dialog: window, must be there
   text: string representing the text to add
Return:
   t if OK, nil if something wrong"
  (if (windowp dialog)
      (let* ((answer-pane (find-component :answer-pane dialog))
             (old-value (cg:value answer-pane)))
        ;; evaluate expression
        (setf (cg:value answer-pane)
          (concatenate 'string old-value
            (if (string-equal "" old-value)
                (format nil "~A" text)
              (format nil "~%~A" text)))))
    nil))

;;;------------------------------------------------------------- MW-EXIT-ON-CLICK
;;; this function replaces the user-close function of the window, because user-close
;;; has some problems when running a project, and most of the time is not activated

(defun mw-exit-on-click (dialog widget)
  "replace the close button of the window."
  (declare (ignore widget))
  ;; get the gate info and open the gate
  (let ((gate (gate dialog)))
    (if gate (mp:open-gate gate))
    ;; this should kill the process
    (close dialog))
  t)

;;;---------------------------------------------------------  MW-EXECUTE-ON-CLICK

(defUn mw-execute-on-click (dialog widget)
  "reads the content of the input frame, evals it and prints it into the answer ~
   pane. If something is selected in the answer pane, uses this value."
  (let* ((input-pane (find-component :input-pane dialog))
         (answer-pane (find-component :answer-pane dialog))
         (input-string (cg:value input-pane))
         start end)
    
    ;; look for anything selected in the answer pane
    (multiple-value-setq (start end)(cg::get-selection answer-pane))
    ;; if end is different from start, then we have something selected
    (if (> end start)
        ;; then recover it
        (setq input-string (selected-object answer-pane)))
    ;; if nothing is selected use the value from input pane
    
    (mw-process-lisp-expr widget input-string)
    ;; write input-string back into the input pane
    (setf (cg:value input-pane) input-string)
    ;; select input from input pane and set focus
    (mw-select-text input-pane :focus t)
    t))

;;;---------------------------------------------------- MW-FIX-DATA-MODE-ON-CLICK

(defUn mw-fix-data-mode-on-click (dialog item)
  (declare (ignore item))           
  (setf (interaction-mode dialog) :lisp)
  (let ((answer-pane (find-component :answer-pane dialog))
        (input-pane (find-component :input-pane dialog)))
    ;; tell the user that we are now in query mode
    (setf (cg:value answer-pane) 
      (format nil "*** Now in LISP mode in package ~S ***"
        (package-name *package*)))
    ;; select input from input pane and set focus
    (mw-select-text input-pane :focus t)
    )
  t)

;;;---------------------------------------------------- MW-FIX-HELP-MODE-ON-CLICK

(defUn mw-fix-help-mode-on-click (dialog item)
  (declare (ignore item))
  (setf (interaction-mode dialog) :conversation)
  
  ;; create conversation process if not there or dead
  (when (or (null (conversation-process dialog))
            (not (mp:process-alive-p (conversation-process dialog))))
    (format t "~%; mw-fix-help-mode-on-click /calling mw-start-conversation-process")
    ;; record it into the current moss window
    (setf (conversation-process dialog)
      (mp:process-run-function 
         (list :name "MOSS conversation" :initial-bindings cg:*default-cg-bindings*) 
         #'mw-start-conversation
         dialog)))
  
  (let ((answer-pane (find-component :answer-pane dialog))
        (input-pane (find-component :input-pane dialog)))
    ;; tell the user that we are now in query mode
    (setf (cg:value answer-pane) 
      "*** Now in conversation mode (end input with period or question mark) ***")
    ;; select input from input pane and set focus
    (mw-select-text input-pane :focus t)
    )
  t)

;;;--------------------------------------------------- MW-FIX-QUERY-MODE-ON-CLICK

(defUn mw-fix-query-mode-on-click (dialog item)
  (declare (ignore item)
           )
  (setf (interaction-mode dialog) :query)
  (let ((answer-pane (find-component :answer-pane dialog))
        (input-pane (find-component :input-pane dialog)))
    ;; tell the user that we are now in query mode
    (setf (cg:value answer-pane) 
      (format nil "*** Now in query mode in package ~S ***
End your query with period or question mark. New line with Shift + Return."
        (package-name *application-package*)))
    ;; select input from input pane and set focus
    (mw-select-text input-pane :focus t)
    )
  t)

;;;----------------------------------------------------------- MW-INPUT-ON-CHANGE

(defUn mw-input-on-change (item text old-value)
  "Called whenever the content of the input pane (a string) changes.
   If we are in query mode or lisp mode (data) and if we have balanced parents ~
   (more parents that close than that open, we execute the expr.
   If we are in conversation mode (help), we check whether the new char is a ~
   period or a question mark, if so we terminate input and process it."
  (declare (ignore old-value)(special *moss-window*))
  (let ()
    ;(print `(MW-INPUT-ON-CHANGE text ,text) *debug-io*)
    (case (interaction-mode (cg:parent item))
      (:query
       (cond 
        ((and
          (mw-is-last-char-newline? text)
          (mw-is-valid-lisp-expr? text))
         (catch-system-error "while processing query" (mw-process-query item))
         ;; remove the last char and post it
         (setq text (subseq text 0 (1- (length text))))
         (setf (cg:value item) text)
         ;; select the text from the input pane
         (mw-select-text item :focus t)
         )))
      
      (:lisp
       (cond
        ;; a return terminates the input
        ((and (mw-is-last-char-newline? text)
              (mw-is-valid-lisp-expr? text))         
         ;; OK, remove the last char and post text
         (setq text (subseq text 0 (1- (length text))))
         (setf (cg:value item) text)
         ;; process lisp expr, ship text string along
         (mw-process-lisp-expr item text)
         ;; select the text from the input pane
         (mw-select-text item :focus t)
         )))
      
      (:conversation
       (cond 
        ;; a period or a question mark terminate the input
        ((and (> (length text) 0)
              (member (char text (1- (length text))) '(#\. #\?) :test #'equal))
         ;(cg::beep)
         (format *debug-io* "~&;MW-INPUT-ON-CHANGE/ input text: ~S" text)
         ;; remove the last char
         (setq text (subseq text 0 (1- (length text))))
         ;; and post it
         (setf (cg:value item) text)
         (setf (user-input *moss-window*) text)
         ;; select input text and reposition cursor
         (mw-select-text item :focus t)
         (format *debug-io* "~&;MW-INPUT-ON-CHANGE/ input moss window: ~S" 
           (user-input *moss-window*))
         )))
      ;; otherwise process the key stroke by doing nothing
      )
    ;; return t to accept changes ?
    t))

;;;------------------------------------------------- MW-INTERNAL-FORMAT-ON-CHANGE

(defUn mw-internal-format-on-change (widget new-value old-value)
  "sets up a flag for printing internal format of objects"
  (declare (ignore old-value widget))
  (setq *print-internal-format* new-value)
  t)

;;;----------------------------------------------------- MW-IS-LAST-CHAR-NEWLINE?

(defUn mw-is-last-char-newline? (text)
  "checks if last char is newline or return"
  (and (> (length text) 0)
       (member (char text (1- (length text))) '(#\return #\newline) 
               :test #'equal)))

;;;------------------------------------------------------- MW-IS-VALID-LISP-EXPR?

(defUn mw-is-valid-lisp-expr? (text)
  "checks if the inpu string can be read as a lisp expression. Brute force."
  (let (test errno)
    (multiple-value-setq (test errno)
      (ignore-errors
       (read-from-string text)))
    (if (or test (not errno)) t)))

#|
(mw-is-valid-lisp-expr? "t")
T
(mw-is-valid-lisp-expr? "")
NIL
(mw-is-valid-lisp-expr? "(+ 2 3)")
T
(mw-is-valid-lisp-expr? "(+ 2 ")
NIL
|#
;;;--------------------------------------------------------- MW-OVERVIEW-ON-CLICK

(defUn mw-overview-on-click (dialog widget)
  "creates a specific window for displaying the classes, instances and objects"
  (declare (ignore widget))
  ;; build the tree of class names
  (let ((forest (moss::%make-entity-tree-names 
                 (moss::%make-entity-tree))))
    
    (format t "~% mw-overview-on-click /forest: ~S" forest)
    
    ;; display with concepts in alphabetic order
    (ow-make-outline-window  
     (mw-gg (onto-sort (mw-ff forest))) dialog
     :title (format nil "MOSS ~A - ~A -ONTOLOGY and KNOWLEDGE BASE"
              *moss-version-number* (package-name *package*))
     :database (database dialog)
     :moss-window dialog
     :owner (owner dialog) ; the first arg is unused by ow-make-outline-window
     )))

(defUn mw-ff (ll &aux lres)
  (loop 
    (unless ll (return-from mw-ff (reverse lres)) )
    (cond
     ((and (cadr ll)(listp (cadr ll)))
      (push (cons (car ll) (mw-ff (cadr ll))) lres)
      (pop ll) (pop ll))
     (t (push (pop ll) lres)))))

(defUn mw-gg (ll &aux lres)
  (loop
    (unless ll (return-from mw-gg (reverse lres)) )
    (cond
     ((listp (car ll))
      (push (caar ll) lres)
      (push  (mw-gg (cdar ll)) lres)
      (pop ll))
     (t (push (pop ll) lres)))))

(defUn onto-sort (ll)
  (cond ((null ll) nil)
        ((null (member-if #'listp ll)) (sort ll #'string<=))
        (t (mapcar #'cdr 
             (sort 
              (mapcar #'(lambda (xx)
                          (if (stringp xx) 
                              (cons xx xx)
                            (let ((carxx (car xx))
                                  (yy (onto-sort (cdr xx))))
                              (cons carxx (cons carxx yy))
                              )))
                ll)
              #'string<= :key #'car)))
        ))

;;;--------------------------------------------------------- MW-PACKAGE-ON-CHANGE

(defun mw-package-on-change (widget new-value old-value)
  "on change reset package to the one specified."
  (declare (special *package*))
  (let ((package (find-package (string-upcase new-value))))
    (if package
        (progn
          (mw-display-more-info 
           (cg:parent widget) 
           (format nil "...Current package changed to: ~S." 
             (string-upcase new-value)))
          (setq *package* package)
          )
      (progn
        (cg:beep)
        (mw-display-more-info 
         (cg:parent widget) 
         (format nil "...Can't find the specified package: ~S." new-value))
        ;; restore old value
        (setf (cg:value widget) old-value)))))

;;;--------------------------------------------------------- MW-PROCESS-LISP-EXPR

(defUn mw-process-lisp-expr (widget text)
  "takes expr, tries to evaluate it. Traps errors. Prints result into output pane.
Arguments:
   widget: input pane
   expr: lisp expression to evaluate
Return:
   t"
  (let* ((answer-pane (find-sibling :answer-pane widget))
         (old-text (cg:value answer-pane))
         ;(expr (read-from-string text))
         expr ; JPB1605
         test errno alinea)
    ;; if cleaning pane, old text will be erased
    (if (clean-pane-before-printing (cg:parent widget))
        (setf old-text ""))
    ;; if answer pane is empty, do not add blank line
    (setq alinea 
          (if (equal old-text "") "" "~2%"))
    (print `(mw-process-lisp-expr expr ,expr))
    ;; evaluate expr catching possible errors
    (multiple-value-setq (test errno)
      (ignore-errors
       ;(eval expr)))
       ;; to avoid crash on unmatched parents
       (setq expr (read-from-string text)) ; JPB1605
       (eval expr)))
    (print `(mw-process-lisp-expr test ,test errno ,errno))
    ;; if test is nil and errno is not, then print error
    (if (and (null test) errno)
        (setf (cg:value answer-pane) 
          (format nil "~A~?? ~A~% *** Error trying to evaluate: ~S~%  ~S" 
            old-text alinea nil text text (type-of errno)))
      ;; otherwise print result
      (setf (cg:value answer-pane)
        (format nil "~A~?? ~S~%~S" old-text alinea nil expr test)))
    t))

;;;------------------------------------------------------------  MW-PROCESS-QUERY

(defUn mw-process-query (widget)
  "reads a query from the input pane. Checks its syntax. If OK, prints the result ~
   into the answer pane. Uses a number of functions from the query package.
   Search can be interrupted by clicking the ABORT QUERY button.
   This callback executes within the package in which the window has been created."
  (let ((expr (read-from-string (cg:value (find-sibling :input-pane widget))))
        (gauge (find-sibling :entity-gauge widget))
        (ctr 0)
        (dialog (cg:parent widget))
        parsed-expr candidates result gauge-max checking-info)
    ;; reset gauge
    (setf (cg:value gauge) 0)
    (if (clean-pane-before-printing dialog) 
        (setf (cg:value (find-sibling :answer-pane widget)) ""))
    ;; reset result list
    (setf (query-last-results dialog) nil)
    ;; if desired clean up the answer area
    (when (clean-pane-before-printing dialog) 
      (setf (cg:value (find-sibling :answer-pane widget)) ""))
    ;(moss::mw-format "query: ~S" expr)
    ;; parse the expression
    (setq parsed-expr (parse-user-query expr))
    ;; if error, then quits
    (unless parsed-expr 
      (mw-display-more-info dialog "...Can't understand the query.")
      (return-from mw-process-query nil))
    
    ;; build an initial set of objects to examine: candidates
    (setq candidates (moss::collect-candidates parsed-expr))
    ;(moss::mw-format "parsed query; ~S ~%candidates: ~S" parsed-expr candidates)
    ;; if the resulting candidate-list is *none*, then we have no solutions
    (when (or (eq candidates 'moss::*none*)  ; early failure detected by find-subset
              (null candidates)) ; no instances in this class ??
      (mw-display-more-info dialog "...Could not find anything.")
      (return-from mw-process-query nil))
    
    ;; here we have a list of candidates. If the query was an entry point then
    ;; we do not need to filter anything (validate will return immediately)
    ;; otherwise we must prepare filters that will be used for each candidate
    (unless (or (symbolp parsed-expr)(stringp parsed-expr))
      (setq checking-info 
            (moss::split-query-and-compute-filters parsed-expr '(:all)))
      ;; reduce the list by applying local filter to each candidate
      (setq candidates (remove nil (mapcar (car checking-info) candidates)))
      (qtformat dialog "candidates filtered locally:~% ~S" candidates)
      ;; if the resulting candidate-list is *none*, then we have no solutions
      (when (or (eq candidates 'moss::*none*)  ; early failure detected by find-subset
                (null candidates)) ; no instances in this class ??
        (mw-display-more-info dialog "...Could not find anything.")
        (return-from mw-process-query nil)))
    
    ;; set up a progress gauge
    (setq gauge-max (length candidates))
    ;; when tracing tell what happens
    (qtformat dialog "Examining ~S candidate(s)" gauge-max)
    ;; loop for filtering objects, and printing them
    (dolist (candidate candidates)
      (qtformat dialog "...examining:~% ~S" candidate)      
      ;; check for abort condition
      (when (query-abort-condition dialog)
        (setf (query-abort-condition dialog) nil) ;reset flag
        (mw-display-more-info dialog "... Query aborted at user request...")
        (return-from mw-process-query nil))
      (incf ctr)
      ;; trace
      (qtformat dialog "... candidate#~S: ~S (~{~S ~})" 
                ctr CANDIDATE (moss::send candidate 'moss::=summary))
      (setq result (moss::validate2 candidate parsed-expr () checking-info))
      ;; update gauge
      (incf (cg:value gauge)
            (ceiling (/ (* 100 ctr) gauge-max)))
      (when result
        ;; save result onto the result list
        (push (cons ctr candidate) (query-last-results dialog))
        ;; print info for each object
        (mw-display-more-info 
         dialog
         (format nil "~&~3D - ~{~A~}" ctr (moss::send candidate 'moss::=summary)))))
    
    ;; at the end push gauge to 100%
    (setf (cg:value gauge) 100)
    t))

;;;----------------------------------------------------------------- MW-RESET-FONT

(defUn mw-reset-font ()
  "resets the font back to the default in the input and output pane of the MOSS ~
   window."
  ;;;  (let ((ww *moss-window*)
  ;;;        input output)
  ;;;    (when (and ww (wptr ww))
  ;;;      (setq output (view-named :answer-pane ww)
  ;;;            input (view-named :input-pane ww))
  ;;;      ;; must clear window to affect default fonts
  ;;;      (set-dialog-item-text output "")
  ;;;      (set-dialog-item-text input "")
  ;;;      ;; now reset font
  ;;;      (ed-set-view-font output *mw-default-font*)
  ;;;      (set-view-font input *mw-default-font*)
  ;;;      :done)))
  nil)

;;;--------------------------------------------------------------- MW-SELECT-TEXT

(defUn mw-select-text (pane &key focus)
  "select text from input pane and eventually focus on it.
Arguments:
   pane: input pane (must be valid)
   focus (key): if true set focus on pane
Return:
   t"
  (let ((newline-count (count '#\newline (cg:value pane))))
    ;; we must add 1 for each newline in the text...
    (cg::set-selection pane 0 (+ newline-count (length (cg:value pane))))
    ;(sleep 0.4)
    (when focus 
      ;; we cannot set focus directly otherwise button is not updated
      ;; we create a process (birrrkkkhhh!) to do so
      (mp:process-run-function  
       (list :name "focus process" :initial-bindings cg:*default-cg-bindings*)
       #'mw-select-text-set-focus pane)
      )
    t))

;;;----------------------------------------------------- MW-SELECT-TEXT-SET-FOCUS

(defUn mw-select-text-set-focus (pane)
  (sleep .2) ; wait 2/10s
  (cg:set-focus-component pane)
  (mp:process-kill sys:*current-process*))

;;;---------------------------------------------------- MW-SET-LANGUAGE-ON-CHANGE

(defUn mw-set-language-on-change (combo new old)
  "set the language to the new value"
  (declare (ignore combo old)
           (special *language*))
  (setq *language* new)
  t)

;;;-------------------------------------------------------- MW-START-CONVERSATION

(defun mw-start-conversation (dialog)
  "kludge: function called by mw-start-conversation-process to make sure we run ~
   the conversation process in the :moss package. calls start-conversation."
  (declare (special _main-conversation))
  (let ((*package* (find-package :moss)))
    (start-conversation _main-conversation :input dialog :output dialog)))

;;;------------------------------------------------------- MW-TRACE-BOX-ON-CHANGE

(defUn mw-trace-box-on-change (widget new-value old-value)
  "toggles the trace switch moss::*mw-trace-query*"
  (declare (ignore widget old-value)(special MOSS::*MW-TRACE-QUERY*))
  (setq moss::*mw-trace-query* new-value)
  t)

;;;------------------------------------------------------------ MW-USE-BIGGER-FONT

(defUn mw-use-bigger-font (&optional (size 18))
  "sets the font to a larger size in the input and output panes of the MOSS ~
   window."
  ;;;  (let ((ww *moss-window*)
  ;;;        mw-font input output)
  ;;;    (when (and ww (wptr ww))
  ;;;      (setq output (view-named :answer-pane ww)
  ;;;            input (view-named :input-pane ww))
  ;;;      ;; make new font 
  ;;;      (setq mw-font (subst size 11 *mw-default-font*))
  ;;;      ;; must clear window to affect default fonts
  ;;;      (set-dialog-item-text output "")
  ;;;      (set-dialog-item-text input "")
  ;;;      ;; now reset font
  ;;;      (ed-set-view-font output mw-font)
  ;;;      (set-view-font input mw-font)
  ;;;      :done)))
  (declare (ignore size))
  nil)

(format t "~%;*** MOSS v~A - MOSS window loaded ***" *moss-version-number*)

;;; :EOF