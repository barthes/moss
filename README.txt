MOSS v 9.1.0
============
This version of MOSS is intended for the ACL environment of Frantz Lisp. It uses the ACL windowing system.


====================================================================================================

Known Bugs
==========
A large number of bugs remains in this alpha version of the sofware. Most of them are not crucial to casual execution.

1. Orphans are not displayed in the overview window.

2. Editor is shaky

3. save/load mechanism is shaky (unsufficiently tested)

4. Some query syntax bring lead to an abort condition, rather than a warning

<to be completed>


Needed Improvements
==================
In addition fo fixing the different bugs:

1. Virtual classes should be taken into account by the query system

2. Large ontology files should be tested more thoroughly

3. The HELP dialog should be developed (easy and fun, but takes time)

4. An RDFS output should be provided (in triple form). This requires defining a local URI.


<more>

