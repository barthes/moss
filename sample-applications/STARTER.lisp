;;;-*- Mode: Lisp; Package: "STARTER" -*-
;;;=============================================================================
;;;13/04/14
;;;			S T A R T E R (File starter.lisp)
;;;		
;;;			Copyright Jean-Paul Barth�s @ UTC 2013
;;;
;;;=============================================================================

;;; THIS FILE CANNOT BE LOADED FROM THE MOSS-WINDOW. It must be loaded manually.

;;; This file creates an empty environment for exercising MOSS
#|
2013
 0414 creation
|#

(defpackage :starter (:use :cl :moss))
(in-package :starter)

(moss::mformat "~%;*** loading Starter environment ***")

;;; remove tracing while objects are built
(moss::toff)

(setq moss::*allow-forward-references* nil)

(defontology 
  (:title "PRIMER environment")
  (:package :starter)
  (:version "1.0")
  (:language :en)
  ) 

(moss::mformat "~%*** starter file loaded ***")