﻿;;;-*- Mode: Lisp; Package: "FAMILY" -*-
;;;=============================================================================
;;;10/02/23
;;;			F A M I L Y (File FAMILY-Mac.LSP)
;;;		
;;;			Copyright Jean-Paul Barthès @ UTC 1994-2010
;;;
;;;=============================================================================

;;; THIS FILE CANNOT BE LOADED FROM THE MOSS-WINDOW. It must be loaded manually.

;;; This file creates a family for checking the LOB window system, and the query 
;;; system. Thus it requires the corresponding modules.
;;;1995
;;;  2/13 Adding relationships in the family to be able to use the file for 
;;;       testing the query system
;;;       Adding other persons not actual members of the family
;;;  2/18 Adding a mechanism for saturating the family links -> v 1.1.1
;;;  2/19 Adding the age property to all persons and birth date (year)
;;;       Changing the version name to comply with the curreunt MOSS version 3.2
;;;       -> v 3.2.2
;;;  2/25 Adding orphans -> v 3.2.3
;;;1997
;;;  2/18 Adding in-package for compatibility with Allegro CL on SUNs
#|
2003
 0517 used to test changes to the new MOSS v4.2a system
2004
 0628 removing ids from the class definitions
 0715 adding :common-graphics-user-package for ACL environments
 0927 changinging the é and é characters to comply with ACL coding...
2005
 0306 defining a special file for mac because of the problem of coding the French
      letters
 1217 Changing the file to adapt to Version 6.
2006
 0822 adding count to the inference loop
2007
 0305 cosmetic changes in the documentation
2008
 0520 changing defownmethod syntax
2010
 0209 changing syntax to align with MOSS version 8.0
|#

(defpackage :family (:use :cl :moss))
(in-package :family)

(moss::mformat "~%;*** loading Family 8.0 Tests ***")

;;; remove tracing while objects are built
(moss::toff)

(setq moss::*allow-forward-references* nil)

(defontology 
  (:title "FAMILY Test Ontology")
  (:package :family)
  (:version "1.0")
  (:language :all)
  ) 

(defconcept (:en "PERSON" :fr "PERSONNE") 
  (:doc :en "Model of a physical person")
  (:tp (:en "NAME" :fr "NOM") (:min 1)(:max 3)(:entry))
  (:tp (:en "FIRST-NAME" :fr "PRENOM"))
  (:tp (:en "NICK-NAME" :fr "SURNOM"))
  (:tp (:en "AGE" :fr "AGE") (:unique))
  (:tp (:en "BIRTH YEAR" :fr "ANNEE DE NAISSANCE") (:unique))
  (:tp (:en "SEX" :fr "SEXE") (:unique))
  (:sp (:en "BROTHER" :fr "FRERE") (:to "PERSON"))
  (:sp (:en "SISTER" :fr "SOEUR") (:to "PERSON"))
  (:rel (:en "HUSBAND" :fr "MARI") (:to "PERSON"))
  (:rel (:en "WIFE" :fr "FEMME") (:to "PERSON"))
  (:rel (:en "MOTHER" :fr "MERE") (:to "PERSON"))
  (:rel (:en "FATHER" :fr "PERE") (:to "PERSON"))
  (:rel (:en "SON" :fr "FILS") (:to "PERSON"))
  (:rel (:en "DAUGHTER" :fr "FILLE") (:to "PERSON"))
  (:rel (:en "NEPHEW" :fr "NEVEU") (:to "PERSON"))
  (:rel (:en "NIECE" :fr "NIECE") (:to "PERSON"))
  (:rel (:en "UNCLE" :fr "ONCLE") (:to "PERSON"))
  (:rel (:en "AUNT" :fr "TANTE") (:to "PERSON"))
  (:rel (:en "GRAND-FATHER" :fr "GRAND PERE") (:to "PERSON"))
  (:rel (:en "GRAND-MOTHER" :fr "GRAND MERE") (:to "PERSON"))
  (:rel (:en "GRAND-CHILD" :fr "PETITS-ENFANTS") (:to "PERSON"))
  (:rel (:en "COUSIN" :fr "COUSIN") (:to "PERSON"))
  )

(defconcept (:en "COURSE" :fr "COURS")
  (:tp (:en "TITLE" :fr "TITRE") (:unique))
  (:tp (:en "LABEL" :fr "CODE") (:entry))
  (:doc (:en "Course followed usually by students"))
  )

(defconcept (:en "STUDENT" :fr "ETUDIANT")
  (:doc (:en "A STUDENT is a person usually enrolled in higher education"))
  (:is-a "PERSON")
  (:rel (:en "COURSES" :fr "COURS") (:to "COURSE"))
  )

(defconcept (:en "ORGANIZATION" :fr "ORGANISME")
  (:doc (:en "an ORGANIZATION is a body of persons organized for a specific purpose ~
              as a club, union or society."))
  (:tp (:en "NAME" :fr "NOM"))
  (:tp (:en "ABBREVIATION" :fr "SIGLE") (:entry))
  (:rel (:en "EMPLOYEE" :fr "EMPLOYE") (:to "PERSON"))
  (:rel (:en "STUDENT" :fr "ETUDIANT") (:to "STUDENT"))
  )




#|
(defownmethod =make-entry ("NAME" HAS-PROPERTY-NAME TERMINAL-PROPERTY
                                    :class-ref PERSON)
  (data &key package)
  "Builds an entry point for person using standard make-entry primitive"
  (declare (ignore package))
  (moss::%make-entry-symbols data))
|#

(defownmethod =if-needed ("AGE" HAS-MOSS-PROPERTY-NAME MOSS-ATTRIBUTE :class-ref PERSON)
  (obj-id)
  (let ((birth-year (car (send obj-id '=get 'HAS-BIRTH-YEAR))))
    (if (numberp birth-year) 
      ;; do not forget, we must return a list of values
      (list (- (get-current-year) birth-year)))))

(definstmethod =summary PERSON ()
  "Extract first names and names from a person object"
  (let ((result (format nil "~{~A~^ ~} ~{~A~^-~}" 
                        (g==> "first-name") (g==> "name"))))
    (if (string-equal result " ")
      (setq result "<no data available>"))
    (list result)))

;(format nil "~{~A~^ ~}, ~{~A ~}" '("Barthès" "Biesel") '("Dominique" "Sophie"))

;;; First define a macro allowing to create persons easily

(defMacro mp (name first-name sex var &rest properties)
  `(progn
     (defVar ,var)
     (defindividual "PERSON" 
       ("NAME" ,@name)
       ("FIRST-NAME" ,@first-name)
       ("SEX" ,sex)
       (:var ,var)
       ,@properties)))

(defMacro ms (name first-name sex var &rest properties)
  `(progn
     (defVar ,var)
     (defindividual STUDENT 
       ("NAME" ,@name)
       ("FIRST-NAME" ,@first-name)
       ("SEX" ,sex)
       (:var ,var)
       ,@properties)))

;;; Instances

(mp ("Barthès") ("Jean-Paul" "A") "m" _jpb ("BIRTH YEAR" 1945))
(mp ("Barthès" "Biesel") ("Dominique") "f" _dbb ("HUSBAND" _jpb)("BIRTH YEAR" 1946))
(mp ("Barthès") ("Camille" "Xavier") "m" _cxb ("FATHER" _jpb) ("MOTHER" _dbb)
    ("BIRTH YEAR" 1981))
(ms ("Barthès") ("Peggy" "Sophie") "f" _psb ("FATHER" _jpb) ("MOTHER" _dbb)
    ("BROTHER" _cxb)("BIRTH YEAR" 1973))
(mp ("Barthès") ("Pierre-Xavier") "m" _pxb ("BROTHER" _jpb)("AGE" 54))
(mp ("Barthès") ("Claude" "Houzard") "f" _chb ("HUSBAND" _pxb)("AGE" 52))
(mp ("Barthès") ("Antoine") "m" _ab ("FATHER" _pxb)("MOTHER" _chb)
    ("COUSIN" _cxb _psb)("AGE" 27))
(mp ("Barthès") ("Sébastien") "m" _sb ("FATHER" _pxb)("MOTHER" _chb)
    ("BROTHER" _ab)("COUSIN" _cxb _psb)("AGE" 24))
(mp ("Barthès") ("Emilie") "f" _eb ("FATHER" _pxb)("MOTHER" _chb)
    ("BROTHER" _ab _sb)("COUSIN" _cxb _psb)("AGE" 21))
(mp ("Barthès") ("Papy") "m" _apb ("SON" _jpb _pxb)("AGE" 85))
(mp ("Barthès") ("Mamy") "f" _mlb ("SON" _jpb _pxb)("HUSBAND" _apb)("AGE" 81))
(mp ("Labrousse" "Barthès") ("Marie-Geneviève") "f" _mgl ("FATHER" _apb)
    ("MOTHER" _mlb)("AGE" 48)("BROTHER" _jpb _pxb))
(mp ("Labrousse") ("Michel") "m" _ml ("WIFE" _mgl)("AGE" 52))
(mp ("Labrousse") ("Sylvie") "f" _sl ("FATHER" _ml)("MOTHER" _mgl)
    ("COUSIN" _psb _cxb _ab _sb _eb)("AGE" 19))
(mp ("Labrousse") ("Claire") "f" _cl ("FATHER" _ml)("MOTHER" _mgl)("SISTER" _sl)
    ("COUSIN" _psb _cxb _ab _sb _eb)("AGE" 18))
(mp ("Labrousse") ("Agnès") "f" _al ("FATHER" _ml)("MOTHER" _mgl)
    ("SISTER" _sl _cl)("COUSIN" _psb _cxb _ab _sb _eb)("AGE" 15))
(mp ("Canac") ("Bruno") "m" _bc)

;---
(ms ("Chen") ("Kejia") "f" _kjc)
(ms ("de Azevedo") ("Hilton") "m" _hda)
(ms ("Scalabrin") ("Edson") "m" _es)
(ms ("Marchand") ("Yannick") "m" _ym)
(ms ("Vandenberghe") ("Ludovic") "m" _lv)
;---
(mp ("Fontaine") ("Dominique") "m" _df)
(mp ("Li") ("ChuMin") "m" _cml)
(mp ("Kassel") ("Gilles") "m" _gk)
(mp ("Guérin") ("Jean-Luc") "m" _jlg)
(mp ("Trigano") ("Philippe") "m" _pt)

(defindividual ORGANIZATION ("ABBREVIATION" "UTC")
  ("NAME" "Université de Technologie de Compiègne")
  ("EMPLOYEE" _jpb _dbb _df _gk _pt)
  ("STUDENT" _kjc _hda))

(defindividual ORGANIZATION  ("ABBREVIATION" "IC")
  ("NAME" "Imperial College")
  ("STUDENT" _psb))

(moss::mformat "~%*** saturating family-links ***")

;(setq *persons* (send *qh* '=process-query '(person)))
(defParameter *persons*
  (list _jpb _dbb _cxb _psb _pxb _chb _ab _sb _eb _apb _mlb _mgl _ml _sl _cl _al _kjc
        _hda _es _ym _lv _df _cml _gk _jlg _pt))
(defVar *rule-set* nil)
(setq *rule-set* nil)

(defMacro ar (&rest rule)
  `(push ',rule *rule-set*))

;;;  Rules to close the family links
;;;
;;; If person P1 has-sex M
;;;              has-brother P2
;;;    then person P2 has-brother P1
(ar "m" has-brother has-brother)
;;;
;;; If person P1 has-sex M
;;;              has-sister P2
;;;    then person P2 has-brother P1
(ar "m" has-sister has-brother)
;;;
;;; If person P1 has-sex M
;;;              has-father P2
;;;    then person P2 has-son P1
(ar "m" has-father has-son)
;;;
;;; If person P1 has-sex M
;;;              has-mother P2
;;;    then person P2 has-son P1
(ar "m" has-mother has-son)
;;;
;;; If person P1 has-cousin P2
;;;    then person P2 has-cousin P1
(ar "m" has-cousin has-cousin)
(ar "f" has-cousin has-cousin)
;;;
;;; If person P1 has-sex M
;;;              has-son P2
;;;    then person P2 has-father P1
(ar "m" has-son has-father)
;;;
;;; If person P1 has-sex M
;;;              has-daughter P2
;;;    then person P2 has-father P1
(ar "m" has-daughter has-father)
;;;
;;; If person P1 has-sex M
;;;              has-wife P2
;;;    then person P2 has-husband P1
(ar "m" has-wife has-husband)
;;;
;;; If person P1 has-sex F
;;;              has-brother P2
;;;    then person P2 has-sister P1
(ar "f" has-brother has-sister)
;;;
;;; If person P1 has-sex F
;;;              has-sister P2
;;;    then person P2 has-sister P1
(ar "f" has-sister has-sister)
;;;
;;; If person P1 has-sex F
;;;              has-father P2
;;;    then person P2 has-daughter P1
(ar "f" has-father has-daughter)
;;;
;;; If person P1 has-sex F
;;;              has-mother P2
;;;    then person P2 has-daughter P1
(ar "f" has-mother has-daughter)
;;;
;;; If person P1 has-sex F
;;;              has-son P2
;;;    then person P2 has-mother P1
(ar "f" has-son has-mother)
;;;
;;; If person P1 has-sex F
;;;              has-daughter P2
;;;    then person P2 has-mother P1
(ar "f" has-daughter has-mother)
;;;
;;; If person P1 has-sex F
;;;              has-husband P2
;;;    then person P2 has-wife P1
(ar "f" has-husband has-wife)


;;; the version using the MOSS service functions does not seem significantly 
;;; faster than the one using the messages

(defUn apply-rule (p1 p2 rule)
  "produces a list of messages to send to modify the data.
Arguments:
   p1: person 1
   p2: person 2
   rule: (sex) rule to be applied
Return:
   nil or message to be sent."
  ;; note that we use =get and not =get-id that would require the local id of
  ;; the specified property, e.g. $t-person-name or $t-student-name, which
  ;; cannot be guarantied in a global rule
  (let ((sex (car rule))
        (sp-name (cadr rule))
        (ret-sp-name (caddr rule)))
    ;; if P1's sex is that of rule
    ;;    and P1 is linked to P2 by property sp-name
    ;; then
    ;;    link P2 to P1 by property ret-sp-name
    ;; e.g. 
    ;;   "f" has-daughter has-mother
    ;;   if P1 is the daughter of P2, add that P2 is the mother of P1
    (if (and (equal sex 
                    (car (moss::%get-value 
                          P1 
                          (moss::%get-property-id-from-name P1 'has-sex))))
             (member P2 (moss::%get-value 
                         P1 
                         (moss::%get-property-id-from-name P1 sp-name))))
      `(send ',P2 '=add-related-objects ',ret-sp-name ',P1))))

#|
(defun apply-rule (p1 p2 rule)
  "produces a list of messages to send to modify the data.
Arguments:
   p1: person 1
   p2: person 2
   rule: (sex) rule to be applied
Return:
   nil or message to be sent."
  ;; note that we use =get and not =get-id that would require the local id of
  ;; the specified property, e.g. $t-person-name or $t-student-name, which
  ;; cannot be guarantied in a global rule
  (let ((sex (car rule))
        (sp-name (cadr rule))
        (ret-sp-name (caddr rule)))
    (if (and (equal sex (car (send P1 '=get 'HAS-SEX)))
             (member P2 (send P1 '=get sp-name)))
      `(send ',P2 '=add-sp ',ret-sp-name ',P1))))
|#

#|
(apply-rule _pxb _jpb '("m" HAS-BROTHER HAS-BROTHER))
|#

(defUn close-family-links (family-list rule-set)
  "Function that takes a list of persons and computes all the links that must be added
to such persons to close the parent links brother, sister, father, mother, son,
daughter, cousin. It executes a loop until no more changes can occur, applying
rules from a rule set. The result is a list of instructions to be executed."
  (let ((change-flag t) action action-list (count 0))
    (print count)
    ;; Loop until no more rule applies
    (loop
      (unless change-flag (return nil))
      ;; reset flag
      (setq change-flag nil)
      ;; loop on couple of objects in the family-list
      (dolist (P1 family-list)
        (dolist (P2 family-list)
          ;; loop on each rule
          (dolist (rule rule-set)
            ;; if the rule applies it produces an instruction to be executed
            (setq action (apply-rule P1 P2 rule))
            ;; which is appended to the instruction list
            (when (and action 
                       (not (member action  action-list :test #'equal)))
              ;; print a mark into the debug trace to tell user we are doing
              ;; some work
              (moss::mformat "*")
              (incf count)
              (if (eql (mod count 50) 0) (moss::mformat "~%~S" count)) ; new line after 50 *s
              (push action action-list)
              ;; in addition we mark that a changed occured by setting the change flag
              (setq change-flag t))))))
    ;; return list of actions
    (reverse action-list)))

(moss::mformat "~%*** closing family links (takes some time) ***")

;;; When loading the file with m-load, a lot of things are deferred and executed
;;; once the file has been loaded, in particlar instance links. 
;;; Thus, the saturation of the instance links cannot be done during the loading of the 
;;; file but must also be deferred. 

(push '(mapcar #'eval (close-family-links *persons* *rule-set*)) 
      moss::*deferred-defaults*)

#|
(defVar instructions)

(eval-when (load compile execute)
  (moss::mformat "~%; family-file / persons:~%~S" *persons*)
  (moss::mformat "~%; family-file / rule set:~%~S" *rule-set*)
  (setq instructions (close-family-links *persons* *rule-set*))
  (format t "~%; family-file / instructions for closing links:~%~S" instructions)
  (mapcar #'eval instructions)
  )

(sleep 5)
|#
;;; Adding orphans

(defobject ("NAME" "Dupond")("sex" "m"))
(defobject ("NAME" "Durand" )("SEX" "f")("AGE" 33))


(moss::mformat "~%*** family file loaded ***")